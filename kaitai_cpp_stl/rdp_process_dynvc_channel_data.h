#ifndef _H_RDP_PROCESS_DYNVC_CHANNEL_DATA_H_
#define _H_RDP_PROCESS_DYNVC_CHANNEL_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <dynvc_pdu.h>
#include <rdp_segmented_data.h>
#include <rdp8_bulk_encoded_data.h>

dynvc_pdu_t::channel_type_t rdp_process_dynvc_channel_name_to_channel_type(const char *channel_name);

void rdp_process_dynvc_channel_type(std::function<uint8_t(bool,uint32_t)> func);

class rdp_process_dynvc_channel_data_t
{
    public:
        rdp_process_dynvc_channel_data_t(dynvc_pdu_t *_root) : root_(_root) {};
        ~rdp_process_dynvc_channel_data_t() {};
        std::string decode(const std::string &data);
    private:
        dynvc_pdu_t *root_;
};

#endif  // _H_RDP_PROCESS_DYNVC_CHANNEL_DATA_H_
