#ifndef _H_RDP_PROCESS_STATIC_CHANNEL_DATA_H_
#define _H_RDP_PROCESS_STATIC_CHANNEL_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <channel_pdu.h>

void rdp_process_static_channel_decompress(std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> func);

class rdp_process_static_channel_data_t
{
    public:
        rdp_process_static_channel_data_t(channel_pdu_t *_root) : root_(_root) {};
        ~rdp_process_static_channel_data_t() {};
        std::string decode(const std::string &data);
    private:
        channel_pdu_t *root_;
};

#endif  // _H_RDP_PROCESS_STATIC_CHANNEL_DATA_H_
