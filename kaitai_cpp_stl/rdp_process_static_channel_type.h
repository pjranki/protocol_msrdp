#ifndef _H_RDP_PROCESS_STATIC_CHANNEL_TYPE_H_
#define _H_RDP_PROCESS_STATIC_CHANNEL_TYPE_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <t125_mcs_pdu.h>
#include <channel_pdu.h>

channel_pdu_t::channel_type_t rdp_process_static_channel_name_to_channel_type(const char *channel_name);

void rdp_process_static_channel_type(std::function<uint8_t(bool,uint16_t)> func);

class rdp_process_static_channel_type_t
{
    public:
        rdp_process_static_channel_type_t(t125_mcs_pdu_t *_root) : root_(_root) {};
        ~rdp_process_static_channel_type_t() {};
        std::string decode(const std::string &data);
    private:
        t125_mcs_pdu_t *root_;
};

#endif  // _H_RDP_PROCESS_STATIC_CHANNEL_TYPE_H_
