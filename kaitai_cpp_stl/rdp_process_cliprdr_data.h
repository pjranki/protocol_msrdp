#ifndef _H_RDP_PROCESS_CLIPRDR_DATA_H_
#define _H_RDP_PROCESS_CLIPRDR_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <cliprdr_pdu.h>

class rdp_process_cliprdr_data_t
{
    public:
        rdp_process_cliprdr_data_t(cliprdr_pdu_t *_root) : root_(_root) {};
        ~rdp_process_cliprdr_data_t() {};
        std::string decode(const std::string &data);
    private:
        cliprdr_pdu_t *root_;
};

#endif  // _H_RDP_PROCESS_CLIPRDR_DATA_H_
