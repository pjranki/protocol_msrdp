#include "rdp_process_slowpath_data.h"

static std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> g_decompress = nullptr;

void rdp_process_slowpath_data_decompress(std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> func)
{
    g_decompress = func;
}

static bool set_slowpath_pdu_type_2(slowpath_data_pdu_t *root, uint8_t code)
{
    uint8_t *buffer = ((uint8_t*)root);
    for ( size_t i = 0; i < (sizeof(*root) - sizeof(uint8_t)); i++)
    {
        uint8_t tmp = buffer[i];
        buffer[i] = 0xaa;
        if ( root->pdu_type_2() == 0xaa )
        {
            buffer[i] = 0x55;
            if ( root->pdu_type_2() == 0x55 )
            {
                buffer[i] = code;
                return true;
            }
        }
        buffer[i] = tmp;
    }
    return false;
}

static bool set_compressed_flag(slowpath_data_pdu_t *root, bool set)
{
    uint8_t *buffer = ((uint8_t*)root);
    uint8_t tmp[sizeof(bool)];
    for ( size_t i = (sizeof(*root) - sizeof(tmp)); i > 0; i--)
    {
        memcpy(tmp, &(buffer[i]), sizeof(tmp));
        *((bool*)(&buffer[i])) = true;
        if ( root->is_compressed() == true )
        {
            *((bool*)(&buffer[i])) = false;
            if ( root->is_compressed() == false )
            {
               *((bool*)(&buffer[i])) = set;
                return true;
            }
        }
        memcpy(&(buffer[i]), tmp, sizeof(tmp));
    }
    return false;
}

std::string rdp_process_slowpath_data_t::decode(const std::string &data)
{
    if (this->root_->is_compressed())
    {
        std::string out_data;
        if ( g_decompress && g_decompress(data,
                false, // to client
                this->root_->compression_type(),
                this->root_->compression_packed_at_front(),
                this->root_->compression_packed_flushed(),
                &out_data)
            )
        {
            return out_data;
        }
        set_slowpath_pdu_type_2(this->root_, slowpath_data_pdu_t::PDUTYPE2_RAW);
        return data;
    }
    return data;
}
