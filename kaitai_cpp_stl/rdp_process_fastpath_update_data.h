#ifndef _H_RDP_PROCESS_FASTPATH_UPDATE_DATA_H_
#define _H_RDP_PROCESS_FASTPATH_UPDATE_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <ts_fp_update.h>

void rdp_process_fastpath_update_decompress(std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> func);

class rdp_process_fastpath_update_data_t
{
    public:
        rdp_process_fastpath_update_data_t(ts_fp_update_t *_root) : root_(_root) {};
        ~rdp_process_fastpath_update_data_t() {};
        std::string decode(const std::string &data);
    private:
        ts_fp_update_t *root_;
};

#endif  // _H_RDP_PROCESS_FASTPATH_UPDATE_DATA_H_
