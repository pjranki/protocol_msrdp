#ifndef _H_RDP_PROCESS_SLOWPATH_DATA_H_
#define _H_RDP_PROCESS_SLOWPATH_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <functional>

#include <slowpath_data_pdu.h>

void rdp_process_slowpath_data_decompress(std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> func);

class rdp_process_slowpath_data_t
{
    public:
        rdp_process_slowpath_data_t(slowpath_data_pdu_t *_root) : root_(_root) {};
        ~rdp_process_slowpath_data_t() {};
        std::string decode(const std::string &data);
    private:
        slowpath_data_pdu_t *root_;
};

#endif  // _H_RDP_PROCESS_SLOWPATH_DATA_H_
