#ifndef _H_RDP_DECODE_DATA_H_
#define _H_RDP_DECODE_DATA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t rdp_decode_data_size(const void *data, size_t size);
void rdp_decode_data(const void *data, size_t data_size, bool to_server);

bool rdp_decode_is_heartbeat(const void *data, size_t data_size, bool to_server);

#ifdef __cplusplus
}
#endif

#endif  // _H_RDP_DECODE_DATA_H_
