#include <string.h>
#include <stdio.h>

#include "rdp_decode_data.h"

#include <rdp.h>
#include <ts_fp_input_pdu.h>
#include <ts_fp_update_pdu.h>
#include <ts_fp_update.h>
#include <ts_rail_pdu.h>
#include <ts_rail_ri_pdu.h>
#include <ts_rail_wi_pdu.h>
#include <ts_drawing_order.h>
#include <ts_primary_drawing_order.h>
#include <ts_secondary_drawing_order.h>
#include <ts_altsec_drawing_order.h>
#include <ts_window_order.h>
#include <ts_compdesk_drawing_order.h>
#include <ts_update_bitmap_data.h>
#include <ts_update_palette_data.h>
#include <ts_update_sync.h>
#include <ts_bitmap_data.h>
#include <ts_bitmap_data_ex.h>
#include <ts_pointerposattribute.h>
#include <ts_colorpointerattribute.h>
#include <ts_cachedpointerattribute.h>
#include <ts_pointerattribute.h>
#include <ts_cd_header.h>
#include <ts_compressed_bitmap_header_ex.h>
#include <ts_palette_entry.h>
#include <ts_surfcmd.h>
#include <ts_point16.h>
#include <t123_tpkt.h>
#include <x224_tpdu.h>
#include <t125_mcs_pdu.h>
#include <rdp_server_redirection_packet.h>
#include <ts_security_pdu.h>
#include <slowpath_pdu.h>
#include <slowpath_control_pdu.h>
#include <slowpath_data_pdu.h>
#include <channel_pdu.h>
#include <rdp_raw_pdu.h>
#include <rdpdr_pdu.h>
#include <cliprdr_pdu.h>
#include <dynvc_pdu.h>
#include <echo_pdu.h>
#include <rdpsnd_pdu.h>
#include <rdpsnd_lossy_pdu.h>
#include <sndin_pdu.h>
#include <audio_format.h>
#include <rdpgfx_pdu.h>
#include <rdpgfx_redir_pdu.h>
#include <wdag_dvc_pdu.h>

static void hexdumpline(size_t offset, void *buf, size_t size)
{
    char line[128];
    char *data = (char*)buf;
    char *pos = &(line[0]);
    size_t i;

    memset(line, 0, sizeof(line));

    if (size > 16)
    {
        size = 16;
    }

    for (i = 0; i < size; i++)
    {
        snprintf(pos, 4, "%02x ", (unsigned char)(data[i]));
        pos += 3;
    }
    for (i = 0; i < (16 - size); i++)
    {
        memcpy(pos, "   ", 3);
        pos += 3;
    }
    memcpy(pos, "| ", 2);
    pos += 2;
    for (i = 0; i < size; i++)
    {
        if ((data[i] >= ' ') && (data[i] <= '~'))
        {
            *pos = data[i];
        }
        else
        {
            *pos = '.';
        }
        pos += 1;
    }
    printf("%08x %s\n", (int) offset, line);
}

static void hexdump(const void *buf, size_t size)
{
    size_t i;
    char *data;

    for (i = 0; i < size; i += 16)
    {
        data = ((char*)buf) + i;
        if ((i + 16) <= size)
        {
            hexdumpline(i, data, 16);
        }
        else
        {
            hexdumpline(i, data, size - i);
        }
    }
}

static const char *rdp_decode_direction_str(bool to_server)
{
    return to_server ? "client-to-server" : "server-to-client";
}

static const char *rdp_decode_fragment_compressed_str(bool fragment, bool compressed)
{
    if ( fragment && compressed )
    {
        return "FRAGMENT | COMPRESSED";
    }
    if ( fragment )
    {
        return "FRAGMENT";
    }
    if ( compressed )
    {
        return "COMPRESSED";
    }
    return "RAW";
}

static void byte_swap_be_u16(uint16_t *value)
{
    if ( *((uint16_t*)"\x12\x34") == 0x1234 )
    {
        // big endian
        // nothing to do
    }
    else
    {
        // little endian
        uint16_t new_value = 0;
        new_value |= (*value >> 8) & 0x00ff;
        new_value |= (*value << 8) & 0xff00;
        *value = new_value;
    }
}

size_t rdp_decode_data_size(const void *data, size_t size)
{
    if ( size < 1 )
    {
        return 0;
    }

    uint8_t t123_tpkt_version = ((const uint8_t*)data)[0];
    uint8_t ts_fp_action = t123_tpkt_version & 0x3;
    size_t length = 0;

    if ( ts_fp_action == 3 )
    {
        if ( size < 4 )
        {
            return 0;
        }
        uint16_t t123_tpkt_length = *((const uint16_t*)(((const uint8_t*)data) + 2));
        byte_swap_be_u16(&t123_tpkt_length);
        length = (size_t)t123_tpkt_length;
    }

    if ( ts_fp_action == 0 )
    {
        if ( size < 2 )
        {
            return 0;
        }
        uint8_t ts_fp_length1 = ((const uint8_t*)data)[1];
        if ( ( ts_fp_length1 & 0x80 ) == 0 )
        {
            length = (size_t)ts_fp_length1;
        }
        else
        {
            if ( size < 3 )
            {
                return 0;
            }
            uint8_t ts_fp_length2 = ((const uint8_t*)data)[2];
            length = (((size_t)(ts_fp_length1 &~ 0x80)) << 8) | ((size_t)ts_fp_length2);
        }
    }

    if ( length > size )
    {
        return 0;
    }
    return length;
}

static void dump_ts_fp_input_pdu(ts_fp_input_pdu_t &ts_fp_input_pdu)
{
    if ( 0 == ts_fp_input_pdu.input_event_array()->input_event()->size() )
    {
        printf("(TS_FP_INPUT_PDU with no events)\n");
    }
    for ( auto _event : *ts_fp_input_pdu.input_event_array()->input_event() )
    {
        ts_fp_input_pdu_t::ts_fp_input_event_t &event = *_event;
        switch(event.code())
        {
            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_SCANCODE:
                printf("TS_FP_KEYBOARD_EVENT: scancode:0x%02x\n",
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_keyboard_event_t*>(event.event())->key_code());
                break;

            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_MOUSE:
                printf("TS_FP_POINTER_EVENT: (%d, %d)\n",
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_pointer_event_t*>(event.event())->x_pos(),
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_pointer_event_t*>(event.event())->y_pos());
                break;

            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_MOUSEX:
                printf("TS_FP_POINTERX_EVENT: (%d, %d)\n",
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_pointerx_event_t*>(event.event())->x_pos(),
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_pointerx_event_t*>(event.event())->y_pos());
                break;

            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_SYNC:
                printf("TS_FP_SYNC_EVENT\n");
                break;

            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_UNICODE:
                printf("TS_FP_UNICODE_KEYBOARD_EVENT: unicode_code:0x%04x\n",
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_unicode_keyboard_event_t*>(event.event())->unicode_code());
                break;

            case ts_fp_input_pdu_t::ts_fp_input_event_t::FASTPATH_INPUT_EVENT_QOE_TIMESTAMP:
                printf("TS_FP_QOE_TIMESTAMP_EVENT: %d (ms)\n",
                    reinterpret_cast<ts_fp_input_pdu_t::ts_fp_qoe_timestamp_event_t*>(event.event())->timestamp());
                break;

            default:
                printf("(unknown event)\n");
                break;
        }
    }
}

static void dump_ts_window_info_order_info(ts_window_order_t::ts_window_info_order_t &ts_window_info_order, ts_window_order_t::ts_window_info_order_info_t &ts_window_info_order_info)
{
    printf("[window_id=0x%08x] TS_WINDOW_ORDER_INFO\n", ts_window_info_order.window_id());
    printf(" fields_present_flags: 0x%08x\n", ts_window_info_order.fields_present_flags());
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000002) != 0 )
    {
        printf(" owner_window_id: 0x%08x\n", ts_window_info_order_info.owner_window_id());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000008) != 0 )
    {
        printf(" style: 0x%08x\n", ts_window_info_order_info.style());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000008) != 0 )
    {
        printf(" extended_style: 0x%08x\n", ts_window_info_order_info.extended_style());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000010) != 0 )
    {
        printf(" show_state: 0x%02x\n", ts_window_info_order_info.show_state());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000004) != 0 )
    {
        wchar_t title[0x1000];
        memset(title, 0, sizeof(title));
        memcpy(title, ts_window_info_order_info.title_info()->string().c_str(), ts_window_info_order_info.title_info()->string().size());
        printf(" title_info: '%S'\n", title);
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00004000) != 0 )
    {
        printf(" client_offset_x: 0x%08x\n", ts_window_info_order_info.client_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00004000) != 0 )
    {
        printf(" client_offset_y: 0x%08x\n", ts_window_info_order_info.client_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000) != 0 )
    {
        printf(" client_area_width: 0x%08x\n", ts_window_info_order_info.client_area_width());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000) != 0 )
    {
        printf(" client_area_height: 0x%08x\n", ts_window_info_order_info.client_area_height());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x80) != 0 )
    {
        printf(" window_left_resize_margin: 0x%08x\n", ts_window_info_order_info.window_left_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x80) != 0 )
    {
        printf(" window_right_resize_margin: 0x%08x\n", ts_window_info_order_info.window_right_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x08000000) != 0 )
    {
        printf(" window_top_resize_margin: 0x%08x\n", ts_window_info_order_info.window_top_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x08000000) != 0 )
    {
        printf(" window_bottom_resize_margin: 0x%08x\n", ts_window_info_order_info.window_bottom_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00020000) != 0 )
    {
        printf(" rp_content: 0x%02x\n", ts_window_info_order_info.rp_content());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00040000) != 0 )
    {
        printf(" root_parent_handle: 0x%08x\n", ts_window_info_order_info.root_parent_handle());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000800) != 0 )
    {
        printf(" window_offset_x: 0x%08x\n", ts_window_info_order_info.window_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000800) != 0 )
    {
        printf(" window_offset_y: 0x%08x\n", ts_window_info_order_info.window_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00008000) != 0 )
    {
        printf(" window_client_delta_x: 0x%08x\n", ts_window_info_order_info.window_client_delta_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00008000) != 0 )
    {
        printf(" window_client_delta_y: 0x%08x\n", ts_window_info_order_info.window_client_delta_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000400) != 0 )
    {
        printf(" window_width: 0x%08x\n", ts_window_info_order_info.window_width());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000400) != 0 )
    {
        printf(" window_height: 0x%08x\n", ts_window_info_order_info.window_height());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000100) != 0 )
    {
        printf(" num_window_rects: 0x%d\n", ts_window_info_order_info.num_window_rects());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000100) != 0 )
    {
        // ts_rectangle_16
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00001000) != 0 )
    {
        printf(" visible_offset_x: 0x%08x\n", ts_window_info_order_info.visible_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00001000) != 0 )
    {
        printf(" visible_offset_y: 0x%08x\n", ts_window_info_order_info.visible_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000200) != 0 )
    {
        printf(" numb_visible_rects: 0x%d\n", ts_window_info_order_info.numb_visible_rects());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000200) != 0 )
    {
        // ts_rectangle_16
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00400000) != 0 )
    {
        wchar_t overlay_description[0x4000];
        memset(overlay_description, 0, sizeof(overlay_description));
        memcpy(overlay_description, ts_window_info_order_info.overlay_description()->string().c_str(), ts_window_info_order_info.overlay_description()->string().size());
        printf(" overlay_description: '%S'\n", overlay_description);
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00800000) != 0 )
    {
        printf(" taskbar_button: 0x%02x\n", ts_window_info_order_info.taskbar_button());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00080000) != 0 )
    {
        printf(" enforce_server_z_order: 0x%02x\n", ts_window_info_order_info.enforce_server_z_order());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000040) != 0 )
    {
        printf(" app_bar_state: 0x%02x\n", ts_window_info_order_info.app_bar_state());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000001) != 0 )
    {
        printf(" app_bar_edge: 0x%02x\n", ts_window_info_order_info.app_bar_edge());
    }
}

static void dump_ts_window_info_order(ts_window_order_t::ts_window_info_order_t &ts_window_info_order)
{
    ts_window_order_t::ts_window_info_order_info_t *ts_window_info_order_info = nullptr;
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000000 ) != 0 )
    {
        printf("------------------------ NEW WINDOW ------------------------\n");
    }
    switch(ts_window_info_order.order_type())
    {
        case ts_window_order_t::ts_window_info_order_t::WINDOW_ORDER_TYPE_WINDOW_INFORMATION:
            ts_window_info_order_info = reinterpret_cast<ts_window_order_t::ts_window_info_order_info_t*>(ts_window_info_order.order());
            dump_ts_window_info_order_info(ts_window_info_order, *ts_window_info_order_info);
            break;

        case ts_window_order_t::ts_window_info_order_t::WINDOW_ORDER_TYPE_WINDOW_ICON:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_ICON\n", ts_window_info_order.window_id());
            break;

        case ts_window_order_t::ts_window_info_order_t::WINDOW_ORDER_TYPE_CACHED_ICON:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_CACHED_ICON\n", ts_window_info_order.window_id());
            break;

        case ts_window_order_t::ts_window_info_order_t::WINDOW_ORDER_TYPE_DELETE_WINDOW:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_DELETE_WINDOW\n", ts_window_info_order.window_id());
            break;

        default:
            printf("(unknown window order 0x%08x)\n", ts_window_info_order.order_type());
            break;
    }
}

static void dump_ts_notifyicon_order(ts_window_order_t::ts_notifyicon_order_t &ts_notifyicon_order)
{
    switch(ts_notifyicon_order.order_type())
    {
        case ts_window_order_t::ts_notifyicon_order_t::NOTIFYICON_ORDER_TYPE_ICON_INFORMATION:
            printf("[window_id=0x%08x] TS_NOTIFYICON_ORDER_INFO\n", ts_notifyicon_order.window_id());
            break;

        case ts_window_order_t::ts_notifyicon_order_t::NOTIFYICON_ORDER_TYPE_DELETE_ICON:
            printf("[window_id=0x%08x] TS_NOTIFYICON_ORDER_DELETE\n", ts_notifyicon_order.window_id());
            break;

        default:
            printf("(unknown notify icon order 0x%08x)\n", ts_notifyicon_order.order_type());
            break;
    }
}

static void dump_ts_desktop_order(ts_window_order_t::ts_desktop_order_t &ts_desktop_order)
{
    switch(ts_desktop_order.order_type())
    {
        case ts_window_order_t::ts_desktop_order_t::DESKTOP_ORDER_TYPE_ACTIVELY_MONITORED :
            printf("TS_DESKTOP_ORDER_ACTIVELY_MONITORED\n");
            break;

        case ts_window_order_t::ts_desktop_order_t::DESKTOP_ORDER_TYPE_NON_MONITORED :
            printf("TS_DESKTOP_ORDER_NON_MONITORED\n");
            break;

        default:
            printf("(unknown desktop order 0x%08x)\n", ts_desktop_order.order_type());
            break;
    }
}

static void dump_ts_window_order(ts_window_order_t &ts_window_order)
{
    switch(ts_window_order.order_type())
    {
        case ts_window_order_t::WINDOW_ORDER_TYPE_WINDOW:
            dump_ts_window_info_order(*reinterpret_cast<ts_window_order_t::ts_window_info_order_t*>(ts_window_order.order()));
            break;

        case ts_window_order_t::WINDOW_ORDER_TYPE_NOTIFY:
            dump_ts_notifyicon_order(*reinterpret_cast<ts_window_order_t::ts_notifyicon_order_t*>(ts_window_order.order()));
            break;

        case ts_window_order_t::WINDOW_ORDER_TYPE_DESKTOP:
            dump_ts_desktop_order(*reinterpret_cast<ts_window_order_t::ts_desktop_order_t*>(ts_window_order.order()));
            break;

        default:
            printf("(unknown window drawing order 0x%08x)\n", ts_window_order.order_type());
            break;
    }
}

static void dump_ts_altsec_drawing_orders(ts_altsec_drawing_order_t &ts_altsec_drawing_order)
{
    switch(ts_altsec_drawing_order.order_type())
    {
        case ts_altsec_drawing_order_t::TS_ALTSEC_SWITCH_SURFACE:
            printf("TS_ALTSEC_SWITCH_SURFACE\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_CREATE_OFFSRC_BITMAP:
            printf("TS_ALTSEC_CREATE_OFFSRC_BITMAP\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_STREAM_BITMAP_FIRST:
            printf("TS_ALTSEC_STREAM_BITMAP_FIRST\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_STREAM_BITMAP_NEXT:
            printf("TS_ALTSEC_STREAM_BITMAP_NEXT\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_CREATE_NINEGRID_BITMAP:
            printf("TS_ALTSEC_CREATE_NINEGRID_BITMAP\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_FIRST:
            printf("TS_ALTSEC_GDIP_FIRST\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_NEXT:
            printf("TS_ALTSEC_GDIP_NEXT\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_END:
            printf("TS_ALTSEC_GDIP_END\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_CACHE_FIRST:
            printf("TS_ALTSEC_GDIP_CACHE_FIRST\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_CACHE_NEXT:
            printf("TS_ALTSEC_GDIP_CACHE_NEXT\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_GDIP_CACHE_END:
            printf("TS_ALTSEC_GDIP_CACHE_END\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_WINDOW:
            dump_ts_window_order(*reinterpret_cast<ts_window_order_t*>(ts_altsec_drawing_order.order()));
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_COMPDESK_FIRST:
            printf("TS_COMPDESK_DRAWING_ORDER\n");
            break;

        case ts_altsec_drawing_order_t::TS_ALTSEC_FRAME_MARKER:
            printf("TS_ALTSEC_FRAME_MARKER\n");
            break;

        default:
            printf("(unknown alternative order type 0x%02x)\n", ts_altsec_drawing_order.order_type());
            break;
    }
}

static void dump_ts_fp_update_orders(ts_fp_update_t::ts_fp_update_orders_t &ts_fp_update_orders)
{
    for ( auto _drawing_order : *ts_fp_update_orders.orders() )
    {
        ts_drawing_order_t &drawing_order = *_drawing_order;
        switch(drawing_order.order_class())
        {
            case ts_drawing_order_t::ORDER_CLASS_PRIMARY:
                printf("ORDER_CLASS_PRIMARY\n");
                break;
            case ts_drawing_order_t::ORDER_CLASS_SECONDARY:
                printf("ORDER_CLASS_SECONDARY\n");
                break;
            case ts_drawing_order_t::ORDER_CLASS_ALTERNATIVE:
                dump_ts_altsec_drawing_orders(*reinterpret_cast<ts_altsec_drawing_order_t*>(drawing_order.order()));
                break;
            default:
                printf("(unknown drawing order class)\n");
                break;
        }
    }
}

static void dump_ts_update_bitmap_data(ts_update_bitmap_data_t &ts_update_bitmap_data)
{
    printf("TS_UPDATE_BITMAP_DATA\n");
}

static void dump_ts_update_palette_data(ts_update_palette_data_t &ts_update_palette_data)
{
    printf("TS_UPDATE_PALETTE_DATA\n");
}

static void dump_ts_update_sync(ts_update_sync_t &ts_update_sync)
{
    printf("TS_UPDATE_SYNC\n");
}

static void dump_ts_surfcmd(ts_surfcmd_t &ts_surfcmd)
{
    switch(ts_surfcmd.cmd_type())
    {
        case ts_surfcmd_t::CMDTYPE_SET_SURFACE_BITS:
            printf("TS_SURFCMD_SET_SURF_BITS\n");
            break;
        case ts_surfcmd_t::CMDTYPE_FRAME_MARKER:
            printf("TS_FRAME_MARKER\n");
            break;
        case ts_surfcmd_t::CMDTYPE_STREAM_SURFACE_BITS:
            printf("TS_SURFCMD_STREAM_SURF_BITS\n");
            break;
        default:
            printf("(unknown TS_SURFCMD class)\n");
            break;
    }
}

static void dump_ts_pointerposattribute(ts_pointerposattribute_t &ts_pointerposattribute)
{
    printf("TS_POINTERPOSATTRIBUTE (%d, %d)\n",
        ts_pointerposattribute.position()->x_pos(),
        ts_pointerposattribute.position()->y_pos());
}

static void dump_ts_colorpointerattribute(ts_colorpointerattribute_t &ts_colorpointerattribute)
{
    printf("TS_COLORPOINTERATTRIBUTE\n");
}

static void dump_ts_cachedpointerattribute(ts_cachedpointerattribute_t &ts_cachedpointerattribute)
{
    printf("TS_CACHEDPOINTERATTRIBUTE\n");
}

static void dump_ts_pointerattribute(ts_pointerattribute_t &ts_pointerattribute)
{
    printf("TS_POINTERATTRIBUTE\n");
}

static void dump_ts_fp_surfcmds(ts_fp_update_t::ts_fp_surfcmds_t &ts_fp_surfcmds)
{
    for ( auto _ts_surfcmd : *ts_fp_surfcmds.surface_commands() )
    {
        ts_surfcmd_t &ts_surfcmd = *_ts_surfcmd;
        dump_ts_surfcmd(ts_surfcmd);
    }
}

static void dump_ts_fp_systempointerhiddenattribute(ts_fp_update_t::ts_fp_systempointerhiddenattribute_t &ts_fp_systempointerhiddenattribute)
{
    printf("TS_FP_SYSTEMPOINTERHIDDENATTRIBUTE\n");
}

static void dump_ts_fp_systempointerdefaultattribute(ts_fp_update_t::ts_fp_systempointerdefaultattribute_t &ts_fp_systempointerdefaultattribute)
{
    printf("TS_FP_SYSTEMPOINTERDEFAULTATTRIBUTE\n");
}

static void dump_ts_fp_largepointerattribute(ts_fp_update_t::ts_fp_largepointerattribute_t &ts_fp_largepointerattribute)
{
    printf("TS_FP_LARGEPOINTERATTRIBUTE\n");
}

static void dump_ts_fp_meta_command_update(ts_fp_update_t::ts_fp_meta_command_update_t &ts_fp_meta_command_update)
{
    printf("TS_FP_META_COMMAND_UPDATE (UNDOCUMENTED)\n");
}

static void dump_ts_fp_delta_encoder_update(ts_fp_update_t::ts_fp_delta_encoder_update_t &ts_fp_delta_encoder_update)
{
    printf("TS_FP_DELTA_ENCODER_UPDATE (UNDOCUMENTED)\n");
}

static void dump_ts_fp_update(ts_fp_update_t &ts_fp_update)
{
    ts_fp_update_t::ts_fp_update_data_t &ts_fp_update_data = *(ts_fp_update.data());
    switch(ts_fp_update.code())
    {
        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_RAW:
            if ( reinterpret_cast<ts_fp_update_t::ts_fp_update_raw_t*>(ts_fp_update_data.data())->data().size() > 0 )
            {
                printf("FASTPATH UPDATE (%s) DATA (%s):\n",
                    rdp_decode_fragment_compressed_str(ts_fp_update.is_fragment(), ts_fp_update.is_compressed()),
                    rdp_decode_direction_str(false));
                hexdump(
                    reinterpret_cast<ts_fp_update_t::ts_fp_update_raw_t*>(ts_fp_update_data.data())->data().c_str(),
                    reinterpret_cast<ts_fp_update_t::ts_fp_update_raw_t*>(ts_fp_update_data.data())->data().size()
                );
            }
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_ORDERS:
            dump_ts_fp_update_orders(*reinterpret_cast<ts_fp_update_t::ts_fp_update_orders_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_BITMAP:
            dump_ts_update_bitmap_data(*(reinterpret_cast<ts_fp_update_t::ts_fp_update_bitmap_t*>(ts_fp_update_data.data())->bitmap_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_PALETTE:
            dump_ts_update_palette_data(*(reinterpret_cast<ts_fp_update_t::ts_fp_update_palette_t*>(ts_fp_update_data.data())->palette_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_SYNCHRONIZE:
            dump_ts_update_sync(*(reinterpret_cast<ts_fp_update_t::ts_fp_update_synchronize_t*>(ts_fp_update_data.data())->update_sync()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_SURFCMDS:
            dump_ts_fp_surfcmds(*reinterpret_cast<ts_fp_update_t::ts_fp_surfcmds_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_PTR_NULL:
            dump_ts_fp_systempointerhiddenattribute(*reinterpret_cast<ts_fp_update_t::ts_fp_systempointerhiddenattribute_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_PTR_DEFAULT:
            dump_ts_fp_systempointerdefaultattribute(*reinterpret_cast<ts_fp_update_t::ts_fp_systempointerdefaultattribute_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_PTR_POSITION:
            dump_ts_pointerposattribute(*(reinterpret_cast<ts_fp_update_t::ts_fp_pointerposattribute_t*>(ts_fp_update_data.data())->pointer_position_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_COLOR:
            dump_ts_colorpointerattribute(*(reinterpret_cast<ts_fp_update_t::ts_fp_colorpointerattribute_t*>(ts_fp_update_data.data())->color_pointer_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_CACHED:
            dump_ts_cachedpointerattribute(*(reinterpret_cast<ts_fp_update_t::ts_fp_cachedpointerattribute_t*>(ts_fp_update_data.data())->cached_pointer_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_POINTER:
            dump_ts_pointerattribute(*(reinterpret_cast<ts_fp_update_t::ts_fp_pointerattribute_t*>(ts_fp_update_data.data())->new_pointer_data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_LARGE_POINTER:
            dump_ts_fp_largepointerattribute(*reinterpret_cast<ts_fp_update_t::ts_fp_largepointerattribute_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_META_COMMAND:
            dump_ts_fp_meta_command_update(*reinterpret_cast<ts_fp_update_t::ts_fp_meta_command_update_t*>(ts_fp_update_data.data()));
            break;

        case ts_fp_update_t::ts_fp_update_data_t::FASTPATH_UPDATETYPE_DELTA_ENCODER:
            dump_ts_fp_delta_encoder_update(*reinterpret_cast<ts_fp_update_t::ts_fp_delta_encoder_update_t*>(ts_fp_update_data.data()));
            break;

        default:
            printf("(unknown TS_FP_UPDATE)\n");
            break;
    }
}

static void dump_ts_fp_update_pdu(ts_fp_update_pdu_t &ts_fp_update_pdu)
{
    if ( 0 == ts_fp_update_pdu.update_array()->update()->size() )
    {
        printf("(TS_FP_UPDATE_PDU with no updates)\n");
    }
    for ( auto _update : *ts_fp_update_pdu.update_array()->update() )
    {
        ts_fp_update_t &update = *_update;
        dump_ts_fp_update(update);
    }
}

static void dump_server_redir_pkt(rdp_server_redirection_packet_t &server_redirection_packet)
{
    printf("SERVER REDIRECTION PACKET\n");
    // TODO
}

static void dump_security_layer_pdu(ts_security_pdu_t &ts_security_pdu)
{
    switch(ts_security_pdu.msg())
    {
        case ts_security_pdu_t::SEC_EXCHANGE_PKT:
            printf("SEC_EXCHANGE_PKT\n");
            break;

        case ts_security_pdu_t::SEC_TRANSPORT_REQ:
            printf("SEC_TRANSPORT_REQ\n");
            break;

        case ts_security_pdu_t::SEC_TRANSPORT_RSP:
            printf("SEC_TRANSPORT_RSP\n");
            break;

        case ts_security_pdu_t::SEC_INFO_PKT:
            printf("SEC_INFO_PKT\n");
            break;

        case ts_security_pdu_t::SEC_LICENSE_PKT:
            printf("SEC_LICENSE_PKT\n");
            break;

        case ts_security_pdu_t::SEC_REDIRECTION_PKT:
            dump_server_redir_pkt(*reinterpret_cast<rdp_server_redirection_packet_t*>(ts_security_pdu.packet()));
            break;

        case ts_security_pdu_t::SEC_AUTODETECT_REQ:
            printf("SEC_AUTODETECT_REQ\n");
            break;

        case ts_security_pdu_t::SEC_AUTODETECT_RSP:
            printf("SEC_AUTODETECT_RSP\n");
            break;

        case ts_security_pdu_t::SEC_HEARTBEAT:
            printf("SEC_HEARTBEAT: reserved: %d, period: %d, count1: %d, count2: %d\n",
                reinterpret_cast<ts_security_pdu_t::server_heartbeat_pdu_t*>(ts_security_pdu.packet())->reserved(),
                reinterpret_cast<ts_security_pdu_t::server_heartbeat_pdu_t*>(ts_security_pdu.packet())->period(),
                reinterpret_cast<ts_security_pdu_t::server_heartbeat_pdu_t*>(ts_security_pdu.packet())->count1(),
                reinterpret_cast<ts_security_pdu_t::server_heartbeat_pdu_t*>(ts_security_pdu.packet())->count2());
            break;

        default:
            printf("(unknown security layer packet, flags=0x%04x)\n", ts_security_pdu.flags());
            break;
    }
}

static void dump_ts_update_orders(slowpath_data_pdu_t::ts_update_orders_t &ts_update_orders)
{
    for ( auto _drawing_order : *ts_update_orders.orders() )
    {
        ts_drawing_order_t &drawing_order = *_drawing_order;
        switch(drawing_order.order_class())
        {
            case ts_drawing_order_t::ORDER_CLASS_PRIMARY:
                printf("ORDER_CLASS_PRIMARY\n");
                break;
            case ts_drawing_order_t::ORDER_CLASS_SECONDARY:
                printf("ORDER_CLASS_SECONDARY\n");
                break;
            case ts_drawing_order_t::ORDER_CLASS_ALTERNATIVE:
                dump_ts_altsec_drawing_orders(*reinterpret_cast<ts_altsec_drawing_order_t*>(drawing_order.order()));
                break;
            default:
                printf("(unknown drawing order class)\n");
                break;
        }
    }
}

static void dump_ts_surfcmds(slowpath_data_pdu_t::ts_surfcmds_t &ts_surfcmds)
{
    for ( auto _ts_surfcmd : *ts_surfcmds.surface_commands() )
    {
        ts_surfcmd_t &ts_surfcmd = *_ts_surfcmd;
        dump_ts_surfcmd(ts_surfcmd);
    }
}

static void dump_ts_graphics_update(slowpath_data_pdu_t::ts_graphics_update_t &ts_graphic_update)
{
    switch(ts_graphic_update.update_type())
    {
        case slowpath_data_pdu_t::ts_graphics_update_t::UPDATETYPE_ORDERS:
            dump_ts_update_orders(*reinterpret_cast<slowpath_data_pdu_t::ts_update_orders_t*>(ts_graphic_update.data()));
            break;

        case slowpath_data_pdu_t::ts_graphics_update_t::UPDATETYPE_BITMAP:
            dump_ts_update_bitmap_data(*(reinterpret_cast<slowpath_data_pdu_t::ts_update_bitmap_t*>(ts_graphic_update.data())->bitmap_data()));
            break;

        case slowpath_data_pdu_t::ts_graphics_update_t::UPDATETYPE_PALETTE:
            dump_ts_update_palette_data(*(reinterpret_cast<slowpath_data_pdu_t::ts_update_palette_t*>(ts_graphic_update.data())->palette_data()));
            break;

        case slowpath_data_pdu_t::ts_graphics_update_t::UPDATETYPE_SYNCHRONIZE:
            dump_ts_update_sync(*reinterpret_cast<ts_update_sync_t*>(ts_graphic_update.data()));
            break;

        case slowpath_data_pdu_t::ts_graphics_update_t::UPDATETYPE_SURFCMDS:
            dump_ts_surfcmds(*reinterpret_cast<slowpath_data_pdu_t::ts_surfcmds_t*>(ts_graphic_update.data()));
            break;

        default:
            printf("(unknown ts_graphics_update type 0x%02x)\n", (uint8_t)(ts_graphic_update.update_type()));
            break;
    }
}

static void dump_ts_pointer_pdu(slowpath_data_pdu_t::ts_pointer_pdu_t &ts_pointer_pdu)
{
    switch(ts_pointer_pdu.message_type())
    {
        case slowpath_data_pdu_t::ts_pointer_pdu_t::TS_PTRMSGTYPE_SYSTEM:
            printf("TS_SYSTEMPOINTERATTRIBUTE system_pointer_type 0x%08x\n",
                reinterpret_cast<slowpath_data_pdu_t::ts_systempointerattribute_t*>(ts_pointer_pdu.data())->system_pointer_type());
            break;

        case slowpath_data_pdu_t::ts_pointer_pdu_t::TS_PTRMSGTYPE_POSITION:
            dump_ts_pointerposattribute(*reinterpret_cast<ts_pointerposattribute_t*>(ts_pointer_pdu.data()));
            break;

        case slowpath_data_pdu_t::ts_pointer_pdu_t::TS_PTRMSGTYPE_COLOR:
            dump_ts_colorpointerattribute(*reinterpret_cast<ts_colorpointerattribute_t*>(ts_pointer_pdu.data()));
            break;

        case slowpath_data_pdu_t::ts_pointer_pdu_t::TS_PTRMSGTYPE_CACHED:
            dump_ts_cachedpointerattribute(*reinterpret_cast<ts_cachedpointerattribute_t*>(ts_pointer_pdu.data()));
            break;

        case slowpath_data_pdu_t::ts_pointer_pdu_t::TS_PTRMSGTYPE_POINTER:
            dump_ts_pointerattribute(*reinterpret_cast<ts_pointerattribute_t*>(ts_pointer_pdu.data()));
            break;

        default:
            printf("(unknown ts_pointer_pdu message type 0x%02x)\n", (uint8_t)(ts_pointer_pdu.message_type()));
            break;
    }
}

static void dump_slowpath_data(slowpath_data_pdu_t::slowpath_data_t &slowpath_data)
{
    switch(slowpath_data.pdu_type_2())
    {
        case slowpath_data_pdu_t::PDUTYPE2_RAW:
            printf("SLOWPATH DATA RAW\n");
            hexdump(
                reinterpret_cast<slowpath_data_pdu_t::slowpath_data_raw_t*>(slowpath_data.data())->data().c_str(),
                reinterpret_cast<slowpath_data_pdu_t::slowpath_data_raw_t*>(slowpath_data.data())->data().size());
            break;

        case slowpath_data_pdu_t::PDUTYPE2_UPDATE:
            dump_ts_graphics_update(*reinterpret_cast<slowpath_data_pdu_t::ts_graphics_update_t*>(slowpath_data.data()));
            break;

        case slowpath_data_pdu_t::PDUTYPE2_CONTROL:
            printf("SLOWPATH CONTROL\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_POINTER:
            dump_ts_pointer_pdu(*reinterpret_cast<slowpath_data_pdu_t::ts_pointer_pdu_t*>(slowpath_data.data()));
            break;

        case slowpath_data_pdu_t::PDUTYPE2_INPUT:
            printf("SLOWPATH INPUT\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SYNCHRONIZE:
            printf("SLOWPATH SYNCHRONIZE\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_REFRESH_RECT:
            printf("SLOWPATH REFRESH RECT\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_PLAY_SOUND:
            printf("PLAY SOUND duration %d ms, %d Hz\n",
            reinterpret_cast<slowpath_data_pdu_t::ts_play_sound_pdu_data_t*>(slowpath_data.data())->duration(),
            reinterpret_cast<slowpath_data_pdu_t::ts_play_sound_pdu_data_t*>(slowpath_data.data())->frequency());
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SUPPRESS_OUTPUT:
            printf("SLOWPATH SUPPRESS OUTPUT\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SHUTDOWN_REQUEST:
            printf("SLOWPATH SHUTDOWN REQUEST\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SHUTDOWN_DENIED:
            printf("SLOWPATH SHUTDOWN DENIED\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SAVE_SESSION_INFO:
            printf("SLOWPATH SAVE SESSION INFO\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_FONTLIST:
            printf("SLOWPATH FONTLIST\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_FONTMAP:
            printf("SLOWPATH FONT MAP\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SET_KEYBOARD_INDICATORS:
            printf("SLOWPATH SET KEYBOARD INDICATORS\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_BITMAPCACHE_PERSISTENT_LIST:
            printf("SLOWPATH BITMAPCACHE PERSISTENT LIST\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_BITMAPCACHE_ERROR_PDU:
            printf("SLOWPATH BITMAPCACHE ERROR PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SET_KEYBOARD_IME_STATUS:
            printf("SLOWPATH SET KEYBOARD IME STATUS\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_OFFSCRECACHE_ERROR_PDU:
            printf("SLOWPATH OFFSCRECACHE ERROR PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_SET_ERROR_INFO_PDU:
            printf("SLOWPATH SET ERROR INFO PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_DRAWNINEGRID_ERROR_PDU:
            printf("SLOWPATH DRAWNINEGRID ERROR PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_DRAWGDIPLUS_ERROR_PDU:
            printf("SLOWPATH DRAWGDIPLUS ERROR PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_ARC_STATUS_PDU:
            printf("SLOWPATH ARC STATUS PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_INPUT_MODE_CHANGE_PDU:
            printf("SLOWPATH INPUT MODE CHANGE PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_STATUS_INFO_PDU:
            printf("SLOWPATH STATUS INFO PDU\n");
            break;

        case slowpath_data_pdu_t::PDUTYPE2_MONITOR_LAYOUT_PDU:
            printf("SLOWPATH MONITOR LAYOUT PDU\n");
            break;

        default:
            printf("(unknown slowpath data PDU, pdu_type_2=0x%02x)\n", (uint8_t)(slowpath_data.pdu_type_2()));
            break;
    }
}

static void dump_slowpath_data_pdu(slowpath_data_pdu_t &data_pdu)
{
    dump_slowpath_data(*(data_pdu.data()));
}

static void dump_slowpath_control_pdu(slowpath_control_pdu_t &control_pdu)
{
    switch(control_pdu.pdu_type())
    {
        case slowpath_control_pdu_t::PDUTYPE_DEMANDACTIVEPDU:
            printf("SLOWPATH DEMAND ACTIVE PDU\n");
            break;

        case slowpath_control_pdu_t::PDUTYPE_CONFIRMACTIVEPDU:
            printf("SLOWPATH CONFIRM ACTIVE PDU\n");
            break;

        case slowpath_control_pdu_t::PDUTYPE_DEACTIVATEALLPDU:
            printf("SLOWPATH DEACTIVATE ALL PDU\n");
            break;

        case slowpath_control_pdu_t::PDUTYPE_SERVER_REDIR_PKT:
            dump_server_redir_pkt(*reinterpret_cast<rdp_server_redirection_packet_t*>(control_pdu.data()));
            break;

        case slowpath_control_pdu_t::PDUTYPE_DATAPDU:
            dump_slowpath_data_pdu(*reinterpret_cast<slowpath_data_pdu_t*>(control_pdu.data()));
            break;

        default:
            printf("(unknown slowpath PDU, pdu_type=0x%02x)\n", (uint8_t)(control_pdu.pdu_type()));
            break;
    }
}

static void dump_slowpath_pdu(slowpath_pdu_t &slowpath_pdu)
{
    for ( auto _data : *slowpath_pdu.data() )
    {
        slowpath_pdu_t::slowpath_control_t &data = *_data;
        if (data.flow_pdu())
        {
            printf("SLOWPATH FLOW PDU value=0x%04x\n", data.flow_value());
        }
        else
        {
            dump_slowpath_control_pdu(*data.data());
        }
    }
}

static void dump_rdpgfx_capset(rdpgfx_pdu_t::rdpgfx_capset_t &capset)
{
    printf("RDPGFX CAPSET version 0x%08x, data %d bytes\n", capset.version(), (uint32_t)(capset.caps_data().size()));
}

static void dump_rdpgfx_caps_confirm_pdu(rdpgfx_pdu_t::rdp_caps_confirm_pdu_t &caps_confirm_pdu)
{
    printf("RDPGFX CAPSCONFIRM\n");
    dump_rdpgfx_capset(*caps_confirm_pdu.caps_set());
}

static void dump_rdpgfx_caps_advertise_pdu(rdpgfx_pdu_t::rdpgfx_caps_advertise_pdu_t &caps_advertise_pdu)
{
    printf("RDPGFX CAPSADVERTISE\n");
    for ( auto _capset : *(caps_advertise_pdu.caps_sets()) )
    {
        auto capset = *_capset;
        dump_rdpgfx_capset(capset);
    }
}

static void dump_rdpgfx_pdu(rdpgfx_pdu_t &rdpgfx_pdu)
{
    switch(rdpgfx_pdu.cmd_id())
    {
        case rdpgfx_pdu_t::RDPGFX_CMDID_WIRETOSURFACE_1:
            printf("RDPGFX WIRETOSURFACE_1\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_WIRETOSURFACE_2:
            printf("RDPGFX WIRETOSURFACE_2\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_DELETEENCODINGCONTEXT:
            printf("RDPGFX DELETEENCODINGCONTEXT\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_SOLIDFILL:
            printf("RDPGFX SOLIDFILL\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_SURFACETOSURFACE:
            printf("RDPGFX SURFACETOSURFACE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_SURFACETOCACHE:
            printf("RDPGFX SURFACETOCACHE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CACHETOSURFACE:
            printf("RDPGFX CACHETOSURFACE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_EVICTCACHEENTRY:
            printf("RDPGFX EVICTCACHEENTRY\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CREATESURFACE:
            printf("RDPGFX CREATESURFACE surface_id=0x%02x %dx%d pixel_format=0x%02x\n",
                reinterpret_cast<rdpgfx_pdu_t::rdpgfx_create_surface_pdu_t*>(rdpgfx_pdu.data())->surface_id(),
                reinterpret_cast<rdpgfx_pdu_t::rdpgfx_create_surface_pdu_t*>(rdpgfx_pdu.data())->width(),
                reinterpret_cast<rdpgfx_pdu_t::rdpgfx_create_surface_pdu_t*>(rdpgfx_pdu.data())->height(),
                reinterpret_cast<rdpgfx_pdu_t::rdpgfx_create_surface_pdu_t*>(rdpgfx_pdu.data())->pixel_format()->format());
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_DELETESURFACE:
            printf("RDPGFX DELETESURFACE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_STARTFRAME:
            printf("RDPGFX STARTFRAME\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_ENDFRAME:
            printf("RDPGFX ENDFRAME\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_FRAMEACKNOWLEDGE:
            printf("RDPGFX FRAMEACKNOWLEDGE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_RESETGRAPHICS:
            printf("RDPGFX RESETGRAPHICS\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_MAPSURFACETOOUTPUT:
            printf("RDPGFX MAPSURFACETOOUTPUT\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CACHEIMPORTOFFER:
            printf("RDPGFX CACHEIMPORTOFFER\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CACHEIMPORTREPLY:
            printf("RDPGFX CACHEIMPORTREPLY\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CAPSADVERTISE:
            dump_rdpgfx_caps_advertise_pdu(*reinterpret_cast<rdpgfx_pdu_t::rdpgfx_caps_advertise_pdu_t*>(rdpgfx_pdu.data()));
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_CAPSCONFIRM:
            dump_rdpgfx_caps_confirm_pdu(*reinterpret_cast<rdpgfx_pdu_t::rdp_caps_confirm_pdu_t*>(rdpgfx_pdu.data()));
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_DIAGNOSTIC:
            printf("RDPGFX DIAGNOSTIC\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_MAPSURFACETOWINDOW:
            printf("RDPGFX MAPSURFACETOWINDOW\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_QOEFRAMEACKNOWNLEDGE:
            printf("RDPGFX QOEFRAMEACKNOWNLEDGE\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_MAPSURFACETOSCALEDOUTPUT:
            printf("RDPGFX MAPSURFACETOSCALEDOUTPUT\n");
            break;

        case rdpgfx_pdu_t::RDPGFX_CMDID_MAPSURFACETOSCALEDWINDOW:
            printf("RDPGFX MAPSURFACETOSCALEDWINDOW\n");
            break;

        default:
            printf("(unknown Microsoft::Windows::RDS::Graphics type %d)\n", rdpgfx_pdu.cmd_id());
            break;
    }
}

static void dump_rdpgfx_redir_pdu(rdpgfx_redir_pdu_t &rdpgfx_redir_pdu)
{
    rdpgfx_redir_pdu_t::graphics_update_pdu_t *graphics_update_pdu = nullptr;
    rdpgfx_redir_pdu_t::graphics_update_ack_pdu_t *graphics_update_ack_pdu = nullptr;
    rdpgfx_redir_pdu_t::create_surface_pdu_t *create_surface_pdu = nullptr;
    rdpgfx_redir_pdu_t::delete_surface_pdu_t *delete_surface_pdu = nullptr;
    rdpgfx_redir_pdu_t::error_pdu_t *error_pdu = nullptr;
    rdpgfx_redir_pdu_t::create_composition_surface_pdu_t *create_composition_surface_pdu = nullptr;
    struct RdpWin32RemoteAppPresenter *remote_app_presenter = NULL;

    switch(rdpgfx_redir_pdu.cmd_id())
    {
        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_PDU:
            graphics_update_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::graphics_update_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_PDU\n",
                (uint32_t)(graphics_update_pdu->window_id() >> 32), (uint32_t)(graphics_update_pdu->window_id() >> 0));
            break;

        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_ACK_PDU:
            graphics_update_ack_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::graphics_update_ack_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_ACK_PDU\n",
                (uint32_t)(graphics_update_ack_pdu->window_id() >> 32), (uint32_t)(graphics_update_ack_pdu->window_id() >> 0));
            break;

        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_CREATE_SURFACE_PDU:
            create_surface_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::create_surface_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_CREATE_SURFACE_PDU\n",
                (uint32_t)(create_surface_pdu->window_id() >> 32), (uint32_t)(create_surface_pdu->window_id() >> 0));
            printf(" \\VmSharedMemory\\Host\\%S\\%S\n", L"<HiDefRemoteAppContainerGUID>", (const wchar_t*)create_surface_pdu->section_name().c_str());
            printf(" vm='%S'\n", L"<HiDefRemoteAppContainerGUID>");
            printf(" section='%S'\n", (const wchar_t*)create_surface_pdu->section_name().c_str());
            printf(" width=%d, height=%d, bpp=4, region_size=%d, scan_line_size=%d\n",
                create_surface_pdu->width(), create_surface_pdu->height(), create_surface_pdu->region_size(), create_surface_pdu->scan_line_size());
            break;

        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_DELETE_SURFACE_PDU:
            delete_surface_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::delete_surface_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_DELETE_SURFACE_PDU\n",
                (uint32_t)(delete_surface_pdu->window_id() >> 32), (uint32_t)(delete_surface_pdu->window_id() >> 0));
            break;

        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_ERROR_PDU:
            error_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::error_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("RDPGFX_REDIR_CMDID_ERROR_PDU error=0x%08x\n", error_pdu->error());
            break;

        case rdpgfx_redir_pdu_t::RDPGFX_REDIR_CMDID_CREATE_COMPOSITION_SURFACE_PDU:
            create_composition_surface_pdu = reinterpret_cast<rdpgfx_redir_pdu_t::create_composition_surface_pdu_t*>(rdpgfx_redir_pdu.data());
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_CREATE_COMPOSITION_SURFACE_PDU\n",
                (uint32_t)(create_composition_surface_pdu->window_id() >> 32), (uint32_t)(create_composition_surface_pdu->window_id() >> 0));
            break;

        default:
            printf("(unknown Microsoft::Windows::RDS::RemoteAppGraphicsRedirection type %d)\n", rdpgfx_redir_pdu.cmd_id());
            break;
    }
}

static void dump_wdag_dvc_pdu(wdag_dvc_pdu_t &wdag_dvc_pdu)
{
    wdag_dvc_pdu_t::wdag_msg_validate_url_t* validate_url = nullptr;
    switch(wdag_dvc_pdu.command())
    {
        case wdag_dvc_pdu_t::WDAG_COMMAND_OPEN_DOWNLOADS_FOLDER:
            printf("WDAG_REQ_OPEN_DOWNLOADS_FOLDER\n");
            break;

        case wdag_dvc_pdu_t::WDAG_COMMAND_VALIDATE_URL:
            validate_url = reinterpret_cast<wdag_dvc_pdu_t::wdag_msg_validate_url_t*>(wdag_dvc_pdu.msg());
            printf("WDAG_REQ_VALIDATE_URL: %s://%S\n", validate_url->use_http() ? "http" : "https", (const wchar_t*)(validate_url->url().c_str()));
            break;

        case wdag_dvc_pdu_t::WDAG_COMMAND_OPEN_BROWSER_ON_HOST:
            printf("WDAG_REQ_OPEN_BROWSER_ON_HOST\n");
            break;

        default:
            printf("WDAG_RSP result 0x%08x\n", wdag_dvc_pdu.result());
            break;
    }
}

void dump_audio_format(audio_format_t &audio_format)
{
    printf("  AUDIO FORMAT format_tag 0x%04x, channels %d, samples_per_sec %d, avg_bytes_per_sec %d, block_align %d, bits_per_sample %d, size %d\n",
        audio_format.format_tag(),
        audio_format.channels(),
        audio_format.samples_per_sec(),
        audio_format.avg_bytes_per_sec(),
        audio_format.block_align(),
        audio_format.bits_per_sample(),
        audio_format.size());
    //hexdump(audio_format.data().c_str(), audio_format.data().size());
}

void dump_rdpsnd_version_and_formats(rdpsnd_pdu_t::snd_version_and_formats_t &snd_version_and_formats)
{
    printf("RDPSND FORMATS flags 0x%08x, volume 0x%08x, pitch 0x%08x, d_gram_port 0x%04x, last_block_confirmed %d, version 0x%02x, pad 0x%02x, number_of_formats %d\n",
        snd_version_and_formats.flags(),
        snd_version_and_formats.volume(),
        snd_version_and_formats.pitch(),
        snd_version_and_formats.d_gram_port(),
        snd_version_and_formats.last_block_confirmed(),
        snd_version_and_formats.version(),
        snd_version_and_formats.pad(),
        snd_version_and_formats.number_of_formats());
    for ( auto _audio_format : *(snd_version_and_formats.snd_formats()) )
    {
        auto audio_format = *_audio_format;
        dump_audio_format(audio_format);
    }
}

void dump_rdpsnd_sndwave2(rdpsnd_pdu_t::sndwave2_t &sndwave2)
{
    printf("RDPSND WAVE2 time_stamp 0x%04x, format_no 0x%04x, block_no %d, pad 0x%02x 0x%02x 0x%02x, audio_time_stamp 0x%08x, data %d bytes\n",
        sndwave2.time_stamp(),
        sndwave2.format_no(),
        sndwave2.block_no(),
        (uint8_t)(sndwave2.pad()[0]), (uint8_t)(sndwave2.pad()[1]), (uint8_t)(sndwave2.pad()[2]),
        sndwave2.audio_time_stamp(),
        (uint32_t)sndwave2.data().size());
}

void dump_rdpsnd_pdu(rdpsnd_pdu_t &rdpsnd_pdu)
{
    switch(rdpsnd_pdu.msg_type())
    {
        case rdpsnd_pdu_t::SNDC_WAVE_DATA:
            printf("RDPSND WAVE_DATA\n");
            break;

        case rdpsnd_pdu_t::SNDC_CLOSE:
            printf("RDPSND CLOSE\n");
            break;

        case rdpsnd_pdu_t::SNDC_WAVE:
            printf("RDPSND WAVE\n");
            break;

        case rdpsnd_pdu_t::SNDC_SETVOLUME:
            printf("RDPSND SETVOLUME level %d\n", 
                reinterpret_cast<rdpsnd_pdu_t::sndvol_t*>(rdpsnd_pdu.body())->volume());
            break;

        case rdpsnd_pdu_t::SNDC_SETPITCH:
            printf("RDPSND SETPITCH pitch %d\n", 
                reinterpret_cast<rdpsnd_pdu_t::sndpitch_t*>(rdpsnd_pdu.body())->pitch());
            break;

        case rdpsnd_pdu_t::SNDC_WAVECONFIRM:
            printf("RDPSND WAVECONFIRM timestamp 0x%04x, confirm_block_no=%d, pad=0x%02x\n",
                reinterpret_cast<rdpsnd_pdu_t::sndwav_confirm_t*>(rdpsnd_pdu.body())->time_stamp(),
                reinterpret_cast<rdpsnd_pdu_t::sndwav_confirm_t*>(rdpsnd_pdu.body())->confirm_block_no(),
                reinterpret_cast<rdpsnd_pdu_t::sndwav_confirm_t*>(rdpsnd_pdu.body())->pad());
            break;

        case rdpsnd_pdu_t::SNDC_TRAINING:
            printf("RDPSND TRAINING timestamp 0x%04x, packet_size=0x%04x, data %d bytes\n",
                reinterpret_cast<rdpsnd_pdu_t::sndtraining_t*>(rdpsnd_pdu.body())->time_stamp(),
                reinterpret_cast<rdpsnd_pdu_t::sndtraining_t*>(rdpsnd_pdu.body())->packet_size(),
                (uint32_t)(reinterpret_cast<rdpsnd_pdu_t::sndtraining_t*>(rdpsnd_pdu.body())->data().size()));
            //hexdump(reinterpret_cast<rdpsnd_pdu_t::sndtraining_t*>(rdpsnd_pdu.body())->data().c_str(),
            //    reinterpret_cast<rdpsnd_pdu_t::sndtraining_t*>(rdpsnd_pdu.body())->data().size());
            break;

        case rdpsnd_pdu_t::SNDC_FORMATS:
            dump_rdpsnd_version_and_formats(*reinterpret_cast<rdpsnd_pdu_t::snd_version_and_formats_t*>(rdpsnd_pdu.body()));
            break;

        case rdpsnd_pdu_t::SNDC_CRYPTKEY:
            printf("RDPSND CRYPTKEY\n");
            break;

        case rdpsnd_pdu_t::SNDC_WAVEENCRYPT:
            printf("RDPSND WAVEENCRYPT\n");
            break;

        case rdpsnd_pdu_t::SNDC_UDPWAVE:
            printf("RDPSND UDPWAVE\n");
            break;

        case rdpsnd_pdu_t::SNDC_UDPWAVELAST:
            printf("RDPSND UDPWAVELAST\n");
            break;

        case rdpsnd_pdu_t::SNDC_QUALITYMODE:
            printf("RDPSND QUALITYMODE quality_mode 0x%04x, reserved 0x%04x\n",
                reinterpret_cast<rdpsnd_pdu_t::sndqualitymode_t*>(rdpsnd_pdu.body())->quality_mode(),
                reinterpret_cast<rdpsnd_pdu_t::sndqualitymode_t*>(rdpsnd_pdu.body())->reserved());
            break;

        case rdpsnd_pdu_t::SNDC_WAVE2:
            dump_rdpsnd_sndwave2(*reinterpret_cast<rdpsnd_pdu_t::sndwave2_t*>(rdpsnd_pdu.body()));
            break;

        default:
            printf("(unknown RDPSND message type %d)\n", rdpsnd_pdu.msg_type());
            break;
    }
}

void dump_sndin_formats(sndin_pdu_t::sndin_formats_t &sndin_formats)
{
    printf("SNDIN FORMATS num_formats %d, size_formats_packet 0x%08x\n",
        sndin_formats.num_formats(),
        sndin_formats.size_formats_packet());
    for ( auto _audio_format : *(sndin_formats.sound_formats()) )
    {
        auto audio_format = *_audio_format;
        dump_audio_format(audio_format);
    }
    hexdump(
         sndin_formats.extra_data().c_str(),
         sndin_formats.extra_data().size());
}

void dump_sndin_open(sndin_pdu_t::sndin_open_t &sndin_open)
{
    printf("SNDIN OPEN frames_per_packet %d, initial_format %d, format_tag 0x%04x, channels %d, samples_per_sec %d, avg_bytes_per_sec %d, block_align %d, bits_per_sample %d\n",
        sndin_open.frames_per_packet(),
        sndin_open.initial_format(),
        sndin_open.format_tag(),
        sndin_open.channels(),
        sndin_open.samples_per_sec(),
        sndin_open.avg_bytes_per_sec(),
        sndin_open.block_align(),
        sndin_open.bits_per_sample());
    hexdump(sndin_open.extra_format_data().c_str(), sndin_open.extra_format_data().size());
}

static void dump_sndin_pdu(sndin_pdu_t &sndin_pdu)
{
    switch(sndin_pdu.message_id())
    {
        case sndin_pdu_t::MSG_SNDIN_VERSION:
            printf("SNDIN VERSION version %d\n", 
                reinterpret_cast<sndin_pdu_t::sndin_version_t*>(sndin_pdu.message())->version());
            break;

        case sndin_pdu_t::MSG_SNDIN_FORMATS:
            dump_sndin_formats(*reinterpret_cast<sndin_pdu_t::sndin_formats_t*>(sndin_pdu.message()));
            break;

        case sndin_pdu_t::MSG_SNDIN_OPEN:
            dump_sndin_open(*reinterpret_cast<sndin_pdu_t::sndin_open_t*>(sndin_pdu.message()));
            break;

        case sndin_pdu_t::MSG_SNDIN_OPEN_REPLY:
            printf("SNDIN OPEN REPLY result 0x%08x\n", 
                reinterpret_cast<sndin_pdu_t::sndin_open_reply_t*>(sndin_pdu.message())->result());
            break;

        case sndin_pdu_t::MSG_SNDIN_DATA_INCOMING:
            printf("SNDIN INCOMING\n");
            break;

        case sndin_pdu_t::MSG_SNDIN_DATA:
            printf("SNDIN DATA size %d bytes\n",
                (uint32_t)(reinterpret_cast<sndin_pdu_t::sndin_data_t*>(sndin_pdu.message())->data().size()));
            break;

        case sndin_pdu_t::MSG_SNDIN_FORMATCHANGE:
            printf("SNDIN FORMATCHANGE new_format %d\n",
                reinterpret_cast<sndin_pdu_t::sndin_formatchange_t*>(sndin_pdu.message())->new_format());
            break;

        default:
            printf("(unknown SNDIN message type %d)\n", sndin_pdu.message_id());
            break;
    }
}

static void dump_dynvc_data(dynvc_pdu_t::dynvc_data_t &dynvc_data, uint32_t channel_id)
{
    switch(dynvc_data.channel_type())
    {
        case dynvc_pdu_t::CHANNEL_TYPE_RAW:
            if ( reinterpret_cast<rdp_raw_pdu_t*>(dynvc_data.data())->data().size() > 0 )
            {
                printf("DYNVC CHANNEL %d (RAW) DATA (%s):\n", 
                    channel_id, rdp_decode_direction_str(dynvc_data.to_server()));
                hexdump(
                    reinterpret_cast<rdp_raw_pdu_t*>(dynvc_data.data())->data().c_str(),
                    reinterpret_cast<rdp_raw_pdu_t*>(dynvc_data.data())->data().size()
                );
            }
            break;

        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_REMOTE_APP_GRAPHICS_REDIRECTION:
            dump_rdpgfx_redir_pdu(*reinterpret_cast<rdpgfx_redir_pdu_t*>(dynvc_data.data()));
            break;

        case dynvc_pdu_t::CHANNEL_TYPE_WDAG_DVC:
            dump_wdag_dvc_pdu(*reinterpret_cast<wdag_dvc_pdu_t*>(dynvc_data.data()));
            break;

        case dynvc_pdu_t::CHANNEL_TYPE_ECHO:
            printf("ECHO (%s):\n", rdp_decode_direction_str(dynvc_data.to_server()));
            hexdump(
                reinterpret_cast<echo_pdu_t*>(dynvc_data.data())->message().c_str(),
                reinterpret_cast<echo_pdu_t*>(dynvc_data.data())->message().size()
            );
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_INPUT:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Input");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GRAPHICS:
            dump_rdpgfx_pdu(*reinterpret_cast<rdpgfx_pdu_t*>(dynvc_data.data()));
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_AUTH_REDIRECTION:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::AuthRedirection");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_TELEMETRY:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Telemetry");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_LOCATION:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Location");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GEOMETRY:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Geometry::v08.01");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_DISPLAY_CONTROL:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::DisplayControl");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_CONTROL_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Video::Control::v08.01");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_DATA_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Video::Data::v08.01");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_FRAME_BUFFER_CONTROL_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Frame_Buffer::Control::v08.01");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_AUDIO_PLAYBACK_DVC:
            dump_rdpsnd_pdu(*reinterpret_cast<rdpsnd_pdu_t*>(dynvc_data.data()));
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_AUDIO_PLAYBACK_LOSSY_DVC:
            dump_rdpsnd_pdu(*(reinterpret_cast<rdpsnd_lossy_pdu_t*>(dynvc_data.data())->data()));
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_AUDIO_INPUT:
            dump_sndin_pdu(*reinterpret_cast<sndin_pdu_t*>(dynvc_data.data()));
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_XPSRD:
            printf("TODO: dump channel data '%s'\n", "XPSRD");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_TSVCTKT:
            printf("TODO: dump channel data '%s'\n", "TSVCTKT");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_TSMF:
            printf("TODO: dump channel data '%s'\n", "TSMF");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_PNPDR:
            printf("TODO: dump channel data '%s'\n", "PNPDR");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_FILE_REDIRECTOR_CHANNEL:
            printf("TODO: dump channel data '%s'\n", "FileRedirectorChannel");
            break;

        case dynvc_pdu_t::CHANNEL_TYPE_RDCAMERA_DEVICE_ENUMERATOR:
            printf("TODO: dump channel data '%s'\n", "RDCamera_Device_Enumerator");
            break;
 
        case dynvc_pdu_t::CHANNEL_TYPE_RDCAMERA_DEVICE:
            printf("TODO: dump channel data '%s'\n", "RDCamera_Device");
            break;

        default:
            printf("ERROR: unimplemented dynvc channel type %d on channel id %d)\n",
                dynvc_data.channel_type(), channel_id);
            break;
    }
}

static void dump_rdpdr_pdu(rdpdr_pdu_t &rdpdr_pdu)
{
    rdpdr_pdu_t::dr_core_client_name_req_t *core_client_name_req = nullptr;
    switch(rdpdr_pdu.packet_id())
    {
        case rdpdr_pdu_t::PAKID_CORE_CLIENTID_CONFIRM:
            printf("RDPDR CORE_CLIENTID_CONFIRM version %d.%d, client_id 0x%08x\n",
                reinterpret_cast<rdpdr_pdu_t::dr_core_clientid_confirm_t*>(rdpdr_pdu.data())->version_major(),
                reinterpret_cast<rdpdr_pdu_t::dr_core_clientid_confirm_t*>(rdpdr_pdu.data())->version_minor(),
                reinterpret_cast<rdpdr_pdu_t::dr_core_clientid_confirm_t*>(rdpdr_pdu.data())->client_id());
            break;

        case rdpdr_pdu_t::PAKID_CORE_CLIENT_NAME:
            core_client_name_req = reinterpret_cast<rdpdr_pdu_t::dr_core_client_name_req_t*>(rdpdr_pdu.data());
            if ( core_client_name_req->unicode() )
            {
                printf("RDPDR CORE_CLIENT_NAME unicode_flags 0x%08x, code_page 0x%08x, computer_name_w '%S'\n",
                    core_client_name_req->unicode_flag(),
                    core_client_name_req->code_page(),
                    (const wchar_t*)(core_client_name_req->computer_name_w().c_str()));
            }
            else
            {
                printf("RDPDR CORE_CLIENT_NAME unicode_flags 0x%08x, code_page 0x%08x, computer_name_a '%s'\n",
                    core_client_name_req->unicode_flag(),
                    core_client_name_req->code_page(),
                    core_client_name_req->computer_name_a().c_str());
            }
            break;

        case rdpdr_pdu_t::PAKID_CORE_CLIENT_CAPABILITY:
            printf("RDPDR CORE_CLIENT_CAPABILITY\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_DEVICELIST_ANNOUNCE:
            printf("RDPDR CORE_DEVICELIST_ANNOUNCE\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_DEVICELIST_REMOVE:
            printf("RDPDR CORE_DEVICELIST_REMOVE\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_DEVICE_IOCOMPLETION:
            printf("RDPDR CORE_DEVICE_IOCOMPLETION\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_DEVICE_IOREQUEST:
            printf("RDPDR CORE_DEVICE_IOREQUEST\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_SERVER_ANNOUNCE:
            printf("RDPDR CORE_SERVER_ANNOUNCE version %d.%d, client_id 0x%08x\n",
                reinterpret_cast<rdpdr_pdu_t::dr_core_server_announce_req_t*>(rdpdr_pdu.data())->version_major(),
                reinterpret_cast<rdpdr_pdu_t::dr_core_server_announce_req_t*>(rdpdr_pdu.data())->version_minor(),
                reinterpret_cast<rdpdr_pdu_t::dr_core_server_announce_req_t*>(rdpdr_pdu.data())->client_id());
            break;

        case rdpdr_pdu_t::PAKID_PRN_CACHE_DATA:
            printf("RDPDR PRN_CACHE_DATA\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_SERVER_CAPABILITY:
            printf("RDPDR CORE_SERVER_CAPABILITY\n");
            break;

        case rdpdr_pdu_t::PAKID_PRN_USING_XPS:
            printf("RDPDR PRN_USING_XPS\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_USER_LOGGEDON:
            printf("RDPDR CORE_USER_LOGGEDON:\n");
            break;

        case rdpdr_pdu_t::PAKID_CORE_DEVICE_REPLY:
            printf("RDPDR CORE_DEVICE_REPLY\n");
            break;

        default:
            printf("(rdpdr has unknown packet ID 0x%02x)\n", rdpdr_pdu.packet_id());
            break;
    }
}

static void dump_cliprdr_format_list(cliprdr_pdu_t::cliprdr_format_list_t &format_list)
{
    if ( cliprdr_pdu_t::cliprdr_format_list_t::FORMAT_LIST_SHORT_NAMES == format_list.format_list_type() )
    {
        printf("CLIPRDR FORMAT_LIST (short names)\n");
        auto short_format_name_list = *reinterpret_cast<cliprdr_pdu_t::cliprdr_short_format_names_t*>(format_list.format_list_data())->short_format_names();
        for ( auto _short_format_name : short_format_name_list )
        {
            auto short_format_name = *_short_format_name;
            if ( short_format_name.unicode() )
            {
                printf("  FORMAT %d '%S' (unicode)\n", short_format_name.format_id(), (const wchar_t*)(short_format_name.format_unicode_name().c_str()));
            }
            else
            {
                printf("  FORMAT %d '%s' (ascii)\n", short_format_name.format_id(), short_format_name.format_ascii_name().c_str());
            }
        }
    }
    else if ( cliprdr_pdu_t::cliprdr_format_list_t::FORMAT_LIST_LONG_NAMES == format_list.format_list_type() )
    {
        printf("CLIPRDR FORMAT_LIST (long names)\n");
        auto long_format_name_list = *reinterpret_cast<cliprdr_pdu_t::cliprdr_long_format_names_t*>(format_list.format_list_data())->long_format_names();
        for ( auto _long_format_name : long_format_name_list )
        {
            auto long_format_name = *_long_format_name;
            printf("  FORMAT %d '%S' (unicode)\n", long_format_name.format_id(), (const wchar_t*)(long_format_name.format_name().c_str()));
        }
    }
    else if ( cliprdr_pdu_t::cliprdr_format_list_t::FORMAT_LIST_DATA == format_list.format_list_type() )
    {
        printf("CLIPRDR FORMAT_LIST (raw data)\n");
        auto raw_format_names = *reinterpret_cast<cliprdr_pdu_t::cliprdr_raw_format_names_t*>(format_list.format_list_data());
        hexdump(raw_format_names.data().c_str(), raw_format_names.data().size());
    }
    else
    {
        printf("  NOT IMPLEMENTED FORMAT LIST TYPE %d\n", format_list.format_list_type());
    }
}

const char *cliprdr_result(uint16_t msg_flags)
{
    if ( (msg_flags & 0x0002) != 0)
    {
        return "CB_RESPONSE_FAIL";
    }
    if ( (msg_flags & 0x0001) != 0)
    {
        return "CB_RESPONSE_OK";
    }
    return "unknown result";
}

static void dump_cliprdr_pdu(cliprdr_pdu_t &cliprdr_pdu)
{
    switch(cliprdr_pdu.msg_type())
    {
        case cliprdr_pdu_t::CB_MONITOR_READY:
            printf("CLIPRDR MONITOR_READY\n");
            break;

        case cliprdr_pdu_t::CB_FORMAT_LIST:
            dump_cliprdr_format_list(*reinterpret_cast<cliprdr_pdu_t::cliprdr_format_list_t*>(cliprdr_pdu.data()));
            break;

        case cliprdr_pdu_t::CB_FORMAT_LIST_RESPONSE:
            printf("CLIPRDR FORMAT_LIST_RESPONSE (%s)\n", cliprdr_result(cliprdr_pdu.msg_flags()));
            break;

        case cliprdr_pdu_t::CB_FORMAT_DATA_REQUEST:
            printf("CLIPRDR FORMAT_DATA_REQUEST\n");
            break;

        case cliprdr_pdu_t::CB_FORMAT_DATA_RESPONSE:
            printf("CLIPRDR FORMAT_DATA_RESPONSE (%s)\n", cliprdr_result(cliprdr_pdu.msg_flags()));
            hexdump(
                reinterpret_cast<cliprdr_pdu_t::cliprdr_format_data_response_t*>(cliprdr_pdu.data())->requested_format_data().c_str(),
                reinterpret_cast<cliprdr_pdu_t::cliprdr_format_data_response_t*>(cliprdr_pdu.data())->requested_format_data().size());
            break;

        case cliprdr_pdu_t::CB_TEMP_DIRECTORY:
            printf("CLIPRDR TEMP_DIRECTORY\n");
            break;

        case cliprdr_pdu_t::CB_CLIP_CAPS:
            printf("CLIPRDR CLIP_CAPS\n");
            break;

        case cliprdr_pdu_t::CB_FILECONTENTS_REQUEST:
            printf("CLIPRDR FILECONTENTS_REQUEST\n");
            break;

        case cliprdr_pdu_t::CB_FILECONTENTS_RESPONSE:
            printf("CLIPRDR FILECONTENTS_RESPONSE (%s) stream_id %d\n",
                cliprdr_result(cliprdr_pdu.msg_flags()),
                reinterpret_cast<cliprdr_pdu_t::cliprdr_filecontents_response_t*>(cliprdr_pdu.data())->stream_id());
            hexdump(
                reinterpret_cast<cliprdr_pdu_t::cliprdr_filecontents_response_t*>(cliprdr_pdu.data())->requested_file_contents_data().c_str(),
                reinterpret_cast<cliprdr_pdu_t::cliprdr_filecontents_response_t*>(cliprdr_pdu.data())->requested_file_contents_data().size());
            break;

        case cliprdr_pdu_t::CB_LOCK_CLIPDATA:
            printf("CLIPRDR LOCK_CLIPDATA clip_data_id %d\n", 
                reinterpret_cast<cliprdr_pdu_t::cliprdr_lock_clipdata_t*>(cliprdr_pdu.data())->clip_data_id());
            break;

        case cliprdr_pdu_t::CB_UNLOCK_CLIPDATA:
            printf("CLIPRDR UNLOCK_CLIPDATA clip_data_id %d\n", 
                reinterpret_cast<cliprdr_pdu_t::cliprdr_unlock_clipdata_t*>(cliprdr_pdu.data())->clip_data_id());
            break;

        default:
            printf("(cliprdr has unknown cmd 0x%02x)\n", cliprdr_pdu.msg_type());
            break;
    }
}

static void dump_dynvc_pdu(dynvc_pdu_t &dynvc_pdu)
{
    uint32_t channel_id = dynvc_pdu.channel_id();
    switch(dynvc_pdu.cmd_type())
    {
        case dynvc_pdu_t::DYNVC_CMD_TYPE_CREATE_REQ:
            printf("DYNVC_CMD_CREATE_REQ: channel %d '%s'\n", channel_id,
                reinterpret_cast<dynvc_pdu_t::dynvc_create_req_t*>(dynvc_pdu.data())->channel_name().c_str());
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_CREATE_RSP:
            printf("DYNVC_CMD_CREATE_RSP: channel %d status=0x%08x\n", channel_id,
                reinterpret_cast<dynvc_pdu_t::dynvc_create_rsp_t*>(dynvc_pdu.data())->channel_status());
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_DATA_FIRST:
            dump_dynvc_data(*(reinterpret_cast<dynvc_pdu_t::dynvc_data_first_t*>(dynvc_pdu.data())->data()), channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_DATA:
            dump_dynvc_data(*reinterpret_cast<dynvc_pdu_t::dynvc_data_t*>(dynvc_pdu.data()), channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_CLOSE_REQ:
            printf("DYNVC_CMD_CLOSE_REQ: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_CLOSE_RSP:
            printf("DYNVC_CMD_CLOSE_RSP: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_CAPABILITY_REQ:
            printf("DYNVC_CMD_CAPABILITY_REQ: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_CAPABILITY_RSP:
            printf("DYNVC_CMD_CAPABILITY_RSP: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_DATA_FIRST_COMPRESSED:
            printf("DYNVC_CMD_DATA_FIRST_COMPRESSED: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_DATA_COMPRESSED:
            printf("DYNVC_CMD_DATA_COMPRESSED: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_SOFT_SYNC_REQ:
            printf("DYNVC_CMD_SOFT_SYNC_REQ: channel %d\n", channel_id);
            break;

        case dynvc_pdu_t::DYNVC_CMD_TYPE_SOFT_SYNC_RSP:
            printf("DYNVC_CMD_SOFT_SYNC_RSP: channel %d\n", channel_id);
            break;

        default:
            printf("(dynvc request on channel %d has unknown cmd 0x%02x)\n",
                channel_id, dynvc_pdu.cmd());
            break;
    }
}

static void dump_ts_rail_pdu(ts_rail_pdu_t &ts_rail_pdu)
{
    ts_rail_pdu_t::ts_rail_order_exec_t *ts_rail_order_exec = nullptr;
    ts_rail_pdu_t::ts_rail_order_activate_t *ts_rail_order_activate = nullptr;
    ts_rail_pdu_t::ts_rail_order_sysparam_t *ts_rail_order_sysparam = nullptr;
    ts_rail_pdu_t::ts_rail_order_syscommand_t *ts_rail_order_syscommand = nullptr;
    ts_rail_pdu_t::ts_rail_order_handshake_t *ts_rail_order_handshake = nullptr;
    ts_rail_pdu_t::ts_rail_order_notify_event_t *ts_rail_order_notify_event = nullptr;
    ts_rail_pdu_t::ts_rail_order_windowmove_t *ts_rail_order_windowmove = nullptr;
    ts_rail_pdu_t::ts_rail_order_localmovesize_t *ts_rail_order_localmovesize = nullptr;
    ts_rail_pdu_t::ts_rail_order_minmaxinfo_t *ts_rail_order_minmaxinfo = nullptr;
    ts_rail_pdu_t::ts_rail_order_clientstatus_t *ts_rail_order_clientstatus = nullptr;
    ts_rail_pdu_t::ts_rail_order_sysmenu_t *ts_rail_order_sysmenu = nullptr;
    ts_rail_pdu_t::ts_rail_order_langbarinfo_t *ts_rail_order_langbarinfo = nullptr;
    ts_rail_pdu_t::ts_rail_order_get_appid_req_t *ts_rail_order_get_appid_req = nullptr;
    ts_rail_pdu_t::ts_rail_order_get_appid_resp_t *ts_rail_order_get_appid_resp = nullptr;
    ts_rail_pdu_t::ts_rail_order_taskbarinfo_t *ts_rail_order_taskbarinfo = nullptr;
    ts_rail_pdu_t::ts_rail_order_languageimeinfo_t *ts_rail_order_languageimeinfo = nullptr;
    ts_rail_pdu_t::ts_rail_order_compartmentinfo_t *ts_rail_order_compartmentinfo = nullptr;
    ts_rail_pdu_t::ts_rail_order_handshake_ex_t *ts_rail_order_handshake_ex = nullptr;
    ts_rail_pdu_t::ts_rail_order_zorder_sync_t *ts_rail_order_zorder_sync = nullptr;
    ts_rail_pdu_t::ts_rail_order_cloak_t *ts_rail_order_cloak = nullptr;
    ts_rail_pdu_t::ts_rail_order_power_display_request_t *ts_rail_order_power_display_request = nullptr;
    ts_rail_pdu_t::ts_rail_order_snap_arrange_t *ts_rail_order_snap_arrange = nullptr;
    ts_rail_pdu_t::ts_rail_order_get_appid_resp_ex_t *ts_rail_order_get_appid_resp_ex = nullptr;
    ts_rail_pdu_t::ts_rail_order_exec_result_t *ts_rail_order_exec_result = nullptr;
    switch(ts_rail_pdu.order_type())
    {
        case ts_rail_pdu_t::TS_RAIL_ORDER_EXEC:
            ts_rail_order_exec = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_exec_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_EXEC\n");
            printf(" exe='%S'\n", (const wchar_t*)(ts_rail_order_exec->exe_or_file().c_str()));
            printf(" args='%S'\n", (const wchar_t*)(ts_rail_order_exec->arguments().c_str()));
            printf(" working_dir='%S'\n", (const wchar_t*)(ts_rail_order_exec->working_dir().c_str()));
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_ACTIVATE:
            ts_rail_order_activate = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_activate_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_ACTIVATE\n", ts_rail_order_activate->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_SYSPARAM:
            ts_rail_order_sysparam = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_sysparam_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_SYSPARAM\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_SYSCOMMAND:
            ts_rail_order_syscommand = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_syscommand_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SYSCOMMAND\n", ts_rail_order_syscommand->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_HANDSHAKE:
            ts_rail_order_handshake = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_handshake_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_HANDSHAKE\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_NOTIFY_EVENT:
            ts_rail_order_notify_event = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_notify_event_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_NOTIFY_EVENT\n", ts_rail_order_notify_event->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_WINDOWMOVE:
            ts_rail_order_windowmove = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_windowmove_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_WINDOWMOVE\n", ts_rail_order_windowmove->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_LOCALMOVESIZE:
            ts_rail_order_localmovesize = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_localmovesize_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_LOCALMOVESIZE\n", ts_rail_order_localmovesize->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_MINMAXINFO:
            ts_rail_order_minmaxinfo = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_minmaxinfo_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_MINMAXINFO\n", ts_rail_order_minmaxinfo->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_CLIENTSTATUS:
            ts_rail_order_clientstatus = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_clientstatus_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_CLIENTSTATUS\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_SYSMENU:
            ts_rail_order_sysmenu = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_sysmenu_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SYSMENU\n", ts_rail_order_sysmenu->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_LANGBARINFO:
            ts_rail_order_langbarinfo = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_langbarinfo_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_LANGBARINFO\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_GET_APPID_REQ:
            ts_rail_order_get_appid_req = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_get_appid_req_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_REQ\n", ts_rail_order_get_appid_req->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_GET_APPID_RESP:
            ts_rail_order_get_appid_resp = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_get_appid_resp_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_RESP\n", ts_rail_order_get_appid_resp->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_TASKBARINFO:
            ts_rail_order_taskbarinfo = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_taskbarinfo_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_TASKBARINFO\n", ts_rail_order_taskbarinfo->window_id_tab());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_LANGUAGEIMEINFO:
            ts_rail_order_languageimeinfo = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_languageimeinfo_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_LANGUAGEIMEINFO\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_COMPARTMENTINFO:
            ts_rail_order_compartmentinfo = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_compartmentinfo_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_COMPARTMENTINFO\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_HANDSHAKE_EX:
            ts_rail_order_handshake_ex = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_handshake_ex_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_HANDSHAKE_EX\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_ZORDER_SYNC:
            ts_rail_order_zorder_sync = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_zorder_sync_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_ZORDER_SYNC\n", ts_rail_order_zorder_sync->window_id_marker());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_CLOAK:
            ts_rail_order_cloak = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_cloak_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_CLOAK\n", ts_rail_order_cloak->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_POWER_DISPLAY_REQUEST:
            ts_rail_order_power_display_request = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_power_display_request_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_POWER_DISPLAY_REQUEST\n");
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_SNAP_ARRANGE:
            ts_rail_order_snap_arrange = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_snap_arrange_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SNAP_ARRANGE\n", ts_rail_order_snap_arrange->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_GET_APPID_RESP_EX:
            ts_rail_order_get_appid_resp_ex = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_get_appid_resp_ex_t*>(ts_rail_pdu.order());
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_RESP_EX\n", ts_rail_order_get_appid_resp_ex->window_id());
            break;

        case ts_rail_pdu_t::TS_RAIL_ORDER_EXEC_RESULT:
            ts_rail_order_exec_result = reinterpret_cast<ts_rail_pdu_t::ts_rail_order_exec_result_t*>(ts_rail_pdu.order());
            printf("TS_RAIL_ORDER_EXEC_RESULT\n");
            printf(" exe='%S'\n", (const wchar_t*)(ts_rail_order_exec_result->exe_or_file().c_str()));
            printf(" result=0x%08x\n", (uint16_t)ts_rail_order_exec_result->exec_result());
            printf(" returncode=0x%08x\n", ts_rail_order_exec_result->raw_result());
            break;

        default:
            printf("(don't know how to handle rail order 0x%04x)\n", (uint16_t)ts_rail_pdu.order_type());
            break;
    }
}

static void dump_static_channel(channel_pdu_t &channel_pdu)
{
    // data
    channel_pdu_t::channel_data_t &channel_data = *(channel_pdu.channel_data());
    switch( channel_pdu.channel_type())
    {
        case channel_pdu_t::CHANNEL_TYPE_RAW:
            if ( reinterpret_cast<rdp_raw_pdu_t*>(channel_data.data())->data().size() > 0 )
            {
                printf("STATIC CHANNEL (%s) DATA (%s):\n",
                    rdp_decode_fragment_compressed_str(channel_pdu.is_fragment(), channel_pdu.is_compressed()),
                    rdp_decode_direction_str(channel_pdu.to_server()));
                hexdump(
                    reinterpret_cast<rdp_raw_pdu_t*>(channel_data.data())->data().c_str(),
                    reinterpret_cast<rdp_raw_pdu_t*>(channel_data.data())->data().size()
                );
            }
            break;

        case channel_pdu_t::CHANNEL_TYPE_RDPDR:
            dump_rdpdr_pdu(*reinterpret_cast<rdpdr_pdu_t*>(channel_data.data()));
            break;

        case channel_pdu_t::CHANNEL_TYPE_CLIPRDR:
            dump_cliprdr_pdu(*reinterpret_cast<cliprdr_pdu_t*>(channel_data.data()));
            break;

        case channel_pdu_t::CHANNEL_TYPE_DRDYNVC:
            dump_dynvc_pdu(*reinterpret_cast<dynvc_pdu_t*>(channel_data.data()));
            break;

        case channel_pdu_t::CHANNEL_TYPE_RAIL:
            dump_ts_rail_pdu(*reinterpret_cast<ts_rail_pdu_t*>(channel_data.data()));
            break;

        case channel_pdu_t::CHANNEL_TYPE_RAIL_RI:
            dump_ts_window_order(*(reinterpret_cast<ts_rail_ri_pdu_t*>(channel_data.data())->window_order()));
            break;

        case channel_pdu_t::CHANNEL_TYPE_RAIL_WI:
            dump_ts_window_order(*(reinterpret_cast<ts_rail_wi_pdu_t*>(channel_data.data())->window_order()));
            break;

        default:
            printf("ERROR: unimplemented static channel type %d\n", channel_pdu.channel_type());
            break;
    }
}

static void dump_t125_mcs_send_data_request(t125_mcs_pdu_t::t125_mcs_send_data_request_t &t125_mcs_send_data_request)
{
    if ( t125_mcs_send_data_request.channel_type() == channel_pdu_t::CHANNEL_TYPE_SECURITY_LAYER )
    {
        dump_security_layer_pdu(*reinterpret_cast<ts_security_pdu_t*>(t125_mcs_send_data_request.user_data()));
    }
    else if ( t125_mcs_send_data_request.channel_type() == channel_pdu_t::CHANNEL_TYPE_SLOWPATH )
    {
        dump_slowpath_pdu(*reinterpret_cast<slowpath_pdu_t*>(t125_mcs_send_data_request.user_data()));
    }
    else
    {
        dump_static_channel(*reinterpret_cast<channel_pdu_t*>(t125_mcs_send_data_request.user_data()));
    }
}

static void dump_t125_mcs_send_data_indication(t125_mcs_pdu_t::t125_mcs_send_data_indication_t &t125_mcs_send_data_indication)
{
    if ( t125_mcs_send_data_indication.channel_type() == channel_pdu_t::CHANNEL_TYPE_SECURITY_LAYER )
    {
        dump_security_layer_pdu(*reinterpret_cast<ts_security_pdu_t*>(t125_mcs_send_data_indication.user_data()));
    }
    else if ( t125_mcs_send_data_indication.channel_type() == channel_pdu_t::CHANNEL_TYPE_SLOWPATH )
    {
        dump_slowpath_pdu(*reinterpret_cast<slowpath_pdu_t*>(t125_mcs_send_data_indication.user_data()));
    }
    else
    {
        dump_static_channel(*reinterpret_cast<channel_pdu_t*>(t125_mcs_send_data_indication.user_data()));
    }
}

static void dump_t125_mcs_pdu(t125_mcs_pdu_t &t125_mcs_pdu)
{
    switch(t125_mcs_pdu.msg())
    {
        case t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_REQUEST:
            dump_t125_mcs_send_data_request(*reinterpret_cast<t125_mcs_pdu_t::t125_mcs_send_data_request_t*>(t125_mcs_pdu.data()));
            break;

        case t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_INDICATION:
            dump_t125_mcs_send_data_indication(*reinterpret_cast<t125_mcs_pdu_t::t125_mcs_send_data_indication_t*>(t125_mcs_pdu.data()));
            break;

        default:
            printf("(unhandled T125 MCS PDU of type %d)\n", t125_mcs_pdu.msg());
            break;
    }
}

static void dump_x224_tpdu(x224_tpdu_t &x224_tpdu)
{
    switch(x224_tpdu.code())
    {
        case x224_tpdu_t::TPDU_CODE_EXPEDITED_DATA:
            printf("X224_TPDU_CODE_EXPEDITED_DATA\n");
            break;

        case x224_tpdu_t::TPDU_CODE_EXPEDITED_DATA_ACK:
            printf("X224_TPDU_CODE_EXPEDITED_DATA_ACK\n");
            break;

        case x224_tpdu_t::TPDU_CODE_REJECT:
            printf("X224_TPDU_CODE_REJECT\n");
            break;

        case x224_tpdu_t::TPDU_CODE_DATA_ACK:
            printf("X224_TPDU_CODE_DATA_ACK\n");
            break;

        case x224_tpdu_t::TPDU_CODE_TPDU_ERROR:
            printf("X224_TPDU_CODE_TPDU_ERROR\n");
            break;

        case x224_tpdu_t::TPDU_CODE_DISCONNECT_REQUEST:
            printf("X224_TPDU_CODE_DISCONNECT_REQUEST\n");
            break;

        case x224_tpdu_t::TPDU_CODE_DISCONNECT_CONFIRM:
            printf("X224_TPDU_CODE_DISCONNECT_CONFIRM\n");
            break;

        case x224_tpdu_t::TPDU_CODE_CONNECTION_CONFIRM:
            printf("X224_TPDU_CODE_CONNECTION_CONFIRM\n");
            break;

        case x224_tpdu_t::TPDU_CODE_CONNECTION_REQUEST:
            printf("X224_TPDU_CODE_CONNECTION_REQUEST\n");
            break;

        case x224_tpdu_t::TPDU_CODE_DATA:
            dump_t125_mcs_pdu(*(reinterpret_cast<x224_tpdu_t::tpdu_data_t*>(x224_tpdu.data())->data()));
            break;

        default:
            printf("(unknown X224 TPDU code)\n");
            break;
    }
}

static void dump_t123_tpkt(t123_tpkt_t &t123_tpkt)
{
    dump_x224_tpdu(*t123_tpkt.data());
}

static void dump_rdp(rdp_t &rdp)
{
    switch (rdp.pdu_type())
    {
        case rdp_t::PDU_TYPE_FASTPATH_INPUT:
            dump_ts_fp_input_pdu(*reinterpret_cast<ts_fp_input_pdu_t*>(rdp.data()));
            break;
        case rdp_t::PDU_TYPE_FASTPATH_UPDATE:
            dump_ts_fp_update_pdu(*reinterpret_cast<ts_fp_update_pdu_t*>(rdp.data()));
            break;
        case rdp_t::PDU_TYPE_X224:
            dump_t123_tpkt(*reinterpret_cast<t123_tpkt_t*>(rdp.data()));
            break;
        default:
            printf("(unknown RDP PDU type)\n");
            break;
    }
}

void rdp_decode_data(const void *data, size_t data_size, bool to_server)
{
    const void *remaining_data = data;
    size_t remaining_data_size = data_size;
    while ( 1 )
    {
        data = remaining_data;
        data_size = rdp_decode_data_size(remaining_data, remaining_data_size);
        if ( 0 == data_size )
        {
            if ( remaining_data_size != 0 )
            {
                printf("RAW REMAINING DATA (%s):\n", rdp_decode_direction_str(to_server));
                hexdump(remaining_data, remaining_data_size);
            }
            return;
        }

        remaining_data = (const void*)((const char*)remaining_data + data_size);
        remaining_data_size = remaining_data_size - data_size;

        try
        {
            std::string buf((const char*)data, data_size);
            std::istringstream is(buf);
            kaitai::kstream ks(&is);
            rdp_t msg(to_server, &ks);
            dump_rdp(msg);
            size_t leftover = (size_t)ks.size() - (size_t)ks.pos();
            if ( 0 != leftover )
            {
                printf("RAW LEFT OVER DATA (%s):\n", rdp_decode_direction_str(to_server));
                hexdump((char*)data + (data_size - leftover), leftover);
            }
        }
        catch(...)
        {
            printf("ERROR PARSING DATA (%s):\n", rdp_decode_direction_str(to_server));
            hexdump(data, data_size);
        }
    }
}

bool rdp_decode_is_heartbeat(const void *data, size_t data_size, bool to_server)
{
    try
    {
        if ( to_server )
        {
            return false;
        }
        std::string buf((const char*)data, data_size);
        std::istringstream is(buf);
        kaitai::kstream ks(&is);
        rdp_t rdp(false, &ks);
        if ( rdp.action() != rdp_t::FASTPATH_ACTION_X224 )
        {
            return false;
        }
        t123_tpkt_t &t123_tpkt = *reinterpret_cast<t123_tpkt_t*>(rdp.data());
        x224_tpdu_t &x224_tpdu = *t123_tpkt.data();
        if ( x224_tpdu.code() != x224_tpdu_t::TPDU_CODE_DATA )
        {
            return false;
        }
        t125_mcs_pdu_t &t125_mcs_pdu = *(reinterpret_cast<x224_tpdu_t::tpdu_data_t*>(x224_tpdu.data())->data());
        uint16_t channel_id = 0;
        const char *channel_data = NULL;
        size_t channel_data_size = 0;
        ts_security_pdu_t *ts_security_pdu = nullptr;
        if ( t125_mcs_pdu.msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_REQUEST )
        {
            t125_mcs_pdu_t::t125_mcs_send_data_request_t &t125_mcs_send_data_request = *reinterpret_cast<t125_mcs_pdu_t::t125_mcs_send_data_request_t*>(t125_mcs_pdu.data());
            if ( t125_mcs_send_data_request.channel_type() == channel_pdu_t::CHANNEL_TYPE_SECURITY_LAYER )
            {
                ts_security_pdu = reinterpret_cast<ts_security_pdu_t*>(t125_mcs_send_data_request.user_data());
            }
        }
        else if ( t125_mcs_pdu.msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_INDICATION )
        {
            t125_mcs_pdu_t::t125_mcs_send_data_indication_t &t125_mcs_send_data_indication = *reinterpret_cast<t125_mcs_pdu_t::t125_mcs_send_data_indication_t*>(t125_mcs_pdu.data());
            if ( t125_mcs_send_data_indication.channel_type() == channel_pdu_t::CHANNEL_TYPE_SECURITY_LAYER )
            {
                ts_security_pdu = reinterpret_cast<ts_security_pdu_t*>(t125_mcs_send_data_indication.user_data());
            }
        }
        if ( !ts_security_pdu )
        {
            return false;
        }
        if( ts_security_pdu->msg() != ts_security_pdu_t::SEC_HEARTBEAT )
        {
            return false;
        }
        return true;
    }
    catch(...)
    {
        return false;
    }
    return false;
}
