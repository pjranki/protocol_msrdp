#include "rdp_process_static_channel_type.h"

channel_pdu_t::channel_type_t rdp_process_static_channel_name_to_channel_type(const char *channel_name)
{
    if ( 0 == strcmp(channel_name, "security_layer") )
    {
        return channel_pdu_t::CHANNEL_TYPE_SECURITY_LAYER;
    }
    if ( 0 == strcmp(channel_name, "slowpath") )
    {
        return channel_pdu_t::CHANNEL_TYPE_SLOWPATH;
    }
    if ( 0 == strcmp(channel_name, "rdpdr") )
    {
        return channel_pdu_t::CHANNEL_TYPE_RDPDR;
    }
    if ( 0 == strcmp(channel_name, "rail") )
    {
        return channel_pdu_t::CHANNEL_TYPE_RAIL;
    }
    if ( 0 == strcmp(channel_name, "rail_wi") )
    {
        return channel_pdu_t::CHANNEL_TYPE_RAIL_WI;
    }
    if ( 0 == strcmp(channel_name, "rail_ri") )
    {
        return channel_pdu_t::CHANNEL_TYPE_RAIL_RI;
    }
    if ( 0 == strcmp(channel_name, "cliprdr") )
    {
        return channel_pdu_t::CHANNEL_TYPE_CLIPRDR;
    }
    if ( 0 == strcmp(channel_name, "drdynvc") )
    {
        return channel_pdu_t::CHANNEL_TYPE_DRDYNVC;
    }
    return channel_pdu_t::CHANNEL_TYPE_RAW;
}

static std::function<uint8_t(bool,uint16_t)> g_lookup_channel_type = nullptr;

void rdp_process_static_channel_type(std::function<uint8_t(bool,uint16_t)> func)
{
    g_lookup_channel_type = func;
}

static bool get_to_server(t125_mcs_pdu_t *root)
{
    bool to_server = true;
    if ( root->msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_REQUEST )
    {
        to_server = true;
    }
    if ( root->msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_INDICATION )
    {
        to_server = false;
    }
    return to_server;
}

static uint16_t get_channel_id(t125_mcs_pdu_t *root, const std::string &data)
{
    uint16_t channel_id = 0;
    if ( root->msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_REQUEST )
    {
        if ( data.size() >= 4 )
        {
            // big endian
            channel_id = ((((uint16_t)(data[2])) << 8) & 0xff00) | ((((uint16_t)(data[3])) << 0) & 0x00ff);
        }
    }
    if ( root->msg() == t125_mcs_pdu_t::T125_MCS_MSG_SEND_DATA_INDICATION )
    {
        if ( data.size() >= 4 )
        {
            // big endian
            channel_id = ((((uint16_t)(data[2])) << 8) & 0xff00) | ((((uint16_t)(data[3])) << 0) & 0x00ff);
        }
    }
    return channel_id;
}

static bool set_channel_type(t125_mcs_pdu_t *root, uint8_t channel_type)
{
    uint8_t *buffer = ((uint8_t*)root);
    for ( size_t i = 0; i < (sizeof(*root) - sizeof(uint8_t)); i++)
    {
        uint8_t tmp = buffer[i];
        buffer[i] = 0xaa;
        if ( root->channel_type() == 0xaa )
        {
            buffer[i] = 0x55;
            if ( root->channel_type() == 0x55 )
            {
                buffer[i] = channel_type;
                return true;
            }
        }
        buffer[i] = tmp;
    }
    return false;
}

std::string rdp_process_static_channel_type_t::decode(const std::string &data)
{
    bool to_server = get_to_server(this->root_);
    uint16_t channel_id = get_channel_id(this->root_, data);
    if ( g_lookup_channel_type && channel_id )
    {
        set_channel_type(this->root_, g_lookup_channel_type(to_server, channel_id));
    }
    return data;
}
