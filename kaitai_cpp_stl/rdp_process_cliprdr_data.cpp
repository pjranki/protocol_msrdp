#include "rdp_process_cliprdr_data.h"

static bool set_msg_flags(cliprdr_pdu_t *root, uint16_t msg_flags)
{
    uint8_t *buffer = ((uint8_t*)root);
    for ( size_t i = 0; i < (sizeof(*root) - sizeof(msg_flags)); i++)
    {
        uint16_t tmp = *((uint16_t*)&buffer[i]);
        *((uint16_t*)&buffer[i]) = 0xaaaa;
        if ( root->msg_flags() == 0xaaaa )
        {
            *((uint16_t*)&buffer[i]) = 0x5555;
            if ( root->msg_flags() == 0x5555 )
            {
                *((uint16_t*)&buffer[i]) = msg_flags;
                return true;
            }
        }
        *((uint16_t*)&buffer[i]) = tmp;
    }
    return false;
}

size_t calc_format_name_size(const char *data, size_t size, bool ascii_names)
{
    size_t char_size = (ascii_names ? 1 : 2);
    for ( size_t i = 0; i < size; i += char_size )
    {
        if ( ascii_names )
        {
            if ( data[i] == 0x00 )
            {
                return i + char_size;
            }
        }
        else
        {
            if ( *((uint16_t*)&data[i]) == 0x0000 )
            {
                return i + char_size;
            }
        }
    }
    return size;
}

void set_format_list_type(cliprdr_pdu_t *root, const std::string &data)
{
    // easy win, the size is not a mulitple of 36 (which it must be for short names)
    if ( (data.size() % 36) != 0 )
    {
        // TODO: broken by kaitai - doesn't handle 'strz' and 'utf-16le' at the same time
        //set_msg_flags(root, cliprdr_pdu_t::CB_FLAG_LONG_NAMES | root->msg_flags());
        return;
    }

    // heuristic
    bool ascii_names = (root->msg_flags() & cliprdr_pdu_t::CB_FLAG_ASCII_NAMES) != 0;
    for ( size_t i = 0; i < (data.size() / 36); i++ )
    {
        const char *format_item = data.c_str() + (i * 36);
        const char *format_name = format_item + 4;
        uint32_t format_id = *((uint32_t*)format_item);
        if ( format_id == 0 )
        {
            // TODO: broken by kaitai - doesn't handle 'strz' and 'utf-16le' at the same time
            //set_msg_flags(root, cliprdr_pdu_t::CB_FLAG_LONG_NAMES | root->msg_flags());
            return;
        }
        size_t format_name_size = calc_format_name_size(format_name, 32, ascii_names);
        if ( format_name_size <= 32 )
        {
            format_id = *((uint32_t*)(format_name + format_name_size));
            if ( format_id == 0 )
            {
                set_msg_flags(root, cliprdr_pdu_t::CB_FLAG_SHORT_NAMES | root->msg_flags());
                return;
            }
        }
    }

    // Could not determine
}

std::string rdp_process_cliprdr_data_t::decode(const std::string &data)
{
    if ( this->root_->msg_type() == cliprdr_pdu_t::CB_FORMAT_LIST )
    {
        set_format_list_type(this->root_, data);
    }
    return data;
}
