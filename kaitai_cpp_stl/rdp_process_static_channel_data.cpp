#include "rdp_process_static_channel_data.h"

#include <map>
#include <string>
#include <mutex>

static std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> g_decompress = nullptr;

void rdp_process_static_channel_decompress(std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> func)
{
    g_decompress = func;
}

static bool set_channel_type(channel_pdu_t *root, uint8_t channel_type)
{
    uint8_t *buffer = ((uint8_t*)root);
    for ( size_t i = 0; i < (sizeof(*root) - sizeof(uint8_t)); i++)
    {
        uint8_t tmp = buffer[i];
        buffer[i] = 0xaa;
        if ( root->channel_type() == 0xaa )
        {
            buffer[i] = 0x55;
            if ( root->channel_type() == 0x55 )
            {
                buffer[i] = channel_type;
                return true;
            }
        }
        buffer[i] = tmp;
    }
    return false;
}


static std::mutex g_fragment_mutex;
static bool g_fragment_init = false;
static std::map<uint8_t,std::string> g_fragment_to_client;
static std::map<uint8_t,size_t> g_fragment_to_client_size;
static std::map<uint8_t,std::string> g_fragment_to_server;
static std::map<uint8_t,size_t> g_fragment_to_server_size;

static void fragment_init()
{
    if ( g_fragment_init )
    {
        return;
    }
    g_fragment_init = true;
    for ( size_t i = 0; i < 0xff; i++ )
    {
        g_fragment_to_client[(uint8_t)i] = std::string("", 0);
        g_fragment_to_client_size[(uint8_t)i] = 0;
        g_fragment_to_server[(uint8_t)i] = std::string("", 0);
        g_fragment_to_server_size[(uint8_t)i] = 0;
    }
}

std::string rdp_process_static_channel_data_t::decode(const std::string &data)
{
    if ( this->root_->is_fragment() )
    {
        std::lock_guard<std::mutex> guard(g_fragment_mutex);
        fragment_init();
        uint8_t channel_type = this->root_->channel_type();
        if ( channel_type == channel_pdu_t::CHANNEL_TYPE_RAW )
        {
            return data;
        }
        static std::map<uint8_t,std::string> &fragment = this->root_->to_server() ? g_fragment_to_server : g_fragment_to_client;
        static std::map<uint8_t,size_t> &fragment_size = this->root_->to_server() ? g_fragment_to_server_size : g_fragment_to_client_size;
        if ( this->root_->first_fragment() )
        {
            fragment[channel_type] = data;
            fragment_size[channel_type] = this->root_->length();
            set_channel_type(this->root_, channel_pdu_t::CHANNEL_TYPE_RAW);
            return std::string("", 0);
        }
        if ( this->root_->last_fragment() )
        {
            std::string out_data = fragment[channel_type];
            size_t out_data_size = fragment_size[channel_type];
            fragment[channel_type] = std::string("", 0);
            fragment_size[channel_type] = 0;
            return out_data;
        }
        if ( fragment_size[channel_type] > 0 )
        {
            fragment[channel_type] = fragment[channel_type] + data;
            set_channel_type(this->root_, channel_pdu_t::CHANNEL_TYPE_RAW);
            return std::string("", 0);
        }
        set_channel_type(this->root_, channel_pdu_t::CHANNEL_TYPE_RAW);
        return data;
    }
    if (this->root_->is_compressed())
    {
        std::string out_data;
        if ( g_decompress && g_decompress(data,
                this->root_->to_server(),
                this->root_->compression_type(),
                this->root_->compression_packed_at_front(),
                this->root_->compression_packed_flushed(),
                &out_data)
            )
        {
            return out_data;
        }
        set_channel_type(this->root_, channel_pdu_t::CHANNEL_TYPE_RAW);
        return data;
    }
    return data;
}
