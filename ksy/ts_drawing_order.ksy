meta:
  id: ts_drawing_order
  endian: le
  imports:
    - ts_primary_drawing_order
    - ts_secondary_drawing_order
    - ts_altsec_drawing_order

doc: |
  MS-RDPEGDI 2.2.2.2.1 Drawing Order (DRAWING_ORDER)
  The DRAWING_ORDER structure is used to describe and encapsulate a
  single primary, secondary, or alternate secondary drawing order sent
  from server to client. All drawing orders conform to this basic
  structure (see sections 2.2.2.2.1.1.2, 2.2.2.2.1.2.1.1,
  and 2.2.2.2.1.3.1.1).
  mstscax!CUH::ProcessOrders

params:
  - id: primary_last_order_type
    type: u1
    doc: |
      Used only for TS_PRIMARY_DRAWING_ORDERS, this passes the order type
      of the previous order to the next order. This is because primary order
      types save space by only sending the drawing order type if it changes.

seq:
  - id: control_flags
    type: u1
    doc: |
      An 8-bit, unsigned integer. A control byte that identifies the class of the
      drawing order.
      If the TS_STANDARD (0x01) flag is set, the order is a primary drawing order.
      If both the TS_STANDARD (0x01) and TS_SECONDARY (0x02) flags are set, the order
      is a secondary drawing order. Finally, if only the TS_SECONDARY (0x02) flag is
      set, the order is an alternate secondary drawing order.
      More flags MAY be present, depending on the drawing order class. The flags listed
      are common to all three classes of drawing orders.
  - id: primary_order_type_change
    type: u1
    if: ((control_flags & 0x03) == 0x01) and ((control_flags & 0x08) != 0)
    doc: |
      Used only for TS_PRIMARY_DRAWING_ORDERS, this passes the order type
      of the previous order to the next order. This is because primary order
      types save space by only sending the drawing order type if it changes.
  - id: order
    type:
      switch-on: order_class
      -name: type
      cases:
        'order_class::primary': ts_primary_drawing_order(control_flags, primary_order_type)
        'order_class::secondary': ts_secondary_drawing_order(control_flags)
        'order_class::alternative': ts_altsec_drawing_order(control_flags)

instances:
  order_class:
    value: control_flags & 0x03
    enum: order_class
  primary_order_type:
    value: '((control_flags & 0x03) == 0x01) and ((control_flags & 0x08) != 0) ? primary_order_type_change : primary_last_order_type'
    doc: |
      Used only for TS_PRIMARY_DRAWING_ORDERS, this passes the order type
      of the previous order to the next order. This is because primary order
      types save space by only sending the drawing order type if it changes.

enums:
  order_class:
    0x00:
      id: unknown
      doc: Unknown order class 0.
    0x01:
      id: primary
      doc: Indicates a MS-RDPEGDI 2.2.2.2.1.1 Primary Drawing Order.
    0x03:
      id: secondary
      doc: Indicates a MS-RDPEGDI 2.2.2.2.1.2 Secondary Drawing Order.
    0x02:
      id: alternative
      doc: Indicates a MS-RDPEGDI 2.2.2.2.1.3 Alternative Secondary Drawing Order.
  control_flag:
    0x01:
      id: ts_standard
      doc: TS_STANDARD and TS_SECONDARY are used to determine the class (primary, secondary, alternative)
    0x02:
      id: ts_secondary
      doc: TS_STANDARD and TS_SECONDARY are used to determine the class (primary, secondary, alternative)
