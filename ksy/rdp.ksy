meta:
  id: rdp
  endian: le
  imports:
    - ts_fp_input_pdu
    - ts_fp_update_pdu
    - t123_tpkt

doc: |
  rdpbase!CNamedPipeStream::ReadBuffer (hook CRDPENCCONStreamBuffer::put_PayloadSize to see when buffer is filled)
  rdpbase!CNamedPipeStream::WriteBuffer

params:
  - id: to_server
    type: bool

seq:
  - id: version
    type: u1
    doc: |
      T123/X224 version number or first byte of fastpath PDU.
  - id: ts_fp_input_pdu
    type: ts_fp_input_pdu(version)
    if: pdu_type == pdu_type::fastpath_input
    -set:
      - id: to_server
        value: true
        doc: |
          Input packets only go to the RDP server.
      - id: version
        value: (version & 0xfc) | (fastpath_action::fastpath.to_i & 0x3)
        doc: |
          Input packets use fastpath PDUs.
  - id: ts_fp_update_pdu
    type: ts_fp_update_pdu(version)
    if: pdu_type == pdu_type::fastpath_update
    -set:
      - id: to_server
        value: false
        doc: |
          Update packets only go to the RDP client.
      - id: version
        value: (version & 0xfc) | (fastpath_action::fastpath.to_i & 0x3)
        doc: |
          Update packets use fastpath PDUs.
  - id: t123_tpkt
    type: t123_tpkt(version)
    if: pdu_type == pdu_type::x224
    -set:
      - id: version
        value: (version & 0xfc) | (fastpath_action::x224.to_i & 0x3)
        doc: |
          T123 TPKT is used for X224 PDUs.

instances:
  action:
    value: version & 0x3
    enum: fastpath_action
  pdu_type:
    value: 'action == fastpath_action::x224 ? pdu_type::x224 : ( to_server ? pdu_type::fastpath_input : pdu_type::fastpath_update)'
    enum: pdu_type

-update:
  - id: version
    value: (ts_fp_input_pdu.first_byte & 0xfc) | (fastpath_action::fastpath.to_i & 0x3)
    if: pdu_type == pdu_type::fastpath_input
    doc: |
      Update the version byte with values from the fastpath input PDU.
  - id: version
    value: (ts_fp_update_pdu.first_byte & 0xfc) | (fastpath_action::fastpath.to_i & 0x3)
    if: pdu_type == pdu_type::fastpath_update
    doc: |
      Update the version byte with values from the fastpath update PDU.
  - id: version
    value: fastpath_action::x224.to_i
    if: pdu_type == pdu_type::x224
    doc: |
      Update the version byte with the X224 version.

enums:
  fastpath_action:
    0x0: fastpath
    0x3: x224
  pdu_type:
    0x1: fastpath_input
    0x2: fastpath_update
    0x3: x224
