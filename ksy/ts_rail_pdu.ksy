meta:
  id: ts_rail_pdu
  endian: le

doc: |
    MS-RDPERP 2.2.2 Static Virtual Channel Protocol
    The RAIL Static Virtual Channel (named "RAIL") is responsible
    for communicating non–RDP specific data between the RAIL client
    and server. The following sections outline the messages that are
    transmitted over the virtual channel.
    mstscax!RdpRemoteAppPlugin::OnVcPacket
    mstscax!RdpRemoteAppCore::OnRailPdu

seq:
  - id: order_type
    type: u2
    enum: ts_rail_order
    doc: |
      An unsigned 16-bit integer. The type of the Virtual Channel
      message.
  - id: order_length
    type: u2
    doc: |
      An unsigned 16-bit integer. The length of the Virtual Channel PDU,
      in bytes.
  - id: order
    type:
      switch-on: order_type
      -name: type
      cases:
        'ts_rail_order::exec': ts_rail_order_exec
        'ts_rail_order::activate': ts_rail_order_activate
        'ts_rail_order::sysparam': ts_rail_order_sysparam
        'ts_rail_order::syscommand': ts_rail_order_syscommand
        'ts_rail_order::handshake': ts_rail_order_handshake
        'ts_rail_order::notify_event': ts_rail_order_notify_event
        'ts_rail_order::windowmove': ts_rail_order_windowmove
        'ts_rail_order::localmovesize': ts_rail_order_localmovesize
        'ts_rail_order::minmaxinfo': ts_rail_order_minmaxinfo
        'ts_rail_order::clientstatus': ts_rail_order_clientstatus
        'ts_rail_order::sysmenu': ts_rail_order_sysmenu
        'ts_rail_order::langbarinfo': ts_rail_order_langbarinfo
        'ts_rail_order::exec_result': ts_rail_order_exec_result
        'ts_rail_order::get_appid_req': ts_rail_order_get_appid_req
        'ts_rail_order::get_appid_resp': ts_rail_order_get_appid_resp
        'ts_rail_order::taskbarinfo': ts_rail_order_taskbarinfo
        'ts_rail_order::languageimeinfo': ts_rail_order_languageimeinfo
        'ts_rail_order::compartmentinfo': ts_rail_order_compartmentinfo
        'ts_rail_order::handshake_ex': ts_rail_order_handshake_ex
        'ts_rail_order::zorder_sync': ts_rail_order_zorder_sync
        'ts_rail_order::cloak': ts_rail_order_cloak
        'ts_rail_order::power_display_request': ts_rail_order_power_display_request
        'ts_rail_order::snap_arrange': ts_rail_order_snap_arrange
        'ts_rail_order::get_appid_resp_ex': ts_rail_order_get_appid_resp_ex

enums:
  ts_rail_order:
    0x0001:
      id: exec
      doc: |
        Indicates a Client Execute PDU from client to server.
    0x0002:
      id: activate
      doc: |
        Indicates a Client Activate PDU from client to server.
    0x0003:
      id: sysparam
      doc: |
        Indicates a Client System Parameters Update PDU from client
        to server or a Server System Parameters Update PDU from server
        to client.
    0x0004:
      id: syscommand
      doc: |
        Indicates a Client System Command PDU from client to server.
    0x0005:
      id: handshake
      doc: |
        Indicates a bi-directional Handshake PDU.
    0x0006:
      id: notify_event
      doc: |
        Indicates a Client Notify Event PDU from client to server.
    0x0008:
      id: windowmove
      doc: |
        Indicates a Client Window Move PDU from client to server.
    0x0009:
      id: localmovesize
      doc: |
        Indicates a Server Move/Size Start PDU and a Server Move/Size
        End PDU from server to client.
    0x000a:
      id: minmaxinfo
      doc: |
        Indicates a Server Min Max Info PDU from server to client.
    0x000b:
      id: clientstatus
      doc: |
        Indicates a Client Information PDU from client to server.
    0x000c:
      id: sysmenu
      doc: |
        Indicates a Client System Menu PDU from client to server.
    0x000d:
      id: langbarinfo
      doc: |
        Indicates a Server Language Bar Information PDU from server to
        client, or a Client Language Bar Information PDU from client to
        server.
    0x0080:
      id: exec_result
      doc: |
        Indicates a Server Execute Result PDU from server to client.
    0x000E:
      id: get_appid_req
      doc: |
        Indicates a Client Get Application ID PDU from client to server.
    0x000F:
      id: get_appid_resp
      doc: |
        Indicates a Server Get Application ID Response PDU from server to
        client.
    0x0010:
      id: taskbarinfo
      doc: |
        Indicates a Taskbar Tab Info PDU (section 2.2.2.14.1) from server
        to client.
    0x0011:
      id: languageimeinfo
      doc: |
        Indicates a Language Profile Information PDU from client to server
    0x0012:
      id: compartmentinfo
      doc: |
        Indicates a bi-directional Compartment Status Information PDU.
    0x0013:
      id: handshake_ex
      doc: |
        Indicates a HandshakeEx PDU from server to client.
    0x0014:
      id: zorder_sync
      doc: |
        Indicates a Server Z-Order Sync Information PDU from server to
        client.
    0x0015:
      id: cloak
      doc: |
        Indicates a Window Cloak State Change PDU from client to server.
    0x0016:
      id: power_display_request
      doc: |
        Indicates a Power Display Request PDU (section 2.2.2.13.1) from
        server to client.
    0x0017:
      id: snap_arrange
      doc: |
        Indicates a Client Window Snap PDU (section 2.2.2.7.5) from client
        to server.
    0x0018:
      id: get_appid_resp_ex
      doc: |
        Indicates a Server Get Application ID Extended Response PDU
        (section 2.2.2.8.2) from server to client.

types:
  ts_rail_order_exec:
    doc: |
      MS-RDPERP 2.2.2.3.1 Client Execute PDU (TS_RAIL_ORDER_EXEC)
      The Client Execute PDU is sent from a client to a server to request
      that a remote application launch on the server.
    seq:
      - id: flags
        type: u2
        doc: |
          An unsigned 16-bit integer. Specifies a bit field of flags that
          indicate modifications to the Client Execute PDU fields.
      - id: exe_or_file_length
        type: u2
        doc: |
          An unsigned 16-bit integer. Specifies the length of the ExeOrFile
          field in bytes. The length MUST be nonzero. The maximum length is
          520 bytes.
      - id: working_dir_length
        type: u2
        doc: |
          An unsigned 16-bit integer. Specifies the length of the WorkingDir
          field, in bytes. The minimum length is 0, and the maximum length is
          520 bytes.
      - id: arguments_len
        type: u2
        doc: |
          An unsigned 16-bit integer. Specifies the length of the Arguments
          field, in bytes. The minimum length is 0, and the maximum length is
          16,000 bytes.<12>
      - id: exe_or_file
        type: str
        size: exe_or_file_length
        encoding: UTF-16LE
        doc: |
          A non-null-terminated string of Unicode characters. Variable length.
          Specifies the executable, file path, or application user model ID to be
          launched on the server. This field MUST be present. The maximum length
          of this field, including file path translations (see
          TS_RAIL_EXEC_FLAG_TRANSLATE_FILES mask of Flags field), is 520 bytes.
      - id: working_dir
        type: str
        size: working_dir_length
        encoding: UTF-16LE
        if: working_dir_length > 0
        doc: |
          Optional non-null-terminated string of Unicode characters. Variable length.
          Specifies the working directory of the launched ExeOrFile field. If the
          WorkingDirLength field is 0, this field MUST NOT be present; otherwise, it
          MUST be present. The maximum length of this field, including expanded
          environment variables (see TS_RAIL_EXEC_FLAG_EXPAND_WORKINGDIRECTORY mask of
          Flags field), is 520 bytes.
      - id: arguments
        type: str
        size: arguments_len
        encoding: UTF-16LE
        if: arguments_len > 0
        doc: |
          Optional non-null-terminated string of Unicode characters. Variable length.
          Specifies the arguments to the ExeOrFile field. If the ArgumentsLen field is
          0, this field MUST NOT be present; otherwise, it MUST be present. The maximum
          length of this field, including expanded environment variables (see
          TS_RAIL_EXEC_FLAG_EXPAND_ARGUMENTS mask of Flags field), is 16,000 bytes.
    enums:
      ts_rail_exec_flag:
        0x0001:
          id: expand_workingdirectory
          doc: |
            The environment variables in the WorkingDir field MUST be expanded
            on the server.
        0x0002:
          id: translate_files
          doc: |
            The drive letters in the file path MUST be converted to corresponding
            mapped drives on the server. This flag MUST NOT be set if the
            TS_RAIL_EXEC_FLAG_FILE (0x0004) flag is not set.
        0x0004:
          id: file
          doc: |
            If this flag is set, the ExeOrFile field refers to a file path. If
            it is not set, the ExeOrFile field refers to an executable.
        0x0008:
          id: expand_arguments
          doc: |
            The environment variables in the Arguments field MUST be expanded on
            the server.
        0x0010:
          id: app_user_model_id
          doc: |
            If this flag is set, the ExeOrFile field refers to an application user
            model ID. If it is not set, the ExeOrFile field refers to a file path.
            This flag MUST be ignored if the TS_RAIL_EXEC_FLAG_FILE (0x0004) flag
            is set. An application user model ID is a string that uniquely identifies
            an application, regardless of where the application is installed on the
            operating system. The string can be used to identify Windows Store
            applications as well as desktop applications.

  ts_rail_order_activate:
    doc: |
      MS-RDPERP 2.2.2.6.1 Client Activate PDU (TS_RAIL_ORDER_ACTIVATE)
      The Client Activate PDU is sent from client to server when a local RAIL window
      on the client is activated or deactivated.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the associated window on the server
          that is to be activated or deactivated.
      - id: enabled
        type: u1
        doc: |
          An unsigned 8-bit integer. Indicates whether the window is to be activated
          (value = nonzero) or deactivated (value = 0).
    instances:
      activated:
        value: 'enabled != 0 ? true : false'
      deactivated:
        value: 'enabled == 0 ? true : false'

  ts_rail_order_sysparam:
    doc: |
      MS-RDPERP 2.2.2.5.1 Server System Parameters Update PDU (TS_RAIL_ORDER_SYSPARAM)
      The Server System Parameters Update PDU is sent from the server to client to
      synchronize system parameters on the client with those on the server.
      mstscax!RdpRemoteAppCore::OnRailOrderSysParam
      mstscax!RdpWinDesktopRemoteAppUIManager::UpdateSystemParam
    seq:
      - id: system_parameter
        type: u4
        doc: |
          An unsigned 32-bit integer. The type of system parameter being transmitted.
      - id: body
        type: u1
        doc: |
          The content of this field depends on the SystemParameter field. The following
          table outlines the valid values of the SystemParameter field (Value column)
          and corresponding values of the Body field (Meaning column):
            - SPI_SETSCREENSAVEACTIVE:
                Size of Body field: 1 byte.
                0 (FALSE): Screen saver is not enabled.
                Nonzero (TRUE): Screen Saver is enabled.
            - SPI_SETSCREENSAVESECURE:
                Size of Body field: 1 byte.
                0 (FALSE): Do not lock the desktop when switching out of screen saver mode.
                Nonzero (TRUE): Lock the desktop when switching out of screen saver mode.
    enums:
      spi:
        0x00000011:
          id: setscreenactive
          doc: |
            The system parameter indicating whether the screen saver is enabled.
        0x00000077:
          id: setscreensavesecure
          doc: |
            The system parameter indicating whether the desktop is to be locked after
            switching out of screen saver mode (that is, after the screen saver starts
            due to inactivity, then stops due to activity).<16>

  ts_rail_order_syscommand:
    doc: |
      MS-RDPERP 2.2.2.6.3 Client System Command PDU (TS_RAIL_ORDER_SYSCOMMAND)
      The Client System Command PDU packet is sent from the client to the server when a
      local RAIL window on the client receives a command to perform an action on the window,
      such as minimize or maximize. This command is forwarded to the server via the System
      Command PDU.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server to activate or
          deactivate.
      - id: command
        type: u2
        enum: sc
        doc: |
          An unsigned 16-bit integer. Specifies the type of command.
    enums:
      sc:
        0xF000:
          id: size
          doc: Resize the window.
        0xF010:
          id: move
          doc: Move the window.
        0xF020:
          id: minimize
          doc: Minimize the window.
        0xF030:
          id: maximize
          doc: Maximize the window.
        0xF060:
          id: close
          doc: Close the window.
        0xF100:
          id: keymenu
          doc: The ALT + SPACE key combination was pressed; display the window's system menu.
        0xF120:
          id: restore
          doc: Restore the window to its original shape and size.
        0xF160:
          id: default
          doc: Perform the default action of the window's system menu.

  ts_rail_order_handshake:
    doc: |
      MS-RDPERP 2.2.2.2.1 Handshake PDU (TS_RAIL_ORDER_HANDSHAKE)
      The Handshake PDU is exchanged between the server and the client to establish that both
      endpoints are ready to begin RAIL mode. The server sends the Handshake PDU and the client
      responds with the Handshake PDU.
      mstscax!RdpRemoteAppCore::ReceiveHandshake
      mstscax!RdpRemoteAppCore::OnHandshake
    seq:
      - id: build_number
        type: u4
        doc: |
          An unsigned 32-bit integer. The build or version of the sending party.

  ts_rail_order_notify_event:
    doc: |
      MS-RDPERP 2.2.2.6.4 Client Notify Event PDU (TS_RAIL_ORDER_NOTIFY_EVENT)
      The Client Notify Event PDU packet is sent from a client to a server when a local RAIL
      Notification Icon on the client receives a keyboard or mouse message from the user. This
      notification is forwarded to the server via the Notify Event PDU.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the associated window on the server that owns the
          notification icon being specified in the PDU.
      - id: notify_icon_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the associated notification icon on the server that
          SHOULD receive the keyboard or mouse interaction.
      - id: message
        type: u4
        enum: message
        doc: |
          An unsigned 32-bit integer. The message being sent to the notification icon on the server.
    enums:
      message:
        0x00000201:
          id: wm_lbuttondown
          doc: |
            The user pressed the left mouse button in the client area of the notification icon.
        0x00000202:
          id: wm_lbuttonup
          doc: |
            The user released the left mouse button while the cursor was in the client area of the
            notification icon.
        0x00000204:
          id: wm_rbuttondown
          doc: |
            The user pressed the right mouse button in the client area of the notification icon.
        0x00000205:
          id: wm_rbuttonup
          doc: |
            The user released the right mouse button while the cursor was in the client area of the
            notification icon.
        0x0000007B:
          id: wm_contextmenu
          doc: |
            The user selected a notification icon’s shortcut menu with the keyboard. This message is
            sent only for notification icons that follow Windows 2000 behavior
            (see Version field insection 2.2.1.3.2.2.1).
        0x00000203:
          id: wm_lbuttondblclk
          doc: |
            The user double-clicked the left mouse button in the client area of the notification icon.
        0x00000206:
          id: wm_rbuttondblclk
          doc: |
            The user double-clicked the right mouse button in the client area of the notification icon.
        0x00000400:
          id: nin_select
          doc: |
            The user selected a notification icon with the mouse and activated it with the ENTER key.
            This message is sent only for notification icons that follow Windows 2000 behavior
            (see Version field in section 2.2.1.3.2.2.1).
        0x00000401:
          id: nin_keyselect
          doc: |
            The user selected a notification icon with the keyboard and activated it with the SPACEBAR
            or ENTER key. This message is sent only for notification icons that follow Windows 2000 behavior
            (see Version field in section 2.2.1.3.2.2.1).
        0x00000402:
          id: nin_balloonshow
          doc: |
            The user passed the mouse pointer over an icon with which a balloon tooltip is associated
            (see InfoTip field in section 2.2.1.3.2.2.1), and the balloon tooltip was shown. This message
            is sent only for notification icons that follow Windows 2000 behavior
            (see Version field in section 2.2.1.3.2.2.1).
        0x00000403:
          id: nin_ballonhide
          doc: |
            The icon's balloon tooltip disappeared because, for example, the icon was deleted. This message
            is not sent if the balloon is dismissed because of a timeout or mouse click by the user. This
            message is sent only for notification icons that follow Windows 2000 behavior
            (see Version field in section 2.2.1.3.2.2.1).
        0x00000404:
          id: ballontimeout
          doc: |
            The icon's balloon tooltip was dismissed because of a timeout. This message is sent only for
            notification icons that follow Windows 2000 behavior
            (see Version field in section 2.2.1.3.2.2.1).
        0x00000405:
          id: nin_ballonuserclick
          doc: |
            User dismissed the balloon by clicking the mouse. This message is sent only for notification icons
            that follow Windows 2000 behavior (see Version field in section 2.2.1.3.2.2.1).

  ts_rail_order_windowmove:
    doc: |
      MS-RDPERP 2.2.2.7.4 Client Window Move PDU (TS_RAIL_ORDER_WINDOWMOVE)
      The Client Window Move PDU packet is sent from the client to the server when a local window is ending
      a move or resize. The client communicates the locally moved or resized window's position to the server
      by using this packet. The server uses this information to reposition its window.
      Window positions sent using this packet SHOULD include hit-testable margins
      (see section 3.2.5.1.6).
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server corresponding to the local window
          that was moved or resized.
      - id: left
        type: u2
        doc: |
          A signed 16-bit integer. The x-coordinate of the top-left corner of the window's new position.
      - id: top
        type: u2
        doc: |
          A signed 16-bit integer. The y-coordinate of the top-left corner of the window's new position.
      - id: right
        type: u2
        doc: |
          A signed 16-bit integer. The x-coordinate of the bottom-right corner of the window's new position.
      - id: bottom
        type: u2
        doc: |
          A signed 16-bit integer. The y-coordinate of the bottom-right corner of the window's new position.

  ts_rail_order_localmovesize:
    doc: |
      MS-RDPERP 2.2.2.7.3 Server Move/Size End PDU (TS_RAIL_ORDER_LOCALMOVESIZE)
      The Server Move/Size End PDU is sent by the server when a window on the server is completing a
      move or resize. The client uses this information to end a local move/resize of the corresponding
      local window.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppWindow::StartLocalMoveResize
      mstscax!RdpWinDesktopRemoteAppWindow::EndLocalMoveResize
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server that is being moved or resized.
          mstscax!RdpRemoteAppCore::GetRemoteAppWindow
          mstscax!RdpWinDesktopRemoteAppUIManager::GetWindow
          mstscax!RdpRemoteAppCore::GetDesktopRemoteAppWindow
      - id: is_move_size_start
        type: u2
        doc: |
          An unsigned 16-bit integer. Indicates the move or resize is ending. This field MUST be set to 0.
      - id: move_size_type
        type: u2
        enum: rail_wmsz
        doc: |
          An unsigned 16-bit integer. Indicates the type of the move/size.
      - id: top_left_x
        type: u2
        doc: |
          A signed 16-bit integer. The x-coordinate of the moved or resized window's top-left corner.
      - id: top_left_y
        type: u2
        doc: |
          A signed 16-bit integer. The y-coordinate of the moved or resized window's top-left corner.
    enums:
      rail_wmsz:
        0x0001:
          id: left
          doc: The left edge of the window is being sized.
        0x0002:
          id: right
          doc: The right edge of the window is being sized.
        0x0003:
          id: top
          doc: The top edge of the window is being sized.
        0x0004:
          id: topleft
          doc: The top-left corner of the window is being sized.
        0x0005:
          id: topright
          doc: The top-right corner of the window is being sized.
        0x0006:
          id: bottom
          doc: The bottom edge of the window is being sized.
        0x0007:
          id: bottomleft
          doc: The bottom-left corner of the window is being sized.
        0x0008:
          id: bottomright
          doc: The bottom-right corner of the window is being sized.
        0x0009:
          id: move
          doc: The window is being moved by using the mouse.
        0x000A:
          id: keymove
          doc: The window is being moved by using the keyboard.
        0x000B:
          id: keysize
          doc: The window is being resized by using the keyboard.

  ts_rail_order_minmaxinfo:
    doc: |
      MS-RDPERP 2.2.2.7.1 Server Min Max Info PDU (TS_RAIL_ORDER_MINMAXINFO)
      The Server Min Max Info PDU is sent from a server to a client when a window move or resize on
      the server is being initiated. This PDU contains information about the minimum and maximum
      extents to which the window can be moved or sized.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppWindow::UpdateMinMaxInfoFromServer
      mstscax!RdpWinDesktopRemoteAppWindow::OnMinMaxInfoFromServer
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server that is being moved or resized.
          mstscax!RdpRemoteAppCore::GetRemoteAppWindow
          mstscax!RdpWinDesktopRemoteAppUIManager::GetWindow
          mstscax!RdpRemoteAppCore::GetDesktopRemoteAppWindow
      - id: max_width
        type: s2
        doc: |
          A signed 16-bit integer. The width of the maximized window.
      - id: max_height
        type: s2
        doc: |
          A signed 16-bit integer. The height of the maximized window.
      - id: max_pos_x
        type: s2
        doc: |
          A signed 16-bit integer. The x-coordinate of the top-left corner of the maximized window.
      - id: max_pos_y
        type: s2
        doc: |
          A signed 16-bit integer. The y-coordinate of the top-left corner of the maximized window.
      - id: min_track_width
        type: s2
        doc: |
          A signed 16-bit integer. The minimum width to which the window can be resized.
      - id: min_track_height
        type: s2
        doc: |
          A signed 16-bit integer. The minimum height to which the window can be resized.
      - id: max_track_width
        type: s2
        doc: |
          A signed 16-bit integer. The maximum width to which the window can be resized.
      - id: max_track_height
        type: s2
        doc: |
          A signed 16-bit integer. The maximum height to which the window can be resized.

  ts_rail_order_clientstatus:
    doc: |
      MS-RDPERP 2.2.2.2.2 Client Information PDU (TS_RAIL_ORDER_CLIENTSTATUS)
      The Client Information PDU is sent from client to server and contains information about
      RAIL client state and features supported by the client.
    seq:
      - id: flags
        type: u4
        doc: |
          An unsigned 32-bit integer. RAIL features that are supported by the client; MUST be
          set to one or more of the following feature flags, or zero if none of the features
          are supported.
    enums:
      ts_rail_clientstatus:
        0x00000001:
          id: allowlocalmovesize
          doc: |
            Indicates that the client supports the local move/size RAIL feature.
        0x00000002:
          id: autoreconnect
          doc: |
            Indicates that the client is auto-reconnecting to the server after an unexpected
            disconnect of the session.
        0x00000004:
          id: zorder_sync
          doc: |
            Indicates that the client supports Z-order sync using the Z-Order Sync Information
            PDU (section 2.2.2.11.1).
        0x00000010:
          id: window_resize_margin_supported
          doc: |
            Indicates that the client supports resize margins using the Window Information
            PDU (section 2.2.1.3.1).
        0x00000020:
          id: high_dpi_icons_supported
          doc: |
            Indicates that the client supports icons up to 96×96 pixels in size in the Window
            Icon PDU (section 2.2.1.3.1.2.2). If this flag is not present, icon dimensions are
            limited to 32×32 pixels.
        0x00000040:
          id: appbar_remoting_supported
          doc: |
            Indicates that the client supports application desktop toolbar remoting using the
            Window Information PDU (section 2.2.1.3.1).
        0x00000080:
          id: power_display_request_supported
          doc: |
            Indicates that the client supports display-required power requests sent using the
            Power Display Request PDU (section 2.2.2.13.1).
        0x00000200:
          id: bidirectional_cloak_supported
          doc: |
            Indicates that the client is capable of processing Window Cloak State Change PDUs
            (section 2.2.2.12.1) sent by the server.

  ts_rail_order_sysmenu:
    doc: |
      MS-RDPERP 2.2.2.6.2 Client System Menu PDU (TS_RAIL_ORDER_SYSMENU)
      The Client System Menu PDU packet is sent from the client to the server when a local RAIL
      window on the client receives a command to display its System menu. This command is
      forwarded to the server via the System menu PDU.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server that SHOULD display its
          System menu.
      - id: left
        type: s2
        doc: |
          A 16-bit signed integer. The x-coordinate of the top-left corner at which the System
          menu SHOULD be displayed. Specified in screen coordinates.
      - id: top
        type: s2
        doc: |
          A 16-bit signed integer. The y-coordinate of the top-left corner at which the System
          menu SHOULD be displayed. Specified in screen coordinates.

  ts_rail_order_langbarinfo:
    doc: |
      MS-RDPERP 2.2.2.9.1 Language Bar Information PDU (TS_RAIL_ORDER_LANGBARINFO)
      The Language Bar Information PDU is used to set the language bar status. It is sent from a
      client to a server or a server to a client, but only when both support the Language Bar
      docking capability (TS_RAIL_LEVEL_DOCKED_LANGBAR_SUPPORTED). This PDU contains information
      about the language bar status.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppUIManager::UpdateLanguageBar
      mstscax!WindowsRemoteAppLib::RailLanguagebarHandler::OnRailLangbarPdu
    seq:
      - id: language_bar_status
        type: u4
        doc:
          An unsigned 32-bit integer. The server sends the LanguageBarStatus it retrieves from the
          local language bar.
    enums:
      tf_sft:
        0x00000001:
          id: shownormal
          doc: |
            Display the language bar as a floating window. This constant cannot be combined with
            the TF_SFT_DOCK, TF_SFT_MINIMIZED, TF_SFT_HIDDEN, or TF_SFT_DESKBAND constants.
        0x00000002:
          id: dock
          doc: |
            Dock the language bar in its own task pane. This constant cannot be combined with the
            TF_SFT_SHOWNORMAL, TF_SFT_MINIMIZED, TF_SFT_HIDDEN, or TF_SFT_DESKBAND constants.<20>
        0x00000004:
          id: minimized
          doc: |
            Display the language bar as a single icon in the system tray. This constant cannot be
            combined with the TF_SFT_SHOWNORMAL, TF_SFT_DOCK, TF_SFT_HIDDEN, or TF_SFT_DESKBAND
            constants.
        0x00000008:
          id: hidden
          doc: |
            Hide the language bar. This constant cannot be combined with the TF_SFT_SHOWNORMAL,
            TF_SFT_DOCK, TF_SFT_MINIMIZED, or TF_SFT_DESKBAND constants.
        0x00000010:
          id: notransparency
          doc: |
            Make the language bar opaque.
        0x00000020:
          id: lowtransparency
          doc: |
            Make the language bar partially transparent.<21>
        0x00000040:
          id: hightransparency
          doc: |
            Make the language bar highly transparent.<22>
        0x00000080:
          id: labels
          doc: |
            Display text labels next to language bar icons.
        0x00000100:
          id: nolabels
          doc: |
            Hide language bar icon text labels.
        0x00000200:
          id: extraiconsonminimized
          doc: |
            Display text service icons on the taskbar when the language bar is minimized.
        0x00000400:
          id: noextraiconsonminimized
          doc: |
            Hide text service icons on the taskbar when the language bar is minimized.
        0x00000800:
          id: deskband
          doc: |
            Dock the language bar in the system task bar. This constant cannot be combined
            with the TF_SFT_SHOWNORMAL, TF_SFT_DOCK, TF_SFT_MINIMIZED, or TF_SFT_HIDDEN
            constants.<23>

  ts_rail_order_exec_result:
    doc: |
      MS-RDPERP 2.2.2.3.2 Server Execute Result PDU (TS_RAIL_ORDER_EXEC_RESULT)
      The Server Execute Result PDU is sent from server to client in response to a Client
      Execute PDU request, and contains the result of the server's attempt to launch the
      requested executable.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpRemoteAppCore::OnExecResult
      mstscax!RdpRemoteAppCore::OnExecResultCB
      mstscax!RdpRemoteAppCore::Fire_ExecResult
      mstscax!RdpWinDesktopRemoteAppUIManager::UpdateExecResult
      mstscax!CUI::UI_OnRemoteApplicationResult
      mstscax!CMsTscAx::OnCoreApiRemoteApplicationResult
      mstscax!CProxy_IMsTscAxEvents<CMsTscAx>::Fire_OnRemoteProgramResult
      mstscax!CProxy_IMsTscAxEvents<CMsTscAx>::Fire_Event
    seq:
      - id: flags
        type: u2
        doc: |
          An unsigned 16-bit integer. Identical to the Flags field of the Client Execute
          PDU. The server sets this field to enable the client to match the Client Execute
          PDU with the Server Execute Result PDU.
      - id: exec_result
        type: u2
        enum: rail_exec
        doc: |
          An unsigned 16-bit integer. The result of the Client Execute PDU. This field MUST
          be set to one of the following values.
      - id: raw_result
        type: u4
        doc: |
          An unsigned 32-bit integer. Contains an operating system-specific return code for
          the result of the Client Execute request.<13>
      - id: padding
        type: u2
        doc: |
          An unsigned 16-bit integer. Not used.
      - id: exe_or_file_length
        type: u2
        doc: |
          An unsigned 16-bit integer. Specifies the length of the ExeOrFile field in bytes.
          The length MUST be nonzero. The maximum length is 520 bytes.
      - id: exe_or_file
        type: str
        size: exe_or_file_length
        encoding: UTF-16LE
        doc: |
          The executable or file that was attempted to be launched. This field is copied from
          the ExeOrFile field of the Client Execute PDU. The server sets this field to enable
          the client to match the Client Execute PDU with the Server Execute Result PDU.
    enums:
      rail_exec:
        0x0000:
          id: s_ok
          doc: |
            The Client Execute request was successful and the requested application or file
            has been launched.
        0x0001:
          id: e_hook_not_loaded
          doc: |
            The Client Execute request could not be satisfied because the server is not
            monitoring the current input desktop.
        0x0002:
          id: e_decode_failed
          doc: |
            The Execute request could not be satisfied because the request PDU was malformed.
        0x0003:
          id: e_not_in_allowlist
          doc: |
            The Client Execute request could not be satisfied because the requested application
            was blocked by policy from being launched on the server.
        0x0005:
          id: e_file_not_found
          doc: |
            The Client Execute request could not be satisfied because the application or file
            path could not be found.
        0x0006:
          id: e_fail
          doc: |
            The Client Execute request could not be satisfied because an unspecified error
            occurred on the server.
        0x0007:
          id: e_session_locked
          doc: |
            The Client Execute request could not be satisfied because the remote session is
            locked.

  ts_rail_order_get_appid_req:
    doc: |
      MS-RDPERP 2.2.2.6.5 Client Get Application ID PDU (TS_RAIL_ORDER_GET_APPID_REQ)
      The Client Get Application ID PDU is sent from a client to a server. This PDU requests
      information from the server about the Application ID that the window SHOULD<17> have on
      the client.
      The server MAY ignore this PDU.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer specifying the ID of the associated window on the server
          that requires needs an Application ID.

  ts_rail_order_get_appid_resp:
    doc: |
      MS-RDPERP 2.2.2.8.1 Server Get Application ID Response PDU (TS_RAIL_ORDER_GET_APPID_RESP)
      The Server Get Application ID Response PDU is sent from a server to a client as a response
      to a Client Get Application ID PDU (section 2.2.2.6.5).
      This PDU specifies the Application ID that the specified window SHOULD<18> have on the
      client. The client MAY ignore this PDU.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpRemoteAppCore::UpdateAppID
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer specifying the ID of the associated window on the server
          whose Application ID is being sent to the client.
          mstscax!RdpRemoteAppCore::GetRemoteAppWindow
          mstscax!RdpRemoteAppCore::UpdateAppID
      - id: application_id
        type: strz
        size: 520
        encoding: UTF-16LE
        doc: |
          A null-terminated string of Unicode characters specifying the Application ID that the
          Client SHOULD associate with its window, if it supports using the Application ID for
          identifying and grouping windows.

  ts_rail_order_taskbarinfo:
    doc: |
      MS-RDPERP 2.2.2.14.1 Taskbar Tab Info PDU (TS_RAIL_ORDER_TASKBARINFO)
      The Taskbar Tab Info PDU is sent from the server to the client when a remote tabbed application
      adds, removes, or changes the state of a taskbar tab. It is sent only when the client advertises
      support for the extended shell integration capability
      (TS_RAIL_LEVEL_SHELL_INTEGRATION_SUPPORTED).
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppUIManager::UpdateTabOnTaskbar
    seq:
      - id: taskbar_message
        type: u4
        enum: rail_taskbar_msg_tab
        doc: |
          An unsigned 32-bit integer. Contains the type of tabbed application event that occurred in
          the remote session.
      - id: window_id_tab
        type: u4
        doc: |
          An unsigned 32-bit integer. The window ID of the remote application window whose tab group
          state is changing.
      - id: body
        type: u4
        doc: |
          The contents of this field depend on the TaskbarMessage field. The following table outlines
          the valid values of the TaskbarMessage field and the corresponding values of the Body field.
           - RAIL_TASKBAR_MSG_TAB_REGISTER:
             The window ID of the window that is being added to the taskbar tab group owned by
             WindowIdTab.
           - RAIL_TASKBAR_MSG_TAB_UNREGISTER:
             Not used. The Body field MUST still be present but SHOULD be set to zero.
           - RAIL_TASKBAR_MSG_TAB_ORDER:
             The window ID that the tab owned by WindowIdTab SHOULD be placed immediately before in its
             tab group. If this value is zero, the tab SHOULD be placed at the end of its tab group.
           - RAIL_TASKBAR_MSG_TAB_ACTIVE:
             The window ID corresponding to the tab that SHOULD be set active in the tab group owned by
             WindowIdTab.
           - RAIL_TASKBAR_MSG_TAB_PROPERTIES:
             The properties of the tab corresponding to WindowIdTab that are to be set. Valid properties
             are described by the [MSDN-STPFLAG] enumeration.
    enums:
      rail_taskbar_msg_tab:
        0x00000001:
          id: register
          doc: A remote application added a window to its tab group.
        0x00000002:
          id: unregister
          doc: A remote application removed a window from its tab group.
        0x00000003:
          id: order
          doc: |
            The position of a window in the tab group order for a remote application
            has changed. This message SHOULD be sent immediately after a
            RAIL_TASKBAR_MSG_TAB_REGISTER message.
        0x00000004:
          id: active
          doc: The active (selected) tab of a remote application tab group has changed.
        0x00000005:
          id: properties
          doc: The properties of a tab in a remote application tab group have changed.

  ts_rail_order_languageimeinfo:
    doc: |
      MS-RDPERP 2.2.2.10.1 Language Profile Information PDU (TS_RAIL_ORDER_LANGUAGEIMEINFO)
      The Language Profile Information PDU is used to send the current active language
      profile of the client to the server. It is only sent when both client and server
      support this capability (TS_RAIL_LEVEL_LANGUAGE_IME_SYNC_SUPPORTED). This PDU
      contains information about the current active language profile.
    seq:
      - id: profile_type
        type: u4
        enum: ts_profiletype
        doc: |
          An unsigned 4-byte integer that identifies the profile type of the language. The
          value SHOULD be either TF_PROFILETYPE_INPUTPROCESSOR (0x0001) or
          TF_PROFILETYPE_KEYBOARDLAYOUT (0x0002).
      - id: language_id
        type: u2
        doc: |
          An unsigned 2-byte integer. This is the language identifier that identifies both
          the language and the country/region. For a list of language identifiers, see
          [MSDN-MUI].
      - id: language_profile_clsid
        size: 16
        doc: |
          A globally unique identifier (section 2.2.2.10.1.1) that uniquely identifies the
          text service of the client. This field MUST be set to GUID_NULL if the ProfileType
          field is set to TF_PROFILETYPE_KEYBOARDLAYOUT (0x0002).
      - id: profile_guid
        size: 16
        doc: |
          A globally unique identifier (section 2.2.2.10.1.1) that uniquely identifies the
          language profile of the client. This field MUST be set to GUID_NULL if the
          ProfileType field is set to TF_PROFILETYPE_KEYBOARDLAYOUT (0x0002).
      - id: keyboard_layout
        type: u4
        doc: |
          An unsigned 4-byte integer. The active input locale identifier, also known as the
          "HKL" (for example, 0x00010409 identifies a "United States-Dvorak" keyboard layout,
          while 0x00020418 is a "Romanian (Programmers)" keyboard layout). For a list of input
          locale identifiers, see [MSFT-DIL].
    enums:
      ts_profiletype:
        0x00000001:
          id: inputprocessor
          doc: Indicates that the profile type is an input processor.
        0x00000002:
          id: keyboardlayout
          doc: Indicates that the profile type is a keyboard layout.

  ts_rail_order_compartmentinfo:
    doc: |
      MS-RDPERP 2.2.2.10.2 Compartment Status Information PDU (TS_RAIL_ORDER_COMPARTMENTINFO_BODY)
      The Compartment Status Information PDU is used to send the current input method editor
      (IME) status information. It is sent from a client to the server, or from a server to the
      client, but only when client and server both support this capability
      (TS_RAIL_LEVEL_LANGUAGE_IME_SYNC_SUPPORTED). This PDU is used to send the current compartment
      values of the client or server and is sent only if the current language profile type is
      TF_PROFILETYPE_INPUTPROCESSOR (0x0001).
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppUIManager::UpdateLanguageCompartment
      mstscax!WindowsRemoteAppLib::CRdpWinLanguageEvents::OnCompartmentChangePdu
    seq:
      - id: ime_state
        type: u4
        enum: ime_state
        doc: |
          A 32-bit, unsigned integer. Indicates the open or closed state of the IME.
      - id: ime_conv_mode
        type: u4
        enum: ime_cmode
        doc: |
          A 32-bit, unsigned integer. Indicates the IME conversion mode.
      - id: ime_sentence_mode
        type: u4
        enum: ime_smode
        doc: |
          An unsigned 4-byte integer that identifies the sentence mode of the IME.
      - id: kana_mode
        type: u4
        enum: kana_mode
        doc: |
          An unsigned 4-byte integer that identifies whether the input mode is Romaji or KANA
          for Japanese text processors. The value is 0x0000 for all non-Japanese text processors.
    enums:
      ime_state:
        0x00000000:
          id: closed
          doc: The IME state is closed.
        0x00000001:
          id: open
          doc: The IME state is open.
      ime_cmode:
        0x00000001:
          id: native
          doc: The input mode is native. If not set, the input mode is alphanumeric.
        0x00000002:
          id: katakana
          doc: The input mode is Katakana. If not set, the input mode is Hiragana.
        0x00000008:
          id: fullshape
          doc: The input mode is full-width. If not set, the input mode is half-width.
        0x00000010:
          id: roman
          doc: The input mode is Roman.
        0x00000020:
          id: charcode
          doc: Character-code input is in effect.
        0x00000040:
          id: hanjaconvert
          doc: Hanja conversion mode is in effect.
        0x00000080:
          id: softkbd
          doc: A soft (on-screen) keyboard is being used.
        0x00000100:
          id: noconversion
          doc: IME conversion is inactive (that is, the IME is closed).
        0x00000200:
          id: eudc
          doc: End-User Defined Character (EUDC) conversion mode is in effect.
        0x00000400:
          id: symbol
          doc: Symbol conversion mode is in effect.
        0x00000800:
          id: fixed
          doc: Fixed conversion mode is in effect.
      ime_smode:
        0x00000000:
          id: none
          doc: Indicates that the IME uses no information for sentence.
        0x00000001:
          id: pluralclause
          doc: Indicates that the IME uses plural clause information to carry out conversion processing.
        0x00000002:
          id: singleconvert
          doc: Indicates that the IME carries out conversion processing in single-character mode.
        0x00000004:
          id: automatic
          doc: Indicates that the IME carries conversion processing in automatic mode.
        0x00000008:
          id: phrasepredict
          doc: Indicates that the IME uses phrase information to predict the next character.
        0x00000010:
          id: conversation
          doc: Indicates that the IME uses conversation mode. This is useful for chat applications.
      kana_mode:
        0x00000000:
          id: off
          doc: Indicates that the KANA input mode is off.
        0x00000001:
          id: on
          doc: Indicates that the KANA input mode is activated.

  ts_rail_order_handshake_ex:
    doc: |
      MS-RDPERP 2.2.2.2.3 HandshakeEx PDU (TS_RAIL_ORDER_HANDSHAKE_EX)
      The HandshakeEx PDU (instead of the Handshake PDU) is sent from the server to
      the client if both the client and the server specified support for it by including
      the TS_RAIL_LEVEL_HANDSHAKE_EX_SUPPORTED flag in the Remote Programs Capability
      Set (section 2.2.1.1.1). This PDU is also sent by the server in an Enhanced
      RemoteApp session to signal that it is ready to begin Enhanced RemoteApp mode.
      The server sends the HandshakeEx PDU, and the client responds with the Handshake
      PDU (section 2.2.2.2.1).
      mstscax!RdpRemoteAppCore::ReceiveHandshake
      mstscax!RdpRemoteAppCore::OnHandshake
    seq:
      - id: build_number
        type: u4
        doc: |
          An unsigned 32-bit integer. The build or version of the sending party.
      - id: rail_handshake_flags
        type: u4
        doc: |
          unsigned 32-bit integer. Flags for setting up RAIL session parameters.
    enums:
      ts_rail_order_handshakeex_flags:
        0x00000001:
          id: hidef
          doc: |
            Indicates that Enhanced RemoteApp (section 1.3.3) is supported. This implies
            support for the Remote Desktop Protocol: Graphics Pipeline Extension
            ([MS-RDPEGFX] section 1.5), specifically the RDPGFX_MAP_SURFACE_TO_WINDOW_PDU
            ([MS-RDPEGFX] section 2.2.2.20) message.
        0x00000002:
          id: extended_spi_supported
          doc: |
            Indicates that additional system parameter flags are supported in the Client
            System Parameters Update PDU (section 2.2.2.4.1).
        0x00000004:
          id: snap_arrange_supported
          doc: |
            Indicates that the server supports the Client Window Snap PDU
            (section 2.2.2.7.5).

  ts_rail_order_zorder_sync:
    doc: |
      MS-RDPERP 2.2.2.11.1 Server Z-Order Sync Information PDU (TS_RAIL_ORDER_ZORDER_SYNC)
      The Z-Order Sync Information PDU is sent from the server to the client if the
      client has advertised support for Z-order sync in the Client Information PDU
      (section 2.2.2.2.2).
      mstscax!RdpRemoteAppCore::OnRailPdu
    seq:
      - id: window_id_marker
        type: u4
        doc: |
          An unsigned 32-bit integer. Indicates the ID of the marker window
          (section 3.3.1.3), which is used to manage the activation of RAIL windows as
          specified in section 3.2.5.2.9.2.

  ts_rail_order_cloak:
    doc: |
      MS-RDPERP 2.2.2.12.1 Window Cloak State Change PDU (TS_RAIL_ORDER_CLOAK)
      Windows are either in a cloaked or uncloaked state. Changes in the cloak state of a
      RAIL window on the client, or a remoted window on the server, are communicated by
      Window Cloak State Change PDU.
      The client sends the Window Cloak State Change PDU if both the client and server
      support syncing per-window cloak state (indicated by the
      TS_RAIL_LEVEL_WINDOW_CLOAKING_SUPPORTED flag in the Remote Programs Capability Set
      (section 2.2.1.1.1)). The server uses this information to sync the cloak state to
      the associated window on the server.
      The server sends the Window Cloak State Change PDU if the client is capable of
      processing this PDU (indicated by the TS_RAIL_CLIENTSTATUS_BIDIRECTIONAL_CLOAK_SUPPORTED
      flag in the Client Information PDU (section 2.2.2.2.2)). The client uses this information
      to sync the cloak state to the associated window on the client.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppWindow::UpdateCloakState
      mstscax!CloakSyncComponent::OnWindowCloakedRemotely
      mstscax!CloakSyncComponent::OnWindowUncloakedRemotely
    seq:
      - id: window_id
        type: u4
        doc: |
          unsigned 32-bit integer. The ID of the window that is to be cloaked or uncloaked.
      - id: cloaked
        type: u1
        doc: |
          An unsigned 8-bit integer that indicates whether the window SHOULD be cloaked or
          uncloaked.
           - 0x00: The window SHOULD be uncloaked.
           - 0x01: The window SHOULD be cloaked.

  ts_rail_order_power_display_request:
    doc: |
      MS-RDPERP 2.2.2.13.1 Power Display Request PDU (TS_RAIL_ORDER_POWER_DISPLAY_REQUEST)
      The Power Display Request PDU is sent from the server to the client if the client has
      advertised support for display-required power request sync in the Client Information
      PDU (section 2.2.2.2.2).
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpWinDesktopRemoteAppUIManager::SetPowerDisplayRequest
    seq:
      - id: active
        type: u4
        doc: |
          A 32-bit, unsigned integer. Indicates the active or inactive state of the
          display-required power request.
           - 0x00000000:
             The display-required power request state is inactive, which means the display
             of the device SHOULD enter the powered-off state if there is no user input for
             an extended period.
           - 0x00000001:
             The display-required power request state is active, which means the display of
             the device should remain in the powered-on state even if there is no user input
             for an extended period.

  ts_rail_order_snap_arrange:
    doc: |
      MS-RDPERP 2.2.2.7.5 Client Window Snap PDU (TS_RAIL_ORDER_SNAP_ARRANGE)
      The Client Window Snap PDU packet is sent from the client to the server when a local
      window is repositioned by the local window manager due to the use of a window arrangement
      feature, such as Snap or Snap Assist. The client communicates the new position of the local
      window to the server by sending this packet. The server uses this information to reposition
      the corresponding window using an equivalent window arrangement feature. This packet is only
      sent if the server has advertised support for the Window Snap feature in the HandshakeEx PDU
      (section 2.2.2.2.3); otherwise, a Client Window Move PDU (section 2.2.2.7.4) is sent instead.
      Window positions sent using this packet SHOULD include hit-testable margins
      (see section 3.2.5.1.6).
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server corresponding to the local
          window that was snapped.
      - id: left
        type: s2
        doc: |
          A signed 16-bit integer. The x-coordinate of the top-left corner of the window's new position.
      - id: top
        type: s2
        doc: |
          A signed 16-bit integer. The y-coordinate of the top-left corner of the window's new position.
      - id: right
        type: s2
        doc: |
          A signed 16-bit integer. The x-coordinate of the bottom-right corner of the window's new position.
      - id: bottom
        type: s2
        doc: |
          A signed 16-bit integer. The y-coordinate of the bottom-right corner of the window's new position.

  ts_rail_order_get_appid_resp_ex:
    doc: |
      MS-RDPERP 2.2.2.8.2 Server Get Application ID Extended Response PDU (TS_RAIL_ORDER_GET_APPID_RESP_EX)
      The Server Get Application ID Extended Response PDU is sent from a server to a client as a
      response to a Client Get Application ID PDU (section 2.2.2.6.5).
      This PDU specifies the Application ID that the specified window SHOULD<19> have on the client.
      The client MAY ignore this PDU.
      mstscax!RdpRemoteAppCore::OnRailPdu
      mstscax!RdpRemoteAppCore::UpdateProcessInfo
      mstscax!RdpWinDesktopRemoteAppWindow::UpdateProcessInfo
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer specifying the ID of the associated window on the server whose
          Application ID is being sent to the client.
      - id: application_id
        type: strz
        size: 520
        encoding: UTF-16LE
        doc: |
          A null-terminated string of Unicode characters specifying the Application ID that the Client
          SHOULD associate with its window, if it supports using the Application ID for identifying and
          grouping windows.
      - id: process_id
        type: u4
        doc: |
          An unsigned 32-bit integer specifying the ID of the process associated with the window on the
          server whose Application ID is being sent to the client.
      - id: process_image_name
        type: strz
        size: 520
        encoding: UTF-16LE
        doc: |
          A null-terminated string of Unicode characters specifying the image name of the process
          associated with the window on the server whose Application ID is being sent to the client.
