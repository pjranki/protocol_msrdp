meta:
  id: echo_pdu
  endian: le

doc: |
  MS-RDPEECO 2.2 Message Syntax
  Echo service for RDP clients/servers on dynamic channel "ECHO"
  mstscax!CEcho::OnNewChannelConnection
  mstscax!CEchoChannelCallback::OnDataReceived

seq:
  - id: message
    size-eos: true
    doc: |
      A variable-length array of bytes containing a message.
