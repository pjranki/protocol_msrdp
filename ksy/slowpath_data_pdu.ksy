meta:
  id: slowpath_data_pdu
  endian: le
  imports:
    - ts_rectangle16
    - ts_drawing_order
    - ts_update_bitmap_data
    - ts_update_palette_data
    - ts_update_sync
    - ts_surfcmd
    - ts_pointerposattribute
    - ts_colorpointerattribute
    - ts_cachedpointerattribute
    - ts_pointerattribute
    - ts_monitor_layout_pdu

doc: |
  MS-RDPBCGR 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
  The TS_SHAREDATAHEADER header is a T.128 header ([T128] section 8.3) that MUST be
  present in all Data PDUs
  mstscax!CCO::OnPacketReceived

seq:
  - id: share_id
    -orig-id: shareId
    type: u4
    doc: |
      A 32-bit, unsigned integer. Share identifier for the packet
      (see [T128] section 8.4.2 for more information about share IDs).
  - id: pad
    -orig-id: pad1
    type: u1
    doc: |
      An 8-bit, unsigned integer. Padding. Values in this field MUST be ignored.
  - id: stream_id
    -orig-id: streamId
    type: u1
    enum: stream
    doc: |
      An 8-bit, unsigned integer. The stream identifier for the packet.
  - id: uncompressed_length
    -orig-id: uncompressedLength
    type: u2
    doc: |
      A 16-bit, unsigned integer. The uncompressed length of the packet in bytes.
  - id: pdu_type_2
    -orig-id: pduType2
    type: u1
    doc: |
      An 8-bit, unsigned integer. The type of Data PDU.
  - id: compressed_type
    type: u1
    doc: |
      An 8-bit, unsigned integer. The compression type and flags specifying the data
      following the Share Data Header (section 2.2.8.1.1.1.2).
  - id: compressed_length
    -orig-id: compressedLength
    type: u2
    doc: |
      A 16-bit, unsigned integer. The compressed length of the packet in bytes.
  - id: data
    size-eos: true
    process: msrdp_slowpath_data(_root)
    type: slowpath_data(pdu_type_2)

instances:
  is_compressed:
    value: (compressed_type & 0x20) != 0
  compression_type:
    value: compressed_type & 0xf
  compression_packed_at_front:
    value: (compressed_type & 0x40) != 0
  compression_packed_flushed:
    value: (compressed_type & 0x80) != 0

enums:
  stream:
    0x00:
      id: undefined
      doc: Undefined stream priority. This value might be used in the Server Synchronize PDU (section 2.2.1.19) due to a server-side RDP bug. It MUST NOT be used in conjunction with any other PDUs.
    0x01:
      id: low
      doc: Low-priority stream.
    0x02:
      id: med
      doc: Medium-priority stream.
    0x04:
      id: hi
      doc: High-priority stream.
  pdutype2:
    0x02:
      id: update
      doc: Graphics Update PDU (section 2.2.9.1.1.3)
    0x14:
      id: control
      doc: Control PDU (section 2.2.1.15.1)
    0x1B:
      id: pointer
      doc: Pointer Update PDU (section 2.2.9.1.1.4)
    0x1C:
      id: input
      doc: Input Event PDU (section 2.2.8.1.1.3)
    0x1F:
      id: synchronize
      doc: Synchronize PDU (section 2.2.1.14.1)
    0x21:
      id: refresh_rect
      doc: Refresh Rect PDU (section 2.2.11.2.1)
    0x22:
      id: play_sound
      doc: Play Sound PDU (section 2.2.9.1.1.5.1)
    0x23:
      id: suppress_output
      doc: Suppress Output PDU (section 2.2.11.3.1)
    0x24:
      id: shutdown_request
      doc: Shutdown Request PDU (section 2.2.2.1.1)
    0x25:
      id: shutdown_denied
      doc: Shutdown Request Denied PDU (section 2.2.2.2.1)
    0x26:
      id: save_session_info
      doc: Save Session Info PDU (section 2.2.10.1.1)
    0x27:
      id: fontlist
      doc: Font List PDU (section 2.2.1.18.1)
    0x28:
      id: fontmap
      doc: Font Map PDU (section 2.2.1.22.1)
    0x29:
      id: set_keyboard_indicators
      doc: Set Keyboard Indicators PDU (section 2.2.8.2.1.1)
    0x2B:
      id: bitmapcache_persistent_list
      doc: Persistent Key List PDU (section 2.2.1.17.1)
    0x2C:
      id: bitmapcache_error_pdu
      doc: Bitmap Cache Error PDU ([MS-RDPEGDI] section 2.2.2.3.1)
    0x2D:
      id: set_keyboard_ime_status
      doc: Set Keyboard IME Status PDU (section 2.2.8.2.2.1)
    0x2E:
      id: offscrecache_error_pdu
      doc: Offscreen Bitmap Cache Error PDU ([MS-RDPEGDI] section 2.2.2.3.2)
    0x2F:
      id: set_error_info_pdu
      doc: Set Error Info PDU (section 2.2.5.1.1)
    0x30:
      id: drawninegrid_error_pdu
      doc: DrawNineGrid Cache Error PDU ([MS-RDPEGDI] section 2.2.2.3.3)
    0x31:
      id: drawgdiplus_error_pdu
      doc: GDI+ Error PDU ([MS-RDPEGDI] section 2.2.2.3.4)
    0x32:
      id: arc_status_pdu
      doc: Auto-Reconnect Status PDU (section 2.2.4.1.1)
    0x35:
      id: input_mode_change_pdu
      doc: UNDOCUMENTED
    0x36:
      id: status_info_pdu
      doc: Status Info PDU (section 2.2.5.2)
    0x37:
      id: monitor_layout_pdu
      doc: Monitor Layout PDU (section 2.2.12.1)
    0xff:
      id: raw

types:
  slowpath_data:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
      The TS_SHAREDATAHEADER header is a T.128 header ([T128] section 8.3) that MUST be
      present in all Data PDUs
      mstscax!CCO::OnPacketReceived
    params:
      - id: pdu_type_2
        type: u1
        enum: pdutype2
    seq:
      - id: data
        type:
          switch-on: pdu_type_2
          -name: type
          cases:
            'pdutype2::raw.to_i': slowpath_data_raw
            'pdutype2::update.to_i': ts_graphics_update
            'pdutype2::control.to_i': ts_control_pdu
            'pdutype2::pointer.to_i': ts_pointer_pdu
            'pdutype2::input.to_i': ts_input_pdu_data
            'pdutype2::synchronize.to_i': ts_synchronize_pdu
            'pdutype2::refresh_rect.to_i': ts_refresh_rect_pdu
            'pdutype2::play_sound.to_i': ts_play_sound_pdu_data
            'pdutype2::suppress_output.to_i': ts_suppress_output_pdu
            'pdutype2::shutdown_request.to_i': ts_shutdown_req_pdu
            'pdutype2::shutdown_denied.to_i': ts_shutdown_denied_pdu
            'pdutype2::save_session_info.to_i': ts_save_session_info_pdu_data
            'pdutype2::fontlist.to_i': ts_font_list_pdu
            'pdutype2::fontmap.to_i': ts_font_map_pdu
            'pdutype2::set_keyboard_indicators.to_i': ts_set_keyboard_indicators_pdu
            'pdutype2::bitmapcache_persistent_list.to_i': ts_bitmapcache_persistent_list_pdu
            'pdutype2::bitmapcache_error_pdu.to_i': ts_bitmap_cache_error_pdu
            'pdutype2::set_keyboard_ime_status.to_i': ts_set_keyboard_ime_status_pdu
            'pdutype2::offscrecache_error_pdu.to_i': ts_offscrcache_error_pdu
            'pdutype2::set_error_info_pdu.to_i': ts_set_error_info_pdu
            'pdutype2::drawninegrid_error_pdu.to_i': ts_drawninegrid_error_pdu
            'pdutype2::drawgdiplus_error_pdu.to_i': ts_drawgdiplus_error_pdu
            'pdutype2::arc_status_pdu.to_i': ts_autoreconnect_status_pdu
            'pdutype2::input_mode_change_pdu.to_i': ts_input_mode_change_pdu
            'pdutype2::status_info_pdu.to_i': ts_status_info_pdu
            'pdutype2::monitor_layout_pdu.to_i': ts_monitor_layout_pdu

  slowpath_data_raw:
    doc: |
      Placeholder for unhandled slowpath data PDUs.
    seq:
      - id: data
        size-eos: true

  ts_graphics_update:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.3.1 Slow-Path Graphics Update (TS_GRAPHICS_UPDATE)
      The TS_GRAPHICS_UPDATE structure is used to describe the type and encapsulate
      the data for a slow-path graphics update sent from server to client.<36> All
      slow-path graphic updates conform to this basic structure
      (section 2.2.9.1.1.3.1.1).
      mstscax!CCO::OnSlowPathUpdateReceived
    seq:
      - id: update_type
        -orig-id: updateType
        type: u2
        enum: updatetype
        doc: |
          A 16-bit, unsigned integer. Type of the graphics update.
      - id: data
        type:
          switch-on: update_type
          -name: type
          cases:
            'updatetype::orders': ts_update_orders
            'updatetype::bitmap': ts_update_bitmap
            'updatetype::palette': ts_update_palette
            'updatetype::synchronize': ts_update_sync
            'updatetype::surfcmds': ts_surfcmds
    enums:
      updatetype:
        0x0000:
          id: orders
          doc: Indicates an Orders Update ([MS-RDPEGDI] section 2.2.2.1).
        0x0001:
          id: bitmap
          doc: Indicates a Bitmap Graphics Update (section 2.2.9.1.1.3.1.2).
        0x0002:
          id: palette
          doc: Indicates a Palette Update (section 2.2.9.1.1.3.1.1).
        0x0003:
          id: synchronize
          doc: Indicates a Synchronize Update (section 2.2.9.1.1.3.1.3).
        0x0004:
          id: surfcmds
          doc: Indicates a Surface Commands Update (section 2.2.9.1.2.1.10).

  ts_update_orders:
    doc: |
      MS-RDPEGDI 2.2.2.1 Orders Update (TS_UPDATE_ORDERS_PDU_DATA)
      The TS_UPDATE_ORDERS_PDU_DATA structure contains primary, secondary, and alternate
      secondary drawing orders aligned on byte boundaries. This structure conforms to the
      layout of a Slow Path Graphics Update (see [MS-RDPBCGR] section 2.2.9.1.1.3.1) and
      is encapsulated within a Graphics Update PDU
      (see [MS-RDPBCGR] section 2.2.9.1.1.3.1.1).
      mstscax!CTSCoreGraphics::ProcessOrders
      mstscax!CUH::ProcessOrders
    seq:
      - id: number_orders
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of Drawing Order (section 2.2.2.1.1)
          structures contained in the orderData field.
      - id: orders
        type: 'ts_drawing_order(orders.size == 0 ? 1 : orders[orders.size - 1].primary_order_type)'
        repeat: expr
        repeat-expr: number_orders
        doc: |
          A variable-sized array of Drawing Order (section 2.2.2.1.1) structures packed on
          byte boundaries. Each structure contains a primary, secondary, or alternate secondary
          drawing order. The controlFlags field of the Drawing Order identifies the type of
          drawing order.

  ts_update_bitmap:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.3.1.2 Bitmap Update (TS_UPDATE_BITMAP)
      The TS_UPDATE_BITMAP structure contains one or more rectangular clippings taken from
      the server-side screen frame buffer ([T128] section 8.17).
      mstscax!CTSCoreGraphics::ProcessBitmap
    seq:
      - id: bitmap_data
        -orig-id: bitmapData
        type: ts_update_bitmap_data
        doc: |
          The actual bitmap update data, as specified in section 2.2.9.1.1.3.1.2.1.

  ts_update_palette:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.3.1.1 Palette Update (TS_UPDATE_PALETTE)
      The TS_UPDATE_PALETTE structure contains global palette information that covers the entire
      session's palette ([T128] section 8.18.6). Only 256-color palettes are sent in this update.
      Palletized color is supported only in RDP 4.0, 5.0, 5.1, 5.2, 6.0, 6.1, 7.0, and 7.1.
      mstscax!CTSCoreGraphics::ProcessPalette
    seq:
      - id: palette_data
        type: ts_update_palette_data
        doc: |
          The actual palette update data, as specified in section 2.2.9.1.1.3.1.1.1.

  ts_surfcmds:
    doc: |
      Surface Commands Update (TS_SURFCMDS)
      The TS_SURFCMDS structure encapsulates one or more Surface Command
      (section 2.2.9.1.2.1.10.1) structures.
      mstscax!CTSCoreGraphics::ProcessSurfaceCommands
    seq:
      - id: surface_commands
        type: ts_surfcmd
        repeat: eos
        doc: |
          An array of Surface Command (section 2.2.9.1.2.1.10.1) structures
          containing a collection of commands to be processed by the client.

  ts_control_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.15.1 Control PDU Data (TS_CONTROL_PDU)
      The TS_CONTROL_PDU structure is a standard T.128 Synchronize PDU ([T128] section 8.12).
    seq:
      - id: action
        type: u2
        enum: ctrlaction
        doc: 16-bit, unsigned integer. The action code.
      - id: grant_id
        -orig-id: grantId
        type: u2
        doc: A 16-bit, unsigned integer. The grant identifier.
      - id: control_id
        -orig-id: controlId
        type: u4
        doc: A 32-bit, unsigned integer. The control identifier.
    enums:
      ctrlaction:
        0x0001:
          id: request_control
          doc: Request control
        0x0002:
          id: granted_control
          doc: Granted control
        0x0003:
          id: detach
          doc: Detach
        0x0004:
          id: cooperate
          doc: Cooperate

  ts_pointer_pdu:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.4 Server Pointer Update PDU (TS_POINTER_PDU)
      The Pointer Update PDU is sent from server to client and is used to convey pointer
      information, including pointers' bitmap images, use of system or hidden pointers,
      use of cached cursors and position updates.
      mstscax!CCM::CM_SlowPathPDU
    seq:
      - id: message_type
        -orig-id: messageType
        type: u2
        enum: ts_ptrmsgtype
        doc: |
          A 16-bit, unsigned integer. Type of pointer update.
      - id: pad
        -orig-id: pad2Octets
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: data
        type:
          switch-on: message_type
          -name: type
          cases:
            'ts_ptrmsgtype::system': ts_systempointerattribute
            'ts_ptrmsgtype::position': ts_pointerposattribute
            'ts_ptrmsgtype::color': ts_colorpointerattribute
            'ts_ptrmsgtype::cached': ts_cachedpointerattribute
            'ts_ptrmsgtype::pointer': ts_pointerattribute
    enums:
      ts_ptrmsgtype:
        0x0001:
          id: system
          doc: Indicates a System Pointer Update (section 2.2.9.1.1.4.3).
        0x0003:
          id: position
          doc: Indicates a Pointer Position Update (section 2.2.9.1.1.4.2).
        0x0006:
          id: color
          doc: Indicates a Color Pointer Update (section 2.2.9.1.1.4.4).
        0x0007:
          id: cached
          doc: Indicates a Cached Pointer Update (section 2.2.9.1.1.4.6).
        0x0008:
          id: pointer
          doc: Indicates a New Pointer Update (section 2.2.9.1.1.4.5).

  ts_systempointerattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.4.3 System Pointer Update (TS_SYSTEMPOINTERATTRIBUTE)
      The TS_SYSTEMPOINTERATTRIBUTE structure is used to hide the pointer or to set its
      shape to the operating system default ([T128] section 8.14.1).
      mstscax!CCM::CM_SlowPathPDU
      mstscax!CCM::CM_NullSystemPointerPDU
      mstscax!CCM::CM_DefaultSystemPointerPDU
    seq:
      - id: system_pointer_type
        -orig-id: systemPointerType
        type: u4
        enum: sysptr
        doc: |
          A 32-bit, unsigned integer. The type of system pointer.
    enums:
      sysptr:
        0x00000000:
          id: null_
          doc: The hidden pointer.
        0x00007F00:
          id: default
          doc: The default system pointer.

  ts_input_pdu_data:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1 Client Input Event PDU Data (TS_INPUT_PDU_DATA)
      The TS_INPUT_PDU_DATA structure contains a collection of Slow-Path Input Events
      (section 2.2.8.1.1.3.1.1) generated by the client and intended to be processed
      by the server.
    seq:
      - id: num_events
        -orig-id: numEvents
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of Slow-Path Input Events packed together
          in the slowPathInputEvents field.
      - id: pad_2_octets
        -orig-id: pad2Octets
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: input_event_array
        -orig-id: slowPathInputEvents
        type: ts_input_event
        repeat: expr
        repeat-expr: num_events
        doc: |
          A collection of Slow-Path Input Events to be processed by the server.
          The number of events present in this array is given by the numEvents field.

  ts_input_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1 Slow-Path Input Event (TS_INPUT_EVENT)
      The TS_INPUT_EVENT structure is used to wrap event-specific information for all
      slow-path input events.
    seq:
      - id: event_time
        -orig-id: eventTime
        type: u4
        doc: |
          A 32-bit, unsigned integer. The 32-bit time stamp for the input event.
          This value is ignored by the server.
      - id: code
        -orig-id: messageType
        type: u2
        enum: input_event
        doc: A 16-bit, unsigned integer. The input event type.
      - id: event
        -orig-id: slowPathInputData
        type:
          switch-on: code
          -name: type
          cases:
            'input_event::sync': ts_sync_event
            'input_event::unused': ts_unused_event
            'input_event::scancode': ts_keyboard_event
            'input_event::unicode': ts_unicode_keyboard_event
            'input_event::mouse': ts_pointer_event
            'input_event::mousex': ts_pointerx_event
    enums:
      input_event:
        0x0000:
          id: sync
          doc: Indicates a Synchronize Event (section 2.2.8.1.1.3.1.1.5).
        0x0002:
          id: unused
          doc: Indicates an Unused Event (section 2.2.8.1.1.3.1.1.6).
        0x0004:
          id: scancode
          doc: Indicates a Keyboard Event (section 2.2.8.1.1.3.1.1.1).
        0x0005:
          id: unicode
          doc: Indicates a Unicode Keyboard Event (section 2.2.8.1.1.3.1.1.2).
        0x8001:
          id: mouse
          doc: Indicates a Mouse Event (section 2.2.8.1.1.3.1.1.3).
        0x8002:
          id: mousex
          doc: Indicates an Extended Mouse Event (section 2.2.8.1.1.3.1.1.4).

  ts_sync_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.5 Synchronize Event (TS_SYNC_EVENT)
      The TS_SYNC_EVENT structure is a standard T.128 Input Synchronize Event
      ([T128] section 8.18.6). In RDP this event is used to synchronize the values of
      the toggle keys (for example, Caps Lock) and to reset the server key state to all
      keys up. This event is sent by the client to communicate the state of the toggle
      keys. The synchronize event SHOULD be followed by key-down events to communicate
      which keyboard and mouse keys are down.
    seq:
      - id: pad_2_octets
        -orig-id: pad2Octets
        type: u2
        doc: A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: toggle_flags
        -orig-id: toggleFlags
        type: u4
        doc: |
          A 32-bit, unsigned integer. Flags indicating the "on" status of the keyboard
          toggle keys.
    enums:
      ts_sync:
        0x00000001:
          id: scroll_lock
          doc: Indicates that the Scroll Lock indicator light SHOULD be on.
        0x00000002:
          id: num_lock
          doc: Indicates that the Num Lock indicator light SHOULD be on.
        0x00000004:
          id: caps_lock
          doc: Indicates that the Caps Lock indicator light SHOULD be on.
        0x00000008:
          id: kana_lock
          doc: Indicates that the Kana Lock indicator light SHOULD be on.

  ts_unused_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.6 Unused Event (TS_UNUSED_EVENT)
      The TS_UNUSED_EVENT structure is sent by RDP 4.0, 5.0, 5.1, 5.2, 6.0, 6.1, 7.0,
      and 7.1 clients if the server erroneously did not indicate support for scancodes
      in the Input Capability Set (TS_INPUT_CAPABILITYSET) (section 2.2.7.1.6).
    seq:
      - id: pad_4_octets
        -orig-id: pad4Octets
        type: u4
        doc: |
          A 32-bit, unsigned integer. This field is padding, and the values in this
          field MUST be ignored.
      - id: pad_2_octets
        -orig-id: pad2Octets
        type: u2
        doc: |
           A 32-bit, unsigned integer. This field is padding, and the values in this
           field MUST be ignored.

  ts_keyboard_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.1 Keyboard Event (TS_KEYBOARD_EVENT)
      The TS_KEYBOARD_EVENT structure is a standard T.128 Keyboard Event ([T128] section 8.18.2).
      RDP keyboard input is restricted to keyboard scancodes, unlike the code-point or virtual
      codes supported in T.128 (a scancode is an 8-bit value specifying a key location on the
      keyboard). The server accepts a scancode value and translates it into the correct character
      depending on the language locale and keyboard layout used in the session.
    seq:
      - id: keyboard_flags
        -orig-id: keyboardFlags
        type: u2
        doc: |
          A 16-bit, unsigned integer. The flags describing the keyboard event.
      - id: key_code
        -orig-id: keyCode
        type: u2
        doc: |
          A 16-bit, unsigned integer. The scancode of the key which triggered the event.
      - id: pad_2_octets
        -orig-id: pad2Octets
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
    enums:
      kbdflags:
        0x0100:
          id: extended
          doc: |
            Indicates that the keystroke message contains an extended scancode.
            For enhanced 101-key and 102-key keyboards, extended keys include the right ALT and
            right CTRL keys on the main section of the keyboard; the INS, DEL, HOME, END, PAGE UP,
            PAGE DOWN and ARROW keys in the clusters to the left of the numeric keypad; and the
            Divide ("/") and ENTER keys in the numeric keypad.
        0x0200:
          id: extended1
          doc: |
            Used to send keyboard events triggered by the PAUSE key.
            A PAUSE key press and release MUST be sent as the following sequence of
            keyboard events:
                - CTRL (0x1D) DOWN
                - NUMLOCK (0x45) DOWN
                - CTRL (0x1D) UP
                - NUMLOCK (0x45) UP
            The CTRL DOWN and CTRL UP events MUST both include the KBDFLAGS_EXTENDED1 flag.
        0x4000:
          id: down
          doc: |
            Indicates that the key was down prior to this event.
        0x8000:
          id: release
          doc: |
            The absence of this flag indicates a key-down event, while its presence indicates
            a key-release event.

  ts_unicode_keyboard_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.2 Unicode Keyboard Event (TS_UNICODE_KEYBOARD_EVENT)
      The TS_UNICODE_KEYBOARD_EVENT structure is used to transmit a Unicode input code, as
      opposed to a keyboard scancode. Support for the Unicode Keyboard Event is advertised in
      the Input Capability Set (section 2.2.7.1.6).
    seq:
      - id: keyboard_flags
        -orig-id: keyboardFlags
        type: u2
        doc: |
          A 16-bit unsigned integer. The flags describing the Unicode keyboard event.
      - id: unicode_code
        -orig-id: unicodeCode
        type: u2
        doc: |
          A 16-bit unsigned integer. The Unicode character input code.
      - id: pad_2_octets
        -orig-id: pad2Octets
        type: u2
        doc: |
          A 16-bit unsigned integer. Padding. Values in this field MUST be ignored.
    enums:
      kbdflags:
        0x8000:
          id: release
          doc: |
            The absence of this flag indicates a key-down event, while its presence indicates
            a key-release event.

  ts_pointer_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.3 Mouse Event (TS_POINTER_EVENT)
      The TS_POINTER_EVENT structure is a standard T.128 Keyboard Event ([T128] section 8.18.1).
      RDP adds flags to deal with wheel mice and extended mouse buttons.
    seq:
      - id: wheel_rotation
        type: u1
        doc: |
          WheelRotationMask (0x01FF)
          The bit field describing the number of rotation units the mouse wheel
          was rotated. The value is negative if the PTRFLAGS_WHEEL_NEGATIVE flag
          is set.
      - id: down
        type: b1
        doc: |
          PTRFLAGS_DOWN (0x8000)
          Indicates that a click event has occurred at the position specified by
          the xPos and yPos fields. The button flags indicate which button has been
          clicked and at least one of these flags MUST be set.
      - id: button3
        type: b1
        doc: |
          PTRFLAGS_BUTTON3 (0x4000)
          Mouse button 3 (middle button or wheel) was clicked or released. If the
          PTRFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.
      - id: button2
        type: b1
        doc: |
          PTRFLAGS_BUTTON2 (0x2000)
          Mouse button 2 (right button) was clicked or released. If the PTRFLAGS_DOWN
          flag is set, then the button was clicked, otherwise it was released.
      - id: button1
        type: b1
        doc: |
          PTRFLAGS_BUTTON1 (0x1000)
          Mouse button 1 (left button) was clicked or released. If the PTRFLAGS_DOWN
          flag is set, then the button was clicked, otherwise it was released.
      - id: move
        type: b1
        doc: |
          PTRFLAGS_MOVE (0x0800)
          Indicates that the mouse position MUST be updated to the location
          specified by the xPos and yPos fields.
      - id: hwheel
        type: b1
        doc: |
          PTRFLAGS_HWHEEL (0x0400)
          The event is a horizontal mouse wheel rotation. The only valid flags in
          a horizontal wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored. This flag MUST
          NOT be sent to a server that does not indicate support for horizontal
          mouse wheel events in the Input Capability Set (section 2.2.7.1.6).
      - id: wheel
        type: b1
        doc: |
          PTRFLAGS_WHEEL (0x0200)
          The event is a vertical mouse wheel rotation. The only valid flags in
          a vertical wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored.
      - id: wheel_negative
        type: b1
        doc: |
          PTRFLAGS_WHEEL (0x0100)
          The event is a vertical mouse wheel rotation. The only valid flags in
          a vertical wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored.
      - id: x_pos
        type: u2
        doc: |
          A 16-bit, unsigned integer. The x-coordinate of the pointer relative to
          the top-left corner of the server's desktop. This field SHOULD be ignored
          by the server if either the PTRFLAGS_WHEEL (0x0200) or the PTRFLAGS_HWHEEL
          (0x0400) flag is specified in the pointerFlags field.
      - id: y_pos
        type: u2
        doc: |
          A 16-bit, unsigned integer. The y-coordinate of the pointer relative to
          the top-left corner of the server's desktop. This field SHOULD be ignored
          by the server if either the PTRFLAGS_WHEEL (0x0200) or the PTRFLAGS_HWHEEL
          (0x0400) flag is specified in the pointerFlags field.

  ts_pointerx_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.1.3.1.1.4 Extended Mouse Event (TS_POINTERX_EVENT)
      The TS_POINTERX_EVENT structure has the same format as the TS_POINTER_EVENT
      (section 2.2.8.1.1.3.1.1.3). The fields and possible field values are all the same,
      except for the pointerFlags field. Support for the Extended Mouse Event is advertised
      in the Input Capability Set (section 2.2.7.1.6).
    seq:
      - id: pad1
        type: b6
      - id: button2
        type: b1
        doc: |
          PTRXFLAGS_BUTTON2 (0x0002)
          Extended mouse button 2 (also referred to as button 5) was clicked or released.
          If the PTRXFLAGS_DOWN flag is set, the button was clicked; otherwise, it was
          released.
      - id: button1
        type: b1
        doc: |
          PTRXFLAGS_BUTTON1 (0x0001)
          Extended mouse button 1 (also referred to as button 4) was clicked or released.
          If the PTRXFLAGS_DOWN flag is set, the button was clicked; otherwise, it was
          released.
      - id: down
        type: b1
        doc: |
          PTRXFLAGS_DOWN (0x8000)
          Indicates that a click event has occurred at the position specified by
          the xPos and yPos fields. The button flags indicate which button has been
          clicked and at least one of these flags MUST be set.
      - id: pad2
        type: b7
      - id: x_pos
        type: u2
        doc: |
          A 16-bit unsigned integer. The x-coordinate of the pointer.
      - id: y_pos
        type: u2
        doc: |
          A 16-bit unsigned integer. The y-coordinate of the pointer.

  ts_synchronize_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.14.1 Synchronize PDU Data (TS_SYNCHRONIZE_PDU)
      The TS_SYNCHRONIZE_PDU structure is a standard T.128 Synchronize PDU
      ([T128] section 8.6.1).
    seq:
      - id: message_type
        -orig-id: messageType
        type: u2
        doc: |
          A 16-bit, unsigned integer. The message type.
          This field MUST be set to SYNCMSGTYPE_SYNC (1).
      - id: target_user
        -orig-id: targetUser
        type: u2
        doc: |
          A 16-bit, unsigned integer. The MCS channel ID of the target user.

  ts_refresh_rect_pdu:
    doc: |
      MS-RDPBCGR 2.2.11.2.1 Refresh Rect PDU Data (TS_REFRESH_RECT_PDU)
      The TS_REFRESH_RECT_PDU structure contains the contents of the Refresh Rect PDU,
      which is a Share Data Header (section 2.2.8.1.1.1.2) and two fields.
    seq:
      - id: number_of_areas
        -orig-id: numberOfAreas
        type: u8
        doc: |
          An 8-bit, unsigned integer. The number of Inclusive Rectangle (section 2.2.11.1)
          structures in the areasToRefresh field.
      - id: pad_3_octects
        -orig-id: pad_3_Octects
        size: 3
        doc: |
          A 3-element array of 8-bit, unsigned integer values. Padding.
          Values in this field MUST be ignored.
      - id: areas_to_refresh
        -orig-id: areasToRefresh
        type: ts_rectangle16
        repeat: expr
        repeat-expr: number_of_areas
        doc: |
          An array of TS_RECTANGLE16 structures (variable number of bytes). Array of screen
          area Inclusive Rectangles to redraw. The number of rectangles is given by the
          numberOfAreas field.

  ts_play_sound_pdu_data:
    doc: |
      MS-RDPBCGR 2.2.9.1.1.5.1 Play Sound PDU Data (TS_PLAY_SOUND_PDU_DATA)
      The TS_PLAY_SOUND_PDU_DATA structure contains the contents of the Play Sound PDU,
      which is a Share Data Header (section 2.2.8.1.1.1.2) and two fields.
      mstscax!CSP::SP_OnPlaySoundPDU
      mstscax!CSP::SPPlaySound
      kernel32!Beep
    seq:
      - id: duration
        type: u4
        doc: |
          A 32-bit, unsigned integer. Duration of the beep the client MUST play.
          Duration in milliseconds.
      - id: frequency
        type: u4
        doc: |
          A 32-bit, unsigned integer. Frequency of the beep the client MUST play.
          Frequency in Hz in the range of 37 Hz to 32,767 Hz

  ts_suppress_output_pdu:
    doc: |
      MS-RDPBCGR 2.2.11.3.1 Suppress Output PDU Data (TS_SUPPRESS_OUTPUT_PDU)
      The TS_SUPPRESS_OUTPUT_PDU structure contains the contents of the Suppress Output PDU,
      which is a Share Data Header (section 2.2.8.1.1.1.2) and two fields.
    seq:
      - id: allow_display_updates
        -orig-id: allowDisplayUpdates
        type: u1
        doc: |
          An 8-bit, unsigned integer. Indicates whether the client wants to receive display
          updates from the server.
            - SUPPRESS_DISPLAY_UPDATES  (0x00): Turn off display updates from the server.
            - ALLOW_DISPLAY_UPDATES     (0x01): Turn on display updates from the server.
      - id: pad_3_octects
        -orig-id: pad3Octets
        size: 3
        doc: |
          A 3-element array of 8-bit, unsigned integer values. Padding.
          Values in this field MUST be ignored.
      - id: desktop_rect
        -orig-id: desktopRect
        type: ts_rectangle16
        if: allow_display_updates != 0
        doc: |
          An Inclusive Rectangle (section 2.2.11.1) which contains the coordinates of the
          desktop rectangle if the allowDisplayUpdates field is set to ALLOW_DISPLAY_UPDATES (1).
          If the allowDisplayUpdates field is set to SUPPRESS_DISPLAY_UPDATES (0),
          this field MUST NOT be included in the PDU.

  ts_shutdown_req_pdu:
    doc: |
      MS-RDPBCGR 2.2.2.1.1 Shutdown Request PDU Data (TS_SHUTDOWN_REQ_PDU)
      The TS_SHUTDOWN_REQ_PDU structure contains the contents of the Shutdown Request PDU
      (section 2.2.2.1), which is a Share Data Header (section 2.2.8.1.1.1.2) with no PDU
      body.
      (no data)

  ts_shutdown_denied_pdu:
    doc: |
      MS-RDPBCGR 2.2.2.2.1 Shutdown Request Denied PDU Data (TS_SHUTDOWN_DENIED_PDU)
      The TS_SHUTDOWN_DENIED_PDU structure contains the contents of the Shutdown Request
      Denied PDU, which is a Share Data Header (section 2.2.8.1.1.1.2) with no PDU body.
      NOTE: Event CC_EVT_ONSHUTDOWNDENIED does nothing in CC_CONNECTED state.
      mstscax!CoreFSM::CC_Event
      (no data)

  ts_save_session_info_pdu_data:
    doc: |
      MS-RDPBCGR 2.2.10.1.1 Save Session Info PDU Data (TS_SAVE_SESSION_INFO_PDU_DATA)
      The TS_SAVE_SESSION_INFO_PDU_DATA structure is a wrapper around different
      classes of user logon information.
      mstscax!CCO::OnSaveSessionInfoPDU
    seq:
      - id: info_type
        -orig-id: infoType
        type: u4
        enum: infotype
        doc: |
          A 32-bit, unsigned integer. The type of logon information.
      - id: data
        type:
          switch-on: info_type
          -name: type
          cases:
            'infotype::logon': ts_logon_info
            'infotype::logon_long': ts_logon_info_version_2
            'infotype::logon_plainnotify': ts_plain_notify
            'infotype::logon_extended_info': ts_logon_info_extended
    enums:
      infotype:
        0x00000000:
          id: logon
          doc: |
            This is a notification that the user has logged on. The infoData field which
            follows contains a Logon Info Version 1 (section 2.2.10.1.1.1) structure.
        0x00000001:
          id: logon_long
          doc: |
            This is a notification that the user has logged on. The infoData field which
            follows contains a Logon Info Version 2 (section 2.2.10.1.1.2) structure.
            This type is supported by all RDP versions except for RDP 4.0 and 5.0, and
            SHOULD be used if the LONG_CREDENTIALS_SUPPORTED (0x00000004) flag is set in
            the General Capability Set (section 2.2.7.1.1).
        0x00000002:
          id: logon_plainnotify
          doc: |
            This is a notification that the user has logged on. The infoData field which
            follows contains a Plain Notify structure which contains 576 bytes of padding
            (section 2.2.10.1.1.3). This type is supported by all RDP versions except for
            RDP 4.0 and 5.0.
        0x00000003:
          id: logon_extended_info
          doc: |
            The infoData field which follows contains a Logon Info Extended
            (section 2.2.10.1.1.4) structure. This type is supported by all RDP versions
            except for RDP 4.0, 5.0, and 5.1.

  ts_logon_info:
    doc: |
      MS-RDPBCGR 2.2.10.1.1.1 Logon Info Version 1 (TS_LOGON_INFO)
      The TS_LOGON_INFO structure is a fixed-length structure that contains logon
      information intended for the client.
      mstscax!CCO::OnSaveSessionInfoPDU
      mstscax!CTSConnectionHandler::UpdateSessionInfo
      mstscax!CTSConnectionHandler::OnLoginComplete
    seq:
      - id: domain_size
        -orig-id: cbDomain
        type: u4
        doc: |
          A 32-bit, unsigned integer. The size of the Unicode character data (including the
          mandatory null terminator) in bytes present in the fixed-length Domain field.
      - id: domain
        -orig-id: Domain
        type: str
        encoding: utf-16le
        size: 52
        doc: |
          An array of 26 Unicode characters: Null-terminated Unicode string containing the
          name of the domain to which the user is logged on. The length of the character data
          in bytes is given by the cbDomain field.
      - id: user_name_size
        -orig-id: cbUserName
        type: u4
        doc: |
          A 32-bit, unsigned integer. Size of the Unicode character data (including the mandatory
          null terminator) in bytes present in the fixed-length UserName field.
      - id: user_name
        -orig-id: UserName
        type: str
        encoding: utf-16le
        size: 512
        doc: |
          An array of 256 Unicode characters: Null-terminated Unicode string containing the
          username which was used to log on. The length of the character data in bytes is
          given by the cbUserName field.
      - id: session_id
        -orig-id: SessionId
        type: u4
        doc: |
          A 32-bit, unsigned integer. Optional ID of the session on the remote server according
          to the server. Sent by all RDP servers, except for RDP 4.0 servers.

  ts_logon_info_version_2:
    doc: |
      MS-RDPBCGR 2.2.10.1.1.2 Logon Info Version 2 (TS_LOGON_INFO_VERSION_2)
      TS_LOGON_INFO_VERSION_2 is a variable-length structure that contains logon
      information intended for the client.
      mstscax!CCO::OnSaveSessionInfoPDU
      mstscax!CTSConnectionHandler::UpdateSessionInfo
      mstscax!CTSConnectionHandler::OnLoginComplete
    seq:
      - id: version
        -orig-id: Version
        type: u2
        doc: |
          A 16-bit, unsigned integer. The logon version.
          SAVE_SESSION_PDU_VERSION_ONE (0x0001): Version 1
      - id: size
        -orig-id: Size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The total size in bytes of this structure, excluding the Domain
          and UserName variable-length fields.
      - id: session_id
        -orig-id: SessionId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The ID of the session on the remote server according to the server.
      - id: domain_size
        -orig-id: cbDomain
        type: u4
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the Domain field
          (including the mandatory null terminator).
      - id: user_name_size
        -orig-id: cbUserName
        type: u4
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the UserName field
          (including the mandatory null terminator).
      - id: pad
        -orig-id: Pad
        size: 558
        doc: |
          558 bytes. Padding. Values in this field MUST be ignored.
      - id: domain
        -orig-id: Domain
        type: str
        encoding: utf-16le
        size: domain_size
        doc: |
          Variable-length null-terminated Unicode string containing the name of the domain to which
          the user is logged on. The size of this field in bytes is given by the cbDomain field.
      - id: user_name
        -orig-id: UserName
        type: str
        encoding: utf-16le
        size: user_name_size
        doc: |
          Variable-length null-terminated Unicode string containing the user name which was used to
          log on. The size of this field in bytes is given by the cbUserName field.

  ts_plain_notify:
    doc: |
      MS-RDPBCGR 2.2.10.1.1.3 Plain Notify (TS_PLAIN_NOTIFY)
      TS_PLAIN_NOTIFY is a fixed-length structure that contains 576 bytes of padding.
      mstscax!CCO::OnSaveSessionInfoPDU
      mstscax!CTSConnectionHandler::OnLoginComplete
    seq:
      - id: pad
        -orig-id: Pad
        size: 576
        doc: |
          576 bytes. Padding. Values in this field MUST be ignored.

  ts_logon_info_extended:
    doc: |
      MS-RDPBCGR 2.2.10.1.1.4 Logon Info Extended (TS_LOGON_INFO_EXTENDED)
      The TS_LOGON_INFO_EXTENDED structure contains extended logon information and is
      supported by all RDP versions, except for RDP 4.0, 5.0, and 5.1.
      mstscax!CCO::OnSaveSessionInfoPDU
    seq:
      - id: length
        -orig-id: Length
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total size in bytes of this structure, including
          the variable LogonFields field.
      - id: fields_present
        -orig-id: FieldsPresent
        type: u4
        doc: |
          A 32-bit, unsigned integer. The flags indicating which fields are present in the
          LogonFields field.
      - id: auto_reconnect_cookie_data_size
        type: u4
        if: (fields_present & logon_ex::autoreconnectcookie.to_i) != 0
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the variable-length data.
          NOTE: Cannot be larger than 0x80 - otherwise will error.
      - id: auto_reconnect_cookie_data
        type: arc_sc_private_packet
        size: auto_reconnect_cookie_data_size
        if: (fields_present & logon_ex::autoreconnectcookie.to_i) != 0
      - id: logon_notification_data_size
        type: u4
        if: (fields_present & logon_ex::logonerrors.to_i) != 0
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the variable-length data.
      - id: logon_notification_data
        type: ts_logon_errors_info
        size: logon_notification_data_size
        if: (fields_present & logon_ex::logonerrors.to_i) != 0
    enums:
      logon_ex:
        0x00000001:
          id: autoreconnectcookie
          doc: |
            An auto-reconnect cookie field is present. The LogonFields field of the associated
            Logon Info (section 2.2.10.1.1.4.1) structure MUST contain a Server Auto-Reconnect
            Packet (section 2.2.4.2) structure.
        0x00000002:
          id: logonerrors
          doc: |
            A logon error field is present. The LogonFields field of the associated Logon Info
            MUST contain a Logon Errors Info (section 2.2.10.1.1.4.1.1) structure.

  arc_sc_private_packet:
    doc: |
      MS-RDPBCGR 2.2.4.2 Server Auto-Reconnect Packet (ARC_SC_PRIVATE_PACKET)
      The ARC_SC_PRIVATE_PACKET structure contains server-supplied information used to seamlessly
      re-establish a connection to a server after network interruption. It is sent as part of the
      Save Session Info PDU logon information (section 2.2.10.1.1.4).
      mstscax!CTSRdpConnectionStack::SetAutoReconnectCookie
    seq:
      - id: len
        -orig-id: cbLen
        type: u4
        doc: |
          A 32-bit, unsigned integer. The length in bytes of the Server Auto-Reconnect Packet.
          This field MUST be set to 0x0000001C (28 bytes).
      - id: version
        -orig-id: Version
        type: u4
        doc: |
          A 32-bit, unsigned integer. The value representing the auto-reconnect version.
          AUTO_RECONNECT_VERSION_1 (0x00000001): Version 1 of auto-reconnect.
      - id: logon_id
        -orig-id: LogonId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The session identifier for reconnection.
      - id: arc_random_bits
        -orig-id: ArcRandomBits
        size-eos: true
        doc: |
          Byte buffer containing a 16-byte, random number generated as a key for secure
          reconnection (section 5.5).

  ts_logon_errors_info:
    doc: |
      MS-RDPBCGR 2.2.10.1.1.4.1.1 Logon Errors Info (TS_LOGON_ERRORS_INFO)
      The TS_LOGON_ERRORS_INFO structure contains information that describes a logon error
      notification.
      mstscax!CCO::OnSaveSessionInfoPDU
      mstscax!CCO::OnLogonErrors
    seq:
      - id: error_notification_type
        -orig-id: ErrorNotificationType
        type: u4
        doc: |
          A 32-bit, unsigned integer that specifies an NTSTATUS value (see [ERRTRANS] for
          information about translating NTSTATUS error codes to usable text strings).
      - id: error_notification_data
        -orig-id: ErrorNotificationData
        type: u4
        doc: |
          A 32-bit, unsigned integer that specifies the session identifier, or an arbitrary
          value.

  ts_font_list_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.18.1 Font List PDU Data (TS_FONT_LIST_PDU)
      The TS_FONT_LIST_PDU structure contains the contents of the Font List PDU, which is a
      Share Data Header (section 2.2.8.1.1.1.2) and four fields.
    seq:
      - id: number_fonts
        -orig-id: numberFonts
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of fonts.
          This field SHOULD be set to zero.
      - id: total_num_fonts
        -orig-id: totalNumFonts
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of fonts.
          This field SHOULD be set to zero.
      - id: list_flags
        -orig-id: listFlags
        type: u2
        doc: |
          A 16-bit, unsigned integer. The sequence flags.
          This field SHOULD be set to 0x0003, which is the logical OR'd value of FONTLIST_FIRST (0x0001)
          and FONTLIST_LAST (0x0002).
      - id: entry_size
        -orig-id: entrySize
        type: u2
        doc: |
          A 16-bit, unsigned integer. The entry size. This field SHOULD be set to 0x0032 (50 bytes).

  ts_font_map_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.22.1 Font Map PDU Data (TS_FONT_MAP_PDU)
      The TS_FONT_MAP_PDU structure contains the contents of the Font Map PDU, which is
      a Share Data Header (section 2.2.8.1.1.1.2) and four fields.
      mstscax!CTSConnectionHandler::OnConnected
    seq:
      - id: number_entries
        -orig-id: numberEntries
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of fonts.
          This field SHOULD be set to zero.
      - id: total_num_entries
        -orig-id: totalNumEntries
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of fonts.
          This field SHOULD be set to zero.
      - id: map_flags
        -orig-id: mapFlags
        type: u2
        doc: |
          A 16-bit, unsigned integer. The sequence flags.
          This field SHOULD be set to 0x0003, which is the logical OR'ed value of
          FONTMAP_FIRST (0x0001) and FONTMAP_LAST (0x0002).
      - id: entry_size
        -orig-id: entrySize
        type: u2
        doc: |
          A 16-bit, unsigned integer. The entry size.
          This field SHOULD be set to 0x0004 (4 bytes).

  ts_set_keyboard_indicators_pdu:
    doc: |
      MS-RDPBCGR 2.2.8.2.1.1 Set Keyboard Indicators PDU Data (TS_SET_KEYBOARD_INDICATORS_PDU)
      The TS_SET_KEYBOARD_INDICATORS_PDU structure contains the actual contents of the Set
      Keyboard Indicators PDU (section 2.2.8.2.1). The contents of the LedFlags field is
      identical to the flags used in the Client Synchronize Input Event Notification
      (section 2.2.8.1.1.3.1.1.5).
      mstscax!CIH::IH_UpdateKeyboardIndicators
    seq:
      - id: unit_id
        -orig-id: UnitId
        type: u2
        doc: |
          A 16-bit, unsigned integer. Hardware related value. This field SHOULD be ignored
          by the client and as a consequence SHOULD be set to zero by the server.
      - id: led_flags
        -orig-id: LedFlags
        type: u2
        doc: |
          A 16-bit, unsigned integer. The flags indicating the "on" status of the keyboard
          toggle keys.
    enums:
      ts_sync:
        0x0001:
          id: scroll_lock
          doc: Indicates that the Scroll Lock indicator light SHOULD be on.
        0x0002:
          id: num_lock
          doc: Indicates that the Num Lock indicator light SHOULD be on.
        0x0004:
          id: caps_lock
          doc: Indicates that the Caps Lock indicator light SHOULD be on.
        0x0008:
          id: kana_lock
          doc: Indicates that the Kana Lock indicator light SHOULD be on.

  ts_bitmapcache_persistent_list_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.17.1 Persistent Key List PDU Data (TS_BITMAPCACHE_PERSISTENT_LIST_PDU)
      The TS_BITMAPCACHE_PERSISTENT_LIST_PDU structure contains a list of cached bitmap keys
      saved from Cache Bitmap (Revision 2) Orders ([MS-RDPEGDI] section 2.2.2.2.1.2.3) that
      were sent in previous sessions. By including a key in the Persistent Key List PDU Data
      the client indicates to the server that it has a local copy of the bitmap associated
      with the key, which means that the server does not need to retransmit the bitmap to the
      client (for more details about the Persistent Bitmap Cache, see [MS-RDPEGDI] section 3.1.1.1.1).
      The bitmap keys can be sent in more than one Persistent Key List PDU, with each PDU being
      marked using flags in the bBitMask field. The number of bitmap keys encapsulated within
      the Persistent Key List PDU Data SHOULD be limited to 169.
    seq:
      - id: num_entries_cache_0
        -orig-id: numEntriesCache0
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of entries for Bitmap Cache 0 in the
          current Persistent Key List PDU.
      - id: num_entries_cache_1
        -orig-id: numEntriesCache1
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of entries for Bitmap Cache 1 in the
          current Persistent Key List PDU.
      - id: num_entries_cache_2
        -orig-id: numEntriesCache2
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of entries for Bitmap Cache 2 in the
          current Persistent Key List PDU.
      - id: num_entries_cache_3
        -orig-id: numEntriesCache3
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of entries for Bitmap Cache 3 in the
          current Persistent Key List PDU.
      - id: num_entries_cache_4
        -orig-id: numEntriesCache4
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of entries for Bitmap Cache 4 in the
          current Persistent Key List PDU.
      - id: total_entries_cache_0
        -orig-id: totalEntriesCache0
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of entries for Bitmap Cache 0 expected
          across the entire sequence of Persistent Key List PDUs. This value MUST remain
          unchanged across the sequence. The sum of the totalEntries0, totalEntries1,
          totalEntries2, totalEntries3, and totalEntries4 fields MUST NOT exceed 262,144.
      - id: total_entries_cache_1
        -orig-id: totalEntriesCache1
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of entries for Bitmap Cache 1 expected
          across the entire sequence of Persistent Key List PDUs. This value MUST remain
          unchanged across the sequence. The sum of the totalEntries0, totalEntries1,
          totalEntries2, totalEntries3, and totalEntries4 fields MUST NOT exceed 262,144.
      - id: total_entries_cache_2
        -orig-id: totalEntriesCache2
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of entries for Bitmap Cache 2 expected
          across the entire sequence of Persistent Key List PDUs. This value MUST remain
          unchanged across the sequence. The sum of the totalEntries0, totalEntries1,
          totalEntries2, totalEntries3, and totalEntries4 fields MUST NOT exceed 262,144.
      - id: total_entries_cache_3
        -orig-id: totalEntriesCache3
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of entries for Bitmap Cache 3 expected
          across the entire sequence of Persistent Key List PDUs. This value MUST remain
          unchanged across the sequence. The sum of the totalEntries0, totalEntries1,
          totalEntries2, totalEntries3, and totalEntries4 fields MUST NOT exceed 262,144.
      - id: total_entries_cache_4
        -orig-id: totalEntriesCache4
        type: u2
        doc: |
          A 16-bit, unsigned integer. The total number of entries for Bitmap Cache 4 expected
          across the entire sequence of Persistent Key List PDUs. This value MUST remain
          unchanged across the sequence.
      - id: bit_mask
        -orig-id: bBitMask
        type: u1
        doc: |
          An 8-bit, unsigned integer. The sequencing flag.
          If neither PERSIST_FIRST_PDU (0x01) nor PERSIST_LAST_PDU (0x02) are set, then the
          current PDU is an intermediate packet in a sequence of Persistent Key List PDUs.
      - id: pad_2
        -orig-id: Pad2
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: pad_3
        -orig-id: Pad3
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: entries
        type: ts_bitmapcache_persistent_list_pdu
        repeat: eos
        doc: |
          An array of TS_BITMAPCACHE_PERSISTENT_LIST_ENTRY structures which describe 64-bit bitmap
          keys. The keys MUST be arranged in order from low cache number to high cache number.
          For instance, if a PDU contains one key for Bitmap Cache 0 and two keys for Bitmap Cache 1,
          then numEntriesCache0 will be set to 1, numEntriesCache1 will be set to 2, and
          numEntriesCache2, numEntriesCache3, and numEntriesCache4 will all be set to zero.
          The keys will be arranged in the following order: (Bitmap Cache 0, Key 1),
          (Bitmap Cache 1, Key 1), (Bitmap Cache 1, Key 2).
    instances:
      is_sequence:
        value: (bit_mask & 0x3) != 0x3
      is_sequence_first:
        value: (bit_mask & 0x3) == 0x1
      is_sequence_next:
        value: (bit_mask & 0x3) == 0x0
      is_sequence_last:
        value: (bit_mask & 0x3) == 0x2
    enums:
      persist:
        0x01:
          id: first_pdu
          doc: Indicates that the PDU is the first in a sequence of Persistent Key List PDUs.
        0x02:
          id: last_pdu
          doc: Indicates that the PDU is the last in a sequence of Persistent Key List PDUs.

  ts_bitmapcache_persistent_list_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.17.1.1 Persistent List Entry (TS_BITMAPCACHE_PERSISTENT_LIST_ENTRY)
      The TS_BITMAPCACHE_PERSISTENT_LIST_ENTRY structure contains a 64-bit bitmap key to be
      sent back to the server.
    seq:
      - id: key
        -orig-id: Key
        type: u8
        doc: |
          64-bit persistent bitmap cache key.
    instances:
      key1:
        -orig-id: Key1
        value: key & 0xffffffff
        doc: |
          Low 32 bits of the 64-bit persistent bitmap cache key.
      key2:
        -orig-id: Key2
        value: (key >> 32) & 0xffffffff
        doc: |
          A 32-bit, unsigned integer. High 32 bits of the 64-bit persistent bitmap cache key.

  ts_bitmap_cache_error_pdu:
    doc: |
      MS-RDPEGDI 2.2.2.3.1.1 Bitmap Cache Error PDU Data (TS_BITMAP_CACHE_ERROR_PDU)
      The TS_BITMAP_CACHE_ERROR_PDU structure contains the contents of the Bitmap Cache Error PDU,
      which is essentially a Share Data Header (see [MS-RDPBCGR] section 2.2.8.1.1.1.2) and an
      array of Bitmap Cache Error Info (section 2.2.2.3.1.1.1) structures.
    seq:
      - id: num_info_blocks
        -orig-id: NumInfoBlocks
        type: u1
        doc: |
          An 8-bit, unsigned integer. The number of Bitmap Cache Error Info (section 2.2.2.3.1.1.1)
          structures in the Info field.
      - id: pad1
        -orig-id: Pad1
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field are arbitrary and MUST be ignored.
      - id: pad2
        -orig-id: Pad2
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field are arbitrary and MUST be ignored.
      - id: info
        -orig-id: Info
        type: ts_bitmap_cache_error_info
        repeat: expr
        repeat-expr: num_info_blocks
        doc: |
          An array of Bitmap Cache Error Info (section 2.2.2.3.1.1.1) structures, each structure
          specifying what actions to take with each of the bitmap caches.

  ts_bitmap_cache_error_info:
    doc: |
      MS-RDPEGDI 2.2.2.3.1.1.1 Bitmap Cache Error Info (TS_BITMAP_CACHE_ERROR_INFO)
      The TS_BITMAP_CACHE_ERROR_INFO structure specifies what actions are to be taken on a particular
      bitmap cache when a caching error occurs.
    seq:
      - id: cache_id
        -orig-id: CacheID
        type: u1
        doc: |
          An 8-bit, unsigned integer. ID of the bitmap cache represented by this block.
      - id: bit_field
        -orig-id: bBitField
        type: u1
        enum: bc_err
        doc: |
          An 8-bit, unsigned integer. A bit field containing several flags.
      - id: pad
        -orig-id: Pad
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field are ignored.
      - id: new_num_entries
        -orig-id: NewNumEntries
        type: u4
        doc: |
          A 32-bit, unsigned integer. The new number of entries in the cache.
          This value MUST be less than or equal to the number of entries specified in the
          Revision 2 Bitmap Cache Capability Set ([MS-RDPBCGR] section 2.2.7.1.4.2).
    enums:
      bc_err:
        0x01:
          id: flush_cache
          doc: |
            Indicates that the contents of the cache MUST be emptied.
        0x02:
          id: newnumentries_valid
          doc: |
            Indicates that the NewNumEntries field is valid. If the BC_ERR_FLUSH_CACHE (0x01) flag is
            not set, and the NewNumEntries field specifies a new non-zero size, the previous cache
            contents in the initial NewNumEntries cells MUST be preserved.

  ts_set_keyboard_ime_status_pdu:
    doc: |
      MS-RDPBCGR 2.2.8.2.2.1 Set Keyboard IME Status PDU Data (TS_SET_KEYBOARD_IME_STATUS_PDU)
      The TS_SET_KEYBOARD_IME_STATUS_PDU structure contains the actual contents of the Set
      Keyboard IME Status PDU (section 2.2.8.2.2). The ImeState and ImeConvMode fields are
      used as input parameters to a Fujitsu Oyayubi-specific IME control function on Asian
      IME clients.
      For more information on input method editors (IMEs), see [International], section
      "Input Method Editors" in chapter 5.
      mstscax!CTSInput::OnUpdateKeyboardImeStatus
    seq:
      - id: unit_id
        -orig-id: UnitId
        type: u2
        doc: |
          A 16-bit, unsigned integer. The unit identifier for which the IME message is intended.
          This field SHOULD be ignored by the client and as a consequence SHOULD be set to zero
          by the server.
      - id: ime_state
        -orig-id: ImeState
        type: u4
        enum: ime_state
        doc: |
          A 32-bit, unsigned integer. Indicates the open or closed state of the IME.
      - id: ime_conv_mode
        -orig-id: ImeConvMode
        type: u4
        enum: ime_cmode
        doc: |
          A 32-bit, unsigned integer. Indicates the IME conversion mode.
    enums:
      ime_state:
        0x00000000:
          id: closed
          doc: The IME state is closed.
        0x00000001:
          id: open
          doc: The IME state is open.
      ime_cmode:
        0x00000001:
          id: native
          doc: The input mode is native. If not set, the input mode is alphanumeric.
        0x00000002:
          id: katakana
          doc: The input mode is Katakana. If not set, the input mode is Hiragana.
        0x00000008:
          id: fullshape
          doc: The input mode is full-width. If not set, the input mode is half-width.
        0x00000010:
          id: roman
          doc: The input mode is Roman.
        0x00000020:
          id: charcode
          doc: Character-code input is in effect.
        0x00000040:
          id: hanjaconvert
          doc: Hanja conversion mode is in effect.
        0x00000080:
          id: softkbd
          doc: A soft (on-screen) keyboard is being used.
        0x00000100:
          id: nocoversion
          doc: IME conversion is inactive (that is, the IME is closed).
        0x00000200:
          id: eudc
          doc: End-User Defined Character (EUDC) conversion mode is in effect.
        0x00000400:
          id: symbol
          doc: Symbol conversion mode is in effect.
        0x00000800:
          id: fixed
          doc: Fixed conversion mode is in effect.

  ts_offscrcache_error_pdu:
    doc: |
      MS-RDPEGDI 2.2.2.3.2.1 Offscreen Bitmap Cache Error PDU Data (TS_OFFSCRCACHE_ERROR_PDU)
      The TS_OFFSCRCACHE_ERROR_PDU structure contains the contents of the Offscreen Bitmap
      Cache Error PDU, which is essentially a Share Data Header
      (see [MS-RDPBCGR] section 2.2.8.1.1.1.2) and a flags field.
    seq:
      - id: flags
        type: u4
        doc: |
          A 32-bit, unsigned integer. Indicates the support for offscreen bitmap caching.
          This field MUST be set to OC_ERR_FLUSH_AND_DISABLE_OFFSCREEN (0x00000001), which
          specifies that the offscreen cache MUST be flushed and that further offscreen
          bitmap caching MUST be disabled.
    enums:
      oc_err:
        0x00000001: flush_and_disable_offscreen

  ts_set_error_info_pdu:
    doc: |
      MS-RDPBCGR 2.2.5.1.1 Set Error Info PDU Data (TS_SET_ERROR_INFO_PDU)
      The TS_SET_ERROR_INFO_PDU structure contains the contents of the Set Error Info PDU,
      which is a Share Data Header (section 2.2.8.1.1.1.2) with an error value field.
      mstscax!CTSConnectionHandler::SetServerErrorInfo
    seq:
      - id: error_info
        -orig-id: errorInfo
        type: u4
        doc: |
          A 32-bit, unsigned integer. Error code.

  ts_drawninegrid_error_pdu:
    doc: |
      MS-RDPEGDI 2.2.2.3.3.1 DrawNineGrid Cache Error PDU Data (TS_DRAWNINEGRID_ERROR_PDU)
      The TS_DRAWNINEGRID_ERROR_PDU structure contains the contents of the DrawNineGrid
      Cache Error PDU, which is essentially a Share Data Header
      (see [MS-RDPBCGR] section 2.2.8.1.1.1.2) and a flags field.
    seq:
      - id: flags
        type: u4
        doc: |
          A 32-bit, unsigned integer. Indicates support for NineGrid bitmap caching.
          This field MUST be set to DNG_ERR_FLUSH_AND_DISABLE_DRAWNINEGRID (0x00000001),
          which means that the NineGrid bitmap cache MUST be flushed and that further
          NineGrid bitmap caching MUST be disabled.
    enums:
      dng_err:
        0x00000001: flush_and_disable_drawninegrid

  ts_drawgdiplus_error_pdu:
    doc: |
      MS-RDPEGDI 2.2.2.3.4.1 GDI+ Error PDU Data (TS_DRAWGDIPLUS_ERROR_PDU)
      The TS_DRAWGDIPLUS_ERROR_PDU structure contains the contents of the GDI+ Error PDU,
      which is essentially a Share Data Header (see [MS-RDPBCGR] section 2.2.8.1.1.1.2) and
      a flags field.
    seq:
      - id: flags
        type: u4
        doc: |
          A 32-bit, unsigned integer. Indicates support for GDI+ 1.1 rendering.
          This field MUST be set to GDIPLUS_ERR_FLUSH_AND_DISABLE_DRAWGDIPLUS (0x00000001),
          which means that the GDI+ drawing MUST be flushed and further GDI+ 1.1 rendering
          primitives MUST be disabled (implying that future drawings will be sent as bitmaps).
    enums:
      gdiplus_err:
        0x00000001: flush_and_disable_drawgdiplus

  ts_autoreconnect_status_pdu:
    doc: |
      MS-RDPBCGR 2.2.4.1.1 Auto-Reconnect Status PDU Data (TS_AUTORECONNECT_STATUS_PDU)
      The TS_AUTORECONNECT_STATUS_PDU structure contains the contents of the Auto-Reconnect
      Status PDU, which is a Share Data Header (section 2.2.8.1.1.1.2) with a status field.
      mstscax!CCO::OnPacketReceived
    seq:
      - id: arc_status
        -orig-id: arcStatus
        type: u4
        doc: |
          A 32-bit, unsigned integer. This field MUST be set to zero.

  ts_input_mode_change_pdu:
    doc: |
      UNDOCUMENTED
      mstscax!CIH::IH_ChangeInputMode
      mstscax!CTSInput::ChangeInputMode
    seq:
      - id: input_mode_flags
        -orig-id: InputModeFlags
        type: u4
    enums:
      input_mode_flag:
        0x00000001: relative_mouse_mode

  ts_status_info_pdu:
    doc: |
      MS-RDPBCGR 2.2.5.2 Server Status Info PDU
      The Status Info PDU is sent by the server to update the client with status information.
      This PDU is only sent to clients that have indicated that they are capable of status
      updates using the RNS_UD_CS_SUPPORT_STATUSINFO_PDU flag in the Client Core Data
      (section 2.2.1.3.2).
      mstscax!CTSConnectionHandler::OnStatusInfoReceived
    seq:
      - id: status_code
        -orig-id: statusCode
        type: u4
        doc: |
          A 32-bit, unsigned integer. Status code.
