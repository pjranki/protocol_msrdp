meta:
  id: nscodec_bitmap_stream
  endian: le

doc: |
  MS-RDPNSC 2.2.2 NSCodec Compressed Bitmap Stream (NSCODEC_BITMAP_STREAM)
  The NSCODEC_BITMAP_STREAM structure contains a stream of bitmap data compressed using NSCodec
  bitmap compression techniques (section 3.1.8). The bitmap data is represented using the AYCoCg
  color space ([MS-RDPEGDI] section 3.1.9.1.2).
  NSCodec compressed bitmap data is sent encapsulated in a Set Surface Bits Surface Command
  ([MS-RDPBCGR] section 2.2.9.2.1) when sending a bitmap image that MUST NOT be cached, or in the
  Cache Bitmap - Revision 3 ([MS-RDPEGDI] section 2.2.2.2.1.2.8) Secondary Drawing Order when
  sending a bitmap image that MUST be cached (bitmap caching is discussed in [MS-RDPEGDI] section
  3.1.1.1.1). In all these cases, the data is encapsulated inside an Extended Bitmap Data
  ([MS-RDPBCGR] section 2.2.9.2.1.1) structure.
  The width and height of the compressed bitmap are obtained from the width and height fields of
  the encapsulating Extended Bitmap Data structure.
  mstscax!NSCodecDecompressor::Decompress

seq:
  - id: luma_plane_byte_count
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the number of bytes used by the LumaPlane field.
      This value MUST be greater than zero.
  - id: orange_chroma_plane_byte_count
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the number of bytes used by the OrangeChromaPlane
      field. This value MUST be greater than zero.
  - id: green_chroma_plane_byte_count
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the number of bytes used by the GreenChromaPlane
      field. This value MUST be greater than zero.
  - id: alpha_plane_byte_count
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the number of bytes used by the AlphaPlane field.
  - id: color_loss_level
    type: u1
    doc: |
      An 8-bit, unsigned integer that indicates the Color Loss Level ([MS-RDPEGDI] section
      3.1.9.1.4) that was applied to the chroma values of this packet. This value MUST be in the
      range 1 to 7 (inclusive).
  - id: chroma_subsampling_level
    type: u4
    doc: |
      An 8-bit, unsigned integer that indicates whether chroma subsampling is being used
      ([MS-RDPEGDI] section 3.1.9.1.3).
       - FALSE (0x00) : Chroma subsampling is not being used.
       - TRUE (0x01)  : Chroma subsampling is being used.
  - id: reserved
    type: u2
    doc: |
      A 16-bit field. Reserved for future use.
  - id: luma_plane
    size: luma_plane_byte_count
    doc: |
      A variable-length array of bytes that contains the luma plane data.

      The LumaPlaneByteCount field is used to determine whether the data is in raw format, or
      if it has been RLE (2) compressed. If LumaPlaneByteCount is equal to the expected raw
      size of the luma plane, the data is in raw format. If LumaPlaneByteCount is smaller than
      the expected size, the data has been RLE compressed. LumaPlaneByteCount MUST NOT be larger
      than the expected size of the luma plane.

      If chroma subsampling is not being used, the expected raw size of the luma plane is
      calculated as follows (input to the calculation is the raw image width and height).

          LumaPlaneWidth = ImageWidth
          LumaPlaneHeight = ImageHeight
          LumaPlaneByteCount = ImageWidth * ImageHeight

      If chroma subsampling is being used, the expected raw size of the luma plane is calculated
      as follows.

          LumaPlaneWidth = ROUND_UP_TO_NEAREST_MULTIPLE_OF_8(ImageWidth)
          LumaPlaneHeight = ImageHeight
          LumaPlaneByteCount = LumaPlaneWidth * ImageHeight

      If the luma channel has been RLE compressed, this field contains an NSCodec RLE Segments
      (section 2.2.2.1) structure. Otherwise, it contains the raw bytes of the color plane.

  - id: orange_chroma_plane
    size: orange_chroma_plane_byte_count
    doc: |
      A variable-length array of bytes that contains the orange chroma plane.

      The OrangeChromaPlaneByteCount field is used to determine whether the data is in raw format
      or has been RLE compressed. If OrangeChromaPlaneByteCount is equal to the expected raw size
      of the chroma plane, the data is in raw format. If OrangeChromaPlaneByteCount is smaller
      than the expected size, the data has been RLE compressed. OrangeChromaPlaneByteCount MUST NOT
      be larger than the expected size of the chroma plane.

      If chroma subsampling is not being used, the expected raw size of the orange chroma plane is
      calculated as follows (input to the calculation is the raw image width and height).

          ChromaPlaneWidth = ImageWidth
          ChromaPlaneHeight = ImageHeight
          ChromaPlaneByteCount = ImageWidth * ImageHeight

      If chroma subsampling is being used, the expected raw size of the orange chroma plane is
      calculated as follows.

          ChromaPlaneWidth = ROUND_UP_TO_NEAREST_MULTIPLE_OF_8(ImageWidth) / 2
          ChromaPlaneHeight = ROUND_UP_TO_NEAREST_MULTIPLE_OF_2(ImageHeight) / 2
          ChromaPlaneByteCount = ChromaPlaneWidth * ChromaPlaneHeight

      If the orange chroma channel has been RLE compressed, this field contains an NSCodec RLE
      Segments (section 2.2.2.1) structure. Otherwise, it contains the raw bytes of the color
      plane.

      Depending on the values of the ColorLossLevel and ChromaSubsamplingLevel fields, the orange
      chroma plane can be transformed by color loss reduction ([MS-RDPEGDI] section 3.1.9.1.4) and
      chroma subsampling ([MS-RDPEGDI] section 3.1.9.1.3).

  - id: green_chroma_plane
    size: green_chroma_plane_byte_count
    doc: |
      A variable-length array of bytes that contains the green chroma plane.

      The GreenChromaPlaneByteCount field is used to determine whether the data is in raw format
      or has been RLE compressed. If GreenChromaPlaneByteCount is equal to the expected raw size
      of the chroma plane, the data is in raw format. If GreenChromaPlaneByteCount is smaller than
      the expected size, the data has been RLE compressed. GreenChromaPlaneByteCount MUST NOT be
      larger than the expected size of the chroma plane.

      If chroma subsampling is not being used, the expected raw size of the green chroma plane is
      calculated as follows (input to the calculation is the raw image width and height).

          ChromaPlaneWidth = ImageWidth
          ChromaPlaneHeight = ImageHeight
          ChromaPlaneByteCount = ImageWidth * ImageHeight

      If chroma subsampling is being used, the expected raw size of the green chroma plane is
      calculated as follows.

          ChromaPlaneWidth = ROUND_UP_TO_NEAREST_MULTIPLE_OF_8(ImageWidth) / 2
          ChromaPlaneHeight = ROUND_UP_TO_NEAREST_MULTIPLE_OF_2(ImageHeight) / 2
          ChromaPlaneByteCount = ChromaPlaneWidth * ChromaPlaneHeight

      If the green chroma channel has been RLE compressed, this field contains an NSCodec RLE
      Segments (section 2.2.2.1) structure. Otherwise, it contains the raw bytes of the color
      plane.

      Depending on the values of the ColorLossLevel and ChromaSubsamplingLevel fields, the green
      chroma plane can be transformed by color loss reduction ([MS-RDPEGDI] section 3.1.9.1.4) and
      chroma subsampling ([MS-RDPEGDI] section 3.1.9.1.3).

  - id: alpha_plane
    size: alpha_plane_byte_count
    doc: |
      A variable-length array of bytes that contains the alpha plane. This field MUST NOT be present
      if AlphaPlaneByteCount equals 0.

      If the AlphaPlaneByteCount field is greater than zero, it MUST be used to determine whether the
      AlphaPlane data is in raw format or has been RLE compressed. If AlphaPlaneByteCount is equal to
      the expected raw size of the Alpha plane, the data is in raw format. If AlphaPlaneByteCount is
      smaller than the expected size, the data has been RLE compressed. AlphaPlaneByteCount MUST NOT
      be larger than the expected size of the alpha plane.

      The expected raw size of the alpha plane is calculated as follows (input to the calculation is
      the raw image width and height).

          AlphaPlaneWidth = ImageWidth
          AlphaPlaneHeight = ImageHeight
          AlphaPlaneByteCount = ImageWidth * ImageHeight

      If the alpha channel has been RLE compressed, this field contains an NSCodec RLE Segments 
      section 2.2.2.1) structure. Otherwise, it contains the raw bytes of the color plane.

types:
  nscodec_rle_segments:
    doc: |
      MS-RDPNSC 2.2.2.1 NSCodec RLE Segments (NSCODEC_RLE_SEGMENTS)
      The NSCODEC_RLE_SEGMENTS structure contains the run-length encoded contents of a color plane
      and consists of a collection of NSCodec RLE run segment (section 2.2.2.2.1) and NSCodec RLE
      literal segment (section 2.2.2.2.2) structures.
      RLE compression is the final stage that is applied when compressing a bitmap using NSCodec
      bitmap compression (for more details, refer to the compression flow diagram in section 3.1.8.3).
      mstscax!NSCodecDecompressor::Decompress
    seq:
      - id: segments
        type: 'nscodec_rle_run(segments.size == 0 ? false : segments[segments.size - 1].literal, segments.size == 0 ? 0 : segments[segments.size - 1].run_value)'
        repeat: until
        repeat-until: (_io.size - _io.pos) <= 4
        doc: |
          A variable-length field that contains an array of NSCodec RLE Run Segment (section 2.2.2.2.1)
          and NSCodec RLE Literal Segment (section 2.2.2.2.2) structures.
      - id: end_data
        size: 4
        doc: |
          A 32-bit, unsigned integer that contains the last four raw bytes of the original color plane.

  nscodec_rle_run:
    doc: |
      MS-RDPNSC 2.2.2.2.1 NSCodec RLE Run Segment (NSCODEC_RLE_RUN_SEGMENT)
      The NSCODEC_RLE_RUN_SEGMENT structure is used to represent an RLE run (section 3.1.8.1).
      mstscax!NSRunLengthDecode
    params:
      - id: prev_literal
        type: bool
      - id: prev_run_value
        type: u1
    seq:
      - id: run_value
        type: u1
      - id: run_length_factor_1
        type: u1
        if: segment
      - id: run_length_factor_2
        type: u4
        if: segment and (run_length_factor_1 == 0xff)
    instances:
      segment:
        value: prev_literal and (prev_run_value == run_value)
      literal:
        value: not segment
      run_confirm:
        value: run_value
      run_length_factor_1_u4:
        value: run_length_factor_1 << 0
      run_repeat_count:
        value: 'literal ? 1 : (run_length_factor_1 == 0xff ? run_length_factor_2 : run_length_factor_1_u4 + 2)'
        doc: |
          How many time the runValue repeats.
