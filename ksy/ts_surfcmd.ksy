meta:
  id: ts_surfcmd
  endian: le
  imports:
    - ts_bitmap_data_ex

doc: |
  MS-RDPBCGR 2.2.9.1.2.1.10.1 Surface Command (TS_SURFCMD)
  The TS_SURFCMD structure is used to specify the Surface Command type
  and to encapsulate the data for a Surface Command sent from a server
  to a client. All Surface Commands in section 2.2.9.2 conform to this
  structure.
  mstscax!CTSCoreGraphics::ProcessSurfaceCommands

seq:
  - id: cmd_type
    type: u2
    enum: cmdtype
    doc: |
      A 16-bit unsigned integer. Surface Command type.
  - id: cmd_data
    doc: |
      Variable-length data specific to the Surface Command.
    type:
      switch-on: cmd_type
      -name: type
      cases:
        'cmdtype::set_surface_bits': ts_surfcmd_set_surf_bits
        'cmdtype::frame_marker': ts_surfcmd_frame_marker
        'cmdtype::stream_surface_bits': ts_surfcmd_stream_surf_bits

enums:
  cmdtype:
    0x0001:
      id: set_surface_bits
      doc: |
        Indicates a Set Surface Bits Command (section 2.2.9.2.1).
    0x0004:
      id: frame_marker
      doc: |
        Indicates a Frame Marker Command (section 2.2.9.2.3).
    0x0006:
      id: stream_surface_bits
      doc: |
        Indicates a Stream Surface Bits Command (section 2.2.9.2.2).

types:
  ts_surfcmd_set_surf_bits:
    doc: |
      MS-RDPBCGR 2.2.9.2.1 Set Surface Bits Command (TS_SURFCMD_SET_SURF_BITS)
      The Set Surface Bits Command is used to transport encoded bitmap
      data destined for a rectangular region of the primary drawing
      surface from an RDP server to an RDP client.
      mstscax!CTSCoreGraphics::ProcessSurfaceCommands
      mstscax!CTSCoreGraphics::ProcessUpdateSurfaceBits
    seq:
      - id: dest_left
        type: u2
        doc: |
          A 16-bit, unsigned integer. Left bound of the destination rectangle
          that will contain the decoded bitmap data.
      - id: dest_top
        type: u2
        doc: |
          A 16-bit, unsigned integer. Top bound of the destination rectangle
          that will contain the decoded bitmap data.
      - id: dest_right
        type: u2
        doc: |
          A 16-bit, unsigned integer. Exclusive right bound of the destination
          rectangle that will contain the decoded bitmap data. This field SHOULD
          be ignored, as the width of the encoded bitmap image is specified in
          the Extended Bitmap Data (section 2.2.9.2.1.1) present in the
          variable-length bitmapData field.
      - id: dest_bottom
        type: u2
        doc: |
          A 16-bit, unsigned integer. Exclusive bottom bound of the destination
          rectangle that will contain the decoded bitmap data. This field SHOULD
          be ignored, as the height of the encoded bitmap image is specified in
          the Extended Bitmap Data present in the variable-length bitmapData field.
      - id: bitmap_data
        type: ts_bitmap_data_ex
        doc: |
          An Extended Bitmap Data structure that contains an encoded bitmap image.

  ts_surfcmd_frame_marker:
    doc: |
      MS-RDPBCGR 2.2.9.2.3 Frame Marker Command (TS_FRAME_MARKER)
      The Frame Marker Command is used to group multiple surface commands
      so that these commands can be processed and presented to the user as
      a single entity, a frame.
      mstscax!CTSCoreGraphics::ProcessSurfaceCommands
      mstscax!CTSGraphics::EndOfUpdateBatch
    seq:
      - id: frame_action
        type: u2
        enum: surfacecmd_frameaction
        doc: |
          A 16-bit, unsigned integer. Identifies the beginning and end of a frame.
      - id: frame_id
        type: u4
        doc: |
          A 32-bit, unsigned integer. The ID identifying the frame.
    enums:
      surfacecmd_frameaction:
        0x0000:
          id: begin
          doc: Indicates the start of a new frame.
        0x0001:
          id: end
          doc: Indicates the end of the current frame.

  ts_surfcmd_stream_surf_bits:
    doc: |
      MS-RDPBCGR  2.2.9.2.2 Stream Surface Bits Command (TS_SURFCMD_STREAM_SURF_BITS)
      The Stream Surface Bits Command is used to transport encoded bitmap
      data destined for a rectangular region of the primary drawing surface
      from an RDP server to an RDP client.
      mstscax!CTSCoreGraphics::ProcessSurfaceCommands
      mstscax!CTSCoreGraphics::ProcessUpdateSurfaceBits
    seq:
      - id: dest_left
        type: u2
        doc: |
          A 16-bit, unsigned integer. Left bound of the destination rectangle
          that will contain the decoded bitmap data.
      - id: dest_top
        type: u2
        doc: |
          A 16-bit, unsigned integer. Top bound of the destination rectangle
          that will contain the decoded bitmap data.
      - id: dest_right
        type: u2
        doc: |
          A 16-bit, unsigned integer. Exclusive right bound of the destination
          rectangle that will contain the decoded bitmap data. This field SHOULD
          be ignored, as the width of the encoded bitmap image is specified in
          the Extended Bitmap Data (section 2.2.9.2.1.1) present in the
          variable-length bitmapData field.
      - id: dest_bottom
        type: u2
        doc: |
          A 16-bit, unsigned integer. Exclusive bottom bound of the destination
          rectangle that will contain the decoded bitmap data. This field SHOULD
          be ignored, as the height of the encoded bitmap image is specified in
          the Extended Bitmap Data present in the variable-length bitmapData field.
      - id: bitmap_data
        type: ts_bitmap_data_ex
        doc: |
          An Extended Bitmap Data structure that contains an encoded bitmap image.
