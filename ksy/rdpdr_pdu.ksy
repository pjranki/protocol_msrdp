meta:
  id: rdpdr_pdu
  endian: le

doc: |
    MS-RDPEFS 2 Messages
    The following sections specify how Remote Desktop Protocol: File System Virtual Channel
    Extension messages are transported and Remote Desktop Protocol: File System Virtual Channel
    Extension message syntax.
    MS-RDPEFS 2.2.1.1 Shared Header (RDPDR_HEADER)
    This header is present at the beginning of every message in this protocol. The purpose of
    this header is to describe the type of the message.
    mstscax!VCManager::ChannelOpenEvent
    mstscax!ProcObj::ProcessServerPacket

seq:
  - id: component
    type: u2
    enum: rdpdr_ctyp
    doc: |
      A 16-bit unsigned integer that identifies the component to which the packet is sent.
  - id: packet_id
    type: u2
    enum: pakid
    doc: |
      A 16-bit unsigned integer. The PacketId field is a unique ID that identifies the packet
      function. This field MUST be set to one of the following values.
  - id: data
    type:
      switch-on: packet_id
      -name: type
      cases:
        'pakid::prn_cache_data': dr_prn_cache_data
        'pakid::prn_using_xps': dr_prn_using_xps
        'pakid::core_server_announce': dr_core_server_announce_req
        'pakid::core_clientid_confirm': dr_core_clientid_confirm
        'pakid::core_client_name': dr_core_client_name_req
        'pakid::core_devicelist_announce': dr_core_devicelist_announce_req
        'pakid::core_device_reply': dr_core_device_announce_rsp
        'pakid::core_device_iorequest': dr_device_iorequest
        'pakid::core_device_iocompletion': dr_device_iocompetion
        'pakid::core_server_capability': dr_core_capability_req
        'pakid::core_client_capability': dr_core_capabiltiy_rsp
        'pakid::core_devicelist_remove': dr_devicelist_remove
        'pakid::core_user_loggedon': dr_core_user_loggedon

enums:
  rdpdr_ctyp:
    0x4472:
      id: core
      doc: |
        Device redirector core component; most of the packets in this protocol are sent under
        this component ID.
    0x5052:
      id: prn
      doc: |
        Printing component. The packets that use this ID are typically about printer cache
        management and identifying XPS printers.
  pakid:
    0x496E:
      id: core_server_announce
      doc: |
        Server Announce Request, as specified in section 2.2.2.2.
    0x4343:
      id: core_clientid_confirm
      doc: |
        Client Announce Reply and Server Client ID Confirm, as specified in sections 2.2.2.3
        and 2.2.2.6.
    0x434E:
      id: core_client_name
      doc: |
        Client Name Request, as specified in section 2.2.2.4.
    0x4441:
      id: core_devicelist_announce
      doc: |
        Client Device List Announce Request, as specified in section 2.2.2.9.
    0x6472:
      id: core_device_reply
      doc: |
        Server Device Announce Response, as specified in section 2.2.2.1.
    0x4952:
      id: core_device_iorequest
      doc: |
        Device I/O Request, as specified in section 2.2.1.4.
    0x4943:
      id: core_device_iocompletion
      doc: |
        Device I/O Response, as specified in section 2.2.1.5.
    0x5350:
      id: core_server_capability
      doc: |
        Server Core Capability Request, as specified in section 2.2.2.7.
    0x4350:
      id: core_client_capability
      doc: |
        Client Core Capability Response, as specified in section 2.2.2.8.
    0x444D:
      id: core_devicelist_remove
      doc: |
        Client Drive Device List Remove, as specified in section 2.2.3.2.
    0x5043:
      id: prn_cache_data
      doc: |
        Add Printer Cachedata, as specified in [MS-RDPEPC] section 2.2.2.3.
    0x554C:
      id: core_user_loggedon
      doc: |
        Server User Logged On, as specified in section 2.2.2.5.
    0x5543:
      id: prn_using_xps
      doc: |
        Server Printer Set XPS Mode, as specified in [MS-RDPEPC] section 2.2.2.2.

types:
  dr_prn_using_xps:
    doc: |
      MS-RDPEPC 2.2.2.2 Server Printer Set XPS Mode (DR_PRN_USING_XPS)
      This message is sent from server to client to set the device in XPS mode (see section 3.1.1.2).
      mstscax!ProcObj::ProcessServerPacket
    seq:
      - id: printer_id
        type: u4
        doc: |
          A 32-bit unsigned integer. This message is handled by the Print Virtual Channel Extension
          only if the PrinterId field matches the previously established DeviceId field for a printer
          device, see [MS-RDPEFS] section 3.1.1.
          mstscax!DrObjectMgr<DrDevice>::GetObjectW
      - id: flags
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is unused. It can contain any value and MUST be ignored
          on receipt.

  dr_prn_cache_data:
    doc: |
      MS-RDPEPC 2.2.1.1 Server Printer Cache Event (SERVER_PRINTER_CACHE_EVENT)
      The Server Printer Cache Event is used to identify various server events associated with Printer Cached
      Config Data (section 3.1.1.1) in the messages sent from the server to the client.
      mstscax!W32DrPRN::ProcessPrinterCacheInfo
    seq:
      - id: cachedata
        type: u4
        enum: rdpdr
        doc: |
          A 32-bit unsigned integer value, the type of event.
      - id: data
        type:
          switch-on: cachedata
          -name: type
          cases:
            'rdpdr::add_printer_event': dr_prn_add_cachedata
            'rdpdr::update_printer_event': dr_prn_update_cachedata
            'rdpdr::delete_printer_event': dr_prn_delete_cachedata
            'rdpdr::rename_printer_event': dr_prn_rename_cachedata
    enums:
      rdpdr:
        0x00000001:
          id: add_printer_event
          doc: Add printer cachedata event.
        0x00000002:
          id: update_printer_event
          doc: Update printer cachedata event.
        0x00000003:
          id: delete_printer_event
          doc: Delete printer cachedata event.
        0x00000004:
          id: rename_printer_event
          doc: Rename printer cachedata event.

  dr_prn_add_cachedata:
    doc: |
      MS-RDPEPC 2.2.2.3 Add Printer Cachedata (DR_PRN_ADD_CACHEDATA)
      This message is sent from the server to the client when a printer queue is created manually on the
      redirected port (port redirection is explained in [MS-RDPESP]) on the server.
      ALL BYTES IN THIS MESSAGE WILL BE ADDED TO THE FOLLOWING REGISTRY KEY IN THE CLIENT:
        - HIVE: HKEY_CURRENT_USER
        - KEY: \Software\Microsoft\Terminal Server Client\Default\AddIns\RDPDR\<printer_name>
        - VALUE: PrinterCacheData
        - TYPE: REG_BINARY
      A maximum of 500,000 bytes can be stored in this value.
      mstscax!W32DrPRN::AddPrinterCacheInfo
    seq:
      - id: port_dos_name
        type: str
        encoding: ascii
        size: 8
        doc: |
          A string of ASCII characters with a maximum length of 8 characters that represent the name of
          the device as it appears on the client. If this field is not be null-terminated, then the
          PortDosName string is equal to all 8 characters.
      - id: pnp_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the PnPName field, including
          its null terminator.
      - id: driver_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the DriverName field, including
          its null terminator.
      - id: printer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the PrintName field, including
          its null terminator.
      - id: config_data_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the CachedPrinterConfigData
          field.
      - id: pnp_name
        type: strz
        encoding: utf-16le
        size: pnp_name_len
        doc: |
          A null-terminated Unicode string. This field can be set to any valid Unicode string and MUST
          be ignored on receipt.
      - id: driver_name
        type: strz
        encoding: utf-16le
        size: driver_name_len
        doc: |
          A null-terminated Unicode string. This string specifies the printer driver name that is associated
          with the printer configuration data.
      - id: printer_name
        type: strz
        encoding: utf-16le
        size: printer_name_len
        doc: |
          A null-terminated Unicode string that specifies the client printer, which was sent for this
          configuration data message.
      - id: cached_printer_config_data
        size: config_data_len
        doc: |
          A variable-length array of bytes. This field is a BLOB of data that describes the cached printer
          configuration (see section 3.1.1.1).

  dr_prn_update_cachedata:
    doc: |
      MS-RDPEPC 2.2.2.4 Update Printer Cachedata (DR_PRN_UPDATE_CACHEDATA)
      ALL BYTES IN THIS CachedPrinterConfigData WILL BE UPDATED IN THE FOLLOWING REGISTRY KEY IN THE CLIENT:
        - HIVE: HKEY_CURRENT_USER
        - KEY: \Software\Microsoft\Terminal Server Client\Default\AddIns\RDPDR\<printer_name>
        - VALUE: PrinterCacheData
        - TYPE: REG_BINARY
      mstscax!W32DrPRN::UpdatePrinterCacheInfo
      mstscax!DrPRN::UpdatePrinterCacheData
    seq:
      - id: printer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer specifying the number of bytes in the PrinterName field, including its
          null terminator.
      - id: config_data_len
        type: u4
        doc: |
          A 32-bit unsigned integer specifying the number of bytes in the CachedPrinterConfigData field.
      - id: printer_name
        type: strz
        encoding: utf-16le
        size: printer_name_len
        doc: |
          A null-terminated Unicode string that specifies the printer, for which the updated printer configuration
          data is sent.
      - id: cached_printer_config_data
        size: config_data_len
        doc: |
          A variable-length array of bytes. This field is a BLOB of data that describes the cached printer
          configuration (see section 3.1.1.1).

  dr_prn_delete_cachedata:
    doc: |
      MS-RDPEPC 2.2.2.5 Delete Printer Cachedata (DR_PRN_DELETE_CACHEDATA)
      This message is sent by the server when a manually created printer on the redirected port is deleted.
      DELETES THE FOLLOWING KEY:
        - HIVE: HKEY_CURRENT_USER
        - KEY: \Software\Microsoft\Terminal Server Client\Default\AddIns\RDPDR\<printer_name>
      mstscax!W32DrPRN::DeletePrinterCacheInfo
    seq:
      - id: printer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the PrinterName field, including
          its null terminator.
      - id: printer_name
        type: strz
        encoding: utf-16le
        size: printer_name_len
        doc: |
          A null-terminated Unicode string that specifies the printer that was deleted.

  dr_prn_rename_cachedata:
    doc: |
      MS-RDPEPC 2.2.2.6 Rename Printer Cachedata (DR_PRN_RENAME_CACHEDATA)
      This message is sent by the server when the user renames a redirected printer.
      THE FOLLOWING REGISTRY KEY WILL BE RENAMED IN THE FOLLOWING REGISTRY KEY IN THE CLIENT:
        - HIVE: HKEY_CURRENT_USER
        - SRC: \Software\Microsoft\Terminal Server Client\Default\AddIns\RDPDR\<old_printer_name>
        - DST: \Software\Microsoft\Terminal Server Client\Default\AddIns\RDPDR\<new_printer_name>
        - VALUE: PrinterCacheData
        - TYPE: REG_BINARY
      mstscax!W32DrPRN::RenamePrinterCacheInfo
      mstscax!DrPRN::UpdatePrinterNameInCacheData
      mstscax!W32DrPRN::RenamePrinter
    seq:
      - id: old_printer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the OldPrinterName field,
          including its null terminator.
      - id: new_printer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the NewPrinterName field,
          including its null terminator.
      - id: old_printer_name
        type: strz
        encoding: utf-16le
        size: old_printer_name_len
        doc: |
          A null-terminated Unicode string that specifies the printer name before the rename operation.
      - id: new_printer_name
        type: strz
        encoding: utf-16le
        size: new_printer_name_len
        doc: |
          A null-terminated Unicode string that specifies the printer name after the rename operation.

  dr_core_server_announce_req:
    doc: |
      MS-RDPEFS 2.2.2.2 Server Announce Request (DR_CORE_SERVER_ANNOUNCE_REQ)
      The server initiates the protocol with this message.
      mstscax!ProcObj::ProcessCoreServerPacket
      mstscax!ProcObj::MsgCoreAnnounce
    seq:
      - id: version_major
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the server major version number.
          This field MUST be set to 0x0001.
      - id: version_minor
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the server minor version number.
      - id: client_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the unique ID generated by the server as specified in
          section 3.3.5.1.2.

  dr_core_clientid_confirm:
    doc: |
      MS-RDPEFS 2.2.2.6 Server Client ID Confirm (DR_CORE_SERVER_CLIENTID_CONFIRM)
      The server confirms the client ID sent by the client in the Client Announce Reply message.
      mstscax!ProcObj::ProcessCoreServerPacket
      mstscax!ProcObj::AnnounceClientCapability
    seq:
      - id: version_major
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the major version number of the file system redirection
          protocol. This field MUST be set to 0x0001.
      - id: version_minor
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the minor version number of the file system redirection
          protocol.
      - id: client_id
        type: u4
        doc: |
          A 32-bit unsigned integer that confirms the unique ID value of the ClientId field, which was sent
          by the client in the Client Announce Reply message.

  dr_core_client_name_req:
    doc: |
      MS-RDPEFS 2.2.2.4 Client Name Request (DR_CORE_CLIENT_NAME_REQ)
      The client announces its machine name.
    seq:
      - id: unicode_flag
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the format of the ComputerName field. Only the least
          significant bit of this field is valid (the most significant 31 bits MUST be ignored). The least
          significant bit MUST be set to one of the following values:
            - 0x1: ComputerName is in Unicode characters.
            - 0x0: ComputerName is in ASCII characters.
      - id: code_page
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the code page of the ComputerName field; it MUST be set to 0.
      - id: computer_name_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the ComputerName field, including null
          terminator.
      - id: computer_name_a
        type: str
        encoding: ascii
        size: computer_name_len
        if: not unicode
        doc: |
          A variable-length array of ASCII, the format of which is determined by the UnicodeFlag field. This is a
          string that identifies the client computer name. The string MUST be null-terminated. The protocol imposes
          no limitations on the characters used in this field.
      - id: computer_name_w
        type: str
        encoding: utf-16le
        size: computer_name_len
        if: unicode
        doc: |
          A variable-length array of Unicode characters, the format of which is determined by theUnicodeFlag field.
          This is a string that identifies the client computer name. The string MUST be null-terminated. The protocol
          imposes no limitations on the characters used in this field.
    instances:
      unicode:
        value: 'unicode_flag & 0x1 != 0'

  device_announce:
    doc: |
      MS-RDPEFS 2.2.1.3 Device Announce Header (DEVICE_ANNOUNCE)
      This header is embedded in the Client Device List Announce message. Its purpose is to describe
      different types of devices.
    seq:
      - id: device_type
        type: u4
        enum: rdpdr_dtyp
        doc: |
          A 32-bit unsigned integer that identifies the device type.
      - id: device_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies a unique ID that identifies the announced device. This ID MUST be
          reused if the device is removed by means of the Client Drive Device List Remove packet specified in
          section 2.2.3.2.
      - id: preferred_dos_name
        type: strz
        encoding: ascii
        size: 8
        doc: |
          A string of ASCII characters (with a maximum length of eight characters) that represents the name of the
          device as it appears on the client. This field MUST be null-terminated, so the maximum device name is 7
          characters long. The following characters are considered invalid for the PreferredDosName field:
            <, >, ", /, \, |
          If any of these characters are present, the DR_CORE_DEVICE_ANNOUNC_RSP packet for this device (section 2.2.2.1)
          will be sent with STATUS_ACCESS_DENIED set in the ResultCode field.
          If DeviceType is set to RDPDR_DTYP_SMARTCARD, the PreferredDosName MUST be set to "SCARD".
          Note A column character, ":", is valid only when present at the end of the PreferredDosName field, otherwise
          it is also considered invalid.
      - id: device_data_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the DeviceData field.
      - id: device_data
        size: device_data_length
        doc: |
          A variable-length byte array whose size is specified by the DeviceDataLength field. The content depends on
          the DeviceType field. See [MS-RDPEPC] section 2.2.2.1 for the printer device type. See [MS-RDPESP] section
          2.2.2.1 for the serial and parallel port device types. See section 2.2.3.1 of this protocol for the file
          system device type. For a smart card device, the DeviceDataLength field MUST be set to zero. See [MS-RDPESC]
          for details about the smart card device type.
    enums:
      rdpdr_dtyp:
        0x00000001:
          id: serial
          doc: Serial port device
        0x00000002:
          id: parallel
          doc: Parallel port device
        0x00000004:
          id: print
          doc: Printer device
        0x00000008:
          id: filesystem
          doc: File system device
        0x00000020:
          id: smartcard
          doc: Smart card device

  dr_core_devicelist_announce_req:
    doc: |
      MS-RDPEFS 2.2.2.9 Client Device List Announce Request (DR_CORE_DEVICELIST_ANNOUNCE_REQ)
      The client announces the list of devices to redirect on the server.
    seq:
      - id: device_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of items in the DeviceList array.
      - id: device_list
        type: device_announce
        repeat: expr
        repeat-expr: device_count
        doc: |
          A variable-length array of DEVICE_ANNOUNCE (section 2.2.1.3) headers. This field specifies a list of
          devices that are being announced. The number of entries is specified by the DeviceCount field. There is
          no alignment padding between individual DEVICE_ANNOUNCE structures. They are ordered sequentially
          within this packet.

  dr_core_device_announce_rsp:
    doc: |
      MS-RDPEFS 2.2.2.1 Server Device Announce Response (DR_CORE_DEVICE_ANNOUNCE_RSP)
      The server responds to a Client Device List Announce Request with this message.
      mstscax!ProcObj::ProcessCoreServerPacket
    seq:
      - id: device_id
        type: u4
        doc: |
          A 32-bit unsigned integer. This ID MUST be the same as one of the IDs specified in the Client Device
          List Announce Request message. The server sends a separate Server Device Announce Response message for
          each announced device.
      - id: result_code
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the NTSTATUS code that indicates the success or failure of
          device initialization. NTSTATUS codes are specified in [MS-ERREF] section 2.3.

  dr_device_iorequest:
    doc: |
      MS-RDPEFS 2.2.1.4 Device I/O Request (DR_DEVICE_IOREQUEST)
      This header is embedded in all server requests on a specific device.
      mstscax!ProcObj::ProcessCoreServerPacket
      mstscax!ProcObj::ProcessIORequestPacket
      mstscax!DrDevice::ProcessIORequest
      mstscax!DrDevice::DefaultIORequestMsgHandle (default handler)
    seq:
      - id: device_id
        type: u4
        doc: |
          A 32-bit unsigned integer that is a unique ID. The value MUST match the DeviceId value in the Client
          Device List Announce Request (section 2.2.2.9).
      - id: file_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies a unique ID retrieved from the Device Create Response
          (section 2.2.1.5.1).
      - id: completion_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies a unique ID for each request. The ID is considered valid
          until a Device I/O Response (section 2.2.1.5) is received. Subsequently, the ID MUST be reused.
      - id: major_function
        type: u4
        enum: irp_mj
        doc: |
          A 32-bit unsigned integer that identifies the request function.
      - id: minor_function
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is valid only when the MajorFunction field is set to
          IRP_MJ_DIRECTORY_CONTROL. If the MajorFunction field is set to another value, the MinorFunction field
          value SHOULD be 0x00000000;<5> otherwise, the MinorFunction field MUST have one of the following values.
      - id: data
        type:
          switch-on: major_function
          -name: type
          cases:
            'irp_mj::create': dr_create_req
            'irp_mj::close': dr_close_req
            'irp_mj::read': dr_read_req
            'irp_mj::write': dr_write_req
            'irp_mj::query_information': dr_drive_query_information_req
            'irp_mj::set_information': dr_drive_set_information_req
            'irp_mj::flush_buffers': dr_device_flush_buffers_req
            'irp_mj::query_volume_information': dr_drive_query_volume_information_req
            'irp_mj::set_volume_information': dr_drive_set_volume_information_req
            'irp_mj::directory_control': dr_drive_directory_control_req(minor_function)
            'irp_mj::device_control': dr_control_req
            'irp_mj::internal_device_control': dr_internal_control_req
            'irp_mj::shutdown': dr_shutdown_req
            'irp_mj::lock_control': dr_drive_lock_req
            'irp_mj::cleanup': dr_cleanup_req
            'irp_mj::query_sd_info': dr_drive_query_sd_info_req
            'irp_mj::set_sd_info': dr_drive_set_sd_info_req
    enums:
      irp_mj:
        0x00000000:
          id: create
          doc: Create request
        0x00000002:
          id: close
          doc: Close request
        0x00000003:
          id: read
          doc: Read request
        0x00000004:
          id: write
          doc: Write request
        0x00000005:
          id: query_information
          doc: Query information request
        0x00000006:
          id: set_information
          doc: Set information request
        0x00000009:
          id: flush_buffers
          doc: (UNDOCUMENTED) Flush buffers request
        0x0000000A:
          id: query_volume_information
          doc: Query volume information request
        0x0000000B:
          id: set_volume_information
          doc: Set volume information request
        0x0000000C:
          id: directory_control
          doc: Directory control request
        0x0000000E:
          id: device_control
          doc: Device control request
        0x0000000F:
          id: internal_device_control
          doc: (UNDOCUMENTED) Internal device control request
        0x00000010:
          id: shutdown
          doc: (UNDOCUMENTED) Shutdown request
        0x00000011:
          id: lock_control
          doc: File lock control request
        0x00000012:
          id: cleanup
          doc: (UNDOCUMENTED) Cleanup request
        0x00000014:
          id: query_sd_info
          doc: (UNDOCUMENTED) Query SD info request
        0x00000015:
          id: set_sd_info
          doc: (UNDOCUMENTED) Set SD info request
      irp_mn:
        0x00000001:
          id: query_directory
          doc: Query directory request
      irp_mn:
        0x00000002:
          id: notify_change_directory
          doc: Notify change directory request

  dr_create_req:
    doc: |
      MS-RDPEFS 2.2.1.4.1 Device Create Request (DR_CREATE_REQ)
      This header initiates a create request. This message can have different purposes depending on the
      device for which it is issued. The device type is determined by the DeviceId field in the
      DR_DEVICE_IOREQUEST header.
      mstscax!W32DrDeviceAsync::MsgIrpCreate
      mstscax!W32DrAutoPrn::MsgIrpCreate
      mstscax!W32DrDeviceAsync::_AsyncMsgIrpCreateFunc
      mstscax!W32DrDeviceAsync::AsyncMsgIrpCreateFunc
      mstscax!W32DrDeviceOverlapped::MsgIrpCreate
      mstscax!W32DrAutoPrn::AsyncMsgIrpCreateFunc
    seq:
      - id: desired_access
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the level of access.
          This field is specified in [MS-SMB2] section 2.2.13.
      - id: allocation_size
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies the initial allocation size for the file.
      - id: file_attributes
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the attributes for the file being created.
          This field is specified in [MS-SMB2] section 2.2.13.
      - id: shared_access
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the sharing mode for the file being opened.
          This field is specified in [MS-SMB2] section 2.2.13.
      - id: create_disposition
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the action for the client to take if the file
          already exists. This field is specified in [MS-SMB2] section 2.2.13.
          For ports and other devices, this field MUST be set to FILE_OPEN (0x00000001).
      - id: create_options
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the options for creating the file.
          This field is specified in [MS-SMB2] section 2.2.13.
      - id: path_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Path field, including
          the null-terminator.
      - id: path
        type: strz
        encoding: utf-16le
        size: path_length
        doc: |
          A variable-length array of Unicode characters, including the null-terminator, whose size is
          specified by the PathLength field. The protocol imposes no limitations on the characters
          used in this field.

  dr_close_req:
    doc: |
      MS-RDPEFS 2.2.1.4.2 Device Close Request (DR_CLOSE_REQ)
      This header initiates a close request. This message can have different purposes depending on the
      device for which it is issued. The device type is determined by the DeviceId field in the
      DR_DEVICE_IOREQUEST header.
      mstscax!W32DrDevice::MsgIrpClose
      mstscax!W32DrDeviceAsync::AsyncMsgIrpCloseFunc
      mstscax!W32DrAutoPrn::MsgIrpClose
      mstscax!W32DrDeviceAsync::_AsyncMsgIrpCloseFunc
      mstscax!W32DrAutoPrn::AsyncMsgIrpCloseFunc
    seq:
      - id: padding
        size: 32
        doc: |
          An array of 32 bytes. Reserved. This field can be set to any value, and MUST be ignored.

  dr_read_req:
    doc: |
      MS-RDPEFS 2.2.1.4.3 Device Read Request (DR_READ_REQ)
      This header initiates a read request. This message can have different purposes depending on the
      device for which it is issued. The device type is determined by the DeviceId field in the
      DR_DEVICE_IOREQUEST header.
      mstscax!W32DrAutoPrn::MsgIrpRead
      mstscax!W32DrDeviceOverlapped::MsgIrpRead
      mstscax!W32DrDeviceOverlapped::MsgIrpReadWrite
      mstscax!W32DrDeviceAsync::StartIOFunc
      mstscax!W32DrDeviceAsync::AsyncReadIOFunc
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the maximum number of bytes to be read from
          the device.
      - id: offset
        type: u8
        doc: |
          A 64-bit unsigned integer. This field specifies the file offset where the read operation is
          performed.
      - id: padding
        size: 20
        doc: |
          An array of 20 bytes. Reserved. This field can be set to any value and MUST be ignored.

  dr_write_req:
    doc: |
      MS-RDPEFS 2.2.1.4.4 Device Write Request (DR_WRITE_REQ)
      This header initiates a write request. This message can have different purposes depending on the
      device for which it is issued. The device type is determined by the DeviceId field in the
      DR_DEVICE_IOREQUEST header.
      mstscax!W32DrDeviceAsync::MsgIrpWrite
      mstscax!W32DrDeviceAsync::MsgIrpReadWrite
      mstscax!W32DrDeviceAsync::AsyncWriteIOFunc
      mstscax!W32DrDeviceAsync::StartIOFunc
      mstscax!W32DrDeviceAsync::AsyncWriteIOFunc
      mstscax!W32DrAutoPrn::AsyncWriteIOFunc
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the WriteData field.
      - id: offset
        type: u8
        doc: |
          A 64-bit unsigned integer. This field specifies the file offset at which the data MUST be written.
          If the client advertised a minor version of at least 0x000D in the Client Announce Reply
          (section 2.2.2.3), then a value of 0xFFFFFFFFFFFFFFFF indicates that the client MUST treat this
          write request as an append operation.<6>
      - id: padding
        size: 20
        doc: |
          An array of 20 bytes. Reserved. This field can be set to any value and MUST be ignored.
      - id: write_data
        size: length
        doc: |
          A variable-length array of bytes, where the length is specified by the Length field in this packet.
          This array contains data to be written on the target device.

  dr_drive_query_information_req:
    doc: |
      MS-RDPEFS 2.2.3.3.8 Server Drive Query Information Request (DR_DRIVE_QUERY_INFORMATION_REQ)
      The server issues a query information request on a redirected file system device.
      mstscax!W32Drive::MsgIrpQueryFileInfo
    seq:
      - id: fs_information_class
        type: u4
        doc: |
          A 32-bit unsigned integer. The possible values for this field are defined in [MS-FSCC] section 2.4.
          This field MUST contain one of the following values:
            - 0x00000004: FileBasicInformation
                          This information class is used to query a file for the times of creation, last access,
                          last write, and change, in addition to file attribute information. The Reserved field of
                          the FileBasicInformation structure ([MS-FSCC] section 2.4.7) MUST NOT be present.
            - 0x00000005: FileStandardInformation
                          This information class is used to query for file information such as allocation size,
                          end-of-file position, and number of links. The Reserved field of the FileStandardInformation
                          structure ([MS-FSCC] section 2.4.38) MUST NOT be present.
            - 0x00000023: FileAttributeTagInformation
                          This information class is used to query for file attribute and reparse tag information.
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the QueryBuffer field.
      - id: padding
        size: 24
        doc: |
          An array of 24 bytes. This field is unused and MUST be ignored.
      - id: query_buffer
        size: length
        doc: |
          A variable-length array of bytes. The size of the array is specified by the Length field. The content of this
          field is based on the value of the FsInformationClass field, which determines the different structures that MUST
          be contained in the QueryBuffer field. For a complete list of these structures, see [MS-FSCC] section 2.4. The
          "File information class" table defines all the possible values for the FsInformationClass field.

  dr_drive_set_information_req:
    doc: |
      MS-RDPEFS 2.2.3.3.9 Server Drive Set Information Request (DR_DRIVE_SET_INFORMATION_REQ)
      The server issues a set information request on a redirected file system device.
      mstscax!W32Drive::MsgIrpSetFileInfo
    seq:
      - id: fs_information_class
        type: u4
        doc: |
          A 32-bit unsigned integer. The possible values for this field are defined in [MS-FSCC] section 2.4. The
          FsInformationClass field is a 32-bit value, even though the values described in [MS-FSCC] are single byte only.
          For the purposes of conversion, the highest 24 bits are always set to zero.
          This field MUST contain one of the following values:
            - 0x00000004: FileBasicInformation
                          This information class is used to set file information such as the times of creation, last access,
                          last write, and change, in addition to file attributes.
            - 0x00000014: FileEndOfFileInformation
                          This information class is used to set end-of-file information for a file.
            - 0x0000000D: FileDispositionInformation
                          This information class is used to mark a file for deletion.
            - 0x0000000A: FileRenameInformation
                          This information class is used to rename a file.
            - 0x00000013: FileAllocationInformation
                          This information class is used to set the allocation size for a file.
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the SetBuffer field.
      - id: padding
        size: 24
        doc: |
          An array of 24 bytes. This field is unused and MUST be ignored.
      - id: set_buffer
        size: length
        doc: |
          A variable-length array of bytes. The size of the array is specified by the Length field. The content of this
          field is based on the value of the FsInformationClass field, which determines the different structures that MUST
          be contained in the SetBuffer field. For a complete list of these structures, refer to [MS-FSCC] section 2.4.

  dr_device_flush_buffers_req:
    doc: |
      UNDOCUMENTED
      Flushes the buffers on a specified file and causes all buffered data to be written to a file.
      mstscax!W32DrDevice::MsgIrpFlushBuffers
      kernel32!FlushFileBuffers
      (no data)

  dr_drive_query_volume_information_req:
    doc: |
      MS-RDPEFS 2.2.3.3.6 Server Drive Query Volume Information Request (DR_DRIVE_QUERY_VOLUME_INFORMATION_REQ)
      The server issues a query volume information request on a redirected file system device.
      mstscax!W32Drive::MsgIrpQueryVolumeInfo
    seq:
      - id: fs_information_class
        type: u4
        doc: |
          A 32-bit unsigned integer. The possible values for this field are specified in [MS-FSCC] section 2.5.
          This field MUST contain one of the following values:
            - 0x00000001: FileFsVolumeInformation
                          Used to query information for a volume on which a file system is mounted. The Reserved field of
                          the FileFsVolumeInformation structure ([MS-FSCC] section 2.5.9) MUST NOT be present.
            - 0x00000003: FileFsSizeInformation
                          Used to query sector size information for a file system volume.
            - 0x00000005: FileFsAttributeInformation
                          Used to query attribute information for a file system.
            - 0x00000007: FileFsFullSizeInformation
                          Used to query sector size information for a file system volume.
            - 0x00000004: FileFsDeviceInformation
                          Used to query device information for a file system volume.
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the QueryVolumeBuffer field.
      - id: padding
        size: 24
        doc: |
          An array of 24 bytes. This field is unused and MUST be ignored.
      - id: query_volume_buffer
        size: length
        doc: |
          A variable-length array of bytes. The size of the array is specified by the Length field. The content of this
          field is based on the value of the FsInformationClass field, which determines the different structures that
          MUST be contained in the QueryVolumeBuffer field. For a complete list of these structures, refer to [MS-FSCC]
          section 2.5. The "File system information class" table defines all the possible values for the FsInformationClass
          field.

  dr_drive_set_volume_information_req:
    doc: |
      MS-RDPEFS 2.2.3.3.7 Server Drive Set Volume Information Request (DR_DRIVE_SET_VOLUME_INFORMATION_REQ)
      The server issues a set volume information request on a redirected file system device.
      mstscax!W32Drive::MsgIrpSetVolumeInfo
    seq:
      - id: fs_information_class
        type: u4
        doc: |
          A 32-bit unsigned integer. The possible values for this field are defined in [MS-FSCC] section 2.5.
          This field MUST contain the following value:
            - 0x00000002: FileFsLabelInformation
                          Used to set the label for a file system volume.
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the SetVolumeBuffer field.
      - id: padding
        size: 24
        doc: |
          An array of 24 bytes. This field is unused and MUST be ignored.
      - id: set_volume_buffer
        size: length
        doc: |
          A variable-length array of bytes. The size of the array is specified by the Length field. The content of
          this field is based on the value of the FsInformationClass field, which determines the different structures
          that MUST be contained in the SetVolumeBuffer field. For a complete list of these structures, refer to
          [MS-FSCC] section 2.5. The "File system information class" table defines all the possible values for the
          FsInformationClass field.

  dr_drive_directory_control_req:
    doc: |
      Can be DR_DRIVE_QUERY_DIRECTORY_REQ or DR_DRIVE_NOTIFY_CHANGE_DIRECTORY_REQ depending on
      the MinorFunction.
      mstscax!W32Drive::MsgIrpDirectoryControl
    params:
      - id: minor_function
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is valid only when the MajorFunction field is set to
          IRP_MJ_DIRECTORY_CONTROL. If the MajorFunction field is set to another value, the MinorFunction field
          value SHOULD be 0x00000000;<5> otherwise, the MinorFunction field MUST have one of the following values.
    seq:
      - id: data
        type:
          switch-on: minor_function
          -name: enum
          cases:
            0: dr_drive_query_directory_req
            1: dr_drive_query_directory_req
            2: dr_drive_notify_change_directory_req

  dr_drive_notify_change_directory_req:
    doc: |
      MS-RDPEFS 2.2.3.3.11 Server Drive NotifyChange Directory Request (DR_DRIVE_NOTIFY_CHANGE_DIRECTORY_REQ)
      The server issues a notify change directory request on a redirected file system device to request directory
      change notification.
      mstscax!W32Drive::StartFSFunc
      mstscax!W32Drive::_AsyncDirCtrlFunc
      mstscax!W32Drive::AsyncNotifyChangeDir
    seq:
      - id: watch_tree
        type: u1
        doc: |
          An 8-bit unsigned integer. If nonzero, a change anywhere within the tree MUST trigger the notification
          response; otherwise, only a change in the root directory will do so.
      - id: completion_filter
        type: u4
        doc: |
          A 32-bit unsigned integer. This field has the same meaning as the CompletionFilter field in the SMB2
          CHANGE_NOTIFY Request message specified in [MS-SMB2] section 2.2.35.
      - id: padding
        size: 27
        doc: |
          An array of 27 bytes. This field is unused and MUST be ignored.

  dr_drive_query_directory_req:
    doc: |
      MS-RDPEFS 2.2.3.3.10 Server Drive Query Directory Request (DR_DRIVE_QUERY_DIRECTORY_REQ)
      The server issues a query directory request on a redirected file system device. This request is used to obtain
      a directory enumeration.
      mstscax!W32Drive::MsgIrpQueryDirectory
    seq:
      - id: fs_information_class
        type: u4
        doc: |
          A 32-bit unsigned integer. The possible values are specified in [MS-FSCC] section 2.4. This field MUST contain
          one of the following values:
            - 0x00000001: FileDirectoryInformation
                          Basic information about a file or directory. Basic information is defined as the file's name,
                          time stamp, and size, or its attributes.
            - 0x00000002: FileFullDirectoryInformation
                          Full information about a file or directory. Full information is defined as all the basic
                          information, plus extended attribute size.
            - 0x00000003: FileBothDirectoryInformation
                          Basic information plus extended attribute size and short name about a file or directory.
                          The Reserved field of the FileBothDirectoryInformation structure ([MS-FSCC] section 2.4.8) 
                          UST NOT be present.
            - 0x0000000C: FileNamesInformation
                          Detailed information on the names of files in a directory.
      - id: initial_query
        type: u1
        doc: |
          An 8-bit unsigned integer. If the value of this field is zero, the request is for the next file in the directory
          that was specified in a previous Server Drive Query Directory Request. If such a file does not exist, the client
          MUST complete this request with STATUS_NO_MORE_FILES in the IoStatus field of the Client Drive I/O Response packet
          (section 2.2.3.4). If the value of this field is non-zero and such a file does not exist, the client MUST complete
          this request with STATUS_NO_SUCH_FILE in the IoStatus field of the Client Drive I/O Response.
      - id: path_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Path field, including the null-terminator.
      - id: padding
        size: 23
        doc: |
          An array of 23 bytes. This field is unused and MUST be ignored.
      - id: path
        size: path_length
        doc: |
          A variable-length array of Unicode characters that specifies the directory on which this operation will be performed.
          The Path field MUST be null-terminated. If the value of the InitialQuery field is zero, then the contents of the
          Path field MUST be ignored, irrespective of the value specified in the PathLength field.

  dr_control_req:
    doc: |
      MS-RDPEFS 2.2.1.4.5 Device Control Request (DR_CONTROL_REQ)
      This header initiates a device control request. This message can have different purposes depending on the device for
      which it is issued. The device type is determined by the DeviceId field in the DR_DEVICE_IOREQUEST header.
      mstscax!W32Drive::MsgIrpDeviceControl
      mstscax!W32DrDeviceAsync::StartIOFunc
      mstscax!W32DrDeviceAsync::AsyncIOCTLFunc
      mstscax!W32DrPRT::MsgIrpDeviceControl
      mstscax!DrPRT::MsgIrpDeviceControlTranslate
      mstscax!*::SerialXoffCounter
      mstscax!W32DrPRT::SerialGetProperties
      mstscax!*::SerialGetDTRRTS
      mstscax!*::SerialLSRMSTInsert
      mstscax!W32DrPRT::SerialConfigSize
      mstscax!W32DrPRT::SerialGetConfig
      mstscax!*::SerialGetStats
      mstscax!*::SerialClearStats
      mstscax!W32DrPRT::SerialGetCommStatus
      mstscax!W32DrPRT::SerialPurge
      mstscax!DrPRT::SerialGetBaudRate
      mstscax!DrPRT::SerialGetLineControl
      mstscax!W32DrPRT::SerialGetChars
      mstscax!W32DrPRT::SerialSetChars
      mstscax!W32DrPRT::SerialGetHandflow
      mstscax!W32DrPRT::SerialSetHandflow
      mstscax!W32DrPRT::SerialGetModemStatus
      mstscax!W32DrPRT::SerialWaitOnMask
      mstscax!W32DrPRT::_StartWaitOnMaskFunc
      mstscax!W32DrPRT::StartWaitOnMaskFunc
      mstscax!DrPRT::SerialClearDTR
      mstscax!DrPRT::SerialHandleEscapeCode
      mstscax!W32DrPRT::SerialResetDevice
      mstscax!DrPRT::SerialSetRTS
      mstscax!DrPRT::SerialClearRTS
      mstscax!DrPRT::SerialSetXOff
      mstscax!DrPRT::SerialSetXon
      mstscax!W32DrPRT::SerialGetWaitMask
      mstscax!W32DrPRT::SerialSetWaitMask
      mstscax!DrPRT::SerialSetDTR
      mstscax!DrPRT::SerialSetBaudRate
      mstscax!W32DrPRT::SerialSetQueueSize
      mstscax!DrPRT::SerialSetLineControl
      mstscax!DrPRT::SerialSetBreakOn
      mstscax!DrPRT::SerialSetBreakOff
      mstscax!DrPRT::SerialImmediateChar
      mstscax!W32DrPRT::SerialSetTimeouts
      mstscax!W32DrPRT::SerialGetTimeouts
      mstscax!W32DrDeviceAsync::DispatchIOCTLDirectlyToDriver
      mstscax!W32SCard::MsgIrpDeviceControl
    seq:
      - id: output_buffer_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the maximum number of bytes expected in the OutputBuffer field of the
          Device Control Response (section 2.2.1.5.5).
      - id: input_buffer_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the InputBuffer field.
      - id: io_control_code
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is specific to the redirected device.
      - id: padding
        size: 20
        doc: |
          An array of 20 bytes. Reserved. This field can be set to any value and MUST be ignored.
      - id: input_buffer
        size: input_buffer_length
        doc: |
          A variable-size byte array whose size is specified by the InputBufferLength field.

  dr_internal_control_req:
    doc: |
      UNDOCUMENTED
      mstscax!*::MsgIrpInternalDeviceControl

  dr_shutdown_req:
    doc: |
      UNDOCUMENTED
      mstscax!*::MsgIrpShutdown

  dr_drive_lock_req:
    doc: |
      MS-RDPEFS 2.2.3.3.12 Server Drive Lock Control Request (DR_DRIVE_LOCK_REQ)
      The server issues a request to lock or unlock portions of a file.
      mstscax!W32Drive::MsgIrpLockControl
    seq:
      - id: operation
        type: u4
        enum: rdp_lowio_op
        doc: |
          A 32-bit unsigned integer that specifies the type of the locking operation.
      - id: padding
        type: b31
        doc: |
          31 bits of padding. This field is unused and MUST be ignored.
      - id: f
        type: b1
        doc: |
          If this bit is set, the client MUST wait for the locking operation to complete. If this bit is not
          set and the region cannot be locked, the request SHOULD fail.
      - id: num_locks
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of RDP_LOCK_INFO structures in the Locks array.
      - id: padding2
        size: 20
        doc: |
          An array of 20 bytes. Reserved. This field can be set to any value and MUST be ignored.
      - id: locks
        type: rdp_lock_info
        repeat: expr
        repeat-expr: num_locks
        doc: |
          A variable-length array of RDP_LOCK_INFO structures. This field specifies one or more regions of the
          file to lock or unlock.
    enums:
      rdp_lowio_op:
        0x00000002:
          id: sharedlock
          doc: The server is requesting a shared lock.
        0x00000003:
          id: exclusivelock
          doc: The server is requesting an exclusive lock.
        0x00000004:
          id: unlock
          doc: The server is requesting to unlock a portion of the file.
        0x00000005:
          id: unlock_multiple
          doc: The server is requesting to unlock multiple portions of the file.

  rdp_lock_info:
    doc: |
      MS-RDPEFS 2.2.1.6 RDP_LOCK_INFO
      The RDP_LOCK_INFO packet specifies the region of the file to lock or unlock.
    seq:
      - id: length
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies the length of the region. A value of zero is valid and MUST
          result in locking the zero length region.
      - id: offset
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies the offset at which the region starts.

  dr_cleanup_req:
    doc: |
      UNDOCUMENTED
      mstscax!W32DrDevice::MsgIrpCleanup
      mstscax!W32DrAutoPrn::MsgIrpCleanup
      (no data)

  dr_drive_query_sd_info_req:
    doc: |
      UNDOCUMENTED
      Can be used to query the security descriptor on a given file.
      mstscax!W32Drive::MsgIrpQuerySdInfo
      advapi32!GetFileSecurityW
    seq:
      - id: security_information
        type: u4
        doc: |
          SECURTIY_INFORMATION value passed to GetFileSecurityW.
      - id: padding
        size: 28
        doc: |
          Bring size up to 32 (56 total for the packet, a requirement for IOREQUEST).

  dr_drive_set_sd_info_req:
    doc: |
      UNDOCUMENTED
      Can be used to set the security descriptor on a given file.
      mstscax!W32Drive::MsgIrpSetSdInfo
      advapi32!SetFileSecurityW
    seq:
      - id: security_information
        type: u4
        doc: |
          SECURTIY_INFORMATION value passed to SetFileSecurityW.
      - id: length
        type: u4
      - id: padding
        size: 24
        doc: |
          An array of 24 bytes. This field is unused and MUST be ignored.
      - id: security_descriptor
        size: length
        doc: |
          SECURITY_DESCRIPTOR passed to SetFileSecurityW. Must be at least 40 bytes.

  dr_device_iocompetion:
    doc: |
      MS-RDPEFS 2.2.1.5 Device I/O Response (DR_DEVICE_IOCOMPLETION)
      A message with this header indicates that the I/O request is complete. In a Device I/O Response message,
      a request message is matched to the Device I/O Request (section 2.2.1.4) header based on the CompletionId
      field value. There is only one response per request.
    seq:
      - id: device_id
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST match the DeviceId field in the DR_DEVICE_IOREQUEST header
          for the corresponding request.
      - id: completion_id
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST match the CompletionId field in the DR_DEVICE_IOREQUEST
          header for the corresponding request. After processing a response packet with this ID, the same ID MUST
          be reused in another request.
      - id: io_status
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the NTSTATUS code that indicates success or failure for the
          request. NTSTATUS codes are specified in [MS-ERREF] section 2.3.
      - id: data
        size-eos: true

  dr_create_rsp:
    doc: |
      MS-RDPEFS 2.2.1.5.1 Device Create Response (DR_CREATE_RSP)
      A message with this header describes a response to a Device Create Request (section 2.2.1.4.1).
    seq:
      - id: file_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies a unique ID for the created file object. The ID MUST be reused
          after sending a Device Close Response (section 2.2.1.5.2).
      - id: information
        type: u1
        doc: |
          An unsigned 8-bit integer. This field indicates the success of the Device Create Request (section 2.2.1.4.1).
          The value of the Information field depends on the value of CreateDisposition field in the Device Create
          Request (section 2.2.1.4.1). If the IoStatus field is set to 0x00000000, this field MAY be skipped,<7> in
          which case the server MUST assume that the Information field is set to 0x00. The possible values of the
          Information field are:
            0x00000000: FILE_SUPERSEDED: A new file was created.
            0x00000001: FILE_OPENED: An existing file was opened.
            0x00000003: FILE_OVERWRITTEN: An existing file was overwritten.

  dr_close_rsp:
    doc: |
      MS-RDPEFS 2.2.1.5.2 Device Close Response (DR_CLOSE_RSP)
      This message is a reply to a Device Close Request (section 2.2.1.4.2).
    seq:
      - id: padding
        type: u4
        doc: |
          An array of 4 bytes. Reserved. This field can be set to any value and MUST be ignored.

  dr_read_rsp:
    doc: |
      MS-RDPEFS 2.2.1.5.3 Device Read Response (DR_READ_RSP)
      A message with this header describes a response to a Device Read Request (section 2.2.1.4.3).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the ReadData field.
      - id: read_data
        size: length
        doc: |
          A variable-length array of bytes that specifies the output data from the read request. The length of
          ReadData is specified by the Length field in this packet.

  dr_write_rsp:
    doc: |
      MS-RDPEFS 2.2.1.5.4 Device Write Response (DR_WRITE_RSP)
      A message with this header describes a response to a Device Write Request (section 2.2.1.4.4).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes written in response to the write request.
      - id: padding
        type: u1
        doc: |
          An 8-bit unsigned integer intended to allow the client minor flexibility in determining the overall
          packet length. This field is unused and MUST be ignored.

  dr_drive_query_information_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.8 Client Drive Query Information Response (DR_DRIVE_QUERY_INFORMATION_RSP)
      This message is sent by the client as a response to the Server Drive Query Information Request
      (section 2.2.3.3.8).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Buffer field.
      - id: buffer
        size: length
        doc: |
          A variable-length array of bytes, in which the number of bytes is specified in the Length field. The
          content of this field is based on the value of the FsInformationClass field in the Server Drive Query
          Information Request message, which determines the different structures that MUST be contained in the
          Buffer field. For a complete list of these structures, refer to [MS-FSCC] section 2.4. The
          "File information class" table defines all the possible values for the FsInformationClass field.

  dr_drive_set_information_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.9 Client Drive Set Information Response (DR_DRIVE_SET_INFORMATION_RSP)
      This message is sent by the client as a response to the Server Drive Set Information Request (section 2.2.3.3.9).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST be equal to the Length field in the Server Drive Set
          Information Request (section 2.2.3.3.9).
      - id: padding
        size: 1
        doc: |
          An optional, 8-bit unsigned integer that is intended to allow the client minor flexibility in determining
          the overall packet length. This field is unused and MUST be ignored.

  dr_drive_query_volume_information_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.6 Client Drive Query Volume Information Response (DR_DRIVE_QUERY_VOLUME_INFORMATION_RSP)
      This message is sent by the client as a response to the Server Drive Query Volume Information Request
      (section 2.2.3.3.6).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Buffer field.
      - id: buffer
        size: length
        doc: |
          A variable-length array of bytes whose size is specified by the Length field. The content of this field is
          based on the value of the FsInformationClass field in the Server Drive Query Volume Information Request message,
          which determines the different structures that MUST be contained in the Buffer field. For a complete list of
          these structures, refer to [MS-FSCC] section 2.5. The "File system information class" table defines all the
          possible values for the FsInformationClass field.
      - id: padding
        size: 1
        doc: |
          An optional, 8-bit unsigned integer that is intended to allow the client minor flexibility in determining
          the overall packet length. This field is unused and MUST be ignored.

  dr_drive_set_volume_information_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.7 Client Drive Set Volume Information Response (DR_DRIVE_SET_VOLUME_INFORMATION_RSP)
      This message is sent by the client as a response to the Server Drive Set Volume Information Request
      (section 2.2.3.3.7).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer. It MUST match the Length field in the Server Drive Set Volume Information Request.

  dr_drive_notify_change_directory_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.11 Client Drive NotifyChange Directory Response (DR_DRIVE_NOTIFY_CHANGE_DIRECTORY_RSP)
      This message is sent by the client as a response to the Server Drive NotifyChange Directory Request
      (section 2.2.3.3.11).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Buffer field.
      - id: buffer
        size: length
        doc: |
          A variable-length array of bytes, in which the number of bytes is specified in the Length field. This field
          has the same meaning as the Buffer field in the SMB2 CHANGE_NOTIFY Response message specified in [MS-SMB2]
          section 2.2.36. This buffer MUST be empty when the Server Close Drive Request (section 2.2.3.3.2) message
          has been issued and no drive-specific events have occurred.
      - id: padding
        size: 1
        doc: |
          An optional, 8-bit unsigned integer intended to allow the client minor flexibility in determining the overall
          packet length. This field is unused and MUST be ignored.

  dr_drive_query_directory_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.10 Client Drive Query Directory Response (DR_DRIVE_QUERY_DIRECTORY_RSP)
      This message is sent by the client as a response to the Server Drive Query Directory Request
      (section 2.2.3.3.10).
    seq:
      - id: length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the Buffer field.
      - id: buffer
        size: length
        doc: |
          A variable-length array of bytes, in which the number of bytes is specified in the Length field. The content
          of this field is based on the value of the FsInformationClass field in the Server Drive Query Directory Request
          message, which determines the different structures that MUST be contained in the Buffer field. For a complete
          list of these structures, refer to [MS-FSCC] section 2.4. The "File information class" table defines all the
          possible values for the FsInformationClass field.
      - id: padding
        size: 1
        doc: |
          An optional, 8-bit unsigned integer intended to allow the client minor flexibility in determining the overall
          packet length. This field is unused and MUST be ignored.

  dr_control_rsp:
    doc: |
      MS-RDPEFS 2.2.1.5.5 Device Control Response (DR_CONTROL_RSP)
      A message with this header describes a response to a Device Control Request (section 2.2.1.4.5).
    seq:
      - id: output_buffer_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the OutputBuffer field.
      - id: output_buffer
        size: output_buffer_length
        doc: |
          A variable-length array of bytes whose size is specified by the OutputBufferLength field.

  dr_drive_lock_rsp:
    doc: |
      MS-RDPEFS 2.2.3.4.12 Client Drive Lock Control Response (DR_DRIVE_LOCK_RSP)
      This message is sent by the client as a response to the Server Drive Lock Control Request (section 2.2.3.3.12).
    seq:
      - id: padding
        size: 5
        doc: |
          5 bytes of padding. This field is unused and MUST be ignored.

  dr_drive_query_sd_info_rsp:
    doc: |
      UNDOCUMENTED
    seq:
      - id: output_buffer_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the size of the SECURITY_DESCRIPTOR
      - id: output_buffer
        size: output_buffer_length
        doc: |
          Returned SECURITY_DESCRIPTOR.

  dr_drive_set_sd_info_rsp:
    doc: |
      UNDOCUMENTED
    seq:
      - id: padding
        size: 5
        doc: |
          5 bytes of padding. This field is unused and MUST be ignored.

  dr_capability_set:
    doc: |
      MS-RDPEFS 2.2.1.2.1 Capability Message (CAPABILITY_SET)
      The CAPABILITY_SET structure is used to describe the type, size, and version of a capability set exchanged
      between clients and servers. All Capability Messages conform to this basic structure. The Capability Message
      is embedded in the Server Core Capability Request and Client Core Capability Response messages.
      Capability Messages are ordered as an array of CAPABILITY_SET structures contained in a Server Core Capability
      Request or Client Core Capability Response message; however, the presence of a Capability Message is
      non-compulsory. If CapabilityMessage data is not present in a Server Core Capability Request or Client Core
      Capability Response packet, it is assumed that all fields of the Capability Message are set to zero, with exception
      of the CapabilityType and CapabilityLength fields of the Header.
      mstscax!ProcObj::InitServerCapability
    seq:
      - id: capability_type
        type: u2
        enum: cap
        doc: |
          A 16-bit unsigned integer that identifies the type of capability being described.
      - id: capability_length
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies that size, in bytes, of the capability message, this header included.
      - id: version
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the capability-specific version for the specific value of CapabilityType.
      - id: data
        size: 'capability_length < 8 ? 0 : capability_length - 8'
    enums:
      cap:
        0x0001:
          id: general_type
          doc: General capability set (GENERAL_CAPS_SET)
        0x0002:
          id: printer_type
          doc: Print capability set (PRINTER_CAPS_SET)
        0x0003:
          id: port_type
          doc: Port capability set (PORT_CAPS_SET)
        0x0004:
          id: drive_type
          doc: Drive capability set (DRIVE_CAPS_SET)
        0x0005:
          id: smartcard_type
          doc: Smart card capability set (SMARTCARD_CAPS_SET)<2>

  dr_core_capability_req:
    doc: |
      MS-RDPEFS 2.2.2.7 Server Core Capability Request (DR_CORE_CAPABILITY_REQ)
      The server announces its capabilities and requests the same from the client.
      mstscax!ProcObj::ProcessCoreServerPacket
      mstscax!ProcObj::OnServerCapability
    seq:
      - id: num_capabilities
        type: u2
        doc: |
          A 16-bit integer that specifies the number of items in the CapabilityMessage array.
      - id: padding
        type: u2
        doc: |
          A 16-bit unsigned integer of padding. This field is unused and MUST be ignored.
      - id: capability_message
        type: dr_capability_set
        repeat: expr
        repeat-expr: num_capabilities
        doc: |
          An array of CAPABILITY_SET structures (section 2.2.1.2.1). The number of capabilities is specified by the
          numCapabilities field.

  dr_core_capabiltiy_rsp:
    doc: |
      MS-RDPEFS 2.2.2.8 Client Core Capability Response (DR_CORE_CAPABILITY_RSP)
      This packet is identical to Server Core Capability Request (section 2.2.2.7) with the exception that the
      PacketId field in RDPDR_HEADER (section 2.2.1.1) MUST be set to PAKID_CORE_CLIENT_CAPABILITY.
    seq:
      - id: num_capabilities
        type: u2
        doc: |
          A 16-bit integer that specifies the number of items in the CapabilityMessage array.
      - id: padding
        type: u2
        doc: |
          A 16-bit unsigned integer of padding. This field is unused and MUST be ignored.
      - id: capability_message
        type: dr_capability_set
        repeat: expr
        repeat-expr: num_capabilities
        doc: |
          An array of CAPABILITY_SET structures (section 2.2.1.2.1). The number of capabilities is specified by the
          numCapabilities field.

  dr_devicelist_remove:
    doc: |
      MS-RDPEFS 2.2.3.2 Client Drive Device List Remove (DR_DEVICELIST_REMOVE)
      The client removes a list of already-announced devices from the server.
    seq:
      - id: device_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of entries in the DeviceIds field.
      - id: device_ids
        type: u4
        repeat: expr
        repeat-expr: device_count
        doc: |
          A variable-length array of 32-bit unsigned integers that specifies device IDs. The IDs specified in this
          array match the IDs specified in the Client Device List Announce (section 2.2.3.1) packet.
          Note The client can send the DR_DEVICELIST_REMOVE message for devices that are removed after a session is
          connected. The server can accept the DR_DEVICE_REMOVE message for any removed device, including file system
          and port devices. The server can also accept reused DeviceIds of devices that have been removed, providing
          the implementation uses the DR_DEVICE_REMOVE message to do so.

  dr_core_user_loggedon:
    doc: |
      MS-RDPEFS 2.2.2.5 Server User Logged On (DR_CORE_USER_LOGGEDON)
      The server announces that it has successfully logged on to the session.
      mstscax!ProcObj::ProcessCoreServerPacket
      (no data)
