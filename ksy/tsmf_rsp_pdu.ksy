meta:
  id: tsmf_rsp_pdu
  endian: le
  imports:
    - tsmm_capabilities
    - tsmm_platform_cookie

doc: |
  MS-RDPEV 2.1 Transport
  Dynamic channel "TSMF"
  NOTE: On a WDAG host, this channel cannot be opened by the guest as the channel
  listener is never created.

seq:
  - id: interface_id
    -orig-id: InterfaceValue
    type: u4
    doc: |
      A 30-bit unsigned integer that represents the common identifier for the interface.
      The default value is 0x00000000. If the message uses this default interface ID, the
      message is interpreted for the main interface for which this channel has been
      instantiated. All other values MUST be retrieved either from an
      Interface Query response (QI_RSP), [MS-RDPEXPS] section2.2.2.1.2, or from responses
      that contain interface IDs.
      This ID is valid until an interface release (IFACE_RELEASE) message
      ([MS-RDPEXPS] section 2.2.2.2) is sent or received with that ID. After an IFACE_RELEASE
      message is processed, this ID is considered invalid.
  - id: message_id
    -orig-id: MessageId
    type: u4
    doc: |
      A 32-bit unsigned integer that represents a unique ID for the request or response pair.
      Requests and responses are matched based on this ID coupled with the InterfaceId.
  - id: message_payload
    -orig-id: messagePayload
    size-eos: true
    doc: |
      An array of unsigned 8-bit integers describing the payload of the message corresponding
      to the interface for which the packet is sent. The specific structure of the payload is
      described by the message descriptions in sections 2.2.3, 2.2.4, and 2.2.5.

enums:
  stream_id:
    0x80000000:
      id: stub
      -orig-id: STREAM_ID_STUB
      doc: |
        Indicates that the SHARED_MSG_HEADER is being used in a response message.
    0x40000000:
      id: proxy
      -orig-id: STREAM_ID_PROXY
      doc: |
        Indicates that the SHARED_MSG_HEADER is not being used in a response message.
    0x00000000:
      id: none
      -orig-id: STREAM_ID_NONE
      doc: |
        Indicates that the SHARED_MSG_HEADER is being used for interface manipulation
        capabilities exchange as specified in section 2.2.3. This value MUST NOT be used
        for any other messages.

types:
  tsmf_rimcall_qi_rsp:
    doc: |
      MS-RDPEXPS 2.2.2.1.2 Query Interface Response (QI_RSP)
      The QI_RSP message is a response to the QI_REQ request message.
      mstscax!CMessage::WriteIUnknown
      mstscax!CRIMObjManager::GetIdFromObject
    seq:
      - id: new_interface_id_count
        type: u4
      - id: new_interface_id
        -orig-id: NewInterfaceId
        type: u4
        repeat: expr
        repeat-expr: new_interface_id_count
        doc: |
          A 32-bit unsigned integer representing a new interface ID.
          This ID is valid until an IFACE_RELEASE message is sent as a response from the receiving side.
          If NewInterfaceId is omitted from the response, it MUST be assumed that the requested
          interface is not supported by the remote side.
      - id: result
        -orig-id: Result
        type: u4
        doc: |
          An HRESULT that describes the result of the call.

  tsmf_rim_exchange_capability_response:
    doc: |
      MS-RDPEV 2.2.3.2 Interface Manipulation Exchange Capabilities Response (RIM_EXCHANGE_CAPABILITY_RESPONSE)
      This message is sent by the client in response to RIM_EXCHANGE_CAPABILITY_REQUEST.
      mstscax!CProxyIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::ExchangeCapabilities
    seq:
      - id: capability_value
        -orig-id: CapabilityValue
        type: u4
        enum: rim_capability
        doc: |
          A 32-bit unsigned integer that identifies the client's capability.
      - id: result
        -orig-id: Result
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the HRESULT of the operation.
    enums:
      rim_capability:
        0x00000001:
          id: version_01
          doc: |
            The capability to indicate the basic support for interface manipulation.
            This capability MUST be present in the message.

  tsmf_exchange_capabilities_rsp:
    doc: |
      MS-RDPEV 2.2.5.1.3 Exchange Capabilities Response Message (EXCHANGE_CAPABILITIES_RSP)
      This message is used by the client as a response to the exchange capabilities request message
      (EXCHANGE_CAPABILITIES_REQ).
      mstscax!CProxyIMMServerData<IMMServerData>::ExchangeCapabilities
    seq:
      - id: num_client_capabilities
        -orig-id: numClientCapabilities
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of TSMM_CAPABILITIES
          structures in the pClientCapabilityArray field.
      - id: client_capabilities_array
        -orig-id: pClientCapabilityArray
        type: tsmm_capabilities
        repeat: expr
        repeat-expr: num_client_capabilities
        doc: |
          An array of TSMM_CAPABILITIES structures, each containing the capabilities for the client.
      - id: result
        -orig-id: Result
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the result of the operation.

  tsmf_check_format_support_rsp:
    doc: |
      MS-RDPEV 2.2.5.2.3 Check Format Support Response Message (CHECK_FORMAT_SUPPORT_RSP)
      TODO
      mstscax!CProxyIMMServerData<IMMServerData>::IsFormatSupported

  tsmf_set_topology_rsp:
    doc: |
      MS-RDPEV 2.2.5.2.6 Set Topology Response Message (SET_TOPOLOGY_RSP)
      TODO

  tsmf_shutdown_presentation_rsp:
    doc: |
      MS-RDPEV 2.2.5.2.9 Shut Down Presentation Response Message (SHUTDOWN_PRESENTATION_RSP)
      TODO
