meta:
  id: rdp8_bulk_encoded_data
  endian: le
  imports:
    - rdpgfx_pdu

doc: |
  MS-RDPEGFX 2.2.5.3 RDP8_BULK_ENCODED_DATA
  The RDP8_BULK_ENCODED_DATA structure contains a header byte and data that has been encoded
  using RDP 8.0 Bulk Compression techniques (section 3.1.9.1).
  mstscax!CDynVCChannel::OnCompressedData
  mstscax!DecompressRdp8::Decompress
  mstscax!DecodeHuffman2

seq:
  - id: header
    type: u1
    doc: |
      An 8-bit, unsigned integer that specifies the compression type and flags.
  - id: data
    size-eos: true
    doc: |
      A variable-length array of bytes that contains data encoded using RDP 8.0 Bulk
      Compression techniques. If the PACKET_COMPRESSED (0x20) flag is specified in the
      header field, then the data is compressed.

instances:
  is_compressed:
    value: (header & 0x20) != 0
  compression_type:
    value: header & 0x0f

enums:
  packet:
    0x20:
      id: compressed
      doc: The payload data in the data field is compressed.
  packet_compr_tye:
    0x4:
      id: rdp8
      doc: |
        RDP 8.0 bulk compression (see section 3.1.9.1).
        Sets the ring buffer to the following:
         - ring buffer size = 0x2625A0
         - max block size = 0x10000
        mstscax!DecompressRingBuffer::Initialize
        mstscax!DecodeHuffman2
    0x6:
      id: undocumented6
      doc: |
        RDP 8.0 bulk compression (see section 3.1.9.1).
        However, uses different sizes for the ring buffer.
        Sets the ring buffer to the following:
         - ring buffer size = 0x2000
         - max block size = 0x2000
        mstscax!DecompressRingBuffer::Initialize
        mstscax!DecodeHuffman2
