meta:
  id: rdp_guid
  endian: le

doc: |
  MS-DTYP 2.3.4.2 GUID--Packet Representation
  The packet version is used within block protocols. The following diagram represents a
  GUID as an opaque sequence of bytes.

seq:
  - id: data1
    -orig-id: Data1
    type: u4
    doc: The value of the Data1 member (section 2.3.4), in little-endian byte order.
  - id: data2
    -orig-id: Data2
    type: u2
    doc: The value of the Data2 member (section 2.3.4), in little-endian byte order.
  - id: data3
    -orig-id: Data3
    type: u2
    doc: The value of the Data3 member (section 2.3.4), in little-endian byte order.
  - id: data4
    -orig-id: Data4
    size: 8
    doc: The value of the Data4 member (section 2.3.4), in little-endian byte order.
