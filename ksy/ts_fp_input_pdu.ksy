meta:
  id: ts_fp_input_pdu
  endian: le
  imports:
    - fips_information

doc: |
  MS-RDPBCGR 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
  The Fast-Path Input Event PDU is used to transmit input events from client
  to server.<34> Fast-path revises client input packets from the first byte
  with the goal of improving bandwidth. The TPKT Header ([T123] section 8),
  X.224 Class 0 Data TPDU ([X224] section 13.7), and MCS Send Data Request
  ([T125] section 11.32) are replaced; the Security Header
  (section 2.2.8.1.1.2) is collapsed into the fast-path input header, and
  the Share Data Header (section 2.2.8.1.1.1.2) is replaced by a new
  fast-path format. The contents of the input notification events
  (section 2.2.8.1.1.3.1.1) are also changed to reduce their size,
  particularly by removing or reducing headers. Support for fast-path input
  is advertised in the Input Capability Set (section 2.2.7.1.6).

params:
  - id: first_byte
    type: u1
    -default: 0x00

seq:
  - id: length1
    type: u1
    doc: |
      An 8-bit, unsigned integer. If the most significant bit of the length1field is not set, then
      the size of the PDU is in the range 1 to 127 bytes and the length1 field contains the overall
      PDU length (the length2 field is not present in this case). However, if the most significant
      bit of the length1 field is set, then the overall PDU length is given by the low 7 bits of the
      length1 field concatenated with the 8 bits of the length2 field, in big-endian order (the length2
      field contains the low-order bits). The overall PDU length SHOULD be less than or equal to 16,383
      bytes.
  - id: length2
    type: u1
    if: (length1 & 0x80) != 0
    doc: |
      An 8-bit, unsigned integer. If the most significant bit of the length1 field is not
      set, then the length2 field is not present. If the most significant bit of the length1
      field is set, then the overall PDU length is given by the low 7 bits of the length1
      field concatenated with the 8 bits of the length2 field, in big-endian order
      (the length2 field contains the low-order bits). The overall PDU length SHOULD be
      less than or equal to 16,383 bytes.
  - id: fips_information
    type: fips_information
    if: false
    doc: |
      Optional FIPS header information, present when the Encryption Method selected by the
      server (sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_METHOD_FIPS (0x00000010).
      The Fast-Path FIPS Information structure is specified in section 2.2.8.1.2.1.
  - id: data_signature
    size: 8
    if: (flags & fastpath_input::encrypted.to_i) != 0
    doc: |
      MAC generated over the packet using one of the techniques described in section 5.3.6
      (the FASTPATH_INPUT_SECURE_CHECKSUM flag, which is set in the fpInputHeader field,
      describes the method used to generate the signature). This field MUST be present if
      the FASTPATH_INPUT_ENCRYPTED flag is set in the fpInputHeader field.
  - id: num_events_byte
    type: u1
    if: num_events_nibble == 0
    doc: |
      An 8-bit, unsigned integer. The number of fast-path input events packed together in
      the fpInputEvents field (up to 255). This field is present if the numEvents bit
      field in the fast-path header byte is zero.
  - id: input_event_array
    type: ts_fp_input_event_array
    size: 'length >= header_length ? length - header_length : 0'
    doc: |
      An array of Fast-Path Input Event (section 2.2.8.1.2.2) structures to be processed
      by the server. The number of events present in this array is given by the numEvents
      bit field in the fast-path header byte, or by the numEvents field in the Fast-Path
      Input Event PDU (if it is present).

instances:
  action:
    value: first_byte & 0x3
    enum: fastpath_input_action
    doc: |
      A 2-bit, unsigned integer that indicates whether the PDU is in fast-path or slow-path format.
  num_events_nibble:
    value: (first_byte >> 2) & 0xf
    doc: |
      A 4-bit, unsigned integer that collapses the number of fast-path
      input events packed together in the fpInputEvents field into 4
      bits if the number of events is in the range 1 to 15. If the
      number of input events is greater than 15, then the numEvents bit
      field in the fast-path header byte MUST be set to zero, and the
      numEvents optional field inserted after the dataSignature field.
      This allows up to 255 input events in one PDU.
  flags:
    value: (first_byte >> 6) & 0x3
    doc: |
      A 2-bit, unsigned integer that contains the flags describing the cryptographic
      parameters of the PDU.
  num_events_length:
    value: 'num_events_nibble != 0 ? 0 : 1'
  fips_information_length:
    value: 0
  data_signature_length:
    value: '(flags & fastpath_input::encrypted.to_i) != 0 ? 8 : 0'
  header_length_without_length:
    value: 1 + fips_information_length + data_signature_length + num_events_length
  header_length:
    value: 'header_length_without_length + ((length1 & 0x80) != 0 ? 2 : 1)'
  length1_value:
    value: (length1 << 0) &~ 0x80
  length1_bitshift:
    value: ((length1 << 0) & 0x80) >> 4
  length2_mask:
    value: (1 << length1_bitshift) - 1
  length2_value:
    value: (length2 << 0) & length2_mask
  length:
    value: length1_value << length1_bitshift | length2_value
    doc: |
      Contains the overall PDU length.
      The overall PDU length SHOULD be less than or equal to 16,383 bytes.
  num_events:
    value: 'num_events_nibble != 0 ? num_events_nibble : num_events_byte'
    doc: |
      The number of fast-path input events packed together in the fpInputEvents field (up to 255).

-update:
  - id: first_byte
    value: first_byte & 0xc3
    doc: |
      Clear the numEvents nibble.
  - id: first_byte
    value: (first_byte & 0xc3) | ((input_event_array.input_event.size << 2) & 0x3c)
    if: (input_event_array.input_event.size != 0) and (input_event_array.input_event.size <= 15)
    doc: |
      Store the number of events in numEvents nibble if less than or equal to 15.
  - id: num_events_byte
    value: input_event_array.input_event.size
    doc: |
      Store the number of event in numEvents byte.
  - id: length1
    value: (header_length_without_length + 1 + input_event_array.length) & 0x7f
    doc: |
      Assume the length is 1 byte, set the value of length1.
  - id: length1
    value: (((header_length_without_length + 2 + input_event_array.length) >> 8) & 0x7f) | 0x80
    if: (header_length_without_length + 1 + input_event_array.length) > 0x7f
    doc: |
      Wrong assumption - length is 2 bytes, set the value of length1.
  - id: length2
    value: 0x00
    doc: |
      Assume length is 1 byte, length2 is not used.
  - id: length2
    value: (header_length_without_length + 2 + input_event_array.length) & 0xff
    if: (header_length_without_length + 1 + input_event_array.length) > 0x7f
    doc: |
      Wrong assumption - length is 2 bytes, set the value of length2.

enums:
  fastpath_input_action:
    0x0:
      id: fastpath
      doc: |
        Indicates the PDU is a fast-path input PDU.
    0x3:
      id: x224
      doc: |
        Indicates the presence of a TPKT Header initial version byte, which
        indicates that the PDU is a slow-path input PDU (in this case the full
        value of the initial byte MUST be 0x03).
  fastpath_input:
    0x1:
      id: secure_checksum
      -orig-id: FASTPATH_INPUT_SECURE_CHECKSUM
      doc: |
        Indicates that the MAC signature for the PDU was generated using the
        "salted MAC generation" technique (section 5.3.6.1.1). If this bit
        is not set, then the standard technique was used
        (sections 2.2.8.1.1.2.2 and 2.2.8.1.1.2.3).
    0x2:
      id: encrypted
      -orig-id: FASTPATH_INPUT_ENCRYPTED
      doc: |
        Indicates that the PDU contains an 8-byte MAC signature after the
        optional length2 field (that is, the dataSignature field is present)
        and the contents of the PDU are encrypted using the negotiated
        encryption package (sections 5.3.2 and 5.3.6).

types:
  ts_fp_input_event_array:
    doc: |
      An array of Fast-Path Input Event (section 2.2.8.1.2.2) structures
    seq:
      - id: input_event
        type: ts_fp_input_event
        repeat: expr
        repeat-expr: _parent.num_events
        doc: |
          An array of Fast-Path Input Event (section 2.2.8.1.2.2) structures to be processed
          by the server. The number of events present in this array is given by the numEvents
          bit field in the fast-path header byte, or by the numEvents field in the Fast-Path
          Input Event PDU (if it is present).

  ts_fp_input_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2 Fast-Path Input Event (TS_FP_INPUT_EVENT)
      The TS_FP_INPUT_EVENT structure is used to describe the type and
      encapsulate the data for a fast-path input event sent from client to
      server. All fast-path input events conform to this basic structure
      (sections 2.2.8.1.2.2.1 to 2.2.8.1.2.2.6).
    seq:
      - id: code
        type: b3
        enum: fastpath_input_event
        doc: |
          A 3-bit, unsigned integer that specifies the type code of the input event.
      - id: flags
        type: b5
        doc: |
          A 5-bit, unsigned integer that contains flags specific to the input event.
      - id: event
        -set:
          - id: code
            value: _case
        type:
          switch-on: code
          -name: type
          cases:
            'fastpath_input_event::scancode': ts_fp_keyboard_event
            'fastpath_input_event::mouse': ts_fp_pointer_event
            'fastpath_input_event::mousex': ts_fp_pointerx_event
            'fastpath_input_event::sync': ts_fp_sync_event
            'fastpath_input_event::unicode': ts_fp_unicode_keyboard_event
            'fastpath_input_event::qoe_timestamp': ts_fp_qoe_timestamp_event
    enums:
      fastpath_input_event:
        0x0:
          id: scancode
          doc: Indicates a Fast-Path Keyboard Event (section 2.2.8.1.2.2.1).
        0x1:
          id: mouse
          doc: Indicates a Fast-Path Mouse Event (section 2.2.8.1.2.2.3).
        0x2:
          id: mousex
          doc: Indicates a Fast-Path Extended Mouse Event (section 2.2.8.1.2.2.4).
        0x3:
          id: sync
          doc: Indicates a Fast-Path Synchronize Event (section 2.2.8.1.2.2.5).
        0x4:
          id: unicode
          doc: Indicates a Fast-Path Unicode Keyboard Event (section 2.2.8.1.2.2.2).
        0x6:
          id: qoe_timestamp
          doc: Indicates a Fast-Path Quality of Experience (QoE) Timestamp Event (section 2.2.8.1.2.2.6).
      fastpath_input_kbdflags:
        0x01:
          id: release
          doc: |
            The absence of this flag indicates a key-down event, while its presence indicates a key-release event.
        0x02:
          id: extended
          doc: |
            Indicates that the keystroke message contains an extended scancode. For enhanced 101-key and 102-key
            keyboards, extended keys include the right ALT and right CTRL keys on the main section of the
            keyboard; the INS, DEL, HOME, END, PAGE UP, PAGE DOWN and ARROW keys in the clusters to the
            left of the numeric keypad; and the Divide ("/") and ENTER keys in the numeric keypad.
        0x04:
          id: extended1
          doc: |
            Used to send keyboard events triggered by the PAUSE key.
            A PAUSE key press and release MUST be sent as the following sequence of keyboard events:
             - CTRL (0x1D) DOWN
             - NUMLOCK (0x45) DOWN
             - CTRL (0x1D) UP
             - NUMLOCK (0x45) UP
            The CTRL DOWN and CTRL UP events MUST both include the FASTPATH_INPUT_KBDFLAGS_EXTENDED1 flag.
      fastpath_input_sync:
        0x01:
          id: scroll_lock
          doc: Indicates that the Scroll Lock indicator light SHOULD be on.
        0x02:
          id: num_lock
          doc: Indicates that the Num Lock indicator light SHOULD be on.
        0x04:
          id: caps_lock
          doc: Indicates that the Caps Lock indicator light SHOULD be on.
        0x08:
          id: sync_kana_lock
          doc: Indicates that the Kana Lock indicator light SHOULD be on.

  ts_fp_keyboard_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.1 Fast-Path Keyboard Event (TS_FP_KEYBOARD_EVENT)
      The TS_FP_KEYBOARD_EVENT structure is the fast-path variant of the
      TS_KEYBOARD_EVENT (section 2.2.8.1.1.3.1.1.1).
    seq:
      - id: key_code
        type: u1
        doc: |
          An 8-bit, unsigned integer. The scancode of the key which triggered the event.

  ts_fp_pointer_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.3 Fast-Path Mouse Event (TS_FP_POINTER_EVENT)
      The TS_FP_POINTER_EVENT structure is the fast-path variant of the TS_POINTER_EVENT
      (section 2.2.8.1.1.3.1.1.3) structure.
    seq:
      - id: wheel_rotation
        type: u1
        doc: |
          WheelRotationMask (0x01FF)
          The bit field describing the number of rotation units the mouse wheel
          was rotated. The value is negative if the PTRFLAGS_WHEEL_NEGATIVE flag
          is set.
      - id: down
        type: b1
        doc: |
          PTRFLAGS_DOWN (0x8000)
          Indicates that a click event has occurred at the position specified by
          the xPos and yPos fields. The button flags indicate which button has been
          clicked and at least one of these flags MUST be set.
      - id: button3
        type: b1
        doc: |
          PTRFLAGS_BUTTON3 (0x4000)
          Mouse button 3 (middle button or wheel) was clicked or released. If the
          PTRFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.
      - id: button2
        type: b1
        doc: |
          PTRFLAGS_BUTTON2 (0x2000)
          Mouse button 2 (right button) was clicked or released. If the PTRFLAGS_DOWN
          flag is set, then the button was clicked, otherwise it was released.
      - id: button1
        type: b1
        doc: |
          PTRFLAGS_BUTTON1 (0x1000)
          Mouse button 1 (left button) was clicked or released. If the PTRFLAGS_DOWN
          flag is set, then the button was clicked, otherwise it was released.
      - id: move
        type: b1
        doc: |
          PTRFLAGS_MOVE (0x0800)
          Indicates that the mouse position MUST be updated to the location
          specified by the xPos and yPos fields.
      - id: hwheel
        type: b1
        doc: |
          PTRFLAGS_HWHEEL (0x0400)
          The event is a horizontal mouse wheel rotation. The only valid flags in
          a horizontal wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored. This flag MUST
          NOT be sent to a server that does not indicate support for horizontal
          mouse wheel events in the Input Capability Set (section 2.2.7.1.6).
      - id: wheel
        type: b1
        doc: |
          PTRFLAGS_WHEEL (0x0200)
          The event is a vertical mouse wheel rotation. The only valid flags in
          a vertical wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored.
      - id: wheel_negative
        type: b1
        doc: |
          PTRFLAGS_WHEEL (0x0100)
          The event is a vertical mouse wheel rotation. The only valid flags in
          a vertical wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the
          WheelRotationMask; all other pointer flags are ignored.
      - id: x_pos
        type: u2
        doc: |
          A 16-bit, unsigned integer. The x-coordinate of the pointer relative to
          the top-left corner of the server's desktop. This field SHOULD be ignored
          by the server if either the PTRFLAGS_WHEEL (0x0200) or the PTRFLAGS_HWHEEL
          (0x0400) flag is specified in the pointerFlags field.
      - id: y_pos
        type: u2
        doc: |
          A 16-bit, unsigned integer. The y-coordinate of the pointer relative to
          the top-left corner of the server's desktop. This field SHOULD be ignored
          by the server if either the PTRFLAGS_WHEEL (0x0200) or the PTRFLAGS_HWHEEL
          (0x0400) flag is specified in the pointerFlags field.

  ts_fp_pointerx_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.4 Fast-Path Extended Mouse Event (TS_FP_POINTERX_EVENT)
      The TS_FP_POINTERX_EVENT structure is the fast-path variant of the
      TS_POINTERX_EVENT (section 2.2.8.1.1.3.1.1.4) structure. Support for
      the Extended Mouse Event is advertised in the Input Capability Set
      (section 2.2.7.1.6).
    seq:
      - id: pad1
        type: b6
      - id: button2
        type: b1
        doc: |
          PTRXFLAGS_BUTTON2 (0x0002)
          Extended mouse button 2 (also referred to as button 5) was clicked or released.
          If the PTRXFLAGS_DOWN flag is set, the button was clicked; otherwise, it was
          released.
      - id: button1
        type: b1
        doc: |
          PTRXFLAGS_BUTTON1 (0x0001)
          Extended mouse button 1 (also referred to as button 4) was clicked or released.
          If the PTRXFLAGS_DOWN flag is set, the button was clicked; otherwise, it was
          released.
      - id: down
        type: b1
        doc: |
          PTRXFLAGS_DOWN (0x8000)
          Indicates that a click event has occurred at the position specified by
          the xPos and yPos fields. The button flags indicate which button has been
          clicked and at least one of these flags MUST be set.
      - id: pad2
        type: b7
      - id: x_pos
        type: u2
        doc: |
          A 16-bit unsigned integer. The x-coordinate of the pointer.
      - id: y_pos
        type: u2
        doc: |
          A 16-bit unsigned integer. The y-coordinate of the pointer.

  ts_fp_sync_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.5 Fast-Path Synchronize Event (TS_FP_SYNC_EVENT)
      The TS_FP_SYNC_EVENT structure is the fast-path variant of the TS_SYNC_EVENT
      (section 2.2.8.1.1.3.1.1.5) structure.
      (no data)

  ts_fp_unicode_keyboard_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.2 Fast-Path Unicode Keyboard Event (TS_FP_UNICODE_KEYBOARD_EVENT)
      The TS_FP_UNICODE_KEYBOARD_EVENT structure is the fast-path variant of the
      TS_UNICODE_KEYBOARD_EVENT (section 2.2.8.1.1.3.1.1.2) structure. Support for the Unicode
      Keyboard Event is advertised in the Input Capability Set (section 2.2.7.1.6).
    seq:
      - id: unicode_code
        type: u2
        doc: |
          A 16-bit unsigned integer. The Unicode character input code.

  ts_fp_qoe_timestamp_event:
    doc: |
      MS-RDPBCGR 2.2.8.1.2.2.6 Fast-Path Quality of Experience (QoE) Timestamp Event (TS_FP_QOETIMESTAMP_EVENT)
      The TS_FP_QOETIMESTAMP_EVENT structure is used to enable the calculation of
      Quality of Experience (QoE) metrics. This event is sent solely for informational
      and debugging purposes and MUST NOT be transmitted to the server if the
      TS_INPUT_FLAG_QOE_TIMESTAMPS (0x0200) flag was not received in the Input Capability
      Set (section 2.2.7.1.6).
    seq:
      - id: timestamp
        type: u4
        doc: |
          A 32-bit, unsigned integer. A client-generated timestamp, in milliseconds,
          that indicates when the current input batch was encoded by the client. The
          value of the first timestamp sent by the client implicitly defines the origin
          for all subsequent timestamps. The server is responsible for handling roll-over
          of the timestamp.
