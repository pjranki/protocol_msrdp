meta:
  id: ts_cachedpointerattribute
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.1.1.4.6 Cached Pointer Update (TS_CACHEDPOINTERATTRIBUTE)
  The TS_CACHEDPOINTERATTRIBUTE structure is used to instruct the client to
  change the current pointer shape to one already present in the pointer
  cache.
  mstscax!CCM::CM_CachedPointerPDU

seq:
  - id: cache_index
    type: u2
    doc: |
      A 16-bit, unsigned integer. A zero-based cache entry containing the
      cache index of the cached pointer to which the client's pointer MUST
      be changed. The pointer data MUST have already been cached using either
      the Color Pointer Update (section 2.2.9.1.1.4.4) or New Pointer Update
      (section 2.2.9.1.1.4.5).
