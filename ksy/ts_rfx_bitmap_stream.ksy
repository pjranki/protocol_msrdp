meta:
  id: ts_rfx_bitmap_stream
  endian: le

doc: |
    MS-RDPRFX 2.2.2 Encode Messages
    An encoded RemoteFX stream comprises a sequence of encode messages. The
    sequencing and encapsulation of these messages are described in section
    3.1.8.3.1.
    mstscax!CaCpuDecompressor::Decompress
    mstscax!CaCpuDecompressor::Decode
    mstscax!Cac::DecoderCpu::Decode
    mstscax!Cac::DecoderImpl::Decode
    mstscax!CacCommon::DecStreamProcessor::Process
    mstscax!CaCpuDecompressor::Transfer

seq:
  - id: blocks
    type: ts_rfx_blockt
    repeat: eos
    doc: |
      A variable-length array of TS_RFX_BLOCKT (section 2.2.2.1.1) structures.

types:
  ts_rfx_blockt:
    doc: |
      MS-RDPRFX 2.2.2.1.1 TS_RFX_BLOCKT
      The TS_RFX_BLOCKT structure identifies the type of an encode message and
      specifies the size of the message.
      mstscax!CacCommon::DecStreamProcessor::Process
      mstscax!CacCommon::WfParser::GetNextBlock
    seq:
      - id: block_type
        type: u2
        enum: wbt
        doc: |
          A 16-bit, unsigned integer. Specifies the data block type.
      - id: block_len
        type: u4
        doc: |
          A 32-bit, unsigned integer. Specifies the size, in bytes, of the data
          block. This size includes the size of the blockType and blockLen fields,
          as well as all trailing data.
      - id: codec_id
        type: u1
        if: ts_rfx_codec_channelt
        doc: |
          An 8-bit, unsigned integer. Specifies the codec ID. This field MUST be set
          to 0x01.
      - id: channel_id
        type: u1
        if: ts_rfx_codec_channelt
        doc: |
          An 8-bit, unsigned integer. Specifies the channel ID. If the blockType is set
          to WBT_CONTEXT (0xCCC3), then channelId MUST be set to 0xFF. For all other
          values of blockType, channelId MUST be set to 0x00.
      - id: block_data
        size: block_data_size
        type:
          switch-on: block_type
          -name: type
          cases:
            'wbt::sync': ts_rfx_sync
            'wbt::codec_versions': ts_rfx_codec_versions
            'wbt::channels': ts_rfx_channels
            'wbt::context': ts_rfx_context
            'wbt::frame_begin': ts_rfx_frame_begin
            'wbt::frame_end': ts_rfx_frame_end
            'wbt::region': ts_rfx_region
            'wbt::extension': ts_rfx_tileset

    instances:
      ts_rfx_codec_channelt:
        value: '(block_type == wbt::context) or (block_type == wbt::frame_begin) or (block_type == wbt::frame_end) or (block_type == wbt::region) or (block_type == wbt::extension)'
        doc: |
          The TS_RFX_CODEC_CHANNELT structure is an extension of the TS_RFX_BLOCKT
          structure. It is present as the first field in messages that are targeted
          for a specific combination of codec and channel.
      block_data_size:
        value: 'block_len >= 6 ? block_len - 6 : 0'
    enums:
      cbt:
        0xCAC2:
          id: tileset
          doc: Used internally in a TS_RFX_TILESET (section 2.2.2.3.4) structure.
        0xCAC3:
          id: tile
          doc: A TS_RFX_TILE (section 2.2.2.3.4.1) structure.
      cby:
        0xCBC0:
          id: caps
          doc: A TS_RFX_CAPS (section 2.2.1.1.1) structure.
        0xCBC1:
          id: capset
          doc: A TS_RFX_CAPSET (section 2.2.1.1.1.1) structure.
      wbt:
        0xCCC0:
          id: sync
          doc: A TS_RFX_SYNC (section 2.2.2.2.1) structure.
        0xCCC1:
          id: codec_versions
          doc: A TS_RFX_CODEC_VERSIONS (section 2.2.2.2.2) structure.
        0xCCC2:
          id: channels
          doc: A TS_RFX_CHANNELS (section 2.2.2.2.3) structure.
        0xCCC3:
          id: context
          doc: A TS_RFX_CONTEXT (section 2.2.2.2.4) structure.
        0xCCC4:
          id: frame_begin
          doc: A TS_RFX_FRAME_BEGIN (section 2.2.2.3.1) structure.
        0xCCC5:
          id: frame_end
          doc: A TS_RFX_FRAME_END (section 2.2.2.3.2) structure.
        0xCCC6:
          id: region
          doc: A TS_RFX_REGION (section 2.2.2.3.3) structure.
        0xCCC7:
          id: extension
          doc: A TS_RFX_TILESET (section 2.2.2.3.4) structure.

  ts_rfx_sync:
    doc: |
      MS-RDPRFX 2.2.2.2.1 TS_RFX_SYNC
      The TS_RFX_SYNC message MUST be the first message in any encoded stream. The
      decoder MUST examine this message to determine whether the protocol version
      is supported.
      mstscax!CaCpuDecompressor::Initialize
      mstscax!Cac::CacCodec::InitDecoderProps
      mstscax!CacCommon::InitDecoderProps
      mstscax!CacCommon::DecStreamProcessor::InitProps
    seq:
      - id: magic
        type: u4
        doc: |
          A 32-bit, unsigned integer. This field MUST be set to WF_MAGIC (0xCACCACCA).
      - id: version
        type: u2
        doc: |
          A 16-bit, unsigned integer. Indicates the version number. This field MUST
          be set to WF_VERSION_1_0 (0x0100).

  ts_rfx_codec_versiont:
    doc: |
      MS-RDPRFX 2.2.2.1.4 TS_RFX_CODEC_VERSIONT
      The TS_RFX_CODEC_VERSIONT structure is used to specify support for a specific
      version of the RemoteFX codec.
      mstscax!CacCommon::CalDecStreamProcessor::ProcessCodecVersions
    seq:
      - id: codec_id
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies the codec ID. This field MUST be set to
          0x01. The decoder SHOULD ignore this field.
      - id: version
        type: u2
        doc: |
          A 16-bit, signed integer. This field MUST be set to 0x0100. The decoder SHOULD
          ignore this field.

  ts_rfx_codec_versions:
    doc: |
      MS-RDPRFX 2.2.2.2.2 TS_RFX_CODEC_VERSIONS
      The TS_RFX_CODEC_VERSIONS message indicates the version of the RemoteFX codec that
      is being used.
      mstscax!CacCommon::DecStreamProcessor::dispatchCodecVersions
      mstscax!CacCommon::DecStreamProcessor::checkCodecVersions
      mstscax!CacCommon::CalDecStreamProcessor::ProcessCodecVersions
      mstscax!Cac::DecoderImpl::HandleCodecVersion
    seq:
      - id: num_codecs
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies the number of codec version data blocks in
          the codecs field. This field MUST be set to 0x01.
      - id: codecs
        type: ts_rfx_codec_versiont
        repeat: expr
        repeat-expr: num_codecs
        doc: |
          A TS_RFX_CODEC_VERSIONT (section 2.2.2.1.4) structure. The codecId field MUST be
          set to 0x01 and the version field MUST be set to WF_VERSION_1_0 (0x0100).

  ts_rfx_channelt:
    doc: |
      MS-RDPRFX 2.2.2.1.3 TS_RFX_CHANNELT
      The TS_RFX_CHANNELT structure is used to specify the screen resolution of a channel.
      mstscax!CacCommon::CalDecStreamProcessor::ProcessChannels
      mstscax!Cac::DecoderImpl::HandleChannels
    seq:
      - id: channel_id
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies the identifier of the channel. This field
          MUST be set to 0x00.
      - id: width
        type: u2
        doc: |
          A 16-bit, signed integer. Specifies the frame width of the channel. This field
          SHOULD<3> be within the range of 1 to 4096 (inclusive).
      - id: height
        type: u2
        doc: |
          A 16-bit, signed integer. Specifies the frame width of the channel. This field
          SHOULD<3> be within the range of 1 to 4096 (inclusive).

  ts_rfx_channels:
    doc: |
      MS-RDPRFX 2.2.2.2.3 TS_RFX_CHANNELS
      The TS_RFX_CHANNELS message contains the list of channels. Each active monitor on the
      server must correspond to an entry in this list. The list can have more entries than
      the number of active monitors. The decoder endpoint MUST be able to support channels
      with different frame dimensions.
      mstscax!CacCommon::DecStreamProcessor::dispatchChannels
      mstscax!CacCommon::DecStreamProcessor::checkChannels
      mstscax!CacCommon::CalDecStreamProcessor::ProcessChannels
      mstscax!Cac::DecoderImpl::HandleChannels
    seq:
      - id: num_channels
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies the number of channel data blocks in the
          channels field.
      - id: channels
        type: ts_rfx_channelt
        repeat: expr
        repeat-expr: num_channels
        doc: |
          A variable-length array of TS_RFX_CHANNELT (section 2.2.2.1.3) structures. The
          number of elements in this array is specified in the numChannels field.

  ts_rfx_context:
    doc: |
      MS-RDPRFX 2.2.2.2.4 TS_RFX_CONTEXT
      The TS_RFX_CONTEXT message contains information regarding the encoding properties being
      used.
      mstscax!CacCommon::DecStreamProcessor::dispatchContext
      mstscax!CacCommon::CalDecStreamProcessor::ProcessContext
      mstscax!Cac::DecoderImpl::HandleContext
    seq:
      - id: ctx_id
        type: u1
        doc: |
          An 8-bit unsigned integer. Specifies an identifier for this context message. This
          field MUST be set to 0x00. The decoder SHOULD ignore this field.
      - id: tile_size
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the tile size used by the RemoteFX codec. This
          field MUST be set to CT_TILE_64x64 (0x0040), indicating that a tile is 64 x 64 pixels.
      - id: r
        type: b1
        doc: |
          A 1-bit field reserved for future use. This field MUST be ignored when received.
      - id: qt
        type: b2
        doc: |
          A 2-bit unsigned integer. Specifies the quantization type. This field MUST be set to
          SCALAR_QUANTIZATION (0x1). The decoder SHOULD ignore this field.
      - id: et
        type: b4
        doc: |
          A 4-bit unsigned integer. Specifies the entropy algorithm. This field MUST be set to
          one of the following values.
            - CLW_ENTROPY_RLGR1 (0x01): RLGR algorithm as detailed in 3.1.8.1.7.1.
            - CLW_ENTROPY_RLGR3 (0x04): RLGR algorithm as detailed in 3.1.8.1.7.2.
          The decoder SHOULD ignore this value and use the value defined in the properties field
          of TS_RFX_TILESET (section 2.2.2.3.4).
      - id: xft
        type: b4
        doc: |
          A 4-bit unsigned integer. Specifies the DWT. This field MUST be set to
          CLW_XFORM_DWT_53_A (0x1), which indicates the DWT given by the equations
          in sections 3.1.8.1.4 and 3.1.8.2.4.
      - id: cct
        type: b2
        doc: |
          A 2-bit unsigned integer. Specifies the color conversion transform. This field MUST be
          set to COL_CONV_ICT (0x1) to specify the transform defined by the equations in sections
          3.1.8.1.3 and 3.1.8.2.5. The decoder SHOULD ignore this field.
      - id: flags
        type: b3
        doc: |
          A 3-bit unsigned integer. Specifies operational flags.
            - CODEC_MODE (0x02): The codec is operating in image mode.
                                 If this flag is not set, the codec is operating in video mode.
          When operating in image mode, the Encode Headers messages (section 2.2.2.2) MUST always
          precede an encoded frame. When operating in video mode, the header messages MUST be present
          at the beginning of the stream and MAY be present elsewhere.

  ts_rfx_frame_begin:
    doc: |
      MS-RDPRFX 2.2.2.3.1 TS_RFX_FRAME_BEGIN
      The TS_RFX_FRAME_BEGIN message indicates the start of a new frame for a specific channel in
      the encoded stream.
      mstscax!CacCommon::DecStreamProcessor::dispatchFrameBegin
      mstscax!CacCommon::CalDecStreamProcessor::ProcessFrameBegin
      mstscax!Cac::DecoderImpl::HandleFrameBegin
    seq:
      - id: frame_idx
        type: u4
        doc: |
          A 32-bit unsigned integer. Specifies the index of the frame in the current video sequence.
          This field is used when the codec is operating in video mode, as specified using the flags
          field of the TS_RFX_CONTEXT (section 2.2.2.2.4) message. If the codec is operating in image
          mode, this field MUST be ignored. If the codec is operating in video mode, this field SHOULD
          be ignored.
      - id: num_regions
        type: u2
        doc: |
          A 16-bit signed integer. Specifies the number of TS_RFX_REGION (section 2.2.2.3.3) messages
          following this TS_RFX_FRAME_BEGIN message. That is, the number of regions in the frame.

  ts_rfx_frame_end:
    doc: |
      MS-RDPRFX 2.2.2.3.2 TS_RFX_FRAME_END
      The TS_RFX_FRAME_END message specifies the end of a frame for a specific channel in the encoded
      stream.
      mstscax!CacCommon::DecStreamProcessor::dispatchFrameEnd
      mstscax!CacCommon::CalDecStreamProcessor::ProcessFrameEnd
      mstscax!Cac::DecoderImpl::HandleFrameEnd

  ts_rfx_rect:
   doc: |
     MS-RDPRFX 2.2.2.1.6 TS_RFX_RECT
     The TS_RFX_RECT structure is used to specify a rectangle.
   seq:
     - id: x
       type: u2
       doc: |
         A 16-bit, unsigned integer. The X-coordinate of the rectangle.
     - id: y
       type: u2
       doc: |
         A 16-bit, unsigned integer. The Y-coordinate of the rectangle.
     - id: width
       type: u2
       doc: |
         A 16-bit, unsigned integer. The width of the rectangle.
     - id: height
       type: u2
       doc: |
         A 16-bit, unsigned integer. The height of the rectangle.

  ts_rfx_region:
    doc: |
      MS-RDPRFX 2.2.2.3.3 TS_RFX_REGION
      The TS_RFX_REGION message contains information about the list of change rectangles on the screen
      for a specific channel. It also specifies the number of trailing TS_RFX_TILESET (section 2.2.2.3.4)
      messages.
      mstscax!CacCommon::DecStreamProcessor::Process
      mstscax!CacCommon::DecStreamProcessor::dispatchRegion
      mstscax!CacCommon::CalDecStreamProcessor::ProcessRegion
    seq:
      - id: region_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. Contains a collection of bit-packed property fields. The format of
          this field is described by the following bitfield diagram:
            |  0  | 1 2 3 4 5 6 7 |
            | lrf |   reserved    |
          lrf (1 bit): A 1-bit unsigned integer. This field MUST be set to 0x1. The decoder SHOULD ignore
          this field.
          reserved (7 bits: A 7-bit integer reserved for future use. This field MUST be ignored.
      - id: num_rects
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the number of TS_RFX_RECT (section 2.2.2.1.6) structures
          present in the rects field. If this value is zero, the decoder MUST generate a rectangle with
          coordinates (0, 0, width, height) that reflects the width and height of the channel's frame
          (section 2.2.2.1.3).
      - id: rects
        type: ts_rfx_rect
        repeat: expr
        repeat-expr: num_rects
        doc: |
          A variable-length array of TS_RFX_RECT (section 2.2.2.1.6) structures. This array defines the
          region. The number of rectangles in the array is specified in the numRects field. Processing
          rules for the rectangles in this array are specified in section 3.1.8.2.6.
      - id: region_type
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the region type. This field MUST be set to
          CBT_REGION (0xCAC1).
      - id: num_tilesets
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the number of TS_RFX_TILESET (section 2.2.2.3.4) messages
          following this TS_RFX_REGION message. This field MUST be set to 0x0001.

  ts_rfx_codec_quant:
    doc: |
      MS-RDPRFX 2.2.2.1.5 TS_RFX_CODEC_QUANT
      The TS_RFX_CODEC_QUANT structure holds the scalar quantization values for the ten sub-bands in the
      3-level DWT decomposition. Each field in this structure MUST have a value in the range of 6 to 15.
    seq:
      - id: lh3
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-3 DWT sub-band.
      - id: ll3
        type: b4
        doc: A 4-bit, unsigned integer. The LL quantization factor for the level-3 DWT sub-band.
      - id: hh3
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-3 DWT sub-band.
      - id: hl3
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factors for the level-3 DWT sub-band.
      - id: hl2
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factor for the level-2 DWT sub-band.
      - id: lh2
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-2 DWT sub-band.
      - id: lh1
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-1 DWT sub-band.
      - id: hh2
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-2 DWT sub-band.
      - id: hh1
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-1 DWT sub-band.
      - id: hl1
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factor for the level-1 DWT sub-band.

  ts_rfx_tileset:
    doc: |
      MS-RDPRFX 2.2.2.3.4 TS_RFX_TILESET
      The TS_RFX_TILESET message contains encoding parameters and data for an arbitrary number of encoded
      tiles.
      mstscax!CacCommon::DecStreamProcessor::dispatchExtension
      mstscax!CacCommon::CalDecStreamProcessor::ProcessExtension
      mstscax!CacCommon::CalDecStreamProcessor::processTileSet
      mstscax!Cac::DecoderImpl::HandleTileSet
      mstscax!CacDecoding::DecodingThreadManager::ProcessFrame
    seq:
      - id: subtype
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the message type.
          This field MUST be set to CBT_TILESET (0xCAC2).
      - id: idx
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the identifier of the TS_RFX_CONTEXT (section 2.2.2.2.4)
          message referenced by this TileSet message. This field MUST be set to 0x0000. The decoder
          SHOULD ignore this field.
      - id: qt
        type: b2
        doc: |
          A 2-bit unsigned integer. Specifies the quantization type. This field MUST be set to
          SCALAR_QUANTIZATION (0x1). The decoder SHOULD ignore this field.
      - id: et
        type: b4
        doc: |
          A 4-bit unsigned integer. Specifies the entropy algorithm. This field MUST be set to one of the
          following values.
            - CLW_ENTROPY_RLGR1 (0x01): RLGR algorithm as detailed in 3.1.8.1.7.1.
                                        mstscax!CacDecoding::decrlgr2
            - CLW_ENTROPY_UNKNOWN (0x02): Unknown type, see mstscax!Cac::DecoderImpl::HandleTileSet.
                                          mstscax!CacDecoding::decrlgr2
            - CLW_ENTROPY_RLGR3 (0x04): RLGR algorithm as detailed in 3.1.8.1.7.2.
                                        mstscax!CacDecoding::decrlgr3
      - id: xft
        type: b4
        doc: |
          A 4-bit unsigned integer. Specifies the DWT. This field MUST be set to CLW_XFORM_DWT_53_A (0x1),
          which indicates the DWT given by the equations in sections 3.1.8.1.4 and 3.1.8.2.4. The decoder
          SHOULD ignore this field.
      - id: cct
        type: b2
        doc: |
          A 2-bit unsigned integer. Specifies the color conversion transform. This field MUST be set to
          COL_CONV_ICT (0x1) to specify the transform defined by the equations in sections 3.1.8.1.3 and
          3.1.8.2.5. The decoder SHOULD ignore this field.
      - id: flags
        type: b3
        doc: |
          A 3-bit unsigned integer. Specifies operational flags:
            - CODEC_MODE (0x02): The codec is operating in image mode.
                                 If this flag is not set, the codec is operating in video mode.
          The encoder MUST set this value to the value of flags that is set in the properties field of
          TS_RFX_CONTEXT. The decoder MUST ignore this flag and MUST use the flags specified in the flags
          field of the TS_RFX_CONTEXT.
      - id: a
        type: b1
        doc: |
          A 1-bit field that specifies whether this is the last TS_RFX_TILESET in the region. This field
          MUST be set to TRUE (0x1). The decoder SHOULD ignore this field.
      - id: num_quant
        type: u2
        doc: |
          An 8-bit, unsigned integer. Specifies the number of TS_RFX_CODEC_QUANT (section 2.2.2.1.5)
          structures present in the quantVals field.
      - id: tile_size
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies the width and height of a tile. This field MUST be set to
          0x40. The decoder SHOULD ignore this field.
      - id: num_tiles
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the number of TS_RFX_TILE (section 2.2.2.3.4.1) structures
          present in the tiles field.
      - id: tile_data_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. Specifies the size, in bytes, of the tiles field. The tiles field
          contains encoded data for all of the tiles that have changed.
          Not trusted by the client, uses the packet size to determine the data for tiles.
      - id: quant_vals
        type: ts_rfx_codec_quant
        repeat: expr
        repeat-expr: num_quant
        doc: |
          A variable-length array of TS_RFX_CODEC_QUANT (section 2.2.2.1.5) structures. The number of
          structures present in the array is indicated in the numQuant field.
      - id: tiles
        type: ts_rfx_tile
        repeat: expr
        repeat-expr: num_tiles
        doc: |
          A variable-length array of TS_RFX_TILE (section 2.2.2.3.4.1) structures. The number of structures
          present in the array is indicated in the numTiles field, while the total size, in bytes, of this
          field is specified by the tilesDataSize field.

  ts_rfx_tile:
    doc: |
      MS-RDPRFX 2.2.2.3.4.1 TS_RFX_TILE
      The TS_RFX_TILE structure specifies the position of the tile on the frame and contains the encoded data
      for the three tile components of Y, Cb, and Cr.
      mstscax!CacCommon::CalDecStreamProcessor::processTileSet
      mstscax!Cac::DecoderImpl::HandleTileSet
      mstscax!CacInvXform::IDwtCpu::SetTileCount
      mstscax!CacInvXform::IDwtCpu::SetImageSize
      mstscax!CacDecoding::DecodingThreadManager::ProcessFrame
      mstscax!CacDecoding::DecodingThreadContext::ProcessNextTile
      mstscax!CacDecoding::Decoding::UnRlgr2LnTiles3V10_threadsafe
      mstscax!CacDecoding::TileUnRlgr2V10Ln::UnRlgr2Bands
      mstscax!CacDecoding::TileUnRlgr2V10Ln::unRlgr2CompBands
      mstscax!CacInvXform::IDwtCpu::CopyTile
      mstscax!CacInvXform::IDwtCpu::copyTile
    seq:
      - id: quant_idx_y
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies an index into the TS_RFX_CODEC_QUANT array provided in the
          TS_RFX_TILESET message. The specified TS_RFX_CODEC_QUANT element MUST be used for de-quantization
          of the sub-bands for the Y-component.
      - id: quant_idx_cb
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies an index into the TS_RFX_CODEC_QUANT array provided in the
          TS_RFX_TILESET message. The specified TS_RFX_CODEC_QUANT element MUST be used for de-quantization
          of the sub-bands for the Cb-component.
      - id: quant_idx_cr
        type: u1
        doc: |
          An 8-bit, unsigned integer. Specifies an index into the TS_RFX_CODEC_QUANT array provided in the
          TS_RFX_TILESET message. The specified TS_RFX_CODEC_QUANT element MUST be used for de-quantization
          of the sub-bands for the Cr-component.
      - id: x_idx
        type: u2
        doc: |
          A 16-bit, unsigned integer. The X-index of the encoded tile in the screen tile grid.
      - id: y_idx
        type: u2
        doc: |
          A 16-bit, unsigned integer. The Y-index of the encoded tile in the screen tile grid.
      - id: y_len
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the size, in bytes, of the YData field.
      - id: cb_len
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the size, in bytes, of the CbData field.
      - id: cr_len
        type: u2
        doc: |
          A 16-bit, unsigned integer. Specifies the size, in bytes, of the CrData field.
      - id: y_data
        size: y_len
        doc: |
          A variable-length array. Contains the encoded data for the Y-component of the tile. The size, in
          bytes, of this field is specified by the YLen field.
      - id: cb_data
        size: cb_len
        doc: |
          A variable-length array. Contains the encoded data for the Cb-component of the tile. The size, in
          bytes, of this field is specified by the CbLen field.
      - id: cr_data
        size: cr_len
        doc: |
          A variable-length array. Contains the encoded data for the Cr-component of the tile. The size, in
          bytes, of this field is specified by the CrLen field.
