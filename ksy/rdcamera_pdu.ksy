meta:
  id: rdcamera_pdu
  endian: le

doc: |
  MS-RDPECAM 2.1 Transport
  Dynamic channel "RDCamera_Device_Enumerator"
  Each camera device gets its own channel with "RDCamera_Device_%d"
  mstscax!DeviceEnumeratorListenerCallback::OnNewChannelConnection
  mstscax!DeviceEnumeratorVCCallback::OnDataReceived
  mstscax!MediaSourceListenerCallback::OnNewChannelConnection
  mstscax!MediaSourceVCCallback::OnDataReceived
  mstscax!RDMediaProtocolHelper::ValidateMessage

seq:
  - id: version
    -orig-id: Version
    type: u1
    doc: |
      An 8-bit unsigned integer that specifies the protocol version of the message.
      This field MUST be set to either 1 or 2.
  - id: message_id
    -orig-id: MessageId
    type: u1
    enum: message_id
    doc: |
      An 8-bit unsigned integer that specifies the type of the message.
  - id: message
    type:
      switch-on: message_id
      -name: type
      cases:
        'message_id::success_response': rdcamera_success_response
        'message_id::error_response': rdcamera_error_response
        'message_id::select_version_request': rdcamera_select_version_request
        'message_id::select_version_response': rdcamera_select_version_response
        'message_id::device_added_notification': rdcamera_device_added_notification
        'message_id::device_removed_notification': rdcamera_device_removed_notification
        'message_id::activate_device_request': rdcamera_activate_device_request
        'message_id::deactivate_device_request': rdcamera_deactivate_device_request
        'message_id::stream_list_request': rdcamera_stream_list_request
        'message_id::stream_list_response': rdcamera_stream_list_response
        'message_id::media_type_list_request': rdcamera_media_type_list_request
        'message_id::media_type_list_response': rdcamera_media_type_list_response
        'message_id::current_media_type_request': rdcamera_current_media_type_request
        'message_id::current_media_type_response': rdcamera_current_media_type_response
        'message_id::start_streams_request': rdcamera_start_streams_request
        'message_id::stop_streams_request': rdcamera_stop_streams_request
        'message_id::sample_request': rdcamera_sample_request
        'message_id::sample_response': rdcamera_sample_response
        'message_id::sample_error_response': rdcamera_sample_error_response
        'message_id::property_list_request': rdcamera_property_list_request
        'message_id::property_list_response': rdcamera_property_list_response
        'message_id::property_value_request': rdcamera_property_value_request
        'message_id::property_value_response': rdcamera_property_value_response
        'message_id::set_property_value_request': rdcamera_set_property_value_request

enums:
  message_id:
    1:
      id: success_response
      -orig-id: SuccessResponse
      doc: |
        A Success Response (section 2.2.3.1) message.
    2:
      id: error_response
      -orig-id: ErrorResponse
      doc: |
        An Error Response (section 2.2.3.2) message.
    3:
      id: select_version_request
      -orig-id: SelectVersionRequest
      doc: |
        A Select Version Request (section 2.2.2.1) message.
    4:
      id: select_version_response
      -orig-id: SelectVersionResponse
      doc: |
        A Select Version Response (section 2.2.2.2) message.
    5:
      id: device_added_notification
      -orig-id: DeviceAddedNotification
      doc: |
        A Device Added Notification (section 2.2.2.3) message.
    6:
      id: device_removed_notification
      -orig-id: DeviceRemovedNotification
      doc: |
        A Device Removed Notification (section 2.2.2.4) message.
    7:
      id: activate_device_request
      -orig-id: ActivateDeviceRequest
      doc: |
        An Activate Device Request (section 2.2.3.3) message.
    8:
      id: deactivate_device_request
      -orig-id: DeactivateDeviceRequest
      doc: |
        A Deactivate Device Request (section 2.2.3.4) message.
    9:
      id: stream_list_request
      -orig-id: StreamListRequest
      doc: |
        A Stream List Request (section 2.2.3.5) message.
    10:
      id: stream_list_response
      -orig-id: StreamListResponse
      doc: |
        A Stream List Response (section 2.2.3.6) message.
    11:
      id: media_type_list_request
      -orig-id: MediaTypeListRequest
      doc: |
        A Media Type List Request (section 2.2.3.7) message.
    12:
      id: media_type_list_response
      -orig-id: MediaTypeListResponse
      doc: |
        A Media Type List Response (section 2.2.3.8) message.
    13:
      id: current_media_type_request
      -orig-id: CurrentMediaTypeRequest
      doc: |
        A Current Media Type Request (section 2.2.3.9) message.
    14:
      id: current_media_type_response
      -orig-id: CurrentMediaTypeResponse
      doc: |
        A Current Media Type Response (section 2.2.3.10) message.
    15:
      id: start_streams_request
      -orig-id: StartStreamsRequest
      doc: |
        A Start Streams Request (section 2.2.3.11) message.
    16:
      id: stop_streams_request
      -orig-id: StopStreamsRequest
      doc: |
        A Stop Streams Request (section 2.2.3.12) message.
    17:
      id: sample_request
      -orig-id: SampleRequest
      doc: |
        A Sample Request (section 2.2.3.13) message.
    18:
      id: sample_response
      -orig-id: SampleResponse
      doc: |
        A Sample Response (section 2.2.3.14) message.
    19:
      id: sample_error_response
      -orig-id: SampleErrorResponse
      doc: |
        A Sample Error Response (section 2.2.3.15) message.
    20:
      id: property_list_request
      -orig-id: PropertyListRequest
      doc: |
        A Property List Request (section 2.2.3.16) message.
        This message is supported only by version 2 of the protocol.
    21:
      id: property_list_response
      -orig-id: PropertyListResponse
      doc: |
        A Property List Response (section 2.2.3.17) message.
        This message is supported only by version 2 of the protocol.
    22:
      id: property_value_request
      -orig-id: PropertyValueRequest
      doc: |
        A Property Value Request (section 2.2.3.18) message.
        This message is supported only by version 2 of the protocol.
    23:
      id: property_value_response
      -orig-id: PropertyValueResponse
      doc: |
        A Property Value Response (section 2.2.3.19) message.
        This message is supported only by version 2 of the protocol.
    24:
      id: set_property_value_request
      -orig-id: SetPropertyValueRequest
      doc: |
        A Set Property Value Request (section 2.2.3.20) message.
        This message is supported only by version 2 of the protocol.
  error_code:
    1:
      id: unexpected_error
      -orig-id: UnexpectedError
      doc: |
        An unexpected error occurred.
    2:
      id: invalid_message
      -orig-id: InvalidMessage
      doc: |
        An invalid message was received. Either the message is malformed,
        or the protocol version or message type is unexpected.
    3:
      id: not_initialized
      -orig-id: NotInitialized
      doc: |
        The object MUST be initialized before the requested operation can be carried out.
        This error could be returned, for example, when attempting to communicate with a
        deactivated camera device.
    4:
      id: invalid_request
      -orig-id: InvalidRequest
      doc: |
        The request is invalid in the current state.
    5:
      id: invalid_stream_number
      -orig-id: InvalidStreamNumber
      doc: |
        The provided stream number was invalid.
    6:
      id: invalid_media_type
      -orig-id: InvalidMediaType
      doc: |
        The data specified for the stream format is invalid, inconsistent, or not supported.
    7:
      id: out_of_memory
      -orig-id: OutOfMemory
      doc: |
        The client ran out of memory.
    8:
      id: item_not_found
      -orig-id: ItemNotFound
      doc: |
        The device does not support the requested property.
        This error code is generated only by version 2 of the protocol.
    9:
      id: set_not_found
      -orig-id: SetNotFound
      doc: |
        The device does not support the requested property set.
        This error code is generated only by version 2 of the protocol.
    10:
      id: operation_not_supported
      -orig-id: OperationNotSupported
      doc: |
        The requested operation is not supported.
        This error code is generated only by version 2 of the protocol.
  property_set:
    1:
      id: camera_control
      -orig-id: CameraControl
      doc: |
        This property set category controls camera device settings.
    2:
      id: video_proc_amp
      -orig-id: VideoProcAmp
      doc: |
        This property set controls devices that can adjust the image color attributes of
        analog or digital signals.
  property_id_camera_control:
    1:
      id: exposure
      -orig-id: Exposure 1
      doc: |
        This property controls the exposure time of the device.
    2:
      id: focus
      -orig-id: Focus
      doc: |
        This property controls the focus setting of the device.
    3:
      id: pan
      -orig-id: Pan
      doc: |
        This property controls the pan setting of the device.
    4:
      id: roll
      -orig-id: Roll
      doc: |
        This property controls the roll setting of the device.
    5:
      id: tilt
      -orig-id: Tilt
      doc: |
        This property controls the tilt setting of the device.
    6:
      id: zoom
      -orig-id: Zoom
      doc: |
        This property controls the zoom setting of the device.
  property_id_video_proc_amp:
    1:
      id: backlight_compensation
      -orig-id: BacklightCompensation
      doc: |
        This property controls the backlight compensation setting of the device. This value MUST be either 0 or 1. The value 0 indicates that backlight compensation is disabled. The value 1 indicates that backlight compensation is enabled.
    2:
      id: brightness
      -orig-id: Brightness
      doc: |
        This property controls the brightness setting of the device.
    3:
      id: contrast
      -orig-id: Contrast
      doc: |
        This property controls the contrast setting of the device.
    4:
      id: hue
      -orig-id: Hue
      doc: |
        This property controls the hue setting of the device.
    5:
      id: white_balance
      -orig-id: WhiteBalance
      doc: |
        This property controls the white balance setting of the device.
  mode:
    0x01:
      id: manual
      -orig-id: Manual
      doc: Indicates that the property can be controlled manually.
    0x02:
      id: auto
      -orig-id: Auto
      doc: Indicates that the property can be controlled automatically.

types:
  rdcamera_success_response:
    doc: |
      MS-RDPECAM 2.2.3.1 Success Response
      The Success Response message is sent by the client over a device channel
      to indicate that a request from the server succeeded.
      (no data)

  rdcamera_error_response:
    doc: |
      MS-RDPECAM 2.2.3.2 Error Response
      The Error Response message is sent by the client over a device channel if
      an error occurs while processing a request from the server.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::SendErrorResponse
      mstscax!RDMediaProtocolHelper::CreateErrorResponse
      mstscax!RDMediaProtocolHelper::HRESULTToRDMErrorCode
    seq:
      - id: error_code
        -orig-id: ErrorCode
        type: u4
        enum: error_code
        doc: |
          A 32-bit unsigned integer containing an error code.

  rdcamera_select_version_request:
    doc: |
      MS-RDPECAM 2.2.2.1 Select Version Request
      The Select Version Request message is sent by the client over the device enumeration channel
      to allow the server to select the protocol version. The client MUST set the Version field to
      the maximum version number supported.
      In response the server MUST send the Select Version Response (section 2.2.2.2) message.
      mstscax!DeviceEnumeratorListenerCallback::OnNewChannelConnection
      mstscax!DeviceEnumeratorListenerCallback::InitDeviceChannels
      mstscax!GetCameraRedirConfigCollection
      mstscax!MFApiWrapper::MFCreateAttributes
      mstscax!MFApiWrapper::MFEnumDeviceSources
      mstscax!DeviceEnumeratorListenerCallback::CreateDeviceChannel
      mstscax!DeviceEnumeratorListenerCallback::CreateDVC
      mstscax!DeviceEnumeratorListenerCallback::RegisterForDeviceNotifications
      mstscax!DeviceEnumeratorListenerCallback::staticMsgWndProc
      mstscax!DeviceEnumeratorVCCallback::OnChannelOpened
      mstscax!DeviceEnumeratorVCCallback::SendSelectVersionRequest
      (no data)

  rdcamera_select_version_response:
    doc: |
      MS-RDPECAM 2.2.2.2 Select Version Response
      The Select Version Response message is sent by the server over the device enumeration channel
      in response to the Select Version Request (section 2.2.2.1) message.
      mstscax!DeviceEnumeratorVCCallback::OnDataReceived
      mstscax!DeviceEnumeratorVCCallback::OnSelectVersionResponse
      mstscax!DeviceEnumeratorVCCallback::SendDeviceAddedNotification
      mstscax!RDMediaProtocolHelper::CreateDeviceAddedNotification
      (no data)

  rdcamera_device_added_notification:
    doc: |
      MS-RDPECAM 2.2.2.3 Device Added Notification
      The Device Added Notification message is sent by the client over the device enumeration channel
      for each redirected video capture device.
      mstscax!DeviceEnumeratorListenerCallback::staticMsgWndProc
      mstscax!DeviceEnumeratorListenerCallback::MsgWndProc
      mstscax!DeviceEnumeratorListenerCallback::OnAddDevice
      mstscax!DeviceEnumeratorListenerCallback::GetDeviceActivationObject
      mstscax!MFApiWrapper::MFCreateAttributes
      mstscax!MFApiWrapper::MFEnumDeviceSources
      mstscax!DeviceEnumeratorListenerCallback::AssignDeviceChannelAndSendNotification
      mstscax!DeviceEnumeratorVCCallback::SendDeviceAddedNotification
      mstscax!RDMediaProtocolHelper::CreateDeviceAddedNotification
    seq:
      - id: device_name
        -orig-id: DeviceName
        type: strz
        encoding: utf-16le
        doc: |
          A null-terminated, variable-length array of Unicode characters containing the display name of
          the redirected device.
      - id: virtual_channel_name
        -orig-id: VirtualChannelName
        type: strz
        encoding: ascii
        doc: |
          A null-terminated, variable-length array of ANSI characters containing the name of the dynamic
          virtual channel associated with the redirected device.

  rdcamera_device_removed_notification:
    doc: |
      MS-RDPECAM 2.2.2.4 Device Removed Notification
      The Device Removed Notification message is sent by the client over the device enumeration channel
      when redirection of a specific video capture device needs to be stopped (for example, when a device
      is disconnected from the client).
      mstscax!DeviceEnumeratorListenerCallback::staticMsgWndProc
      mstscax!DeviceEnumeratorListenerCallback::MsgWndProc
      mstscax!DeviceEnumeratorListenerCallback::OnRemoveDevice
      mstscax!DeviceEnumeratorVCCallback::SendDeviceRemovedNotification
      mstscax!RDMediaProtocolHelper::CreateDeviceRemovedNotification
    seq:
      - id: virtual_channel_name
        -orig-id: VirtualChannelName
        type: strz
        encoding: ascii
        doc: |
          A null-terminated, variable-length array of ANSI characters containing the name of the dynamic
          virtual channel associated with the removed device.

  rdcamera_activate_device_request:
    doc: |
      MS-RDPECAM 2.2.3.3 Activate Device Request
      The Activate Device Request message is sent by the server over a device channel to activate the video
      capture device associated with the channel. In response the client MUST send either the
      Success Response (section 2.2.3.1) or Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::OnActivateDevice
      mstscax!CameraAdapter::ActivateDevice
      (no data)

  rdcamera_deactivate_device_request:
    doc: |
      MS-RDPECAM 2.2.3.4 Deactivate Device Request
      The Deactivate Device Request message is sent by the server over a device channel to deactivate the
      video capture device associated with the channel. In response the client MUST send either the
      Success Response (section 2.2.3.1) or Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::OnDeactivateDevice
      mstscax!CameraAdapter::DeactivateDevice
      (no data)

  rdcamera_stream_list_request:
    doc: |
      MS-RDPECAM 2.2.3.5 Stream List Request
      The Stream List Request message is sent by the server over a device channel to retrieve the list of
      video streams supported by the video capture device associated with the channel.
      In response the client MUST send either the Stream List Response (section 2.2.3.6) or
      Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::OnEnumStreams
      mstscax!CameraAdapter::EnumStreams
      mstscax!RDMediaProtocolHelper::CreateStreamListResponse
      (no data)

  rdcamera_stream_list_response:
    doc: |
      MS-RDPECAM 2.2.3.6 Stream List Response
      The Stream List Response message is sent by the client over a device channel in response to the
      Stream List Request (section 2.2.3.5) message. It contains the list of video streams supported
      by the video capture device associated with the channel.
      mstscax!CameraAdapter::EnumStreams
      mstscax!RDMediaProtocolHelper::CreateStreamListResponse
    seq:
      - id: stream_descriptions
        -orig-id: StreamDescriptions
        type: rdcamera_stream_description
        repeat: eos
        doc: |
          An array of 1 to 255 STREAM_DESCRIPTION (section 2.2.3.6.1) structures.

  rdcamera_stream_description:
    doc: |
      MS-RDPECAM 2.2.3.6.1 STREAM_DESCRIPTION
      The STREAM_DESCRIPTION structure contains properties of a video stream.
      mstscax!CameraAdapter::EnumStreams
    seq:
      - id: frame_source_types
        -orig-id: FrameSourceTypes
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the type of data the stream provides.
      - id: stream_category
        -orig-id: StreamCategory
        type: u1
        enum: stream_category
        doc: |
          An 8-bit unsigned integer that specifies the category of the stream.
      - id: selected
        -orig-id: Selected
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST be set to 1 if the stream is currently selected
          to produce video data, otherwise it MUST be set to 0.
      - id: can_be_shared
        -orig-id: CanBeShared
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST be set to 1 if the stream can be shared between
          applications, otherwise it MUST be set to 0.
    enums:
      frame_source_type:
        0x0001:
          id: color
          -orig-id: Color
          doc: This stream provides color data.
        0x0002:
          id: infrared
          -orig-id: Infrared
          doc: This stream provides infrared data.
        0x0008:
          id: custom
          -orig-id: Custom
          doc: This stream provides custom data.
      stream_category:
        1:
          id: capture
          -orig-id: Capture
          doc: Capture category streams provide a stream of compressed or uncompressed digital video.

  rdcamera_media_type_list_request:
    doc: |
      MS-RDPECAM 2.2.3.7 Media Type List Request
      The Media Type List Request message is sent by the server over a device channel to retrieve the
      list of stream formats supported on the specified stream by the video capture device associated
      with the channel. In response the client MUST send either the
      Media Type List Response (section 2.2.3.8) or Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToUINT8
      mstscax!MediaSourceVCCallback::OnEnumMediaTypes
      mstscax!CameraAdapter::EnumMediaTypes
      mstscax!RDMediaProtocolHelper::CreateMediaTypeListResponse
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST be set to the index of the stream being queried.
          This index MUST match the index in the STREAM_DESCRIPTION array returned in the
          Stream List Response (section 2.2.3.6) message.

  rdcamera_media_type_list_response:
    doc: |
      MS-RDPECAM 2.2.3.8 Media Type List Response
      The Media Type List Response is sent by the client over a device channel in response to a
      Media Type List Request (section 2.2.3.7) message. It contains the list of stream formats
      supported by the specified stream of the video capture device associated with the channel.
      mstscax!CameraAdapter::EnumMediaTypes
      mstscax!RDMediaProtocolHelper::CreateMediaTypeListResponse
    seq:
      - id: media_type_description
        -orig-id: MediaTypeDescriptions
        type: rdcamera_media_type_description
        repeat: eos
        doc: |
          An array of 1 or more MEDIA_TYPE_DESCRIPTION (section 2.2.3.8.1) structures.

  rdcamera_media_type_description:
    doc: |
      MS-RDPECAM 2.2.3.8.1 MEDIA_TYPE_DESCRIPTION
      The MEDIA_TYPE_DESCRIPTION structure specifies the properties of a stream format.
      mstscax!CameraAdapter::EnumMediaTypes
      mstscax!CameraAdapter::GetCurrentMediaType
    seq:
      - id: format
        -orig-id: Format
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the stream codec.
      - id: width
        -orig-id: Width
        type: u4
        doc: |
          A 32-bit unsigned integer that MUST be set to the width of the image in pixels.
      - id: height
        -orig-id: Height
        type: u4
        doc: |
          A 32-bit unsigned integer that MUST be set to the height of the image in pixels.
      - id: frame_rate_numerator
        -orig-id: FrameRateNumerator
        type: u4
        doc: |
          A 32-bit unsigned integer containing the numerator of the frame rate.
          The frame rate is expressed as a ratio.
          For example, if the frame rate is 30 frames per second (fps), the ratio is 30/1.
          If the frame rate is 29.97 fps, the ratio is 30,000/1001.
      - id: frame_rate_denominator
        -orig-id: FrameRateDenominator
        type: u4
        doc: |
          A 32-bit unsigned integer containing the denominator of the frame rate.
      - id: pixel_aspect_ratio_numerator
        -orig-id: PixelAspectRatioNumerator
        type: u4
        doc: |
          A 32-bit unsigned integer that MUST be set to the horizontal component of the pixel aspect ratio.
      - id: pixel_aspect_ratio_denominator
        -orig-id: PixelAspectRatioDenominator
        type: u4
        doc: |
          A 32-bit unsigned integer that MUST be set to the vertical component of the pixel aspect ratio.
      - id: flags
        -orig-id: Flags
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST contain a combination of zero or more flags.
    enums:
      format:
        1:
          id: h265
          -orig-id: H264
          doc: |
            H.264 video as described in [ITU-H.264-201704].
            Media samples contain H.264 bitstream data with start codes and interleaved sequence
            parameter set/picture parameter set (SPS/PPS) packets. Each sample contains one complete
            picture, either one field or one frame.
        2:
          id: mjpg
          -orig-id: MJPG
          doc: |
            Motion JPEG.
            Motion JPEG is a video compression format in which each video frame of a digital video
            sequence is independently compressed as a JPEG image.
        3:
          id: yuy2
          -orig-id: YUY2
          doc: |
            YUY2 video as specified in [MSDN-YUVFormats].
        4:
          id: nv12
          -orig-id: NV12
          doc: |
            NV12 video as described in [MSDN-YUVFormats].
        5:
          id: i420
          -orig-id: I420
          doc: |
            I420 video.
            Identical to YV12 as described in [MSDN-YUVFormats] except that the order of the
            U and V planes is reversed.
        6:
          id: rgb24
          -orig-id: RGB24
          doc: |
            RGB, 24 bits per pixel.
        7:
          id: rgb32
          -orig-id: RGB32
          doc: |
            RGB, 32 bits per pixel.
      flag:
        0x01:
          id: decoding_required
          -orig-id: DecodingRequired
          doc: |
            The video samples from the stream format SHOULD be decoded on the server.
        0x02:
          id: bottom_up_image
          -orig-id: BottomUpImage
          doc: |
            The images from the stream format are bottom-up.
            In a bottom-up image, the last row of pixels appears first in memory.

  rdcamera_current_media_type_request:
    doc: |
      MS-RDPECAM 2.2.3.9 Current Media Type Request
      The Current Media Type Request message is sent by the server over a device channel to
      retrieve the stream format currently selected on the specified stream of the video
      capture device associated with the channel. In response the client MUST send either
      the Current Media Type Response (section 2.2.3.10) or Error Response (section 2.2.3.2)
      message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToUINT8
      mstscax!MediaSourceVCCallback::OnGetCurrentMediaType
      mstscax!CameraAdapter::GetCurrentMediaType
      mstscax!RDMediaProtocolHelper::CreateCurrentMediaTypeResponse
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST be set to the index of the stream being queried.
          This index MUST match the index in the STREAM_DESCRIPTION array returned in the Stream
          List Response (section 2.2.3.6) message.

  rdcamera_current_media_type_response:
    doc: |
      MS-RDPECAM 2.2.3.10 Current Media Type Response
      The Current Media Type Response message is sent by the client over a device channel in
      response to the Current Media Type Request (section 2.2.3.9) message. It contains the
      stream format currently selected on the specified stream of the video capture device
      associated with the channel.
      mstscax!CameraAdapter::GetCurrentMediaType
      mstscax!RDMediaProtocolHelper::CreateCurrentMediaTypeResponse
    seq:
      - id: media_type_description
        -orig-id: MediaTypeDescription
        type: rdcamera_media_type_description
        doc: |
          MEDIA_TYPE_DESCRIPTION (section 2.2.3.8.1) structure containing properties of the stream
          format currently selected on the specified stream of the video capture device associated
          with the channel.

  rdcamera_start_streams_request:
    doc: |
      MS-RDPECAM 2.2.3.11 Start Streams Request
      The Start Streams Request message is sent by the server over a device channel to start the
      specified streams on the video capture device associated with the channel. In response the
      client MUST send either the Success Response (section 2.2.3.1) or Error Response
      (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToRDM_START_STREAM_INFO
      mstscax!MediaSourceVCCallback::OnStartStreams
      mstscax!CameraAdapter::StartStreams
    seq:
      - id: start_streams_info
        -orig-id: StartStreamsInfo
        type: rdcamera_start_stream_info
        doc: |
          An array of 1 to 255 START_STREAM_INFO (section 2.2.3.11.1) structures.

  rdcamera_start_stream_info:
    doc: |
      MS-RDPECAM 2.2.3.11.1 START_STREAM_INFO
      The START_STREAM_INFO structure contains information required to start a stream.
      mstscax!RDMediaProtocolHelper::PayloadToRDM_START_STREAM_INFO
      mstscax!CameraAdapter::StartStreams
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer containing the index of the stream being started.
          This index MUST match the index in the STREAM_DESCRIPTION array returned in the
          Stream List Response (section 2.2.3.6) message.
      - id: media_type_description
        -orig-id: MediaTypeDescription
        type: rdcamera_media_type_description
        doc: |
          A MEDIA_TYPE_DESCRIPTION (section 2.2.3.8.1) structure containing properties of the
          stream format that the stream MUST produce.

  rdcamera_stop_streams_request:
    doc: |
      MS-RDPECAM 2.2.3.12 Stop Streams Request
      The Stop Streams Request message is sent by the server over a device channel to stop all
      streams of the video capture device associated with the channel. In response the client
      MUST send either the Success Response (section 2.2.3.1) or Error Response (section 2.2.3.2)
      message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::OnStopStreams
      mstscax!CameraAdapter::StopStreams
      (no data)

  rdcamera_sample_request:
    doc: |
      MS-RDPECAM 2.2.3.13 Sample Request
      The Sample Request message is sent by the server over a device channel to request a video
      sample from the specified stream on the video capture device associated with the channel.
      For each Sample Request message received the client MUST send back either a
      Sample Response (section 2.2.3.14) message when a new sample is ready or an
      Error Response (section 2.2.3.2) message if it failed to produce a sample.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToUINT8
      mstscax!MediaSourceVCCallback::OnRequestSample
      mstscax!CameraAdapter::RequestSample
      mstscax!MediaSourceVCCallback::OnNewSample
      Mfplat!CMFSample::ConvertToContiguousBuffer
      mstscax!RDMediaProtocolHelper::CreateSampleResponse
      mstscax!MediaSourceVCCallback::OnSampleError
      mstscax!RDMediaProtocolHelper::CreateSampleErrorResponse
      mstscax!RDMediaProtocolHelper::HRESULTToRDMErrorCode
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer containing the index of the stream being queried.
          This index MUST match the index in the STREAM_DESCRIPTION array returned in the
          Stream List Response (section 2.2.3.6) message.

  rdcamera_sample_response:
    doc: |
      MS-RDPECAM 2.2.3.14 Sample Response
      The Sample Response message is sent by the client over a device channel when a new video
      sample is available and there are outstanding Sample Requests (section 2.2.3.13).
      It contains the video sample bits.
      mstscax!MediaSourceVCCallback::OnNewSample
      Mfplat!CMFSample::ConvertToContiguousBuffer
      mstscax!RDMediaProtocolHelper::CreateSampleResponse
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer containing the index of the stream which produced the sample.
          This index MUST match the index in the STREAM_DESCRIPTION array returned in the
          Stream List Response (section 2.2.3.6) message.
      - id: sample
        -orig-id: Sample
        size-eos: true
        doc: |
          A variable-length array of bytes containing the sample data. The data format depends on
          the stream format selected in the Start Streams Request (section 2.2.3.11) message.

  rdcamera_sample_error_response:
    doc: |
      MS-RDPECAM 2.2.3.15 Sample Error Response
      The Sample Error Response is sent by the client over a device channel if an error occurred
      while producing a video sample and there is at least one outstanding Sample Request
      (section 2.2.3.13) message.
      mstscax!MediaSourceVCCallback::OnSampleError
      mstscax!RDMediaProtocolHelper::CreateSampleErrorResponse
      mstscax!RDMediaProtocolHelper::HRESULTToRDMErrorCode
    seq:
      - id: stream_index
        -orig-id: StreamIndex
        type: u1
        doc: |
          An 8-bit unsigned integer containing the index of the stream from which a sample was
          requested. This index MUST match the index in the STREAM_DESCRIPTION array returned in
          the Stream List Response (section 2.2.3.6) message.
      - id: error_code
        -orig-id: ErrorCode
        type: u4
        enum: error_code
        doc: |
          A 32-bit unsigned integer containing one of the error codes listed in section 2.2.3.2.

  rdcamera_property_list_request:
    doc: |
      MS-RDPECAM 2.2.3.16 Property List Request
      The Stream List Request message is sent by the server over a device channel to retrieve the
      list of device properties supported by the video capture device. In response the client MUST
      send either the Property List Response (section 2.2.3.17) or Error Response (section 2.2.3.2)
      message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!MediaSourceVCCallback::OnPropertyListRequest
      mstscax!CameraAdapter::EnumProperties
      mstscax!RDMediaProtocolHelper::CreatePropertyListResponse
      (no data)

  rdcamera_property_list_response:
    doc: |
      MS-RDPECAM 2.2.3.17 Property List Response
      The Property List Response message is sent by the client over a device channel in response to
      the Property List Request (section 2.2.3.16) message. It contains the list of device properties
      supported by the video capture device associated with the channel.
      mstscax!CameraAdapter::EnumProperties
      mstscax!RDMediaProtocolHelper::CreatePropertyListResponse
    seq:
      - id: properties
        -orig-id: Properties
        type: rdcamera_property_description
        repeat: eos
        doc: |
          A variable-length array of zero or more PROPERTY_DESCRIPTION (section 2.2.3.17.1) structures.

  rdcamera_property_description:
    doc: |
      MS-RDPECAM 2.2.3.17.1 PROPERTY_DESCRIPTION
      The PROPERTY_DESCRIPTION structure specifies a device property.
      mstscax!CameraAdapter::EnumProperties
      mstscax!RDMediaProtocolHelper::CreatePropertyListResponse
    seq:
      - id: property_set
        -orig-id: PropertySet
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the property set.
      - id: property_id
        -orig-id: PropertyId
        type: u1
        doc: |
          An 8-bit unsigned integer that contains the identifier of the property within the
          property set
          specified by the PropertySet field.
      - id: capabilities
        -orig-id: Capabilities
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies how a property is controlled.
      - id: min_value
        -orig-id: MinValue
        type: u4
        doc: |
          A 32-bit signed integer containing the minimum allowed value of this property.
      - id: max_value
        -orig-id: MaxValue
        type: u4
        doc: |
          A 32-bit signed integer containing the maximum allowed value of this property.
      - id: step
        -orig-id: Step
        type: u4
        doc: |
          A 32-bit signed integer containing the minimum allowed value by which this property
          can be incremented or decremented.
      - id: default_value
        -orig-id: DefaultValue
        type: u4
        doc: |
          A 32-bit signed integer containing the step value that SHOULD be used to create values
          within the range defined by the MinValue and MaxValue fields.

  rdcamera_property_value_request:
    doc: |
      MS-RDPECAM 2.2.3.18 Property Value Request
      The Property Value Request message is sent by the server over a device channel to retrieve the
      current value of the specified device property of the video capture device.
      In response the client MUST send either the Property Value Response (section 2.2.3.19) or
      Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToRDMPropertyId
      mstscax!MediaSourceVCCallback::OnPropertyValueRequest
      mstscax!CameraAdapter::GetPropertyValue
      mstscax!RDMediaProtocolHelper::CreatePropertyValueResponse
    seq:
      - id: property_set
        -orig-id: PropertySet
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the property set of the property being queried.
          Valid property sets are defined in the description of the PropertySet field of the
          PROPERTY_DESCRIPTION (section 2.2.3.17.1) structure.
      - id: property_id
        -orig-id: PropertyId
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the ID of the property being queried.
          Valid IDs are defined in the description of the PropertyId field of the
          PROPERTY_DESCRIPTION structure.

  rdcamera_property_value_response:
    doc: |
      MS-RDPECAM 2.2.3.19 Property Value Response
      The Property Value Response message is sent by the client over a device channel in response
      to the Property Value Request (section 2.2.3.18) message. It contains the current value of
      the requested device property of the video capture device associated with the channel.
      mstscax!CameraAdapter::GetPropertyValue
      mstscax!RDMediaProtocolHelper::CreatePropertyValueResponse
    seq:
      - id: property_value
        -orig-id: PropertyValue
        type: rdcamera_property_value
        doc:
          A PROPERTY_VALUE (section 2.2.3.19.1) structure containing the current value of the device
          property requested in the corresponding Property Value Request (section 2.2.3.18) message.

  rdcamera_property_value:
    doc: |
      MS-RDPECAM The PROPERTY_VALUE structure contains the current value of
      a video capture device property.
      mstscax!CameraAdapter::GetPropertyValue
      mstscax!RDMediaProtocolHelper::CreatePropertyValueResponse
      mstscax!CameraAdapter::SetPropertyValue
    seq:
      - id: mode
        -orig-id: Mode
        type: u1
        enum: mode
        doc: |
          An 8-bit unsigned integer that specifies how the property was set.
      - id: value
        -orig-id: Value
        type: u4
        doc: |
          A 32-bit signed integer containing the current value of the property.

  rdcamera_set_property_value_request:
    doc: |
      MS-RDPECAM 2.2.3.20 Set Property Value Request
      The Set Property Value Request message is sent by the server over a device channel to set the
      current value of the specified device property of the video capture device or to switch the
      property between manual and automatic mode. In response the client MUST send either the
      Success Response (section 2.2.3.1) or Error Response (section 2.2.3.2) message.
      mstscax!MediaSourceVCCallback::OnDataReceived
      mstscax!RDMediaProtocolHelper::PayloadToRDMPropertyIdAndRDM_PROPERTY_VALUE
      mstscax!MediaSourceVCCallback::OnSetPropertyValueRequest
      mstscax!CameraAdapter::SetPropertyValue
    seq:
      - id: property_set
        -orig-id: PropertySet
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the property set of the property being queried.
          Valid property sets are defined in the description of the PropertySet field of the
          PROPERTY_DESCRIPTION (section 2.2.3.17.1) structure.
      - id: property_id
        -orig-id: PropertyId
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the ID of the property being queried. Valid IDs are
          defined in the description of the PropertyId field of the PROPERTY_DESCRIPTION structure.
      - id: property_value
        -orig-id: PropertyValue
        type: rdcamera_property_value
        doc: |
          A PROPERTY_VALUE (section 2.2.3.19.1) structure. If the Mode field is set to Auto (2) the
          Value field MUST be ignored.
