meta:
  id: fbr_pdu
  endian: le

doc: |
  Frame Buffer channel.
  Dynamic channel "Microsoft::Windows::RDS::Frame_Buffer::Control::v08.01"
  Not supported on WDAG:
    - EnableFBR property is not set.
    - See mstscax!CFrameBufferPlugin::CheckClientSupport
  mstscax!CFrameBufferPlugin::OnNewChannelConnection
  mstscax!FrameBufferClientChannel::OnDataReceived

seq:
  - id: size
    type: u4
    doc: |
      Total size of this PDU.
  - id: msg_type
    type: u4
    enum: fbr
    doc: |
      1 for client hello, 2 for server hello, 3 for client ack.
  - id: reserved
    type: u1
    doc: |
      Unused.
  - id: msg
    type:
      switch-on: msg_type
      -name: type
      cases:
        'fbr::client_hello': fbr_client_hello
        'fbr::server_hello': fbr_server_hello
        'fbr::client_ack': fbr_client_ack

enums:
  fbr:
    1: client_hello
    2: server_hello
    3: client_ack

types:
  fbr_client_hello:
    doc: |
      Send when this dynamic channel is successfully opened.
      mstscax!FrameBufferClientChannel::CreateClientHello
    seq:
    - id: sid_size
      type: u4
      doc: |
        Size of SID
    - id: sid
      -orig-id: SID
      size: sid_size
      doc: |
        A SID structure of the logged on user.
    - id: reserved
      type: u1
      doc: |
        Unused.

  fbr_server_hello:
    doc: |
      Sends the following to be opened on the client:
        Memory: "Global\\Microsoft::Windows::RDS::FBR-<MEMORY_NAME_SUFFIX>"
        Mutex: "Global\\Microsoft::Windows::RDS::FBR-<MUTEX_NAME_SUFFIX>"
        Event: "Global\\Microsoft::Windows::RDS::FBR-<EVENT_NAME_SUFFIX>"
      mstscax!FrameBufferClientChannel::HandleServerHello
      mstscax!FrameBufferClientChannel::ParseServerHello
    seq:
      - id: memory_name_suffix_offset
        -orig-id: szMemoryNameSuffixOffset
        type: u4
        doc: |
          Must be after fixed-sized data in the PDU (exactly 33 bytes).
      - id: memory_name_suffix_size
        -orig-id: szMemoryNameSuffixSize
        type: u4
        doc: |
          Must be less-than or equal to 520 bytes.
      - id: mutex_name_suffix_offset
        -orig-id: szMutexNameSuffixOffset
        type: u4
        doc: |
          Must be immediately after szMemoryNameSuffix.
      - id: mutex_name_suffix_size
        -orig-id: szMutexNameSuffixSize
        type: u4
        doc: |
          Must be less-than or equal to 520 bytes.
      - id: event_name_suffix_offset
        -orig-id: szEventNameSuffixOffset
        type: u4
      - id: event_name_suffix_size
        -orig-id: szEventNameSuffixSize
        type: u4
        doc: |
          Must be less-than or equal to 520 bytes.
      - id: memory_name_suffix
        -orig-id: szMemoryNameSuffix
        size: memory_name_suffix_size
        type: str
        encoding: utf16-le
        doc: |
          Wide-character string of the Memory Name Suffix.
          Must be less-than or equal to 520 bytes.
      - id: mutex_name_suffix
        -orig-id: szMutexNameSuffix
        size: mutex_name_suffix_size
        type: str
        encoding: utf16-le
        doc: |
          Wide-character string of the Mutex Name Suffix.
          Must be less-than or equal to 520 bytes.
      - id: event_name_suffix
        -orig-id: szEventNameSuffix
        size: event_name_suffix_size
        type: str
        encoding: utf16-le
        doc: |
          Wide-character string of the Event Name Suffix.
          Must be less-than or equal to 520 bytes.

  fbr_client_ack:
    doc: |
      Sent in response to a server hello.
      mstscax!FrameBufferClientChannel::CreateClientAck
      (no data)
