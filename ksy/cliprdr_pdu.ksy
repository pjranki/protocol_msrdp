meta:
  id: cliprdr_pdu
  endian: le

doc: |
    MS-RDPECLIP 2 Messages
    TThe Remote Desktop Protocol: Clipboard Virtual Channel Extension is designed to operate
    over a static virtual channel, as specified in [MS-RDPBCGR] sections 1.3.3, 2.2.6 and
    3.1.5.2. The virtual channel name is "CLIPRDR". The Remote Desktop Protocol layer manages
    the creation, setup, and transmission of data over the virtual channel.
    MS-RDPECLIP 2.2.1 Clipboard PDU Header (CLIPRDR_HEADER)
    The CLIPRDR_HEADER structure is present in all clipboard PDUs. It is used to identify the
    PDU type, specify the length of the PDU, and convey message flags.
    mstscax!CClientRdrVirtualChannel::OnVirtualChannelPdu
    mstscax!CClientClipRdrPduDispatcher::DispatchPdu
    mstscax!CClipRdrPduDispatcher::DispatchPdu

seq:
  - id: msg_type
    type: u2
    enum: cb
    doc: |
      An unsigned, 16-bit integer that specifies the type of the clipboard PDU that follows the dataLen field.
  - id: msg_flags
    type: u2
    doc: |
      An unsigned, 16-bit integer that indicates message flags.
  - id: data_len
    type: u4
    doc: |
      An unsigned, 32-bit integer that specifies the size, in bytes, of the data which follows the Clipboard PDU
      Header.<1>
  - id: data
    size: data_len
    process: msrdp_cliprdr_data(_root)
    type:
      switch-on: msg_type
      -name: type
      cases:
        'cb::monitor_ready': cliprdr_monitor_ready
        'cb::format_list': cliprdr_format_list(msg_flags)
        'cb::format_list_response': cliprdr_format_list_response
        'cb::format_data_request': cliprdr_format_data_request
        'cb::format_data_response': cliprdr_format_data_response
        'cb::temp_directory': cliprdr_temp_directory
        'cb::clip_caps': cliprdr_caps
        'cb::filecontents_request': cliprdr_filecontents_request
        'cb::filecontents_response': cliprdr_filecontents_response
        'cb::lock_clipdata': cliprdr_lock_clipdata
        'cb::unlock_clipdata': cliprdr_unlock_clipdata

enums:
  cb:
    0x0001:
      id: monitor_ready
      doc: Monitor Ready PDU
    0x0002:
      id: format_list
      doc: Format List PDU
    0x0003:
      id: format_list_response
      doc: Format List Response PDU
    0x0004:
      id: format_data_request
      doc: Format Data Request PDU
    0x0005:
      id: format_data_response
      doc: Format Data Response PDU
    0x0006:
      id: temp_directory
      doc: Temporary Directory PDU
    0x0007:
      id: clip_caps
      doc: Clipboard Capabilities PDU
    0x0008:
      id: filecontents_request
      doc: File Contents Request PDU
    0x0009:
      id: filecontents_response
      doc: File Contents Response PDU
    0x000A:
      id: lock_clipdata
      doc: Lock Clipboard Data PDU
    0x000B:
      id: unlock_clipdata
      doc: Unlock Clipboard Data PDU
  cb_flag:
    0x0001:
      id: resposne_ok
      doc: |
        Used by the Format List Response PDU, Format Data Response PDU, and File Contents
        Response PDU to indicate that the associated request Format List PDU, Format Data
        Request PDU, and File Contents Request PDU were processed successfully.
    0x0002:
      id: response_fail
      doc: |
        Used by the Format List Response PDU, Format Data Response PDU, and File Contents
        Response PDU to indicate that the associated Format List PDU, Format Data Request
        PDU, and File Contents Request PDU were not processed successfully.
    0x0004:
      id: ascii_names
      doc: |
        Used by the Short Format Name variant of the Format List Response PDU to indicate
        that the format names are in ASCII 8.
    0x8000:
      id: long_names
      doc: |
        This is an artificial flag added to make parsing format list requests easier.
        The process function can set this flag as needed.
    0x4000:
      id: short_names
      doc: |
        This is an artificial flag added to make parsing format list requests easier.
        The process function can set this flag as needed.

types:
  cliprdr_monitor_ready:
    doc: |
      MS-RDPECLIP 2.2.2.2 Server Monitor Ready PDU (CLIPRDR_MONITOR_READY)
      The Monitor Ready PDU is sent from the server to the client to indicate that the server
      is initialized and ready. This PDU is transmitted by the server after it has sent the
      Clipboard Capabilities PDU to the client.
      mstscax!CClipClient::OnMonitorReady
      (no data)

  cliprdr_format_list:
    doc: |
      MS-RDPECLIP 2.2.3.1 Format List PDU (CLIPRDR_FORMAT_LIST)
      The Format List PDU is sent by either the client or the server when its local system
      clipboard is updated with new clipboard data. This PDU contains the Clipboard Format
      ID and name pairs of the new Clipboard Formats on the clipboard.
      mstscax!CClipClient::OnFormatList
      mstscax!CClipBase::OnFormatList
      mstscax!CClipBase::CreateDataObjectFromFormatList
      mstscax!CFormatListCache::Update
    params:
      - id: msg_flags
        type: u2
    seq:
      - id: format_list_data
        size-eos: true
        type:
          switch-on: format_list_type
          -name: type
          cases:
            'format_list::data': cliprdr_raw_format_names
            'format_list::short_names': cliprdr_short_format_names(msg_flags)
            'format_list::long_names': cliprdr_long_format_names
        doc: |
          An array consisting solely of either Short Format Names or Long Format Names. The type
          of structure used in the array is determined by the presence of the CB_USE_LONG_FORMAT_NAMES
          (0x00000002) flag in the generalFlags field of the General Capability Set (section 2.2.2.1.1.1).
          Each array holds a list of the Clipboard Format ID and name pairs available on the local
          system clipboard of the sender. If Short Format Names are being used, and the embedded Clipboard
          Format names are in ASCII 8 format, then the msgFlags field of the clipHeader must contain the
          CB_ASCII_NAMES (0x0004) flag.
    instances:
      format_list_type:
        value: (msg_flags >> 14) & 0x3
        enum: format_list
    enums:
      format_list:
        0: data
        1: short_names
        2: long_names

  cliprdr_raw_format_names:
    doc: |
      Raw format list data, no artificial flag was set to tell us if it is
      short of long names.
    seq:
      - id: data
        size-eos: true

  cliprdr_short_format_names:
    doc: |
      MS-RDPECLIP 2.2.3.1.1 Short Format Names (CLIPRDR_SHORT_FORMAT_NAMES)
      The CLIPRDR_SHORT_FORMAT_NAMES structure holds a collection of CLIPRDR_SHORT_FORMAT_NAME structures.
      mstscax!CreateFormatNamePacker
      mstscax!CShortFormatNamePacker::SetBlobToDecode
      mstscax!CShortFormatNamePacker::DecodeFormatNames
    params:
      - id: msg_flags
        type: u2
    seq:
      - id: short_format_names
        type: cliprdr_short_format_name(msg_flags)
        repeat: eos
        doc: |
          An array of CLIPRDR_SHORT_FORMAT_NAME structures.

  cliprdr_short_format_name:
    doc: |
      MS-RDPECLIP 2.2.3.1.1.1 Short Format Name (CLIPRDR_SHORT_FORMAT_NAME)
      The CLIPRDR_SHORT_FORMAT_NAME structure holds a Clipboard Format ID and Clipboard Format name pair.
      mstscax!CShortFormatNamePacker::DecodeFormatNames
      mstscax!CFormatNamePacker::IsExcludedFormat
    params:
      - id: msg_flags
        type: u2
    seq:
      - id: format_id
        type: u4
        doc: |
          An unsigned, 32-bit integer specifying the Clipboard Format ID.
      - id: format_ascii_name
        size: 32
        type: str
        encoding: ascii
        if: not unicode
        doc: |
          A 32-byte block containing the null-terminated name assigned to the Clipboard Format
          (32 ASCII 8 characters or 16 Unicode characters). If the name does not fit, it MUST
          be truncated.
          Not all Clipboard Formats have a name, and in that case the formatName field MUST
          contain only zeros.
          This can be an ASCII or UNICODE string depending on the CB_ASCII_NAMES flag in the
          msgFlags field in the CLIPRDR_PDU.
      - id: format_unicode_name
        size: 32
        type: str
        encoding: utf-16le
        if: unicode
        doc: |
          A 32-byte block containing the null-terminated name assigned to the Clipboard Format
          (32 ASCII 8 characters or 16 Unicode characters). If the name does not fit, it MUST
          be truncated.
          Not all Clipboard Formats have a name, and in that case the formatName field MUST
          contain only zeros.
          This can be an ASCII or UNICODE string depending on the CB_ASCII_NAMES flag in the
          msgFlags field in the CLIPRDR_PDU.
    instances:
      unicode:
        value: (msg_flags & 0x0004) == 0

  cliprdr_long_format_names:
    doc: |
      MS-RDPECLIP 2.2.3.1.2 Long Format Names (CLIPRDR_LONG_FORMAT_NAMES)
      The CLIPRDR_LONG_FORMAT_NAMES structure holds a collection of CLIPRDR_LONG_FORMAT_NAME structures.
      mstscax!CreateFormatNamePacker
      mstscax!CLongFormatNamePacker::SetBlobToDecode
      mstscax!CLongFormatNamePacker::DecodeFormatNames
    seq:
      - id: long_format_names
        type: cliprdr_long_format_name
        repeat: eos
        doc: |
          An array of CLIPRDR_LONG_FORMAT_NAME structures.

  cliprdr_long_format_name:
    doc: |
      MS-RDPECLIP 2.2.3.1.2.1 Long Format Name (CLIPRDR_LONG_FORMAT_NAME)
      The CLIPRDR_LONG_FORMAT_NAME structure holds a Clipboard Format ID and a Clipboard Format name pair.
      mstscax!CLongFormatNamePacker::DecodeFormatNames
      mstscax!CFormatNamePacker::IsExcludedFormat
    seq:
      - id: format_id
        type: u4
        doc: |
          An unsigned, 32-bit integer specifying the Clipboard Format ID.
      - id: format_name
        type: strz
        encoding: utf-16le
        doc: |
          A variable length null-terminated Unicode string name that contains the Clipboard Format name.
          Not all Clipboard Formats have a name; in such cases, the formatName field MUST consist of a
          single Unicode null character.

  cliprdr_format_list_response:
    doc: |
      MS-RDPECLIP 2.2.3.2 Format List Response PDU (FORMAT_LIST_RESPONSE)
      The Format List Response PDU is sent as a reply to the Format List PDU. It is used to indicate
      whether processing of the Format List PDU was successful.
      The msgType field of the Clipboard PDU Header MUST be set to CB_FORMAT_LIST_RESPONSE (0x0003).
      The CB_RESPONSE_OK (0x0001) or CB_RESPONSE_FAIL (0x0002) flag MUST be set in the msgFlags field
      of the Clipboard PDU Header.
      mstscax!CClipBase::OnFormatListResponse
      (no data)

  cliprdr_format_data_request:
    doc: |
      MS-RDPECLIP 2.2.5.1 Format Data Request PDU (CLIPRDR_FORMAT_DATA_REQUEST)
      The Format Data Request PDU is sent by the recipient of the Format List PDU. It is used to request
      the data for one of the formats that was listed in the Format List PDU.
      mstscax!CClipBase::OnFormatDataRequest
    seq:
      - id: requested_format_id
        type: u4
        doc: |
          An unsigned, 32-bit integer that specifies the Clipboard Format ID of the clipboard data.
          The Clipboard Format ID MUST be one listed previously in the Format List PDU.

  cliprdr_format_data_response:
    doc: |
      MS-RDPECLIP 2.2.5.2 Format Data Response PDU (CLIPRDR_FORMAT_DATA_RESPONSE)
      The Format Data Response PDU is sent as a reply to the Format Data Request PDU. It is used to indicate
      whether processing of the Format Data Request PDU was successful. If the processing was successful,
      the Format Data Response PDU includes the contents of the requested clipboard data.
      mstscax!CClipBase::OnFormatDataResponse
      mstscax!CFormatDataPacker::DecodeFormatData
      mstscax!CPalettePacker::DecodePalette
      mstscax!CMetaFilePacker::DecodeMetaFile
      mstscax!CHdropPacker::DecodeHdrop
      mstscax!CFileNamePacker::DecodeFileName
      mstscax!CFormatDataPacker::ValidateFilePaths
    seq:
      - id: requested_format_data
        size-eos: true
        doc: |
          Variable length clipboard format data. The contents of this field MUST be one of the following
          types: generic, Packed Metafile Payload, or Packed Palette Payload.

  cliprdr_temp_directory:
    doc: |
      MS-RDPECLIP 2.2.2.3 Client Temporary Directory PDU (CLIPRDR_TEMP_DIRECTORY)
      The Temporary Directory PDU is an optional PDU sent from the client to the server. This PDU
      informs the server of a location on the client file system that MUST be used to deposit files
      being copied to the client. The location MUST be accessible by the server to be useful.
      Section 3.1.1.3 specifies how direct file access impacts file copy and paste. This PDU is
      sent by the client after receiving the Monitor Ready PDU.
    seq:
      - id: temp_dir
        size: 520
        type: strz
        encoding: utf-16le
        doc: |
          A 520-byte block that contains a null-terminated string that represents the directory on the
          client that MUST be used to store temporary clipboard related information. The supplied path
          MUST be absolute and relative to the local client system, for example, "c:\temp\clipdata".
          Any space not used in this field SHOULD be filled with null characters.

  cliprdr_caps:
    doc: |
      MS-RDPECLIP 2.2.2.1 Clipboard Capabilities PDU (CLIPRDR_CAPS)
      The CLIPRDR_CAPS_SET structure is used to wrap capability set data and to specify the type and
      size of this data exchanged between the client and the server. All capability sets conform to
      this basic structure.
      mstscax!CClipClient::OnClipCaps
      mstscax!CClipBase::OnClipCaps
    seq:
      - id: capability_sets_count
        type: u2
        doc: |
          An unsigned, 16-bit integer that specifies the number of CLIPRDR_CAPS_SETs, present in the
          capabilitySets field.
      - id: pad1
        type: u2
        doc: |
          An unsigned, 16-bit integer used for padding. Values in this field are ignored.
      - id: capability_sets
        type: cliprdr_caps_set
        repeat: expr
        repeat-expr: capability_sets_count
        doc: |
          A variable-sized array of capability sets, each conforming in structure to the CLIPRDR_CAPS_SET.

  cliprdr_caps_set:
    doc: |
      MS-RDPECLIP 2.2.2.1.1 Capability Set (CLIPRDR_CAPS_SET)
      The CLIPRDR_CAPS_SET structure is used to wrap capability set data and to specify the type and size
      of this data exchanged between the client and the server. All capability sets conform to this basic
      structure.
      mstscax!CClipBase::OnClipCaps
    seq:
      - id: capability_set_type
        type: u2
        enum: cb_capstype
        doc: |
          An unsigned, 16-bit integer used as a type identifier of the capability set.
      - id: length_capability
        type: u2
        doc: |
          An unsigned, 16-bit integer that specifies the combined length, in bytes, of the capabilitySetType,
          capabilityData and lengthCapability fields.
      - id: capability_data
        size: 'length_capability >= 4 ? length_capability - 4 : 0'
        type:
          switch-on: capability_set_type
          -name: type
          cases:
            'cb_capstype::general': cliprdr_general_capability
        doc: |
          Capability set data specified by the type given in the capabilitySetType field. This field is a
          variable number of bytes.
    enums:
      cb_capstype:
        0x0001: general

  cliprdr_general_capability:
    doc: |
      MS-RDPECLIP 2.2.2.1.1.1 General Capability Set (CLIPRDR_GENERAL_CAPABILITY)
      The CLIPRDR_GENERAL_CAPABILITY structure is used to advertise general clipboard settings.
      mstscax!CClipBase::OnClipCaps
    seq:
      - id: version
        type: u4
        doc: |
          An unsigned, 32-bit integer that specifies the Remote Desktop Protocol: Clipboard Virtual Channel
          Extension version number. This field is for informational purposes and MUST NOT be used to make
          protocol capability decisions. The actual features supported are specified in the generalFlags
          field.
      - id: general_flags
        type: u4
        doc: |
          An unsigned, 32-bit integer that specifies the general capability flags.
    enums:
      cb_caps:
        0x00000001:
          id: version_1
          doc: Version 1
        0x00000002:
          id: version_2
          doc: Version 2
      cb:
        0x00000002:
          id: use_long_format_names
          doc: |
            The Long Format Name variant of the Format List PDU is supported for exchanging updated format
            names. If this flag is not set, the Short Format Name variant MUST be used. If this flag is set
            by both protocol endpoints, then the Long Format Name variant MUST be used.
        0x00000004:
          id: stream_fileclip_enabled
          doc: |
            File copy and paste using stream-based operations are supported using the File Contents Request
            PDU and File Contents Response PDU.
        0x00000008:
          id: fileclip_no_file_paths
          doc: |
            Indicates that any description of files to copy and paste MUST NOT include the source path of
            the files.
        0x00000010:
          id: can_lock_clipdata
          doc: |
            Locking and unlocking of File Stream data on the clipboard is supported using the Lock Clipboard
            Data PDU and Unlock Clipboard Data PDU.
        0x00000020:
          id: huge_file_support_enabled
          doc: |
            Indicates support for transferring files that are larger than 4,294,967,295 bytes in size. If this
            flag is not set, then only files of size less than or equal to 4,294,967,295 bytes can be exchanged
            using the File Contents Request PDU and File Contents Response PDU.

  cliprdr_filecontents_request:
    doc: |
      MS-RDPECLIP 2.2.5.3 File Contents Request PDU (CLIPRDR_FILECONTENTS_REQUEST)
      The File Contents Request PDU is sent by the recipient of the Format List PDU. It is used to request either
      the size of a remote file copied to the clipboard or a portion of the data in the file.
      mstscax!CClipBase::OnFileContentsRequest
    seq:
      - id: stream_id
        type: u4
        doc: |
          An unsigned, 32-bit format ID used to associate the File Contents Request PDU with the corresponding
          File Contents Response PDU. The File Contents Response PDU is sent as a reply and contains an identical
          value in the streamId field.
      - id: index
        type: u4
        doc: |
          A signed, 32-bit integer that specifies the numeric ID of the remote file that is the target of the
          File Contents Request PDU. This field is used as an index that identifies a particular file in a
          File List. This File List SHOULD have been obtained as clipboard data in a prior Format Data Request PDU
          and Format Data Response PDU exchange.
      - id: flags
        type: u4
        doc: |
          An unsigned, 32-bit integer that specifies the type of operation to be performed by the recipient.
      - id: position
        type: u8
        doc: |
          An unsigned, 64-bit integer that specifies the offset into the remote file, identified by the index field,
          from where the data needs to be extracted to satisfy a FILECONTENTS_RANGE operation.
      - id: requested
        type: u4
        doc: |
          An unsigned, 32-bit integer that specifies the size, in bytes, of the data to retrieve. For a
          FILECONTENTS_SIZE operation, this field MUST be set to 0x00000008. In the case of a FILECONTENTS_RANGE
          operation, this field contains the maximum number of bytes to read from the remote file.
      - id: clip_data_id
        type: u4
        doc: |
          An optional unsigned, 32-bit integer that identifies File Stream data which was tagged in a prior Lock
          Clipboard Data PDU (section 2.2.4.1).
    enums:
      filecontents:
        0x00000001:
          id: size
          doc: |
            A request for the size of the file identified by the lindex field. The size MUST be returned as a
            64-bit, unsigned integer. The cbRequested field MUST be set to 0x00000008 and both the nPositionLow
            and nPositionHigh fields MUST be set to 0x00000000.
        0x00000002:
          id: range
          doc: |
            A request for the data present in the file identified by the lindex field. The data to be retrieved
            is extracted starting from the offset given by the nPositionLow and nPositionHigh fields. The maximum
            number of bytes to extract is specified by the cbRequested field.

  cliprdr_filecontents_response:
    doc: |
      MS-RDPECLIP 2.2.5.4 File Contents Response PDU (CLIPRDR_FILECONTENTS_RESPONSE)
      The File Contents Response PDU is sent as a reply to the File Contents Request PDU. It is used to indicate
      whether processing of the File Contents Request PDU was successful. If the processing was successful, the
      File Contents Response PDU includes either a file size or extracted file data, based on the operation
      requested in the corresponding File Contents Request PDU.
      mstscax!CClipBase::OnFileContentsResponse
    seq:
      - id: stream_id
        type: u4
        doc: |
          An unsigned, 32-bit numeric ID used to associate the File Contents Response PDU with the corresponding
          File Contents Request PDU. The File Contents Request PDU that triggered the response MUST contain an
          identical value in the streamId field.
      - id: requested_file_contents_data
        size-eos: true
        doc: |
          This field contains a variable number of bytes. If the response is to a FILECONTENTS_SIZE (0x00000001)
          operation, the requestedFileContentsData field holds a 64-bit, unsigned integer containing the size of
          the file. In the case of a FILECONTENTS_RANGE (0x00000002) operation, the requestedFileContentsData
          field contains a byte-stream of data extracted from the file.

  cliprdr_lock_clipdata:
    doc: |
      MS-RDPECLIP 2.2.4.1 Lock Clipboard Data PDU (CLIPRDR_LOCK_CLIPDATA)
      The Lock Clipboard Data PDU can be sent at any point in time after the clipboard capabilities and
      temporary directory have been exchanged in the Clipboard Initialization Sequence (section 1.3.2.1)
      by a Local Clipboard Owner (section 1.3.2.2.1). The purpose of this PDU is to request that the Shared
      Clipboard Owner (section 1.3.2.2.1) retain all File Stream (section 1.3.1.1.5) data on the clipboard
      until the Unlock Clipboard Data PDU (section 2.2.4.2) is received. This ensures that File Stream data
      can be requested by the Local Owner in a subsequent File Contents Paste Sequence (section 1.3.2.2.3)
      by using the File Contents Request PDU (section 2.2.5.3) even when the Shared Owner clipboard has
      changed and the File Stream data is no longer available.
      mstscax!CClipBase::OnLockClipData
      mstscax!CClipBase::StoreReader
    seq:
      - id: clip_data_id
        type: u4
        doc: |
          An unsigned, 32-bit integer that is used to tag File Stream data on the Shared Owner clipboard so
          that it can be requested in a subsequent File Contents Request PDU (section 2.2.5.3).

  cliprdr_unlock_clipdata:
    doc: |
      MS-RDPECLIP 2.2.4.2 Unlock Clipboard Data PDU (CLIPRDR_UNLOCK_CLIPDATA)
      The Unlock Clipboard Data PDU can be sent at any point in time after the Clipboard Initialization
      Sequence (section 1.3.2.1) by a Local Clipboard Owner (section 1.3.2.2.1). The purpose of this PDU
      is to notify the Shared Clipboard Owner (section 1.3.2.2.1) that File Stream data that was locked in
      response to the Lock Clipboard Data PDU (section 2.2.4.1) can be released.
      mstscax!CClipBase::OnUnlockClipData
      mstscax!CClipBase::RemoveReader
    seq:
      - id: clip_data_id
        type: u4
        doc: |
          An unsigned, 32-bit integer that identifies the File Stream data that was locked by the Lock
          Clipboard Data PDU (section 2.2.4.1) and can now be released.
