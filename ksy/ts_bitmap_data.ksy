meta:
  id: ts_bitmap_data
  endian: le
  imports:
    - ts_cd_header

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.2.2 Bitmap Data (TS_BITMAP_DATA)
  The TS_BITMAP_DATA structure wraps the bitmap data for a screen area
  rectangle containing a clipping taken from the server-side screen
  frame buffer.
  mstscax!CTSCoreGraphics::ProcessBitmapRect

seq:
  - id: dest_left
    type: u2
    doc: |
      A 16-bit, unsigned integer. Left bound of the rectangle.
  - id: dest_top
    type: u2
    doc: |
      A 16-bit, unsigned integer. Top bound of the rectangle.
  - id: dest_right
    type: u2
    doc: |
      A 16-bit, unsigned integer. Right bound of the rectangle.
  - id: dest_bottom
    type: u2
    doc: |
      A 16-bit, unsigned integer. Bottom bound of the rectangle.
  - id: width
    type: u2
    doc: |
      A 16-bit, unsigned integer. The width of the rectangle.
  - id: height
    type: u2
    doc: |
      A 16-bit, unsigned integer. The height of the rectangle.
  - id: bits_per_pixel
    type: u2
    doc: |
      A 16-bit, unsigned integer. The color depth of the rectangle data
      in bits-per-pixel.
  - id: flags
    type: u2
    doc: |
      A 16-bit, unsigned integer. The flags describing the format of
      the bitmap data in the bitmapDataStream field.
  - id: bitmap_length
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the data in the
      bitmapComprHdr and bitmapDataStream fields.
  - id: bitmap_compr_hdr
    type: ts_cd_header
    if: ((flags & 0x0001) != 0) and ((flags & 0x0400) == 0)
    doc: |
      Optional Compressed Data Header structure (section 2.2.9.1.1.3.1.2.3)
      specifying the bitmap data in the bitmapDataStream. This field MUST
      be present if the BITMAP_COMPRESSION (0x0001) flag is present in the
      flags field, but the NO_BITMAP_COMPRESSION_HDR (0x0400) flag is not.
  - id: bitmap_data_stream
    size: bitmap_data_stream_size
    doc: |
      A variable-length array of bytes describing a bitmap image. Bitmap
      data is either compressed or uncompressed, depending on whether the
      BITMAP_COMPRESSION flag is present in the flags field. Uncompressed
      bitmap data is formatted as a bottom-up, left-to-right series of
      pixels. Each pixel is a whole number of bytes. Each row contains a
      multiple of four bytes (including up to three bytes of padding, as
      necessary). Compressed bitmaps not in 32 bpp format are compressed
      using Interleaved RLE and encapsulated in an RLE Compressed Bitmap
      Stream structure (section 2.2.9.1.1.3.1.2.4), while compressed bitmaps
      at a color depth of 32 bpp are compressed using RDP 6.0 Bitmap
      Compression and stored inside an RDP 6.0 Bitmap Compressed Stream
      structure ([MS-RDPEGDI] section 2.2.2.5.1).

instances:
  bitmap_data_stream_size:
    value: '((flags & 0x0001) != 0) and ((flags & 0x0400) == 0) ? bitmap_length - 8 : bitmap_length'

enums:
  bitmap:
    0x0001:
      id: compression
      doc: |
        Indicates that the bitmap data is compressed. The bitmapComprHdr
        field MUST be present if the NO_BITMAP_COMPRESSION_HDR (0x0400)
        flag is not set.
    0x0400:
      id: no_compression_hdr
      doc: |
        Indicates that the bitmapComprHdr field is not present (removed
        for bandwidth efficiency to save 8 bytes).
