meta:
  id: ts_fp_update
  endian: le
  imports:
    - ts_drawing_order
    - ts_update_bitmap_data
    - ts_update_palette_data
    - ts_update_sync
    - ts_surfcmd
    - ts_pointerposattribute
    - ts_colorpointerattribute
    - ts_cachedpointerattribute
    - ts_pointerattribute
    - ts_point16

doc: |
  MS-RDPBCGR 2.2.9.1.2.1 Fast-Path Update (TS_FP_UPDATE)
  The TS_FP_UPDATE structure is used to describe and encapsulate the data for
  a fast-path update sent from server to client. All fast-path updates conform
  to this basic structure (sections 2.2.9.1.2.1.1 to 2.2.9.1.2.1.10).
  mstscax!CCO::OnFastPathOutputReceived

seq:
  - id: compression
    type: b2
    enum: fastpath_output_compression
    doc: |
      A 2-bit, unsigned integer that specifies compression parameters.
  - id: fragmentation
    type: b2
    enum: fastpath_fragment
    doc: |
      A 2-bit, unsigned integer that specifies compression parameters.
  - id: code
    type: b4
    doc: |
      A 4-bit, unsigned integer that specifies the type code of the update.
  - id: compression_flags
    type: u1
    if: compression == fastpath_output_compression::used
    doc: |
      An 8-bit, unsigned integer. Optional compression flags. The flags
      used in this field are exactly the same as the flags used in the
      compressedType field in the Share Data Header (section 2.2.8.1.1.1.2)
      and have the same meaning.
  - id: size
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the data in the
      updateData field.
  - id: data
    size: size
    process: msrdp_fastpath_update_data(_root)
    type: ts_fp_update_data(code)

instances:
  is_fragment:
    value: fragmentation != fastpath_fragment::single
  is_compressed:
    value: (compression == fastpath_output_compression::used) and ((compression_flags & 0x20) != 0)
  compression_type:
    value: compression_flags & 0xf
  compression_packed_at_front:
    value: (compression_flags & 0x40) != 0
  compression_packed_flushed:
    value: (compression_flags & 0x80) != 0

enums:
  fastpath_fragment:
    0x0:
      id: single
      doc: |
        The fast-path data in the updateData field is not part of a
        sequence of fragments.
    0x1:
      id: last
      doc: |
        The fast-path data in the updateData field contains the last
        fragment in a sequence of fragments.
    0x2:
      id: first
      doc: |
        The fast-path data in the updateData field contains the first
        fragment in a sequence of fragments.
    0x3:
      id: next
      doc: |
        The fast-path data in the updateData field contains the second
        or subsequent fragment in a sequence of fragments.
  fastpath_output_compression:
    0x2:
      id: used
      doc: |
        Indicates that the compressionFlags field is present.

types:
  ts_fp_update_data:
    params:
      - id: code
        type: u1
    seq:
      - id: data
        type:
          switch-on: code
          -name: type
          cases:
            'fastpath_updatetype::raw.to_i': ts_fp_update_raw
            'fastpath_updatetype::orders.to_i': ts_fp_update_orders
            'fastpath_updatetype::bitmap.to_i': ts_fp_update_bitmap
            'fastpath_updatetype::palette.to_i': ts_fp_update_palette
            'fastpath_updatetype::synchronize.to_i': ts_fp_update_synchronize
            'fastpath_updatetype::surfcmds.to_i': ts_fp_surfcmds
            'fastpath_updatetype::ptr_null.to_i': ts_fp_systempointerhiddenattribute
            'fastpath_updatetype::ptr_default.to_i': ts_fp_systempointerdefaultattribute
            'fastpath_updatetype::ptr_position.to_i': ts_fp_pointerposattribute
            'fastpath_updatetype::color.to_i': ts_fp_colorpointerattribute
            'fastpath_updatetype::cached.to_i': ts_fp_cachedpointerattribute
            'fastpath_updatetype::pointer.to_i': ts_fp_pointerattribute
            'fastpath_updatetype::large_pointer.to_i': ts_fp_largepointerattribute
            'fastpath_updatetype::meta_command.to_i': ts_fp_meta_command_update
            'fastpath_updatetype::delta_encoder.to_i': ts_fp_delta_encoder_update
        doc: |
          Optional and variable-length data specific to the update.
    enums:
      fastpath_updatetype:
        0x0:
          id: orders
          doc: Indicates a Fast-Path Orders Update ([MS-RDPEGDI] section 2.2.2.2).
        0x1:
          id: bitmap
          doc: Indicates a Fast-Path Bitmap Update (section 2.2.9.1.2.1.2).
        0x2:
          id: palette
          doc: Indicates a Fast-Path Palette Update (section 2.2.9.1.2.1.1).
        0x3:
          id: synchronize
          doc: Indicates a Fast-Path Synchronize Update (section 2.2.9.1.2.1.3).
        0x4:
          id: surfcmds
          doc: Indicates a Fast-Path Surface Commands Update (section 2.2.9.1.2.1.10).
        0x5:
          id: ptr_null
          doc: Indicates a Fast-Path System Pointer Hidden Update (section 2.2.9.1.2.1.5).
        0x6:
          id: ptr_default
          doc: Indicates a Fast-Path System Pointer Default Update (section 2.2.9.1.2.1.6).
        0x8:
          id: ptr_position
          doc: Indicates a Fast-Path Pointer Position Update (section 2.2.9.1.2.1.4).
        0x9:
          id: color
          doc: Indicates a Fast-Path Color Pointer Update (section 2.2.9.1.2.1.7).
        0xA:
          id: cached
          doc: Indicates a Fast-Path Cached Pointer Update (section 2.2.9.1.2.1.9).
        0xB:
          id: pointer
          doc: Indicates a Fast-Path New Pointer Update (section 2.2.9.1.2.1.8).
        0xC:
          id: large_pointer
          doc: Indicates a Fast-Path Large Pointer Update (section 2.2.9.1.2.1.11).
        0xD:
          id: meta_command
          doc: Undocumented command
        0xE:
          id: delta_encoder
          doc: Undocumented command
        0xf:
          id: raw

  ts_fp_update_raw:
    seq:
      - id: data
        size-eos: true

  ts_fp_update_orders:
    doc: |
      MS-RDPEGDI 2.2.2.2 Fast-Path Orders Update (TS_FP_UPDATE_ORDERS)
      The TS_FP_UPDATE_ORDERS structure contains primary, secondary, and
      alternate secondary drawing orders aligned on byte boundaries. This
      structure conforms to the layout of a Fast-Path Update (see [MS-RDPBCGR]
      section 2.2.9.1.2.1) and is encapsulated within a Fast-Path Update PDU
      (see [MS-RDPBCGR] section 2.2.9.1.2.1.1).
      mstscax!CTSCoreGraphics::ProcessOrders
      mstscax!CUH::ProcessOrders
    seq:
      - id: number_orders
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of Drawing Order (section 2.2.2.1.1)
          structures contained in the orderData field.
      - id: orders
        type: 'ts_drawing_order(orders.size == 0 ? 1 : orders[orders.size - 1].primary_order_type)'
        repeat: expr
        repeat-expr: number_orders

  ts_fp_update_bitmap:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.2 Fast-Path Bitmap Update (TS_FP_UPDATE_BITMAP)
      The TS_FP_UPDATE_BITMAP structure is the fast-path variant of the
      TS_UPDATE_BITMAP (section 2.2.9.1.1.3.1.2) structure.
      mstscax!CTSCoreGraphics::ProcessBitmap
    seq:
      - id: bitmap_data
        type: ts_update_bitmap_data
        doc: |
          Variable-length bitmap data. Both slow-path and fast-path utilize
          the same data format, a Bitmap Update Data (section 2.2.9.1.1.3.1.2.1)
          structure, to represent this information.

  ts_fp_update_palette:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.1 Fast-Path Palette Update (TS_FP_UPDATE_PALETTE)
      The TS_FP_UPDATE_PALETTE structure is the fast-path variant of the
      TS_UPDATE_PALETTE (section 2.2.9.1.1.3.1.1) structure.
      mstscax!CTSCoreGraphics::ProcessPalette
    seq:
      - id: palette_data
        type: ts_update_palette_data
        doc: |
          Variable-length palette data. Both slow-path and fast-path utilize the
          same data format, a Palette Update Data (section 2.2.9.1.1.3.1.1.1)
          structure, to represent this information.

  ts_fp_update_synchronize:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.3 Fast-Path Synchronize Update (TS_FP_UPDATE_SYNCHRONIZE)
      The TS_FP_UPDATE_SYNCHRONIZE structure is the fast-path variant of the
      TS_UPDATE_SYNCHRONIZE_PDU_DATA (section 2.2.9.1.1.3.1.3) structure.
      mstscax!CTSCoreGraphics::ProcessSync
    seq:
      - id: update_sync
        type: ts_update_sync
        doc: |
          Contains the same data as the slow path variant 2.2.9.1.1.3.1.3 TS_UPDATE_SYNC.

  ts_fp_surfcmds:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.10 Fast-Path Surface Commands Update (TS_FP_SURFCMDS)
      The TS_FP_SURFCMDS structure encapsulates one or more Surface Command
      (section 2.2.9.1.2.1.10.1) structures.
      mstscax!CTSCoreGraphics::ProcessSurfaceCommands
    seq:
      - id: surface_commands
        type: ts_surfcmd
        repeat: eos
        doc: |
          An array of Surface Command (section 2.2.9.1.2.1.10.1) structures
          containing a collection of commands to be processed by the client.

  ts_fp_systempointerhiddenattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.5 Fast-Path System Pointer Hidden Update (TS_FP_SYSTEMPOINTERHIDDENATTRIBUTE)
      The TS_FP_SYSTEMPOINTERHIDDENATTRIBUTE structure is used to hide the pointer.
      mstscax!CCM::CM_NullSystemPointerPDU
      (no data)

  ts_fp_systempointerdefaultattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.6 Fast-Path System Pointer Default Update (TS_FP_SYSTEMPOINTERDEFAULTATTRIBUTE)
      The TS_FP_SYSTEMPOINTERDEFAULTATTRIBUTE structure is used to
      set the shape of the pointer to the operating system default.
      mstscax!CCM::CM_DefaultSystemPointerPDU
      (no data)

  ts_fp_pointerposattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.4 Fast-Path Pointer Position Update (TS_FP_POINTERPOSATTRIBUTE)
      The TS_FP_POINTERPOSATTRIBUTE structure is the fast-path variant of the
      TS_POINTERPOSATTRIBUTE structure (section 2.2.9.1.1.4.2).
      mstscax!CCM::CM_PositionPDU
    seq:
      - id: pointer_position_data
        type: ts_pointerposattribute
        doc: |
          Pointer coordinates. Both slow-path and fast-path utilize the same data format,
          a Pointer Position Update (section 2.2.9.1.1.4.2) structure, to represent
          this information.

  ts_fp_colorpointerattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.7 Fast-Path Color Pointer Update (TS_FP_COLORPOINTERATTRIBUTE)
      The TS_FP_COLORPOINTERATTRIBUTE structure is the fast-path variant of
      the TS_COLORPOINTERATTRIBUTE (section 2.2.9.1.1.4.4) structure.
      mstscax!CCM::CM_ColorPointerPDU
    seq:
      - id: color_pointer_data
        type: ts_colorpointerattribute
        doc: |
          Color pointer data. Both slow-path and fast-path utilize the same
          data format, a Color Pointer Update (section 2.2.9.1.1.4.4) structure,
          to represent this information.

  ts_fp_cachedpointerattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.9 Fast-Path Cached Pointer Update (TS_FP_CACHEDPOINTERATTRIBUTE)
      The TS_FP_CACHEDPOINTERATTRIBUTE structure is the fast-path variant of the
      TS_CACHEDPOINTERATTRIBUTE (section 2.2.9.1.1.4.6) structure.
      mstscax!CCM::CM_CachedPointerPDU
    seq:
      - id: cached_pointer_data
        type: ts_cachedpointerattribute
        doc: |
          Cached pointer data. Both slow-path and fast-path utilize the same data
          format, a Cached Pointer Update (section 2.2.9.1.1.4.6) structure, to
          represent this information.

  ts_fp_pointerattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.8 Fast-Path New Pointer Update (TS_FP_POINTERATTRIBUTE)
      The TS_FP_POINTERATTRIBUTE structure is the fast-path variant of the
      TS_POINTERATTRIBUTE (section 2.2.9.1.1.4.5) structure.
      mstscax!CCM::CM_PointerPDU
      mstscax!CCM::CMCreateNewCursor
    seq:
      - id: new_pointer_data
        type: ts_pointerattribute
        doc: |
          Color pointer data at arbitrary color depth. Both slow-path and fast-path
          utilize the same data format, a New Pointer Update (section 2.2.9.1.1.4.5)
          structure, to represent this information.

  ts_fp_largepointerattribute:
    doc: |
      MS-RDPBCGR 2.2.9.1.2.1.11 Fast-Path Large Pointer Update (TS_FP_LARGEPOINTERATTRIBUTE)
      The TS_FP_LARGEPOINTERATTRIBUTE structure is used to transport mouse pointer shapes
      larger than 96x96 pixels in size.
      mstscax!CCM::CM_PointerPDU
      mstscax!CCM::CMCreateNewCursor
    seq:
      - id: xor_bpp
        type: u2
        doc: |
          A 16-bit, unsigned integer. The color depth in bits-per-pixel of the
          XOR mask contained in the xorMaskData field.
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The zero-based cache entry in the pointer
          cache in which to store the pointer image. The number of cache entries
          is specified using the Pointer Capability Set (section 2.2.7.1.5).
      - id: hot_spot
        type: ts_point16
        doc: |
          A Point (section 2.2.9.1.1.4.1) structure containing the x-coordinates
          and y-coordinates of the pointer hotspot.
      - id: width
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width of the pointer in pixels. The
          maximum allowed pointer width is 384 pixels.
      - id: height
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height of the pointer in pixels. The
          maximum allowed pointer height is 384 pixels.
      - id: length_and_mask
        type: u4
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the andMaskData field.
      - id: length_xor_mask
        type: u4
        doc: |
          A 32-bit, unsigned integer. The size in bytes of the xorMaskData field.
      - id: xor_mask_data
        size: length_xor_mask
        doc: |
          A variable-length array of bytes. Contains the 24-bpp, bottom-up XOR mask
          scan-line data. The XOR mask is padded to a 2-byte boundary for each encoded
          scan-line. For example, if a 3x3 pixel cursor is being sent, then each
          scan-line will consume 10 bytes (3 pixels per scan-line multiplied by 3 bytes
          per pixel, rounded up to the next even number of bytes).
      - id: and_mask_data
        size: length_and_mask
        doc: |
          A variable-length array of bytes. Contains the 1-bpp, bottom-up AND mask
          scan-line data. The AND mask is padded to a 2-byte boundary for each encoded
          scan-line. For example, if a 7x7 pixel cursor is being sent, then each
          scan-line will consume 2 bytes (7 pixels per scan-line multiplied by 1 byte
          per pixel, rounded up to the next even number of bytes).
      - id: padding
        size: (2 - _io.pos) % 2
        doc: |
          An optional 8-bit, unsigned integer used for padding. Values in this field
          MUST be ignored.

  ts_fp_meta_command_update:
    doc: |
      Undocumented fast-path command.
      mstscax!CTSCoreGraphics::ProcessMetaCommandUpdate
      mstscax!CTSGraphics::ProcessMetaCommand
      (no data)

  ts_fp_delta_encoder_update:
    doc: |
      Undocumented fast-path command.
      mstscax!CTSCoreGraphics::ProcessDeltaEncoderUpdate
      mstscax!CTSGraphics::ProcessDeltaEncoder
      (no data)
