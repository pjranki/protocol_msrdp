meta:
  id: ts_compdesk_drawing_order
  endian: le

doc: |
  MS-RDPEDC 2.2.1.1 Drawing and Desktop Mode Changes Order (TS_COMPDESK_TOGGLE)
  mstscax!CUH::UH_OnUnknownAltSecPacket
  mstscax!CTSCoreEventSource::FireSyncNotification
  mstscax!CTSCoreEventSource::InternalFireSyncNotification
  mstscax!CTSThread::AddCallback
  mstscax!CTSThread::RunQueueEvent
  mstscax!RdpWindowPlugin::OnAltSecPDUReceived
  mstscax!RdpWindowPlugin::OnWindowOrder
  mstscax!CSFMPlugin7::OnAltSecPDUReceived (not present in mstscax.dll in newer versions)
  mstscax!CCompDeskPlugin7::OnAltSecPDUReceived (not present in mstscax.dll in newer versions)
seq:
  - id: operation
    type: u1
    doc: |
      An 8-bit unsigned integer. The operation code. This field MUST be set to
      COMPDESKTOGGLE (0x01).
  - id: size
    type: u4
    doc: |
      A 16-bit unsigned integer. The size of the order data that follows the size field.
  - id: data
    size: size
    doc: |
      Desktop composition drawing order data.
