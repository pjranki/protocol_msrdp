meta:
  id: rdcamera_device_enumerator_pdu
  endian: le
  imports:
    - rdcamera_pdu

doc: |
  MS-RDPECAM 2.1 Transport
  Dynamic channel "RDCamera_Device_Enumerator"
  mstscax!DeviceEnumeratorListenerCallback::OnNewChannelConnection
  mstscax!DeviceEnumeratorVCCallback::OnDataReceived

seq:
  - id: data
    type: rdcamera_pdu
    doc: |
      Has the exact same data as a RDCAMERA_PDU.
