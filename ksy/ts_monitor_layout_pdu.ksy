meta:
  id: ts_monitor_layout_pdu
  endian: le

doc: |
  MS-RDPBCGR 2.2.12.1 Monitor Layout PDU
  The Monitor Layout PDU is used by the server to notify the client of the monitor layout in
  the session on the remote server.
  mstscax!CTSConnectionHandler::OnMonitorLayoutReceived
  mstscax!RdpGfxClientChannel::SetMonitorLayout

seq:
  - id: monitor_count
    -orig-id: monitorCount
    type: u4
    doc: |
      A 32-bit, unsigned integer. The number of display monitor definitions in the
      monitorDefArray field.
  - id: monitor_def_array
    -orig-id: monitorDefArray
    type: ts_monitor_def
    repeat: expr
    repeat-expr: monitor_count
    doc: |
      A variable-length array containing a series of TS_MONITOR_DEF structures
      (section 2.2.1.3.6.1), which describe the display monitor layout of the session
      on the remote server. The number of TS_MONITOR_DEF structures that follows is
      given by the monitorCount field.

types:
  ts_monitor_def:
    doc: |
      MS-RDPBCGR 2.2.1.3.6.1 Monitor Definition (TS_MONITOR_DEF)
      The TS_MONITOR_DEF packet describes the configuration of a client-side display monitor.
      The x and y coordinates used to describe the monitor position MUST be relative to the
      upper-left corner of the monitor designated as the "primary display monitor"
      (the upper-left corner of the primary monitor is always (0, 0)).
      mstscax!CTSMonitorConfig::ReconfigureMonitors
    seq:
      - id: left
        type: s4
        doc: | 
          A 32-bit, signed integer. Specifies the x-coordinate of the upper-left corner of the
          display monitor.
      - id: top
        type: s4
        doc: |
          A 32-bit, signed integer. Specifies the y-coordinate of the upper-left corner of the
          display monitor.
      - id: right
        type: s4
        doc: |
          A 32-bit, signed integer. Specifies the inclusive x-coordinate of the lower-right corner
          of the display monitor.
      - id: bottom
        type: s4
        doc: |
          A 32-bit, signed integer. Specifies the inclusive y-coordinate of the lower-right corner
          of the display monitor.
      - id: flags
        type: u4
        doc: |
          A 32-bit, unsigned integer. Monitor configuration flags.
    enums:
      ts_monitor:
        0x00000001:
          id: primary
          doc: The top, left, right, and bottom fields describe the position of the primary monitor.
