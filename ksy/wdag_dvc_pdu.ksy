meta:
  id: wdag_dvc_pdu
  endian: le

doc: |
  Dynamic virtual channel 'WDAG_DVC'
  hvsisettingsprovider!CHvsiDvcCallback::OnDataReceived

seq:
  - id: command
    type: u4
    enum: wdag_command
    doc: |
      WDAG command type.
  - id: msg
    type:
      switch-on: command
      -name: type
      cases:
        'wdag_command::open_downloads_folder': wdag_msg_open_downloads_folder
        'wdag_command::validate_url': wdag_msg_validate_url
        'wdag_command::open_browser_on_host': wdag_msg_open_browser_on_host
        _: wdag_msg_response

enums:
  wdag_command:
    1:
      id: open_downloads_folder
      doc: Open downloads folder on host.
    2:
      id: validate_url
      doc: Validate URL as trusted.
    3:
      id: open_browser_on_host
      doc: TODO

instances:
  result:
    value: '(command.to_i >= 1) and (command.to_i <= 3) ? 0 : command.to_i'

types:
  wdag_msg_response:
    seq:
      - id: data
        size-eos: true

  wdag_msg_open_downloads_folder:
    doc: |
      Open download folder on the host.
      hvsisettingsprovider!CHvsiDvcCallback::OnDataReceived
      hvsisettingsprovider!OpenHostHvsiDownloadsFolder
      hvsimgr!CHvsiRdpRpcServer::RpcOpenHostHvsiDownloadsFolder
      hvsimgr!CHvsiContainer::RpcOpenBrowserOnHost

  wdag_msg_validate_url:
    doc: |
      Validate URL as trusted.
      hvsisettingsprovider!CHvsiDvcCallback::OnDataReceived
      hvsisettingsprovider!IsHVSITrusted
      hvsisettingsprovider!IsHVSITrustedHost
    seq:
      - id: use_http
        type: u4
        doc: |
          Protocol for url:
           - 0: https
           - 1: http
      - id: url
        type: str
        encoding: UTF-16LE
        size-eos: true
        doc: |
          URL to validate (excluding the "http://" or "https://" component).

  wdag_msg_open_browser_on_host:
    doc: |
      Open URL on in host browser.
      hvsisettingsprovider!CHvsiRdpRpcClient::OpenBrowserOnHost
      hvsimgr!CHvsiRdpRpcServer::RpcOpenBrowserOnHost
      hvsimgr!CHvsiContainer::RpcOpenBrowserOnHost
      hvsimgr!LaunchInEdge
    seq:
      - id: use_http
        type: u4
        doc: |
          Protocol for url:
           - 0: https
           - 1: http
      - id: use_private_browsing
        type: u4
        doc: |
          Protocol for url:
           - 0: (normal browsing)
           - 1: Add -private to command line of new browser
      - id: external_user_input_id
        size: 16
        doc: |
          GUID that is added to the command line after -ExternalUserInputId
      - id: url
        type: str
        encoding: UTF-16LE
        size-eos: true
        doc: |
          URL to open on host (excluding the "http://" or "https://" component).
