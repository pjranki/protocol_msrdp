meta:
  id: clearcodec_bitmap_stream
  endian: le
  imports:
    - nscodec_bitmap_stream

doc: |
    MS-RDPEGFX 2.2.4.1 CLEARCODEC_BITMAP_STREAM
    The CLEARCODEC_BITMAP_STREAM structure encapsulates metadata and a stream of bitmap
    data encoded using ClearCodec compression techniques. Bitmaps with widths larger
    than 65,535 pixels and heights larger than 65,535 pixels MUST NOT be encoded using
    ClearCodec. ClearCodec-compressed bitmap data is transported in the bitmapData field
    of the RDPGFX_WIRE_TO_SURFACE_PDU_1 (section 2.2.2.1) message.
    mstscax!ClearDecompressor::Decompress

seq:
  - id: flags
    type: u1
    doc: |
      An 8-bit unsigned integer that specifies glyph and control flags.
  - id: seq_number
    type: u1
    doc: |
      An 8-bit unsigned integer that specifies the sequencing of the stream. For the
      first ClearCodec message in the remote session, this value MUST be 0x00. In
      subsequent messages, the value of the seqNumber field MUST be equal to the value
      of the seqNumber field in the previous ClearCodec message plus one. The sequence
      number counter wraps around the value 0xFF, with 0x00 following message 0xFF.
  - id: glyph_index
    type: u2
    if: (flags & 0x01) != 0
    doc: |
      An optional 16-bit unsigned integer that specifies the position in the Decompressor
      Glyph Storage ADM element for the current glyph. This field MUST NOT be present if
      the CLEARCODEC_FLAG_GLYPH_INDEX (0x01) flag is not present in the flags field. If
      this field is present, its value MUST be in the range 0 (0x0000) to 3,999 (0x0F9F),
      inclusive.
  - id: composite_payload
    type: clearcodec_composite_payload
    if: (flags & 0x02) == 0
    doc: |
      An optional variable-length CLEARCODEC_COMPOSITE_PAYLOAD (section 2.2.4.1.1) structure.
      This field MUST NOT be present if the CLEARCODEC_FLAG_GLYPH_INDEX (0x01) flag and the
      CLEARCODEC_FLAG_GLYPH_HIT (0x02) flag are both present in the flags field.

enums:
  clearcodec_flag:
    0x01:
      id: glyph_index
      doc: |
        Indicates that the glyphIndex field is present. This flag MUST NOT be used in
        conjunction with a bitmap that has an area larger than 1024 square pixels.
    0x02:
      id: glyph_hit
      doc: |
        Indicates the source of the glyph data. This flag MUST NOT be present if the
        CLEARCODEC_FLAG_GLYPH_INDEX (0x01) flag is not present.
        If the CLEARCODEC_FLAG_GLYPH_HIT flag is not present, the glyph data is present
        in the compositePayload field. The decompressed payload MUST be placed in the
        Decompressor Glyph Storage (section 3.3.1.9) ADM element at the index specified
        by the glyphIndex field.
        If the CLEARCODEC_FLAG_GLYPH_HIT flag is present, the glyph data is already present
        in the Decompressor Glyph Storage ADM element at the index specified by the glyphIndex
        field. In this case, the compositePayload field MUST NOT be present.
    0x04:
      id: cache_reset
      doc: |
        Indicates that both the V-Bar Storage Cursor (section 3.3.1.11) ADM element and
        Short V-Bar Storage Cursor (section 3.3.1.13) ADM element MUST be reset to 0 before
        decoding the stream.

types:
  clearcodec_composite_payload:
    doc: |
      MS-RDPEGFX 2.2.4.1.1 CLEARCODEC_COMPOSITE_PAYLOAD
      The CLEARCODEC_COMPOSITE_PAYLOAD structure contains bitmap data encoded using
      ClearCodec compression techniques.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: residual_byte_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the residualData
          field.
      - id: bands_byte_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the bandsData
          field.
      - id: subcodec_byte_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the subcodecData
          field.
      - id: residual_data
        type: clearcodec_residual_data
        size: residual_byte_count
        if: residual_byte_count != 0
        doc: |
          An optional variable-length CLEARCODEC_RESIDUAL_DATA (section 2.2.4.1.1.1) structure
          that contains the compressed data for the first layer of the image. If the
          residualByteCount field is zero, this field MUST NOT be present.
      - id: bands_data
        type: clearcodec_bands_data
        size: bands_byte_count
        if: bands_byte_count != 0
        doc: |
          An optional variable-length CLEARCODEC_BANDS_DATA (section 2.2.4.1.1.2) structure that
          contains the compressed data for the second layer of the image. If the bandsByteCount
          field is zero, this field MUST NOT be present.
      - id: subcodec_data
        type: clearcodec_subcodecs_data
        size: subcodec_byte_count
        if: subcodec_byte_count != 0
        doc: |
          An optional variable-length CLEARCODEC_SUBCODECS_DATA (section 2.2.4.1.1.3) structure
          that contains the compressed data for the third layer of the image. If the
          subcodecByteCount field is zero, this field MUST NOT be present.

  clearcodec_residual_data:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.1 CLEARCODEC_RESIDUAL_DATA
      The CLEARCODEC_RESIDUAL_DATA structure contains the first layer of pixels
      in an encoded image. The number of pixels encoded by this structure MUST
      be less than or equal to the number of pixels in the original image. The
      pixels are ordered from left to right and then top to bottom, and are
      stored as a succession of CLEARCODEC_RGB_RUN_SEGMENT
      (section 2.2.4.1.1.1.1) structures.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: run_segments
        type: clearcodec_rgb_run_segment
        repeat: eos
        doc: |
          A variable-length array of CLEARCODEC_RGB_RUN_SEGMENT structures.

  clearcodec_rgb_run_segment:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.1.1 CLEARCODEC_RGB_RUN_SEGMENT
      The CLEARCODEC_RGB_RUN_SEGMENT structure encodes a single RGB run segment.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: blue_value
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the blue value of the current
          pixel.
      - id: green_value
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the green value of the current
          pixel.
      - id: red_value
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the red value of the current
          pixel.
      - id: run_length_factor_1
        type: u1
        doc: |
          An 8-bit unsigned integer. If this value is less than 255 (0xFF), the
          runLengthFactor2 and runLengthFactor3 fields MUST NOT be present, and
          the current pixel MUST be repeated for the next runLengthFactor1
          positions. If the runLengthFactor1 field equals 255 (0xFF), the
          runLengthFactor2 field MUST be present, and the run length is calculated
          from the runLengthFactor2 field. The value of runLengthFactor1 MUST be
          greater than zero.
      - id: run_length_factor_2
        type: u2
        if: run_length_factor_1 == 0xff
        doc: |
          An optional 16-bit unsigned integer. If this value is less than 65,535
          (0xFFFF), the runLengthFactor3 field MUST NOT be present, and the current
          pixel MUST be repeated for the next runLengthFactor2 positions. If the
          runLengthFactor2 field equals 65,535 (0xFFFF), the runLengthFactor3 field
          MUST be present (and nonzero), and the run length is calculated from the
          runLengthFactor3 field. If present, the value of runLengthFactor2 MUST
          be greater than zero.
      - id: run_length_factor_3
        type: u4
        if: (run_length_factor_1 == 0xff) and (run_length_factor_2 == 0xffff)
        doc: |
          An optional 32-bit unsigned integer. If this field is present, it contains
          the run length, and the current pixel MUST be repeated for the next
          runLengthFactor3 positions. This field SHOULD NOT be used if the run length
          is smaller than 65,535 (0xFFFF). If present, the value of runLengthFactor3
          MUST be greater than zero.
    instances:
      run_repeat_count:
        value: 'run_length_factor_1 != 0xff ? run_length_factor_1 : (run_length_factor_2 != 0xffff ? run_length_factor_2 : run_length_factor_3)'
        doc: |
          How many times to repeat the RGB value.

  clearcodec_bands_data:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.2 CLEARCODEC_BANDS_DATA
      The CLEARCODEC_BANDS_DATA structure contains the second layer of pixels in an
      encoded image. This layer MUST be decoded on top of the first layer, in some
      cases overwriting pixels in the first layer. The data consists of a succession
      of CLEARCODEC_BAND (section 2.2.4.1.1.2.1) structures.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: bands
        type: clearcodec_band
        repeat: eos
        doc: |
          A variable-length array of CLEARCODEC_BAND structures.

  clearcodec_band:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.2.1 CLEARCODEC_BAND
      The CLEARCODEC_BAND structure specifies a horizontal band that is composed of
      columns of pixels. Each of these columns is referred to as a "V-Bar".
      The maximum height of a band is 52 pixels.
      mstscax!ClearDecompressor::DecodeTextBand
    seq:
      - id: x_start
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the horizontal position (relative
          to the left edge of the bitmap) where the band starts.
      - id: x_end
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the horizontal position (relative
          to the left edge of the bitmap) where the band ends. This is an inclusive
          coordinate.
      - id: y_start
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the vertical position (relative to
          the top edge of the bitmap) where the band starts.
      - id: y_end
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the vertical position (relative to
          the top edge of the bitmap) where the band ends. This is an inclusive
          coordinate.
      - id: blue_bkg
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the blue value of the background for
          this band.
      - id: green_bkg
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the green value of the background for
          this band.
      - id: red_bkg
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the red value of the background for
          this band.
      - id: v_bars
        type: clearcodec_v_bar
        repeat: expr
        repeat-expr: 'x_end >= x_start ? x_end - x_start + 1 : 0'
        doc: |
          A variable-length array of CLEARCODEC_VBAR (section 2.2.4.1.1.2.1.1) structures.
          The total count of CLEARCODEC_VBAR structures MUST be equal to (xEnd - xStart + 1),
          one per x-coordinate in the band. The V-Bars are encoded from left to right, with
          the first V-Bar corresponding to the xStart field and the last corresponding to
          the xEnd field.

  clearcodec_v_bar:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.2.1.1 CLEARCODEC_VBAR
      The CLEARCODEC_VBAR structure is used to encode a single column of pixels (referred to
      as a "V-Bar") and is encapsulated inside a CLEARCODEC_BAND (section 2.2.4.1.1.2.1)
      structure. The xStart, xEnd, yStart and yEnd fields of the CLEARCODEC_BAND structure
      specify the area within which the V-Bar is contained.
      mstscax!ClearDecompressor::DecodeTextBand
    seq:
      - id: header_u2
        type: u2
      - id: header_u1
        type: u1
        if: short_v_bar_cache_hit
      - id: short_v_bar_pixels
        size: short_v_bar_pixels_size
        if: short_v_bar_cache_miss
        doc: |
          An optional variable-length array of bytes that MUST be present only if the vBarHeader
          field contains a SHORT_VBAR_CACHE_MISS structure. If this field is present, the number
          of bytes in the field MUST be equal to 3 * (shortVBarYOff - shortVBarYOn): one RGB
          triplet per pixel where shortVBarYOff and shortVBarYOn are specified in the
          SHORT_VBAR_CACHE_MISS structure. This field contains raw pixels in top-to-bottom order.
          The pixels are encoded in little-endian byte order (blue in the first byte, green in
          the second byte, and red in the third byte).
          Each pixel in the V-Bar MUST be placed at position (xPos, yPos) in the image (relative
          to the top-left corner), where xPos and yPos are calculated as follows:
              xPos = xStart + position of the V-Bar in the vBars field of the CLEARCODEC_BAND structure
              yPos = yStart + position of the pixel in the V-Bar Storage ADM element
    instances:
      v_bar_cache_hit:
        value: (header_u2 & 0x8000) == 0x8000
        doc: |
          CLEARCODEC_VBAR has a VBAR_CACHE_HIT (section 2.2.4.1.1.2.1.1.1) structure.
          The VBAR_CACHE_HIT structure is used to specify a V-Bar cache hit.
          The use of this structure implies that the necessary V-Bar data is already present in
          the V-Bar Storage (section 3.3.1.10) ADM element at the index specified by the vBarIndex
          field. In this case, the shortVBarPixels field of the encapsulating CLEARCODEC_VBAR
          (section 2.2.4.1.1.2.1.1) structure MUST NOT be present, and the size of the data in
          the V-Bar Storage ADM element MUST be equal to 3 * (yEnd - yStart + 1) bytes, where
          yEnd and yStart are specified in the encapsulating CLEARCODEC_BAND
          (section 2.2.4.1.1.2.1) structure.
          mstscax!ClearDecompressor::DecodeTextBand
      short_v_bar_cache_hit:
        value: (header_u2 & 0xc000) == 0x4000
        doc: |
          CLEARCODEC_VBAR has a SHORT_VBAR_CACHE_HIT (section 2.2.4.1.1.2.1.1.2) structure.
          The SHORT_VBAR_CACHE_HIT structure is used to specify a Short V-Bar cache hit.
          The use of this structure implies that the necessary Short V-Bar data is already
          present in the Short V-Bar Storage (section 3.3.1.12) ADM element at the index
          specified by the shortVBarIndex field.
          In this case, the shortVBarPixels field of the encapsulating CLEARCODEC_VBAR
          (section 2.2.4.1.1.2.1.1) structure MUST NOT be present, and the size of the data in
          the Short V-Bar Storage ADM element MUST NOT exceed 3 * (yEnd - yStart + 1 - shortVBarYOn)
          bytes, where yEnd and yStart are specified in the encapsulating CLEARCODEC_BAND
          (section 2.2.4.1.1.2.1) structure.
          As part of processing this header, each pixel position in the V-Bar Storage ADM element at
          the V-Bar Storage Cursor (section 3.3.1.11) ADM element MUST be updated using the data in
          the Short V-Bar Storage ADM element. The number of pixels placed into the V-Bar Storage ADM
          element MUST equal yEnd – yStart + 1. For each position y within the V-Bar, the pixels MUST
          be updated as follows:
            - If y < shortVBarYOn, then use the blueBkg, greenBKg, and redBkg values specified in the
              encapsulating CLEARCODEC_BAND structure
            - If y >= shortVBarYOn and y < shortVBarYOn + Short V-Bar pixel count, then use the color
              found in the Short V-Bar Storage ADM element at pixel position y – shortVBarYOn
            - If y >= shortVBarYOn + Short V-Bar pixel count, then use the blueBkg, greenBKg, and
              redBkg values specified in the encapsulating CLEARCODEC_BAND structure
          The V-Bar Storage Cursor (section 3.3.1.11) ADM element MUST be incremented by 1 and MUST
          wrap to zero when incremented from 32767.
          mstscax!ClearDecompressor::DecodeTextBand
      short_v_bar_cache_miss:
        value: (header_u2 & 0xc000) == 0x0000
        doc: |
          CLEARCODEC_VBAR has a SHORT_VBAR_CACHE_MISS (section 2.2.4.1.1.2.1.1.3) structure.
          The SHORT_VBAR_CACHE_MISS structure is used to specify a Short V-Bar cache miss.
          As part of processing this header, each pixel position in the Short V-Bar Storage (section
          3.3.1.12) ADM element at the Short V-Bar Storage Cursor (section 3.3.1.13) ADM element MUST
          be updated using the data in the shortVBarPixels field of the encapsulating CLEARCODEC_VBAR
          (section 2.2.4.1.1.2.1.1) structure. The number of pixels placed into the Short V-Bar Storage
          ADM element MUST equal shortVBarYOff - shortVBarYOn (shortVBarYOff MUST be larger than or
          equal to shortVBarYOn).
          The Short V-Bar Storage Cursor ADM element MUST be incremented by 1.
          In addition to updating the Short V-Bar Storage ADM element, each pixel position in the V-Bar
          Storage (section 3.3.1.10) ADM element and the V-Bar Storage Cursor (section 3.3.1.11) ADM
          element MUST be updated using the data in the Short V-Bar Storage ADM element. The number of
          pixels placed into the V-Bar Storage ADM element MUST equal yEnd – yStart + 1. For each position
          y within the V-Bar, the pixels MUST be updated as follows:
            - If y < shortVBarYOn, then use the blueBkg, greenBKg, and redBkg values specified in the
              encapsulating CLEARCODEC_BAND structure
            - If y >= shortVBarYOn and y < shortVBarYOn + Short V-Bar pixel count, then use the color
              found in the Short V-Bar Storage ADM element at pixel position y – shortVBarYOn
            - If y >= shortVBarYOn + Short V-Bar pixel count, then use the blueBkg, greenBKg, and redBkg
              values specified in the encapsulating CLEARCODEC_BAND structure
          The V-Bar Storage Cursor (section 3.3.1.11) ADM element MUST be incremented by 1 and MUST
          wrap to zero when incremented from 32767.
          mstscax!ClearDecompressor::DecodeTextBand
      v_bar_index:
        value: 'v_bar_cache_hit ? header_u2 & 0x7fff : 0'
        doc: |
          A 15-bit unsigned integer that specifies the position in the V-Bar Storage ADM
          element for the current V-Bar.
      short_v_bar_index:
        value: 'short_v_bar_cache_hit ? header_u2 & 0x3fff : 0'
        doc: |
          A 14-bit unsigned integer that specifies the position in the Short V-Bar Storage
          ADM element for the current Short V-Bar.
      short_v_bar_y_on:
        value: 'short_v_bar_cache_hit ? header_u1 : (short_v_bar_cache_miss ? header_u2 & 0xff : 0)'
        doc: |
          An 8-bit unsigned integer that specifies where the Short V-Bar begins, expressed
          as an offset from the top of the V-Bar.
      short_v_bar_y_off:
        value: 'short_v_bar_cache_miss ? (header_u2 >> 8) & 0x3f : 0'
        doc: |
          A 6-bit unsigned integer that specifies where the Short V-Bar ends, expressed as an
          offset from the top of the V-Bar.
      short_v_bar_pixels_size:
        value: 'short_v_bar_cache_miss and (short_v_bar_y_off >= short_v_bar_y_on) ? 3 * (short_v_bar_y_off - short_v_bar_y_on) : 0'

  clearcodec_subcodecs_data:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.3 CLEARCODEC_SUBCODECS_DATA
      The CLEARCODEC_SUBCODECS_DATA structure contains the third layer of pixels in an encoded
      image. This layer MUST be decoded on top of the second layer, in some cases overwriting
      pixels in the first and second layers. The data consists of a succession of
      CLEARCODEC_SUBCODEC (section 2.2.4.1.1.3.1) structures.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: subcodecs
        type: clearcodec_subcodec
        repeat: eos
        doc: |
          A variable-length array of CLEARCODEC_SUBCODEC structures.

  clearcodec_subcodec:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.3.1 CLEARCODEC_SUBCODEC
      The CLEARCODEC_SUBCODEC structure encapsulates an uncompressed bitmap or a bitmap encoded
      with the NSCodec Codec ([MS-RDPNSC] sections 1 through 3) or the RLEX scheme as specified
      in section 2.2.4.1.1.3.1.1.
      mstscax!ClearDecompressor::Decompress
    seq:
      - id: x_start
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the horizontal position (relative to the left
          edge of the bitmap) where the subcodec-encoded bitmap MUST be placed once it has been
          decoded.
      - id: y_start
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the vertical position (relative to the top
          edge of the bitmap) where the subcodec-encoded bitmap MUST be placed once it has been
          decoded.
      - id: width
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the width of the subcodec-encoded bitmap.
      - id: height
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the height of the subcodec-encoded bitmap.
      - id: bitmap_data_byte_count
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of bytes in the bitmapData field.
          This field MUST be used to determine whether the bitmap in the bitmapData field is in
          compressed or uncompressed format. The value in the bitmapDataByteCount field MUST NOT
          exceed (3 * width * height).
      - id: sub_codec_id
        type: u1
        enum: sub_codecid
      - id: bitmap_data
        size: bitmap_data_byte_count
        doc: |
          A variable-length array of bytes that contains bitmap data.
          If the subCodecId field equals 0x00, the bitmapData field contains the raw pixels of
          the bitmap in little-endian byte order (blue in the first byte, green in the second byte,
          and red in the third byte). The pixels are ordered from left to right and then top to
          bottom.
          If the subCodecId field equals 0x01, the bitmapData field contains a bitmap encoded with
          the NSCodec Codec ([MS-RDPNSC] section 1, 2 and 3).
          If the subCodecId field equals 0x02, the bitmapData field contains a CLEARCODEC_SUBCODEC_RLEX
          (section 2.2.4.1.1.3.1.1) structure.
        type:
          switch-on: sub_codec_id
          -name: type
          cases:
            'sub_codecid::raw': clearcodec_subcodec_raw
            'sub_codecid::nscodec': nscodec_bitmap_stream
            'sub_codecid::rlex': clearcodec_subcodec_rlex
    enums:
      sub_codecid:
        0x00:
          id: raw
          doc: Raw bitmap pixels.
        0x01:
          id: nscodec
          doc: NSCodec Codec ([MS-RDPNSC] section 1, 2 and 3).
        0x02:
          id: rlex
          doc: CLEARCODEC_SUBCODEC_RLEX (section 2.2.4.1.1.3.1.1).

  clearcodec_subcodec_raw:
    doc: |
      Raw bitmap pixels.
    seq:
      - id: data
        size-eos: true
        doc: |
          Raw bitmap pixels.

  clearcodec_subcodec_rlex:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.3.1.1 CLEARCODEC_SUBCODEC_RLEX
      The CLEARCODEC_SUBCODEC_RLEX structure contains a palette and segments that contain encoded
      indexes that reference colors in the palette.
      mstscax!DecompressRLEX
    seq:
      - id: palette_count
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the number of RLEX_RGB_TRIPLET
          (section 2.2.4.1.1.3.1.1.1) structures in the paletteEntries field. This value MUST be
          less than or equal to 0x7F. The number of bits in the stopIndex field of each
          CLEARCODEC_SUBCODEC_RLEX_SEGMENT (section 2.2.4.1.1.3.1.1.2) structure embedded in the
          segments field is given by floor(log2(paletteCount – 1)) + 1.
      - id: palette_entries
        type: rlex_rgb_triplet
        repeat: expr
        repeat-expr: palette_count
        doc: |
          A variable-length array of RLEX_RGB_TRIPLET structures. The number of elements in this
          array is specified by the paletteCount field.
      - id: segments
        type: clearcodec_subcodec_rlex_segment(palette_count)
        repeat: eos
        doc: |
          A variable-length array of CLEARCODEC_SUBCODEC_RLEX_SEGMENT structures.

  rlex_rgb_triplet:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.3.1.1.1 RLEX_RGB_TRIPLET
      The RLEX_RGB_TRIPLET structure is used to express the red, green, and blue components
      necessary to reproduce a color in the additive RGB space.
      mstscax!DecompressRLEX
    seq:
      - id: blue
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the blue RGB color component.
      - id: green
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the green RGB color component.
      - id: red
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the red RGB color component.

  clearcodec_subcodec_rlex_segment:
    doc: |
      MS-RDPEGFX 2.2.4.1.1.3.1.1.2 CLEARCODEC_SUBCODEC_RLEX_SEGMENT
      The CLEARCODEC_SUBCODEC_RLEX_SEGMENT structure contains a collection of encoded palette
      indexes. This encoding exploits the fact that a collection of palette indexes can consist
      of the following:
        - Repeated values
        - Sequences of values that monotonically increase by 1
      A palette index that repeats N times is called a "run of length N" (for example, 0x03, 0x03
      is a run of length 2), while a sequence of palette indexes that monotonically increase by 1
      is called a "suite" (0x04, 0x05, 0x06 is a suite with a stopping value of 0x06 and a depth
      of 3). In the specification for the CLEARCODEC_SUBCODEC_RLEX_SEGMENT structure, the run length
      factor fields (runLengthFactor1, runLengthFactor2, and runLengthFactor3) represent the number
      of times a starting color (defined by the stopIndex and suiteDepth fields) repeats before a
      suite (also defined by the stopIndex and suiteDepth fields) begins.
      mstscax!DecompressRLEX
    params:
      - id: palette_count
        type: u1
        doc: |
          The number of palette entries in the parent CLEARCODEC_SUBCODEC_RLEX.
          Used to calculate the number of bits in stopIndex and suiteDepth.
    seq:
      - id: stop_index_suite_depth
        type: u1
        doc: |
          Contains both the stopIndex and suiteDepth in a single byte. stopIndex and suiteDepth
          both can be a variable number of bits - but must combine to be a total of 8 bits.
      - id: run_length_factor_1
        type: u1
        doc: |
          An 8-bit unsigned integer. If the value of the runLengthFactor1 field is less than 255
          (0xFF), the runLengthFactor2 and runLengthFactor3 fields MUST NOT be present and the
          startColor value MUST be applied to the next runLengthFactor1 pixels. If the value of
          the runLengthFactor1 field equals 255 (0xFF), the runLengthFactor2 field MUST be present,
          and the run length is calculated from the runLengthFactor2 field.
      - id: run_length_factor_2
        type: u2
        if: run_length_factor_1 == 0xff
        doc: |
          An optional 16-bit unsigned integer. If the value of the runLengthFactor2 field is less
          than 65,535 (0xFFFF), the runLengthFactor3 field MUST NOT be present, and the startColor
          value MUST be applied to the next runLengthFactor2 pixels. If the value of the
          runLengthFactor2 field equals 65,535 (0xFFFF), the runLengthFactor3 field MUST be present,
          and the run length is calculated from the runLengthFactor3 field.
      - id: run_length_factor_3
        type: u4
        if: (run_length_factor_1 == 0xff) and (run_length_factor_2 == 0xffff)
        doc: |
          An optional 32-bit unsigned integer. If this field is present, it contains the run length.
          The startColor value MUST be applied to the next runLengthFactor3 pixels. This field SHOULD
          NOT be used if the run length is smaller than 65,535 (0xFFFF).
    instances:
      v0:
        value: palette_count
        doc: |
          1) Input value for 'floor(log2(paletteCount – 1)) + 1' calculation
      v1:
        value: ((((v0 - 1) >> 1) | (v0 - 1)) >> 2) | ((v0 - 1) >> 1) | (v0 - 1)
        doc: |
          2) 'floor(log2(paletteCount – 1)) + 1' calculation
      v2:
        value: (((v1 >> 4) | v1) >> 8) | (v1 >> 4) | v1
        doc: |
          3) 'floor(log2(paletteCount – 1)) + 1' calculation
      v2_shift_fix:
        value: 16
        doc: |
          Because KPB produces "data loss" warnings for "u2 >> 16", this is a workaround
      v3:
        value: ((v2 | (v2 >> v2_shift_fix)) & 0x55555555) + (((v2 | (v2 >> v2_shift_fix)) >> 1) & 0x55555555)
        doc: |
          4) 'floor(log2(paletteCount – 1)) + 1' calculation
      v4:
        value: ((v3 & 0x33333333) + ((v3 >> 2) & 0x33333333) + (((v3 & 0x33333333) + ((v3 >> 2) & 0x33333333)) >> 4)) & 0xF0F0F0F
        doc: |
          5) 'floor(log2(paletteCount – 1)) + 1' calculation
      v5:
        value: (((v4 >> 8) & 0xff) + v4 + (((v4 >> 8) + v4) >> 16)) & 0x3F
        doc: |
          6) Output value for 'floor(log2(paletteCount – 1)) + 1' calculation
      stop_index_bit_count:
        value: v5
      stop_index_mask:
        value: (1 << stop_index_bit_count) - 1
      stop_index:
        value: stop_index_suite_depth & stop_index_mask
        doc: |
          A variable number of bits (maximum 7 bits) that defines an unsigned integer.
          The number of bits is determined by the paletteCount field of the encapsulating
          CLEARCODEC_SUBCODEC_RLEX (section 2.2.4.1.1.3.1.1) structure and the sum of
          the number of bits in this field and the suiteDepth field MUST equal 8 (the
          bits in the stopIndex field are present in the least significant bits of the
          containing byte). The stopIndex field specifies the position of an RLEX_RGB_TRIPLET
          (section 2.2.4.1.1.3.1.1.1) structure in the paletteEntries field of the
          encapsulating CLEARCODEC_SUBCODEC_RLEX structure. This RLEX_RGB_TRIPLET structure
          is referred to as stopColor.
      suite_depth:
        value: stop_index_suite_depth >> stop_index_bit_count
        doc: |
          A variable number of bits (maximum 8 bits) that defines an unsigned integer.
          The sum of the number of bits in this field and the stopIndex field MUST equal
          8, and the bits in the suiteDepth field are present in the most significant bits
          of the containing byte. The suiteDepth field specifies the number of consecutive
          indexes encoded in the current suite. Each index represents one pixel preceding
          the stopIndex and starting from stopIndex – suiteDepth (referred to as startIndex).
          The startIndex value specifies the position of an RLEX_RGB_TRIPLET structure
          (referred to as startColor) in the paletteEntries field of the encapsulating
          CLEARCODEC_SUBCODEC_RLEX structure.
      run_repeat_count:
        value: 'run_length_factor_1 != 0xff ? run_length_factor_1 : (run_length_factor_2 != 0xffff ? run_length_factor_2 : run_length_factor_3)'
        doc: |
          How many times to repeat the RGB value.
