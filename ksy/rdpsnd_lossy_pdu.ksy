meta:
  id: rdpsnd_lossy_pdu
  endian: le
  imports:
    - rdpsnd_pdu

doc: |
  MS-RDPEA 2.2.1 RDPSND PDU Header (SNDPROLOG)
  The RDPSND PDU header is present in many audio PDUs. It is used to identify
  the PDU type, specify the length of the PDU, and convey message flags.
  Dynamic channel "AUDIO_PLAYBACK_LOSSY_DVC".
  mstscax!CRdpAudioPlaybackListenerCallback::OnNewChannelConnection
  mstscax!CRdpAudioPlaybackChannelCallback::OnDataReceived
  mstscax!CRdpAudioController::DataArrived

seq:
  - id: data
    type: rdpsnd_pdu
    doc: |
      Contains the exact same data as a RDPSND_PDU.
