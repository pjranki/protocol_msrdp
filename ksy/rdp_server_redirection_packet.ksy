meta:
  id: rdp_server_redirection_packet
  endian: le

doc: |
  MS-RDPBCGR 2.2.13.1 Server Redirection Packet (RDP_SERVER_REDIRECTION_PACKET)
  The RDP_SERVER_REDIRECTION_PACKET structure contains information to enable a client to reconnect
  to a session on a specified server. This data is sent to a client in a Redirection PDU to enable
  load-balancing of Remote Desktop sessions across a collection of machines. For more information
  about the load balancing of Remote Desktop sessions, see [MSFT-SDLBTS] "Load-Balanced Configurations"
  and "Revectoring Clients".
  For WDAG, mstscax!CCO::OnServerRedirectionPacket() fails early as EnableServerRedirectionPduProcessing
  is not set.
  mstscax!CSL::OnPacketReceived
  mstscax!CCO::OnServerRedirectionPacket

seq:
  - id: session_id
    type: u4
    doc: |
      A 32-bit unsigned integer. The session identifier to which the client MUST reconnect.
      This identifier MUST be specified in the RedirectedSessionID field of the Client Cluster Data
      (section 2.2.1.3.5) if a reconnect attempt takes place. The Client Cluster Data is transmitted
      as part of the MCS Connect Initial PDU (section 2.2.1.3).
  - id: redir_flags
    type: u4
    doc: |
      A 32-bit unsigned integer. A bit field that contains redirection information flags, some of which
      indicate the presence of additional data at the end of the packet.
  - id: target_net_address_length
    type: u4
    if: (redir_flags & 0x00000001) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the TargetNetAddress field.
  - id: target_net_address
    type: strz
    encoding: utf-16le
    size: target_net_address_length
    if: (redir_flags & 0x00000001) != 0
    doc: |
      A variable-length array of bytes containing the IP address of the server
      (for example, "192.168.0.1" using dotted decimal notation) in Unicode format,
      including a null-terminator.
  - id: load_balance_info_length
    type: u4
    if: (redir_flags & 0x00000002) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the LoadBalanceInfo field.
  - id: load_balance_info
    size: load_balance_info_length
    if: (redir_flags & 0x00000002) != 0
    doc: |
      A variable-length array of bytes containing load balancing information that MUST be treated
      as opaque data by the client and passed to the server if the LB_TARGET_NET_ADDRESS (0x00000001)
      flag is not present in the RedirFlags field and a reconnection takes place.
      See section 3.2.5.3.1 for details on populating the routingToken field of the
      X.224 Connection Request PDU (section 2.2.1.1).
  - id: user_name_length
    type: u4
    if: (redir_flags & 0x00000004) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the UserName field.
  - id: user_name
    type: strz
    encoding: utf-16le
    size: user_name_length
    if: (redir_flags & 0x00000004) != 0
    doc: |
      A variable-length array of bytes containing the username of the user in Unicode format,
      including a null-terminator.
  - id: domain_length
    type: u4
    if: (redir_flags & 0x00000008) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the Domain field.
  - id: domain
    type: strz
    encoding: utf-16le
    size: domain_length
    if: (redir_flags & 0x00000008) != 0
    doc: |
      A variable-length array of bytes containing the domain to which the user connected in
      Unicode format, including a null-terminator.
  - id: password_length
    type: u4
    if: (redir_flags & 0x00000010) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the Password field.
  - id: password
    type: strz
    encoding: utf-16le
    size: password_length
    if: (redir_flags & 0x00000010) != 0
    doc: |
      A variable-length array of bytes containing a password to be used when connecting to the
      redirected server. If the LB_PASSWORD_IS_PK_ENCRYPTED (0x00004000) flag is specified in the
      RedirFlags field, then the password MUST be treated as an opaque encrypted blob and sent to
      the target server using the RDSTLS protocol (section 5.4.5.3). If the LB_PASSWORD_IS_PK_ENCRYPTED
      flag is not set, then the Password field contains a cleartext password (in Unicode format),
      including a null-terminator, that MUST be passed to the target server on successful connection.
  - id: target_fqdn_length
    type: u4
    if: (redir_flags & 0x00000100) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the TargetFQDN field.
  - id: target_fqdn
    type: strz
    encoding: utf-16le
    size: target_fqdn_length
    if: (redir_flags & 0x00000100) != 0
    doc: |
      A variable-length array of bytes containing the fully qualified domain name (FQDN) of the target
      machine, including a null-terminator.
  - id: target_net_bios_name_length
    type: u4
    if: (redir_flags & 0x00000200) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the TargetNetBiosName field.
  - id: target_net_bios_name
    type: strz
    encoding: utf-16le
    size: target_net_bios_name_length
    if: (redir_flags & 0x00000200) != 0
    doc: |
      A variable-length array of bytes containing the NETBIOS name of the target machine,
      including a null-terminator.
  - id: tsv_url_length
    type: u4
    if: (redir_flags & 0x00001000) != 0
    doc: |
      The length, in bytes, of the TsvUrl field.<40>
  - id: tsv_url
    type: strz
    encoding: utf-16le
    size: tsv_url_length
    if: (redir_flags & 0x00001000) != 0
    doc: |
      A variable-length array of bytes.<41> If the client has previously sent a TsvUrl field in the
      LoadBalanceInfo to the server in the expected format, then the server will return the same TsvUrl
      to the client in this field. The client verifies that it is the same as the one that it previously
      passed to the server and if they don't match, the client immediately disconnects the connection.
  - id: redirection_guid_length
    type: u4
    if: (redir_flags & 0x00008000) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the RedirectionGuid field.
  - id: redirection_guid
    type: strz
    encoding: utf-16le
    size: redirection_guid_length
    if: (redir_flags & 0x00008000) != 0
    doc: |
      A variable-length array of bytes containing a Base64-encoded ([RFC4648] section 4) GUID
      ([MS-DTYP] section 2.3.4) in Unicode format that functions as a unique identifier for the redirected
      connection.
  - id: target_certificate_length
    type: u4
    if: (redir_flags & 0x00010000) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the TargetCertificate field.
  - id: target_certificate
    type: strz
    encoding: utf-16le
    size: target_certificate_length
    if: (redir_flags & 0x00010000) != 0
    doc: |
      A variable-length array of bytes containing a Base64-encoded Target Certificate Container
      (section 2.2.13.1.2) structure in Unicode format that encapsulates the X.509 certificate of the
      target server.
  - id: target_net_addresses_length
    type: u4
    if: (redir_flags & 0x00000800) != 0
    doc: |
      A 32-bit unsigned integer. The length, in bytes, of the TargetNetAddresses field.
  - id: target_net_addresses
    type: rdp_target_net_addresses
    size: target_net_addresses_length
    if: (redir_flags & 0x00000800) != 0
    doc: |
      A variable-length array of bytes containing the target IP addresses of the server to connect against,
      stored in a Target Net Addresses (section 2.2.13.1.1) structure.

enums:
  lb:
    0x00000001:
      id: target_net_address
      doc: |
        Indicates that the TargetNetAddressLength and TargetNetAddress fields are present.
    0x00000002:
      id: load_balance_info
      doc: |
        Indicates that the LoadBalanceInfoLength and LoadBalanceInfo fields are present.
    0x00000004:
      id: username
      doc: |
        Indicates that the UserNameLength and UserName fields are present.
    0x00000008:
      id: domain
      doc: |
        Indicates that the DomainLength and Domain fields are present.
    0x00000010:
      id: password
      doc: |
        Indicates that the PasswordLength and Password fields are present.
    0x00000020:
      id: dontstoreusername
      doc: |
        Indicates that when reconnecting, the client MUST send the username specified in the
        UserName field to the server in the Client Info PDU (section 2.2.1.11.1.1).
    0x00000040:
      id: smartcard_logon
      doc: |
        Indicates that the user can use a smart card for authentication.
    0x00000080:
      id: noredirect
      doc: |
        Indicates that the contents of the PDU are for informational purposes only.
        No actual redirection is required.
    0x00000100:
      id: target_fqdn
      doc: |
        Indicates that the TargetFQDNLength and TargetFQDN fields are present.
    0x00000200:
      id: target_netbios_name
      doc: |
        Indicates that the TargetNetBiosNameLength and TargetNetBiosName fields are present.
    0x00000800:
      id: target_net_addresses
      doc: |
        Indicates that the TargetNetAddressesLength and TargetNetAddresses fields are present.
    0x00001000:
      id: client_tsv_url
      doc: |
        Indicates that the TsvUrlLength and TsvUrl fields are present.<38>
    0x00002000:
      id: server_tsv_capable
      doc: |
        Indicates that the server supports redirection based on the TsvUrl present in the
        LoadBalanceInfo sent by the client.<39>
    0x00004000:
      id: password_is_pk_encrypted
      doc: |
        Indicates that the data in the Password field is encrypted and contains data that
        SHOULD be used in the RDSTLS Authentication Request PDU with Password Credentials
        (section 2.2.17.2).
    0x00008000:
      id: redirection_guid
      doc: |
        Indicates that the RedirectionGuidLength and RedirectionGuid fields are present.
    0x00010000:
      id: target_certificate
      doc: |
        Indicates that the TargetCertificateLength and TargetCertificate fields are present.

types:
  rdp_target_net_addresses:
    doc: |
      MS-RDPBCGR 2.2.13.1.1 Target Net Addresses (TARGET_NET_ADDRESSES)
      The TARGET_NET_ADDRESSES structure is used to hold a collection of IP addresses in Unicode format.
      mstscax!CCO::OnServerRedirectionPacket
    seq:
      - id: address_count
        type: u4
        doc: |
          The TARGET_NET_ADDRESSES structure is used to hold a collection of IP addresses in Unicode format.
      - id: addresses
        type: rdp_target_net_address
        repeat: expr
        repeat-expr: address_count
        doc: |
          An array of Target Net Address (section 2.2.13.1.1.1) structures, each containing an IP address.

  rdp_target_net_address:
    doc: |
      MS-RDPBCGR 2.2.13.1.1.1 Target Net Address (TARGET_NET_ADDRESS)
      The TARGET_NET_ADDRESS structure holds a Unicode text representation of an IP address.
      mstscax!CCO::OnServerRedirectionPacket
    seq:
      - id: address_length
        type: u4
        doc: |
          A 32-bit, unsigned integer. The length in bytes of the address field.
      - id: address
        type: strz
        encoding: utf-16le
        size: address_length
        doc: |
          A variable-length array of bytes containing an IP address in Unicode format,
          including a null-terminator.
