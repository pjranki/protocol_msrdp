meta:
  id: ts_update_sync
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.3 Synchronize Update (TS_UPDATE_SYNC)
  The TS_UPDATE_SYNC structure is an artifact of the T.128 protocol
  ([T128] section 8.6.2) and SHOULD be ignored.
  mstscax!CTSCoreGraphics::ProcessSync
  mstscax!CTSGraphics::EndOfUpdateBatch

seq:
  - id: update_type
    type: u2
    doc: |
      16-bit, unsigned integer. The update type. This field MUST be set to
      UPDATETYPE_SYNCHRONIZE (0x0003).
  - id: padding
    type: u2
    doc: |
      A 16-bit, unsigned integer. Padding. Values in this field MUST be
      ignored.
