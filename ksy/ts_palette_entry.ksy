meta:
  id: ts_palette_entry
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.1.2 RGB Palette Entry (TS_PALETTE_ENTRY)
  The TS_PALETTE_ENTRY structure is used to express the red, green,
  and blue components necessary to reproduce a color in the additive
  RGB space.
  mstscax!CUH::ProcessPalette

seq:
  - id: red
    type: u1
    doc: |
      An 8-bit, unsigned integer. The red RGB color component.
  - id: green
    type: u1
    doc: |
      An 8-bit, unsigned integer. The green RGB color component.
  - id: blue
    type: u1
    doc: |
      An 8-bit, unsigned integer. The blue RGB color component.
