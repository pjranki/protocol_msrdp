meta:
  id: t125_mcs_pdu
  endian: be
  imports:
    - ts_security_pdu
    - slowpath_pdu
    - channel_pdu

doc: |
  T125 11 Meaning of MCSPDUs
  mstscax!CMCS::SendBuffer
  mstscax!CMCS::OnDataAvailable

params:
  - id: channel_type
    type: u1
    doc: |
      This is the type (protocol) of the channel.
      It is set in the msrdp_static_channel_type class.
      This allows for an external implementation to lookup the channel ID
      and set the type - so that the contents of that channel will be
      parsed correctly. It is set to 0 (raw) by default.

seq:
  - id: msg
    type: b6
    enum: t125_mcs_msg
    doc: The type of this message.
  - id: flags
    type: b2
    doc: Flags specific to message type.
  - id: data
    size-eos: true
    process: msrdp_static_channel_type(_root)
    type:
      switch-on: msg
      -name: type
      cases:
        't125_mcs_msg::disconnect_provider_ultimatum': t125_mcs_disconnect_provider_ultimatum(flags)
        't125_mcs_msg::attach_user_confirm': t125_mcs_attach_user_confirm(flags)
        't125_mcs_msg::detach_user_indication': t125_mcs_detach_user_indication(flags)
        't125_mcs_msg::channel_join_confirm': t125_mcs_channel_join_confirm(flags)
        't125_mcs_msg::send_data_request': t125_mcs_send_data_request(channel_type)
        't125_mcs_msg::send_data_indication': t125_mcs_send_data_indication(channel_type)

instances:
  to_server:
    value: '(msg == t125_mcs_msg::send_data_request ? true : (msg == t125_mcs_msg::send_data_indication ? false : true))'
    doc: |
      Message is to server node if its a send_data_request.
      Message is to client node if its a send_data_indication.
      Message is assumed to be going to server if any other message type.

enums:
  t125_mcs_msg:
    101: connect_initial
    102: connect_response
    103: connect_additional
    104: connect_result
    0: plumb_domain_indication
    1: erect_domain_request
    2: merge_channels_request
    3: merge_channels_confirm
    4: purge_channels_indication
    5: merge_tokens_tequest
    6: merge_tokens_confirm
    7: purge_tokens_indication
    8: disconnect_provider_ultimatum
    9: reject_mcs_pdu_ultimatum
    10: attach_user_request
    11: attach_user_confirm
    12: detach_user_request
    13: detach_user_indication
    14: channel_join_request
    15: channel_join_confirm
    16: channel_leave_request
    17: channel_convene_request
    18: channel_convene_confirm
    19: channel_disband_request
    20: channel_disband_indication
    21: channel_admit_request
    22: channel_admit_indication
    23: channel_expel_request
    24: channel_expel_indication
    25: send_data_request
    26: send_data_indication
    27: uniform_send_data_request
    28: uniform_send_data_indication
    29: token_grab_request
    30: token_grab_confirm
    31: token_inhibit_request
    32: token_inhibit_confirm
    33: token_give_request
    34: token_give_indication
    35: token_give_response
    36: token_give_confirm
    37: token_please_request
    38: token_please_indication
    39: token_release_request
    40: token_release_confirm
    41: token_test_request
    42: token_test_confirm
  data_priority:
    0: top
    1: high
    2: medium
    3: low
  rn:
    0: domain_disconnected
    1: provider_initiated
    2: token_purged
    3: user_requested
    4: channel_purged
  rt:
    0: successful
    1: domain_merging
    2: domain_not_hierarchical
    3: no_such_channel
    4: no_such_domain
    5: no_such_user
    6: not_admitted
    7: other_user_id
    8: parameters_unacceptable
    9: token_not_available
    10: token_not_possessed
    11: too_many_channels
    12: too_many_tokens
    13: too_many_users
    14: unspecified_failure
    15: user_rejected

types:
  t125_mcs_disconnect_provider_ultimatum:
    doc: |
      T125 11.15 DisconnectProviderUltimatum
      mstscax!CMCS::MCSHandleControlPkt
    params:
      - id: flags
        type: u1
        doc: |
          Contains the most significant 2 bits of the reason
    seq:
      - id: reason_byte
        type: u1
        doc: |
          Most significant bit contains the least significant bit of the reason.
    instances:
      reason:
        value: ((flags << 1) & 0x06) | ((reason_byte >> 7) & 0x01)
        enum: rn

  t125_mcs_attach_user_confirm:
    doc: |
      T125 11.18 AttachUserConfirm
      mstscax!CMCS::MCSHandleControlPkt
      mstscax!CNC::NC_OnMCSAttachUserConfirm
    params:
      - id: flags
        type: u1
        doc: |
          Contains the data present flag (0x2) and the the most significant bit of the result.
    seq:
      - id: result_byte
        type: u1
        if: data_present
        doc: |
          Most significant bits contain the 3 least significant bits of the result.
      - id: user_id_offset
        type: u2
        if: data_present
    instances:
      data_present:
        value: (flags & 0x02) != 0
      result:
        value: 'data_present ? ((flags << 3) & 0x08) | ((result_byte >> 5) & 0x07) : rt::unspecified_failure.to_i'
        enum: rt
      user_id:
        value: user_id_offset + 1001

  t125_mcs_detach_user_indication:
    doc: |
      T125 11.20 DetachUserIndication
      NOTE: Docs suggest there is data, mstscax!CMCS::MCSHandleControlPkt does not process data.
            Assuming for now that there is no data in this message.
      mstscax!CMCS::MCSHandleControlPkt
    params:
      - id: flags
        type: u1

  t125_mcs_channel_join_confirm:
    doc: |
      T125 11.22 ChannelJoinConfirm
      mstscax!CMCS::MCSHandleControlPkt
      mstscax!CNC::NC_OnMCSChannelJoinConfirm
      mstscax!CChan::ChannelOnConnected
    params:
      - id: flags
        type: u1
        doc: |
          Contains the data present flag (0x2) and the the most significant bit of the result.
    seq:
      - id: result_byte
        type: u1
        if: data_present
        doc: |
          Most significant bits contain the 3 least significant bits of the result.
      - id: user_id_offset
        type: u2
        if: data_present
        doc: |
          Unused in mstscax implementation
      - id: requested_channel_id
        type: u2
        if: data_present
        doc: |
          Unused in mstscax implementation
      - id: channel_id
        type: u2
        if: data_present
        doc: |
          Channel ID
    instances:
      data_present:
        value: (flags & 0x02) != 0
      result:
        value: 'data_present ? ((flags << 3) & 0x08) | ((result_byte >> 5) & 0x07) : rt::unspecified_failure.to_i'
        enum: rt
      user_id:
        value: user_id_offset + 1001

  t125_mcs_send_data_request:
    doc: |
      T125 11.32 SendDataRequest
      mstscax!CMCS::MCSHandleCRPDU
    params:
      - id: channel_type
        type: u1
    seq:
      - id: initiator
        type: u2
      - id: channel_id
        type: u2
      - id: data_priority
        type: b2
        enum: data_priority
      - id: segmentation_begin
        type: b1
      - id: segmentation_end
        type: b1
      - id: padding
        type: b4
      - id: length_byte_1
        type: u1
      - id: length_byte_2
        type: u1
        if: is_two_byte_length
      - id: user_data
        size: length
        type:
          switch-on: '(channel_type > 0) and (channel_type <= 2) ? channel_type : 0'
          -name: type
          cases:
            0: channel_pdu(true, channel_type)
            1: ts_security_pdu
            2: slowpath_pdu
    instances:
      is_single_byte_length:
        value: (length_byte_1 & 0x80) == 0
      is_two_byte_length:
        value: not is_single_byte_length and ((length_byte_1 & 0x40) == 0)
      is_chained_data_blocks:
        value: not is_single_byte_length and not is_two_byte_length
      length_byte_1_u4:
        value: (length_byte_1 & 0x7f) << 0
      length:
        value: 'is_two_byte_length ? (((length_byte_1_u4 << 8) & 0x3f00) | length_byte_2) : length_byte_1_u4 & 0x7f'

  t125_mcs_send_data_indication:
    doc: |
      T125 11.33 SendDataIndication
    params:
      - id: channel_type
        type: u1
    seq:
      - id: initiator
        type: u2
      - id: channel_id
        type: u2
      - id: data_priority
        type: b2
        enum: data_priority
      - id: segmentation_begin
        type: b1
      - id: segmentation_end
        type: b1
      - id: padding
        type: b4
      - id: length_byte_1
        type: u1
      - id: length_byte_2
        type: u1
        if: is_two_byte_length
      - id: user_data
        size: length
        type:
          switch-on: '(channel_type > 0) and (channel_type <= 2) ? channel_type : 0'
          -name: type
          cases:
            0: channel_pdu(false, channel_type)
            1: ts_security_pdu
            2: slowpath_pdu
    instances:
      is_single_byte_length:
        value: (length_byte_1 & 0x80) == 0
      is_two_byte_length:
        value: not is_single_byte_length and ((length_byte_1 & 0x40) == 0)
      is_chained_data_blocks:
        value: not is_single_byte_length and not is_two_byte_length
      length_byte_1_u4:
        value: (length_byte_1 & 0x7f) << 0
      length:
        value: 'is_two_byte_length ? (((length_byte_1_u4 << 8) & 0x3f00) | length_byte_2) : length_byte_1_u4 & 0x7f'
