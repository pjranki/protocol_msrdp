meta:
  id: rdp_data_segment
  endian: le
  imports:
    - rdp8_bulk_encoded_data

doc: |
  MS-RDPEGFX 2.2.5.2 RDP_DATA_SEGMENT
  The RDP_DATA_SEGMENT structure contains data that has been encoded using RDP 8.0 Bulk
  Compression techniques (section 3.1.9.1).
  mstscax!RdpGfxClientChannel::OnDataReceived
  mstscax!RdpGfxProtocolClientDecoder::Decode
  mstscax!DecompressUnchopper::Decompress

seq:
  - id: size
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the size, in bytes, of the bulkData field.
  - id: bulk_data
    type: rdp8_bulk_encoded_data
    size: size
    doc: |
      A variable-length RDP8_BULK_ENCODED_DATA structure (section 2.2.5.3).
