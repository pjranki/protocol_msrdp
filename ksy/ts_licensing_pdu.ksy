meta:
  id: ts_licensing_pdu
  endian: le

doc: |
  MS-RDPELE 2.2.2 Licensing PDU (TS_LICENSING_PDU)
  The Licensing PDU packet encapsulates licensing messages that are exchanged between a client and
  a terminal server.
  mstscax!CSL::OnPacketReceived
  mstscax!CSL::SLReceivedLicPacket
  mstscax!CSL::SLLicenseData
  mstscax!CLic::CLicenseData
  mstscax!LicenseClientHandleServerMessage

seq:
  - id: msg_type
    type: u1
    enum: license_type
    doc: |
      An 8-bit, unsigned integer. A type of the licensing packet. For more details about the different
      licensing packets, see [MS-RDPELE] section 2.2.2.
  - id: flags
    type: u1
    doc: |
      An 8-bit unsigned integer. License preamble flags.
  - id: msg_size
    type: u2
    doc: |
      An 16-bit, unsigned integer. The size in bytes of the licensing packet
      (including the size of the preamble).
  - id: licensing_message
    size: 'msg_size >= 4 ? msg_size - 4 : 0'
    type:
      switch-on: msg_type
      -name: type
      cases:
        'license_type::license_request': server_license_request
        'license_type::platform_challenge': server_platform_challenge
        'license_type::new_license': server_new_license
        'license_type::upgrade_license': server_upgrade_license
        'license_type::license_info': client_license_info
        'license_type::new_license_request': client_new_license_request
        'license_type::platform_challenge_response': client_platform_challenge_response
        'license_type::error_alert': license_error_message

instances:
  version:
    value: flags & 0xf

enums:
  license_type:
    0x01:
      id: license_request
      doc: Indicates a License Request PDU ([MS-RDPELE] section 2.2.2.1).
    0x02:
      id: platform_challenge
      doc: Indicates a Platform Challenge PDU ([MS-RDPELE] section 2.2.2.4).
    0x03:
      id: new_license
      doc: Indicates a New License PDU ([MS-RDPELE] section 2.2.2.7).
    0x04:
      id: upgrade_license
      doc: Indicates an Upgrade License PDU ([MS-RDPELE] section 2.2.2.6).
    0x12:
      id: license_info
      doc: Indicates a License Information PDU ([MS-RDPELE] section 2.2.2.3).
    0x13:
      id: new_license_request
      doc: Indicates a New License Request PDU ([MS-RDPELE] section 2.2.2.2).
    0x15:
      id: platform_challenge_response
      doc: Indicates a Platform Challenge Response PDU ([MS-RDPELE] section 2.2.2.5).
    0xFF:
      id: error_alert
      doc: Indicates a Licensing Error Message PDU (section 2.2.1.12.1.3).
  license_flag:
    0x0F:
      id: license_protocol_version_mask
      doc: |
        The license protocol version. See the discussion which follows this table for more information.
    0x80:
      id: extend_error_msg_supported
      doc: |
        Indicates that extended error information using the Licensing Error Message (section 2.2.1.12.1.3)
        is supported.
  preamble:
    0x2:
      id: version_2_0
      doc: RDP 4.0
    0x3:
      id: version_3_0
      doc: RDP 5.0, 5.1, 5.2, 6.0, 6.1, 7.0, 7.1, 8.0, 8.1, 10.0, 10.1, 10.2, 10.3, 10.4, and 10.5

types:
  server_license_request:
    doc: |
      MS-RDPELE 2.2.2.1 Server License Request (SERVER_LICENSE_REQUEST)
      TODO
      mstscax!LicenseClientHandleServerMessage
      mstscax!UnpackHydraServerLicenseRequest
      mstscax!LicenseClientHandleServerRequest
    seq:
      - id: todo
        size-eos: true

  server_platform_challenge:
    doc: |
      MS-RDPELE 2.2.2.4 Server Platform Challenge (SERVER_PLATFORM_CHALLENGE)
      TODO
      mstscax!LicenseClientHandleServerMessage
      mstscax!UnPackHydraServerPlatformChallenge
      mstscax!LicenseClientHandleServerPlatformChallenge
    seq:
      - id: todo
        size-eos: true

  server_new_license:
    doc: |
      MS-RDPELE 2.2.2.7 Server New License (SERVER_NEW_LICENSE)
      TODO
      mstscax!LicenseClientHandleServerMessage
      mstscax!UnPackHydraServerNewLicense
      mstscax!LicenseClientHandleNewLicense
    seq:
      - id: todo
        size-eos: true

  server_upgrade_license:
    doc: |
      MS-RDPELE 2.2.2.6 Server Upgrade License (SERVER_UPGRADE_LICENSE)
      TODO
      mstscax!LicenseClientHandleServerMessage
      mstscax!UnPackHydraServerNewLicense
      mstscax!LicenseClientHandleNewLicense
    seq:
      - id: todo
        size-eos: true

  client_license_info:
    doc: |
      MS-RDPELE 2.2.2.3 Client License Information (CLIENT_LICENSE_INFO)
      TODO
    seq:
      - id: todo
        size-eos: true

  client_new_license_request:
    doc: |
      MS-RDPELE 2.2.2.2 Client New License Request (CLIENT_NEW_LICENSE_REQUEST)
      TODO
    seq:
      - id: todo
        size-eos: true

  client_platform_challenge_response:
    doc: |
      MS-RDPELE 2.2.2.5 Client Platform Challenge Response (CLIENT_PLATFORM_CHALLENGE_RESPONSE)
      TODO
    seq:
      - id: todo
        size-eos: true

  license_error_message:
    doc: |
      MS-RDPELE 2.2.2.7.1 License Error Message (LICENSE_ERROR_MESSAGE)
      TODO
      mstscax!LicenseClientHandleServerMessage
      mstscax!UnPackLicenseErrorMessage
    seq:
      - id: todo
        size-eos: true
