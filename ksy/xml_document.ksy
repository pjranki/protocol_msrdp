meta:
  id: xml_document
  endian: le

doc: |
  MS-RDPEXPS 2.2.5 XML Document (XML_DOCUMENT)
  This sub-packet represents an XML document that is being sent inside another packet
  (for more information, see [MSFT-XMLPAPER]). The XML document is a single character string.
  The exact size of the document is specified by the cbXMLSize field; a protocol parser MUST
  use the value in the cbXMLSize field for the string length and MUST NOT use null-termination
  of the XMLDocument string to determine the size.
  mstscax!CProxyReceive_IXMLDOMDocument2
  msxml6!DOMDocumentWrapper::load

seq:
  - id: xml_size
    -orig-id: cbXMLSize
    type: u4
    doc: |
      A 32-bit unsigned integer. This field MUST contain the number of bytes in the following
      XMLDocument field.
  - id: xml_document
    -orig-id: XMLDocument
    size: xml_size
    doc: |
      An array of 8-bit unsigned integers. This field contains the serialized XML document.
      The contents are treated as opaque by this protocol.
