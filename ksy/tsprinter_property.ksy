meta:
  id: tsprinter_property
  endian: le

doc: |
  MS-RDPEXPS 2.2.7 Printer Property (TSPRINTER_PROPERTY)
  This structure is embedded inside other packets. It describes the resulting output when
  certain printer capabilities are queried.
  mstscax!CProxyReceive_TSPRINTER_PROPERTY
  mstscax!CProxyWrite_TSPRINTER_PROPERTY
  mstscax!UnMarshalPrinterPropertiesCollection
  mstscax!MarshalPrinterPropertiesCollection
  mstscax!UpdatePrinterPropertiesCollection

seq:
  - id: property_type
    -orig-id: PropertyType
    type: u4
    enum: property_type
    doc: |
      A 32-bit integer that MUST have one of the following values that indicate the
      content of the cbPropertyValue and pPropertyValue fields.
  - id: property_name_size
    -orig-id: cbPropertyName
    type: u4
    doc: |
      The number of bytes in pPropertyName.
  - id: property_name
    -orig-id: pPropertyName
    type: strz
    encoding: utf-16le
    size: property_name_size
    doc: |
      An array of Unicode (1) characters identifying the name of the property.
      This field MUST NOT be null-terminated. The size of the pPropertyName field is
      specified in bytes by the cbPropertyName field. This field is treated as payload
      in the Printer Driver Interface.
  - id: property_value_size
    -orig-id: cbPropertyValue
    type: u4
    doc: |
      A 32-bit unsigned integer. This field contains the number of bytes in the
      pPropertyValue field.
  - id: property_value
    -orig-id: pPropertyValue
    size: property_value_size
    doc: |
      The value of the property. This field is treated as payload in the Remote Desktop
      Protocol: XPS Print Virtual Channel Extension, as specified in [MS-RDPEXPS].

enums:
  property_type:
    0x00000002:
      id: value32
      doc: pPropertyValue contains a 32-bit integer; cbPropertyValue is equal to 4.
    0x00000003:
      id: value64
      doc: pPropertyValue contains a 64-bit integer; cbPropertyValue is equal to 8.
    0x00000004:
      id: value8
      doc: pPropertyValue contains an 8-bit integer; cbPropertyValue is equal to 1.
    0x0000000A:
      id: buffer
      doc: pPropertyValue contains a variable size buffer; the size of the buffer is contained in the cbPropertyValue field.
