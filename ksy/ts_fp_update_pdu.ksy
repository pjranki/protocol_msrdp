meta:
  id: ts_fp_update_pdu
  endian: le
  imports:
    - fips_information
    - ts_fp_update

doc: |
  MS-RDPBCGR 2.2.9.1.2 Server Fast-Path Update PDU (TS_FP_UPDATE_PDU)
  Fast-path revises server output packets from the first byte with the goal of
  improving bandwidth. The TPKT Header ([T123] section 8), X.224 Class 0 Data
  TPDU ([X224] section 13.7), and MCS Send Data Indication ([T125] section 11.33)
  are replaced; the Security Header (section 2.2.8.1.1.2) is collapsed into the
  fast-path output header; and the Share Data Header (section 2.2.8.1.1.1.2) is
  replaced by a new fast-path format. The contents of the graphics and pointer
  updates (sections 2.2.9.1.1.3 and 2.2.9.1.1.4) are also changed to reduce their
  size, particularly by removing or reducing headers. Support for fast-path
  output is advertised in the General Capability Set (section 2.2.7.1.1).
  mstscax!CSL::OnFastPathOutputReceived
  mstscax!CCO::OnFastPathOutputReceived

params:
  - id: first_byte
    type: u1
    -default: 0x00

seq:
  - id: length1
    type: u1
    doc: |
      An 8-bit, unsigned integer. If the most significant bit of the length1
      field is not set, then the size of the PDU is in the range 1 to 127
      bytes and the length1 field contains the overall PDU length (the length2
      field is not present in this case). However, if the most significant bit
      of the length1 field is set, then the overall PDU length is given by the
      low 7 bits of the length1 field concatenated with the 8 bits of the length2
      field, in big-endian order (the length2 field contains the low-order bits).
      The overall PDU length SHOULD be less than or equal to 16,383 bytes.
  - id: length2
    type: u1
    if: (length1 & 0x80) != 0
    doc: |
      An 8-bit, unsigned integer. If the most significant bit of the length1
      field is not set, then the length2 field is not present. If the most
      significant bit of the length1 field is set, then the overall PDU length
      is given by the low 7 bits of the length1 field concatenated with the 8
      bits of the length2 field, in big-endian order (the length2 field contains
      the low-order bits). The overall PDU length SHOULD be less than or equal
      to 16,383 bytes.
  - id: fips_information
    type: fips_information
    if: false
    doc: |
      Optional FIPS header information, present when the Encryption Method
      selected by the server (sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_METHOD_FIPS
      (0x00000010). The Fast-Path FIPS Information structure is specified in section
      2.2.8.1.2.1.
  - id: data_signature
    size: 8
    if: (flags & fastpath_output::encrypted.to_i) != 0
    doc: |
      MAC generated over the packet using one of the techniques specified in section
      5.3.6 (the FASTPATH_OUTPUT_SECURE_CHECKSUM flag, which is set in the
      fpOutputHeader field, describes the method used to generate the signature).
      This field MUST be present if the FASTPATH_OUTPUT_ENCRYPTED flag is set in the
      fpOutputHeader field.
  - id: update_array
    type: ts_fp_update_array
    size: 'length >= header_length ? length - header_length : 0'
    doc: |
      An array of Fast-Path Update (section 2.2.9.1.2.1) structures to be processed
      by the client.

instances:
  action:
    value: first_byte & 0x3
    enum: fastpath_output_action
    doc: |
      A 2-bit, unsigned integer that indicates whether the PDU is in fast-path
      or slow-path format.
  reserved:
    value: (first_byte >> 2) & 0xf
    doc: |
      A 4-bit, unsigned integer that is unused and reserved for future use.
      This field MUST be set to zero.
  flags:
    value: (first_byte >> 6) & 0x3
    doc: |
      A 2-bit, unsigned integer that contains flags describing the cryptographic
      parameters of the PDU.
  fips_information_length:
    value: 0
  data_signature_length:
    value: '(flags & fastpath_output::encrypted.to_i) != 0 ? 8 : 0'
  header_length_without_length:
    value: 1 + fips_information_length + data_signature_length
  header_length:
    value: 'header_length_without_length + ((length1 & 0x80) != 0 ? 2 : 1)'
  length1_value:
    value: (length1 << 0) &~ 0x80
  length1_bitshift:
    value: ((length1 << 0) & 0x80) >> 4
  length2_mask:
    value: (1 << length1_bitshift) - 1
  length2_value:
    value: (length2 << 0) & length2_mask
  length:
    value: length1_value << length1_bitshift | length2_value
    doc: |
      Contains the overall PDU length.
      The overall PDU length SHOULD be less than or equal to 16,383 bytes.

-update:
  - id: length1
    value: (header_length_without_length + 1 + update_array.length) & 0x7f
    doc: |
      Assume the length is 1 byte, set the value of length1.
  - id: length1
    value: (((header_length_without_length + 2 + update_array.length) >> 8) & 0x7f) | 0x80
    if: (header_length_without_length + 1 + update_array.length) > 0x7f
    doc: |
      Wrong assumption - length is 2 bytes, set the value of length1.
  - id: length2
    value: 0x00
    doc: |
      Assume length is 1 byte, length2 is not used.
  - id: length2
    value: (header_length_without_length + 2 + update_array.length) & 0xff
    if: (header_length_without_length + 1 + update_array.length) > 0x7f
    doc: |
      Wrong assumption - length is 2 bytes, set the value of length2.

enums:
  fastpath_output_action:
    0x0:
      id: fastpath
      -orig-id: FASTPATH_OUTPUT_ACTION_FASTPATH
      doc: |
        Indicates that the PDU is a fast-path output PDU.
    0x3:
      id: x224
      -orig-id: FASTPATH_OUTPUT_ACTION_X224
      doc: |
        Indicates the presence of a TPKT Header ([T123] section 8)
        initial version byte which indicates that the PDU is a
        slow-path output PDU (in this case the full value of the
        initial byte MUST be 0x03).
  fastpath_output:
    0x1:
      id: secure_checksum
      -orig-id: FASTPATH_OUTPUT_SECURE_CHECKSUM
      doc: |
        Indicates that the MAC signature for the PDU was generated using the
        "salted MAC generation" technique (section 5.3.6.1.1). If this bit
        is not set, then the standard technique was used
        (sections 2.2.8.1.1.2.2 and 2.2.8.1.1.2.3).
    0x2:
      id: encrypted
      -orig-id: FASTPATH_OUTPUT_ENCRYPTED
      doc: |
        Indicates that the PDU contains an 8-byte MAC signature after the optional
        length2 field (that is, the dataSignature field is present), and the
        contents of the PDU are encrypted using the negotiated encryption
        package (sections 5.3.2 and 5.3.6).

types:
  ts_fp_update_array:
    doc: |
      An array of Fast-Path Update (section 2.2.9.1.2.1) structures.
    seq:
      - id: update
        type: ts_fp_update
        repeat: eos
        doc: |
          An array of Fast-Path Update (section 2.2.9.1.2.1) structures to be processed
          by the client.
