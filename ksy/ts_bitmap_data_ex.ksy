meta:
  id: ts_bitmap_data_ex
  endian: le
  imports:
    - ts_compressed_bitmap_header_ex

doc: |
  MS-RDPBCGR 2.2.9.2.1.1 Extended Bitmap Data (TS_BITMAP_DATA_EX)
  The TS_BITMAP_DATA_EX structure is used to encapsulate encoded
  bitmap data.
  mstscax!CTSCoreGraphics::ProcessSurfaceCommands
  mstscax!CTSCoreGraphics::ProcessUpdateSurfaceBits

seq:
  - id: bpp
    type: u1
    doc: |
      An 8-bit, unsigned integer. The color depth of the bitmap data
      in bits-per-pixel.
  - id: flags
    type: u1
    doc: |
      An 8-bit, unsigned integer that contains flags.
  - id: reserved
    type: u1
    doc: |
      An 8-bit, unsigned integer. This field is reserved for future use.
      It MUST be set to zero.
  - id: codec_id
    type: u1
    doc: |
      An 8-bit, unsigned integer. The client-assigned ID that identifies
      the bitmap codec that was used to encode the bitmap data. Bitmap codec
      parameters are exchanged in the Bitmap Codecs Capability Set (section
      2.2.7.2.10). If this field is 0, then the bitmap data is not encoded
      and can be used without performing any decoding transformation.
  - id: width
    type: u2
    doc: |
      A 16-bit, unsigned integer. The width of the decoded bitmap image in pixels.
  - id: height
    type: u2
    doc: |
      A 16-bit, unsigned integer. The height of the decoded bitmap image in pixels.
  - id: bitmap_data_length
    type: u4
    doc: |
      A 32-bit, unsigned integer. The size in bytes of the bitmapData field.
  - id: ex_bitmap_data_header
    type: ts_compressed_bitmap_header_ex
    if: (flags & 0x01) != 0
    doc: |
      An optional Extended Compressed Bitmap Header (section 2.2.9.2.1.1.1) structure
      that contains nonessential information associated with bitmap data in the bitmapData
      field. This field MUST be present if the EX_COMPRESSED_BITMAP_HEADER_PRESENT (0x01)
      flag is present.
  - id: bitmap_data
    size: bitmap_data_length
    doc: |
      A variable-length array of bytes containing bitmap data encoded using the
      codec identified by the ID in the codecID field.

enums:
  ex_compressed_bitmap:
    0x01:
      id: header_present
      doc: |
        Indicates that the optional exBitmapDataHeader field is present.
