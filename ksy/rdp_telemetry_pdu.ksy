meta:
  id: rdp_telemetry_pdu
  endian: le

doc: |
  MS-RDPET 2.2.1 RDP_TELEMETRY_PDU
  The RDP_TELEMETRY_PDU message is a client-to-server PDU that is used to
  transmit metrics with respect to the time it took the client to complete
  a fully functional connection to the server.
  Telemetry service for on dynamic channel "Microsoft::Windows::RDS::Telemetry"
  mstscax!TelemetryClientPlugin::OnNewChannelConnection
  mstscax!TelemetryChannel::OnDataReceived
  mstscax!TelemetryChannel::SendTMTConnectionTime
  NOTE: OnDataReceived just returns 0x8000FFFF

seq:
  - id: id
    -orig-id: Id
    type: u1
    doc: |
      An 8-bit unsigned integer that MUST contain the value 0x01.
  - id: length
    -orig-id: length
    type: u1
    doc: |
      An 8-bit unsigned integer that specifies the length, in bytes, of the PDU.
      This field MUST be set to 0x12.
  - id: prompt_for_credentials_millis
    -orig-id: PromptForCredentialsMillis
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the difference, in milliseconds,
      between the time when the connection was initiated, and the time when a
      credentials prompt dialog was shown to the user. This value MUST be zero
      if no credentials prompt dialog was displayed.
  - id: prompt_for_credentials_done_millis
    -orig-id: PromptForCredentialsDoneMillis
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the difference, in milliseconds,
      between the time when the connection was initiated, and the time when
      credentials were successfully provided by the user. This value MUST be zero
      if no credentials prompt dialog was displayed.
  - id: graphics_channel_opened_millis
    -orig-id: GraphicsChannelOpenedMillis
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the difference, in milliseconds,
      between the time when the connection was initiated, and the time when the
      Remote Desktop Protocol: Graphics Pipeline Extension dynamic virtual channel
      ([MS-RDPEGFX] section 2.1) was accepted by the client.
  - id: first_graphics_received_millis
    -orig-id: FirstGraphicsReceivedMillis
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the difference in milliseconds,
      between the time when the connection was initiated, and the time when the
      first Desktop Protocol: Graphics Pipeline Extension graphics message
      ([MS-RDPEGFX] section 2.2) was received by the client.
