meta:
  id: rdp6_bitmap_stream
  endian: le

doc: |
  MS-RDPEGDI 2.2.2.5.1 RDP 6.0 Bitmap Compressed Bitmap Stream (RDP6_BITMAP_STREAM)
  The RDP6_BITMAP_STREAM structure contains a stream of bitmap data compressed using
  RDP 6.0 Bitmap Compression techniques (section 3.1.9). Depending on the compression
  techniques employed, the bitmap data is represented using the AYCoCg or ARGB color
  space (section 3.1.9.1.2).
  Compressed bitmap data is sent encapsulated in:
   - Bitmap Update ([MS-RDPBCGR] section 2.2.9.1.1.3.1.2)
   - Fast-Path Bitmap Update ([MS-RDPBCGR] section 2.2.9.1.2.1.2 )
   - Cache Bitmap - Revision 1 (section 2.2.2.2.1.2.2) Secondary Drawing Orders
   - Cache Bitmap - Revision 2 (section 2.2.2.2.1.2.3) Secondary Drawing Orders.
  In all of these cases, the data is encapsulated inside a Bitmap Data structure
  ([MS-RDPBCGR] section 2.2.9.1.1.3.1.2.2).
  mstscax!PlanarDecompressor::Decompress
  mstscax!Planar::BD_GetCompressionSettings
  mstscax!Planar::BD_PlanarBitmapSize
  mstscax!Planar::BD_DecompressBitmap

seq:
  - id: reserved
    type: b2
    doc: |
      A 2-bit, unsigned integer field. Reserved for future use.
  - id: no_alpha_plane
    type: b1
    doc: |
      A 1-bit field. Indicates if an alpha plane is present. If NA is equal to 1, there
      is no alpha plane. The values of the alpha plane are then assumed to be 0xFF (fully
      opaque), and the bitmap data contains only three color planes. If NA is equal to 0,
      the alpha plane is sent as the first color plane.
  - id: rle
    type: b1
    doc: |
      A 1-bit field. If RLE is equal to 1, RDP 6.0 RLE is used to compress the color planes
      (section 3.1.9.2). If not, RLE is equal to 0, and the color plane is sent uncompressed.
  - id: chroma_subsampling_level
    type: b1
    doc: |
      A 1-bit field that indicates whether chroma subsampling is being used (section 3.1.9.1.3).
      If CS is equal to 1, chroma subsampling is being used, and the CLL field MUST be greater
      than 0, as chroma subsampling applies only to the AYCoCg color space.
  - id: color_loss_level
    type: b3
    doc: |
      A 3-bit, unsigned integer field that indicates the Color Loss Level (section 3.1.9.1.4).
      If CLL is set to 0, the color space used is ARGB. Otherwise, CLL MUST be in the range 1
      to 7 (inclusive), and the color space used is AYCoCg.
  - id: data
    size-eos: true
    doc: |
      This is the contents of AlphaPlane, LumaOrRedPlane, OrangeChromaOrGreenPlane and 
      GreenChromaOrBluePlane. The size of each depends on the width, height and pixel format
      of the destination texture on the client. Hence we cannot separate them out without
      guilty knowledge of the client's state.

      AlphaPlane (variable):
      A variable-length field that contains the alpha plane. If the RLE subfield in the
      FormatHeader indicates that all of the color planes are RLE compressed (section 3.1.9.2),
      this field contains an RDP 6.0 RLE Segments (section 2.2.2.5.1.1) structure. Otherwise, it
      contains the raw bytes of the color plane.

      LumaOrRedPlane (variable):
      A variable-length field that contains the luma plane (AYCoCg color space) or the red plane
      (ARGB color space). If the CLL subfield of the FormatHeader is greater than 0, the AYCoCg
      color space MUST be used. Otherwise, the ARGB color space MUST be used.
      If the RLE subfield in the FormatHeader indicates that all of the color planes are RLE
      compressed (section 3.1.9.2), this field contains an RDP 6.0 RLE Segments (section 2.2.2.5.1.1)
      structure. Otherwise, it contains the raw bytes of the color plane.

      OrangeChromaOrGreenPlane (variable):
      A variable-length field that contains the orange chroma plane (AYCoCg color space) or the green
      plane (ARGB color space). If the CLL subfield of the FormatHeader is greater than 0, the AYCoCg
      color space MUST be used. Otherwise, the ARGB color space MUST be used.
      If the RLE subfield in the FormatHeader indicates that all of the color planes are RLE
      compressed (section 3.1.9.2), this field contains an RDP 6.0 RLE Segments (section 2.2.2.5.1.1)
      structure. Otherwise, it contains the raw bytes of the color plane.
      Depending on the values of the CLL and CS subfields of the FormatHeader (in the case of the
      AYCoCg color space), the orange chroma plane could have been transformed by color loss reduction
      (section 3.1.9.1.4) and chroma subsampling (section 3.1.9.1.3).

      GreenChromaOrBluePlane (variable):
      A variable-length field that contains the green chroma plane (AYCoCg color space) or the blue
      plane (ARGB color space). If the CLL subfield of the FormatHeader is greater than 0, the AYCoCg
      color space MUST be used. Otherwise, the ARGB color space MUST be used.
      If the RLE subfield in the FormatHeader indicates that all of the color planes are RLE compressed
      (section 3.1.9.2), this field contains an RDP 6.0 RLE Segments (section 2.2.2.5.1.1) structure.
      Otherwise, it contains the raw bytes of the color plane.
      Depending on the values of the CLL and CS subfields of the FormatHeader (in the case of the AYCoCg
      color space), the green chroma plane could have been transformed by color loss reduction
      (section 3.1.9.1.4) and chroma subsampling (section 3.1.9.1.3).
  - id: pad
    type: u1
    if: not rle
    doc: |
      An 8-bit, unsigned integer containing padding values that MUST be ignored. This optional field
      is only present if the RLE subfield of the FormatHeader field is zero.

types:
  rdp6_rle_segments:
    doc: |
      MS-RDPEGDI 2.2.2.5.1.1 RDP 6.0 RLE Segments (RDP6_RLE_SEGMENTS)
      The RDP6_RLE_SEGMENTS structure contains the run-length encoded contents of a color plane
      and consists of a collection of RDP6_RLE_SEGMENT structures.
      mstscax!DecodeBitmapFromRLE
    seq:
      - id: segments
        type: rdp6_rle_segment
        repeat: eos
        doc: |
          This field contains a variable-length array of RDP 6.0 RLE Segment (section 2.2.2.5.1.2)
          structures.

  rdp6_rle_segment:
    doc: |
      MS-RDPEGDI 2.2.2.5.1.2 RDP 6.0 RLE Segment (RDP6_RLE_SEGMENT)
      The RDP6_RLE_SEGMENT structure encodes an RLE segment that contains a RAW and RUN component
      (section 3.1.9.2).
      mstscax!DecodeRLEBytes
    seq:
      - id: raw_bytes
        type: b4
        doc: |
          A 4-bit, unsigned integer field. The number of RAW bytes in the rawValues field.
      - id: run_length
        type: b4
        doc: |
          A 4-bit, unsigned integer field. The number of times the last RAW byte in the rawValues
          field is repeated (known as the run-length).
          Because a RUN MUST be a sequence of at least three values (section 3.1.9.2), the values 1
          and 2 are used in the nRunLength field to encode extra long RUN sequences of more than 16
          values:
           - If the nRunLength field is set to 1, the actual run-length is 16 plus the value in
             cRawBytes. On decode, the number of RAW bytes in the rawValues field is assumed to be
             zero. This gives a maximum run-length of 31 values.
           - If the nRunLength field is set to 2, the actual run-length is 32 plus the value in
             cRawBytes. On decode, the number of RAW bytes in the rawValues field is assumed to be
             zero. This gives a maximum run-length of 47 values.
      - id: raw_values
        size: raw_values_size
        doc: |
          A variable-length field that contains the actual RAW bytes representing a portion of the
          color plane being encoded. If the bytes belong to the first scan line, they represent
          absolute values. Otherwise, the bytes represent delta values (section 3.1.9.2).
    instances:
      raw_values_size:
        value: '(run_length == 1) or (run_length == 2) ? 0 : raw_bytes'
