meta:
  id: rdcamera_device_pdu
  endian: le
  imports:
    - rdcamera_pdu

doc: |
  MS-RDPECAM 2.1 Transport
  Each camera device gets its own channel with "RDCamera_Device_%d"
  mstscax!MediaSourceListenerCallback::OnNewChannelConnection
  mstscax!MediaSourceVCCallback::OnDataReceived

seq:
  - id: data
    type: rdcamera_pdu
    doc: |
      Has the exact same data as a RDCAMERA_PDU.
