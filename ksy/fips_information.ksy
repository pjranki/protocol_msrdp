meta:
  id: fips_information
  endian: le

doc: |
  MS-RDPBCGR 2.2.8.1.2.1 Fast-Path FIPS Information (TS_FP_FIPS_INFO)
  The TS_FP_FIPS_INFO structure contains FIPS information for inclusion in a fast-path header.

seq:
  - id: length
    type: u2
    doc: |
      A 16-bit, unsigned integer.
      The length of the FIPS Security Header (section 2.2.8.1.1.2.3).
      This field MUST be set to 0x0010 (16 bytes).
  - id: version
    type: u1
    doc: |
      An 8-bit, unsigned integer.
      The version of the FIPS Header.
      This field SHOULD be set to TSFIPS_VERSION1 (0x01).
  - id: padlen
    type: u1
    doc: |
      An 8-bit, unsigned integer.
      The number of padding bytes of padding appended to the end of the
      packet prior to encryption to make sure that the data to be encrypted
      is a multiple of the 3DES block size (that is, a multiple of 8
      because the block size is 64 bits).
