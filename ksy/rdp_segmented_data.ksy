meta:
  id: rdp_segmented_data
  endian: le
  imports:
    - rdp_data_segment
    - rdp8_bulk_encoded_data

doc: |
  MS-RDPEGFX 2.2.5.1 RDP_SEGMENTED_DATA
  The RDP_SEGMENTED_DATA structure is used to wrap one or more RDP_DATA_SEGMENT
  (section 2.2.5.2) structures. Each segment contains data that has been encoded
  using RDP 8.0 Bulk Compression techniques (section 3.1.9.1).
  mstscax!RdpGfxClientChannel::OnDataReceived
  mstscax!RdpGfxProtocolClientDecoder::Decode
  mstscax!DecompressUnchopper::Decompress

seq:
  - id: descriptor
    type: u1
    enum: rdp_segmented
    doc: |
      An 8-bit unsigned integer reserved for future use. This field MUST be set to zero.
  - id: segment_count
    type: u2
    if: 'descriptor == rdp_segmented::multipart'
    doc: |
      An optional 16-bit unsigned integer that specifies the number of elements in the
      segmentArray field.
  - id: uncompressed_size
    type: u4
    if: 'descriptor == rdp_segmented::multipart'
    doc: |
      An optional 32-bit unsigned integer that specifies the size, in bytes, of the data
      present in the segmentArray field once it has been reassembled and decompressed.
  - id: bulk_data
    type: rdp8_bulk_encoded_data
    if: 'descriptor == rdp_segmented::single'
    doc: |
      An optional variable-length RDP8_BULK_ENCODED_DATA structure (section 2.2.5.3).
  - id: segment_array
    type: rdp_data_segment
    if: 'descriptor == rdp_segmented::multipart'
    repeat: expr
    repeat-expr: segment_count
    doc: |
      An optional variable-length array of RDP_DATA_SEGMENT structures. The number of
      elements in this array is specified by the segmentCount field.

enums:
  rdp_segmented:
    0xE0:
      id: single
      doc: |
        The segmentCount, uncompressedSize, and segmentArray fields MUST NOT be present,
        and the bulkData field MUST be present.
    0xE1:
      id: multipart
      doc: |
        The segmentCount, uncompressedSize, and segmentArray fields MUST be present, and
        the bulkData field MUST NOT be present.
