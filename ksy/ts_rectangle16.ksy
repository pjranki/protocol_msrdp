meta:
  id: ts_rectangle16
  endian: le

doc: |
  MS-RDPBCGR 2.2.11.1 Inclusive Rectangle (TS_RECTANGLE16)
  MS-RDPERP 2.2.1.2.2 Rectangle (TS_RECTANGLE_16)
  The TS_RECTANGLE16 structure describes a rectangle expressed in inclusive coordinates
  (the right and bottom coordinates are included in the rectangle bounds).

seq:
  - id: left
    type: u2
    doc: An unsigned 16-bit integer. The x-coordinate of the rectangle's top-left corner.
  - id: top
    type: u2
    doc: An unsigned 16-bit integer. The y-coordinate of the rectangle's top-left corner.
  - id: right
    type: u2
    doc: An unsigned 16-bit integer. The x-coordinate of the rectangle's bottom-right corner.
  - id: bottom
    type: u2
    doc: An unsigned 16-bit integer. The y-coordinate of the rectangle's bottom-right corner.
