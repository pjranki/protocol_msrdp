meta:
  id: ts_update_bitmap_data
  endian: le
  imports:
    - ts_bitmap_data

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.2.1 Bitmap Update Data (TS_UPDATE_BITMAP_DATA)
  The TS_UPDATE_BITMAP_DATA structure encapsulates the bitmap data that
  defines a Bitmap Update (section 2.2.9.1.1.3.1.2).
  mstscax!CTSCoreGraphics::ProcessBitmap

seq:
  - id: update_type
    type: u2
    doc: |
      A 16-bit, unsigned integer. The update type. This field MUST be set
      to UPDATETYPE_BITMAP (0x0001).
  - id: number_rectangles
    type: u2
    doc: |
      A 16-bit, unsigned integer. The number of screen rectangles present
      in the rectangles field.
  - id: rectangle
    type: ts_bitmap_data
    repeat: expr
    repeat-expr: number_rectangles
    doc: |
      Variable-length array of TS_BITMAP_DATA (section 2.2.9.1.1.3.1.2.2)
      structures, each of which contains a rectangular clipping taken from
      the server-side screen frame buffer. The number of screen clippings
      in the array is specified by the numberRectangles field.
