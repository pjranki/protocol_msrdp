meta:
  id: ts_colorpointerattribute
  endian: le
  imports:
    - ts_point16

doc: |
  MS-RDPBCGR 2.2.9.1.1.4.4 Color Pointer Update (TS_COLORPOINTERATTRIBUTE)
  The TS_COLORPOINTERATTRIBUTE structure represents a regular T.128 24 bpp
  color pointer, as specified in [T128] section 8.14.3. This pointer update
  is used for both monochrome and color pointers in RDP.
  mstscax!CCM::CM_ColorPointerPDU
  mstscax!CCM::CMCreateNewColorCursor

seq:
  - id: cache_index
    type: u2
    doc: |
      A 16-bit, unsigned integer. The zero-based cache entry in the pointer
      cache in which to store the pointer image. The number of cache entries
      is specified using the Pointer Capability Set (section 2.2.7.1.5).
  - id: hot_spot
    type: ts_point16
    doc: |
      A Point (section 2.2.9.1.1.4.1) structure containing the x-coordinates
      and y-coordinates of the pointer hotspot.
  - id: width
    type: u2
    doc: |
      A 16-bit, unsigned integer. The width of the pointer in pixels. The
      maximum allowed pointer width is 96 pixels if the client set the
      LARGE_POINTER_FLAG_96x96 (0x00000001) flag in the Large Pointer Capability
      Set (section 2.2.7.2.7). If the LARGE_POINTER_FLAG_96x96 was not set,
      the maximum allowed pointer width is 32 pixels.
  - id: height
    type: u2
    doc: |
      A 16-bit, unsigned integer. The height of the pointer in pixels. The maximum
      allowed pointer height is 96 pixels if the client set the
      LARGE_POINTER_FLAG_96x96 (0x00000001) flag in the Large Pointer Capability
      Set (section 2.2.7.2.7). If the LARGE_POINTER_FLAG_96x96 was not set,
      the maximum allowed pointer height is 32 pixels.
  - id: length_and_mask
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the andMaskData field.
  - id: length_xor_mask
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the xorMaskData field.
  - id: xor_mask_data
    size: length_xor_mask
    doc: |
      A variable-length array of bytes. Contains the 24-bpp, bottom-up XOR mask
      scan-line data. The XOR mask is padded to a 2-byte boundary for each encoded
      scan-line. For example, if a 3x3 pixel cursor is being sent, then each
      scan-line will consume 10 bytes (3 pixels per scan-line multiplied by 
      bytes per pixel, rounded up to the next even number of bytes).
  - id: and_mask_data
    size: length_and_mask
    doc: |
      A variable-length array of bytes. Contains the 1-bpp, bottom-up AND mask
      scan-line data. The AND mask is padded to a 2-byte boundary for each encoded
      scan-line. For example, if a 7x7 pixel cursor is being sent, then each
      scan-line will consume 2 bytes (7 pixels per scan-line multiplied by 1 bpp,
      rounded up to the next even number of bytes).
  - id: padding
    size: (2 - _io.pos) % 2
    doc: |
      An optional 8-bit, unsigned integer. Padding. Values in this field MUST be
      ignored.
