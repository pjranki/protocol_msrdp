meta:
  id: ts_pointerposattribute
  endian: le
  imports:
    - ts_point16

doc: |
  MS-RDPBCGR 2.2.9.1.1.4.2 Pointer Position Update (TS_POINTERPOSATTRIBUTE)
  The TS_POINTERPOSATTRIBUTE structure is used to indicate that the client
  pointer MUST be moved to the specified position relative to the top-left
  corner of the server's desktop ([T128] section 8.14.4).
  mstscax!CCM::CM_PositionPDU

seq:
  - id: position
    type: ts_point16
    doc: |
      A Point (section 2.2.9.1.1.4.1) structure containing the new
      x-coordinates and y-coordinates of the pointer.
