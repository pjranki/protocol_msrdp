meta:
  id: rdpgfx_redir_pdu
  endian: le

doc: |
  Dynamic virtual channel 'Microsoft::Windows::RDS::RemoteAppGraphicsRedirection'
  C++ classes of interest:
    - RemoteAppGfxRedirChannel
    - RdpWin32RemoteAppPresenter
  mstscax!RemoteAppGfxRedirChannel::OnDataReceived

seq:
  - id: cmd_id
    type: u2
    enum: rdpgfx_redir_cmdid
    doc: |
      Command ID of this PDU.
  - id: flags
    type: u2
    doc: |
      Reserved, should be set to zero.
  - id: pdu_length
    type: u4
    doc: |
      Total size of this PDU.
  - id: data
    size: data_size
    type:
      switch-on: cmd_id
      -name: type
      cases:
        'rdpgfx_redir_cmdid::graphics_update_pdu': graphics_update_pdu
        'rdpgfx_redir_cmdid::graphics_update_ack_pdu': graphics_update_ack_pdu
        'rdpgfx_redir_cmdid::create_surface_pdu': create_surface_pdu
        'rdpgfx_redir_cmdid::delete_surface_pdu': delete_surface_pdu
        'rdpgfx_redir_cmdid::error_pdu': error_pdu
        'rdpgfx_redir_cmdid::create_composition_surface_pdu': create_composition_surface_pdu

instances:
  data_size:
    value: 'pdu_length >= 8 ? pdu_length - 8 : 0'

enums:
  rdpgfx_redir_cmdid:
    2: graphics_update_pdu
    3: graphics_update_ack_pdu
    4: create_surface_pdu
    5: delete_surface_pdu
    6: error_pdu
    7: create_composition_surface_pdu

types:
  rdpx_rect:
    seq:
      - id: left
        type: u4
      - id: top
        type: u4
      - id: width
        type: u4
      - id: height
        type: u4

  graphics_update_pdu:
    doc: |
      Locates the RdpWin32RemoteAppPresenter instance using:
        - mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap
      And then calls present:
        - mstscax!RdpWin32RemoteAppPresenter::Present
      mstscax!RemoteAppGfxRedirChannel::HandleGraphicsUpdatePdu
    seq:
      - id: timestamp
        type: u8
        doc: |
          Used for logging lag.
      - id: window_id
        type: u8
        doc: |
          Used to locate the RdpWin32RemoteAppPresenter instance.
          mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap
      - id: region
        type: rdpx_rect
        doc: |
          Window to present.
          mstscax!RdpWin32RemoteAppPresenter::Present

  graphics_update_ack_pdu:
    doc: |
      Is sent on completion of GRAPHICS_UPDATE_PDU.
      mstscax!RemoteAppGfxRedirChannel::SendGraphicsUpdateAckPdu
    seq:
      - id: window_id
        type: u8
        doc: |
          This is the same value as was sent with GRAPHICS_UPDATE_PDU.

  create_surface_pdu:
    doc: |
      Uses a section object to send an RdpXPixelFormat_ARGB8888 image to the client.
      The Bits Per Pixel (BPP) is hard-coded to 32 (4 bytes per pixel).
      mstscax!RemoteAppGfxRedirChannel::HandleCreateSurfacePdu
      mstscax!RdpWin32RemoteAppPresenter::UseSharedMemoryTexture
    seq:
      - id: window_id
        type: u8
        doc: |
          Used to locate the RdpWin32RemoteAppPresenter instance.
          mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap
      - id: region_size
        type: u4
        doc: |
          Total size of shared memory region. Client does not trust this value
          and hence generates it using '4 x width x height'. This size is then
          checked to be no bigger than the mapped regions sized (check with VirtualQuery).
      - id: scan_line_size
        type: u4
        doc: |
          Number of bytes that make up a single line of pixels in the shared memory
          image. Client does not trust this value - instead using '4 * width'.
      - id: height
        type: u4
        doc: |
          Height of the image in shared memory (in pixels).
      - id: width
        type: u4
        doc: |
          Width of the image in shared memory (in pixels).
      - id: section_name_length
        type: u4
        doc: |
          Number of unicode characters (including the nul) in the section name.
          The client does not trust this value and scans until it finds the nul
          terminator.
      - id: section_name
        type: str
        size: section_name_length * 2
        encoding: UTF-16LE
        doc: |
          Section name (GUID string) that identifies the section object in the virtual
          machine (i.e. WDAG VM). Makes up the path:
            - "\VmSharedMemory\Host\{Virtual Machine GUID}\{Section Name GUID}"

  delete_surface_pdu:
    doc: |
      Deletes the shared texture instance within RdpWin32RemoteAppPresenter using:
        - mstscax!RdpWin32RemoteAppPresenter::DiscardSharedMemoryTexture
      NOTE: Does not unmap shared memory.
      mstscax!RemoteAppGfxRedirChannel::HandleDeleteSurfacePdu
    seq:
      - id: window_id
        type: u8
        doc: |
          Used to locate the RdpWin32RemoteAppPresenter instance.
          mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap

  error_pdu:
    doc: |
      mstscax!RemoteAppGfxRedirChannel::HandleErrorPdu
    seq:
      - id: error
        type: u4

  create_composition_surface_pdu:
    doc: |
      Calls the following dcomp.dll APIs:
        - dcomp!
      mstscax!RemoteAppGfxRedirChannel::HandleCreateCompositionSurface
    seq:
      - id: window_id
        type: u8
        doc: |
          Used to locate the RdpWin32RemoteAppPresenter instance.
          mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap
      - id: handle
        type: u8
        doc: |
          The handle to an existing composition surface that was created by a call to
          DCompositionCreateSurfaceHandle (win32 API).
