meta:
  id: tsmm_capabilities
  -orig-id: TSMM_CAPABILITIES
  endian: le

doc: |
  MS-RDPEV 2.2.6 TSMM_CAPABILITIES Structure
  mstscax!CMMPlayerManager::ExchangeCapabilities
  mstscax!HandleCommonRemoteCapabilities
  mstscax!PopulateCommonHostCapabilities

seq:
  - id: capability_type
    -orig-id: CapabilityType
    type: u4
    doc: |
      A 32-bit unsigned integer that indicates the capability type.<3>
  - id: capability_length
    -orig-id: cbCapabilityLength
    type: u4
    doc: |
      A 32-bit unsigned integer. This field MUST contain the number of bytes in the
      pCapabilityData field.
      The number of bytes in the pCapabilityData field is dependent on the CapabilityType.<4>
  - id: capability_data
    -orig-id: pCapabilityData
    size: capability_length
    doc: |
      An array of 8-bit unsigned integers. This field contains the capability value.
      When the CapabilityType field is set to 0x00000001 for a protocol version request,
      the pCapabilityData field MUST be a 32-bit unsigned integer with a value of 0x00000002
      to indicate client support for the current protocol version.
      When the CapabilityType field is set to 0x00000002, the pCapabilityData field MUST be
      a 32-bit unsigned integer representing a supported platform. The value for the supported
      platforms is derived as a union of the MMREDIR_CAPABILITY_PLATFORM constants defined in
      section 2.2.10.
      When the CapabilityType field is set to 0x00000003, the pCapabilityData field MUST be
      a 32-bit unsigned integer from the MMREDIR_CAPABILITY_AUDIOSUPPORT constants defined in
      section 2.2.15. The value of this integer constant determines whether audio is supported.
      When the CapabilityType field is set to 0x00000004, the pCapabilityData field MUST be
      a 32-bit unsigned integer integer indicating the one-way network latency in milliseconds.
