meta:
  id: xpsrd_req_pdu
  endian: le
  imports:
    - rdp_guid
    - tsprinter_property

doc: |
  MS-RDPEXPS 2.1 Transport
  Dynamic channel "XPSRD"
  mstscax!CXPSClientHandler::OnNewChannelConnection
  mstscax!CRimChannel::OnDataReceived
  mstscax!CRIMObjManager::OnDataReceived
  mstscax!CRIMObjectPool<CMemory>::GetPooledObject
  mstscax!CMemory::Initialize
  mstscax!CRIMStreamStub::OnDataReceived (could also be mstscax!CRIMStreamProxy::OnDataReceived)
  mstscax!CRIMObjectPool<CMessage>::GetPooledObject
  mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
  mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke

seq:
  - id: interface_id
    -orig-id: InterfaceValue
    type: u4
    enum: interface_id
    doc: |
      A 32-bit unsigned integer that represents the common identifier for the interface.
      The default value is 0x00000000, and if the message uses this default interface ID, it is
      interpreted as the main interface for which this channel has been instantiated. All other
      values MUST be retrieved either from an Interface Query message response (QI_RSP) or from
      responses that contain interface IDs. For example, in section 2.2.4.3.1.1 the Callback
      parameter describes an interface, and it represents an ID suitable for use as an interface
      ID in another set of request/reply sequences.
      This ID is valid until an IFACE_RELEASE message is sent/received with that ID. After an
      IFACE_RELEASE message, this ID is considered invalid. A packet with an invalid InterfaceId
      causes channel termination from the packet parser on the receiving end.
  - id: message_id
    -orig-id: MessageId
    type: u4
    doc: |
      A 32-bit unsigned integer that represents a unique ID for the request and response pair.
      Requests and responses are matched, in part, based on this ID.
  - id: function_id
    -orig-id: FunctionId
    type: u4
    doc: |
      A 32-bit unsigned integer. This field MUST be present only in request packets. Its value
      is either used in interface manipulation messages or defined for a specific interface.
  - id: rimcall
    if: function_id < 100
    type:
      switch-on: function_id
      cases:
        'function_id_rimcall::release.to_i': xpsrd_rimcall_iface_release
        'function_id_rimcall::queryinterface.to_i': xpsrd_rimcall_qi_req
  - id: printer_driver
    if: (function_id >= 100) and ((interface_id.to_i & ~0xc0000000) == interface_id::printer_driver.to_i)
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_printer_driver::init_printer_req.to_i': xpsrd_init_printer_req
        'function_id_printer_driver::get_all_dev_caps_req.to_i': xpsrd_get_all_dev_caps_req
        'function_id_printer_driver::convert_devmode_req.to_i': xpsrd_convert_devmode_req
        'function_id_printer_driver::get_default_devmode_req.to_i': xpsrd_get_default_devmode_req
        'function_id_printer_driver::get_device_cap_req.to_i': xpsrd_get_device_cap_req
        'function_id_printer_driver::doc_properties_req.to_i': xpsrd_doc_properties_req
        'function_id_printer_driver::async_doc_props_req.to_i': xpsrd_async_doc_props_req
        'function_id_printer_driver::async_printer_props_req.to_i': xpsrd_async_printer_props_req
        'function_id_printer_driver::printer_props_req.to_i': xpsrd_printer_props_req
        'function_id_printer_driver::cancel_async_doc_props_req.to_i': xpsrd_cancel_async_doc_props_req
        'function_id_printer_driver::cancel_async_printer_props_req.to_i': xpsrd_cancel_cancel_async_printer_props_req
        'function_id_printer_driver::move_doc_properties_req.to_i': xpsrd_move_doc_properties_req
        'function_id_printer_driver::mxdc_getpdev_adjustment_req.to_i': xpsrd_mxdc_getpdev_adjustment_req

enums:
  interface_id:
    0x00000000:
      id: printer_driver
      doc: |
        MS-RDPEXPS 2.2.4 Printer Driver Interface
        The Printer Driver Interface is identified by the default interface ID 0x00000000.
        The default interface does not require Query Interface Request (QI_REQ) or
        Query Interface Response (QI_RSP) messages to initialize the interface.
        The Printer Driver Interface negotiates parameters for rendering a print job on the client.
        This interface consists of three message types: initialization, capabilities, and
        user interface.
        Certain fields in this interface are payload between the client printer driver and the
        printing subsystem. The content and the meaning of these fields depend on internal structures
        for these two systems, and are not interpreted in any way by the Printer Driver Interface.
        Special consideration has to be taken regarding the implementation on both sides when they
        are based on different operating systems. The implementation has to translate these operating
        system-specific differences between the client printer driver and the printing subsystem.
  function_id_rimcall:
    0x00000001:
      id: release
      -orig-id: RIMCALL_RELEASE
      doc: |
        Release the given interface ID.
    0x00000002:
      id: queryinterface
      -orig-id: RIMCALL_QUERYINTERFACE
      doc: |
        Query for a new interface.
  function_id_printer_driver:
    0x00000100:
      id: init_printer_req
      -orig-id: INIT_PRINTER_REQ
      doc: |
        Server Initialize Printer request
    0x00000101:
      id: get_all_dev_caps_req
      -orig-id: GET_ALL_DEV_CAPS_REQ
      doc: |
        Server Get All Dev Caps request
    0x00000102:
      id: convert_devmode_req
      -orig-id: CONVERT_DEVMODE_REQ
      doc: |
        Server Convert Devmode request
    0x00000103:
      id: get_default_devmode_req
      doc: |
        UNDOCUMENTED
        mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_GetDefaultDevMode
    0x00000104:
      id: get_device_cap_req
      -orig-id: GET_DEVICE_CAP_REQ
      doc: |
        Server Get Device Capability request
    0x00000105:
      id: doc_properties_req
      -orig-id: DOC_PROPERTIES_REQ
      doc: |
        Server Document Properties request
    0x00000106:
      id: async_doc_props_req
      -orig-id: ASYNC_DOC_PROPS_REQ
      doc: |
        Server Async Document Properties request
    0x00000107:
      id: async_printer_props_req
      -orig-id: ASYNC_PRINTER_PROPS_REQ
      doc: |
        Server Async Printer Properties request
    0x00000108:
      id: printer_props_req
      doc: |
        UNDOCUMENTED
        mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_PrinterProperties
    0x00000109:
      id: cancel_async_doc_props_req
      -orig-id: CANCEL_ASYNC_DOC_PROPS_REQ
      doc: |
        Server Cancel Async Document Properties request
    0x0000010A:
      id: cancel_async_printer_props_req
      -orig-id: CANCEL_ASYNC_PRINTER_PROPS_REQ
      doc: |
        Server Cancel Async Printer Properties request
    0x0000010B:
      id: move_doc_properties_req
      -orig-id: MOVE_DOC_PROPERTIES_REQ
      doc: |
        Server Move Document Properties Window request
    0x0000010C:
      id: mxdc_getpdev_adjustment_req
      -orig-id: MXDC_GETPDEV_ADJUSTMENT_REQ
      doc: |
        Server Get Device Adjustments request
  xpsredir_printer_properties_flags:
    0x00000000:
      id: with_permission
      -orig-id: XPSREDIR_PRINTER_PROPERTIES_Flags_WITH_PERMISSION
      doc: |
        If this value is set, the printer properties are queried with administrative privilege.
    0x00000001:
      id: no_permission
      -orig-id: XPSREDIR_PRINTER_PROPERTIES_Flags_NO_PERMISSION
      doc: |
        If this value is set, the printer properties are queried with user privilege.

types:
  xpsrd_rimcall_qi_req:
    doc: |
      MS-RDPEXPS 2.2.2.1.1 Query Interface Request (QI_REQ)
      The QI_REQ request message is sent from either the client side or the server side,
      and is used to request a new interface ID.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CProxyIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::QueryInterface
      mstscax!CTSClientPrintDriver::NonDelegatingQueryInterface
      mstscax!CMessage::WriteIUnknown
      mstscax!CRIMObjManager::GetIdFromObject
    seq:
      - id: new_interface_guid
        -orig-id: NewInterfaceGUID
        type: rdp_guid
        doc: |
          A 16-byte GUID that identifies the new interface.

  xpsrd_rimcall_iface_release:
    doc: |
      MS-RDPEXPS 2.2.2.2 Interface Release (IFACE_RELEASE)
      Terminates the lifetime of the interface. This message is one-way only.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CMessage::RemoveIUnknown
      mstscax!CRIMObjManager::RemoveObject
      (no data)

  xpsrd_init_printer_req:
    doc: |
      MS-RDPEXPS 2.2.4.1.1 Server Initialize Printer Request (INIT_PRINTER_REQ)
      The INIT_PRINTER_REQ message MUST be the first message, sent from the server to the
      client, in the Printer Driver Interface. This message establishes a link between a
      particular client-side printer and the dynamic virtual channel connection. After
      this link is established, any messages coming on the same channel implicitly refer
      to the same client printer driver.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_InitializePrinter
      mstscax!CTSClientPrintDriver::InitializePrinter
      mstscax!CPrinterCollection::GetPrinterName
    seq:
      - id: client_printer_id
        -orig-id: ClientPrinterId
        type: u4
        doc: |
          A 32-bit unsigned integer. This ID is exchanged by the
          Remote Desktop Protocol: File System Virtual Channel Extension, as specified in [MS-RDPEFS].
          The ClientPrinterId value MUST be the same as the DeviceId field in the DEVICE_ANNOUNCE
          packet (as specified in [MS-RDPEFS] section 2.2.1.3).

  xpsrd_get_all_dev_caps_req:
    doc: |
      MS-RDPEXPS 2.2.4.2.1 Server Get All Dev Caps Request (GET_ALL_DEV_CAPS_REQ)
      The GET_ALL_DEV_CAPS_REQ request is sent from the server to the client to retrieve all printer
      capabilities from the client.
      IDA shows there are 36 capability indices that are iterated and returned.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_GetAllDevCaps
      mstscax!CTSClientPrintDriver::GetAllDevCaps
      winspool!DeviceCapabilitiesW
      *!DrvDeviceCapabilities
      *!DrvSplDeviceCaps
      mstscax!CProxyWrite_TSDEVICE_CAPABILITY
      (no_data)

  xpsrd_convert_devmode_req:
    doc: |
      MS-RDPEXPS 2.2.4.2.3 Server Convert Devmode Request (CONVERT_DEVMODE_REQ)
      When the server operating system installs the virtual printer driver, it calls into it to convert
      an internal DEVMODE structure into a printer-specific DEVMODE structure. This message redirects
      this call to be processed by the client-side printer driver.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_ConvertDevMode
      mstscax!CTSClientPrintDriver::ConvertDevMode
      mstscax!CTSClientPrintDriver::VerifyDevMode
    seq:
      - id: mode
        -orig-id: fMode
        type: u4
        doc: |
          A 32-bit unsigned integer. The content is generated by the printing subsystem and is treated
          as payload in this protocol.
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. The number of bytes in the DevmodeIn field.
      - id: devmode_in
        -orig-id: DevmodeIn
        size: devmode_in_size
        doc: |
          An array of 8-bit unsigned integers. The content is generated by the printing subsystem and
          is treated as payload in this protocol.
      - id: devmode_out_size
        -orig-id: cbDevmodeOut
        type: u4
        doc: |
          A 32-bit unsigned integer. The number of bytes in the DevmodeOut field.
      - id: devmode_out
        -orig-id: DevmodeOut
        size: devmode_out_size
        doc: |
          An array of 8-bit unsigned integers. The content is generated by the client printer driver
          and is treated as payload in the Printer Driver Interface.
      - id: max_output_buffer_size
        -orig-id: cbProvided
        type: u4
        doc: |
          A 32-bit unsigned integer. The maximum number of bytes in the OutputBufferSize field of the
          CONVERT_DEVMODE_RSP response that corresponds to this request.

  xpsrd_get_default_devmode_req:
    doc: |
      UNDOCUMENTED
      NOTE: This has been removed and is just a stub function returning error 0x80004001
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_GetDefaultDevMode
      (no data)

  xpsrd_get_device_cap_req:
    doc: |
      MS-RDPEXPS 2.2.4.2.5 Server Get Device Capability Request (GET_DEVICE_CAP_REQ)
      This message is sent by the server side to query a particular printer capability from the
      client-side printer driver.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_GetDeviceCapability
      mstscax!CTSClientPrintDriver::GetDeviceCapability
      mstscax!CTSClientPrintDriver::GetDeviceCapabilityBuffer_Clean
      winspool!DeviceCapabilitiesW
    seq:
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the size, in bytes, of the
          following DevmodeIn field.
      - id: devmode_in
        -orig-id: DevmodeIn
        size: devmode_in_size
        doc: |
          An array of 8-bit unsigned integers. The content is generated by the printing subsystem
          and is treated as payload in the Printer Driver Interface.
      - id: device_cap
        -orig-id: DeviceCap
        type: u2
        doc: |
          A 16-bit unsigned integer. The content is generated by the printing subsystem and is
          treated as payload in the Printer Driver Interface.
      - id: input_buffer_size
        -orig-id: InputBufferSize
        type: u4
        doc: |
          A 32-bit unsigned integer. MUST contain the maximum size in bytes of the OutputBuffer
          field in the corresponding GET_DEVICE_CAP_RSP response to this request message.

  xpsrd_doc_properties_req:
    doc: |
      MS-RDPEXPS 2.2.4.2.7 Server Document Properties Request (DOC_PROPERTIES_REQ)
      When an application on a server invokes the document properties dialog box on the server side,
      a call to the printer driver results. A call for this scenario is redirected by this message.
      NOTE: Will display a window for document properties.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_DocumentPropertiesW
      mstscax!CTSClientPrintDriver::DocumentPropertiesW
      mstscax!CTSClientPrintDriver::VerifyDevMode
      mstscax!CClientUtilsWin32::UT_IsRunningInAppContainer
      mstscax!CTSClientPrintDriver::ModernDocumentProperties
      winspool!DocumentPropertiesW
    seq:
      - id: mode
        -orig-id: fMode
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST identify the mode in which the server obtains
          document properties from the client printer driver; the server retrieves document properties
          for the specified mode only. The content of this field is generated by the printing subsystem
          and is treated as a payload in the Printer Driver Interface.
      - id: server_window
        -orig-id: hServerWindow
        type: u8
        doc: |
          A 64-bit unsigned integer. On the server side, when a call is made to a printer driver to
          show a document properties user interface (UI), it also specifies a WindowHandle to be used
          for parenting any UI that is displayed by the printer driver. The WindowHandle is passed in
          this field.
          NOTE: A window can be created on the client using RAIL channels that can be the parent
                of the document properties window.
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. The field MUST contain the number of bytes in the DevmodeIn field.
      - id: devmode_in
        -orig-id: DevmodeIn
        size: devmode_in_size
        doc: |
          An array of 8-bit unsigned integers. The content is generated by the printing subsystem
          and is treated as payload in the Printer Driver Interface.
      - id: max_devmode_out_size
        -orig-id: OutputDevModeSizeProvided
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the maximum number of bytes in the
          DevmodeOut field of the corresponding DOC_PROPERTIES_RSP response message to this
          request.

  xpsrd_async_doc_props_req:
    doc: |
      MS-RDPEXPS 2.2.4.3.2.1 Server Async Document Properties Request (ASYNC_DOC_PROPS_REQ)
      This request is sent from server to client and is used to start the display of a
      document properties UI on the client.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_AsyncDocumentProperties
      mstscax!CMessage::ReceiveIUnknown
      mstscax!CTSClientPrintDriver::AsyncDocumentProperties
      mstscax!CTSClientPrintDriver::static_AsyncDocumentProperties
      mstscax!CTSClientPrintDriver::DocumentPropertiesW
      mstscax!CTSClientPrintDriver::VerifyDevMode
    seq:
      - id: mode
        -orig-id: fMode
        type: u4
        doc: |
          A 32-bit unsigned integer. The content is generated by the printing subsystem and
          is treated as payload in the Printer Driver Interface.
      - id: server_window
        -orig-id: hServerWindow
        type: u8
        doc: |
          A 64-bit unsigned integer. When an operating system calls into a server virtual
          printer driver to display the document properties UI, it also supplies a WindowHandle
          to use as a parent for the UI displayed. The WindowHandle is passed to the client
          printer driver in this field.
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of bytes in the following
          DevmodeIn field.
      - id: devmode_in
        -orig-id: DevmodeIn
        size: devmode_in_size
        doc: |
          An array of 8-bit unsigned integers. The content is generated by the printing subsystem
          and is treated as payload in the Printer Driver Interface.
      - id: max_devmode_out_size
        -orig-id: OutputDevModeSize
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the maximum size in bytes of the Devmode
          field of the DOC_PROPS_CALLBACK_REQ request message.
      - id: reserved
        -orig-id: Reserved
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST be set to 0x00000001.
      - id: callback
        -orig-id: Callback
        type: u4
        doc: |
          A 32-bit unsigned integer. This value represents a unique interface ID to be used by a
          DOC_PROPS_CALLBACK_REQ request message.

  xpsrd_doc_props_callback_rsp:
    doc: |
      MS-RDPEXPS 2.2.4.3.2.4 Server Document Properties Callback Response (DOC_PROPS_CALLBACK_RSP)
      This packet is sent as an acknowledgment of a DOC_PROPS_CALLBACK_REQ
      (Client Document Properties Callback Request) from the server to the client.
      mstscax!CProxyITS_PrinterDriver_DocumentPropertiesCallBack<ITS_PrinterDriver_DocumentPropertiesCallBack>::DocumentPropertiesCallBack
    seq:
      - id: reserved
        -orig-id: Reserved
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST be set to 0x00000000.
          This is treated as a HRESULT of success.

  xpsrd_async_printer_props_req:
    doc: |
      MS-RDPEXPS 2.2.4.3.1.1 Server Async Printer Properties Request (ASYNC_PRINTER_PROPS_REQ)
      This request is sent from the server to the client, and causes the client to display a printer
      properties user interface.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_AsyncPrinterProperties
      mstscax!CTSClientPrintDriver::AsyncPrinterProperties
      mstscax!CTSClientPrintDriver::static_AsyncPrinterProperties
      mstscax!CTSClientPrintDriver::ModernPrinterProperties
      mstscax!CTSClientPrintDriver::PrinterProperties
    seq:
      - id: flags
        -orig-id: Flags
        type: u4
        enum: xpsredir_printer_properties_flags
        doc: |
          A 32-bit unsigned integer. The value of this field is used to determine the privilege
          level used to query for printer properties.
      - id: server_window
        -orig-id: hServerWindow
        type: u8
        doc: |
          A 64-bit unsigned integer. When an operating system calls into the server virtual printer
          driver to display the printer properties UI, it also supplies a WindowHandle to use as a
          parent for the UI displayed. The WindowHandle is passed to the client printer driver in
          this field.
      - id: reserved
        -orig-id: Reserved
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST be set to 0x00000001.
      - id: callback
        -orig-id: Callback
        type: u4
        doc: |
          A 32-bit unsigned integer. This value represents a unique InterfaceId
          (as described in section 3.1.1) that MUST be used in the InterfaceId field of a
          PRINTER_PROPS_CALLBACK_REQ request message.

  xpsrd_printer_props_callback_rsp:
    doc: |
      MS-RDPEXPS 2.2.4.3.1.4 Server Printer Properties Callback Response (PRINTER_PROPS_CALLBACK_RSP)
      An acknowledgment of the PRINTER_PROPS_CALLBACK_REQ
      (Client Printer Properties Callback Request) from server to client.
      mstscax!CProxyITS_PrinterDriver_PrinterPropertiesCallBack<ITS_PrinterDriver_PrinterPropertiesCallBack>::PrinterPropertiesCallBack
    seq:
      - id: reserved
        -orig-id: Reserved
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST be set to 0x00000000.
          This is treated as a HRESULT of success.

  xpsrd_printer_props_req:
    doc: |
      UNDOCUMENTED
      Works as a synchronous variant of ASYNC_PRINTER_PROPS_REQ.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_PrinterProperties
      mstscax!CTSClientPrintDriver::PrinterProperties
    seq:
      - id: flags
        -orig-id: Flags
        type: u4
        enum: xpsredir_printer_properties_flags
        doc: |
          A 32-bit unsigned integer. The value of this field is used to determine the privilege
          level used to query for printer properties.
      - id: server_window
        -orig-id: hServerWindow
        type: u8
        doc: |
          A 64-bit unsigned integer. When an operating system calls into the server virtual printer
          driver to display the printer properties UI, it also supplies a WindowHandle to use as a
          parent for the UI displayed. The WindowHandle is passed to the client printer driver in
          this field.

  xpsrd_cancel_async_doc_props_req:
    doc: |
      MS-RDPEXPS 2.2.4.3.2.5 Server Cancel Async Document Properties Request (CANCEL_ASYNC_DOC_PROPS_REQ)
      This request is sent from server to client to cancel the display of a document properties UI.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_CancelAsyncDocumentProperties
      mstscax!CTSClientPrintDriver::CancelAsyncDocumentProperties
      mstscax!CTSClientPrintDriver::CancelUI
      (no data)

  xpsrd_cancel_cancel_async_printer_props_req:
    doc: |
      MS-RDPEXPS 2.2.4.3.1.5 Server Cancel Async Printer Properties Request (CANCEL_ASYNC_PRINTER_PROPS_REQ)
      This request is sent from the server to the client to cancel the display of a printer properties UI.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_CancelAsyncPrinterProperties
      mstscax!CTSClientPrintDriver::CancelAsyncPrinterProperties
      mstscax!CTSClientPrintDriver::CancelUI
      (no data)

  xpsrd_move_doc_properties_req:
    doc: |
      MS-RDPEXPS 2.2.4.3.2.7 Server Move Document Properties Window Request (MOVE_DOC_PROPERTIES_REQ)
      This request is sent from server to client to reposition the document properties UI window
      at a specific location.
      NOTE: Does nothing in AppContainer e.g. WDAG.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_MoveAsyncDocumentProperties
      mstscax!CTSClientPrintDriver::MoveAsyncDocumentProperties
      mstscax!CClientUtilsWin32::UT_IsRunningInAppContainer
    seq:
      - id: x_pos
        -orig-id: xPos
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the X axis coordinate of the window's
          target location.
      - id: y_pos
        -orig-id: yPos
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the Y axis coordinate of the window's
          target location.

  xpsrd_mxdc_getpdev_adjustment_req:
    doc: |
      MS-RDPEXPS 2.2.4.2.9 Server Get Device Adjustment Request (MXDC_GETPDEV_ADJUSTMENT_REQ)
      This server-to-client request is sent to retrieve device adjustment information.
      NOTE: Not all capability in this API is reachable by WDAG.
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Dispatch_Invoke
      mstscax!CStubITS_PrintDriver<ITS_PrintDriver>::Invoke_MxdcGetPDEVAdjustment
      mstscax!CProxyReceive_TSPRINTER_PROPERTY
      mstscax!CTSClientPrintDriver::MxdcGetPDEVAdjustment
      mstscax!CTSClientPrintDriver::VerifyDevMode
      mstscax!UnMarshalPrinterPropertiesCollection
      mstscax!CClientUtilsWin32::UT_IsRunningInAppContainer
      mstscax!MarshalPrinterPropertiesCollection
    seq:
      - id: devmode_in_size
        -orig-id: cbDevModeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of bytes in the
          pDevmodeIn field.
      - id: devmode_in
        -orig-id: pDevmodeIn
        size: devmode_in_size
        doc: |
          A DEVMODE structure sent as an array of bytes. The content is generated by the printing
          subsystem and is treated as payload in the Printer Driver Interface.
      - id: in_buffer_size
        -orig-id: cbInBuffer
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of bytes in the pInBuffer
          field.
      - id: in_buffer
        -orig-id: pInBuffer
        size: in_buffer_size
        doc: |
          An array of bytes. Opaque payload passed by the Printer Driver Interface from the printing
          subsystem to the client printer driver.
      - id: num_in_props
        -orig-id: numInProps
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of items in the pInProps field.
      - id: in_props
        -orig-id: pInProps
        type: tsprinter_property
        repeat: expr
        repeat-expr: num_in_props
        doc: |
          An array of TSPRINTER_PROPERTY elements. The content is generated by the printing subsystem
          and is treated as payload in the Printer Driver Interface.
