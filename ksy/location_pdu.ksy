meta:
  id: location_pdu
  endian: le

doc: |
  Location (Geographical co-ordinates) channel.
  Dynamic channel 'Microsoft::Windows::RDS::Location'
  NOTE: WDAG cannot open this channel (error 0xc0000001)
  mstscax!RdpLocationClientPlugin::OnNewChannelConnection
  mstscax!RdpLocationClientChannel::OnDataReceived

seq:
  - id: msg_type
    type: u2
    doc: |
      Type of data stored in message.
  - id: msg_size
    type: u4
    doc: |
      Total size of this PDU message.
  - id: msg
    type:
      switch-on: msg_type
      -name: type
      cases:
        1: location_server_ready_pdu
        2: location_client_ready_pdu
        3: location_coordinate_abs_pdu
        4: location_coordinate_rel_1_pdu
        5: location_coordinate_rel_2_pdu

types:
  location_four_byte_signed_integer:
    doc: |
      The FOUR_BYTE_SIGNED_INTEGER structure is used to encode a value in the range
      -0x1FFFFFFF to 0x1FFFFFFF by using a variable number of bytes. For example,
      -0x001A1B1C is encoded as {0xBA, 0x1B, 0x1C}, and -0x00000002 is encoded as {0x22}.
      The three most significant bits of the first byte encode the number of bytes in
      the structure and the sign.
    seq:
      - id: val1
        type: b5
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: s
        type: b1
        doc: |
          A 1-bit, unsigned integer field containing an encoded representation of whether
          the value is positive or negative.
      - id: c
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      sign:
        value: 's ? -1 : 1'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val1_shifted:
        value: (val1_u4 & 0x1f) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? (val4_u4 & 0xff) : 0'
      value:
        value: sign * (val1_shifted | val2_shifted | val3_shifted | val4_shifted)
        doc: Value in the range -0x1FFFFFFF to 0x1FFFFFFF.

  location_four_byte_signed_divided_integer:
    seq:
      - id: val1
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing the most significant 2 bits of the
          value represented by this structure.
      - id: d
        type: b3
        doc: |
          A 3-bit, unsigned integer field containing how many times to divide the signed
          integer by 10.
      - id: s
        type: b1
        doc: |
          A 1-bit, unsigned integer field containing an encoded representation of whether
          the value is positive or negative.
      - id: c
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      sign:
        value: 's ? -1 : 1'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val1_shifted:
        value: (val1_u4 & 0x3) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? (val4_u4 & 0xff) : 0'
      value:
        value: sign * (val1_shifted | val2_shifted | val3_shifted | val4_shifted)
      divide_by:
        value: 10 * d

  location_server_ready_pdu:
    doc: |
      Sent from server to client.
      mstscax!RdpLocationClientChannel::OnDataReceived
    seq:
      - id: flags
        type: u4
        doc: |
          Must be set to 0x10000. Might be a version field.
      - id: reserved
        type: u4
        doc: |
          Set to zero. Don't know what this is for.

  location_client_ready_pdu:
    doc: |
      Sent from client to server.
      mstscax!RdpLocationClientChannel::SendClientReadyPdu
    seq:
      - id: flags
        type: u4
        doc: |
          Must be set to 0x10000. Might be a version field.
      - id: reserved
        type: u4
        doc: |
          Set to zero. Don't know what this is for.

  location_coordinate_abs_pdu:
    doc: |
      Sends absolute co-ordinates client to server.
      mstscax!RdpLocationClientChannel::SendLocation3D
      mstscax!RdpLocationClientChannel::SendLocationPdu
      mstscax!RdpLocationClientChannel::EncodeBaseCoordinate
      mstscax!RdpLocationClientChannel::EncodeInt32
    seq:
      - id: x
        type: location_four_byte_signed_divided_integer
      - id: y
        type: location_four_byte_signed_divided_integer
      - id: counter
        type: location_four_byte_signed_integer

  location_coordinate_rel_1_pdu:
    doc: |
      Sends relative co-ordinates client to server.
      mstscax!RdpLocationClientChannel::SendLocation3D
      mstscax!RdpLocationClientChannel::SendLocationPdu
      mstscax!RdpLocationClientChannel::EncodeDeltaCoordinate
      mstscax!RdpLocationClientChannel::EncodeInt32
    seq:
      - id: delta_x
        type: location_four_byte_signed_integer
      - id: delta_y
        type: location_four_byte_signed_integer

  location_coordinate_rel_2_pdu:
    doc: |
      Sends co-ordinates client to server.
      mstscax!RdpLocationClientChannel::SendLocation3D
      mstscax!RdpLocationClientChannel::SendLocationPdu
      mstscax!RdpLocationClientChannel::EncodeDeltaCoordinate
      mstscax!RdpLocationClientChannel::EncodeInt32
    seq:
      - id: delta_x
        type: location_four_byte_signed_integer
      - id: delta_y
        type: location_four_byte_signed_integer
      - id: counter
        type: location_four_byte_signed_integer
