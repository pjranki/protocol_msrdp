meta:
  id: alphacodec_bitmap_stream
  endian: le

doc: |
  MS-RDPEGFX 2.2.4.3 ALPHACODEC_BITMAP_STREAM
  The ALPHACODEC_BITMAP_STREAM structure specifies the opacity of each pixel in the
  encoded bitmap. The number of pixels encoded in the segments field MUST equal the area
  of the original image when decoded.
  mstscax!AlphaDecompressor::Decompress
  mstscax!AlphaDecompressor::DecompressInternal
  mstscax!ChannelRunLengthDecode

seq:
  - id: alpha_sig
    size: 2
    # contents: [0x4c, 0x41]
    doc: |
      A 16-bit unsigned integer. This field MUST contain the value 16,716 (0x414C).
  - id: compressed
    type: u2
    doc: |
      A 16-bit unsigned integer. If this field equals 0x0000, the segments field contains
      the alpha channel values, encoded in raw format, one after the other, in top-left
      to bottom-right order. If this field is nonzero, the segments field contains one or
      more CLEARCODEC_ALPHA_RLE_SEGMENT (section 2.2.4.3.1) structures.
  - id: raw_data
    size-eos: true
    if: compressed == 0x0000
    doc: |
      Raw alpha data.
  - id: segments
    type: clearcodec_alpha_rle_segment
    repeat: eos
    if: compressed != 0x0000
    doc: |
      An optional variable-length array of bytes or CLEARCODEC_ALPHA_RLE_SEGMENT
      structures, depending on the value of the compressed field.

types:
  clearcodec_alpha_rle_segment:
    doc: |
      MS-RDPEGFX 2.2.4.3.1 CLEARCODEC_ALPHA_RLE_SEGMENT
      The CLEARCODEC_ALPHA_RLE_SEGMENT structure encodes a single alpha channel run segment.
      mstscax!ChannelRunLengthDecode
    seq:
      - id: run_value
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the alpha value of the current pixel.
      - id: run_length_factor_1
        type: u1
        doc: |
          An 8-bit unsigned integer. If the value of the runLengthFactor1 field is less than
          255 (0xFF), the runLengthFactor2 and runLengthFactor3 fields MUST NOT be present,
          and the current alpha value MUST be applied to the next runLengthFactor1 pixels.
          If the value of the runLengthFactor1 field equals 255 (0xFF), the runLengthFactor2
          field MUST be present, and the run length is calculated from the runLengthFactor2
          field.
      - id: run_length_factor_2
        type: u2
        if: run_length_factor_1 == 0xff
        doc: |
          An optional 16-bit unsigned integer. If the value of the runLengthFactor2 field is
          less than 65,535 (0xFFFF), the runLengthFactor3 field MUST NOT be present, and the
          current alpha value MUST be applied to the next runLengthFactor2 pixels. If the
          value of the runLengthFactor2 field equals 65,535 (0xFFFF), the runLengthFactor3
          field MUST be present, and the run length is calculated from the runLengthFactor3
          field.
      - id: run_length_factor_3
        type: u4
        if: (run_length_factor_1 == 0xff) and (run_length_factor_2 == 0xffff)
        doc: |
          An optional 32-bit unsigned integer. If this field is present, it contains the run
          length. The current alpha value MUST be applied to the next runLengthFactor3 pixels.
          This field SHOULD NOT be used if the run length is smaller than 65,535 (0xFFFF).
    instances:
      run_repeat_count:
        value: 'run_length_factor_1 != 0xff ? run_length_factor_1 : (run_length_factor_2 != 0xffff ? run_length_factor_2 : run_length_factor_3)'
        doc: |
          How many time the runValue repeats.
