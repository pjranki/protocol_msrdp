meta:
  id: rfx_avc420_bitmap_stream
  endian: le

doc: |
  MS-RDPEGFX 2.2.4.4 RFX_AVC420_BITMAP_STREAM
  The RFX_AVC420_BITMAP_STREAM structure encapsulates regions of a graphics frame compressed using
  the MPEG-4 AVC/H.264 codec in YUV420p mode (as specified in [ITU-H.264-201201]) and conforming to
  the byte stream format specified in [ITU-H.264-201201] Annex B. The data compressed using these
  techniques is transported in the bitmapData field of the RD PGFX_WIRE_TO_SURFACE_PDU_1 (section
  2.2.2.1) message or encapsulated in the RFX_AVC444_BITMAP_STREAM structure (section 2.2.4.5) or
  the RFX_AVC444V2_BITMAP_STREAM structure (section 2.2.4.6).
  Note that the width and height of the MPEG-4 AVC/H.264 codec bitstream MUST be aligned to a
  multiple of 16 and MUST be cropped by the region mask specified in the regionRects field that is
  embedded in the avc420MetaData field.
  mstscax!OffscreenSurface::DecodeBytesToSurfaceRegion
  mstscax!RdpSurfaceDecoder::DecodeToSurfaceTexture2D
  mstscax!RdpX_CreateObject
  mstscax!XObjectId_RdpXAvc420Decoder_CreateObject
  mstscax!HWCompatibilityDecodeTests::CheckHWDecodeCompatibility
  mstscax!Avc420Decompressor::InitializeDecoder
  mstscax!Avc420Decompressor::CreateAlphaTexture
  mstscax!Avc444Decompressor::Decompress
  mstscax!AvcDecompressor::DecompressHelper
  mstscax!Avc420Decompressor::DecompressDXVA (if DXVA enabled)
  mstscax!CMFApi::CreateSample
  mstscax!AvcSample::ConvertToContiguousBuffer
  mstscax!AvcMediaBuffer::Lock
  mstscax!Avc420Decompressor::DecompressCPUTexture (if no byte array interface found)
  mstscax!Avc420Decompressor::DecompressCPUByteArray (if byte array interface found)
  mstscax!Avc420Decompressor::DecompressCPUCommon (also called by AVC444v1 and AVC444v2)
  mstscax!AvcDecoder::DecompressCPUMF (also called by AVC444v1 and AVC444v2)
  mstscax!AvcDecoder::DecompressCPU (also called by AVC444v1 and AVC444v2)
  mstscax!Avc420Decompressor::GetRectangles

seq:
  - id: avc420_meta_data
    type: rfx_avc420_metablock
    doc: |
      A variable-length RFX_AVC420_METABLOCK (section 2.2.4.4.1) structure.
  - id: avc420_encoded_bitstream
    size-eos: true
    doc: |
      An array of bytes that represents a single frame encoded using the MPEG-4 AVC/H.264 codec in
      YUV420p mode (as specified in [ITU-H.264-201201]) and conforming to the byte stream format
      specified in [ITU-H.264-201201] Annex B. Color conversion is described in section 3.3.8.3.1.
      mstscax!AvcSoftwareDecoder::DecodeOneFrame (software decoder)
      msmpeg2vdec!*::ProcessInput (no symbols provided by microsoft)

types:
  rfx_avc420_rect16:
    doc: |
      MS-RDPEGFX 2.2.1.2 RDPGFX_RECT16
      The RDPGFX_RECT16 structure specifies a rectangle relative to the origin of a target
      surface using exclusive coordinates (the right and bottom bounds are not included in
      the rectangle).
    seq:
      - id: left
        type: u2
        doc: A 16-bit unsigned integer that specifies the leftmost bound of the rectangle.
      - id: top
        type: u2
        doc: A 16-bit unsigned integer that specifies the upper bound of the rectangle.
      - id: right
        type: u2
        doc: A 16-bit unsigned integer that specifies the rightmost bound of the rectangle.
      - id: bottom
        type: u2
        doc: A 16-bit unsigned integer that specifies the lower bound of the rectangle.

  rfx_avc420_quant_quality:
    doc: |
      MS-RDPEGFX 2.2.4.4.2 RDPGFX_AVC420_QUANT_QUALITY
      The RDPGFX_AVC420_QUANT_QUALITY structure describes the quantization parameter and quality
      level associated with a rectangular region.
    seq:
      - id: progressively_encoded
        type: b1
        doc: |
          A 1-bit field that indicates whether a rectangular region is progressively encoded.
          A value of 0x1 indicates that the region is progressively encoded.
      - id: reserved
        type: b1
        doc: |
          A 1-bit field that is reserved for future use. This field SHOULD be set to zero.
      - id: quantization_parameter
        type: b6
        doc: |
          A 6-bit, unsigned integer that that specifies the quantization parameter associated with
          a rectangular region. This value MUST be in the range required by [ITU-H.264-201201]
          sections 7.4.2.1.1 and 7.4.3 for high profiles ([ITU-H.264-201201] section A.2.4).
      - id: quality_value
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the quality level associated with a rectangular
          region. This value MUST be in the range 0 to 100 inclusive.

  rfx_avc420_metablock:
    doc: |
      MS-RDPEGFX 2.2.4.4.1 RFX_AVC420_METABLOCK
      The RFX_AVC420_METABLOCK structure describes metadata associated with MPEG-4 AVC/H.264 encoded
      data sent from the server to the client. The data contained within the RFX_AVC420_METABLOCK
      structure is purely informational and SHOULD NOT be used by the client when decoding the MPEG-4
      AVC/H.264 stream. When decoding the stream, the data that is available in the
      RFX_AVC420_METABLOCK is present within the MPEG-4 AVC/H.264 stream.
      mstscax!AvcDecoder::DecodeHeader
    seq:
      - id: num_region_rects
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the total number of elements in the regionRects field.
          The quantQualityVals field MUST contain the same number of elements as the regionRects field.
      - id: region_rects
        type: rfx_avc420_rect16
        repeat: expr
        repeat-expr: num_region_rects
        doc: |
          A variable-length array of RDPGFX_RECT16 (section 2.2.1.2) structures that specifies the region
          mask to apply to the MPEG-4 AVC/H.264 encoded data. The total number of elements in this field
          is specified by the numRegionRects field.
      - id: quant_quality_vals
        type: rfx_avc420_quant_quality
        repeat: expr
        repeat-expr: num_region_rects
        doc: |
          A variable-length array of RDPGFX_AVC420_QUANT_QUALITY (section 2.2.4.4.2) structures that
          describes the quantization parameter and quality level associated with each rectangle in the
          regionRects field. The total number of elements in this field is specified by the numRegionRects
          field.
