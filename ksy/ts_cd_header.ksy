meta:
  id: ts_cd_header
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.2.3 Compressed Data Header (TS_CD_HEADER)
  The TS_CD_HEADER structure is used to describe compressed bitmap data.
  mstscax!Planar::BD_DecompressBitmap

seq:
  - id: comp_first_row_size
    type: u2
    doc: |
      A 16-bit, unsigned integer. The field MUST be set to 0x0000.
  - id: comp_main_body_size
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the compressed
      bitmap data (which follows this header).
  - id: scan_width
    type: u2
    doc: |
      A 16-bit, unsigned integer. The width of the bitmap (which follows
      this header) in pixels (this value MUST be divisible by 4).
  - id: uncompressed_size
    type: u2
    doc: |
      A 16-bit, unsigned integer. The size in bytes of the bitmap data
      (which follows this header) after it has been decompressed.
