meta:
  id: slowpath_control_pdu
  endian: le
  imports:
    - rdp_server_redirection_packet
    - slowpath_data_pdu

doc: |
  MS-RDPBCGR 2.2.8.1.1 Slow-Path (T.128) Formats
  MS-RDPBCGR 2.2.8.1.1.1.1 Share Control Header (TS_SHARECONTROLHEADER)
  T128 8.3 ASPDU formats
  The TS_SHARECONTROLHEADER header is a T.128 header ([T128] section 8.3).
  Messages are sent over a static channel.
  The channel ID is set in the CSL._SL.channelID member of the CSL class.
  mstscax!CCO::OnPacketReceived

seq:
  - id: pdu_type_version
    type: u2
  - id: pdu_source
    type: u2
    doc: |
      A 16-bit unsigned integer. The channel ID that is the transmission source of the PDU.
  - id: data
    type:
      switch-on: pdu_type.to_i
      -name: type
      cases:
        'pdutype::demandactivepdu.to_i': ts_demand_active_pdu
        'pdutype::confirmactivepdu.to_i': ts_confirm_active_pdu
        'pdutype::deactivateallpdu.to_i': ts_deactivate_all_pdu
        'pdutype::server_redir_pkt.to_i': rdp_server_redirection_packet
        'pdutype::datapdu.to_i': slowpath_data_pdu

instances:
  pdu_type:
    value: pdu_type_version & 0x000f
    enum: pdutype
    doc: |
      A 4-bit unsigned integer that specifies the PDU type.
  pdu_version:
    value: (pdu_type_version >> 4) & 0x0fff
    doc: |
      A 12-bit unsigned integer that specifies the PDU version.

enums:
  pdutype:
    0x1:
      id: demandactivepdu
      doc: Demand Active PDU (section 2.2.1.13.1).
    0x3:
      id: confirmactivepdu
      doc: Confirm Active PDU (section 2.2.1.13.2).
    0x6:
      id: deactivateallpdu
      doc: Deactivate All PDU (section 2.2.3.1).
    0x7:
      id: datapdu
      doc: |
        Data PDU (actual type is revealed by the pduType2 field in
        the Share Data Header (section 2.2.8.1.1.1.2) structure).
    0xA:
      id: server_redir_pkt
      doc: Enhanced Security Server Redirection PDU (section 2.2.13.3.1).

types:
  ts_demand_active_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.13.1.1 Demand Active PDU Data (TS_DEMAND_ACTIVE_PDU)
      T128 8.4.1 ASCE activation (legacy mode)
      The TS_DEMAND_ACTIVE_PDU structure is a standard T.128 Demand Active PDU ([T128] section 8.4.1).
      The Demand Active PDU is an RDP Connection Sequence PDU sent from server to client
      during the Capabilities Exchange phase of the RDP Connection Sequence
      (see section 1.3.1.1 for an overview of the RDP Connection Sequence phases). It is
      sent upon successful completion of the Licensing phase of the
      RDP Connection Sequence.
      In normal (connected) operation, the client will error (See notes on CoreFSM state machine).
      Client needs to be in state CoreFSM state CC_WAITINGFORDEMANDACTIVE.
      mstscax!CTSConnectionHandler::OnConfiguringRemoteSession
      mstscax!CoreFSM::CC_OnDemandActivePDU
      mstscax!CoreFSM::CCFSMProc
      mstscax!ExecuteCCFSM
      mstscax!CoreFSM::OnDemandActive
      mstscax!CoreFSM::CCShareStart
      mstscax!CCoreCapabilitiesManager::StoreDemandActiveData
      mstscax!CCoreCapabilitiesManager::VerifyCombinedCapsFromNetwork
    seq:
      - id: share_id
        -orig-id: shareId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The share identifier for the packet ([T128] section 8.4.2
          for more information regarding share IDs).
      - id: length_source_descriptor
        -orig-id: lengthSourceDescriptor
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the sourceDescriptor field.
      - id: length_combined_capabilities
        -orig-id: lengthCombinedCapabilities
        type: u2
        doc: |
          A 16-bit, unsigned integer. The combined size in bytes of the numberCapabilities,
          pad2Octets, and capabilitySets fields.
      - id: source_descriptor
        -orig-id: sourceDescriptor
        size: length_source_descriptor
        doc: |
          A variable-length array of bytes containing a source descriptor
          (see [T128] section 8.4.1 for more information regarding source descriptors).
      - id: combined_capabilities
        size: length_combined_capabilities
        type: ts_caps_sets
        if: length_combined_capabilities > 4
        doc: |
          This parameter is a list of this ASCE's combined capabilities, which
          contains one copy of each of the legacy mode capability sets in any order.
          See T.128 8.2 for further information on capabilities.
      - id: session_id
        -orig-id: sessionId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The session identifier. This field is ignored by the client.

  ts_caps_sets:
    doc: |
      A collection of TS_CAPS_SET(s).
    seq:
      - id: number_capabilities
        -orig-id: numberCapabilities
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of capability sets.
      - id: pad
        -orig-id: pad2Octets
        type: u2
        doc: |
          A 16-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: capability_sets
        -orig-id: capabilitySets
        type: ts_caps_set
        repeat: expr
        repeat-expr: number_capabilities
        doc: |
          An array of Capability Set (section 2.2.1.13.1.1.1) structures.
          The number of capability sets is specified by the numberCapabilities field.

  ts_caps_set:
    doc: |
      MS-RDPBCGR 2.2.1.13.1.1.1 Capability Set (TS_CAPS_SET)
      The TS_CAPS_SET structure is used to describe the type and size of a capability set
      exchanged between clients and servers. All capability sets conform to this basic
      structure (section 2.2.7).
    seq:
      - id: capability_set_type
        -orig-id: capabilitySetType
        type: u2
        enum: capstype
        doc: |
          A 16-bit, unsigned integer. The type identifier of the capability set.
      - id: length_capability
        -orig-id: lengthCapability
        type: u2
        doc: |
          A 16-bit, unsigned integer. The length in bytes of the capability data, including
          the size of the capabilitySetType and lengthCapability fields.
      - id: capability_data
        -orig-id: capabilityData
        size: length_capability
        doc: |
          Capability set data which conforms to the structure of the type given by the
          capabilitySetType field.
    enums:
      capstype:
        0x0001:
          id: general
          doc: General Capability Set (section 2.2.7.1.1)
        0x0002:
          id: bitmap
          doc: Bitmap Capability Set (section 2.2.7.1.2)
        0x0003:
          id: order
          doc: Order Capability Set (section 2.2.7.1.3)
        0x0004:
          id: bitmapcache
          doc: Revision 1 Bitmap Cache Capability Set (section 2.2.7.1.4.1)
        0x0005:
          id: control
          doc: Control Capability Set (section 2.2.7.2.2)
        0x0007:
          id: activation
          doc: Window Activation Capability Set (section 2.2.7.2.3)
        0x0008:
          id: pointer
          doc: Pointer Capability Set (section 2.2.7.1.5)
        0x0009:
          id: share
          doc: Share Capability Set (section 2.2.7.2.4)
        0x000A:
          id: colorcache
          doc: Color Table Cache Capability Set ([MS-RDPEGDI] section 2.2.1.1)
        0x000C:
          id: sound
          doc: Sound Capability Set (section 2.2.7.1.11)
        0x000D:
          id: input
          doc: Input Capability Set (section 2.2.7.1.6)
        0x000E:
          id: font
          doc: Font Capability Set (section 2.2.7.2.5)
        0x000F:
          id: brush
          doc: Brush Capability Set (section 2.2.7.1.7)
        0x0010:
          id: glyphcache
          doc: Glyph Cache Capability Set (section 2.2.7.1.8)
        0x0011:
          id: offscreencache
          doc: Offscreen Bitmap Cache Capability Set (section 2.2.7.1.9)
        0x0012:
          id: bitmapcache_hostsupport
          doc: Bitmap Cache Host Support Capability Set (section 2.2.7.2.1)
        0x0013:
          id: bitmapcache_rev2
          doc: Revision 2 Bitmap Cache Capability Set (section 2.2.7.1.4.2)
        0x0014:
          id: virtualchannel
          doc: Virtual Channel Capability Set (section 2.2.7.1.10)
        0x0015:
          id: drawninegridcache
          doc: DrawNineGrid Cache Capability Set ([MS-RDPEGDI] section 2.2.1.2)
        0x0016:
          id: drawgdiplus
          doc: Draw GDI+ Cache Capability Set ([MS-RDPEGDI] section 2.2.1.3)
        0x0017:
          id: rail
          doc: Remote Programs Capability Set ([MS-RDPERP] section 2.2.1.1.1)
        0x0018:
          id: window
          doc: Window List Capability Set ([MS-RDPERP] section 2.2.1.1.2)
        0x0019:
          id: compdesk
          doc: Desktop Composition Extension Capability Set (section 2.2.7.2.8)
        0x001A:
          id: multifragmentupdate
          doc: Multifragment Update Capability Set (section 2.2.7.2.6)
        0x001B:
          id: large_pointer
          doc: Large Pointer Capability Set (section 2.2.7.2.7)
        0x001C:
          id: surface_commands
          doc: Surface Commands Capability Set (section 2.2.7.2.9)
        0x001D:
          id: bitmap_codecs
          doc: Bitmap Codecs Capability Set (section 2.2.7.2.10)
        0x001E:
          id: frame_acknowledge
          doc: Frame Acknowledge Capability Set ([MS-RDPRFX] section 2.2.1.3)

  ts_confirm_active_pdu:
    doc: |
      MS-RDPBCGR 2.2.1.13.2.1 Confirm Active PDU Data (TS_CONFIRM_ACTIVE_PDU)
      The Confirm Active PDU is an RDP Connection Sequence PDU sent from client to server
      during the Capabilities Exchange phase of the RDP Connection Sequence
      (see section 1.3.1.1 for an overview of the RDP Connection Sequence phases). It is
      sent as a response to the Demand Active PDU (section 2.2.1.13.1). Once the Confirm
      Active PDU has been sent, the client can start sending input PDUs (section 2.2.8)
      to the server.
    seq:
      - id: share_id
        -orig-id: shareId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The share identifier for the packet
          (see [T128] section 8.4.2 for more information regarding share IDs).
      - id: originator_id
        -orig-id: originatorId
        type: u2
        doc: |
          A 16-bit, unsigned integer. The identifier of the packet originator.
          This field MUST be set to the server channel ID (0x03EA).
      - id: length_source_descriptor
        -orig-id: lengthSourceDescriptor
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the sourceDescriptor field.
      - id: length_combined_capabilities
        -orig-id: lengthCombinedCapabilities
        type: u2
        doc: |
          A 16-bit, unsigned integer. The combined size in bytes of the numberCapabilities,
          pad2Octets, and capabilitySets fields.
      - id: source_descriptor
        -orig-id: sourceDescriptor
        size: length_source_descriptor
        doc: |
          A variable-length array of bytes containing a source descriptor
          (see [T128] section 8.4.1 for more information regarding source descriptors).
      - id: combined_capabilities
        size: length_combined_capabilities
        type: ts_caps_sets
        if: length_combined_capabilities > 4
        doc: |
          This parameter is a list of this ASCE's combined capabilities, which
          contains one copy of each of the legacy mode capability sets in any order.
          See T.128 8.2 for further information on capabilities.

  ts_deactivate_all_pdu:
    doc: |
      MS-RDPBCGR 2.2.3.1.1 Deactivate All PDU Data (TS_DEACTIVATE_ALL_PDU)
      The Deactivate All PDU is sent from server to client to indicate that the connection
      will be dropped or that a capability re-exchange using a Deactivation-Reactivation
      Sequence will occur
      (see section 1.3.1.3 for an overview of the Deactivation-Reactivation Sequence).
      mstscax!CoreFSM::CC_Event
      mstscax!CoreFSM::CCFSMProc
      mstscax!ExecuteCCFSM
      mstscax!CoreFSM::CCEnableShareSendCmpnts
      mstscax!CCM::CM_Disable
      mstscax!CCM::CM_Disable
      mstscax!CTSCoreGraphics::Disable
      mstscax!CoreFSM::CCShareEnd
      mstscax!CTSConnectionHandler::OnDeactivateAllPDU
    seq:
      - id: share_id
        -orig-id: shareId
        type: u4
        doc: |
          A 32-bit, unsigned integer. The share identifier for the packet
          (see [T128] section 8.4.2 for more information regarding share IDs).
      - id: length_source_descriptor
        -orig-id: lengthSourceDescriptor
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the sourceDescriptor field.
      - id: source_descriptor
        -orig-id: sourceDescriptor
        size: length_source_descriptor
        doc: |
          Variable number of bytes. The source descriptor. This field SHOULD be set to 0x00.
