meta:
  id: rdpinput_pdu
  endian: le

doc: |
  MS-RDPEI 2.2.2.6 RDPINPUT_HEADER
  The RDPINPUT_HEADER structure is included in all input event PDUs and is used to
  identify the input event type and to specify the length of the PDU.
  Dynamic channel "Microsoft::Windows::RDS::Input".
  mstscax!RdpInputClientPlugin::OnNewChannelConnection
  mstscax!RdpInputClientChannel::OnDataReceived

seq:
  - id: event_id
    -orig-id: eventId
    type: u2
    enum: eventid
    doc: |
      A 16-bit unsigned integer that identifies the type of the input event PDU.
  - id: pdu_length
    -orig-id: pduLength
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the length of the input event PDU in bytes.
      This value MUST include the length of the RDPINPUT_HEADER (6 bytes).
  - id: data
    size: 'pdu_length >= 6 ? pdu_length - 6 : 0'
    type:
      switch-on: event_id
      -name: type
      cases:
        'eventid::sc_ready': rdpinput_sc_ready_pdu
        'eventid::cs_ready': rdpinput_cs_ready_pdu
        'eventid::touch': rdpinput_touch_event_pdu
        'eventid::suspend_input': rdpinput_suspend_input_pdu
        'eventid::resume_input': rdpinput_resume_input_pdu
        'eventid::dismiss_hovering_touch_contact': rdpinput_dismiss_hovering_touch_contact_pdu
        'eventid::pen': rdpinput_pen_event_pdu

enums:
  eventid:
    0x0001:
      id: sc_ready
      doc: RDPINPUT_SC_READY_PDU (section 2.2.3.1).
    0x0002:
      id: cs_ready
      doc: RDPINPUT_CS_READY_PDU (section 2.2.3.2).
    0x0003:
      id: touch
      doc: RDPINPUT_TOUCH_EVENT_PDU (section 2.2.3.3).
    0x0004:
      id: suspend_input
      doc: RDPINPUT_SUSPEND_INPUT_PDU (section 2.2.3.4).
    0x0005:
      id: resume_input
      doc: RDPINPUT_RESUME_INPUT_PDU (section 2.2.3.5).
    0x0006:
      id: dismiss_hovering_touch_contact
      doc: RDPINPUT_DISMISS_HOVERING_TOUCH_CONTACT_PDU (section 2.2.3.6).
    0x0008:
      id: pen
      doc: RDPINPUT_PEN_EVENT_PDU (section 2.2.3.7).

types:
  rdpinput_two_byte_unsigned_integer:
    doc: |
      MS-RDPEI 2.2.2.1 TWO_BYTE_UNSIGNED_INTEGER
      The TWO_BYTE_UNSIGNED_INTEGER structure is used to encode a value in the range 0x0000 to 0x7FFF
      by using a variable number of bytes. For example, 0x1A1B is encoded as { 0x9A, 0x1B }. The most
      significant bit of the first byte encodes the number of bytes in the structure.
    seq:
      - id: val1
        type: b7
        doc: |
          A 7-bit, unsigned integer field containing the most significant 7 bits of the
          value represented by this structure.
      - id: c
        type: b1
        doc: |
          A 1-bit, unsigned integer field that contains an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: two_byte_encoding
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      two_byte_encoding:
        value: 'c ? true : false'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      value:
        value: 'two_byte_encoding ? (((val1_u4 << 8) & 0x7F00) | (val2_u4 & 0x00ff)) : (val1_u4 & 0x007f)'
        doc: Value in the range 0x0000 to 0x7FFF.

  rdpinput_two_byte_signed_integer:
    doc: |
      MS-RDPEI 2.2.2.2 TWO_BYTE_SIGNED_INTEGER
      The TWO_BYTE_SIGNED_INTEGER structure is used to encode a value in the range -0x3FFF to 0x3FFF by
      using a variable number of bytes. For example, -0x1A1B is encoded as { 0xDA, 0x1B }, and -0x0002 is
      encoded as { 0x42 }. The most significant bits of the first byte encode the number of bytes in the
      structure and the sign.
    seq:
      - id: val1
        type: b6
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: s
        type: b1
        doc: |
          A 1-bit, unsigned integer field containing an encoded representation of whether
          the value is positive or negative.
      - id: c
        type: b1
        doc: |
          A 1-bit, unsigned integer field that contains an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: two_byte_encoding
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      two_byte_encoding:
        value: 'c ? true : false'
      sign:
        value: 's ? -1 : 1'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      value:
        value: 'sign * (two_byte_encoding ? (((val1_u4 << 8) & 0x3F00) | (val2_u4 & 0x00ff)) : (val1_u4 & 0x003f))'
        doc: Value in the range -0x3FFF to 0x3FFF.

  rdpinput_four_byte_unsigned_integer:
    doc: |
      MS-RDPEI 2.2.2.3 FOUR_BYTE_UNSIGNED_INTEGER
      The FOUR_BYTE_UNSIGNED_INTEGER structure is used to encode a value in the range
      0x00000000 to 0x3FFFFFFF by using a variable number of bytes. For example, 0x001A1B1C
      is encoded as {0x9A, 0x1B, 0x1C}. The two most significant bits of the first byte
      encode the number of bytes in the structure.
    seq:
      - id: val1
        type: b6
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: c
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val1_shifted:
        value: (val1_u4 & 0x3f) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? (val4_u4 & 0xff) : 0'
      value:
        value: val1_shifted | val2_shifted | val3_shifted | val4_shifted
        doc: Value in the range 0x00000000 to 0x3FFFFFFF.

  rdpinput_four_byte_signed_integer:
    doc: |
      MS-RDPEI 2.2.2.4 FOUR_BYTE_SIGNED_INTEGER
      The FOUR_BYTE_SIGNED_INTEGER structure is used to encode a value in the range
      -0x1FFFFFFF to 0x1FFFFFFF by using a variable number of bytes. For example,
      -0x001A1B1C is encoded as {0xBA, 0x1B, 0x1C}, and -0x00000002 is encoded as {0x22}.
      The three most significant bits of the first byte encode the number of bytes in
      the structure and the sign.
    seq:
      - id: val1
        type: b5
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: s
        type: b1
        doc: |
          A 1-bit, unsigned integer field containing an encoded representation of whether
          the value is positive or negative.
      - id: c
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      sign:
        value: 's ? -1 : 1'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val1_shifted:
        value: (val1_u4 & 0x1f) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? (val4_u4 & 0xff) : 0'
      value:
        value: sign * (val1_shifted | val2_shifted | val3_shifted | val4_shifted)
        doc: Value in the range -0x1FFFFFFF to 0x1FFFFFFF.

  rdpinput_eight_byte_unsigned_integer:
    doc: |
      MS-RDPEI 2.2.2.5 EIGHT_BYTE_UNSIGNED_INTEGER
      The EIGHT_BYTE_UNSIGNED_INTEGER structure is used to encode a value in the range
      0x0000000000000000 to 0x1FFFFFFFFFFFFFFF by using a variable number of bytes.
      For example, 0x001A1B1C1D1E1F2A is encoded as {0xDA, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x2A}.
      The three most significant bits of the first byte encode the number of bytes in the structure.
    seq:
      - id: val1
        type: b5
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: c
        type: b3
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the fourth most significant bits of the
          value represented by this structure.
      - id: val5
        type: u1
        if: c > 3
        doc: |
          An 8-bit, unsigned integer containing the fifth most significant bits of the
          value represented by this structure.
      - id: val6
        type: u1
        if: c > 4
        doc: |
          An 8-bit, unsigned integer containing the sixth most significant bits of the
          value represented by this structure.
      - id: val7
        type: u1
        if: c > 5
        doc: |
          An 8-bit, unsigned integer containing the seventh most significant bits of the
          value represented by this structure.
      - id: val8
        type: u1
        if: c > 6
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val5_u4:
        value: val5 << 0
      val6_u4:
        value: val6 << 0
      val7_u4:
        value: val7 << 0
      val8_u4:
        value: val8 << 0
      val1_shifted:
        value: (val1_u4 & 0x3f) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? ((val4_u4 & 0xff) << (8 * (c - 3))) : 0'
      val5_shifted:
        value: 'c > 3 ? ((val5_u4 & 0xff) << (8 * (c - 4))) : 0'
      val6_shifted:
        value: 'c > 4 ? ((val6_u4 & 0xff) << (8 * (c - 5))) : 0'
      val7_shifted:
        value: 'c > 5 ? ((val7_u4 & 0xff) << (8 * (c - 6))) : 0'
      val8_shifted:
        value: 'c > 6 ? (val8_u4 & 0xff) : 0'
      value:
        value: val1_shifted | val2_shifted | val3_shifted | val4_shifted | val5_shifted | val6_shifted | val7_shifted | val8_shifted
        doc: Value in the range 0x00000000 to 0x3FFFFFFF.

  rdpinput_sc_ready_pdu:
    doc: |
      MS-RDPEI 2.2.3.1 RDPINPUT_SC_READY_PDU
      The RDPINPUT_SC_READY_PDU message is sent by the server endpoint and is used
      to indicate readiness to commence with input remoting transactions.
      mstscax!RdpInputClientChannel::OnDataReceived
    seq:
      - id: protocol_version
        -orig-id: protocolVersion
        type: u4
        enum: rdpinput_protocol
        doc: |
          A 32-bit unsigned integer that specifies the input protocol version supported
          by the server.
      - id: supported_features
        -orig-id: supportedFeatures
        type: u4
        if: 'protocol_version == rdpinput_protocol::v300'
        doc: |
          An optional 32-bit unsigned integer that specifies granular protocol features
          supported by the server.
    enums:
      rdpinput_protocol:
        0x00010000:
          id: v100
          doc: |
            Version 1.0.0 of the RDP input remoting protocol. Servers advertising this
            version only support the remoting of multitouch frames.
        0x00010001:
          id: v101
          doc: |
            Version 1.0.1 of the RDP input remoting protocol. Servers advertising this
            version only support the remoting of multitouch frames.
        0x00020000:
          id: v200
          doc: |
            Version 2.0.0 of the RDP input remoting protocol. Servers advertising this
            version support the remoting of both multitouch and pen frames.
        0x00030000:
          id: v300
          doc: |
            Version 3.0.0 of the RDP input remoting protocol. Servers advertising this
            version support specifying granular protocol features in the optional
            supportedFeatures field.
      sc_ready:
        0x00000001:
          id: multipen_injection_supported
          doc: |
            The simultaneous injection of input from up to four pen devices is supported.

  rdpinput_cs_ready_pdu:
    doc: |
      MS-RDPEI 2.2.3.2 RDPINPUT_CS_READY_PDU
      The RDPINPUT_CS_READY_PDU message is sent by the client endpoint and is used to indicate
      readiness to commence with input remoting transactions.
      mstscax!RdpInputClientChannel::SendCSReadyPdu
    seq:
      - id: flags
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies touch initialization flags.
      - id: protocol_version
        -orig-id: protocolVersion
        type: u4
        enum: rdpinput_protocol
        doc: |
          A 32-bit unsigned integer that specifies the input protocol version supported by the
          client.
      - id: max_touch_contacts
        -orig-id: maxTouchContacts
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the maximum number of simultaneous touch
          contacts supported by the client.
    enums:
      cs_ready_flags:
        0x00000001:
          id: show_touch_visuals
          doc: |
            Touch gesture and contact visuals SHOULD be rendered by the server in the remote
            session.
        0x00000002:
          id: disable_timestamp_injection
          doc: |
            The client does not support touch frame timestamp remoting. The server MUST ignore
            any values specified in the frameOffset field of the RDPINPUT_TOUCH_FRAME
            (section 2.2.3.3.1) structure and the encodeTime field of the RDPINPUT_TOUCH_EVENT_PDU
            (section 2.2.3.3) message.
            This flag SHOULD NOT be sent to a server that only supports version 1.0.0 of the input
            remoting protocol. The server-supported version is specified in the protocolVersion
            field of the RDPINPUT_SC_READY_PDU (section 2.2.3.1) message.
        0x00000004:
          id: enable_multipen_injection
          doc: |
            The server should configure the pen injection subsystem so that the simultaneous
            injection of input from up to four pen devices is possible.
      rdpinput_protocol:
        0x00010000:
          id: v100
          doc: |
            Version 1.0.0 of the RDP input remoting protocol. Clients advertising this version only
            support the remoting of multitouch frames.
        0x00010001:
          id: v101
          doc: |
            Version 1.0.1 of the RDP input remoting protocol. Clients advertising this version only
            support the remoting of multitouch frames.
        0x00020000:
          id: v200
          doc: |
            Version 2.0.0 of the RDP input remoting protocol. Clients advertising this version support
            the remoting of both multitouch and pen frames.
        0x00030000:
          id: v300
          doc: |
            Version 3.0.0 of the RDP input remoting protocol. Clients advertising this version support
            reading protocol features advertised in the supportedFeatures field of the
            RDPINPUT_SC_READY_PDU (section 2.2.3.1) structure.

  rdpinput_touch_event_pdu:
    doc: |
      MS-RDPEI 2.2.3.3 RDPINPUT_TOUCH_EVENT_PDU
      The RDPINPUT_TOUCH_EVENT_PDU message is sent by the client endpoint and is used to remote
      a collection of touch frames.
      (not reachable in WDAG RDP client)
    seq:
      - id: encode_time
        -orig-id: encodeTime
        type: rdpinput_four_byte_unsigned_integer
        doc: |
          A FOUR_BYTE_UNSIGNED_INTEGER (section 2.2.2.3) structure that specifies the time that has
          elapsed (in milliseconds) from when the oldest touch frame was generated to when it was encoded
          for transmission by the client.
      - id: frame_count
        -orig-id: frameCount
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the number of
          RDPINPUT_TOUCH_FRAME (section 2.2.3.3.1) structures in the frames field.
      - id: frames
        type: rdpinput_touch_frame
        repeat: expr
        repeat-expr: frame_count.value
        doc: |
          An array of RDPINPUT_TOUCH_FRAME structures ordered from the oldest in time to the most recent
          in time. The number of structures in this array is specified by the frameCount field.

  rdpinput_touch_frame:
    doc: |
      MS-RDPEI 2.2.3.3.1 RDPINPUT_TOUCH_FRAME
      The RDPINPUT_TOUCH_FRAME structure encapsulates a collection of RDPINPUT_TOUCH_CONTACT
      (section 2.2.3.3.1.1) structures that are part of the same logical touch frame.
    seq:
      - id: contact_count
        -orig-id: contactCount
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the number of
          RDPINPUT_TOUCH_CONTACT structures in the contacts field.
      - id: frame_offset
        -orig-id: frameOffset
        type: rdpinput_eight_byte_unsigned_integer
        doc: |
          An EIGHT_BYTE_UNSIGNED_INTEGER (section 2.2.2.5) structure that specifies the time offset from
          the previous frame (in microseconds). If this is the first frame being transmitted then this
          field MUST be set to zero.
      - id: contacts
        type: rdpinput_touch_contact
        repeat: expr
        repeat-expr: contact_count.value
        doc: |
          An array of RDPINPUT_TOUCH_CONTACT structures. The number of structures in this array is
          specified by the contactCount field.

  rdpinput_touch_contact:
    doc: |
      MS-RDPEI 2.2.3.3.1.1 RDPINPUT_TOUCH_CONTACT
      The RDPINPUT_TOUCH_CONTACT structure describes the characteristics of a contact that is encapsulated
      in an RDPINPUT_TOUCH_FRAME (section 2.2.3.3.1) structure.
    seq:
      - id: contact_id
        -orig-id: contactId
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the ID assigned to the contact.
      - id: fields_present
        -orig-id: fieldsPresent
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the presence of the optional
          contactRectLeft, contactRectTop, contactRectRight, contactRectBottom, orientation, and pressure
          fields.
      - id: x
        type: rdpinput_four_byte_signed_integer
        doc: |
          A FOUR_BYTE_SIGNED_INTEGER (section 2.2.2.4) structure that specifies the X-coordinate
          (relative to the virtual-desktop origin) of the contact.
      - id: y
        type: rdpinput_four_byte_signed_integer
        doc: |
          A FOUR_BYTE_SIGNED_INTEGER structure that specifies the Y-coordinate
          (relative to the virtual-desktop origin) of the contact.
      - id: contact_flags
        -orig-id: contactFlags
        type: rdpinput_four_byte_unsigned_integer
        doc: |
          A FOUR_BYTE_UNSIGNED_INTEGER (section 2.2.2.3) structure that specifies the current state of the
          contact.
      - id: contact_rect_left
        -orig-id: contactRectLeft
        type: rdpinput_two_byte_signed_integer
        if: (fields_present.value & 0x0001) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER (section 2.2.2.2) structure that specifies the leftmost bound
          (relative to the contact point specified by the x and y fields) of the exclusive rectangle describing
          the geometry of the contact. This rectangle MUST be rotated counter-clockwise by the angle specified
          in the orientation field to yield the actual contact geometry. The presence of the contactRectLeft
          field is indicated by the TOUCH_CONTACT_CONTACTRECT_PRESENT (0x0001) flag in the fieldsPresent field.
      - id: contact_rect_top
        -orig-id: contactRectTop
        type: rdpinput_two_byte_signed_integer
        if: (fields_present.value & 0x0001) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER structure that specifies the upper bound (relative to the contact
          point specified by the x and y fields) of the exclusive rectangle describing the geometry of the contact.
          This rectangle MUST be rotated counter-clockwise by the angle specified in the orientation field to
          yield the actual contact geometry. The presence of the contactRectTop field is indicated by the
          TOUCH_CONTACT_CONTACTRECT_PRESENT (0x0001) flag in the fieldsPresent field.
      - id: contact_rect_right
        -orig-id: contactRectRight
        type: rdpinput_two_byte_signed_integer
        if: (fields_present.value & 0x0001) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER structure that specifies the rightmost bound (relative to the
          contact point specified by the x and y fields) of the exclusive rectangle describing the geometry of
          the contact. This rectangle MUST be rotated counter-clockwise by the angle specified in the orientation
          field to yield the actual contact geometry. The presence of the contactRectRight field is indicated by
          the TOUCH_CONTACT_CONTACTRECT_PRESENT (0x0001) flag in the fieldsPresent field.
      - id: contact_rect_bottom
        -orig-id: contactRectBottom
        type: rdpinput_two_byte_signed_integer
        if: (fields_present.value & 0x0001) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER structure that specifies the lower bound (relative to the contact
          point specified by the x and y fields) of the exclusive rectangle describing the geometry of the contact.
          This rectangle MUST be rotated counter-clockwise by the angle specified in the orientation field to yield
          the actual contact geometry. The presence of the contactRectBottom field is indicated by the
          TOUCH_CONTACT_CONTACTRECT_PRESENT (0x0001) flag in the fieldsPresent field.
      - id: orientation
        type: rdpinput_four_byte_unsigned_integer
        if: (fields_present.value & 0x0002) != 0
        doc: |
          An optional FOUR_BYTE_UNSIGNED_INTEGER structure that specifies the angle through which the contact
          rectangle (specified in the contactRectLeft, contactRectTop, contactRectRight and contactRectBottom
          fields) MUST be rotated to yield the actual contact geometry. This value MUST be in the range
          0x00000000 to 0x00000167 (359), inclusive, where 0x00000000 indicates a touch contact aligned with the
          y-axis and pointing from bottom to top; increasing values indicate degrees of rotation in a
          counter-clockwise direction. The presence of the orientation field is indicated by the
          TOUCH_CONTACT_ORIENTATION_PRESENT (0x0002) flag in the fieldsPresent field. If the orientation field
          is not present the angle of rotation MUST be assumed to be zero degrees.
      - id: pressure
        type: rdpinput_four_byte_unsigned_integer
        if: (fields_present.value & 0x0004) != 0
        doc: |
          An optional FOUR_BYTE_UNSIGNED_INTEGER structure that specifies the contact pressure. This value
          MUST be normalized in the range 0x00000000 to 0x00000400 (1024), inclusive. The presence of this
          field is indicated by the TOUCH_CONTACT_PRESSURE_PRESENT (0x0004) flag in the fieldsPresent field.

    enums:
      touch_contact:
        0x0001:
          id: contactrect_present
          doc: |
            The optional contactRectLeft, contactRectTop, contactRectRight, and contactRectBottom fields are all present.
        0x0002:
          id: orientation_present
          doc: |
            The optional orientation field is present.
        0x0004:
          id: pressure_present
          doc: |
            The optional pressure field is present.
      contact_flag:
        0x0001:
          id: down
          doc: The contact transitioned to the engaged state (made contact).
        0x0002:
          id: update
          doc: Contact update.
        0x0004:
          id: up
          doc: The contact transitioned from the engaged state (broke contact).
        0x0008:
          id: inrange
          doc: The contact has not departed and is still in range.
        0x0010:
          id: incontact
          doc: The contact is in the engaged state.
        0x0020:
          id: canceled
          doc: The contact has been canceled.

  rdpinput_suspend_input_pdu:
    doc: |
      MS-RDPEI 2.2.3.4 RDPINPUT_SUSPEND_INPUT_PDU
      The RDPINPUT_SUSPEND_INPUT_PDU message is sent by the server endpoint and is used to instruct
      the client to suspend the transmission of the RDPINPUT_TOUCH_EVENT_PDU (section 2.2.3.3)
      message.
      mstscax!RdpInputClientChannel::OnDataReceived
      (no data)

  rdpinput_resume_input_pdu:
    doc: |
      MS-RDPEI 2.2.3.5 RDPINPUT_RESUME_INPUT_PDU
      The RDPINPUT_RESUME_INPUT_PDU message is sent by the server endpoint and is used to instruct the
      client to resume the transmission of the RDPINPUT_TOUCH_EVENT_PDU (section 2.2.3.3) message.
      mstscax!RdpInputClientChannel::OnDataReceived
      (no data)

  rdpinput_dismiss_hovering_touch_contact_pdu:
    doc: |
      MS-RDPEI 2.2.3.6 RDPINPUT_DISMISS_HOVERING_TOUCH_CONTACT_PDU
      The RDPINPUT_DISMISS_HOVERING_TOUCH_CONTACT_PDU message is sent by the client endpoint to instruct
      the server to transition a contact in the "hovering" state to the "out of range" state
      (section 3.1.1.1).
      (not reachable in WDAG RDP client)
    seq:
      - id: contact_id
        -orig-id: contactId
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the ID assigned to the contact.
          This value MUST be in the range 0x00 to 0xFF (inclusive).

  rdpinput_pen_event_pdu:
    doc: |
      MS-RDPEI 2.2.3.7 RDPINPUT_PEN_EVENT_PDU
      The RDPINPUT_PEN_EVENT_PDU message is sent by the client endpoint and is used to remote a collection
      of pen frames.
      (not reachable in WDAG RDP client)
    seq:
      - id: encode_time
        -orig-id: encodeTime
        type: rdpinput_four_byte_unsigned_integer
        doc: |
          A FOUR_BYTE_UNSIGNED_INTEGER (section 2.2.2.3) structure that specifies the time that has elapsed
          (in milliseconds) from when the oldest pen frame was generated to when it was encoded for
          transmission by the client.
      - id: frame_count
        -orig-id: frameCount
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the number of
          RDPINPUT_PEN_FRAME (section 2.2.3.7.1) structures in the frames field.
      - id: frames
        type: rdpinput_pen_frame
        repeat: expr
        repeat-expr: frame_count.value
        doc: |
          An array of RDPINPUT_PEN_FRAME structures ordered from the oldest in time to the most recent in time.
          The number of structures in this array is specified by the frameCount field.

  rdpinput_pen_frame:
    doc: |
      MS-RDPEI 2.2.3.7.1 RDPINPUT_PEN_FRAME
      The RDPINPUT_PEN_FRAME structure encapsulates a collection of RDPINPUT_PEN_CONTACT (section 2.2.3.7.1.1)
      structures that are part of the same logical pen frame.
    seq:
      - id: contact_count
        -orig-id: contactCount
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the number of RDPINPUT_PEN_CONTACT
          structures in the contacts field.
      - id: frame_offset
        -orig-id: frameOffset
        type: rdpinput_eight_byte_unsigned_integer
        doc: |
          An EIGHT_BYTE_UNSIGNED_INTEGER (section 2.2.2.5) structure that specifies the time offset from the previous
          frame (in microseconds). If this is the first frame being transmitted, then this field MUST be set to zero.
      - id: contacts
        type: rdpinput_pen_contact
        repeat: expr
        repeat-expr: contact_count.value
        doc: |
          An array of RDPINPUT_PEN_CONTACT structures. The number of structures in this array is specified by the
          contactCount field.

  rdpinput_pen_contact:
    doc: |
      MS-RDPEI 2.2.3.7.1.1 RDPINPUT_PEN_CONTACT
      The RDPINPUT_PEN_CONTACT structure describes the characteristics of a contact that is encapsulated in an
      RDPINPUT_PEN_FRAME (section 2.2.3.7.1) structure.
    seq:
      - id: device_id
        -orig-id: deviceId
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the ID assigned to the pen device. If the simultaneous injection
          of pen input was not negotiated using the RDPINPUT_SC_READY_PDU (section 2.2.3.1) and RDPINPUT_CS_READY_PDU
          (section 2.2.3.2) structures, then this ID MUST be set to zero.
      - id: fields_present
        -orig-id: fieldsPresent
        type: rdpinput_two_byte_unsigned_integer
        doc: |
          A TWO_BYTE_UNSIGNED_INTEGER (section 2.2.2.1) structure that specifies the presence of the optional penFlags,
          pressure, rotation, tiltX, and tiltY fields.
      - id: x
        type: rdpinput_four_byte_signed_integer
        doc: |
          A FOUR_BYTE_SIGNED_INTEGER (section 2.2.2.4) structure that specifies the x-coordinate
          (relative to the virtual-desktop origin) of the contact.
      - id: y
        type: rdpinput_four_byte_signed_integer
        doc: |
          A FOUR_BYTE_SIGNED_INTEGER structure that specifies the y-coordinate
          (relative to the virtual-desktop origin) of the contact.
      - id: contact_flags
        -orig-id: contactFlags
        type: rdpinput_four_byte_unsigned_integer
        doc: |
          A FOUR_BYTE_UNSIGNED_INTEGER (section 2.2.2.3) structure that specifies the current state of the contact.
      - id: pen_flags
        -orig-id: penFlags
        type: rdpinput_four_byte_unsigned_integer
        if: (fields_present.value & 0x0001) != 0
        doc: |
          A FOUR_BYTE_UNSIGNED_INTEGER structure that specifies the current state of the pen.
      - id: pressure
        type: rdpinput_four_byte_unsigned_integer
        if: (fields_present.value & 0x0002) != 0
        doc: |
          An optional FOUR_BYTE_UNSIGNED_INTEGER structure that specifies the pressure applied to the pen.
          This value MUST be normalized in the range 0x00000000 to 0x00000400 (1024), inclusive. The presence
          of this field is indicated by the PEN_CONTACT_PRESSURE_PRESENT (0x0002) flag in the fieldsPresent
          field.
      - id: rotation
        type: rdpinput_two_byte_unsigned_integer
        if: (fields_present.value & 0x0004) != 0
        doc: |
          An optional TWO_BYTE_UNSIGNED_INTEGER structure that specifies the clockwise rotation (or twist) of the pen.
          This value MUST be in the range 0x00000000 to 0x00000167 (359), inclusive. The presence of this field
          is indicated by the PEN_CONTACT_ROTATION_PRESENT (0x0004) flag in the fieldsPresent field.
      - id: tilt_x
        -orig-id: tiltX
        type: rdpinput_two_byte_signed_integer
        if: (fields_present.value & 0x0008) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER structure that specifies the angle of tilt of the pen along the x-axis.
          This value MUST be in the range -0x0000005A (-90) to 0x000005A (90), inclusive: a positive value indicates
          a tilt to the right. The presence of this field is indicated by the PEN_CONTACT_TILTX_PRESENT (0x0008) flag
          in the fieldsPresent field.
      - id: tilt_y
        -orig-id: tiltY
        type: rdpinput_two_byte_unsigned_integer
        if: (fields_present.value & 0x0010) != 0
        doc: |
          An optional TWO_BYTE_SIGNED_INTEGER structure that specifies the angle of tilt of the pen along the y-axis.
          This value MUST be in the range -0x0000005A (-90) to 0x000005A (90), inclusive: a positive value indicates
          a tilt toward the user. The presence of this field is indicated by the PEN_CONTACT_TILTY_PRESENT (0x0010)
          flag in the fieldsPresent field.
    enums:
      pen_contact:
        0x0001:
          id: penflags_present
          doc: The optional penFlags field is present.
        0x0002:
          id: pressure_present
          doc: The optional pressure field is present.
        0x0004:
          id: rotation_present
          doc: The optional rotation field is present.
        0x0008:
          id: tiltx_present
          doc: The optional tiltX field is present.
        0x0010:
          id: tilty_present
          doc: The optional tiltY field is present.
      contact_flag:
        0x0001:
          id: down
          doc: The contact transitioned to the engaged state (made contact).
        0x0002:
          id: update
          doc: Contact update.
        0x0004:
          id: up
          doc: The contact transitioned from the engaged state (broke contact).
        0x0008:
          id: inrange
          doc: The contact has not departed and is still in range.
        0x0010:
          id: incontact
          doc: The contact is in the engaged state.
        0x0020:
          id: canceled
          doc: The contact has been canceled.
      pen_flag:
        0x0001:
          id: barrel_pressed
          doc: Indicates that the barrel button is in the pressed state.
        0x0002:
          id: eraser_pressed
          doc: Indicates that the eraser button is in the pressed state.
        0x0004:
          id: inverted
          doc: Indicates that the pen is inverted.
