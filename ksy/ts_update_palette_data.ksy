meta:
  id: ts_update_palette_data
  endian: le
  imports:
    - ts_palette_entry

doc: |
  MS-RDPBCGR 2.2.9.1.1.3.1.1.1 Palette Update Data (TS_UPDATE_PALETTE_DATA)
  The TS_UPDATE_PALETTE_DATA encapsulates the palette data that defines a
  Palette Update (section 2.2.9.1.1.3.1.1).
  mstscax!CTSCoreGraphics::ProcessPalette
  mstscax!CUH::ProcessPalette

seq:
  - id: update_type
    type: u2
    doc: |
      A 16-bit, unsigned integer. The update type. This field MUST be set
      to UPDATETYPE_PALETTE (0x0002).
  - id: padding
    type: u2
    doc: |
      A 16-bit, unsigned integer. Padding. Values in this field MUST be
      ignored.
  - id: number_colors
    type: u4
    doc: |
      A 32-bit, unsigned integer. The number of RGB triplets in the paletteData
      field. This field MUST be set to 256 (the number of entries in an 8 bpp
      palette).
  - id: palette_entry
    type: ts_palette_entry
    repeat: expr
    repeat-expr: number_colors
    doc: |
      An array of palette entries in RGB triplet format (section 2.2.9.1.1.3.1.1.2)
      packed on byte boundaries. The number of triplet entries is given by the
      numberColors field.
