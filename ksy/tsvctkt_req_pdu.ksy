meta:
  id: tsvctkt_req_pdu
  endian: le
  imports:
    - rdp_guid
    - xml_document

doc: |
  MS-RDPEXPS 2.1 Transport
  Dynamic channel "TSVCTKT"
  mstscax!CTicketListenerCallback::OnNewChannelConnection
  mstscax!CRimChannel::OnDataReceived
  mstscax!CRIMObjManager::OnDataReceived
  mstscax!CRIMObjectPool<CMemory>::GetPooledObject
  mstscax!CMemory::Initialize
  mstscax!CRIMStreamStub::OnDataReceived (could also be mstscax!CRIMStreamProxy::OnDataReceived)
  mstscax!CRIMObjectPool<CMessage>::GetPooledObject
  mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
  mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke

seq:
  - id: interface_id
    -orig-id: InterfaceValue
    type: u4
    enum: interface_id
    doc: |
      A 32-bit unsigned integer that represents the common identifier for the interface.
      The default value is 0x00000000, and if the message uses this default interface ID, it is
      interpreted as the main interface for which this channel has been instantiated. All other
      values MUST be retrieved either from an Interface Query message response (QI_RSP) or from
      responses that contain interface IDs. For example, in section 2.2.4.3.1.1 the Callback
      parameter describes an interface, and it represents an ID suitable for use as an interface
      ID in another set of request/reply sequences.
      This ID is valid until an IFACE_RELEASE message is sent/received with that ID. After an
      IFACE_RELEASE message, this ID is considered invalid. A packet with an invalid InterfaceId
      causes channel termination from the packet parser on the receiving end.
  - id: message_id
    -orig-id: MessageId
    type: u4
    doc: |
      A 32-bit unsigned integer that represents a unique ID for the request and response pair.
      Requests and responses are matched, in part, based on this ID.
  - id: function_id
    -orig-id: FunctionId
    type: u4
    doc: |
      A 32-bit unsigned integer. This field MUST be present only in request packets. Its value
      is either used in interface manipulation messages or defined for a specific interface.
  - id: rimcall
    if: function_id < 100
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_rimcall::release.to_i': tsvctkt_rimcall_iface_release
        'function_id_rimcall::queryinterface.to_i': tsvctkt_rimcall_qi_req
  - id: printer_ticket
    if: (function_id >= 100) and ((interface_id.to_i & ~0xc0000000) == interface_id::printer_ticket.to_i)
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_printer_ticket::get_supported_versions_req.to_i': tsvctkt_get_supported_versions_req
        'function_id_printer_ticket::bind_printer_req.to_i': tsvctkt_bind_printer_req
        'function_id_printer_ticket::query_dev_ns_req.to_i': tsvctkt_query_dev_ns_req
        'function_id_printer_ticket::print_tkt_to_devmode_req.to_i': tsvctkt_print_tkt_to_devmode_req
        'function_id_printer_ticket::devmode_to_print_tkt_req.to_i': tsvctkt_devmode_to_print_tkt_req
        'function_id_printer_ticket::print_caps_req.to_i': tsvctkt_print_caps_req
        'function_id_printer_ticket::print_caps_from_print_tkt_req.to_i': tsvctkt_print_caps_from_print_tkt_req
        'function_id_printer_ticket::validate_print_tkt_req.to_i': tsvctkt_validate_print_tkt_req

enums:
  interface_id:
    0x00000000:
      id: printer_ticket
      doc: |
        MS-RDPEXPS 2.2.3 Printer Ticket Interface
        The Printer Ticket Interface is identified by the default interface ID 0x00000000.
        The default interface does not require Query Interface Request (QI_REQ) or
        Query Interface Response (QI_RSP) messages to initialize the interface.
        All requests flow from server to client and all responses flow from client to server.
        Certain fields in this interface are payload between the client printer driver and
        the printing subsystem. The content and meaning of these fields depends on internal
        structures for these two systems and is not interpreted in any way by this protocol.
        Special consideration has to be taken regarding the implementation on both sides when
        they are based on different operating systems. The implementation has to translate
        these operating system-specific differences between the client printer driver and
        the printing subsystem.
  function_id_rimcall:
    0x00000001:
      id: release
      -orig-id: RIMCALL_RELEASE
      doc: |
        Release the given interface ID.
    0x00000002:
      id: queryinterface
      -orig-id: RIMCALL_QUERYINTERFACE
      doc: |
        Query for a new interface.
  function_id_printer_ticket:
    0x00000100:
      id: get_supported_versions_req
      -orig-id: GET_SUPPORTED_VERSIONS_REQ
      doc: Server Get Supported Versions request
    0x00000101:
      id: bind_printer_req
      -orig-id: BIND_PRINTER_REQ
      doc: Server Bind Printer request
    0x00000102:
      id: query_dev_ns_req
      -orig-id: QUERY_DEV_NS_REQ
      doc: Server Query Device Namespace request
    0x00000103:
      id: print_tkt_to_devmode_req
      -orig-id: PRINT_TKT_TO_DEVMODE_REQ
      doc: Server Print Ticket to Devmode request
    0x00000104:
      id: devmode_to_print_tkt_req
      -orig-id: DEVMODE_TO_PRINT_TKT_REQ
      doc: Server Devmode to Print Ticket request
    0x00000105:
      id: print_caps_req
      -orig-id: PRINT_CAPS_REQ
      doc: Server Print Caps request
    0x00000106:
      id: print_caps_from_print_tkt_req
      -orig-id: PRINT_CAPS_FROM_PRINT_TKT_REQ
      doc: Server Print Caps From Print Ticket request
    0x00000107:
      id: validate_print_tkt_req
      -orig-id: VALIDATE_PRINT_TKT_REQ
      doc: Server Validate Print Ticket request

types:
  tsvctkt_rimcall_qi_req:
    doc: |
      MS-RDPEXPS 2.2.2.1.1 Query Interface Request (QI_REQ)
      The QI_REQ request message is sent from either the client side or the server side,
      and is used to request a new interface ID.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CProxyIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::QueryInterface
      mstscax!CTSClientPrintTicket::NonDelegatingQueryInterface
      mstscax!CMessage::WriteIUnknown
      mstscax!CRIMObjManager::GetIdFromObject
    seq:
      - id: new_interface_guid
        -orig-id: NewInterfaceGUID
        type: rdp_guid
        doc: |
          A 16-byte GUID that identifies the new interface.

  tsvctkt_rimcall_iface_release:
    doc: |
      MS-RDPEXPS 2.2.2.2 Interface Release (IFACE_RELEASE)
      Terminates the lifetime of the interface. This message is one-way only.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CMessage::RemoveIUnknown
      mstscax!CRIMObjManager::RemoveObject
      (no data)

  tsvctkt_get_supported_versions_req:
    doc: |
      MS-RDPEXPS 2.2.3.1.1 Server Get Supported Versions Request (GET_SUPPORTED_VERSIONS_REQ)
      This request retrieves an array of integers from the printer driver. The content of this
      array is not interpreted by this protocol and is passed only as payload.
      NOTE: Will error in WDAG due to AppContainer check.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_GetSupportedVersions
      mstscax!CTSClientPrintTicket::GetSupportedVersions
      mstscax!CTSClientPrintTicket::GetTicketInstance
      mstscax!CClientUtilsWin32::UT_IsRunningInAppContainer
      mstscax!CTSClientPrintTicket::GetDriverInfo3
    seq:
      - id: client_printer_id
        -orig-id: ClientPrinterId
        type: u4
        doc: |
          A 32-bit unsigned integer. This ID is exchanged by the Remote Desktop Protocol: File System
          Virtual Channel Extension, as specified in [MS-RDPEFS]. The ClientPrinterId value MUST be
          the same as the DeviceId field in the DEVICE_ANNOUNCE header (as specified in [MS-RDPEFS]
          section 2.2.1.3) that is embedded in the DeviceList field of the DR_CORE_DEVICELIST_ANNOUNCE_REQ
          packet (as specified in [MS-RDPEFS] section 2.2.2.9).

  tsvctkt_bind_printer_req:
    doc: |
      MS-RDPEXPS 2.2.3.1.3 Server Bind Printer Request (BIND_PRINTER_REQ)
      When a redirected server printer is initialized by the server-side operating system, it calls
      into the server printer driver to establish the printer driver context. This message redirects
      such a call to the client side.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_BindPrinter
      mstscax!CTSClientPrintTicket::BindPrinter
      mstscax!CTSClientPrintTicket::GetTicketInstance
      mstscax!CClientUtilsWin32::UT_IsRunningInAppContainer
      mstscax!CTSClientPrintTicket::GetDriverInfo3
    seq:
      - id: client_printer_id
        -orig-id: ClientPrinterId
        type: u4
        doc: |
          A 32-bit unsigned integer. This ID is exchanged by the Remote Desktop Protocol: File System
          Virtual Channel Extension, as specified in [MS-RDPEFS]. The ClientPrinterId value MUST be
          the same as the DeviceId field in the DEVICE_ANNOUNCE header (as specified in [MS-RDPEFS]
          section 2.2.1.3) that is embedded in the DeviceList field of the DR_CORE_DEVICELIST_ANNOUNCE_REQ
          packet (as specified in [MS-RDPEFS] section 2.2.2.9). This value uniquely identifies a printer
          on the client side.
      - id: version
        -orig-id: Version
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is passed by the printing subsystem as a payload and
          is not interpreted by this protocol in any way.

  tsvctkt_query_dev_ns_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.1 Server Query Device Namespace Request (QUERY_DEV_NS_REQ)
      The QUERY_DEV_NS_REQ request message is a server request for a default namespace from the printer
      driver on the client.
      NOTE: Will error with 0x80004005 if printers disabled (or no printer ) on WDAG.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_QueryDeviceNamespace
      mstscax!CTSClientPrintTicket::QueryDeviceNamespace
      mstscax!CTSClientPrintTicket::CreateDriverUniqueUri
      (no data)

  tsvctkt_print_tkt_to_devmode_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.3 Server Print Ticket to Devmode Request (PRINT_TKT_TO_DEVMODE_REQ)
      A PRINT_TKT_TO_DEVMODE_REQ request, flowing from the server to the client, is a request to convert
      an XML-based print ticket to a DEVMODE binary large object (BLOB).
      NOTE: WDAG will parse the XML without printing being enabled, but nothing else.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_ConvertPrintTicketToDevMode
      mstscax!CProxyReceive_IXMLDOMDocument2
      mstscax!CTSClientPrintTicket::ConvertPrintTicketToDevMode
    seq:
      - id: printer_ticket
        -orig-id: PrintTicket
        type: xml_document
        doc: |
          A print ticket in XML_DOCUMENT format to be converted into a DEVMODE BLOB. The content is treated
          as payload in this protocol.
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This value MUST be the number of bytes in the pDevmodeIn byte array field.
      - id: devmode_in
        -orig-id: pDevmodeIn
        size: devmode_in_size
        doc: |
          A DEVMODE structure sent as an array of bytes. The content is generated by the printing subsystem
          and is treated as payload in this protocol.

  tsvctkt_devmode_to_print_tkt_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.5 Server Devmode to Print Ticket Request (DEVMODE_TO_PRINT_TKT_REQ)
      The DEVMODE_TO_PRINT_TKT_REQ server-to-client request is used to convert a DEVMODE BLOB into
      an XML-based print ticket.
      NOTE: WDAG will parse the XML without printing being enabled, but nothing else.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_ConvertDevModeToPrintTicket
      mstscax!CProxyReceive_IXMLDOMDocument2
      mstscax!CTSClientPrintTicket::ConvertDevModeToPrintTicket
      mstscax!CTSClientPrintTicket::NoPT_ConvertDevModeToPrintTicket
      mstscax!CTSClientPrintTicket::OpenPTProvider
      mstscax!CProxyWrite_IXMLDOMDocument2
    seq:
      - id: devmode_in_size
        -orig-id: cbDevmodeIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This value MUST be the number of bytes in the pDevmodeIn byte array field.
      - id: devmode_in
        -orig-id: pDevmodeIn
        size: devmode_in_size
        doc: |
          A DEVMODE BLOB, represented as an array of bytes. The content is generated by the printing subsystem
          and is treated as payload in this protocol.
      - id: print_ticket
        -orig-id: PrintTicket
        type: xml_document
        doc: |
          A print ticket in XML_DOCUMENT format, that is updated with information from the pDevmodeIn BLOB.
          The content is generated by the printing subsystem and is treated as payload in this protocol.

  tsvctkt_print_caps_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.7 Server Print Caps Request (PRINT_CAPS_REQ)
      The PRINT_CAPS_REQ request message is used to request client printer capabilities.
      Each printer exposes certain capabilities such as duplex printing, stapling, or color support.
      These capabilities are redirected by using this message and the corresponding PRINT_CAPS_RSP
      response message. The data contained in this message is opaque to the
      Remote Desktop Protocol: XPS Print Virtual Channel Extension.
      NOTE: WDAG fails with 0x80004005 if printing is disabled.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_GetPrintCapabilities
      mstscax!CTSClientPrintTicket::GetPrintCapabilities
      mstscax!CTSClientPrintTicket::NoPT_GetPrintCapabilities
      mstscax!CTSClientPrintTicket::OpenPTProvider
      mstscax!CPrinterCollection::GetPrinterName
      mstscax!PRNTVPT!PTOpenProvider
      (no data)

  tsvctkt_print_caps_from_print_tkt_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.9 Server Print Caps From Print Ticket Request (PRINT_CAPS_FROM_PRINT_TKT_REQ)
      The PRINT_CAPS_FROM_PRINT_TKT_REQ message is a server-to-client request for print capabilities
      from a given print ticket.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_GetPrintCapabilitiesUsingPrintTicket
      mstscax!CProxyReceive_IXMLDOMDocument2
      mstscax!CTSClientPrintTicket::GetPrintCapabilitiesUsingPrintTicket
      mstscax!CTSClientPrintTicket::NoPT_GetPrintCapabilities
      mstscax!CTSClientPrintTicket::OpenPTProvider
      mstscax!CProxyWrite_IXMLDOMDocument2
    seq:
      - id: print_ticket
        -orig-id: PrintTicket
        type: xml_document
        doc: |
          A print ticket for which printer capabilities MUST be retrieved, in XML_DOCUMENT format; the
          printer capabilities are retrieved for the specified print ticket only. The content is generated
          by the printing subsystem and is treated as payload in this protocol.

  tsvctkt_validate_print_tkt_req:
    doc: |
      MS-RDPEXPS 2.2.3.2.11 Server Validate Print Ticket Request (VALIDATE_PRINT_TKT_REQ)
      The VALIDATE_PRINT_TKT_REQ request message is used to validate and update a print ticket.
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Dispatch_Invoke
      mstscax!CStubITSVC_PrintTicketProvider<ITSVC_PrintTicketProvider>::Invoke_ValidatePrintTicket
      mstscax!CTSClientPrintTicket::ValidatePrintTicket
    seq:
      - id: print_ticket
        -orig-id: PrintTicket
        type: xml_document
        doc: |
          The print ticket to validate, in XML_DOCUMENT format. The content is generated by the printing
          subsystem and is treated as payload in this protocol.
