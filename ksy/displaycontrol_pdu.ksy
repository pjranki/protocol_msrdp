meta:
  id: displaycontrol_pdu
  endian: le

doc: |
  MS-RDPEDISP 2.2.1.1 DISPLAYCONTROL_HEADER
  The DISPLAYCONTROL_HEADER structure is included in all display control PDUs
  and specifies the PDU type and the length of the PDU.
  Display control uses dynamic channel "Microsoft::Windows::RDS::DisplayControl"
  mstscax!RdpDisplayControlPlugin::OnNewChannelConnection
  mstscax!RdpDisplayControlChannel::OnDataReceived

seq:
  - id: type
    -orig-id: Type
    type: u4
    enum: displaycontrol_pdu_type
    doc: |
      A 32-bit unsigned integer that specifies the display control PDU type.
  - id: length
    -orig-id: Length
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the length of the display control
      PDU, in bytes. This value MUST include the length of the
      DISPLAYCONTROL_HEADER (8 bytes).
  - id: message
    type:
      switch-on: type
      -name: type
      cases:
        'displaycontrol_pdu_type::caps': displaycontrol_pdu_caps
        'displaycontrol_pdu_type::monitor_layout': displaycontrol_monitor_layout_pdu

enums:
  displaycontrol_pdu_type:
    0x00000005:
      id: caps
      doc: DISPLAYCONTROL_CAPS_PDU (section 2.2.2.1)
    0x00000002:
      id: monitor_layout
      doc: DISPLAYCONTROL_MONITOR_LAYOUT_PDU (section 2.2.2.2)

types:
  displaycontrol_pdu_caps:
    doc: |
      MS-RDPEDISP 2.2.2.1 DISPLAYCONTROL_CAPS_PDU
      The DISPLAYCONTROL_CAPS_PDU message is a server-to-client PDU that is used to
      specify a set of parameters which the client must adhere to when sending the
      DISPLAYCONTROL_MONITOR_LAYOUT_PDU (section 2.2.2.2) message.
      mstscax!RdpDisplayControlChannel::OnDataReceived
      mstscax!CTSCoreApi::SetDisplayControl
    seq:
      - id: max_num_monitors
        -orig-id: MaxNumMonitors
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the maximum number of monitors
          supported by the server.
          Must be greater than 0.
      - id: max_monitor_area_factor_a
        -orig-id: MaxMonitorAreaFactorA
        type: u4
        doc: |
          A 32-bit unsigned integer that is used to specify the maximum monitor area
          supported by the server. The maximum supported monitor area (in square pixels)
          is given by:
            MaxNumMonitors * MaxMonitorAreaFactorA * MaxMonitorAreaFactorB.
          200 <= MaxMonitorAreaFactorA <= 2000.
      - id: max_monitor_area_factor_b
        -orig-id: MaxMonitorAreaFactorB
        type: u4
        doc: |
          A 32-bit unsigned integer that is used to specify the maximum monitor area
          supported by the server. The maximum supported monitor area (in square pixels)
          is given by:
            MaxNumMonitors * MaxMonitorAreaFactorA * MaxMonitorAreaFactorB.
          200 <= MaxMonitorAreaFactorA <= 2000.

  displaycontrol_monitor_layout_pdu:
    doc: |
      MS-RDPEDISP 2.2.2.2 DISPLAYCONTROL_MONITOR_LAYOUT_PDU
      The DISPLAYCONTROL_MONITOR_LAYOUT_PDU message is a client-to-server PDU that is used
      to request a display configuration change on the server, such as the addition of a
      monitor or a new resolution for an existing monitor. Note that the entire monitor
      layout MUST be included in the Monitors field even if the configuration of only a
      single monitor is updated.
      mstscax!RdpDisplayControlChannel::SendMonitorLayoutPdu
      mstscax!RdpDisplayControlChannel::WriteMonitorLayoutPdu
    seq:
      - id: monitor_layout_size
        -orig-id: MonitorLayoutSize
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the size, in bytes, of a single element
          in the Monitors field. This field MUST be set to 40 bytes, the size of the
          DISPLAYCONTROL_MONITOR_LAYOUT structure (section 2.2.2.2.1).
      - id: num_monitors
        -orig-id: NumMonitors
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of display monitor definitions
          in the Monitors field. The maximum number of monitor definitions allowed is specified
          in the MaxNumMonitors field of the DISPLAYCONTROL_CAPS_PDU (section 2.2.2.1) message.
      - id: monitors
        -orig-id: Monitors
        type: displaycontrol_monitor_layout
        repeat: expr
        repeat-expr: num_monitors
        doc: |
          A variable-length array containing a series of DISPLAYCONTROL_MONITOR_LAYOUT structures
          that specify the display monitor layout of the client. The number of
          DISPLAYCONTROL_MONITOR_LAYOUT structures is specified by the NumMonitors field. The area
          (in square pixels) of the layout specified by the DISPLAYCONTROL_MONITOR_LAYOUT structures
          MUST NOT exceed the maximum monitor area defined by the server in the
          DISPLAYCONTROL_CAPS_PDU message.

  displaycontrol_monitor_layout:
    doc: |
      MS-RDPEDISP 2.2.2.2.1 DISPLAYCONTROL_MONITOR_LAYOUT
      The DISPLAYCONTROL_MONITOR_LAYOUT structure is used to specify the characteristics of a monitor.
      The coordinates used to describe the monitor position MUST be relative to the upper-left corner
      of the monitor designated as the "primary display monitor". The upper-left corner of the primary
      monitor is always (0, 0).
    seq:
      - id: flags
        -orig-id: Flags
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies monitor configuration flags.
      - id: left
        -orig-id: Left
        type: s4
        doc: |
          A 32-bit signed integer that specifies the x-coordinate of the upper-left corner of the
          display monitor.
      - id: top
        -orig-id: Top
        type: s4
        doc: |
          A 32-bit signed integer that specifies the y-coordinate of the upper-left corner of the
          display monitor.
      - id: width
        -orig-id: Width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the width of the monitor in pixels. The width MUST
          be greater than or equal to 200 pixels and less than or equal to 8192 pixels, and MUST NOT be
          an odd value.
      - id: height
        -orig-id: Height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the height of the monitor in pixels. The height MUST
          be greater than or equal to 200 pixels and less than or equal to 8192 pixels.
      - id: physical_width
        -orig-id: PhysicalWidth
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the physical width of the monitor, in millimeters (mm).
          This value MUST be ignored if it is less than 10 mm or greater than 10,000 mm or the
          PhysicalHeight field is less than 10 mm or greater than 10,000 mm.
      - id: physical_height
        -orig-id: PhysicalHeight
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the physical height of the monitor, in millimeters.
          This value MUST be ignored if it is less than 10 mm or greater than 10,000 mm or PhysicalWidth
          is less than 10 mm or greater than 10,000 mm.
      - id: orientation
        -orig-id: Orientation
        type: u4
        enum: orientation
        doc: |
          A 32-bit unsigned integer that specifies the orientation of the monitor in degrees.
          Valid values are 0, 90, 180 or 270.
          This value MUST be ignored if it is not set to one of these values.
      - id: desktop_scale_factor
        -orig-id: DesktopScaleFactor
        type: u4
        doc: |
          A 32-bit, unsigned integer that specifies the desktop scale factor of the monitor.
          This value MUST be ignored if it is less than 100 percent or greater than 500 percent, or if
          DeviceScaleFactor is not 100 percent, 140 percent, or 180 percent.
      - id: device_scale_factor
        -orig-id: DeviceScaleFactor
        type: u4
        doc: |
          A 32-bit, unsigned integer that specifies the device scale factor of the monitor.
          This value MUST be ignored if it is not set to 100 percent, 140 percent, or 180 percent or if
          DesktopScaleFactor is less than 100 percent or greater than 500 percent.<1>
    enums:
      displaycontrol_monitor:
        0x00000001:
          id: primary
          doc: The monitor specified by this structure is the primary monitor.
      orientation:
        0:
          id: landscape
          doc: The desktop is not rotated.
        90:
          id: portrait
          doc: The desktop is rotated clockwise by 90 degrees.
        180:
          id: landscape_flipped
          doc: The desktop is rotated clockwise by 180 degrees.
        270:
          id: portrait_flipped
          doc: The desktop is rotated clockwise by 270 degrees.
