meta:
  id: ts_compressed_bitmap_header_ex
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.2.1.1.1 Extended Compressed Bitmap Header (TS_COMPRESSED_BITMAP_HEADER_EX)
  The TS_COMPRESSED_BITMAP_HEADER_EX structure is used to encapsulate nonessential information
  associated with bitmap data being transported in an Extended Bitmap Data (section 2.2.9.2.1.1)
  structure.
  mstscax!CTSCoreGraphics::ProcessUpdateSurfaceBits

seq:
  - id: high_unique_id
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the high-order bits of a unique 64-bit identifier
      for the bitmap data.
  - id: low_unique_id
    type: u4
    doc: |
      A 32-bit, unsigned integer that contains the low-order bits of a unique 64-bit identifier
      for the bitmap data.
  - id: milliseconds
    type: u8
    doc: |
      A 64-bit, unsigned integer that contains the milliseconds component of the timestamp that
      indicates when the bitmap data was generated. The timestamp (composed of the tmMilliseconds
      and tmSeconds fields), denotes the period of time that has elapsed since January 1, 1970
      (midnight UTC/GMT), not counting leap seconds.
  - id: seconds
    type: u8
    doc: |
      A 64-bit, unsigned integer that contains the seconds component of the timestamp that
      indicates when the bitmap data was generated. The timestamp (composed of the tmMilliseconds
      and tmSeconds fields), denotes the period of time that has elapsed since January 1, 1970
      (midnight UTC/GMT), not counting leap seconds.

instances:
  key1:
    value: low_unique_id
    doc: |
      A 32-bit, unsigned integer that contains the low-order bits of a unique 64-bit identifier
      for the bitmap data.
  key2:
    value: high_unique_id
    doc: |
      A 32-bit, unsigned integer that contains the high-order bits of a unique 64-bit identifier
      for the bitmap data.
