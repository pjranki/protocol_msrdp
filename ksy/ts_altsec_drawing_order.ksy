meta:
  id: ts_altsec_drawing_order
  endian: le
  imports:
    - ts_window_order
    - ts_compdesk_drawing_order

doc: |
  MS-RDPEGDI 2.2.2.2.1.3 Alternate Secondary Drawing Order
  mstscax!CUH::ProcessOrders

params:
  - id: control_flags
    type: u1
    doc: |
      An 8-bit, unsigned integer. The control byte that identifies
      the class and type of the drawing order.

seq:
  - id: order
    type:
      switch-on: order_type
      -name: type
      cases:
        'ts_altsec::switch_surface': ts_switch_surface_order
        'ts_altsec::create_offsrc_bitmap': ts_create_offsrc_bitmap
        'ts_altsec::stream_bitmap_first': ts_stream_bitmap_first
        'ts_altsec::stream_bitmap_next': ts_stream_bitmap_next
        'ts_altsec::create_ninegrid_bitmap': ts_create_ninegrid_bitmap
        'ts_altsec::gdip_first': ts_draw_gdiplus_first_order
        'ts_altsec::gdip_next': ts_draw_gdiplus_next_order
        'ts_altsec::gdip_end': ts_draw_gdiplus_end_order
        'ts_altsec::gdip_cache_first': ts_draw_gdiplus_cache_first_order
        'ts_altsec::gdip_cache_next': ts_draw_gdiplus_cache_next_order
        'ts_altsec::gdip_cache_end': ts_draw_gdiplus_cache_end_order
        'ts_altsec::window': ts_window_order
        'ts_altsec::compdesk_first': ts_compdesk_drawing_order
        'ts_altsec::frame_marker': ts_frame_marker

instances:
  order_type:
    value: (control_flags >> 2) & 0x3f
    enum: ts_altsec
    doc: |
      A 6-bit, unsigned integer. Identifies the type of alternate secondary drawing order.

enums:
  ts_altsec:
    0x00:
      id: switch_surface
      doc: |
        Switch Surface Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.3).
    0x01:
      id: create_offsrc_bitmap
      doc: |
        Create Offscreen Bitmap Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.2).
    0x02:
      id: stream_bitmap_first
      doc: |
        Stream Bitmap First (Revision 1 and 2) Alternate Secondary Drawing Order
        (see section 2.2.2.2.1.3.5.1).
    0x03:
      id: stream_bitmap_next
      doc: |
        Stream Bitmap Next Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.5.2).
    0x04:
      id: create_ninegrid_bitmap
      doc: |
        Create NineGrid Bitmap Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.4).
    0x05:
      id: gdip_first
      doc: |
        Draw GDI+ First Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.2).
    0x06:
      id: gdip_next
      doc: |
        Draw GDI+ Next Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.3).
    0x07:
      id: gdip_end
      doc: |
        Draw GDI+ End Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.4).
    0x08:
      id: gdip_cache_first
      doc: |
        Draw GDI+ First Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.2).
    0x09:
      id: gdip_cache_next
      doc: |
        Draw GDI+ Cache Next Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.3).
    0x0a:
      id: gdip_cache_end
      doc: |
        Draw GDI+ Cache End Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.6.4).
    0x0b:
      id: window
      doc: |
       Windowing Alternate Secondary Drawing Order (see [MS-RDPERP] section 2.2.1.3).
    0x0c:
      id: compdesk_first
      doc: |
        Desktop Composition Alternate Secondary Drawing Order (see [MS-RDPEDC] section 2.2.1.1).
    0x0d:
      id: frame_marker
      doc: |
        Frame Marker Alternate Secondary Drawing Order (see section 2.2.2.2.1.3.7).

types:
  offscr_delete_list:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.2.1 Offscreen Cache Delete List (OFFSCR_DELETE_LIST)
      The OFFSCR_DELETE_LIST structure is used to encode a collection of Offscreen Bitmap
      Cache indices that MUST be deleted.
    seq:
      - id: indices_count
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of 2-byte indices held in the indices field.
      - id: indices
        type: u2
        repeat: expr
        repeat-expr: indices_count
        doc: |
          A collection of offscreen bitmap cache indices that identify cache entries that MUST
          be deleted. Each index is a 16-bit unsigned integer and MUST be greater than or equal
          to 0, and less than the maximum number of entries allowed in the Offscreen Bitmap
          Cache, as specified by the offscreenCacheEntries field of the Offscreen Bitmap Cache
          Capability Set ([MS-RDPBCGR] section 2.2.7.1.9). The number of indices in this list is
          specified by the cIndices field.

  ninegrid_bitmap_info:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.4.1 NineGrid Bitmap Information (NINEGRID_BITMAP_INFO)
      The NINEGRID_BITMAP_INFO structure is used to describe a NineGrid source bitmap
      (see section 4.4). For more information about NineGrid bitmaps, see [NINEGRID].
    seq:
      - id: flags
        type: u4
        enum: dsdng
        doc: |
          A 32-bit, unsigned integer. Option flags for the NineGrid bitmap represented by
          this structure.
      - id: left_width
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width of the left-side NineGrid border. For a
          visual illustration of this field, see section 4.4.
      - id: right_width
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width of the right-side NineGrid border. For a
          visual illustration of this field, see section 4.4.
      - id: top_height
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height of the top NineGrid border. For a visual
          illustration of this field, see section 4.4.
      - id: bottom_height
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height of the bottom NineGrid border. For a visual
          illustration of this field, see section 4.4.
      - id: transparent
        type: u4
        doc: |
          The RGB color in the source bitmap to treat as transparent represented using a Color
          Reference (section 2.2.2.2.1.3.4.1.1) structure. This field is used if the
          DSDNG_TRANSPARENT (0x00000008) flag is set in the flFlags field.
    enums:
      dsdng:
        0x00000001:
          id: stretch
          doc: |
            Indicates that the center portion of the source bitmap MUST be stretched to fill the
            center of the destination NineGrid.
        0x00000002:
          id: tile
          doc: |
            Indicates that the center portion source bitmap MUST be tiled to fill the center of the
            destination NineGrid.
        0x00000004:
          id: perpixelalpha
          doc: |
            Indicates that an AlphaBlend operation MUST be used to compose the destination NineGrid.
            The source bitmap is expected to have per-pixel alpha values. For a description of the
            AlphaBlend operation, see [MSDN-ABLEND].
        0x00000008:
          id: transparent
          doc: |
            Indicates that a TransparentBlt operation MUST be used to compose the destination NineGrid.
            The crTransparent field MUST contain the transparent color. For a description of the
            TransparentBlt operation, see [MSDN-TransparentBlt].
        0x00000010:
          id: mustflip
          doc: |
            Indicates that the source NineGrid MUST be flipped on a vertical axis.
        0x00000020:
          id: truesize
          doc: |
            Indicates that the source bitmap MUST be transferred without stretching or tiling.

  ts_switch_surface_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.3 Switch Surface (SWITCH_SURFACE_ORDER)
      The Switch Surface Alternate Secondary Drawing Order is used by the server to
      instruct the client to switch the target drawing surface either to the primary
      drawing surface (screen desktop) or to an entry in the Offscreen Bitmap Cache.
      Support for offscreen bitmap caching is specified in the Offscreen Bitmap Cache
      Capability Set (see [MS-RDPBCGR] section 2.2.7.1.9).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHSwitchSurface
    seq:
      - id: bitmap_id
        type: u2
        doc: |
          A 16-bit, unsigned integer. The new target drawing surface. If this field
          has a value less than SCREEN_BITMAP_SURFACE (0xFFFF), it SHOULD<5> identify
          an entry in the Offscreen Bitmap Cache that contains a bitmap surface that
          MUST become the new target drawing surface. This value MUST be greater than
          or equal to 0 and less than the maximum number of entries allowed in the
          Offscreen Bitmap Cache as specified by the offscreenCacheEntries field of
          the Offscreen Bitmap Cache Capability Set ([MS-RDPBCGR] section 2.2.7.1.9).
          Otherwise, if this field has the value SCREEN_BITMAP_SURFACE, the target
          drawing surface MUST be changed to the primary drawing surface (screen desktop).

  ts_create_offsrc_bitmap:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.2 Create Offscreen Bitmap (CREATE_OFFSCR_BITMAP_ORDER)
      The Create Offscreen Bitmap Alternate Secondary Drawing Order is used by the server to
      instruct the client to create a bitmap of a particular width and height in the Offscreen
      Bitmap Cache. Support for offscreen bitmap caching is specified in the Offscreen Bitmap
      Cache Capability Set (see [MS-RDPBCGR] section 2.2.7.1.9).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHCreateOffscrBitmap
    seq:
      - id: flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Operational flags.
      - id: cx
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width in pixels of the offscreen bitmap to create.
      - id: cy
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height in pixels of the offscreen bitmap to create.
      - id: delete_list
        type: offscr_delete_list
        if: delete_list_present
        doc: |
          A collection of Offscreen Bitmap Cache entries that MUST be deleted, stored in an
          Offscreen Cache Delete List (section 2.2.2.2.1.3.2.1) structure.
    instances:
      offscreen_bitmap_id:
        value: flags & 0x7fff
        doc: |
          A 15-bit unsigned integer. The index of the Offscreen Bitmap Cache entry wherein
          the bitmap MUST be created. This value MUST be greater than or equal to 0 and less
          than the maximum number of entries allowed in the Offscreen Bitmap Cache as specified
          by the offscreenCacheEntries field of the Offscreen Bitmap Cache Capability Set
          ([MS-RDPBCGR] section 2.2.7.1.9).
      delete_list_present:
        value: (flags & 0x8000) != 0
        doc: |
          A 1-bit field. Indicates that the deleteList field is present.

  ts_stream_bitmap_first:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.5.1 Stream Bitmap First (STREAM_BITMAP_FIRST_ORDER)
      The Stream Bitmap First Alternate Secondary Drawing Order is used by the server to send
      the client the first block in a streamed bitmap and information describing the bitmap
      (such as color depth, width, and height).
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsDrawNineGridHandler::CacheStreamBitmapFirstPDU
    seq:
      - id: bitmap_flags
        type: u1
        doc: An 8-bit, unsigned integer. Flags describing the order contents and layout.
      - id: bitmap_bpp
        type: u1
        doc: An 8-bit, unsigned integer. The color depth in bits per pixel of the streamed bitmap.
      - id: bitmap_type
        type: u2
        enum: ts_draw
        doc: A 16-bit, unsigned integer. The type of the streamed bitmap.
      - id: bitmap_width
        type: u2
        doc: A 16-bit, unsigned integer. The width in pixels of the streamed bitmap.
      - id: bitmap_height
        type: u2
        doc: A 16-bit, unsigned integer. The height in pixels of the streamed bitmap.
      - id: bitmap_size_low
        type: u2
      - id: bitmap_size_high
        type: u2
        if: (bitmap_flags & 0x04) != 0
      - id: bitmap_block_size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the bitmap stream data block
          contained in the BitmapBlock field. This value MUST be less than or equal to the
          value contained in the BitmapSize field; if the STREAM_BITMAP_END (0x01) flag is
          set in the BitmapFlags field, the two values MUST be equal.
      - id: bitmap_block
        size: bitmap_block_size
        doc: |
          A variable-length byte array. The first block of the streamed bitmap (also the
          last if the STREAM_BITMAP_END (0x01) flag is set in the BitmapFlags field). The
          size of this block is given by the BitmapBlockSize field.
    instances:
      bitmap_size_low_u4:
        value: bitmap_size_low << 0
      bitmap_size_high_u4:
        value: bitmap_size_high << 0
      bitmap_size:
        value: '((bitmap_flags & 0x04) != 0 ? (bitmap_size_high_u4 << 16) & 0xffff0000 : 0) | bitmap_size_low_u4 & 0x0000ffff'
        doc: |
          A variable-length field containing the total size in bytes of the streamed bitmap.
          If the STREAM_BITMAP_REV2 (0x04) flag is set in the BitmapFlags field, this field
          MUST contain a 32-bit unsigned integer. If the STREAM_BITMAP_REV2 flag is not set,
          this field MUST contain a 16-bit unsigned integer.
    enums:
      stream_bitmap:
        0x01:
          id: end
          doc: Indicates that the bitmap fits into one stream bitmap block (4,096 bytes).
        0x02:
          id: compressed
          doc: Indicates that the bitmap data is compressed.
        0x04:
          id: rev2
          doc: |
            Indicates that the BitmapSize field is 4 bytes. If this flag is not set,
            the BitmapSize field is 2 bytes.
      ts_draw:
        0x0001:
          id: ninegrid_bitmap_cache
          doc: Indicates that the data in the BitmapBlock field is a NineGrid source bitmap.

  ts_stream_bitmap_next:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.5.2 Stream Bitmap Next (STREAM_BITMAP_NEXT_ORDER)
      The Stream Bitmap Next Alternate Secondary Drawing Order is used by the server to
      send the client intermediate and final blocks in a streamed bitmap.
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsDrawNineGridHandler::CacheStreamBitmapNextPDU
    seq:
      - id: bitmap_flags
        type: u1
        doc: An 8-bit, unsigned integer. Flags describing the order contents and layout.
      - id: bitmap_bpp
        type: u1
        doc: An 8-bit, unsigned integer. The color depth in bits per pixel of the streamed bitmap.
      - id: bitmap_type
        type: u2
        enum: ts_draw
        doc: A 16-bit, unsigned integer. The type of the streamed bitmap.
      - id: bitmap_block_size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the bitmap stream data block
          contained in the BitmapBlock field.
      - id: bitmap_block
        size: bitmap_block_size
        doc: |
          A variable-length byte array. The intermediate or final block of the streamed bitmap.
          The size of this block is given by the BitmapBlockSize field.
    enums:
      stream_bitmap:
        0x01:
          id: end
          doc: Indicates that the bitmap fits into one stream bitmap block (4,096 bytes).
        0x02:
          id: compressed
          doc: Indicates that the bitmap data is compressed.
        0x04:
          id: rev2
          doc: |
            Indicates that the BitmapSize field is 4 bytes. If this flag is not set,
            the BitmapSize field is 2 bytes.
      ts_draw:
        0x0001:
          id: ninegrid_bitmap_cache
          doc: Indicates that the data in the BitmapBlock field is a NineGrid source bitmap.

  ts_create_ninegrid_bitmap:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.4 Create NineGrid Bitmap (CREATE_NINEGRID_BITMAP_ORDER)
      The Create NineGrid Bitmap Alternate Secondary Drawing Order is used by the server to
      instruct the client to create a NineGrid bitmap of a particular width and height in the
      NineGrid Bitmap Cache (the color depth MUST be 32 bpp). Support for NineGrid drawing is
      specified in the DrawNineGrid Cache Capability Set (section 2.2.1.2).
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsDrawNineGridHandler::CreateNineGridBitmap
    seq:
      - id: bitmap_bpp
        type: u1
        doc: |
          An 8-bit, unsigned integer. The color depth in bits per pixel of the NineGrid bitmap
          to create. Currently, all NineGrid bitmaps are sent in 32 bpp, so this field MUST be
          set to 0x20.
      - id: bitmap_id
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the NineGrid Bitmap Cache entry wherein the
          bitmap and NineGrid transformation information MUST be stored. This value MUST be
          greater than or equal to 0 and less than the maximum number of entries allowed in the
          NineGrid Bitmap Cache as specified by the drawNineGridCacheEntries field of the
          DrawNineGrid Cache Capability Set (section 2.2.1.2).
      - id: cx
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width in pixels of the NineGrid bitmap to create.
      - id: cy
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height in pixels of the NineGrid bitmap to create.
      - id: ninegrid_info
        type: ninegrid_bitmap_info
        doc: |
          A NineGrid Bitmap Information (section 2.2.2.2.1.3.4.1) structure that describes
          properties of the NineGrid bitmap to be created.

  ts_draw_gdiplus_first_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.5 Draw GDI+ First (DRAW_GDIPLUS_FIRST_ORDER)
      The Draw GDI+ First Alternate Secondary Drawing Order contains the first batch of
      GDI+ 1.1 drawing primitives that comprise a rendering update sent by the server to
      the client. Support for GDI+ 1.1 rendering is specified in the Draw GDI+ Capability
      Set (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: pad_1_octet
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: total_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size in bytes of the data in all
          of the emfRecords fields in this and subsequent Draw GDI+ Next and Draw GDI+
          End orders.
      - id: total_emf_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size, in bytes, of the EMF+ record
          data (in this and subsequent Draw GDI+ Next and Draw GDI+ End Orders) that MUST
          be passed to the GDI+ subsystem for rendering. Because references to GDI+ primitives
          cached with Draw GDI+ Cache Orders (sections 2.2.2.2.1.3.6.2, 2.2.2.2.1.3.6.3, and
          2.2.2.2.1.3.6.4) can be contained in the EMF+ Records embedded in the emfRecords
          field, the total size of the EMF+ Records passed to the GDI+ subsystem for rendering
          can be larger than what was sent on the wire.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field. If the most significant bit of the Size
          field of an EMF+ Record is set, the EMF+ Record data contains a 16-bit cache index
          that MUST be used to retrieve a cached GDI+ 1.1 primitive from the appropriate GDI+
          cache; the primitive MUST then be embedded in the EMF+ Record stream.

  ts_draw_gdiplus_next_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.6 Draw GDI+ Next (DRAW_GDIPLUS_NEXT_ORDER)
      The Draw GDI+ Next Alternate Secondary Drawing Order contains the second or subsequent
      batch of GDI+ 1.1 drawing primitives that comprise a rendering update sent by the server
      to the client. The first primitive in the sequence MUST have been transmitted with the
      Draw GDI+ First Alternate Secondary Drawing Order. Support for GDI+ 1.1 rendering is
      specified in the Draw GDI+ Capability Set (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: pad_1_octet
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field. If the most significant bit of the Size
          field of an EMF+ Record is set, the EMF+ Record data contains a 16-bit cache index
          that MUST be used to retrieve a cached GDI+ 1.1 primitive from the appropriate GDI+
          cache; the primitive MUST then be embedded in the EMF+ Record stream.

  ts_draw_gdiplus_end_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.7 Draw GDI+ End (DRAW_GDIPLUS_END_ORDER)
      The Draw GDI+ End Alternate Secondary Drawing Order contains the final batch of GDI+ 1.1
      drawing primitives that comprise a rendering update sent by the server to the client.
      Support for GDI+ 1.1 rendering is specified in the Draw GDI+ Capability Set
      (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: pad_1_octet
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field MUST be ignored.
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: total_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size in bytes of the data in all
          of the emfRecords fields in this and subsequent Draw GDI+ Next and Draw GDI+
          End orders.
      - id: total_emf_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size, in bytes, of the EMF+ record
          data (in this and subsequent Draw GDI+ Next and Draw GDI+ End Orders) that MUST
          be passed to the GDI+ subsystem for rendering. Because references to GDI+ primitives
          cached with Draw GDI+ Cache Orders (sections 2.2.2.2.1.3.6.2, 2.2.2.2.1.3.6.3, and
          2.2.2.2.1.3.6.4) can be contained in the EMF+ Records embedded in the emfRecords
          field, the total size of the EMF+ Records passed to the GDI+ subsystem for rendering
          can be larger than what was sent on the wire.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field. If the most significant bit of the Size
          field of an EMF+ Record is set, the EMF+ Record data contains a 16-bit cache index
          that MUST be used to retrieve a cached GDI+ 1.1 primitive from the appropriate GDI+
          cache; the primitive MUST then be embedded in the EMF+ Record stream.

  ts_draw_gdiplus_cache_first_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.2 Draw GDI+ Cache First (DRAW_GDIPLUS_CACHE_FIRST_ORDER)
      The Draw GDI+ Cache First Alternate Secondary Drawing Order contains the first batch
      of GDI+ 1.1 cacheable drawing primitives that comprise a rendering update sent by the
      server to the client. Support for GDI+ 1.1 rendering and primitive caching is specified
      in the Draw GDI+ Capability Set (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: flags
        type: u1
        enum: gdip
        doc: |
          An 8-bit, unsigned integer. Flags indicating instructions on how to handle previous
          cache orders in relation to this one.
      - id: cache_type
        type: u2
        doc: |
          A GDI+ Cache Type (section 2.2.2.2.1.3.6.1.1) structure. GDI+ cache in which to store
          the EMF+ Records contained in the emfRecords field.
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the cache entry into which to write the primitive.
          This value MUST be greater than or equal to 0 and less than the maximum number of entries
          allowed in the GDI+ Graphics, Pen, Brush, Image and Image Attributes caches. The maximum
          number of entries allowed in each of these caches is specified by the GdipCacheEntries
          field of the Draw GDI+ Capability Set (section 2.2.1.3).
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: total_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size in bytes of the data in all of the
          emfRecords fields in this and subsequent Draw GDI+ Cache Next and Draw GDI+ Cache
          End Orders.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field.
    enums:
      gdip:
        0x01:
          id: remove_cacheentry
          doc: |
            Remove the cache entry item at the index specified by CacheIndex before caching the
            one contained in this order.

  ts_draw_gdiplus_cache_next_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.3 Draw GDI+ Cache Next (DRAW_GDIPLUS_CACHE_NEXT_ORDER)
      The Draw GDI+ Cache Next Alternate Secondary Drawing Order contains the second or subsequent
      batch of GDI+ 1.1 cacheable drawing primitives that comprise a rendering update sent by the
      server to the client. The first primitive in the sequence MUST have been transmitted with
      the Draw GDI+ Cache First Alternate Secondary Drawing Order. Support for GDI+ 1.1 rendering
      and primitive caching is specified in the Draw GDI+ Capability Set (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: flags
        type: u1
        enum: gdip
        doc: |
          An 8-bit, unsigned integer. Flags indicating instructions on how to handle previous
          cache orders in relation to this one.
      - id: cache_type
        type: u2
        doc: |
          A GDI+ Cache Type (section 2.2.2.2.1.3.6.1.1) structure. GDI+ cache in which to store
          the EMF+ Records contained in the emfRecords field.
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the cache entry into which to write the primitive.
          This value MUST be greater than or equal to 0 and less than the maximum number of entries
          allowed in the GDI+ Graphics, Pen, Brush, Image and Image Attributes caches. The maximum
          number of entries allowed in each of these caches is specified by the GdipCacheEntries
          field of the Draw GDI+ Capability Set (section 2.2.1.3).
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field.
    enums:
      gdip:
        0x01:
          id: remove_cacheentry
          doc: |
            Remove the cache entry item at the index specified by CacheIndex before caching the
            one contained in this order.

  ts_draw_gdiplus_cache_end_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.6.4 Draw GDI+ Cache End (DRAW_GDIPLUS_CACHE_END_ORDER)
      The Draw GDI+ Cache End Alternate Secondary Drawing Order contains the final
      batch of GDI+ 1.1 cacheable drawing primitives that comprise a rendering update
      sent by the server to the client. Support for GDI+ 1.1 rendering and primitive
      caching is specified in the Draw GDI+ Capability Set (section 2.2.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UH_OnUnknownAltSecPacket
      mstscax!CTSCoreEventSource::FireSyncNotification
      mstscax!CTSCoreEventSource::InternalFireSyncNotification
      (could not find the code that does this)
    seq:
      - id: flags
        type: u1
        enum: gdip
        doc: |
          An 8-bit, unsigned integer. Flags indicating instructions on how to handle previous
          cache orders in relation to this one.
      - id: cache_type
        type: u2
        doc: |
          A GDI+ Cache Type (section 2.2.2.2.1.3.6.1.1) structure. GDI+ cache in which to store
          the EMF+ Records contained in the emfRecords field.
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the cache entry into which to write the primitive.
          This value MUST be greater than or equal to 0 and less than the maximum number of entries
          allowed in the GDI+ Graphics, Pen, Brush, Image and Image Attributes caches. The maximum
          number of entries allowed in each of these caches is specified by the GdipCacheEntries
          field of the Draw GDI+ Capability Set (section 2.2.1.3).
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the emfRecords field.
      - id: total_size
        type: u4
        doc: |
          A 32-bit, unsigned integer. The cumulative size in bytes of the data in all of the
          emfRecords fields in this and subsequent Draw GDI+ Cache Next and Draw GDI+ Cache
          End Orders.
      - id: emf_records
        size: size
        doc: |
          A collection of EMF+ Records specified in [MS-EMFPLUS] section 2.3. The size of the
          emfRecords field is given by the cbSize field.
    enums:
      gdip:
        0x01:
          id: remove_cacheentry
          doc: |
            Remove the cache entry item at the index specified by CacheIndex before caching the
            one contained in this order.

  ts_frame_marker:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.3.7 Frame Marker
      The Frame Marker Alternate Secondary Drawing Order is used by the server to indicate to
      the client the beginning and end of a logical frame of graphics data. Breaking graphics
      data up into logical frames indicates to the client which orders SHOULD be rendered as a
      logical unit, hence helping to prevent screen tearing. Support for frame markers is
      specified in the Order Capability Set (see [MS-RDPBCGR] section 2.2.7.1.3).
      mstscax!CUH::ProcessOrders
    seq:
      - id: action
        type: u4
        enum: ts_frame
        doc: |
          A 32-bit, unsigned integer. Indicates the start or end of a logical frame.
    enums:
      ts_frame:
        0x00000000:
          id: start
          doc: |
            Start of a logical frame of graphics data. All drawing orders from this point in
            the graphics stream are part of the same logical frame and can be rendered as one
            cohesive unit to prevent tearing.
        0x00000001:
          id: end
          doc: |
            End of a logical frame of graphics data.
