meta:
  id: t123_tpkt
  endian: be
  imports:
   - x224_tpdu

doc: |
  T123 8 Packet header to delimit data units in an octet stream
  A simple packet format whose purpose is to delimit TPDUs. Each packet,
  termed a TPKT, is a unit composed of a whole integral number of octets,
  of variable length.
  mstscax!CTSX224Filter::SendBuffer
  mstscax!CTSX224Filter::OnDataAvailable

params:
  - id: version
    type: u1
    -default: 0x03
    doc: Octet 1 is a version number, with binary value 0000 0011

seq:
  - id: reserved
    type: u1
    -default: 0x00
    doc: Octet 2 is reserved for further study.
  - id: length
    type: u2
    doc: |
      Octets 3 and 4 are the unsigned 16-bit binary encoding of the TPKT
      length. This is the length of the entire packet in octets, including
      both the packet header and the TPDU.
  - id: data
    type: x224_tpdu
    size: 'length >= 4 ? length - 4 : 0'
    doc:
      X224 TPDU data

-update:
  - id: length
    value: _root.length
    doc: |
      Set the total length of this PDU (including the header).
