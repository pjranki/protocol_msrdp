meta:
  id: tsmf_req_pdu
  endian: le
  imports:
    - rdp_guid
    - tsmm_capabilities
    - tsmm_platform_cookie

doc: |
  MS-RDPEV 2.1 Transport
  Dynamic channel "TSMF"
  NOTE: On a WDAG host, this channel cannot be opened by the guest as the channel
  listener is never created.
  mstscax!CMMListenerCallback::OnNewChannelConnection
  mstscax!CRimChannel::OnDataReceived
  mstscax!CRIMObjManager::OnDataReceived
  mstscax!CRIMObjectPool<CMemory>::GetPooledObject
  mstscax!CMemory::Initialize
  mstscax!CRIMStreamStub::OnDataReceived (could also be mstscax!CRIMStreamProxy::OnDataReceived)
  mstscax!CRIMObjectPool<CMessage>::GetPooledObject
  mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
  mstscax!CStubIMMClientNotifications<IMMClientNotifications>::Dispatch_Invoke

seq:
  - id: interface_id
    -orig-id: InterfaceValue
    type: u4
    enum: interface_id
    doc: |
      A 30-bit unsigned integer that represents the common identifier for the interface.
      The default value is 0x00000000. If the message uses this default interface ID, the
      message is interpreted for the main interface for which this channel has been
      instantiated. All other values MUST be retrieved either from an
      Interface Query response (QI_RSP), [MS-RDPEXPS] section2.2.2.1.2, or from responses
      that contain interface IDs.
      This ID is valid until an interface release (IFACE_RELEASE) message
      ([MS-RDPEXPS] section 2.2.2.2) is sent or received with that ID. After an IFACE_RELEASE
      message is processed, this ID is considered invalid.
  - id: message_id
    -orig-id: MessageId
    type: u4
    doc: |
      A 32-bit unsigned integer that represents a unique ID for the request or response pair.
      Requests and responses are matched based on this ID coupled with the InterfaceId.
  - id: function_id
    -orig-id: FunctionId
    type: u4
    doc: |
      A 32-bit unsigned integer. This field MUST be present in all packets except response packets.
      Its value is either used in interface manipulation messages or defined for a specific
      interface.
  - id: rimcall
    if: function_id < 100
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_rimcall::release.to_i': tsmf_rimcall_iface_release
        'function_id_rimcall::queryinterface.to_i': tsmf_rimcall_qi_req
  - id: capability_negotiator
    if: (function_id >= 100) and ((interface_id.to_i & ~0xc0000000) == interface_id::capability_negotiator.to_i)
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_capability_negotiator::rim_exchange_req.to_i': tsmf_rim_exchange_capability_request
  - id: client_notification
    if: (function_id >= 100) and ((interface_id.to_i & ~0xc0000000) == interface_id::client_notification.to_i)
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_client_notification::playback_ack.to_i': tsmf_playback_ack
        'function_id_client_notification::event.to_i': tsmf_client_event_notification
  - id: server_data
    if: (function_id >= 100) and ((interface_id.to_i & ~0xc0000000) == interface_id::server_data.to_i)
    type:
      switch-on: function_id
      -name: type
      cases:
        'function_id_server_data::exchange_capabilities_req.to_i': tsmf_exchange_capabilities_req
        'function_id_server_data::set_channel_params.to_i': tsmf_set_channel_params
        'function_id_server_data::add_stream.to_i': tsmf_add_stream
        'function_id_server_data::on_sample.to_i': tsmf_on_sample
        'function_id_server_data::set_video_window.to_i': tsmf_set_video_window
        'function_id_server_data::on_new_presentation.to_i': tsmf_new_presentation
        'function_id_server_data::shutdown_presentation_req.to_i': tsmf_shutdown_presentation_req
        'function_id_server_data::set_topology_req.to_i': tsmf_set_topology_req
        'function_id_server_data::check_format_support_req.to_i': tsmf_check_format_support_req
        'function_id_server_data::on_playback_started.to_i': tsmf_on_playback_started
        'function_id_server_data::on_playback_paused.to_i': tsmf_on_playback_paused
        'function_id_server_data::on_playback_stopped.to_i': tsmf_on_playback_stopped
        'function_id_server_data::on_playback_restarted.to_i': tsmf_on_playback_restarted
        'function_id_server_data::on_playback_rate_changed.to_i': tsmf_on_playback_rate_changed
        'function_id_server_data::on_flush.to_i': tsmf_on_flush
        'function_id_server_data::on_stream_volume.to_i': tsmf_on_stream_volume
        'function_id_server_data::on_channel_volume.to_i': tsmf_on_channel_volume
        'function_id_server_data::on_end_of_stream.to_i': tsmf_on_end_of_stream
        'function_id_server_data::set_allocator.to_i': tsmf_set_allocator
        'function_id_server_data::notify_preroll.to_i': tsmf_notify_preroll
        'function_id_server_data::update_geometry_info.to_i': tsmf_update_geometry_info
        'function_id_server_data::remove_stream.to_i': tsmf_remove_stream
        'function_id_server_data::set_source_video_rect.to_i': tsmf_set_source_video_rectangle

enums:
  interface_id:
    0x00000000:
      id: server_data
      doc: |
        MS-RDPEV 2.2.5 Server Data Interface
        The server data interface is identified by the default interface ID 0x00000000.
        The default interface does not require Query Interface Request (QI_REQ) or
        Query Interface Response (QI_RSP) messages [MS-RDPEXPS] to initialize the interface.
    0x00000001:
      id: client_notification
      doc: |
        MS-RDPEV 2.2.4 Client Notifications Interface
        The client notifications interface is identified by the interface ID 0x00000001.
        The client notifications interface is used by the client to send playback acknowledgment.
    0x00000002:
      id: capability_negotiator
      doc: |
        MS-RDPEV 2.2.3 Interface Manipulation Exchange Capabilities Interface
        The Exchange Capabilities Interface is identified by the interface ID 0x00000002.
        This interface is used to exchange the client's and the server's capabilities for
        interface manipulation.
  stream_id:
    0x80000000:
      id: stub
      -orig-id: STREAM_ID_STUB
      doc: |
        Indicates that the SHARED_MSG_HEADER is being used in a response message.
    0x40000000:
      id: proxy
      -orig-id: STREAM_ID_PROXY
      doc: |
        Indicates that the SHARED_MSG_HEADER is not being used in a response message.
    0x00000000:
      id: none
      -orig-id: STREAM_ID_NONE
      doc: |
        Indicates that the SHARED_MSG_HEADER is being used for interface manipulation
        capabilities exchange as specified in section 2.2.3. This value MUST NOT be used
        for any other messages.
  function_id_rimcall:
    0x00000001:
      id: release
      -orig-id: RIMCALL_RELEASE
      doc: |
        Release the given interface ID.
    0x00000002:
      id: queryinterface
      -orig-id: RIMCALL_QUERYINTERFACE
      doc: |
        Query for a new interface.
  function_id_capability_negotiator:
    0x00000100:
      id: rim_exchange_req
      -orig-id: RIM_EXCHANGE_CAPABILITY_REQUEST
      doc: |
        The server sends the Interface Manipulation Exchange Capabilities Request message.
  function_id_client_notification:
    0x00000100:
      id: playback_ack
      -orig-id: PLAYBACK_ACK
      doc: |
        The client sends the Playback Acknowledgment message.
    0x00000101:
      id: event
      -orig-id: CLIENT_EVENT_NOTIFICATION
      doc: |
        The client sends the Client Event Notification message.
  function_id_server_data:
    0x00000100:
      id: exchange_capabilities_req
      -orig-id: EXCHANGE_CAPABILITIES_REQ
      doc: The server sends the Exchange Capabilities Request message.
    0x00000101:
      id: set_channel_params
      -orig-id: SET_CHANNEL_PARAMS
      doc: The server sends the Set Channel Parameters message.
    0x00000102:
      id: add_stream
      -orig-id: ADD_STREAM
      doc: The server sends the Add Stream message.
    0x00000103:
      id: on_sample
      -orig-id: ON_SAMPLE
      doc: The server sends the On Sample message.
    0x00000104:
      id: set_video_window
      -orig-id: SET_VIDEO_WINDOW
      doc: The server sends the Set Video Window message.
    0x00000105:
      id: on_new_presentation
      -orig-id: ON_NEW_PRESENTATION
      doc: The server sends the New Presentation message.
    0x00000106:
      id: shutdown_presentation_req
      -orig-id: SHUTDOWN_PRESENTATION_REQ
      doc: The server sends the Shut Down Presentation Request message.
    0x00000107:
      id: set_topology_req
      -orig-id: SET_TOPOLOGY_REQ
      doc: The server sends the Set Topology Request message.
    0x00000108:
      id: check_format_support_req
      -orig-id: CHECK_FORMAT_SUPPORT_REQ
      doc: The server sends the Check Format Support Request message.
    0x00000109:
      id: on_playback_started
      -orig-id: ON_PLAYBACK_STARTED
      doc: The server sends the On Playback Started message.
    0x0000010a:
      id: on_playback_paused
      -orig-id: ON_PLAYBACK_PAUSED
      doc: The server sends the On Playback Paused message.
    0x0000010b:
      id: on_playback_stopped
      -orig-id: ON_PLAYBACK_STOPPED
      doc: The server sends the On Playback Stopped message.
    0x0000010c:
      id: on_playback_restarted
      -orig-id: ON_PLAYBACK_RESTARTED
      doc: The server sends the On Playback Restarted message.
    0x0000010d:
      id: on_playback_rate_changed
      -orig-id: ON_PLAYBACK_RATE_CHANGED
      doc: The server sends the On Playback Rate Change message.
    0x0000010e:
      id: on_flush
      -orig-id: ON_FLUSH
      doc: The server sends the On Flush message.
    0x0000010f:
      id: on_stream_volume
      -orig-id: ON_STREAM_VOLUME
      doc: The server sends the On Stream Volume message.
    0x00000110:
      id: on_channel_volume
      -orig-id: ON_CHANNEL_VOLUME
      doc: The server sends the On Channel Volume message.
    0x00000111:
      id: on_end_of_stream
      -orig-id: ON_END_OF_STREAM
      doc: The server sends the On End of Stream message.
    0x00000112:
      id: set_allocator
      -orig-id: SET_ALLOCATOR
      doc: The server sends the Set Allocator Properties message.
    0x00000113:
      id: notify_preroll
      -orig-id: NOTIFY_PREROLL
      doc: The server sends the Notify Preroll message.
    0x00000114:
      id: update_geometry_info
      -orig-id: UPDATE_GEOMETRY_INFO
      doc: The server sends the Update Geometry Information message.
    0x00000115:
      id: remove_stream
      -orig-id: REMOVE_STREAM
      doc: The server sends the Remove Stream message.
    0x00000116:
      id: set_source_video_rect
      -orig-id: SET_SOURCE_VIDEO_RECT
      doc: The server sends the Set Source Video Rectangle message.

types:
  tsmf_rimcall_qi_req:
    doc: |
      MS-RDPEXPS 2.2.2.1.1 Query Interface Request (QI_REQ)
      The QI_REQ request message is sent from either the client side or the server side,
      and is used to request a new interface ID.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CProxyIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::QueryInterface
      mstscax!CProxyIMMClientNotifications<IMMClientNotifications>::QueryInterface
      mstscax!CProxyIMMServerData<IMMServerData>::QueryInterface
      mstscax!CMessage::WriteIUnknown
      mstscax!CRIMObjManager::GetIdFromObject
    seq:
      - id: new_interface_guid
        -orig-id: NewInterfaceGUID
        type: rdp_guid
        doc: |
          A 16-byte GUID that identifies the new interface.

  tsmf_rimcall_iface_release:
    doc: |
      MS-RDPEXPS 2.2.2.2 Interface Release (IFACE_RELEASE)
      Terminates the lifetime of the interface. This message is one-way only.
      mstscax!CStub<IUnknown>::Invoke (often optimized to only one CStub<*****>::Invoke function)
      mstscax!CMessage::RemoveIUnknown
      mstscax!CRIMObjManager::RemoveObject
      (no data)

  tsmf_rim_exchange_capability_request:
    doc: |
      MS-RDPEV 2.2.3.1 Interface Manipulation Exchange Capabilities Request (RIM_EXCHANGE_CAPABILITY_REQUEST)
      This message is used by the server to request interface manipulation capabilities from the client.
      mstscax!CStubIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::Dispatch_Invoke
      mstscax!CStubIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::Invoke_ExchangeCapabilities
      mstscax!CProxyIRIMCapabilitiesNegotiator<IRIMCapabilitiesNegotiator>::ExchangeCapabilities
    seq:
      - id: capability_value
        -orig-id: CapabilityValue
        type: u4
        enum: rim_capability
        doc: |
          A 32-bit unsigned integer that identifies the server's capability.
    enums:
      rim_capability:
        0x00000001:
          id: version_01
          doc: |
            The capability to indicate the basic support for interface manipulation.
            This capability MUST be present in the message.

  tsmf_playback_ack:
    doc: |
      MS-RDPEV 2.2.4.1 Playback Acknowledgment Message (PLAYBACK_ACK)
      The PLAYBACK_ACK message is sent from the client to the server to acknowledge the
      media data that has already been played.
      mstscax!CStubIMMClientNotifications<IMMClientNotifications>::Dispatch_Invoke
      mstscax!CStubIMMClientNotifications<IMMClientNotifications>::Invoke_PlaybackAck
      mstscax!CProxyIMMClientNotifications<IMMClientNotifications>::PlaybackAck
    seq:
      - id: stream_id
        -orig-id: StreamId
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the stream ID of the sample that is
          being acknowledged.
      - id: data_duration
        -orig-id: DataDuration
        type: u8
        doc: |
          A 64-bit unsigned integer that indicates the calculated data duration of the
          sample that is being acknowledged. This field MUST be set to the ThrottleDuration
          field of the TS_MM_DATA_SAMPLE structure of the sample that is being acknowledged.
      - id: data_size
        -orig-id: cbData
        type: u8
        doc: |
          A 64-bit unsigned integer that indicates the data size of the sample that is being
          acknowledged. This field MUST be set to the cbData field of the TS_MM_DATA_SAMPLE
          structure of the sample that is being acknowledged.

  tsmf_client_event_notification:
    doc: |
      MS-RDPEV 2.2.4.2 Client Event Notification Message (CLIENT_EVENT_NOTIFICATION)
      The CLIENT_EVENT_NOTIFICATION message is sent from the client to the server whenever
      an important client event happens.
      mstscax!CStubIMMClientNotifications<IMMClientNotifications>::Dispatch_Invoke
      mstscax!CStubIMMClientNotifications<IMMClientNotifications>::Invoke_ClientEventNotification
      mstscax!CProxyIMMClientNotifications<IMMClientNotifications>::ClientEventNotification
    seq:
      - id: stream_id
        -orig-id: StreamId
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the stream ID where the event originated.
      - id: event_id
        -orig-id: EventId
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the type of event the client is raising.
      - id: data_size
        -orig-id: cbData
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates the number of bytes in the pBlob field.
      - id: data
        -orig-id: pBlob
        size: data_size
        doc: |
          An array of bytes that contains data relevant to whichever event ID is passed.

  tsmf_set_channel_params:
    doc: |
      MS-RDPEV 2.2.5.1.1 Set Channel Parameters Message (SET_CHANNEL_PARAMS)
      This message is used by the server to set channel parameters.
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke
      mstscax!CStubIMMServerData<IMMServerData>::Invoke_SetChannelParameters
      mstscax!CProxyIMMServerData<IMMServerData>::SetChannelParameters
      mstscax!CMMServerDataHandler::SetChannelParameters
    seq:
      - id: presentation_id
        -orig-id: PresentationId
        type: rdp_guid
        doc: |
          A 16-byte GUID that identifies the presentation.
      - id: stream_id
        -orig-id: StreamId
        type: u4
        doc: |
          A 32-bit unsigned integer that indicates channel identifier.
          There MUST be only one channel with the identifier value 0x00000000,
          and it MUST NOT be used for data-streaming sequence messages.

  tsmf_exchange_capabilities_req:
    doc: |
      MS-RDPEV 2.2.5.1.2 Exchange Capabilities Request Message (EXCHANGE_CAPABILITIES_REQ)
      This message is used by the server to exchange its capabilities with the client.
      NOTE: First capability type must be 1 with a version no. of 2.
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke
      mstscax!CStubIMMServerData<IMMServerData>::Invoke_ExchangeCapabilities
      mstscax!CProxyReceive_TSMM_CAPABILITY
      mstscax!CProxyWrite_TSMM_CAPABILITY
      mstscax!CMMServerDataHandler::ExchangeCapabilities
      mstscax!CMMPlayerManager::ExchangeCapabilities
      mstscax!HandleCommonRemoteCapabilities
      mstscax!IsDShowInstalled
      mstscax!CMMPlayerManager::IsAudioDevicePresentAndEnabled
      mstscax!PopulateCommonHostCapabilities
      mstscax!CMMPlayerManager::GetNetworkLatency
    seq:
      - id: num_host_capabilities
        -orig-id: numHostCapabilities
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST contain the number of TSMM_CAPABILITIES
          structures in the pHostCapabilityArray field.
      - id: host_capability_array
        -orig-id: pHostCapabilities
        type: tsmm_capabilities
        repeat: expr
        repeat-expr: num_host_capabilities
        doc: |
          An array of TSMM_CAPABILITIES structures, each containing the capabilities for the
          server.

  tsmf_new_presentation:
    doc: |
      MS-RDPEV 2.2.5.2.1 New Presentation Message (NEW_PRESENTATION)
      This message is used by the server to notify the client of a new presentation.
      mstscax!CProxyIMMServerData<IMMServerData>::OnNewPresentation
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke
      mstscax!CStubIMMServerData<IMMServerData>::Invoke_OnNewPresentation
      mstscax!CMMServerDataHandler::OnNewPresentation
      mstscax!CMMPlayerManager::AddPlayer
      mstscax!CTSMFPlayer::CreateInstance
      mstscax!CTSMFPlayer::CTSMFPlayer
      mstscax!CTSDXPlayer::CreateInstance
      mstscax!CTSDXPlayer::CTSDXPlayer
      mstscax!CBasePlayer::CBasePlayer
      mstscax!CBasePlayer::Initialize
    seq:
      - id: presentation_id
        -orig-id: PresentationId
        type: rdp_guid
        doc: |
          A 16-byte GUID ([MS-DTYP] section 2.3.4.2) that identifies the presentation.
      - id: platform_cookie
        -orig-id: PlatformCookie
        type: tsmm_platform_cookie
        doc: |
          A 32-bit unsigned integer that indicates preferred platforms.
          This field SHOULD be set to values defined in TSMM_PLATFORM_COOKIE.

  tsmf_check_format_support_req:
    doc: |
      MS-RDPEV 2.2.5.2.2 Check Format Support Request Message (CHECK_FORMAT_SUPPORT_REQ)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke
      mstscax!CStubIMMServerData<IMMServerData>::Invoke_IsFormatSupported
      mstscax!CMMServerDataHandler::IsFormatSupported
      mstscax!CMMPlayerManager::CheckFormatSupportInMF
      mstscax!CBasePlayer::OnServerMediaFormat
      mstscax!CBasePlayer::OnServerMediaFormatAsyncCallback::Invoke
      mstscax!CTSMFPlayer::CreateInstance
      mstscax!CTSMFPlayer::OnServerMediaFormat
      mstscax!TsmfSource::AddMediaStream
      mstscax!TsmfSourceStream::SetMediaType
      mstscax!CTSMfWrapper::TS_MFInitMediaTypeFromAMMediaType
      mstscax!CTSMFPlayer::ApplyFormats
      mstscax!CTSMFPlayer::Shutdown
      mstscax!CTSMFPlayer::Release
      mstscax!CMMPlayerManager::CheckFormatSupportInDShow
      mstscax!CTSDXPlayer::OnServerMediaFormat
      mstscax!CTSDXPlayer::ApplyFormats
      mstscax!CTSDXPlayer::Shutdown
      mstscax!CTSDXPlayer::Release

  tsmf_add_stream:
    doc: |
      MS-RDPEV 2.2.5.2.4 Add Stream Message (ADD_STREAM)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_set_topology_req:
    doc: |
      MS-RDPEV 2.2.5.2.5 Set Topology Request Message (SET_TOPOLOGY_REQ)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_remove_stream:
    doc: |
      MS-RDPEV 2.2.5.2.7 Remove Stream Message (REMOVE_STREAM)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_shutdown_presentation_req:
    doc: |
      MS-RDPEV 2.2.5.2.8 Shut Down Presentation Request Message (SHUTDOWN_PRESENTATION_REQ)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_set_source_video_rectangle:
    doc: |
      MS-RDPEV 2.2.5.2.10 Set Source Video Rectangle Message (SET_SOURCE_VIDEO_RECTANGLE)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_playback_started:
    doc: |
      MS-RDPEV 2.2.5.3.1 On Playback Started Message (ON_PLAYBACK_STARTED)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_playback_paused:
    doc: |
      MS-RDPEV 2.2.5.3.2 On Playback Paused Message (ON_PLAYBACK_PAUSED)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_playback_restarted:
    doc: |
      MS-RDPEV 2.2.5.3.3 On Playback Restarted Message (ON_PLAYBACK_RESTARTED)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_playback_stopped:
    doc: |
      MS-RDPEV 2.2.5.3.4 On Playback Stopped Message (ON_PLAYBACK_STOPPED)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_playback_rate_changed:
    doc: |
      MS-RDPEV 2.2.5.3.5 On Playback Rate Changed Message (ON_PLAYBACK_RATE_CHANGED)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_set_allocator:
    doc: |
      MS-RDPEV 2.2.5.4.1 Set Allocator Properties Message (SET_ALLOCATOR)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_notify_preroll:
    doc: |
      MS-RDPEV 2.2.5.4.2 Notify Preroll Message (NOTIFY_PREROLL)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_sample:
    doc: |
      MS-RDPEV 2.2.5.4.3 On Sample Message (ON_SAMPLE)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_flush:
    doc: |
      MS-RDPEV 2.2.5.4.4 On Flush Message (ON_FLUSH)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_end_of_stream:
    doc: |
      MS-RDPEV 2.2.5.4.5 On End of Stream Message (ON_END_OF_STREAM)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_set_video_window:
    doc: |
      MS-RDPEV 2.2.5.5.1 Set Video Window Message (SET_VIDEO_WINDOW)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_update_geometry_info:
    doc: |
      MS-RDPEV 2.2.5.5.2 Update Geometry Information Message (UPDATE_GEOMETRY_INFO)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_stream_volume:
    doc: |
      MS-RDPEV 2.2.5.6.1 On Stream Volume Message (ON_STREAM_VOLUME)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke

  tsmf_on_channel_volume:
    doc: |
      MS-RDPEV 2.2.5.6.2 On Channel Volume Message (ON_CHANNEL_VOLUME)
      TODO
      mstscax!CStubIMMServerData<IMMServerData>::Dispatch_Invoke
