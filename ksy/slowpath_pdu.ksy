meta:
  id: slowpath_pdu
  endian: le
  imports:
    - slowpath_control_pdu

doc: |
  MS-RDPBCGR 2.2.8.1.1 Slow-Path (T.128) Formats
  MS-RDPBCGR 2.2.8.1.1.1.1 Share Control Header (TS_SHARECONTROLHEADER)
  T128 8.3 ASPDU formats
  The TS_SHARECONTROLHEADER header is a T.128 header ([T128] section 8.3).
  Messages are sent over a static channel.
  The channel ID is set in the CSL._SL.channelID member of the CSL class.
  mstscax!CCO::OnPacketReceived

seq:
  - id: data
    type: slowpath_control
    repeat: eos

types:
  slowpath_control:
    doc: |
      Contains either a FLOW PDU or a SLOWPATH CONTROL PDU.
      mstscax!CCO::OnPacketReceived
    seq:
      - id: total_length
        type: u2
        doc: |
          A 16-bit unsigned integer. The total length of the packet in bytes
          (the length includes the size of the Share Control Header). If the totalLength
          field equals 0x8000, then the Share Control Header and any data that follows MAY
          be interpreted as a T.128 FlowPDU as described in [T128] section 8.5
          (the ASN.1 structure definition is detailed in [T128] section 9.1) and MUST be
          ignored.
      - id: flow_value
        type: u2
        if: flow_pdu
        doc: |
          2-byte value that is unused by the client.
          The client does check for 0x41 and 0x42, but does nothing with these values.
      - id: data
        type: slowpath_control_pdu
        size: 'total_length > 2 ? total_length - 2 : 0'
        if: not flow_pdu
    instances:
      flow_pdu:
        value: total_length == 0x8000
