meta:
  id: ts_primary_drawing_order
  endian: le

doc: |
  MS-RDPEGDI 2.2.2.2.1.1.2 Primary Drawing Order (PRIMARY_DRAWING_ORDER)
  The PRIMARY_DRAWING_ORDER structure encapsulates a primary drawing order.
  All primary drawing orders employ special field encoding to reduce the
  number of bytes sent on the wire. Field encoding maintains a copy of the
  most recent field values that were used in each primary drawing order, in
  addition to common state information such as the last bounding rectangle
  used across all orders and the last order type. Only the fields that have
  changed are sent on the wire. This implies that all the fields in a primary
  drawing order are optional, their presence being denoted by the controlFlags
  and fieldFlags fields.
  mstscax!CUH::ProcessOrders
  mstscax!COD::OD_DecodeOrder

params:
  - id: control_flags
    type: u1
    doc: |
      An 8-bit, unsigned integer. A control byte that identifies the class of
      the drawing order and describes the fields that are included in the order
      and the type of coordinates being used.
  - id: param_order_type
    type: u1
    doc: |
      An 8-bit, unsigned integer. An optional identifier describing the type
      of primary drawing order. The initial value for the orderType agreed on
      by both the server and client is TS_ENC_PATBLT_ORDER (0x01) for the PatBlt
      primary drawing order.

seq:
  - id: field_flags_byte_0
    type: u1
    if: field_flags_size >= 1
  - id: field_flags_byte_1
    type: u1
    if: field_flags_size >= 2
  - id: field_flags_byte_2
    type: u1
    if: field_flags_size >= 3
  - id: bounds
    type: bounds
    if: (control_flags & 0x04) != 0
    doc: |
      A variable-length, 1-byte to 9-byte field. The presence of the optional bounds
      field is governed by the TS_BOUNDS (0x04) flag in the controlFlags field, which
      indicates that the order MUST have a bounding region applied. If the controlFlags
      field TS_ZERO_BOUNDS_DELTAS (0x20) flag is also set, the previous bounding rectangle
      MUST be used, as the bounds have not changed (this implies that the bounds field
      is not present). Otherwise, the bounds are encoded as an encoding description byte
      followed by one or more encoded bounds (written in the order: left, top, right, bottom).
      The description byte MUST contain a TS_BOUND_XXX or TS_BOUND_DELTA_XXX flag to
      describe each of the encoded bounds that are present.
  - id: order
    type:
      switch-on: order_type
      -name: type
      cases:
        'ts_enc::dstblt_order': ts_dstblt_order(control_flags, field_flags)
        'ts_enc::patblt_order': ts_patblt_order(control_flags, field_flags)
        'ts_enc::scrblt_order': ts_scrblt_order(control_flags, field_flags)
        'ts_enc::drawninegrid_order': ts_drawninegrid_order(control_flags, field_flags)
        'ts_enc::multi_drawninegrid_order': ts_multi_drawninegrid_order(control_flags, field_flags)
        'ts_enc::lineto_order': ts_lineto_order(control_flags, field_flags)
        'ts_enc::opaquerect_order': ts_opaquerect_order(control_flags, field_flags)
        'ts_enc::savebitmap_order': ts_savebitmap_order(control_flags, field_flags)
        'ts_enc::memblt_order': ts_memblt_order(control_flags, field_flags)
        'ts_enc::mem3blt_order': ts_mem3blt_order(control_flags, field_flags)
        'ts_enc::multi_dstblt_order': ts_multi_dstblt_order(control_flags, field_flags)
        'ts_enc::multi_patblt_order': ts_multi_patblt_order(control_flags, field_flags)
        'ts_enc::multi_scrblt_order': ts_multi_scrblt_order(control_flags, field_flags)
        'ts_enc::multi_opaquerect_order': ts_multi_opaquerect_order(control_flags, field_flags)
        'ts_enc::fast_index_order': ts_fast_index_order(control_flags, field_flags)
        'ts_enc::polygon_sc_order': ts_polygon_sc_order(control_flags, field_flags)
        'ts_enc::polygon_cb_order': ts_polygon_cb_order(control_flags, field_flags)
        'ts_enc::polyline_order': ts_polyline_order(control_flags, field_flags)
        'ts_enc::fast_glyph_order': ts_fast_glyph_order(control_flags, field_flags)
        'ts_enc::ellipse_sc_order': ts_ellipse_sc_order(control_flags, field_flags)
        'ts_enc::ellipse_cb_order': ts_ellipse_cb_order(control_flags, field_flags)
        'ts_enc::index_order': ts_index_order(control_flags, field_flags)

instances:
  order_type:
    value: param_order_type << 0
    enum: ts_enc
  delta_coordinates:
    value: (control_flags & 0x10) != 0
    doc: |
      Indicates that all of the Coord-type fields in the order (see section 2.2.2.2.1.1.1.1)
      are specified as 1-byte signed deltas from their previous values.
  zero_bounds_deltas:
    value: (control_flags & 0x20) != 0
    doc: |
      Indicates that the previous bounding rectangle MUST be used, as the bounds have not
      changed (this implies that the bounds field is not present). This flag is only applicable
      if the TS_BOUNDS (0x04) flag is set.
  zero_field_bytes:
    value: (control_flags >> 6) & 0x3
  z:
    value: param_order_type
  num_fields:
    value: 'z<=0?5:z<=1?12:z<=2?7:z<=6?0:z<=7?5:z<=8?7:z<=9?10:z<=10?7:z<=11?6:z<=12?0:z<=13?9:z<=14?16:z<=15?7:z<=16?14:z<=18?9:z<=19?15:z<=20?7:z<=21?13:z<=22?7:z<=23?0:z<=24?15:z<=25?7:z<=26?13:z<=27?22:z<=31?0:0'
  field_flags_size:
    value: ((num_fields + 8) >> 3) - zero_field_bytes
  field_flags_byte_0_u4:
    value: field_flags_byte_0 << 0
  field_flags_byte_1_u4:
    value: field_flags_byte_1 << 0
  field_flags_byte_2_u4:
    value: field_flags_byte_2 << 0
  field_flags:
    value: '(field_flags_size >= 1 ? (field_flags_byte_0_u4 << 0) & 0x0000ff: 0) | (field_flags_size >= 2 ? (field_flags_byte_1_u4 << 8) & 0x00ff00: 0) | (field_flags_size >= 3 ? (field_flags_byte_2_u4 << 16) & 0xff0000: 0)'
    doc: |
      A variable-length, 1-byte to 3-byte field. The optional fieldFlags field is used to
      indicate the presence of an order field in the encoded fields portion of the packet
      (represented by the primaryOrderData field). Each bit in the fieldFlags field functions
      as a flag and indicates if a particular order field is present.

enums:
  control_flag:
    0x04:
      id: ts_bounds
      doc: Indicates that the primary order has a bounding rectangle.
    0x08:
      id: ts_type_change
      doc: Indicates that the order type has changed and that the orderType field is present.
    0x10:
      id: ts_delta_coordinates
      doc: |
        Indicates that all of the Coord-type fields in the order (see section 2.2.2.2.1.1.1.1) are
        specified as 1-byte signed deltas from their previous values.
    0x20:
      id: ts_zero_bound_delta
      doc: |
        Indicates that the previous bounding rectangle MUST be used, as the bounds have not changed
        (this implies that the bounds field is not present). This flag is only applicable if the TS_BOUNDS
        (0x04) flag is set.
    0x40:
      id: ts_zero_field_byte_bit0
      doc: |
        Used in conjunction with the TS_ZERO_FIELD_BYTE_BIT1 (0x80) flag to form a 2-bit count (so maximum of 3)
        of the number of field flag bytes (present in the fieldFlags field) that are zero and not present,
        counted from the end of the set of field flag bytes. This flag is the least significant bit of the count.
    0x80:
      id: ts_zero_field_byte_bit1
      doc: |
        Used in conjunction with the TS_ZERO_FIELD_BYTE_BIT0 (0x40) flag to form a 2-bit count (so maximum of 3)
        of the number of field flag bytes (present in the fieldFlags field) that are zero and not present,
        counted from the end of the set of field flag bytes. This flag is the most significant bit of the count.
  ts_enc:
    0x00:
      id: dstblt_order
      doc: DstBlt (section 2.2.2.2.1.1.2.1) Primary Drawing Order.
    0x01:
      id: patblt_order
      doc: PatBlt (section 2.2.2.2.1.1.2.3) Primary Drawing Order.
    0x02:
      id: scrblt_order
      doc: ScrBlt (section 2.2.2.2.1.1.2.7) Primary Drawing Order.
    0x07:
      id: drawninegrid_order
      doc: DrawNineGrid (section 2.2.2.2.1.1.2.21) Primary Drawing Order.
    0x08:
      id: multi_drawninegrid_order
      doc: MultiDrawNineGrid (section 2.2.2.2.1.1.2.22) Primary Drawing Order.
    0x09:
      id: lineto_order
      doc: LineTo (section 2.2.2.2.1.1.2.11) Primary Drawing Order.
    0x0A:
      id: opaquerect_order
      doc: OpaqueRect (section 2.2.2.2.1.1.2.5) Primary Drawing Order.
    0x0B:
      id: savebitmap_order
      doc: SaveBitmap (section 2.2.2.2.1.1.2.12) Primary Drawing Order.
    0x0D:
      id: memblt_order
      doc: MemBlt (section 2.2.2.2.1.1.2.9) Primary Drawing Order.
    0x0E:
      id: mem3blt_order
      doc: Mem3Blt (section 2.2.2.2.1.1.2.10) Primary Drawing Order.
    0x0F:
      id: multi_dstblt_order
      doc: MultiDstBlt (section 2.2.2.2.1.1.2.2) Primary Drawing Order.
    0x10:
      id: multi_patblt_order
      doc: MultiPatBlt (section 2.2.2.2.1.1.2.4) Primary Drawing Order.
    0x11:
      id: multi_scrblt_order
      doc: MultiScrBlt (section 2.2.2.2.1.1.2.8) Primary Drawing Order.
    0x12:
      id: multi_opaquerect_order
      doc: MultiOpaqueRect (section 2.2.2.2.1.1.2.6) Primary Drawing Order.
    0x13:
      id: fast_index_order
      doc: FastIndex (section 2.2.2.2.1.1.2.14) Primary Drawing Order.
    0x14:
      id: polygon_sc_order
      doc: PolygonSC (section 2.2.2.2.1.1.2.16) Primary Drawing Order.
    0x15:
      id: polygon_cb_order
      doc: PolygonCB (section 2.2.2.2.1.1.2.17) Primary Drawing Order.
    0x16:
      id: polyline_order
      doc: Polyline (section 2.2.2.2.1.1.2.18) Primary Drawing Order.
    0x18:
      id: fast_glyph_order
      doc: FastGlyph (section 2.2.2.2.1.1.2.15) Primary Drawing Order.
    0x19:
      id: ellipse_sc_order
      doc: EllipseSC (section 2.2.2.2.1.1.2.19) Primary Drawing Order.
    0x1A:
      id: ellipse_cb_order
      doc: EllipseCB (section 2.2.2.2.1.1.2.20) Primary Drawing Order.
    0x1B:
      id: index_order
      doc: GlyphIndex (section 2.2.2.2.1.1.2.13) Primary Drawing Order.

types:
  bounds:
    seq:
      - id: bound_flags
        type: u1
      - id: bound_left
        type: u2
        if: (bound_flags & 0x01) != 0
        doc: |
          TS_BOUND_LEFT
          Indicates that the left bound is present and encoded as a 2-byte, little-endian ordered value.
      - id: bound_top
        type: u2
        if: (bound_flags & 0x02) != 0
        doc: |
          TS_BOUND_TOP
          Indicates that the top bound is present and encoded as a 2-byte, little-endian ordered value.
      - id: bound_right
        type: u2
        if: (bound_flags & 0x04) != 0
        doc: |
          TS_BOUND_RIGHT
          Indicates that the right bound is present and encoded as a 2-byte, little-endian ordered value.
      - id: bound_bottom
        type: u2
        if: (bound_flags & 0x08) != 0
        doc: |
          TS_BOUND_BOTTOM
          Indicates that the bottom bound is present and encoded as a 2-byte, little-endian ordered value.
      - id: bound_delta_left
        type: s1
        if: (bound_flags & 0x10) != 0
        doc: |
          TS_BOUND_DELTA_LEFT
          Indicates that the left bound is present and encoded as a 1-byte signed value used as an offset
          (-128 to 127) from the previous value.
      - id: bound_delta_top
        type: s1
        if: (bound_flags & 0x20) != 0
        doc: |
          TS_BOUND_DELTA_TOP
          Indicates that the top bound is present and encoded as a 1-byte signed value used as an offset
          (-128 to 127) from the previous value.
      - id: bound_delta_right
        type: s1
        if: (bound_flags & 0x40) != 0
        doc: |
          TS_BOUND_DELTA_RIGHT
          Indicates that the right bound is present and encoded as a 1-byte signed value used as an offset
          (-128 to 127) from the previous value.
      - id: bound_delta_bottom
        type: s1
        if: (bound_flags & 0x80) != 0
        doc: |
          TS_BOUND_DELTA_BOTTOM
          Indicates that the bottom bound is present and encoded as a 1-byte signed value used as an offset
          (-128 to 127) from the previous value.

  cord_field:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.1 Coord Field (COORD_FIELD)
      The COORD_FIELD structure is used to describe a value in the range -32768 to 32767.
      - id: control_flags
        type: u1
    params:
      - id: control_flags
        type: u1
    seq:
      - id: value_low
        type: u1
      - id: value_high
        type: u1
        if: (control_flags & 0x10) == 0
    instances:
      value_low_u2:
        value: value_low << 0
      value_high_u2:
        value: value_high << 0
      value:
        value: '((control_flags & 0x10) == 0 ? (value_high_u2 << 8) & 0xff00 : 0) | (value_low_u2 & 0x00ff)'
        doc: |
          A signed, 1-byte or 2-byte value that describes a coordinate in the range
          -32768 to 32767.
          When the controlFlags field (see section 2.2.2.2.1.1.2) of the primary drawing
          order that contains the COORD_FIELD structure has the TS_DELTA_COORDINATES flag
          (0x10) set, the signedValue field MUST contain a signed 1-byte value. If the
          TS_DELTA_COORDINATES flag is not set, the signedValue field MUST contain a 2-byte
          signed value.
          The 1-byte format contains a signed delta from the previous value of the Coord field.
          To obtain the new value of the field, the decoder MUST increment the previous value
          of the field by the signed delta to produce the current value. The 2-byte format is
          simply the full value of the field that MUST replace the previous value.

  variable1_field:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.2 One-Byte Header Variable Field (VARIABLE1_FIELD)
      TheVARIABLE1_FIELD structure is used to encode a variable-length byte-stream
      that will hold a maximum of 255 bytes. This structure is always situated at
      the end of an order.
    seq:
      - id: size
        type: u1
        doc: |
          An 8-bit, unsigned integer. The number of bytes present in the rgbData field.
      - id: data
        size: size
        doc: |
          Variable-length, binary data. The size of this data, in bytes, is given by the cbData field.

  variable2_field:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.3 Two-Byte Header Variable Field (VARIABLE2_FIELD)
      The VARIABLE2_FIELD structure is used to encode a variable-length byte-stream
      that holds a maximum of 32,767 bytes. This structure is always situated at the
      end of an order.
    seq:
      - id: size
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of bytes present in the rgbData field.
      - id: data
        size: size
        doc: |
          Variable-length, binary data. The size of this data, in bytes, is given by the cbData field.

  rop2_operation:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.6 Binary Raster Operation (ROP2_OPERATION)
      The ROP2_OPERATION structure is used to define how the bits in a destination bitmap and
      a selected brush or pen are combined by using Boolean operators.
    seq:
      - id: rop2_operation
        type: u1
        enum: rop2
        doc: |
          An 8-bit, unsigned integer. A raster-operation code that describes a Boolean operation,
          in Reverse Polish Notation, to perform on the bits in a destination bitmap (D) and
          selected brush or pen (P). This operation is a combination of the AND (a), OR (o),
          NOT (n), and XOR (x) Boolean operators.
    enums:
      rop2:
        0x01:
          id: black
          doc: 0
        0x02:
          id: notmergepen
          doc: DPon
        0x03:
          id: masknotpen
          doc: DPna
        0x04:
          id: notcopypen
          doc: Pn
        0x05:
          id: maskpennot
          doc: PDna
        0x06:
          id: not
          doc: Dn
        0x07:
          id: xorpen
          doc: DPx
        0x08:
          id: notmaskpen
          doc: DPan
        0x09:
          id: maskpen
          doc: DPa
        0x0A:
          id: notxorpen
          doc: DPxn
        0x0B:
          id: nop
          doc: D
        0x0C:
          id: mergenotpen
          doc: DPno
        0x0D:
          id: copypen
          doc: P
        0x0E:
          id: mergepennot
          doc: PDno
        0x0F:
          id: mergepen
          doc: PDo
        0x10:
          id: white
          doc: 1

  rop3_operation_index:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.7 Ternary Raster Operation Index (ROP3_OPERATION_INDEX)
      The ROP3_OPERATION_INDEX structure is used to define how the bits in a source
      bitmap, destination bitmap, and a selected brush or pen are combined by using
      Boolean operators.
    seq:
      - id: rop3_index
        type: u1
        enum: rop3
        doc: |
          An 8-bit, unsigned integer. This field contains an index of a raster operation
          code that describes a Boolean operation, in Reverse Polish Notation, to
          perform on the bits in a source bitmap (S), destination bitmap (D), and
          selected brush or pen (P). This operation is a combination of the AND (a),
          OR (o), NOT (n), and XOR (x) Boolean operators.
    enums:
      rop3:
        0x00:
          id: blackness
          doc: |
            ROP: 0x00000042 (BLACKNESS)
            RPN: 0
        0x01:
          id: dpsoon
          doc: |
            ROP: 0x00010289
            RPN: DPSoon
        0x02:
          id: dpsona
          doc: |
            ROP: 0x00020C89
            RPN: DPSona
        0x03:
          id: pson
          doc: |
            ROP: 0x000300AA
            RPN: PSon
        0x04:
          id: sdpona
          doc: |
            ROP: 0x00040C88
            RPN: SDPona
        0x05:
          id: dpon
          doc: |
            ROP: 0x000500A9
            RPN: DPon
        0x06:
          id: pdsxnon
          doc: |
            ROP: 0x00060865
            RPN: PDSxnon
        0x07:
          id: pdsaon
          doc: |
            ROP: 0x000702C5
            RPN: PDSaon
        0x08:
          id: sdpnaa
          doc: |
            ROP: 0x00080F08
            RPN: SDPnaa
        0x09:
          id: pdsxon
          doc: |
            ROP: 0x00090245
            RPN: PDSxon
        0x0a:
          id: dpna
          doc: |
            ROP: 0x000A0329
            RPN: DPna
        0x0b:
          id: psdnaon
          doc: |
            ROP: 0x000B0B2A
            RPN: PSDnaon
        0x0c:
          id: spna
          doc: |
            ROP: 0x000C0324
            RPN: SPna
        0x0d:
          id: pdsnaon
          doc: |
            ROP: 0x000D0B25
            RPN: PDSnaon
        0x0e:
          id: pdsonon
          doc: |
            ROP: 0x000E08A5
            RPN: PDSonon
        0x0f:
          id: pn
          doc: |
            ROP: 0x000F0001
            RPN: Pn
        0x10:
          id: pdsona
          doc: |
            ROP: 0x00100C85
            RPN: PDSona
        0x11:
          id: dson
          doc: |
            ROP: 0x001100A6 (NOTSRCERASE)
            RPN: DSon
        0x12:
          id: sdpxnon
          doc: |
            ROP: 0x00120868
            RPN: SDPxnon
        0x13:
          id: sdpaon
          doc: |
            ROP: 0x001302C8
            RPN: SDPaon
        0x14:
          id: dpsxnon
          doc: |
            ROP: 0x00140869
            RPN: DPSxnon
        0x15:
          id: dpsaon
          doc: |
            ROP: 0x001502C9
            RPN: DPSaon
        0x16:
          id: psdpsanaxx
          doc: |
            ROP: 0x00165CCA
            RPN: PSDPSanaxx
        0x17:
          id: sspxdsxaxn
          doc: |
            ROP: 0x00171D54
            RPN: SSPxDSxaxn
        0x18:
          id: spxpdxa
          doc: |
            ROP: 0x00180D59
            RPN: SPxPDxa
        0x19:
          id: sdpsanaxn
          doc: |
            ROP: 0x00191CC8
            RPN: SDPSanaxn
        0x1a:
          id: pdspaox
          doc: |
            ROP: 0x001A06C5
            RPN: PDSPaox
        0x1b:
          id: sdpsxaxn
          doc: |
            ROP: 0x001B0768
            RPN: SDPSxaxn
        0x1c:
          id: psdpaox
          doc: |
            ROP: 0x001C06CA
            RPN: PSDPaox
        0x1d:
          id: dspdxaxn
          doc: |
            ROP: 0x001D0766
            RPN: DSPDxaxn
        0x1e:
          id: pdsox
          doc: |
            ROP: 0x001E01A5
            RPN: PDSox
        0x1f:
          id: pdsoan
          doc: |
            ROP: 0x001F0385
            RPN: PDSoan
        0x20:
          id: dpsnaa
          doc: |
            ROP: 0x00200F09
            RPN: DPSnaa
        0x21:
          id: sdpxon
          doc: |
            ROP: 0x00210248
            RPN: SDPxon
        0x22:
          id: dsna
          doc: |
            ROP: 0x00220326
            RPN: DSna
        0x23:
          id: spdnaon
          doc: |
            ROP: 0x00230B24
            RPN: SPDnaon
        0x24:
          id: spxdsxa
          doc: |
            ROP: 0x00240D55
            RPN: SPxDSxa
        0x25:
          id: pdspanaxn
          doc: |
            ROP: 0x00251CC5
            RPN: PDSPanaxn
        0x26:
          id: sdpsaox
          doc: |
            ROP: 0x002606C8
            RPN: SDPSaox
        0x27:
          id: sdpsxnox
          doc: |
            ROP: 0x00271868
            RPN: SDPSxnox
        0x28:
          id: dpsxa
          doc: |
            ROP: 0x00280369
            RPN: DPSxa
        0x29:
          id: psdpsaoxxn
          doc: |
            ROP: 0x002916CA
            RPN: PSDPSaoxxn
        0x2a:
          id: dpsana
          doc: |
            ROP: 0x002A0CC9
            RPN: DPSana
        0x2b:
          id: sspxpdxaxn
          doc: |
            ROP: 0x002B1D58
            RPN: SSPxPDxaxn
        0x2c:
          id: spdsoax
          doc: |
            ROP: 0x002C0784
            RPN: SPDSoax
        0x2d:
          id: psdnox
          doc: |
            ROP: 0x002D060A
            RPN: PSDnox
        0x2e:
          id: psdpxox
          doc: |
            ROP: 0x002E064A
            RPN: PSDPxox
        0x2f:
          id: psdnoan
          doc: |
            ROP: 0x002F0E2A
            RPN: PSDnoan
        0x30:
          id: psna
          doc: |
            ROP: 0x0030032A
            RPN: PSna
        0x31:
          id: sdpnaon
          doc: |
            ROP: 0x00310B28
            RPN: SDPnaon
        0x32:
          id: sdpsoox
          doc: |
            ROP: 0x00320688
            RPN: SDPSoox
        0x33:
          id: sn
          doc: |
            ROP: 0x00330008 (NOTSRCCOPY)
            RPN: Sn
        0x34:
          id: spdsaox
          doc: |
            ROP: 0x003406C4
            RPN: SPDSaox
        0x35:
          id: spdsxnox
          doc: |
            ROP: 0x00351864
            RPN: SPDSxnox
        0x36:
          id: sdpox
          doc: |
            ROP: 0x003601A8
            RPN: SDPox
        0x37:
          id: sdpoan
          doc: |
            ROP: 0x00370388
            RPN: SDPoan
        0x38:
          id: psdpoax
          doc: |
            ROP: 0x0038078A
            RPN: PSDPoax
        0x39:
          id: spdnox
          doc: |
            ROP: 0x00390604
            RPN: SPDnox
        0x3a:
          id: spdsxox
          doc: |
            ROP: 0x003A0644
            RPN: SPDSxox
        0x3b:
          id: spdnoan
          doc: |
            ROP: 0x003B0E24
            RPN: SPDnoan
        0x3c:
          id: psx
          doc: |
            ROP: 0x003C004A
            RPN: PSx
        0x3d:
          id: spdsonox
          doc: |
            ROP: 0x003D18A4
            RPN: SPDSonox
        0x3e:
          id: spdsnaox
          doc: |
            ROP: 0x003E1B24
            RPN: SPDSnaox
        0x3f:
          id: psan
          doc: |
            ROP: 0x003F00EA
            RPN: PSan
        0x40:
          id: psdnaa
          doc: |
            ROP: 0x00400F0A
            RPN: PSDnaa
        0x41:
          id: dpsxon
          doc: |
            ROP: 0x00410249
            RPN: DPSxon
        0x42:
          id: sdxpdxa
          doc: |
            ROP: 0x00420D5D
            RPN: SDxPDxa
        0x43:
          id: spdsanaxn
          doc: |
            ROP: 0x00431CC4
            RPN: SPDSanaxn
        0x44:
          id: sdna
          doc: |
            ROP: 0x00440328 (SRCERASE)
            RPN: SDna
        0x45:
          id: dpsnaon
          doc: |
            ROP: 0x00450B29
            RPN: DPSnaon
        0x46:
          id: dspdaox
          doc: |
            ROP: 0x004606C6
            RPN: DSPDaox
        0x47:
          id: psdpxaxn
          doc: |
            ROP: 0x0047076A
            RPN: PSDPxaxn
        0x48:
          id: sdpxa
          doc: |
            ROP: 0x00480368
            RPN: SDPxa
        0x49:
          id: pdspdaoxxn
          doc: |
            ROP: 0x004916C5
            RPN: PDSPDaoxxn
        0x4a:
          id: dpsdoax
          doc: |
            ROP: 0x004A0789
            RPN: DPSDoax
        0x4b:
          id: pdsnox
          doc: |
            ROP: 0x004B0605
            RPN: PDSnox
        0x4c:
          id: sdpana
          doc: |
            ROP: 0x004C0CC8
            RPN: SDPana
        0x4d:
          id: sspxdsxoxn
          doc: |
            ROP: 0x004D1954
            RPN: SSPxDSxoxn
        0x4e:
          id: pdspxox
          doc: |
            ROP: 0x004E0645
            RPN: PDSPxox
        0x4f:
          id: pdsnoan
          doc: |
            ROP: 0x004F0E25
            RPN: PDSnoan
        0x50:
          id: pdna
          doc: |
            ROP: 0x00500325
            RPN: PDna
        0x51:
          id: dspnaon
          doc: |
            ROP: 0x00510B26
            RPN: DSPnaon
        0x52:
          id: dpsdaox
          doc: |
            ROP: 0x005206C9
            RPN: DPSDaox
        0x53:
          id: spdsxaxn
          doc: |
            ROP: 0x00530764
            RPN: SPDSxaxn
        0x54:
          id: dpsonon
          doc: |
            ROP: 0x005408A9
            RPN: DPSonon
        0x55:
          id: dn
          doc: |
            ROP: 0x00550009 (DSTINVERT)
            RPN: Dn
        0x56:
          id: dpsox
          doc: |
            ROP: 0x005601A9
            RPN: DPSox
        0x57:
          id: dpsoan
          doc: |
            ROP: 0x00570389
            RPN: DPSoan
        0x58:
          id: pdspoax
          doc: |
            ROP: 0x00580785
            RPN: PDSPoax
        0x59:
          id: dpsnox
          doc: |
            ROP: 0x00590609
            RPN: DPSnox
        0x5a:
          id: dpx
          doc: |
            ROP: 0x005A0049 (PATINVERT)
            RPN: DPx
        0x5b:
          id: dpsdonox
          doc: |
            ROP: 0x005B18A9
            RPN: DPSDonox
        0x5c:
          id: dpsdxox
          doc: |
            ROP: 0x005C0649
            RPN: DPSDxox
        0x5d:
          id: dpsnoan
          doc: |
            ROP: 0x005D0E29
            RPN: DPSnoan
        0x5e:
          id: dpsdnaox
          doc: |
            ROP: 0x005E1B29
            RPN: DPSDnaox
        0x5f:
          id: dpan
          doc: |
            ROP: 0x005F00E9
            RPN: DPan
        0x60:
          id: pdsxa
          doc: |
            ROP: 0x00600365
            RPN: PDSxa
        0x61:
          id: dspdsaoxxn
          doc: |
            ROP: 0x006116C6
            RPN: DSPDSaoxxn
        0x62:
          id: dspdoax
          doc: |
            ROP: 0x00620786
            RPN: DSPDoax
        0x63:
          id: sdpnox
          doc: |
            ROP: 0x00630608
            RPN: SDPnox
        0x64:
          id: sdpsoax
          doc: |
            ROP: 0x00640788
            RPN: SDPSoax
        0x65:
          id: dspnox
          doc: |
            ROP: 0x00650606
            RPN: DSPnox
        0x66:
          id: dsx
          doc: |
            ROP: 0x00660046 (SRCINVERT)
            RPN: DSx
        0x67:
          id: sdpsonox
          doc: |
            ROP: 0x006718A8
            RPN: SDPSonox
        0x68:
          id: dspdsonoxxn
          doc: |
            ROP: 0x006858A6
            RPN: DSPDSonoxxn
        0x69:
          id: pdsxxn
          doc: |
            ROP: 0x00690145
            RPN: PDSxxn
        0x6a:
          id: dpsax
          doc: |
            ROP: 0x006A01E9
            RPN: DPSax
        0x6b:
          id: psdpsoaxxn
          doc: |
            ROP: 0x006B178A
            RPN: PSDPSoaxxn
        0x6c:
          id: sdpax
          doc: |
            ROP: 0x006C01E8
            RPN: SDPax
        0x6d:
          id: pdspdoaxxn
          doc: |
            ROP: 0x006D1785
            RPN: PDSPDoaxxn
        0x6e:
          id: sdpsnoax
          doc: |
            ROP: 0x006E1E28
            RPN: SDPSnoax
        0x6f:
          id: pdsxnan
          doc: |
            ROP: 0x006F0C65
            RPN: PDSxnan
        0x70:
          id: pdsana
          doc: |
            ROP: 0x00700CC5
            RPN: PDSana
        0x71:
          id: ssdxpdxaxn
          doc: |
            ROP: 0x00711D5C
            RPN: SSDxPDxaxn
        0x72:
          id: sdpsxox
          doc: |
            ROP: 0x00720648
            RPN: SDPSxox
        0x73:
          id: sdpnoan
          doc: |
            ROP: 0x00730E28
            RPN: SDPnoan
        0x74:
          id: dspdxox
          doc: |
            ROP: 0x00740646
            RPN: DSPDxox
        0x75:
          id: dspnoan
          doc: |
            ROP: 0x00750E26
            RPN: DSPnoan
        0x76:
          id: sdpsnaox
          doc: |
            ROP: 0x00761B28
            RPN: SDPSnaox
        0x77:
          id: dsan
          doc: |
            ROP: 0x007700E6
            RPN: DSan
        0x78:
          id: pdsax
          doc: |
            ROP: 0x007801E5
            RPN: PDSax
        0x79:
          id: dspdsoaxxn
          doc: |
            ROP: 0x00791786
            RPN: DSPDSoaxxn
        0x7a:
          id: dpsdnoax
          doc: |
            ROP: 0x007A1E29
            RPN: DPSDnoax
        0x7b:
          id: sdpxnan
          doc: |
            ROP: 0x007B0C68
            RPN: SDPxnan
        0x7c:
          id: spdsnoax
          doc: |
            ROP: 0x007C1E24
            RPN: SPDSnoax
        0x7d:
          id: dpsxnan
          doc: |
            ROP: 0x007D0C69
            RPN: DPSxnan
        0x7e:
          id: spxdsxo
          doc: |
            ROP: 0x007E0955
            RPN: SPxDSxo
        0x7f:
          id: dpsaan
          doc: |
            ROP: 0x007F03C9
            RPN: DPSaan
        0x80:
          id: dpsaa
          doc: |
            ROP: 0x008003E9
            RPN: DPSaa
        0x81:
          id: spxdsxon
          doc: |
            ROP: 0x00810975
            RPN: SPxDSxon
        0x82:
          id: dpsxna
          doc: |
            ROP: 0x00820C49
            RPN: DPSxna
        0x83:
          id: spdsnoaxn
          doc: |
            ROP: 0x00831E04
            RPN: SPDSnoaxn
        0x84:
          id: sdpxna
          doc: |
            ROP: 0x00840C48
            RPN: SDPxna
        0x85:
          id: pdspnoaxn
          doc: |
            ROP: 0x00851E05
            RPN: PDSPnoaxn
        0x86:
          id: dspdsoaxx
          doc: |
            ROP: 0x008617A6
            RPN: DSPDSoaxx
        0x87:
          id: pdsaxn
          doc: |
            ROP: 0x008701C5
            RPN: PDSaxn
        0x88:
          id: dsa
          doc: |
            ROP: 0x008800C6 (SRCAND)
            RPN: DSa
        0x89:
          id: sdpsnaoxn
          doc: |
            ROP: 0x00891B08
            RPN: SDPSnaoxn
        0x8a:
          id: dspnoa
          doc: |
            ROP: 0x008A0E06
            RPN: DSPnoa
        0x8b:
          id: dspdxoxn
          doc: |
            ROP: 0x008B0666
            RPN: DSPDxoxn
        0x8c:
          id: sdpnoa
          doc: |
            ROP: 0x008C0E08
            RPN: SDPnoa
        0x8d:
          id: sdpsxoxn
          doc: |
            ROP: 0x008D0668
            RPN: SDPSxoxn
        0x8e:
          id: ssdxpdxax
          doc: |
            ROP: 0x008E1D7C
            RPN: SSDxPDxax
        0x8f:
          id: pdsanan
          doc: |
            ROP: 0x008F0CE5
            RPN: PDSanan
        0x90:
          id: pdsxna
          doc: |
            ROP: 0x00900C45
            RPN: PDSxna
        0x91:
          id: sdpsnoaxn
          doc: |
            ROP: 0x00911E08
            RPN: SDPSnoaxn
        0x92:
          id: dpsdpoaxx
          doc: |
            ROP: 0x009217A9
            RPN: DPSDPoaxx
        0x93:
          id: spdaxn
          doc: |
            ROP: 0x009301C4
            RPN: SPDaxn
        0x94:
          id: psdpsoaxx
          doc: |
            ROP: 0x009417AA
            RPN: PSDPSoaxx
        0x95:
          id: dpsaxn
          doc: |
            ROP: 0x009501C9
            RPN: DPSaxn
        0x96:
          id: dpsxx
          doc: |
            ROP: 0x00960169
            RPN: DPSxx
        0x97:
          id: psdpsonoxx
          doc: |
            ROP: 0x0097588A
            RPN: PSDPSonoxx
        0x98:
          id: sdpsonoxn
          doc: |
            ROP: 0x00981888
            RPN: SDPSonoxn
        0x99:
          id: dsxn
          doc: |
            ROP: 0x00990066
            RPN: DSxn
        0x9a:
          id: dpsnax
          doc: |
            ROP: 0x009A0709
            RPN: DPSnax
        0x9b:
          id: sdpsoaxn
          doc: |
            ROP: 0x009B07A8
            RPN: SDPSoaxn
        0x9c:
          id: spdnax
          doc: |
            ROP: 0x009C0704
            RPN: SPDnax
        0x9d:
          id: dspdoaxn
          doc: |
            ROP: 0x009D07A6
            RPN: DSPDoaxn
        0x9e:
          id: dspdsaoxx
          doc: |
            ROP: 0x009E16E6
            RPN: DSPDSaoxx
        0x9f:
          id: pdsxan
          doc: |
            ROP: 0x009F0345
            RPN: PDSxan
        0xa0:
          id: dpa
          doc: |
            ROP: 0x00A000C9
            RPN: DPa
        0xa1:
          id: pdspnaoxn
          doc: |
            ROP: 0x00A11B05
            RPN: PDSPnaoxn
        0xa2:
          id: dpsnoa
          doc: |
            ROP: 0x00A20E09
            RPN: DPSnoa
        0xa3:
          id: dpsdxoxn
          doc: |
            ROP: 0x00A30669
            RPN: DPSDxoxn
        0xa4:
          id: pdsponoxn
          doc: |
            ROP: 0x00A41885
            RPN: PDSPonoxn
        0xa5:
          id: pdxn
          doc: |
            ROP: 0x00A50065
            RPN: PDxn
        0xa6:
          id: dspnax
          doc: |
            ROP: 0x00A60706
            RPN: DSPnax
        0xa7:
          id: pdspoaxn
          doc: |
            ROP: 0x00A707A5
            RPN: PDSPoaxn
        0xa8:
          id: dpsoa
          doc: |
            ROP: 0x00A803A9
            RPN: DPSoa
        0xa9:
          id: dpsoxn
          doc: |
            ROP: 0x00A90189
            RPN: DPSoxn
        0xaa:
          id: d
          doc: |
            ROP: 0x00AA0029
            RPN: D
        0xab:
          id: dpsono
          doc: |
            ROP: 0x00AB0889
            RPN: DPSono
        0xac:
          id: spdsxax
          doc: |
            ROP: 0x00AC0744
            RPN: SPDSxax
        0xad:
          id: dpsdaoxn
          doc: |
            ROP: 0x00AD06E9
            RPN: DPSDaoxn
        0xae:
          id: dspnao
          doc: |
            ROP: 0x00AE0B06
            RPN: DSPnao
        0xaf:
          id: dpno
          doc: |
            ROP: 0x00AF0229
            RPN: DPno
        0xb0:
          id: pdsnoa
          doc: |
            ROP: 0x00B00E05
            RPN: PDSnoa
        0xb1:
          id: pdspxoxn
          doc: |
            ROP: 0x00B10665
            RPN: PDSPxoxn
        0xb2:
          id: sspxdsxox
          doc: |
            ROP: 0x00B21974
            RPN: SSPxDSxox
        0xb3:
          id: sdpanan
          doc: |
            ROP: 0x00B30CE8
            RPN: SDPanan
        0xb4:
          id: psdnax
          doc: |
            ROP: 0x00B4070A
            RPN: PSDnax
        0xb5:
          id: dpsdoaxn
          doc: |
            ROP: 0x00B507A9
            RPN: DPSDoaxn
        0xb6:
          id: dpsdpaoxx
          doc: |
            ROP: 0x00B616E9
            RPN: DPSDPaoxx
        0xb7:
          id: sdpxan
          doc: |
            ROP: 0x00B70348
            RPN: SDPxan
        0xb8:
          id: psdpxax
          doc: |
            ROP: 0x00B8074A
            RPN: PSDPxax
        0xb9:
          id: dspdaoxn
          doc: |
            ROP: 0x00B906E6
            RPN: DSPDaoxn
        0xba:
          id: dpsnao
          doc: |
            ROP: 0x00BA0B09
            RPN: DPSnao
        0xbb:
          id: dsno
          doc: |
            ROP: 0x00BB0226 (MERGEPAINT)
            RPN: DSno
        0xbc:
          id: spdsanax
          doc: |
            ROP: 0x00BC1CE4
            RPN: SPDSanax
        0xbd:
          id: sdxpdxan
          doc: |
            ROP: 0x00BD0D7D
            RPN: SDxPDxan
        0xbe:
          id: dpsxo
          doc: |
            ROP: 0x00BE0269
            RPN: DPSxo
        0xbf:
          id: dpsano
          doc: |
            ROP: 0x00BF08C9
            RPN: DPSano
        0xc0:
          id: psa
          doc: |
            ROP: 0x00C000CA (MERGECOPY)
            RPN: PSa
        0xc1:
          id: spdsnaoxn
          doc: |
            ROP: 0x00C11B04
            RPN: SPDSnaoxn
        0xc2:
          id: spdsonoxn
          doc: |
            ROP: 0x00C21884
            RPN: SPDSonoxn
        0xc3:
          id: psxn
          doc: |
            ROP: 0x00C3006A
            RPN: PSxn
        0xc4:
          id: spdnoa
          doc: |
            ROP: 0x00C40E04
            RPN: SPDnoa
        0xc5:
          id: spdsxoxn
          doc: |
            ROP: 0x00C50664
            RPN: SPDSxoxn
        0xc6:
          id: sdpnax
          doc: |
            ROP: 0x00C60708
            RPN: SDPnax
        0xc7:
          id: psdpoaxn
          doc: |
            ROP: 0x00C707AA
            RPN: PSDPoaxn
        0xc8:
          id: sdpoa
          doc: |
            ROP: 0x00C803A8
            RPN: SDPoa
        0xc9:
          id: spdoxn
          doc: |
            ROP: 0x00C90184
            RPN: SPDoxn
        0xca:
          id: dpsdxax
          doc: |
            ROP: 0x00CA0749
            RPN: DPSDxax
        0xcb:
          id: spdsaoxn
          doc: |
            ROP: 0x00CB06E4
            RPN: SPDSaoxn
        0xcc:
          id: s
          doc: |
            ROP: 0x00CC0020 (SRCCOPY)
            RPN: S
        0xcd:
          id: sdpono
          doc: |
            ROP: 0x00CD0888
            RPN: SDPono
        0xce:
          id: sdpnao
          doc: |
            ROP: 0x00CE0B08
            RPN: SDPnao
        0xcf:
          id: spno
          doc: |
            ROP: 0x00CF0224
            RPN: SPno
        0xd0:
          id: psdnoa
          doc: |
            ROP: 0x00D00E0A
            RPN: PSDnoa
        0xd1:
          id: psdpxoxn
          doc: |
            ROP: 0x00D1066A
            RPN: PSDPxoxn
        0xd2:
          id: pdsnax
          doc: |
            ROP: 0x00D20705
            RPN: PDSnax
        0xd3:
          id: spdsoaxn
          doc: |
            ROP: 0x00D307A4
            RPN: SPDSoaxn
        0xd4:
          id: sspxpdxax
          doc: |
            ROP: 0x00D41D78
            RPN: SSPxPDxax
        0xd5:
          id: dpsanan
          doc: |
            ROP: 0x00D50CE9
            RPN: DPSanan
        0xd6:
          id: psdpsaoxx
          doc: |
            ROP: 0x00D616EA
            RPN: PSDPSaoxx
        0xd7:
          id: dpsxan
          doc: |
            ROP: 0x00D70349
            RPN: DPSxan
        0xd8:
          id: pdspxax
          doc: |
            ROP: 0x00D80745
            RPN: PDSPxax
        0xd9:
          id: sdpsaoxn
          doc: |
            ROP: 0x00D906E8
            RPN: SDPSaoxn
        0xda:
          id: dpsdanax
          doc: |
            ROP: 0x00DA1CE9
            RPN: DPSDanax
        0xdb:
          id: spxdsxan
          doc: |
            ROP: 0x00DB0D75
            RPN: SPxDSxan
        0xdc:
          id: spdnao
          doc: |
            ROP: 0x00DC0B04
            RPN: SPDnao
        0xdd:
          id: sdno
          doc: |
            ROP: 0x00DD0228
            RPN: SDno
        0xde:
          id: sdpxo
          doc: |
            ROP: 0x00DE0268
            RPN: SDPxo
        0xdf:
          id: sdpano
          doc: |
            ROP: 0x00DF08C8
            RPN: SDPano
        0xe0:
          id: pdsoa
          doc: |
            ROP: 0x00E003A5
            RPN: PDSoa
        0xe1:
          id: pdsoxn
          doc: |
            ROP: 0x00E10185
            RPN: PDSoxn
        0xe2:
          id: dspdxax
          doc: |
            ROP: 0x00E20746
            RPN: DSPDxax
        0xe3:
          id: psdpaoxn
          doc: |
            ROP: 0x00E306EA
            RPN: PSDPaoxn
        0xe4:
          id: sdpsxax
          doc: |
            ROP: 0x00E40748
            RPN: SDPSxax
        0xe5:
          id: pdspaoxn
          doc: |
            ROP: 0x00E506E5
            RPN: PDSPaoxn
        0xe6:
          id: sdpsanax
          doc: |
            ROP: 0x00E61CE8
            RPN: SDPSanax
        0xe7:
          id: spxpdxan
          doc: |
            ROP: 0x00E70D79
            RPN: SPxPDxan
        0xe8:
          id: sspxdsxax
          doc: |
            ROP: 0x00E81D74
            RPN: SSPxDSxax
        0xe9:
          id: dspdsanaxxn
          doc: |
            ROP: 0x00E95CE6
            RPN: DSPDSanaxxn
        0xea:
          id: dpsao
          doc: |
            ROP: 0x00EA02E9
            RPN: DPSao
        0xeb:
          id: dpsxno
          doc: |
            ROP: 0x00EB0849
            RPN: DPSxno
        0xec:
          id: sdpao
          doc: |
            ROP: 0x00EC02E8
            RPN: SDPao
        0xed:
          id: sdpxno
          doc: |
            ROP: 0x00ED0848
            RPN: SDPxno
        0xee:
          id: dso
          doc: |
            ROP: 0x00EE0086 (SRCPAINT)
            RPN: DSo
        0xef:
          id: sdpnoo
          doc: |
            ROP: 0x00EF0A08
            RPN: SDPnoo
        0xf0:
          id: p
          doc: |
            ROP: 0x00F00021 (PATCOPY)
            RPN: P
        0xf1:
          id: pdsono
          doc: |
            ROP: 0x00F10885
            RPN: PDSono
        0xf2:
          id: pdsnao
          doc: |
            ROP: 0x00F20B05
            RPN: PDSnao
        0xf3:
          id: psno
          doc: |
            ROP: 0x00F3022A
            RPN: PSno
        0xf4:
          id: psdnao
          doc: |
            ROP: 0x00F40B0A
            RPN: PSDnao
        0xf5:
          id: pdno
          doc: |
            ROP: 0x00F50225
            RPN: PDno
        0xf6:
          id: pdsxo
          doc: |
            ROP: 0x00F60265
            RPN: PDSxo
        0xf7:
          id: pdsano
          doc: |
            ROP: 0x00F708C5
            RPN: PDSano
        0xf8:
          id: pdsao
          doc: |
            ROP: 0x00F802E5
            RPN: PDSao
        0xf9:
          id: pdsxno
          doc: |
            ROP: 0x00F90845
            RPN: PDSxno
        0xfa:
          id: dpo
          doc: |
            ROP: 0x00FA0089
            RPN: DPo
        0xfb:
          id: dpsnoo
          doc: |
            ROP: 0x00FB0A09 (PATPAINT)
            RPN: DPSnoo
        0xfc:
          id: pso
          doc: |
            ROP: 0x00FC008A
            RPN: PSo
        0xfd:
          id: psdnoo
          doc: |
            ROP: 0x00FD0A0A
            RPN: PSDnoo
        0xfe:
          id: dpsoo
          doc: |
            ROP: 0x00FE02A9
            RPN: DPSoo
        0xff:
          id: whiteness
          doc: |
            ROP: 0x00FF0062 (WHITENESS)
            RPN: 1

  ts_color:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.1.8 Generic Color (TS_COLOR)
      The TS_COLOR structure holds a 3-byte RGB color triplet (the red, green,
      and blue components necessary to reproduce a color in the additive RGB
      space) or a 1-byte palette index.
    seq:
      - id: red_or_palette_index
        type: u1
        doc: |
          An 8-bit, unsigned integer. RedOrPaletteIndex is used as a palette
          index for 16-color and 256-color palettized color schemes. If the
          RGB color scheme is in effect, this field contains the red RGB
          component. To determine whether a palettized or RGB color scheme
          is in effect, the client MUST examine the preferredBitsPerPixel
          field of the Bitmap Capability Set ([MS-RDPBCGR] section 2.2.7.1.2).
          If preferredBitsPerPixel is less than or equal to 8, then a
          palettized color scheme is in effect; otherwise, an RGB color scheme
          is in effect.
      - id: green
        type: u1
        doc: |
          An 8-bit, unsigned integer. Green contains the green RGB color component.
      - id: blue
        type: u1
        doc: |
          An 8-bit, unsigned integer. Blue contains the blue RGB color component.
    instances:
      red:
        value: red_or_palette_index
        doc: |
          An 8-bit, unsigned integer. Red contains the red RGB color component.
      palette_index:
        value: red_or_palette_index
        doc: |
          An 8-bit, unsigned integer. Contains the palette index.

  ts_dstblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.1 DstBlt (DSTBLT_ORDER)
      The DstBlt Primary Drawing Order is used to paint a rectangle by using a
      destination-only raster operation.
       - Encoding order number: 0 (0x00)
       - Negotiation order number: 0 (0x00)
       - Number of fields: 5
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 9 bytes
      mstscax!COD::ODHandleDstBlts
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).

  ts_patblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.3 PatBlt (PATBLT_ORDER)
      The PatBlt Primary Drawing Order is used to paint a rectangle by using a
      specified brush and three-way raster operation.
       - Encoding order number: 1 (0x01)
       - Negotiation order number: 1 (0x01)
       - Number of fields: 12
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 26 bytes
      Note that the negotiation order number for the PatBlt Primary Drawing 
      Order (0x01) is the same as that for the OpaqueRect Primary Drawing Order
      (section 2.2.2.2.1.1.2.5). Hence support for PatBlt implies support for
      OpaqueRect. The converse is also true.
      mstscax!COD::ODDecodePatBlt
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: back_color
        type: ts_color
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Background color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          Foreground color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The style of the brush used in the drawing operation.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Holds a brush hatch identifier or a Brush Cache index,
          depending on the contents of the BrushStyle field.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          A byte array of length 7. BrushExtra contains an array of bitmap bits that encodes
          the pixel pattern present in the top seven rows of the 8x8 pattern brush. The pixel
          pattern present in the bottom row is encoded in the BrushHatch field. The BrushExtra
          field is only present if the BrushStyle is set to BS_PATTERN (0x03). The rows are
          encoded in reverse order, that is, the pixels in the penultimate row are encoded in
          the first byte, and the pixels in the top row are encoded in the seventh byte. For
          example, a 45-degree downward sloping left-to-right line would be encoded in
          BrushExtra as { 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 } with BrushHatch containing
          the value 0x01 (the bottom row).

  ts_scrblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.7 ScrBlt (SCRBLT_ORDER)
      The ScrBlt Primary Drawing Order is used to perform a bit-block transfer from a source
      region to a destination region. The source surface is always the primary drawing surface,
      while the target surface is the current target surface—specified by the Switch Surface
      Alternate Secondary Drawing Order (section 2.2.2.2.1.3.3).
       - Encoding order number: 2 (0x02)
       - Negotiation order number: 2 (0x02)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 13 bytes
      mstscax!COD::ODHandleScrBlts
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: x_src
        type: cord_field(control_flags)
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          The x-coordinate of the source rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_src
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The y-coordinate of the source rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).

  ts_drawninegrid_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.21 DrawNineGrid (DRAWNINEGRID_ORDER)
      The DrawNineGrid Primary Drawing Order encodes a single NineGrid drawing command
      with a single bounding rectangle.
       - Encoding order number: 7 (0x07)
       - Negotiation order number: 7 (0x07)
       - Number of fields: 5
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 10 bytes
      mstscax!COD::ODHandleDrawNineGrid
      mstscax!CTSGraphicsDrawNineGridHandler::DrawNineGrid
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: src_left
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The left coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given by
          the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_top
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The top coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given by
          the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_right
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The right coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given
          by the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The bottom coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given
          by the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bitmap_id
        type: u2
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The index of the NineGrid Bitmap Cache entry wherein the bitmap and
          NineGrid transformation information are stored. This value MUST be greater than or equal to 0 and
          less than the maximum number of entries allowed in the NineGrid Bitmap Cache as specified by the
          drawNineGridCacheEntries field of the DrawNineGrid Cache Capability Set (section 2.2.1.2). The
          bitmap and transformation information stored in the cache MUST have already been cached in response
          to a Create NineGrid Bitmap (section 2.2.2.2.1.3.4) Alternate Secondary Drawing Order.

  ts_multi_drawninegrid_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.22 MultiDrawNineGrid (MULTI_DRAWNINEGRID_ORDER)
      The MultiDrawNineGrid Primary Drawing Order encodes a single NineGrid drawing command with multiple
      clipping rectangles.
       - Encoding order number: 8 (0x08)
       - Negotiation order number: 9 (0x09)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 396 bytes
      mstscax!COD::ODHandleMultiDrawNineGrid
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: src_left
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The left coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given by
          the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_top
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The top coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given by
          the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_right
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The right coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given
          by the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: src_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The bottom coordinate of the clipping rectangle to be applied to the bitmap stored at the entry given
          by the bitmapId field. The coordinate is specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bitmap_id
        type: u2
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The index of the NineGrid Bitmap Cache entry wherein the bitmap and
          NineGrid transformation information are stored. This value MUST be greater than or equal to 0 and
          less than the maximum number of entries allowed in the NineGrid Bitmap Cache as specified by the
          drawNineGridCacheEntries field of the DrawNineGrid Cache Capability Set (section 2.2.1.2). The
          bitmap and transformation information stored in the cache MUST have already been cached in response
          to a Create NineGrid Bitmap (section 2.2.2.2.1.3.4) Alternate Secondary Drawing Order.
      - id: delta_entries
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of bounding rectangles described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable2_field
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          A Two-Byte Header Variable Field (section 2.2.2.2.1.1.1.3) structure that encapsulates
          a Delta-Encoded Rectangles (section 2.2.2.2.1.1.1.5) structure that contains bounding
          rectangles to use when rendering the order. The number of rectangles described by the
          Delta-Encoded Rectangles structure is specified by the nDeltaEntries field.

  ts_lineto_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.11 LineTo (LINETO_ORDER)
      The LineTo Primary Drawing Order encodes a single line drawing order that is restricted
      to solid color lines, one pixel wide.
       - Encoding order number: 9 (0x09)
       - Negotiation order number: 8 (0x08)
       - Number of fields: 10
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 19 bytes
      mstscax!COD::ODDecodeLineTo
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: back_mode
        type: u2
        enum: back_mode
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          An unsigned, 16-bit integer. This field contains the background mix mode.
      - id: x_start
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The x-coordinate of the starting point of the line specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_start
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The y-coordinate of the starting point of the line specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: x_end
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The x-coordinate of the ending point of the line specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_end
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The y-coordinate of the ending point of the line specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: back_color
        type: ts_color
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          The background color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
          This field MUST be zeroed out.
      - id: rop2
        type: rop2_operation
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The binary raster operation to perform (see section 2.2.2.2.1.1.1.6).
      - id: pen_style
        type: u1
        enum: ps
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The drawing style of the pen. This field MUST be set to PS_SOLID (0x00).
      - id: pen_width
        type: u1
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The width of the pen. This field MUST be set to 0x01.
      - id: pen_color
        type: ts_color
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          The foreground color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
    enums:
      back_mode:
        0x0001:
          id: traparent
          doc: Background remains untouched.
        0x0002:
          id: opaque
          doc: Background is filled with the current background color before the pen is drawn.
      ps:
        0x00:
          id: solid
          doc: Pen style solid.

  ts_opaquerect_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.5 OpaqueRect (OPAQUERECT_ORDER)
      The OpaqueRect Primary Drawing Order is used to paint a rectangle by using an opaque brush.
       - Encoding order number: 10 (0x0A)
       - Negotiation order number: 1 (0x01)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 11 bytes
      Note that the negotiation order number for the OpaqueRect Primary Drawing Order (0x01) is the
      same as that for the PatBlt Primary Drawing Order (section 2.2.2.2.1.1.2.3). Hence support for
      OpaqueRect implies support for PatBlt. The converse is also true.
      mstscax!COD::ODDecodeOpaqueRect
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: red_or_palette_index
        type: u1
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Used as a palette index for 16-color and 256-color palettized color
          schemes. If the RGB color scheme is in effect, this field holds the red RGB component.
      - id: green
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The green RGB color component.
      - id: blue
        type: u1
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The blue RGB color component.
    instances:
      red:
        value: red_or_palette_index
        doc: An 8-bit, unsigned integer. The red RGB color component.
      palette_index:
        value: red_or_palette_index
        doc: An 8-bit, unsigned integer. Palette index for 16-color and 256-color palettized color schemes.

  ts_savebitmap_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.12 SaveBitmap (SAVEBITMAP_ORDER)
      The SaveBitmap Primary Drawing Order encodes a rectangle of the screen image for saving
      or restoring by the client.
       - Encoding order number: 11 (0x0B)
       - Negotiation order number: 11 (0x0B)
       - Number of fields: 6
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 13 bytes
      mstscax!COD::ODHandleSaveBitmap
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: saved_bitmap_position
        type: u4
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          A 32-bit, unsigned integer. Encoded start position of the rectangle in the Saved Bitmap
          that will be read from (in the case of a bitmap restore operation) or written to (in the
          case of a bitmap save operation), depending on the value of the Operation field.
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The left coordinate of the virtual desktop rectangle to save specified by using a Coord Field
          (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The top coordinate of the virtual desktop rectangle to save specified by using a Coord Field
          (section 2.2.2.2.1.1.1.1).
      - id: right_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The right inclusive coordinate of the virtual desktop rectangle to save specified by using a
          Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bottom_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The bottom inclusive coordinate of the virtual desktop rectangle to save specified by using a
          Coord Field (section 2.2.2.2.1.1.1.1).
      - id: operation
        type: u1
        enum: sv
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The operation to perform.
    instances:
      y_pos:
        value: (saved_bitmap_position / (480 * 20)) * 20
        doc: |
          The Y position in the Save Bitmap from which to tile the next bitmap is computed using the
          formulas in section 2.2.2.2.1.1.2.12.
      x_pos:
        value: (saved_bitmap_position % (480 * 20)) / 20
        doc: |
          The X position in the Save Bitmap from which to tile the next bitmap is computed using the
          formulas in section 2.2.2.2.1.1.2.12.
    enums:
      sv:
        0x00:
          id: savebits
          doc: Save bitmap operation.
        0x01:
          id: restorebits
          doc: Restore bitmap operation.

  ts_memblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.9 MemBlt (MEMBLT_ORDER)
      The MemBlt Primary Drawing Order is used to render a bitmap stored in the bitmap cache or
      offscreen bitmap cache to the screen.
       - Encoding order number: 13 (0x0D)
       - Negotiation order number: 3 (0x03)
       - Number of fields: 9
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 17 bytes
      mstscax!COD::ODDecodeMemBlt
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: cache_id
        type: u2
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The cacheId field contains the encoded bitmap cache ID and
          Color Table Cache entry.
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Left coordinate of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Top coordinate of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Width of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Index of the ternary raster operation to perform (see section 2.2.2.2.1.1.1.7). The resultant
          ROP3 operation MUST only depend on the destination and source bits (there MUST NOT be any dependence
          on pattern bits).
      - id: x_src
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The x-coordinate of the source rectangle within the source bitmap specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: y_src
        type: cord_field(control_flags)
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          The inverted y-coordinate of the source rectangle within the source bitmap specified by using a
          Coord Field (section 2.2.2.2.1.1.1.1). The actual value of the y-coordinate MUST be compute
          using the following formula:
            ActualYSrc = (SourceBitmapHeight - nHeight) - nYSrc
      - id: cache_index
        type: u2
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The index of the source bitmap in the bitmap cache specified by the
          cacheId field.

  ts_mem3blt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.10 Mem3Blt (MEM3BLT_ORDER)
      The Mem3Blt Primary Drawing Order is used to render a bitmap stored in the bitmap cache or offscreen
      bitmap cache to the screen by using a specified brush and three-way raster operation.
       - Encoding order number: 14 (0x0E)
       - Negotiation order number: 4 (0x04)
       - Number of fields: 16
       - Number of field encoding bytes: 3
       - Maximum encoded field length: 34 bytes
      mstscax!COD::ODHandleMem3Blt
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: cache_id
        type: u2
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The cacheId field contains the encoded bitmap cache ID and
          Color Table Cache entry.
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Left coordinate of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Top coordinate of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Width of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the blit rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Index of the ternary raster operation to perform (see section 2.2.2.2.1.1.1.7). The resultant
          ROP3 operation MUST only depend on the destination and source bits (there MUST NOT be any dependence
          on pattern bits).
      - id: x_src
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The x-coordinate of the source rectangle within the source bitmap specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: y_src
        type: cord_field(control_flags)
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          The inverted y-coordinate of the source rectangle within the source bitmap specified by using a
          Coord Field (section 2.2.2.2.1.1.1.1). The actual value of the y-coordinate MUST be compute
          using the following formula:
            ActualYSrc = (SourceBitmapHeight - nHeight) - nYSrc
      - id: back_color
        type: ts_color
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          Background color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          Foreground color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The style of the brush used in the drawing operation.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 13) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Holds a brush hatch identifier or a Brush Cache index,
          depending on the contents of the BrushStyle field.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 14) & 0x1) != 0
        doc: |
          A byte array of length 7. BrushExtra contains an array of bitmap bits that encodes
          the pixel pattern present in the top seven rows of the 8x8 pattern brush. The pixel
          pattern present in the bottom row is encoded in the BrushHatch field. The BrushExtra
          field is only present if the BrushStyle is set to BS_PATTERN (0x03). The rows are
          encoded in reverse order, that is, the pixels in the penultimate row are encoded in
          the first byte, and the pixels in the top row are encoded in the seventh byte. For
          example, a 45-degree downward sloping left-to-right line would be encoded in
          BrushExtra as { 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 } with BrushHatch containing
          the value 0x01 (the bottom row).
      - id: cache_index
        type: u2
        if: ((field_flags >> 15) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. The index of the source bitmap in the bitmap cache specified by the
          cacheId field.

  ts_multi_dstblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.2 MultiDstBlt (MULTI_DSTBLT_ORDER)
      The MultiDstBlt Primary Drawing Order is used to paint multiple rectangles by using a
      destination-only raster operation.
       - Encoding order number: 15 (0x0F)
       - Negotiation order number: 15 (0x0F)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 395 bytes
      mstscax!COD::ODHandleDstBlts
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: delta_entries
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of bounding rectangles described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable2_field
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          A Two-Byte Header Variable Field (section 2.2.2.2.1.1.1.3) structure that encapsulates
          a Delta-Encoded Rectangles (section 2.2.2.2.1.1.1.5) structure that contains bounding
          rectangles to use when rendering the order. The number of rectangles described by the
          Delta-Encoded Rectangles structure is specified by the nDeltaEntries field.

  ts_multi_patblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.4 MultiPatBlt (MULTI_PATBLT_ORDER)
      The MultiPatBlt Primary Drawing Order is used to paint multiple rectangles by using a
      specified brush and three-way raster operation.
       - Encoding order number: 16 (0x10)
       - Negotiation order number: 16 (0x10)
       - Number of fields: 14
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 412 bytes
      mstscax!COD::ODHandleMultiPatBlt
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: back_color
        type: ts_color
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Background color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          Foreground color described using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The style of the brush used in the drawing operation.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Holds a brush hatch identifier or a Brush Cache index,
          depending on the contents of the BrushStyle field.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          A byte array of length 7. BrushExtra contains an array of bitmap bits that encodes
          the pixel pattern present in the top seven rows of the 8x8 pattern brush. The pixel
          pattern present in the bottom row is encoded in the BrushHatch field. The BrushExtra
          field is only present if the BrushStyle is set to BS_PATTERN (0x03). The rows are
          encoded in reverse order, that is, the pixels in the penultimate row are encoded in
          the first byte, and the pixels in the top row are encoded in the seventh byte. For
          example, a 45-degree downward sloping left-to-right line would be encoded in
          BrushExtra as { 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 } with BrushHatch containing
          the value 0x01 (the bottom row).
      - id: delta_entries
        type: u1
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of bounding rectangles described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable2_field
        if: ((field_flags >> 13) & 0x1) != 0
        doc: |
          A Two-Byte Header Variable Field (section 2.2.2.2.1.1.1.3) structure that encapsulates
          a Delta-Encoded Rectangles (section 2.2.2.2.1.1.1.5) structure that contains bounding
          rectangles to use when rendering the order. The number of rectangles described by the
          Delta-Encoded Rectangles structure is specified by the nDeltaEntries field.

  ts_multi_scrblt_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.8 MultiScrBlt (MULTI_SCRBLT_ORDER)
      The MultiScrBlt Primary Drawing Order is used to perform multiple bit-block transfers from
      source regions to destination regions of the screen.
       - Encoding order number: 17 (0x11)
       - Negotiation order number: 17 (0x11)
       - Number of fields: 9
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 399 bytes
      mstscax!COD::ODHandleScrBlts
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop
        type: rop3_operation_index
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: x_src
        type: cord_field(control_flags)
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          The x-coordinate of the source rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_src
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The y-coordinate of the source rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: delta_entries
        type: u1
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of bounding rectangles described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable2_field
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          A Two-Byte Header Variable Field (section 2.2.2.2.1.1.1.3) structure that encapsulates
          a Delta-Encoded Rectangles (section 2.2.2.2.1.1.1.5) structure that contains bounding
          rectangles to use when rendering the order. The number of rectangles described by the
          Delta-Encoded Rectangles structure is specified by the nDeltaEntries field.

  ts_multi_opaquerect_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.6 MultiOpaqueRect (MULTI_OPAQUERECT_ORDER)
      The MultiOpaqueRect Primary Drawing Order is used to paint multiple rectangles by using an
      opaque brush.
       - Encoding order number: 18 (0x12)
       - Negotiation order number: 18 (0x12)
       - Number of fields: 9
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 397 bytes
      mstscax!COD::ODHandleMultiOpaqueRect
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          Left coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          Top coordinate of the destination rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: width
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Width of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: height
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Height of the destination rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: red_or_palette_index
        type: u1
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Used as a palette index for 16-color and 256-color palettized color
          schemes. If the RGB color scheme is in effect, this field holds the red RGB component.
      - id: green
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The green RGB color component.
      - id: blue
        type: u1
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The blue RGB color component.
      - id: delta_entries
        type: u1
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of bounding rectangles described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable2_field
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          A Two-Byte Header Variable Field (section 2.2.2.2.1.1.1.3) structure that encapsulates
          a Delta-Encoded Rectangles (section 2.2.2.2.1.1.1.5) structure that contains bounding
          rectangles to use when rendering the order. The number of rectangles described by the
          Delta-Encoded Rectangles structure is specified by the nDeltaEntries field.
    instances:
      red:
        value: red_or_palette_index
        doc: An 8-bit, unsigned integer. The red RGB color component.
      palette_index:
        value: red_or_palette_index
        doc: An 8-bit, unsigned integer. Palette index for 16-color and 256-color palettized color schemes.

  ts_fast_index_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.14 FastIndex (FASTINDEX_ORDER)
      The FastIndex Primary Drawing Order encodes a set of glyph indices at a specified position. This order
      is an improved version of the GlyphIndex (section 2.2.2.2.1.1.2.13) Primary Drawing Order. The regular
      GlyphIndex order contains five brush fields that the FastIndex order does not: BrushOrgX, BrushOrgY,
      BrushStyle, BrushHatch, and BrushExtra. These extra fields MUST all be implicitly assumed to exist and
      contain default values of zero (implying that a solid color brush will be used).
       - Encoding order number: 19 (0x13)
       - Negotiation order number: 19 (0x13)
       - Number of fields: 15
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 285 bytes
      mstscax!COD::ODDecodeFastIndex
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: cache_id
        type: u2
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The ID of the glyph cache in which the glyph data MUST be stored.
          This value MUST be in the range 0 to 9 (inclusive).
      - id: drawing
        type: u2
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          A 16-bit, unsigned integer. Combined flAccel and ulCharInc fields from the GlyphIndex (section
          2.2.2.2.1.1.2.13) Primary Drawing Order. The high-order byte contains the flAccel field, and
          the low-order byte contains the ulCharInc field.
      - id: back_color
        type: ts_color
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The text color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          Opaque rectangle color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: bk_left
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Left coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_top
        type: cord_field(control_flags)
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Top coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_right
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          Right coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          Bottom coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: op_left
        type: cord_field(control_flags)
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          Left coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkLeft.
      - id: op_top
        type: cord_field(control_flags)
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          The top coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
          This field MUST contain opaque rectangle encoding flags if the OpBottom field is set to be the value of -32768.
          In this case, OpTop holds the encoding flags in the low 4 bits.
      - id: op_right
        type: cord_field(control_flags)
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          The right coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkRight.
      - id: op_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          The bottom coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to -32768 if OpTop contains opaque rectangle encoding flags (see the OpTop field).
      - id: x
        type: s2
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The x-coordinate of the point where the origin of the starting glyph MUST be positioned
          specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same as
          BkLeft.
      - id: y
        type: s2
        if: ((field_flags >> 13) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The y-coordinate of the point where the origin of the starting glyph MUST be positioned
          specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same as
          BkTop.
      - id: variable_bytes
        type: variable1_field
        if: ((field_flags >> 14) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure. The contents and format of this field are
          the same as the VariableBytes field of the GlyphIndex (section 2.2.2.2.1.1.2.13) Primary Drawing Order.
    instances:
      encoding_flags:
        value: 'op_bottom.value == 0x8000 ? op_top.value & 0xf : 0'
        enum: oprect
        doc: |
          Encoding flags. The only valid combinations of the encoding flags that are
          currently supported are as follows:
          - 0x0F: The left, top, right, and bottom coordinates of the opaque rectangle
                  all match the background text rectangle and are absent.
          - 0x0D: The left, top, and bottom coordinates of the opaque rectangle all
                  match the background text rectangle and are absent. The actual value
                  of the right coordinate is present.
    enums:
      oprect:
        0x01:
          id: bottom_absent
          doc: Bottom coordinate of the opaque rectangle is the same as BkBottom.
        0x02:
          id: right_absent
          doc: Right coordinate of the opaque rectangle is the same as BkRight.
        0x04:
          id: top_absent
          doc: Top coordinate of the opaque rectangle is the same as BkTop.
        0x08:
          id: left_absent
          doc: Left coordinate of the opaque rectangle is the same as BkLeft.

  ts_polygon_sc_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.16 PolygonSC (POLYGON_SC_ORDER)
      The PolygonSC Primary Drawing Order encodes a solid-color polygon consisting of two or
      more vertices connected by straight lines.
       - Encoding order number: 20 (0x14)
       - Negotiation order number: 20 (0x14)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 249 bytes
      mstscax!COD::ODHandlePolygonSC
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: x_start
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The x-coordinate of the starting point of the polygon path specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_start
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The y-coordinate of the starting point of the polygon path specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: rop2
        type: rop2_operation
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The binary raster operation to perform (see section 2.2.2.2.1.1.1.6).
      - id: fill_mode
        type: u1
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The polygon filling algorithm described using a Fill Mode (section 2.2.2.2.1.1.1.9) structure.
      - id: brush_color
        type: ts_color
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          Foreground color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: delta_entries
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of points along the polygon path described by the CodedDeltaList field.
      - id: coded_delta_list
        type: variable1_field
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure that encapsulates a
          Delta-Encoded Points (section 2.2.2.2.1.1.1.4) structure that contains the points along
          the polygon path. The number of points described by the Delta-Encoded Points structure is
          specified by the NumDeltaEntries field.

  ts_polygon_cb_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.17 PolygonCB (POLYGON_CB_ORDER)
      The PolygonCB Primary Drawing Order encodes a color brush polygon consisting of two or more
      vertices connected by straight lines.
       - Encoding order number: 21 (0x15)
       - Negotiation order number: 21 (0x15)
       - Number of fields: 13
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 263 bytes
      mstscax!COD::ODHandlePolygonCB
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: x_start
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The x-coordinate of the starting point of the polygon path specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: y_start
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The y-coordinate of the starting point of the polygon path specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: back_mode
        type: b1
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The high bit (mask 0x80) indicates whether the background drawing mode MUST be set to TRANSPARENT or
          OPAQUE (see section 2.2.2.2.1.1.2.11). The background drawing mode is only significant if the BrushStyle
          field indicates that the brush is a BS_HATCHED (0x02) or BS_PATTERN (0x03) brush.
      - id: padding
        type: b2
        if: ((field_flags >> 2) & 0x1) != 0
      - id: rop2
        type: b5
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The low 5 bits (mask 0x1F) identify the real ROP2 operation.
      - id: fill_mode
        type: u1
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The polygon filling algorithm described by using a Fill Mode (section 2.2.2.2.1.1.1.9) structure.
      - id: back_color
        type: ts_color
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The background color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          The foreground color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel of a
          brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel of a
          brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The contents and format of this field are the same as the
          BrushStyle field of the PatBlt (section 2.2.2.2.1.1.2.3) Primary Drawing Order.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The contents and format of this field are the same as the
          BrushHatch field of the PatBlt (section 2.2.2.2.1.1.2.3) Primary Drawing Order.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          A byte array of length 7. The contents and format of this field are the same as the
          BrushExtra field of the PatBlt (section 2.2.2.2.1.1.2.3) Primary Drawing Order.
      - id: delta_entries
        type: u1
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of points along the polygon path described by
          the CodedDeltaList field.
      - id: coded_delta_list
        type: variable1_field
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure that encapsulates
          a Delta-Encoded Points (section 2.2.2.2.1.1.1.4) structure that contains the points along
          the polygon path. The number of points described by the Delta-Encoded Points structure is
          specified by the NumDeltaEntries field.

  ts_polyline_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.18 Polyline (POLYLINE_ORDER)
      The Polyline Primary Drawing Order encodes a solid color polyline consisting of two or more
      vertices connected by straight lines.
       - Encoding order number: 22 (0x16)
       - Negotiation order number: 22 (0x16)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 148 bytes
      mstscax!COD::ODHandlePolyLine
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: x_start
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The x-coordinate of the starting point of the polygon path specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: y_start
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The y-coordinate of the starting point of the polygon path specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: rop2
        type: rop2_operation
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The binary raster operation to perform (see section 2.2.2.2.1.1.1.6).
      - id: brush_cache_entry
        type: u1
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          A 16-bit unsigned integer. The brush cache entry. This field is unused (as only solid
          color polylines are drawn) and SHOULD<3> be set to 0x0000.
      - id: pen_color
        type: ts_color
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The foreground color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: delta_entries
        type: u1
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The number of points along the polyline path described by
          the CodedDeltaList field.
      - id: coded_delta_list
        type: variable1_field
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure that encapsulates a
          Delta-Encoded Points (section 2.2.2.2.1.1.1.4) structure that contains the points along
          the polyline path. The number of points described by the Delta-Encoded Points structure
          is specified by the NumDeltaEntries field.

  ts_fast_glyph_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.15 FastGlyph (FASTGLYPH_ORDER)
      The FastGlyph Primary Drawing Order encodes a single glyph at a specified position. This primary
      drawing order is a fast way of outputting a single glyph and bypasses having to send a Cache Glyph
      Secondary Drawing Order (see sections 2.2.2.2.1.2.5 and 2.2.2.2.1.2.6) followed by a GlyphIndex
      (section 2.2.2.2.1.1.2.13) or FastIndex (section 2.2.2.2.1.1.2.14) Primary Drawing Order.
       - Encoding order number: 24 (0x18)
       - Negotiation order number: 24 (0x18)
       - Number of fields: 15
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 285 bytes
      mstscax!COD::ODHandleFastGlyph
      mstscax!CTSGraphicsSurfaceBase::GlyphPassthrough
      mstscax!CTSGraphicsGlyphHandler::DrawGlyphOrder
      mstscax!CTSGraphicsGlyphHandler::GlyphOut
      mstscax!CTSGraphicsGlyphHandler::GlyphSlowOutputBuffer
      mstscax!CTSGraphics::BrushCreate
      mstscax!CTSGraphicsBrush::Initialize
      mstscax!CTSGraphicsBrush::CreateBrushFromBitmapBits
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: cache_id
        type: u1
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The ID of the glyph cache in which the glyph data MUST be stored.
          This value MUST be in the range 0 to 9 (inclusive).
      - id: accel
        type: u1
        enum: so
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Accelerator flags. For glyph related terminology, see [YUAN]
          figures 14-17 and 15-1. For information about string widths and heights, see [MSDN-SWH]. For
          information about character widths, see [MSDN-CW]. This field MUST contain a combination of
          the following flags.
      - id: char_inc
        type: u1
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Specifies whether or not the font is a fixed-pitch (monospace)
          font. If so, this member is equal to the advance width of the glyphs in pixels (see [YUAN]
          figures 14-17); if not, this field is set to 0x00. The minimum value for this field is 0x00
          (inclusive), and the maximum value is 0xFF (inclusive).
      - id: back_color
        type: ts_color
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          Text color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The opaque rectangle color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: bk_left
        type: cord_field(control_flags)
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The left coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_top
        type: cord_field(control_flags)
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          The top coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_right
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The right coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          The bottom coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: op_left
        type: cord_field(control_flags)
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          The left coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkLeft.
      - id: op_top
        type: cord_field(control_flags)
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          The top coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
          This field MUST contain opaque rectangle encoding flags if the OpBottom field is set to be the value of -32768.
          In this case, OpTop holds the encoding flags in the low 4 bits.
      - id: op_right
        type: cord_field(control_flags)
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          The right coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkRight.
      - id: op_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          The bottom coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to -32768 if OpTop contains opaque rectangle encoding flags (see the OpTop field).
      - id: x
        type: s2
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The x-coordinate of the point where the origin of the starting glyph MUST be positioned,
          specified by using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same
          as BkLeft.
      - id: y
        type: s2
        if: ((field_flags >> 13) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The y-coordinate of the point where the origin of the starting glyph MUST be positioned,
          specified by using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same
          as BkTop.
      - id: variable_bytes
        type: variable1_field
        if: ((field_flags >> 14) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure. If the size of this
          VariableBytes field is 1 byte, it contains a 1-byte glyph cache index from which the glyph
          data MUST be retrieved. However, if the size is larger than 1 byte, this field contains a
          Cache Glyph Data - Revision 2 (section 2.2.2.2.1.2.6.1) structure. The glyph data MUST be stored
          in the glyph cache specified by the cacheId field in the entry indicated by the cacheIndex
          field of the glyph data. The Cache Glyph Data - Revision 2 structure MUST be followed by 2
          bytes of padding that MAY<2> contain a little-endian encoded Unicode character representing
          the glyph (this Unicode character MUST NOT be null-terminated).
          All glyph cache indices MUST be greater than or equal to 0, and less than the maximum number
          of entries allowed in the glyph cache with the ID specified by the cacheId field. The maximum
          number of entries allowed in each of the ten glyph caches is specified in the GlyphCache field
          of the Glyph Cache Capability Set ([MS-RDPBCGR] section 2.2.7.1.8).
    enums:
      so:
        0x01:
          id: flag_default_placement
          doc: This flag MUST be set.
        0x02:
          id: horizontal
          doc: Text is horizontal, left-to-right or right-to-left, depending on SO_REVERSED.
        0x04:
          id: vertical
          doc: Text is vertical, top-to-bottom or bottom-to-top, depending on SO_REVERSED.
        0x08:
          id: reversed
          doc: Set if horizontal text is right-to-left or vertical text is bottom-to-top.
        0x10:
          id: zero_bearings
          doc: |
            For a given glyph in the font, the A-width (left-side bearing) and C-width (right-side bearing)
            associated with the glyph have a value of zero.
        0x20:
          id: char_inc_equal_bm_base
          doc: |
            For a given glyph in the font, the B-width associated with the glyph equals the advance width of the glyph.
        0x40:
          id: maxext_equal_bm_side
          doc: |
            The height of the bitmap associated with a given glyph in the font is always equal to the sum of the 
            ascent and descent. This implies that the tops and bottoms of all glyph bitmaps lie on the same line
            in the direction of writing.

  ts_ellipse_sc_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.19 EllipseSC (ELLIPSE_SC_ORDER)
      The EllipseSC Primary Drawing Order encodes a single, solid-color ellipse.
       - Encoding order number: 25 (0x19)
       - Negotiation order number: 25 (0x19)
       - Number of fields: 7
       - Number of field encoding bytes: 1
       - Maximum encoded field length: 13 bytes
      mstscax!COD::ODHandleEllipseSC
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The left coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The top coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: right_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The right coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: bottom_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The bottom coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: rop2
        type: rop2_operation
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The binary raster operation to perform (see section 2.2.2.2.1.1.1.6).
      - id: fill_mode
        type: u1
        enum: fm
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer that specifies the fill mode.
      - id: color
        type: ts_color
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The foreground color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
    enums:
      fm:
        0x00:
          id: nofill
          doc: A polyline ellipse (that is, a non-filled ellipse) MUST be drawn.
        0x01:
          id: alternate
          doc: See section 2.2.2.2.1.1.1.9 for an explanation of this value.
        0x02:
          id: winding
          doc: See section 2.2.2.2.1.1.1.9 for an explanation of this value.

  ts_ellipse_cb_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.20 EllipseCB (ELLIPSE_CB_ORDER)
      The EllipseCB Primary Drawing Order encodes a color brush ellipse.
       - Encoding order number: 26 (0x1A)
       - Negotiation order number: 26 (0x1A)
       - Number of fields: 13
       - Number of field encoding bytes: 2
       - Maximum encoded field length: 27 bytes
      mstscax!COD::ODHandleEllipseCB
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: left_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          The left coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: top_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          The top coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: right_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          The right coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: bottom_rect
        type: cord_field(control_flags)
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          The bottom coordinate of the inclusive rectangle for the ellipse specified by using a Coord
          Field (section 2.2.2.2.1.1.1.1).
      - id: back_mode
        type: b1
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The high bit (mask 0x80) indicates whether the background drawing mode MUST be set to TRANSPARENT or
          OPAQUE (see section 2.2.2.2.1.1.2.11). The background drawing mode is only significant if the BrushStyle
          field indicates that the brush is a BS_HATCHED (0x02) or BS_PATTERN (0x03) brush.
      - id: padding
        type: b2
        if: ((field_flags >> 4) & 0x1) != 0
      - id: rop2
        type: b5
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The low 5 bits (mask 0x1F) identify the real ROP2 operation.
      - id: fill_mode
        type: u1
        enum: fm
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer that specifies the fill mode.
      - id: back_color
        type: ts_color
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          The text color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          Opaque rectangle color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The style of the brush used in the drawing operation.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Holds a brush hatch identifier or a Brush Cache index,
          depending on the contents of the BrushStyle field.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          A byte array of length 7. BrushExtra contains an array of bitmap bits that encodes
          the pixel pattern present in the top seven rows of the 8x8 pattern brush. The pixel
          pattern present in the bottom row is encoded in the BrushHatch field. The BrushExtra
          field is only present if the BrushStyle is set to BS_PATTERN (0x03). The rows are
          encoded in reverse order, that is, the pixels in the penultimate row are encoded in
          the first byte, and the pixels in the top row are encoded in the seventh byte. For
          example, a 45-degree downward sloping left-to-right line would be encoded in
          BrushExtra as { 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 } with BrushHatch containing
          the value 0x01 (the bottom row).
    enums:
      fm:
        0x00:
          id: nofill
          doc: A polyline ellipse (that is, a non-filled ellipse) MUST be drawn.
        0x01:
          id: alternate
          doc: See section 2.2.2.2.1.1.1.9 for an explanation of this value.
        0x02:
          id: winding
          doc: See section 2.2.2.2.1.1.1.9 for an explanation of this value.
      so:
        0x01:
          id: flag_default_placement
          doc: This flag MUST be set.
        0x02:
          id: horizontal
          doc: Text is horizontal, left-to-right or right-to-left, depending on SO_REVERSED.
        0x04:
          id: vertical
          doc: Text is vertical, top-to-bottom or bottom-to-top, depending on SO_REVERSED.
        0x08:
          id: reversed
          doc: Set if horizontal text is right-to-left or vertical text is bottom-to-top.
        0x10:
          id: zero_bearings
          doc: |
            For a given glyph in the font, the A-width (left-side bearing) and C-width (right-side bearing)
            associated with the glyph have a value of zero.
        0x20:
          id: char_inc_equal_bm_base
          doc: |
            For a given glyph in the font, the B-width associated with the glyph equals the advance width of the glyph.
        0x40:
          id: maxext_equal_bm_side
          doc: |
            The height of the bitmap associated with a given glyph in the font is always equal to the sum of the 
            ascent and descent. This implies that the tops and bottoms of all glyph bitmaps lie on the same line
            in the direction of writing.

  ts_index_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.1.2.13 GlyphIndex (GLYPHINDEX_ORDER)
      The GlyphIndex Primary Drawing Order encodes a set of glyph indices at a specified position.
       - Encoding order number: 27 (0x1B)
       - Negotiation order number: 27 (0x1B)
       - Number of fields: 22
       - Number of field encoding bytes: 3
       - Maximum encoded field length: 297 bytes
      mstscax!COD::ODHandleGlyphIndex
    params:
      - id: control_flags
        type: u1
      - id: field_flags
        type: u4
    seq:
      - id: cache_id
        type: u1
        if: ((field_flags >> 0) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The ID of the glyph cache in which the glyph data MUST be stored.
          This value MUST be in the range 0 to 9 (inclusive).
      - id: accel
        type: u1
        enum: so
        if: ((field_flags >> 1) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Accelerator flags. For glyph related terminology, see [YUAN]
          figures 14-17 and 15-1. For information about string widths and heights, see [MSDN-SWH]. For
          information about character widths, see [MSDN-CW]. This field MUST contain a combination of
          the following flags.
      - id: char_inc
        type: u1
        if: ((field_flags >> 2) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Specifies whether or not the font is a fixed-pitch (monospace)
          font. If so, this member is equal to the advance width of the glyphs in pixels (see [YUAN]
          figures 14-17); if not, this field is set to 0x00. The minimum value for this field is 0x00
          (inclusive), and the maximum value is 0xFF (inclusive).
      - id: op_redundant
        type: u1
        if: ((field_flags >> 3) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. A Boolean value indicating whether or not the opaque rectangle is
          redundant. Redundant, in this context, means that the text background is transparent.
           - 0x00 - FALSE - Rectangle is not redundant.
           - 0x01 - TRUE - Rectangle is redundant.
      - id: back_color
        type: ts_color
        if: ((field_flags >> 4) & 0x1) != 0
        doc: |
          The text color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: fore_color
        type: ts_color
        if: ((field_flags >> 5) & 0x1) != 0
        doc: |
          Opaque rectangle color described by using a Generic Color (section 2.2.2.2.1.1.1.8) structure.
      - id: bk_left
        type: cord_field(control_flags)
        if: ((field_flags >> 6) & 0x1) != 0
        doc: |
          Left coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_top
        type: cord_field(control_flags)
        if: ((field_flags >> 7) & 0x1) != 0
        doc: |
          Top coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_right
        type: cord_field(control_flags)
        if: ((field_flags >> 8) & 0x1) != 0
        doc: |
          Right coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: bk_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 9) & 0x1) != 0
        doc: |
          Bottom coordinate of the text background rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
      - id: op_left
        type: cord_field(control_flags)
        if: ((field_flags >> 10) & 0x1) != 0
        doc: |
          Left coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkLeft.
      - id: op_top
        type: cord_field(control_flags)
        if: ((field_flags >> 11) & 0x1) != 0
        doc: |
          The top coordinate of the opaque rectangle specified by using a Coord Field (section 2.2.2.2.1.1.1.1).
          This field MUST contain opaque rectangle encoding flags if the OpBottom field is set to be the value of -32768.
          In this case, OpTop holds the encoding flags in the low 4 bits.
      - id: op_right
        type: cord_field(control_flags)
        if: ((field_flags >> 12) & 0x1) != 0
        doc: |
          The right coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to 0 if it is the same as BkRight.
      - id: op_bottom
        type: cord_field(control_flags)
        if: ((field_flags >> 13) & 0x1) != 0
        doc: |
          The bottom coordinate of the opaque rectangle specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field
          MUST be set to -32768 if OpTop contains opaque rectangle encoding flags (see the OpTop field).
      - id: brush_org_x
        type: u1
        if: ((field_flags >> 14) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The x-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_org_y
        type: u1
        if: ((field_flags >> 15) & 0x1) != 0
        doc: |
          An 8-bit, signed integer. The y-coordinate of the point where the top leftmost pixel
          of a brush pattern MUST be anchored.
      - id: brush_style
        type: u1
        if: ((field_flags >> 16) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. The style of the brush used in the drawing operation.
      - id: brush_hatch
        type: u1
        if: ((field_flags >> 17) & 0x1) != 0
        doc: |
          An 8-bit, unsigned integer. Holds a brush hatch identifier or a Brush Cache index,
          depending on the contents of the BrushStyle field.
      - id: brush_extra
        size: 7
        if: ((field_flags >> 18) & 0x1) != 0
        doc: |
          A byte array of length 7. BrushExtra contains an array of bitmap bits that encodes
          the pixel pattern present in the top seven rows of the 8x8 pattern brush. The pixel
          pattern present in the bottom row is encoded in the BrushHatch field. The BrushExtra
          field is only present if the BrushStyle is set to BS_PATTERN (0x03). The rows are
          encoded in reverse order, that is, the pixels in the penultimate row are encoded in
          the first byte, and the pixels in the top row are encoded in the seventh byte. For
          example, a 45-degree downward sloping left-to-right line would be encoded in
          BrushExtra as { 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 } with BrushHatch containing
          the value 0x01 (the bottom row).
      - id: x
        type: s2
        if: ((field_flags >> 19) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The x-coordinate of the point where the origin of the starting glyph MUST be positioned
          specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same as
          BkLeft.
      - id: y
        type: s2
        if: ((field_flags >> 20) & 0x1) != 0
        doc: |
          A 16-bit, signed integer. The y-coordinate of the point where the origin of the starting glyph MUST be positioned
          specified using a Coord Field (section 2.2.2.2.1.1.1.1). This field MUST be set to -32768 if it is the same as
          BkTop.
      - id: variable_bytes
        type: variable1_field
        if: ((field_flags >> 21) & 0x1) != 0
        doc: |
          A One-Byte Header Variable Field (section 2.2.2.2.1.1.1.2) structure. The contents and format of this field are
          the same as the VariableBytes field of the GlyphIndex (section 2.2.2.2.1.1.2.13) Primary Drawing Order.
    enums:
      so:
        0x01:
          id: flag_default_placement
          doc: This flag MUST be set.
        0x02:
          id: horizontal
          doc: Text is horizontal, left-to-right or right-to-left, depending on SO_REVERSED.
        0x04:
          id: vertical
          doc: Text is vertical, top-to-bottom or bottom-to-top, depending on SO_REVERSED.
        0x08:
          id: reversed
          doc: Set if horizontal text is right-to-left or vertical text is bottom-to-top.
        0x10:
          id: zero_bearings
          doc: |
            For a given glyph in the font, the A-width (left-side bearing) and C-width (right-side bearing)
            associated with the glyph have a value of zero.
        0x20:
          id: char_inc_equal_bm_base
          doc: |
            For a given glyph in the font, the B-width associated with the glyph equals the advance width of the glyph.
        0x40:
          id: maxext_equal_bm_side
          doc: |
            The height of the bitmap associated with a given glyph in the font is always equal to the sum of the 
            ascent and descent. This implies that the tops and bottoms of all glyph bitmaps lie on the same line
            in the direction of writing.
