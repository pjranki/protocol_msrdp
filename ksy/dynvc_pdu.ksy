meta:
  id: dynvc_pdu
  endian: le
  imports:
    - rdp_segmented_data
    - rdp_raw_pdu
    - echo_pdu
    - rdpinput_pdu
    - rdpgfx_pdu
    - auth_redirection_pdu
    - rdp_telemetry_pdu
    - location_pdu
    - geometry_pdu
    - displaycontrol_pdu
    - rdpgfx_redir_pdu
    - tsmm_video_control_pdu
    - tsmm_video_data_pdu
    - fbr_pdu
    - rdpsnd_pdu
    - rdpsnd_lossy_pdu
    - sndin_pdu
    - xpsrd_req_pdu
    - xpsrd_rsp_pdu
    - tsvctkt_req_pdu
    - tsvctkt_rsp_pdu
    - tsmf_req_pdu
    - tsmf_rsp_pdu
    - pnpdr_pdu
    - file_redirector_request_pdu
    - file_redirector_reply_pdu
    - rdcamera_device_enumerator_pdu
    - rdcamera_device_pdu
    - wdag_dvc_pdu

doc: |
  MS-RDPEDYC 2.2 Message Syntax ()
  Each PDU has the same 1-byte header with the optionalFields field following it.
  The cbId and Cmd fields are common to all PDUs. The data following the PDU header
  depends on the type of the message and is addressed in the following sections.
  Requests are made to the client, responses are sent to the server.
  mstscax!CDynVCPlugin::OnStaticDataReceived
  mstscax!CDynVCPlugin::SendChannelData

params:
  - id: to_server
    type: bool
  - id: channel_type
    type: u1

seq:
  - id: cmd
    type: b4
    enum: dynvc_cmd
    doc: |
      Indicates the PDU type.
  - id: sp
    type: b2
    doc:
      The value and meaning depend on the Cmd field.
  - id: cb_id
    type: b2
    doc:
      Indicates the length of the ChannelId field.
  - id: channel_id_byte_1
    type: u1
    if: has_channel_id
  - id: channel_id_byte_2
    type: u1
    if: has_channel_id and (cb_id > 0)
  - id: channel_id_byte_3
    type: u1
    if: has_channel_id and (cb_id > 1)
  - id: channel_id_byte_4
    type: u1
    if: has_channel_id and (cb_id > 1)
  - id: data
    size-eos: true
    process: msrdp_dynvc_channel_data(_root)
    type:
      switch-on: cmd_type
      -name: type
      cases:
        'dynvc_cmd_type::create_req': dynvc_create_req
        'dynvc_cmd_type::create_rsp': dynvc_create_rsp
        'dynvc_cmd_type::data_first': dynvc_data_first(sp, to_server, channel_type)
        'dynvc_cmd_type::data': dynvc_data(to_server, channel_type)
        'dynvc_cmd_type::close_req': dynvc_close_req
        'dynvc_cmd_type::close_rsp': dynvc_close_rsp
        'dynvc_cmd_type::capability_req': dynvc_caps_req
        'dynvc_cmd_type::capability_rsp': dynvc_caps_rsp
        'dynvc_cmd_type::data_first_compressed': dynvc_data_first_compressed(sp)
        'dynvc_cmd_type::data_compressed': dynvc_data_compressed
        'dynvc_cmd_type::soft_sync_req': dynvc_data_soft_sync_req
        'dynvc_cmd_type::soft_sync_rsp': dynvc_data_soft_sync_rsp

instances:
  cb_id_u4:
    value:  cb_id << 0
  channel_id_u4_1:
    value: channel_id_byte_1 << 0
  channel_id_u4_2:
    value: channel_id_byte_2 << 0
  channel_id_u4_3:
    value: channel_id_byte_3 << 0
  channel_id_u4_4:
    value: channel_id_byte_4 << 0
  channel_id_size:
    value: 1 << cb_id_u4
  channel_id_mask:
    value: (1 << ((channel_id_size * 8) - 1)) | ((1 << ((channel_id_size * 8) - 1)) - 1)
  has_channel_id:
    value: '(cmd != dynvc_cmd::capability) and (cmd != dynvc_cmd::soft_sync_req) and (cmd != dynvc_cmd::soft_sync_rsp)'
  channel_id:
    value: '(not has_channel_id ? 0 : channel_id_mask & (((channel_id_byte_4 << 24) & 0xff000000) | ((channel_id_byte_3 << 16) & 0xff0000) | ((channel_id_byte_2 << 8) & 0xff00) | ((channel_id_byte_1 << 0) & 0xff))) & 0xffffffff'
    doc: |
      Channel ID
  pri:
    value: sp
    doc: |
      Version 1 of the Remote Desktop Protocol: Dynamic Virtual Channel Extension (as
      specified in section 2.2.1.1.1) does not support priority classes. The client
      SHOULD ignore this field.
      In version 2 of the Remote Desktop Protocol: Dynamic Virtual Channel Extension,
      this field specifies the priority class for the channel that is being created,
      with the Pri field values 0, 1, 2, and 3 corresponding to PriorityCharge0,
      PriorityCharge1, PriorityCharge2, and PriorityCharge3, as specified in section
      2.2.1.1.2. The method of determining priority class is the same for both client
      to server data and server to client data.
  cmd_bidirectional:
    value: (cmd.to_i & 0x02) != 0
  cmd_type:
    value: '(cmd_bidirectional ? 0xc0 : (to_server ? 0x80 : 0x40)) | cmd.to_i'
    enum: dynvc_cmd_type

enums:
  channel_type:
    0: raw
    1: echo
    2: microsoft_windows_rds_input
    3: microsoft_windows_rds_graphics
    4: microsoft_windows_rds_auth_redirection
    5: microsoft_windows_rds_telemetry
    6: microsoft_windows_rds_location
    7: microsoft_windows_rds_geometry
    8: microsoft_windows_rds_display_control
    9: microsoft_windows_rds_remote_app_graphics_redirection
    10: microsoft_windows_rds_video_control_v08_01
    11: microsoft_windows_rds_video_data_v08_01
    12: microsoft_windows_rds_frame_buffer_control_v08_01
    13: audio_playback_dvc
    14: audio_playback_lossy_dvc
    15: audio_input
    16: xpsrd
    17: tsvctkt
    18: tsmf
    19: pnpdr
    20: file_redirector_channel
    21: rdcamera_device_enumerator
    22: rdcamera_device
    23: wdag_dvc
  dynvc_cmd:
    0x01:
      id: create
      doc: |
        The message contained in the optionalFields field is a Create Request PDU
        (section 2.2.2.1) or a Create Response PDU (section 2.2.2.2).
    0x02:
      id: data_first
      doc: |
       The message contained in the optionalFields field is a Data First PDU (section 2.2.3.1).
    0x03:
      id: data
      doc: |
        The message contained in the optionalFields field is a Data PDU (section 2.2.3.2).
    0x04:
      id: close
      doc: |
        The message contained in the optionalFields field is a Close Request PDU (section 2.2.4)
        or a Close Response PDU (section 2.2.4).
    0x05:
      id: capability
      doc: |
        The message contained in the optionalFields field is a Capability Request PDU (section 2.2.1.1)
        or a Capabilities Response PDU (section 2.2.1.2).
    0x06:
      id: data_first_compressed
      doc: |
        The message contained in the optionalFields field is a Data First Compressed PDU (section 2.2.3.3).
    0x07:
      id: data_compressed
      doc: |
        The message contained in the optionalFields field is a Data Compressed PDU (section 2.2.3.4).
    0x08:
      id: soft_sync_req
      doc: |
        The message contained in the optionalFields field is a Soft-Sync Request PDU (section 2.2.5.1).
    0x09:
      id: soft_sync_rsp
      doc: |
        The message contained in the optionalFields field is a Soft-Sync Response PDU (section 2.2.5.2).
  dynvc_cmd_type:
    0x41:
      id: create_req
      doc: |
        The message contained in the optionalFields field is a Create Request PDU
        (section 2.2.2.1) or a Create Response PDU (section 2.2.2.2).
    0x81:
      id: create_rsp
      doc: |
        The message contained in the optionalFields field is a Create Request PDU
        (section 2.2.2.1) or a Create Response PDU (section 2.2.2.2).
    0xc2:
      id: data_first
      doc: |
       The message contained in the optionalFields field is a Data First PDU (section 2.2.3.1).
    0xc3:
      id: data
      doc: |
        The message contained in the optionalFields field is a Data PDU (section 2.2.3.2).
    0x44:
      id: close_req
      doc: |
        The message contained in the optionalFields field is a Close Request PDU (section 2.2.4)
        or a Close Response PDU (section 2.2.4).
    0x84:
      id: close_rsp
      doc: |
        The message contained in the optionalFields field is a Close Request PDU (section 2.2.4)
        or a Close Response PDU (section 2.2.4).
    0x45:
      id: capability_req
      doc: |
        The message contained in the optionalFields field is a Capability Request PDU (section 2.2.1.1)
        or a Capabilities Response PDU (section 2.2.1.2).
    0x85:
      id: capability_rsp
      doc: |
        The message contained in the optionalFields field is a Capability Request PDU (section 2.2.1.1)
        or a Capabilities Response PDU (section 2.2.1.2).
    0xc6:
      id: data_first_compressed
      doc: |
        The message contained in the optionalFields field is a Data First Compressed PDU (section 2.2.3.3).
    0xc7:
      id: data_compressed
      doc: |
        The message contained in the optionalFields field is a Data Compressed PDU (section 2.2.3.4).
    0x48:
      id: soft_sync_req
      doc: |
        The message contained in the optionalFields field is a Soft-Sync Request PDU (section 2.2.5.1).
    0x89:
      id: soft_sync_rsp
      doc: |
        The message contained in the optionalFields field is a Soft-Sync Response PDU (section 2.2.5.2).
  channel_id:
    0x00:
      id: length_1_byte
      doc: The ChannelId is 1 byte wide.
    0x01:
      id: length_2_byte
      doc: The ChannelId is 2 bytes wide.
    0x02:
      id: length_4_byte
      doc: The ChannelId is 4 bytes wide.

types:
  dynvc_caps_req:
    doc: |
      MS-RDPEDYC 2.2.1.1 DVC Capabilities Request PDU
      The DYNVC_CAPS_REQ PDU is sent by the DVC server manager to indicate version
      level capabilities supported.
      mstscax!CDynVCPlugin::OnStaticDataReceived
    seq:
      - id: pad
        type: u1
        doc: |
          An 8-bit unsigned integer. Unused. MUST be set to 0x00.
      - id: version
        type: u2
        doc: |
          A 16-bit unsigned integer that indicates the protocol version level supported.
      - id: caps
        type:
          switch-on: version
          -name: enum
          cases:
            1: dynvc_caps_version1
            2: dynvc_caps_version2
            3: dynvc_caps_version3
            _: dynvc_caps_version1

  dynvc_caps_version1:
    doc: |
      MS-RDPEDYC 2.2.1.1.1 Version 1 (DYNVC_CAPS_VERSION1)
      The DYNVC_CAPS_VERSION1 PDU is sent by the DVC server manager to indicate
      that it supports version 1 of the Remote Desktop Protocol: Dynamic Channel
      Virtual Channel Extension.<1>
      mstscax!CDynVCPlugin::OnStaticDataReceived
      (no data)

  dynvc_caps_version2:
    doc: |
      MS-RDPEDYC 2.2.1.1.2 Version 2 (DYNVC_CAPS_VERSION2)
      The DYNVC_CAPS_VERSION2 PDU is sent by the DVC server manager to indicate
      that it supports version 2 of the Remote Desktop Protocol: Dynamic Virtual
      Channel Extension.<3>
      mstscax!CDynVCPlugin::OnStaticDataReceived
    seq:
      - id: priority_charge_0
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_1
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_2
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_3
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.

  dynvc_caps_version3:
    doc: |
      MS-RDPEDYC 2.2.1.1.3 Version 3 (DYNVC_CAPS_VERSION3)
      The DYNVC_CAPS_VERSION3 PDU is sent by the DVC server manager to indicate
      that it supports version 3 of the Remote Desktop Protocol: Dynamic Virtual
      Channel Extension.<5>
      mstscax!CDynVCPlugin::OnStaticDataReceived
    seq:
      - id: priority_charge_0
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_1
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_2
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.
      - id: priority_charge_3
        type: u2
        doc: |
          A 16-bit unsigned integer. Specifies the amount of bandwidth that is
          allotted for each priority class.

  dynvc_caps_rsp:
    doc: |
      MS-RDPEDYC 2.2.1.2 DVC Capabilities Response PDU (DYNVC_CAPS_RSP)
      The DYNVC_CAPS_RSP (section 2.2.1.2) PDU is sent by the DVC client manager to the DVC
      server manager acknowledging the version level capabilities supported.
      mstscax!CDynVCPlugin::OnStaticDataReceived
    seq:
      - id: pad
        type: u1
        doc: |
          An 8-bit unsigned integer. Unused. MUST be set to 0x00.
      - id: version
        type: u2
        doc: |
          A 16-bit unsigned integer that indicates the protocol version level supported.

  dynvc_data_soft_sync_req:
    doc: |
      MS-RDPEDYC 2.2.5.1 Soft-Sync Request PDU (DYNVC_SOFT_SYNC_REQUEST)
      A DYNVC_SOFT_SYNC_REQUEST PDU is sent by a DVC server manager over the DRDYNVC static
      virtual channel on the main RDP connection.
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCPlugin::UseSoftSyncProtocolExtensions
      mstscax!CDynVCPlugin::ValidateSoftSyncPDU
      mstscax!CDynVCPlugin::ProcessSoftSyncPDU
      mstscax!CDynVCPlugin::GetTransportForTunnelType
      mstscax!CDynVCChannel::SetNewTransport
      mstscax!CDynVCPlugin::SwitchTransports
    seq:
      - id: pad
        type: u1
        doc: |
          An 8-bit, unsigned integer. Unused. MUST be set to 0x00.
      - id: length
        type: u4
        doc: |
          A 32-bit, unsigned integer indicating the total size, in bytes, of the Length, Flags,
          NumberOfTunnels, and SoftSyncChannelLists fields.
      - id: flags
        type: u2
        doc: |
          A 16-bit, unsigned integer that specifies the contents of this PDU.
      - id: number_of_tunnels
        type: u2
        doc: |
          A 16-bit, unsigned integer that indicates the number of multitransport tunnels on which
          dynamic virtual channel data will be written by the server manager.
      - id: soft_sync_channel_lists
        type: dynvc_soft_sync_channel_list
        repeat: expr
        repeat-expr: number_of_tunnels
        doc: |
          This field can contain one or more DYNVC_SOFT_SYNC_CHANNEL_LIST (section 3.1.5.1.1)
          structures as indicated by the NumberOfTunnels field and the SOFT_SYNC_CHANNEL_LIST_PRESENT
          flag.
    enums:
      soft_sync:
        0x01:
          id: tcp_flushed
          doc: |
            Indicates that no more data will be sent over TCP for the specified DVCs.
            This flag MUST be set.
        0x02:
          id: channel_list_present
          doc: |
            Indicates that one or more Soft-Sync Channel Lists (section 3.1.5.1.1) are present in
            this PDU.

  dynvc_soft_sync_channel_list:
    doc: |
      MS-RDPEDYC 2.2.5.1.1 Soft-Sync Channel List (DYNVC_SOFT_SYNC_CHANNEL_LIST)
      One or more DYNVC_SOFT_SYNC_CHANNEL_LISTs are contained in a Soft-Sync Request PDU to indicate
      which dynamic virtual channels have data written by the server on the specified multitransport
      tunnel. The values specified in the TunnelType and ChannelId fields MUST NOT appear in more
      than one Soft-Sync Channel List.
      mstscax!CDynVCPlugin::ValidateSoftSyncPDU
      mstscax!CDynVCPlugin::ProcessSoftSyncPDU
    seq:
      - id: tunnel_type
        type: u4
        enum: tunneltype
        doc: |
          Indicates the target tunnel type for the transport switch.
      - id: number_of_dvcs
        type: u2
        doc: |
          A 16-bit, unsigned integer indicating the number of DVCs that will have data written by the
          server manager on this tunnel.
      - id: list_of_dvc_ids
        type: u4
        repeat: expr
        repeat-expr: number_of_dvcs
        doc: |
          One or more 32-bit, unsigned integers, as indicated by the NumberOfDVCs field, containing the
          channel ID of each DVC that will have data written by the server manager on this tunnel.
    enums:
      tunneltype:
        0x00000001:
          id: udpfecr
          doc: |
            RDP-UDP Forward Error Correction (FEC) multitransport tunnel ([MS-RDPEMT] section 1.3).
        0x00000003:
          id: udpfecl
          doc: |
            RDP-UDP FEC lossy multitransport tunnel ([MS-RDPEMT] section 1.3).

  dynvc_data_soft_sync_rsp:
    doc: |
      MS-RDPEDYC 2.2.5.2 Soft-Sync Response PDU (DYNVC_SOFT_SYNC_RESPONSE)
      A DYNVC_SOFT_SYNC_RESPONSE PDU is sent by a DVC client manager over the
      DRDYNVC static virtual channel on the main RDP connection in response to
      a Soft-Sync Request PDU (section 2.2.5.1). This PDU MUST include all of
      the multitransport tunnels that will be used by the client manager for
      writing data to a DVC.
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCPlugin::GenerateAndSendSoftSyncResponse
    seq:
      - id: number_of_tunnels
        type: u4
        doc: |
          A 32-bit, unsigned integer indicating the number of multitransport
          tunnels on which DVC data will be written by the client manager.
      - id: tunnels_to_switch
        type: u4
        enum: tunneltype
        repeat: expr
        repeat-expr: number_of_tunnels
        doc: |
          One or more 32-bit, unsigned integers, as indicated by the NumberOfTunnels
          field, containing the type of each tunnel on which DVC data will be written
          by the client manager.
    enums:
      tunneltype:
        0x00000001:
          id: udpfecr
          doc: |
            RDP-UDP Forward Error Correction (FEC) multitransport tunnel ([MS-RDPEMT] section 1.3).
        0x00000003:
          id: udpfecl
          doc: |
            RDP-UDP FEC lossy multitransport tunnel ([MS-RDPEMT] section 1.3).

  dynvc_create_req:
    doc: |
      MS-RDPEDYC 2.2.2.1 DVC Create Request PDU (DYNVC_CREATE_REQ)
      The DYNVC_CREATE_REQ (section 2.2.2.1) PDU is sent by the DVC server manager
      to the DVC client manager to request that a channel be opened.
      mstscax!CDynVCPlugin::OnCreatePacket
    seq:
      - id: channel_name
        type: strz
        encoding: ASCII
        doc: |
          A null-terminated ANSI encoded character string. The name of the
          listener on the TS client with which the TS server application is
          requesting that a channel be opened.

  dynvc_create_rsp:
    doc: |
      MS-RDPEDYC 2.2.2.2 DVC Create Response PDU (DYNVC_CREATE_RSP)
      The DYNVC_CREATE_RSP (section 2.2.2.2) PDU is sent by the DVC client manager
      to indicate the status of the client DVC create operation.
      mstscax!CDynVCPlugin::OnCreatePacket
    seq:
      - id: channel_status
        type: u4
        doc: |
          A 32-bit, signed integer that specifies the HRESULT code that indicates
          success or failure of the client DVC creation. HRESULT codes are specified
          in [MS-ERREF] section 2.1. A zero or positive value indicates success; a
          negative value indicates failure.

  dynvc_close_req:
    doc: |
      MS-RDPEDYC 2.2.4 Closing a DVC (DYNVC_CLOSE)
      A DYNVC_CLOSE (section 2.2.4) PDU is sent by either a DVC server manager or a DVC
      client manager to close a DVC. A DYNVC_CLOSE (section 2.2.4) PDU is used for both
      a close request and a close response.
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCChannel::Close
      mstscax!CDynVCPlugin::SendChannelClose
      (no data)

  dynvc_close_rsp:
    doc: |
      MS-RDPEDYC 2.2.4 Closing a DVC (DYNVC_CLOSE)
      A DYNVC_CLOSE (section 2.2.4) PDU is sent by either a DVC server manager or a DVC
      client manager to close a DVC. A DYNVC_CLOSE (section 2.2.4) PDU is used for both
      a close request and a close response.
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCChannel::Close
      mstscax!CDynVCPlugin::SendChannelClose
      (no data)

  dynvc_data:
    doc: |
      MS-RDPEDYC 2.2.3.2 DVC Data PDU (DYNVC_DATA)
      The DYNVC_DATA PDU is used to send both single messages and blocks of fragmented
      messages when compression is not being used for the data block.
      A single DYNVC_DATA PDU is used to send a message when the total length of the
      message data is less than or equal to 1,590 bytes.
      Multiple DYNVC_DATA PDUs are used to send messages that have been fragmented and
      that are sent subsequent to a DYNVC_DATA_FIRST (section 2.2.3.1) PDU. DYNVC_DATA
      PDUs are sent until the entire fragmented message has been sent.
      mstscax!CDynVCPlugin::OnCreatePacket
    params:
      - id: to_server
        type: bool
      - id: channel_type
        type: u1
        enum: channel_type
    seq:
      - id: data
        size-eos: true
        doc: |
          An array of bytes. Message data is sent as 8-bit unsigned integers. The maximum
          size of the array is 1,600 minus the length of the DYNVC_DATA header in bytes.
          The actual size of this field is the length of the packet after reassembly, as
          described in [MS-RDPBCGR] section 3.1.5.2.2.1, minus the space taken for Cmd,
          Sp, cbId, and ChannelId fields.
        type:
          switch-on: channel_type
          -name: type
          cases:
            'channel_type::raw.to_i': rdp_raw_pdu
            'channel_type::echo.to_i': echo_pdu
            'channel_type::microsoft_windows_rds_input.to_i': rdpinput_pdu
            'channel_type::microsoft_windows_rds_graphics.to_i': rdpgfx_pdu
            'channel_type::microsoft_windows_rds_auth_redirection.to_i': auth_redirection_pdu
            'channel_type::microsoft_windows_rds_telemetry.to_i': rdp_telemetry_pdu
            'channel_type::microsoft_windows_rds_location.to_i': location_pdu
            'channel_type::microsoft_windows_rds_geometry.to_i': geometry_pdu
            'channel_type::microsoft_windows_rds_display_control.to_i': displaycontrol_pdu
            'channel_type::microsoft_windows_rds_remote_app_graphics_redirection.to_i': rdpgfx_redir_pdu
            'channel_type::microsoft_windows_rds_video_control_v08_01.to_i': tsmm_video_control_pdu
            'channel_type::microsoft_windows_rds_video_data_v08_01.to_i': tsmm_video_data_pdu
            'channel_type::microsoft_windows_rds_frame_buffer_control_v08_01.to_i': fbr_pdu
            'channel_type::audio_playback_dvc.to_i': rdpsnd_pdu
            'channel_type::audio_playback_lossy_dvc.to_i': rdpsnd_lossy_pdu
            'channel_type::audio_input.to_i': sndin_pdu
            'channel_type::xpsrd.to_i': xpsrd_req_pdu  # TODO
            'channel_type::tsvctkt.to_i': tsvctkt_req_pdu  # TODO
            'channel_type::tsmf.to_i': tsmf_req_pdu
            'channel_type::pnpdr.to_i': pnpdr_pdu
            'channel_type::file_redirector_channel.to_i': file_redirector_request_pdu
            'channel_type::rdcamera_device_enumerator.to_i': rdcamera_device_enumerator_pdu
            'channel_type::rdcamera_device.to_i': rdcamera_device_pdu
            'channel_type::wdag_dvc.to_i': wdag_dvc_pdu

  dynvc_data_first:
    doc: |
      MS-RDPEDYC 2.2.3.1 DVC Data First PDU (DYNVC_DATA_FIRST)
      The DYNVC_DATA_FIRST PDU is used to send the first block of data of a fragmented
      message when compression is not being used for the data block. It MUST be the first
      PDU sent when a message has been fragmented and the data block is not compressed.
      The total length, in bytes, of the message to be sent is indicated in the Length
      field, and the data field contains the first block of the fragmented data.
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCChannel::OnData
    params:
      - id: len
        type: u1
        doc: |
          Indicates the length of the Length field.
      - id: to_server
        type: bool
      - id: channel_type
        type: u1
        enum: channel_type
    seq:
      - id: length_byte_1
        type: u1
      - id: length_byte_2
        type: u1
        if: len > 0
      - id: length_byte_3
        type: u1
        if: len > 1
      - id: length_byte_4
        type: u1
        if: len > 1
      - id: data
        size-eos: true
        type: dynvc_data(to_server, channel_type)
        doc: |
          An array of bytes. The first block of data of a fragmented message. Message data
          is sent as 8-bit unsigned integers. The DVC header size is defined as the sum of
          the sizes of the Cmd, Len, cbId, ChannelId and Length fields. The length of the
          data in the Data field is determined as follows:
            - If the sum of the DVC header size and the value specified by the Length field
              is less than 1,600 bytes, then the actual data length equals the value specified
              by the Length field.
            - If the sum of the DVC header size and the value specified by the Length field
              is equal to or larger than 1,600 bytes, then the actual data length equals 1,600
              bytes minus the DVC header size.
    instances:
      len_u4:
        value: len << 0
      length_u4_1:
        value: length_byte_1 << 0
      length_u4_2:
        value: length_byte_2 << 0
      length_u4_3:
        value: length_byte_3 << 0
      length_u4_4:
        value: length_byte_4 << 0
      length_size:
        value: 1 << len_u4
      length_mask:
        value: (1 << ((length_size * 8) - 1)) | ((1 << ((length_size * 8) - 1)) - 1)
      length:
        value: length_mask & (((length_byte_4 << 24) & 0xff000000) | ((length_byte_3 << 16) & 0xff0000) | ((length_byte_2 << 8) & 0xff00) | ((length_byte_1 << 0) & 0xff))
        doc: |
          A variable length 8-bit, 16-bit, or 32-bit unsigned integer. Set to total
          length of the message to be sent.

  dynvc_data_first_compressed:
    doc: |
      MS-RDPEDYC 2.2.3.3 DVC Data First Compressed PDU (DYNVC_DATA_FIRST_COMPRESSED)
      The DYNVC_DATA_FIRST_COMPRESSED PDU is used to send the first block of data of a
      fragmented message when the data block is compressed. It MUST be the first PDU sent
      when the message has been fragmented and the data block is compressed. The total
      uncompressed length, in bytes, of the message to be sent is indicated in the Length
      field, and the data field contains the first block of the fragmented, compressed data.
      This PDU MUST NOT be used unless both DVC managers support version 3 of the Remote
      Desktop Protocol: Dynamic Virtual Channel Extension, and a reliable transport is being
      used (UDP-R or TCP).
      mstscax!CDynVCPlugin::OnStaticDataReceived
      mstscax!CDynVCChannel::OnCompressedData
      mstscax!DecompressRdp8::Decompress
    params:
      - id: len
        type: u1
        doc: |
          Indicates the length of the Length field.
    seq:
      - id: length_byte_1
        type: u1
      - id: length_byte_2
        type: u1
        if: len > 0
      - id: length_byte_3
        type: u1
        if: len > 1
      - id: length_byte_4
        type: u1
        if: len > 1
      - id: data
        size-eos: true
        type: rdp_segmented_data
        doc: |
          An RDP_SEGMENTED_DATA ([MS-RDPEGFX] section 2.2.5.1) structure containing a single
          RDP8_BULK_ENCODED_DATA ([MS-RDPEGFX] section 2.2.5.3) segment. The segment contains
          the first block of data in a fragmented message, where the data has been compressed
          with the RDP 8.0 Bulk Compression algorithm ([MS-RDPEGFX] section 3.1.9.1).
    instances:
      len_u4:
        value: len << 0
      length_u4_1:
        value: length_byte_1 << 0
      length_u4_2:
        value: length_byte_2 << 0
      length_u4_3:
        value: length_byte_3 << 0
      length_u4_4:
        value: length_byte_4 << 0
      length_size:
        value: 1 << len_u4
      length_mask:
        value: (1 << ((length_size * 8) - 1)) | ((1 << ((length_size * 8) - 1)) - 1)
      length:
        value: length_mask & (((length_byte_4 << 24) & 0xff000000) | ((length_byte_3 << 16) & 0xff0000) | ((length_byte_2 << 8) & 0xff00) | ((length_byte_1 << 0) & 0xff))
        doc: |
          A variable length 8-bit, 16-bit, or 32-bit unsigned integer. Set to total
          length of the message to be sent.

  dynvc_data_compressed:
    doc: |
      MS-RDPEDYC 2.2.3.4 DVC Data Compressed PDU (DYNVC_DATA_COMPRESSED)
      The DYNVC_DATA_COMPRESSED PDU is used to send both single messages and blocks
      of fragmented messages when the data block is compressed.
      A single DYNVC_DATA_COMPRESSED PDU is used to send a message when the total
      length of the original message data, before compression, is less than or equal
      to 1,590 bytes.
      Multiple DYNVC_DATA_COMPRESSED PDUs are used to send messages that have been
      fragmented and are sent subsequent to a DYNVC_DATA_FIRST_COMPRESSED
      (section 2.2.3.3) PDU. DYNVC_DATA_COMPRESSED PDUs are sent until the entire
      fragmented message has been sent. This PDU MUST NOT be used unless both DVC
      managers support version 3 of the Remote Desktop Protocol: Dynamic Virtual
      Channel Extension, and a reliable transport is being used (UDP-R or TCP).
      mstscax!CDynVCPlugin::OnStaticDataReceived
    seq:
      - id: data
        size-eos: true
        type: rdp_segmented_data
        doc: |
          An RDP_SEGMENTED_DATA ([MS-RDPEGFX] section 2.2.5.1) structure containing a single
          RDP8_BULK_ENCODED_DATA ([MS-RDPEGFX] section 2.2.5.3) segment. The segment contains
          the first block of data in a fragmented message, where the data has been compressed
          with the RDP 8.0 Bulk Compression algorithm ([MS-RDPEGFX] section 3.1.9.1).
