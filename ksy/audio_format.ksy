meta:
  id: audio_format
  endian: le

doc: |
  MS-RDPEA 2.2.2.1.1 Audio Format (AUDIO_FORMAT)
  The AUDIO_FORMAT structure is used to describe a supported audio format.
  mstscax!CRdpAudioController::DataArrived
  mstscax!CFormatHandler::AddFormat

seq:
  - id: format_tag
    -orig-id: wFormatTag
    type: u2
    doc: |
      An unsigned 16-bit integer identifying the compression format of the audio format.
      It MUST be set to a WAVE form Registration Number listed in [RFC2361]. At a minimum,
      clients and servers MUST support WAVE_FORMAT_PCM (0x0001). All compression formats
      supported on specific Windows versions along with corresponding wFormatTag field values
      are specified by the product behavior note in the data field description of this section.
      structures and constants in mmreg.h
      Know formats:
        - 0x0001: WAVE_FORMAT_PCM
        - 0x0002: WAVE_FORMAT_ADPCM
            - See: mmreg.h ADPCMEWAVEFORMAT
            - C:\Windows\SYSTEM32\msadp32.acm
            - msadp32!DriverProc
            - msadp32!acmdStreamOpen
            - data:
                - u2 wSamplesPerBlock
                - padding of 30 bytes
                - (more data can follow here)
        - 0x0006: WAVE_FORMAT_ALAW
            - C:\Windows\SYSTEM32\msg711.acm
            - msg711!DriverProc
            - msg711!acmdStreamOpen
            - (no data)
        - 0x0007: WAVE_FORMAT_MULAW
            - C:\Windows\SYSTEM32\msg711.acm
            - msg711!DriverProc
            - msg711!acmdStreamOpen
            - (no data)
        - 0x0011: WAVE_FORMAT_DVI_ADPCM
            - See: mmreg.h DVIADPCMWAVEFORMAT
            - C:\Windows\SYSTEM32\imaadp32.acm
            - imaadp32!DriverProc
            - imaadp32!acmdStreamOpen
            - data:
                - u2 wSamplesPerBlock
        - 0x0031: WAVE_FORMAT_GSM610
            - C:\Windows\SYSTEM32\msgsm32.acm
            - msgsm32!DriverProc
            - msgsm32!acmdStreamOpen
            - data:
                - u2 wSamplesPerBlock
        - 0x0055: WAVE_FORMAT_MPEGLAYER3
            - See: mmreg.h MPEGLAYER3WAVEFORMAT
            - C:\Windows\System32\l3codeca.acm
            - l3codeca!DriverProc
            - l3codeca!acmdStreamOpen
            - l3codeca!l3audioDecodeOpenStream
            - l3codeca!mp3decOpen
            - l3codeca!CMpgaDecoder::CMpgaDecoder
            - data:
                - u2 wID
                - u4 fdwFlags
                - u2 nBlockSize
                - u2 nFramesPerBlock
                - u2 nCodecDelay
        - 0x0161: WAVE_FORMAT_WMAUDIO2
           - See: mmreg.h WMAUDIO2WAVEFORMAT
           - ACM_WMA_DECOMP_KEY "1A0F78F0-EC8A-11d2-BBBE-006008320064"
           - If size is exactly 49, will copy over ACM_WMA_DECOMP_KEY
        - 0xA106: WAVE_FORMAT_MPEG4_AAC
           - mstscax!CRdpAACDecoder::SetType
           - mstscax!CRdpAACEncoder::SetType
      Full list:
        - C:\Program Files (x86)\Windows Kits\10\Include\<version>\shared\mmreg.h
        - Search for "WAVE form wFormatTag IDs"
      mstscax!CRdpWinAudioCodec::IsFormatSupported
      mstscax!CAudioMaster::AddFormat
      mstscax!CFormatHandler::AddFormat
      mstscax!CFormatHandler::SetDriver
      msacm32!acmDriverEnum
      mstscax!acmDriverEnumCallbackGetACM
      msacm32!acmFormatTagDetailsW
      mstscax!FindSuggestedConverter
      msacm32!acmOpenStream
      msacm32!IDriverMessage
  - id: channels
    -orig-id: nChannels
    type: u2
    doc: |
      An unsigned 16-bit integer that specifies the number of channels in the audio format.
      The number of channels is part of the audio format and is not determined by the
      Remote Desktop Protocol: Audio Output Virtual Channel Extension protocol.
  - id: samples_per_sec
    -orig-id: nSamplesPerSec
    type: u4
    doc: |
      An unsigned 32-bit integer that specifies the number of audio samples per second in
      the audio format.
  - id: avg_bytes_per_sec
    -orig-id: nAvgBytesPerSec
    type: u4
    doc: |
      An unsigned 32-bit integer that specifies the average number of bytes the audio format
      uses to encode one second of audio data.
  - id: block_align
    -orig-id: nBlockAlign
    type: u2
    doc: |
      An unsigned 16-bit integer that specifies the minimum atomic unit of data needed to
      process audio of this format. See [MSDN-AUDIOFORMAT] for more information about block
      alignment semantics.
  - id: bits_per_sample
    -orig-id: wBitsPerSample
    type: u2
    doc: |
      An unsigned 16-bit integer that specifies the number of bits needed to represent a sample.
  - id: size
    -orig-id: cbSize
    type: u2
    doc: |
      An unsigned 16-bit integer specifying the size of the data field.
  - id: data
    size: size
    doc: |
      Extra data specific to the audio format.<4> See [MSDN-AUDIOFORMAT] for additional details
      about extra format information. The size of data, in bytes, is cbSize.
