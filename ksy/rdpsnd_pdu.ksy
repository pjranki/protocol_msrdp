meta:
  id: rdpsnd_pdu
  endian: le
  imports:
    - audio_format

doc: |
  MS-RDPEA 2.2.1 RDPSND PDU Header (SNDPROLOG)
  The RDPSND PDU header is present in many audio PDUs. It is used to identify
  the PDU type, specify the length of the PDU, and convey message flags.
  Static channel "rdpsnd".
  Dynamic channel "AUDIO_PLAYBACK_DVC".
  Dynamic channel "AUDIO_PLAYBACK_LOSSY_DVC".
  mstscax!CRdpAudioPlaybackListenerCallback::OnNewChannelConnection
  mstscax!CRdpAudioPlaybackChannelCallback::OnDataReceived
  mstscax!CRdpAudioController::DataArrived

seq:
  - id: msg_type
    -orig-id: msgType
    enum: sndc
    type: u1
    doc: |
      An 8-bit unsigned integer that specifies the type of audio PDU that follows
      the BodySize field.
  - id: pad
    -orig-id: bPad
    type: u1
    if: rdpsnd_pdu_header
    doc: |
      An 8-bit unsigned integer. Unused. The value in this field is arbitrary and
      MUST be ignored on receipt.
  - id: body_size
    -orig-id: BodySize
    type: u2
    if: rdpsnd_pdu_header
    doc: |
      A 16-bit unsigned integer. If msgType is not set to 0x02 (SNDC_WAVE), then this
      field specifies the size, in bytes, of the data that follows the RDPSND PDU header.
      If msgType is set to 0x02 (SNDC_WAVE), then the representation of BodySize is
      explained in the Header field in section 2.2.3.3.
  - id: body
    doc: |
      A 16-bit unsigned integer. If msgType is not set to 0x02 (SNDC_WAVE), then this field
      specifies the size, in bytes, of the data that follows the RDPSND PDU header.
      If msgType is set to 0x02 (SNDC_WAVE), then the representation of BodySize is
      explained in the Header field in section 2.2.3.3.
    type:
      switch-on: msg_type
      -name: type
      cases:
        'sndc::wave_data': sndwav
        'sndc::close': sndclose
        'sndc::wave': sndwavinfo
        'sndc::setvolume': sndvol
        'sndc::setpitch': sndpitch
        'sndc::waveconfirm': sndwav_confirm
        'sndc::training': sndtraining
        'sndc::formats': snd_version_and_formats
        'sndc::cryptkey': sndcrypt
        'sndc::waveencrypt': sndwavcrypt
        'sndc::udpwave': sndudpwave
        'sndc::udpwavelast': sndudpwavelast
        'sndc::qualitymode': sndqualitymode
        'sndc::wave2': sndwave2

instances:
  rdpsnd_pdu_header:
    value: 'not ( (msg_type == sndc::udpwave) or (msg_type == sndc::udpwavelast) )'

enums:
  sndc:
    0x00:
      id: wave_data
      doc: Data sent after SNDWAVINFO pdu.
    0x01:
      id: close
      doc: Close PDU
    0x02:
      id: wave
      doc: WaveInfo PDU
    0x03:
      id: setvolume
      doc: Volume PDU
    0x04:
      id: setpitch
      doc: Pitch PDU
    0x05:
      id: waveconfirm
      doc: Wave Confirm PDU
    0x06:
      id: training
      doc: Training PDU or Training Confirm PDU
    0x07:
      id: formats
      doc: Server Audio Formats and Version PDU or Client Audio Formats and Version PDU
    0x08:
      id: cryptkey
      doc: Crypt Key PDU
    0x09:
      id: waveencrypt
      doc: Wave Encrypt PDU
    0x0A:
      id: udpwave
      doc: UDP Wave PDU
    0x0B:
      id: udpwavelast
      doc: UDP Wave Last PDU
    0x0C:
      id: qualitymode
      doc: Quality Mode PDU
    0x0D:
      id: wave2
      doc: Wave2 PDU

types:
  snd_version_and_formats:
    doc: |
      MS-RDPEA 2.2.2.1 Server Audio Formats and Version PDU (SERVER_AUDIO_VERSION_AND_FORMATS)
      The Server Audio Formats and Version PDU is a PDU used by the server to send version
      information and a list of supported audio formats to the client.
      This PDU MUST be sent using virtual channels.
      MS-RDPEA 2.2.2.2 Client Audio Formats and Version PDU (CLIENT_AUDIO_VERSION_AND_FORMATS)
      The Client Audio Formats and Version PDU is a PDU that is used to send version information,
      capabilities, and a list of supported audio formats from the client to the server.<5>
      After the server sends its version and a list of supported audio formats to the client,
      the client sends back a Client Audio Formats and Version PDU to the server containing its
      version and a list of formats that both the client and server support.
      This PDU MUST be sent by using virtual channels.
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::CleanSoundFormats
      mstscax!CRdpAudioController::ChooseSoundFormat
      mstscax!CRdpWinAudioCodec::IsFormatSupported
      mstscax!CAudioMaster::AddFormat
      mstscax!CFormatHandler::AddFormat
    seq:
      - id: flags
        -orig-id: dwFlags
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the general capability flags.
      - id: volume
        -orig-id: dwVolume
        type: u4
        doc: |
          A 32-bit unsigned integer. If the TSSNDCAPS_VOLUME flag is not set in the dwFlags field,
          the dwVolume field MUST be ignored. If the TSSNDCAPS_VOLUME flag is set in the dwFlags
          field, the dwVolume field specifies the initial volume of the audio stream. The low-order
          word contains the left-channel volume setting, and the high-order word contains the
          right-channel setting. A value of 0xFFFF represents full volume, and a value of 0x0000 is
          silence.
          This value is to be interpreted logarithmically. This means that the perceived increase in
          volume is the same when increasing the volume level from 0x5000 to 0x6000 as it is from
          0x4000 to 0x5000.
      - id: pitch
        -orig-id: dwPitch
        type: u4
        doc: |
          A 32-bit unsigned integer. If the TSSNDCAPS_PITCH flag is not set in the dwFlags field, the
          dwPitch field MUST be ignored. If the TSSNDCAPS_PITCH flag is set in the dwFlags field, the
          dwPitch field specifies the initial pitch of the audio stream. The pitch is specified as a
          fixed-point value. The high-order word contains the signed integer part of the number, and
          the low-order word contains the fractional part. A value of 0x8000 in the low-order word
          represents one-half, and 0x4000 represents one-quarter. For example, the value 0x00010000
          specifies a multiplier of 1.0 (no pitch change), and a value of 0x000F8000 specifies a
          multiplier of 15.5.
      - id: d_gram_port
        -orig-id: wDGramPort
        type: u2
        doc: |
          A 16-bit unsigned integer that, if set to a nonzero value, specifies the client port that
          the server MUST use to send data over UDP. A zero value means UDP is not supported. This
          field MUST be specified by using big-endian byte ordering.
      - id: number_of_formats
        -orig-id: wNumberOfFormats
        type: u2
        doc: |
          A 16-bit unsigned integer. Number of AUDIO_FORMAT structures contained in the
          sndFormats array.
      - id: last_block_confirmed
        -orig-id: cLastBlockConfirmed
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the initial value for the cBlockNo counter
          used by the WaveInfo PDU, Wave2 PDU, Wave Encrypt PDU, UDP Wave PDU, and UDP Wave
          Last PDU. The value sent by the server is arbitrary. See section 3.3.5.2.1.1 for
          more information about the cBlockNo counter.
      - id: version
        -orig-id: wVersion
        type: u2
        doc: |
          A 16-bit unsigned integer that contains the version of the protocol supported by
          the server/client.<3>
      - id: pad
        -orig-id: bPad
        type: u1
        doc: |
          An 8-bit unsigned integer. This field is unused. The value is arbitrary and
          MUST be ignored on receipt.
      - id: snd_formats
        -orig-id: sndFormats
        type: audio_format
        repeat: expr
        repeat-expr: number_of_formats
        doc: |
          A variable-sized array of audio formats supported by the server, each conforming
          in structure to the AUDIO_FORMAT structure. The number of formats in the array is
          wNumberOfFormats.
          A variable-sized array of audio formats that are supported by the client and the server,
          each conforming in structure to the AUDIO_FORMAT. Each audio format MUST also appear
          in the Server Audio Formats and Version PDU list of audio formats just sent by the server.
          The number of formats in the array is wNumberOfFormats.
    enums:
      tssndcaps:
        0x00000001:
          id: alive
          doc: |
            The client is capable of consuming audio data. This flag MUST be set for audio data
            to be transferred.
        0x00000002:
          id: volume
          doc: |
            The client is capable of applying a volume change to all the audio data that is received.
        0x00000004:
          id: pitch
          doc: |
            The client is capable of applying a pitch change to all the audio data that is received.

  sndqualitymode:
    doc: |
      MS-RDPEA 2.2.2.3 Quality Mode PDU
      The Quality Mode PDU is a PDU used by the client to select one of three quality modes. If both
      the client and server are at least version 6, the client MUST send a Quality Mode PDU immediately
      after sending the audio formats. This packet is only used when the client and server versions are
      both at least 6.<7> This PDU MUST be sent using virtual channels.
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::sendQualityMode
    seq:
      - id: quality_mode
        -orig-id: wQualityMode
        type: u2
        doc: |
          A 16-bit unsigned integer. This field specifies the quality setting the client has requested.
          The definition of these three modes is implementation-dependent, but SHOULD use the following
          guidelines:
            0x0000: DYNAMIC_QUALITY
                    The server dynamically adjusts the audio format to best match the bandwidth and latency
                    characteristics of the network.
            0x0001: MEDIUM_QUALITY
                    The server chooses an audio format from the list of formats the client supports that
                    gives moderate audio quality and requires a moderate amount of bandwidth.
            0x0002: HIGH_QUALITY
                    The server chooses the audio format that provides the best quality audio without regard
                    to the bandwidth requirements for that format.
      - id: reserved
        -orig-id: Reserved
        type: u2
        doc: |
          A 16-bit unsigned integer. This field is unused. The value is arbitrary and MUST be ignored on
          receipt.

  sndcrypt:
    doc: |
      MS-RDPEA 2.2.2.4 Crypt Key PDU (SNDCRYPT)
      The Crypt Key PDU is a PDU used to send a 32-byte key from the server to the client. The key is used
      to encrypt some audio data sent over UDP. This PDU MUST be sent using virtual channels.
    seq:
      - id: reserved
        -orig-id: Reserved
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is unused. The value is arbitrary and MUST be ignored on
          receipt.
      - id: seed
        -orig-id: Seed
        size: 32
        doc: |
          A 32-byte symmetric key used for encryption and decryption of audio data sent over UDP. A random
          number SHOULD be used as the symmetric key. When a Wave Encrypt PDU is sent, the key MUST be used
          to encrypt the audio data. When a UDP Wave PDU is sent with a UDP Wave Last PDU, there is no
          encrypted audio data and the key MUST be used instead to generate a signature.

  sndtraining:
    doc: |
      MS-RDPEA 2.2.3.1 Training PDU (SNDTRAINING)
      The Training PDU is a PDU used by the server to request that the client send it a Training
      Confirm PDU. In response, the client MUST immediately send a Training Confirm PDU to the
      server. The server uses the sending and receiving of these packets for diagnostic purposes.
      This PDU can be sent using virtual channels or UDP.
      mstscax!CRdpAudioController::DataArrived
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer. In the Training PDU this value is arbitrary.
      - id: packet_size
        -orig-id: wPacketSize
        type: u2
        doc: |
          A 16-bit unsigned integer. If the size of data is nonzero, then this field specifies the
          size, in bytes, of the entire PDU. If the size of data is 0, then wPackSize MUST be 0.
      - id: data
        size-eos: true
        doc: |
          Unused. The value in this field is arbitrary and MUST be ignored on receipt.

  sndtrainingconfirm:
    doc: |
      MS-RDPEA 2.2.3.2 Training Confirm PDU (SNDTRAININGCONFIRM)
      The Training Confirm PDU is a PDU sent by the client to confirm the reception of a Training PDU.
      This PDU MUST be sent using virtual channels or UDP. The server MAY use data from this PDU to
      calculate how fast the network can transmit data, as described in section 3.3.5.1.1.5.
      mstscax!CRdpAudioController::DataArrived
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer. This value MUST be set to the same value as the wTimeStamp field
          in the Training PDU received from the server. If the value is not set as indicated, the result
          from the server-side calculation (section 3.3.5.1.1.5) will be invalid.
      - id: packet_size
        -orig-id: wPacketSize
        type: u2
        doc: |
          A 16-bit unsigned integer. This value MUST be set to the same value as the wPackSize field in
          the Training PDU received from the server. If the value is not set as indicated, the result
          from the server-side calculation (section 3.3.5.1.1.5) will be invalid.

  sndwavinfo:
    doc: |
      MS-RDPEA 2.2.3.3 WaveInfo PDU (SNDWAVINFO)
      The WaveInfo PDU is the first of two consecutive PDUs used to transmit audio data over
      virtual channels. This packet contains information about the audio data along with the
      first 4 bytes of the audio data itself. This PDU MUST be sent using static virtual
      channels.
      mstscax!CRdpAudioPlaybackChannelCallback::OnDataReceived
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::StopCloseTimer
      mstscax!CRdpAudioController::OnWaveData
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer representing the time stamp of the audio data. It SHOULD
          be set to a time that represents when this PDU is built.<8>
      - id: format_no
        -orig-id: wFormatNo
        type: u2
        doc: |
          A 16-bit unsigned integer that represents an index into the list of audio formats
          exchanged between the client and server during the initialization phase, as described
          in section 3.1.1.2. The format located at that index is the format of the audio data
          in this PDU and the Wave PDU that immediately follows this packet.
      - id: block_no
        -orig-id: cBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the block ID of the audio data. When the client
          notifies the server that it has consumed the audio data, it sends a Wave Confirm PDU
          (section 2.2.3.8) containing this field in its cConfirmedBlockNo field.
      - id: pad
        -orig-id: bPad
        size: 3
        doc: |
          A 24-bit unsigned integer. This field is unused. The value is arbitrary and MUST be
          ignored on receipt.
      - id: data
        size: 4
        doc: |
          The first four bytes of the audio data. The rest of the audio data arrives in the next
          PDU, which MUST be a Wave PDU. The audio data MUST be in the audio format from the list
          of formats exchanged during the Initialization Sequence (section 2.2.2); this list is
          found at the index specified in the wFormatNo field.

  sndwav:
    doc: |
      MS-RDPEA 2.2.3.4 Wave PDU (SNDWAV)
      The Wave PDU is the second of two consecutive PDUs used to transmit audio data over virtual
      channels. This packet contains the rest of the audio data not sent in the WaveInfo PDU. This
      PDU MUST be sent using virtual channels.
      mstscax!CRdpAudioPlaybackChannelCallback::OnDataReceived
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::StopCloseTimer
      mstscax!CRdpAudioController::OnWaveData
      mstscax!CRdpAudioController::DetectLoss
      mstscax!CRdpAudioController::DetectGlitch
      mstscax!CRdpAudioController::SaveBlockInfo
      mstscax!CRdpAudioController::SendConfirmation
    seq:
      # NOTE: bPad is not defined in seq as it overlaps with the 4-byte header.
      - id: data
        size-eos: true
        doc: |
          The rest of the audio data. The size of the audio data MUST be equal to the BodySize field
          of the RDPSND PDU header of the WaveInfo PDU that immediately preceded this packet, minus
          the size of the preceding WaveInfo PDU packet (not including the size of its Header field).
          The format of the audio data MUST be the format specified in the list of formats exchanged
          during the Initialization Sequence and found at the index specified in the wFormatNo field
          of the preceding WaveInfo PDU.

  sndwavcrypt:
    doc: |
      MS-RDPEA 2.2.3.5 Wave Encrypt PDU (SNDWAVCRYPT)
      The Wave Encrypt PDU is a PDU used to send audio data from the server to the client. This PDU MUST
      be sent over UDP.
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer representing the time stamp of the audio data. It SHOULD be set to
          a time that represents when this PDU is built<9>.
      - id: format_no
        -orig-id: wFormatNo
        type: u2
        doc: |
          A 16-bit unsigned integer that represents an index into the list of formats exchanged between
          the client and server during the initialization phase, as described in section 3.1.1.2.
      - id: block_no
        -orig-id: cBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the block ID of the audio data. When the client notifies
          the server that it has consumed the audio data, it sends a Wave Confirm PDU containing this
          field in its cConfirmedBlockNo field.
      - id: pad
        -orig-id: bPad
        size: 3
        doc: |
          A 24-bit unsigned integer. This field is unused. The value is arbitrary and MUST be ignored on
          receipt.
      - id: signature
        size: 8
        doc: |
          An 8-byte digital signature. If the protocol version of either the server or the client is less
          than 5, then this field MUST NOT exist. If the version of the server and the client are at least 5,
          then this field MUST exist. An explanation of how this field is created is specified in section
          3.3.5.2.1.3.
      - id: data
        size-eos: true
        doc: |
          Encrypted audio data. The audio data MUST be in the format specified by the wFormatNo and MUST
          be encrypted. For an explanation of how the data is encrypted, see section 3.3.5.2.1.3.

  sndudpwave:
    doc: |
      MS-RDPEA 2.2.3.6 UDP Wave PDU (SNDUDPWAVE)
      The UDP Wave PDU is a PDU used to send a fragment of audio data from the server to the client.
      This packet is only used when the client and server versions are both at least 5.
      This PDU MUST be sent over UDP.
    seq:
      - id: block_no
        -orig-id: cBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the block ID of the audio data. When the client notifies
          the server that it has consumed the audio data, it sends a Wave Confirm PDU containing this
          field in its cConfirmedBlockNo field.
      - id: frag_no_byte_1
        type: u1
      - id: frag_no_byte_2
        type: u1
        if: (frag_no_byte_1 & 0x80) != 0
      - id: data
        type: audio_fragdata
        doc: |
          A portion of an Audio FragData structure. Several UDP Wave PDUs and a UDP Wave Last PDU
          contain pieces of a structure conforming to Audio FragData. This algorithm is specified
          in section 3.2.5.2.1.5.
    instances:
      frag_no_byte_1_u4:
        value: frag_no_byte_1 << 0
      frag_no:
        -orig-id: cFragNo
        value: '(frag_no_byte_1 & 0x80) != 0 ? ((frag_no_byte_1_u4 << 8) & 0x7f00) | frag_no_byte_2 : frag_no_byte_1 & 0x7f'
        doc: |
          An 8-bit or 16-bit unsigned integer specifying the order of the audio data fragment in the
          overall audio sample. The 0x80 bit of the first byte is used to determine if the field is
          one or two bytes in length. If the first byte is less than 0x80, then the field is 1 byte.
          If the first byte is greater than or equal to 0x80, then this field is 2 bytes. To calculate
          the value of the field, the second byte holds 8 low-order bits, while the first byte holds
          7 high-order bits.

  audio_fragdata:
    doc: |
      MS-RDPEA 2.2.3.6.1 Audio FragData (AUDIO_FRAGDATA)
      The Audio FragData structure is used to describe the data that is fragmented and sent in
      several UDP Wave PDUs and a final UDP Wave Last PDU.
    seq:
      - id: signature
        -orig-id: Signature
        size: 8
        doc: |
          An 8-byte digital signature. The algorithm for creating this field is the same as creating
          the signature field of a Wave Encrypt PDU as specified in section 3.3.5.2.1.3.
      - id: data
        -orig-id: Data
        size-eos: true
        doc: |
          Audio data. The format of the audio data MUST be the format specified in the wFormatNo field
          of the UDP Wave Last PDU that sends the final piece of this structure.

  sndudpwavelast:
    doc: |
      MS-RDPEA 2.2.3.7 UDP Wave Last PDU (SNDUDPWAVELAST)
      The UDP Wave Last PDU is a PDU used to send the final fragment of audio data from the server to
      the client. This packet is only used when the client and server versions are both at least 5.
      This PDU MUST be sent over UDP.
    seq:
      - id: total_size
        -orig-id: wTotalSize
        type: u2
        doc: |
          A 16-bit unsigned integer that represents the total size of the audio data sent in successive
          PDUs. The amount of audio data in previous UDP Wave PDUs plus the amount of audio data in this
          PDU MUST be equivalent to wTotalSize.
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer representing the time stamp of the audio data.
      - id: format_no
        -orig-id: wFormatNo
        type: u2
        doc: |
          A 16-bit unsigned integer that represents an index into the list of formats exchanged between
          the client and server during the initialization phase, as described in section 3.1.1.2.
      - id: block_no
        -orig-id: cBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the block id of the audio data. When the client notifies
          the server that it has consumed the audio data, it sends a Wave Confirm PDU containing this field
          in its cConfirmedBlockNo field.
      - id: pad
        -orig-id: bPad
        size: 3
        doc: |
          A 24-bit unsigned integer. This field is unused. The value is arbitrary and MUST be ignored on
          receipt.
      - id: data
        -orig-id: Data
        type: audio_fragdata
        doc: |
          A portion of an Audio FragData. Several UDP Wave PDUs and a UDP Wave Last PDU MUST contain pieces
          of a structure conforming to Audio FragData, as specified in section 3.2.5.2.1.5.

  sndwav_confirm:
    doc: |
      MS-RDPEA 2.2.3.8 Wave Confirm PDU (SNDWAV_CONFIRM)
      The Wave Confirm PDU is a PDU that MUST be sent by the client to the server immediately after the
      following two events occur:
        - An audio data sample is received from the server, whether using a WaveInfo PDU and Wave PDU,
          a Wave2 PDU, a Wave Encrypt PDU, or several UDP Wave PDUs followed by a UDP Wave Last PDU.
        - The audio data sample is emitted to completion by the client.
      This PDU can be sent using static virtual channels or UDP.
      mstscax!CRdpAudioController::SendConfirmation
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer. See section 3.2.5.2.1.6 for details of how this field is set.
      - id: confirm_block_no
        -orig-id: cConfirmedBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer that MUST be the same as the cBlockNo field of the UDP Wave Last PDU
          (section 2.2.3.7), the Wave Encrypt PDU (section 2.2.3.5) or the WaveInfo PDU (section 2.2.3.3)
          just received from the server.
      - id: pad
        -orig-id: bPad
        type: u1
        doc: |
          An unsigned 8-bit integer. This field is unused. The value is arbitrary and MUST be ignored on
          receipt.

  sndclose:
    doc: |
      MS-RDPEA 2.2.3.9 Close PDU (SNDCLOSE)
      The Close PDU is a PDU sent by the server to notify the client that audio streaming has stopped.
      This PDU MUST be sent using virtual channels.
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::StartCloseTimer
      mstscax!RdpWinTaskScheduler::QueueTimedTask
      mstscax!RdpAudioCancellableTask::ExecuteTask
      mstscax!CRdpAudioController::RunTask
      mstscax!CRdpAudioController::HandleClose
      mstscax!CRdpAudioController::CleanData
      mstscax!CRdpAudioController::EmptyBlockList
      mstscax!CRdpAudioController::EmptyDataBufferedInDeviceList
      mstscax!CRdpWinAudioWaveoutPlayback::Stop
      (no data)

  sndwave2:
    doc: |
      MS-RDPEA 2.2.3.10 Wave2 PDU (SNDWAVE2)
      The Wave2 PDU is used to transmit audio data over virtual channels.
      mstscax!CRdpAudioPlaybackChannelCallback::OnDataReceived
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpAudioController::OnWaveData
    seq:
      - id: time_stamp
        -orig-id: wTimeStamp
        type: u2
        doc: |
          A 16-bit unsigned integer representing the time stamp of the audio data. It SHOULD<10>
          be set to a time that represents when this PDU is built.
      - id: format_no
        -orig-id: wFormatNo
        type: u2
        doc: |
          A 16-bit unsigned integer that represents an index into the list of audio formats
          exchanged between the client and server during the initialization phase, as described in
          section 3.1.1.2. The format located at that index is the format of the audio data in this
          PDU and the Wave PDU that immediately follows this packet.
          A change of format here will result in the following calls:
            - mstscax!CRdpAudioController::OnNewFormat
            - mstscax!CRdpWinAudioCodec::SetCurrentFormat
      - id: block_no
        -orig-id: cBlockNo
        type: u1
        doc: |
          An 8-bit unsigned integer specifying the block ID of the audio data. When the client
          notifies the server that it has consumed the audio data, it sends a Wave Confirm PDU
          (section 2.2.3.8) containing this field in its cConfirmedBlockNo field.
      - id: pad
        -orig-id: bPad
        size: 3
        doc: |
          A 24-bit unsigned integer. This field is unused. The value is arbitrary and MUST be ignored
          on receipt.
      - id: audio_time_stamp
        -orig-id: dwAudioTimeStamp
        type: u4
        doc: |
          A 32-bit unsigned integer representing the timestamp when the server gets audio data from
          the audio source. The timestamp is the number of milliseconds that have elapsed since the
          system was started. This timestamp SHOULD be used to sync the audio stream with a video
          stream remoted using the Remote Desktop Protocol: Video Optimized Remoting Virtual Channel
          Extension (see the hnsTimestampOffset and hnsTimestamp fields as specified in [MS-RDPEVOR]
          sections 2.2.1.2 and 2.2.1.6, respectively).
      - id: data
        size-eos: true
        doc: |
          Audio data. The format of the audio data MUST be the format specified in the list of formats
          exchanged during the initialization sequence and found at the index specified in the
          wFormatNo field.

  sndvol:
    doc: |
      MS-RDPEA 2.2.4.1 Volume PDU (SNDVOL)
      The Volume PDU is a PDU sent from the server to the client to specify the volume to be set on
      the audio stream. For this packet to be sent, the client MUST have set the flag TSSNDCAPS_VOLUME
      (0x0000002) in the Client Audio Formats and Version PDU (section 2.2.2.2) that is sent during the
      initialization sequence described in section 2.2.2.
      mstscax!CRdpAudioController::DataArrived
      mstscax!CRdpWinAudioWaveoutPlayback::SetVolume
      winmm!waveOutSetVolume
    seq:
      - id: volume
        -orig-id: volume
        type: u4
        doc: |
          A 32-bit unsigned integer specifying the volume to be set on the audio stream.
          See the dwVolume field in section 2.2.2.2 for semantics of the data in this field.

  sndpitch:
    doc: |
      MS-RDPEA 2.2.4.2 Pitch PDU (SNDPITCH)
      The Pitch PDU is a PDU sent from the server to the client to specify the pitch to be set on the
      audio stream. For this packet to be sent, the client MUST have set the flag TSSNDCAPS_PITCH
      (0x0000004) in the Client Audio Formats and Version PDU (section 2.2.2.2) that is sent during the
      initialization sequence specified in section 2.2.2.
    seq:
      - id: pitch
        -orig-id: Pitch
        type: u4
        doc: |
          A 32-bit unsigned integer. Although the server can send this PDU, the client MUST ignore it.
