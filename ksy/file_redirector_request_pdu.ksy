meta:
  id: file_redirector_request_pdu
  endian: le

doc: |
  MS-RDPEPNP 2.2.2 PNP Device I/O Subprotocol
  The messages in the following sections specify the common header and specific messages
  that make up the PNP Device I/O Subprotocol.
  MS-RDPEPNP 2.2.2.1.1 Server Message Header (SERVER_IO_HEADER)
  All I/O Request messages (messages sent from the server to the client) use the following
  Request header.
  Dynamic channel "FileRedirectorChannel" (related/connected to "PNPDR" channel)
  NOTE: Cannot be opened by WDAG (PNPDR channel doesn't created any devices).
  mstscax!CAsyncIIrpStub::OnNewChannelConnection
  mstscax!CRdrServerRequestHandler::OnDataReceived

seq:
  - id: request_id_be
    type: b24
  - id: unused_bits
    -orig-id: UnusedBits
    type: u1
    doc: |
      An 8-bit padding field. Values in this field MUST be ignored.
  - id: function_id
    -orig-id: FunctionId
    type: u4
    enum: function
    doc: |
      A 32-bit unsigned integer. This value identifies the function associated with the request.
      If the value does not match one of these values, the client MUST terminate the dynamic
      virtual channel for the PNP Device I/O Subprotocol.
  - id: data
    type:
      switch-on: function_id
      -name: type
      cases:
        'function::read_request': file_redirector_read_request
        'function::write_request': file_redirector_write_request
        'function::iocontrol_request': file_redirector_iocontrol_request
        'function::iocancel_request': file_redirector_iocancel_request
        'function::create_file_request': file_redirector_create_file_request
        'function::capabilities_request': file_redirector_capabilities_request
        'function::specific_iocancel_request': file_redirector_specific_iocancel_request

instances:
  request_id:
    -orig-id: RequestId
    value: ((request_id_be << 16) & 0xff0000) | ((request_id_be << 8) & 0x00ff00) | ((request_id_be << 0) & 0x00ff00)
    doc: |
      A 24-bit unsigned integer. This server-generated value uniquely identifies the request.
      This value MUST be used to refer to the request in subsequent messages. A request ID MAY
      be reused after the reply message with that ID is received.

enums:
  function:
    0x00000000: read_request
    0x00000001: write_request
    0x00000002: iocontrol_request
    0x00000003: iocancel_request
    0x00000004: create_file_request
    0x00000005: capabilities_request
    0x00000006: specific_iocancel_request

types:
  file_redirector_read_request:
    doc: |
      MS-RDPEPNP 2.2.2.3.3 Read Request Message
      The server sends this message to request a read operation from the specified redirected
      client device.
      mstscax!CRdrServerRequestHandler::DoRead
      mstscax!CStubAsyncFileReadCallback::GetInstanceOf
      mstscax!CFileRedirector::Read
      ntdll!NtReadFile
      mstscax!CStubAsyncFileReadCallback::OnReadComplete
      mstscax!CRdrServerRequestHandler::CompleteRead
    seq:
      - id: size
        -orig-id: cbBytesToRead
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies how many bytes the server requested to
          read from the redirected client device.
      - id: offset_high
        -orig-id: OffsetHigh
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the high offset value for the
          read operation.
      - id: offset_low
        -orig-id: OffsetLow
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the low offset value for the
          read operation.

  file_redirector_write_request:
    doc: |
      MS-RDPEPNP 2.2.2.3.5 Write Request Message
      The server sends this message to perform a write operation on a redirected client device.
      mstscax!CRdrServerRequestHandler::DoWrite
      mstscax!CStubAsyncFileWriteCallback::GetInstanceOf
      mstscax!CFileRedirector::Write
      ntdll!NtWriteFile
      mstscax!CStubAsyncFileWriteCallback::OnWriteComplete
      mstscax!CRdrServerRequestHandler::CompleteWrite
    seq:
      - id: size
        -orig-id: cbWrite
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the size of the data to be written.
      - id: offset_high
        -orig-id: OffsetHigh
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the high offset value for the
          write operation.
      - id: offset_low
        -orig-id: OffsetLow
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the low offset value for the
          write operation.
      - id: data
        -orig-id: Data
        size: size
        doc: |
          A variable-length array of bytes. This field MUST contain the data to be written
          to the particular device.
      - id: unused_byte
        -orig-id: UnusedByte
        type: u1
        doc: |
          An 8-bit unsigned integer. This field is unused and MUST be ignored.

  file_redirector_iocontrol_request:
    doc: |
      MS-RDPEPNP 2.2.2.3.7 IOControl Request Message
      A server sends this message to perform an IOControl operation on the client-side device.
      mstscax!CRdrServerRequestHandler::DoIoControl
      mstscax!CStubAsyncIoControlCallback::GetInstanceOf
      mstscax!CFileRedirector::IoControl
      ntdll!NtDeviceIoControlFile
      mstscax!CStubAsyncIoControlCallback::OnIoControlComplete
      mstscax!CRdrServerRequestHandler::CompleteIoControl
    seq:
      - id: io_code
        -orig-id: IoCode
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the I/O control code to be sent to
          the client device. The IoCode is specific to the redirected device driver; this
          specification cannot specify all possible values for the IoCode field.
      - id: size_in
        -orig-id: cbIn
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the input buffer size.
          This field MAY be 0x00000000.
      - id: size_out
        -orig-id: cbOut
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the output buffer size that can be
          returned using the Data field of the IOControl Reply Message (section 2.2.2.3.8).
          This field MAY be 0x00000000.
      - id: data_in
        -orig-id: DataIn
        size: size_in
        doc: |
          A variable-length array of bytes. The DataIn buffer MUST contain the input data.
          The length of this field is specified by the cbIn field of this message.
      - id: data_out
        -orig-id: DataOut
        size: size_out
        doc: |
          A variable-length, optional array of bytes. The size of field DataOut SHOULD be equal
          to the value provided in field cbOut.
      - id: unused_byte
        -orig-id: UnusedByte
        type: u1
        doc: |
          An 8-bit unsigned integer. This field is unused, MAY be any value, and MUST be ignored.

  file_redirector_iocancel_request:
    doc: |
      UNDOCUMENTED
      Cancels all pending input and output (I/O) operations.
      mstscax!CRdrServerRequestHandler::DoIoCancel
      mstscax!CFileRedirector::IoCancel
      kernelbase!CancelIo
      (no data)

  file_redirector_create_file_request:
    doc: |
      MS-RDPEPNP 2.2.2.3.1 CreateFile Request Message
      A server sends this message to open a file handle on the client-side device. This message MUST
      be sent only once for a given connection within the dynamic virtual channel. A one-to-one
      correspondence exists between file handles opened on the client side and dynamic virtual channels
      used. All I/O traffic that is associated with a file handle MUST be done on the virtual channel
      used to create the file handle. As a result, to open multiple file handles, multiple dynamic
      virtual channels are established between client and server.
      mstscax!CRdrServerRequestHandler::DoCreateFileRedirector
      mstscax!CIrpRedirector::Create
      mstscax!CFileRedirector::Initialize
      mstscax!CLocalDevice::GetInterfaceGuid
      mstscax!CLocalDevice::GetFileName
      setupapi!SetupDiGetClassDevsW
      setupapi!SetupDiEnumDeviceInterfaces
      setupapi!SetupDiGetDeviceInterfaceDetailW
      mstscax!CRdrServerRequestHandler::OnFileCreated
    seq:
      - id: device_id
        -orig-id: DeviceId
        type: u4
        doc: |
          A 32-bit unsigned integer. This field MUST identify the device redirected by the client.
          Device IDs are initially established as described in section 2.2.1.3.1.
      - id: desired_access
        -orig-id: dwDesiredAccess
        type: u4
        doc: |
          A 32-bit unsigned integer. This is a flag field that indicates various access modes to use for
          creating and opening the file. This value SHOULD be set to 0xC0000000, meaning generic read and
          generic write.<9>
      - id: share_mode
        -orig-id: dwShareMode
        type: u4
        doc: |
          A 32-bit unsigned integer that represents a set of bit flags indicating the sharing mode that
          the server application requested.
      - id: creation_disposition
        -orig-id: dwCreationDisposition
        type: u4
        enum: file_disposition
        doc: |
          A 32-bit unsigned integer that specifies the mode for creating or opening the file.
      - id: flags_and_attributes
        -orig-id: dwFlagsAndAttributes
        type: u4
        doc: |
          A 32-bit unsigned integer that represents a set of bit flags specifying other flags and
          attributes associated with the request.
    enums:
      file_share:
         0x00000001: read
         0x00000002: write
      file_disposition:
        0x00000001: create_new
        0x00000002: create_always
        0x00000003: open_existing
        0x00000004: open_always
        0x00000005: truncate_existing
      file_attribute:
        0x00000010: directory
        0x00000020: archive
        0x00000040: device
        0x00000080: normal
      file_flag:
        0x00080000: first_pipe_instance
        0x00100000: open_no_recall
        0x00200000: open_reparse_point
        0x01000000: posix_semantics
        0x02000000: backup_semantics
        0x04000000: delete_on_close
        0x08000000: sequential_scan
        0x10000000: random_access
        0x20000000: no_buffering
        0x40000000: overlapped
        0x80000000: write_through

  file_redirector_capabilities_request:
    doc: |
      MS-RDPEPNP 2.2.2.2.1 Server Capabilities Request Message
      A server sends this message to indicate its version information to the client.
      mstscax!CRdrServerRequestHandler::DoCapabilities
    seq:
      - id: version
        -orig-id: version
        type: u2
        doc: |
          A 16-bit unsigned integer. This field SHOULD indicate the version of the server-side
          implementation of the PNP Device I/O Subprotocol.
            - 0x0004: This server version does not support custom event redirection.
            - 0x0006: This server version supports custom event redirection.

  file_redirector_specific_iocancel_request:
    doc: |
      MS-RDPEPNP 2.2.2.3.9 Specific IoCancel Request Message
      The server sends this message to the client to cancel a specific I/O request.
      mstscax!CRdrServerRequestHandler::DoIoCancelRequest
      mstscax!CStubCancelFileIo::Cancel
      mstscax!CCancelIoFunc::CancelIoFunc
      kernelbase!CancelIoEx
      kernelbase!CancelIo
    seq:
      - id: id_to_cancel_be
        type: b24
      - id: unused_bits
        -orig-id: UnusedBits
        type: u1
        doc: |
          An 8-bit unsigned integer. This field is unused and SHOULD be set to 0x00.
    instances:
      id_to_cancel:
        -orig-id: idToCancel
        value: ((id_to_cancel_be << 16) & 0xff0000) | ((id_to_cancel_be << 8) & 0x00ff00) | ((id_to_cancel_be << 0) & 0x00ff00)
        doc: |
          A 24-bit unsigned integer. This field specifies the RequestId for the
          I/O request to cancel.
