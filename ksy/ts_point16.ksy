meta:
  id: ts_point16
  endian: le

doc: |
  MS-RDPBCGR 2.2.9.1.1.4.1 Point (TS_POINT16)
  The TS_POINT16 structure specifies a point relative to the top-left
  corner of the server's desktop.

seq:
  - id: x_pos
    type: u2
    doc: |
      A 16-bit, unsigned integer. The x-coordinate relative to the top-left
      corner of the server's desktop.
  - id: y_pos
    type: u2
    doc: |
      A 16-bit, unsigned integer. The y-coordinate relative to the top-left
      corner of the server's desktop.
