meta:
  id: tsmm_platform_cookie
  -orig-id: TSMM_PLATFORM_COOKIE
  endian: le

doc: |
  MS-RDPEV 2.2.9 TSMM_PLATFORM_COOKIE Constants
  The platform type is defined by platform cookie values.

seq:
  - id: value
    type: u4
    enum: tsmm_platform

enums:
  tsmm_platform:
    0:
      id: cookie_undefined
      -orig-id: TSMM_PLATFORM_COOKIE_UNDEFINED
      doc: Platform undefined.
    1:
      id: cookie_mf
      -orig-id: TSMM_PLATFORM_COOKIE_MF
      doc: |
        The MF platform. For more information about the MF platform, see [MSDN-MEDIAFOUNDATION].
    2:
      id: cookie_dshow
      -orig-id: TSMM_PLATFORM_COOKIE_DSHOW
      doc: |
        The DShow platform. For more information about the DShow platform, see [MSDN-DIRECTSHOW].
