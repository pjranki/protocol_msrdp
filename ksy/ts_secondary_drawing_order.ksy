meta:
  id: ts_secondary_drawing_order
  endian: le
  imports:
    - ts_cd_header
    - ts_bitmap_data_ex

doc: |
  MS-RDPEGDI 2.2.2.2.1.2 Secondary Drawing Order
  mstscax!CUH::ProcessOrders

params:
  - id: control_flags
    type: u1
    doc: |
      An 8-bit, unsigned integer. A control byte that identifies the class of
      the drawing order and describes the fields that are included in the order
      and the type of coordinates being used.

seq:
  - id: order_length
    type: s2
    doc: |
      A 16-bit, signed integer. The encoded length in bytes of the secondary
      drawing order, including the size of the header. When constructing the order,
      the value in the orderLength field MUST be 13 bytes less than the actual order
      length. Hence, when decoding the order, the orderLength field MUST be adjusted
      by adding 13 bytes. These adjustments are for historical reasons.
  - id: extra_flags
    type: u2
    doc: |
      A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
  - id: order_type_value
    type: u1
    doc: |
      An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
  - id: order_data
    size: order_size
    type:
      switch-on: order_type
      -name: enum
      cases:
        'ts_cache::bitmap_uncompressed': ts_cache_bitmap_order(control_flags, extra_flags, order_type_value)
        'ts_cache::color_table': ts_cache_color_table_order(control_flags, extra_flags, order_type_value)
        'ts_cache::bitmap_compressed': ts_cache_bitmap_order(control_flags, extra_flags, order_type_value)
        'ts_cache::glyph': ts_cache_glyph_order(control_flags, extra_flags, order_type_value)
        'ts_cache::bitmap_uncompressed_rev2': ts_cache_bitmap_rev2_order(control_flags, extra_flags, order_type_value)
        'ts_cache::bitmap_compressed_rev2': ts_cache_bitmap_rev2_order(control_flags, extra_flags, order_type_value)
        'ts_cache::brush': ts_cache_brush_order(control_flags, extra_flags, order_type_value)
        'ts_cache::bitmap_compressed_rev3': ts_cache_bitmap_rev3_order(control_flags, extra_flags, order_type_value)

instances:
  order_type:
    value: order_type_value
    enum: ts_cache
    doc: |
      An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
  order_size:
    value: order_length + 13 - 6
    doc: |
      When constructing the order, the value in the orderLength field MUST be 13 bytes
      less than the actual order length. Hence, when decoding the order, the orderLength
      field MUST be adjusted by adding 13 bytes. These adjustments are for historical
      reasons.

enums:
  ts_cache:
    0x00:
      id: bitmap_uncompressed
      doc: |
        Cache Bitmap - Revision 1 (section 2.2.2.2.1.2.2) Secondary Drawing Order
        with an uncompressed bitmap.
    0x01:
      id: color_table
      doc: |
        Cache Color Table (section 2.2.2.2.1.2.4) Secondary Drawing Order.
    0x02:
      id: bitmap_compressed
      doc: |
        Cache Bitmap - Revision 1 (section 2.2.2.2.1.2.2) Secondary Drawing Order
        with a compressed bitmap.
    0x03:
      id: glyph
      doc: |
        Cache Glyph - Revision 1 (section 2.2.2.2.1.2.5) or Cache Glyph - Revision 2
        (section 2.2.2.2.1.2.6) Secondary Drawing Order. The version is indicated by
        the extraFlags field.
    0x04:
      id: bitmap_uncompressed_rev2
      doc: |
        Cache Bitmap - Revision 2 (section 2.2.2.2.1.2.3) Secondary Drawing Order with
        an uncompressed bitmap.
    0x05:
      id: bitmap_compressed_rev2
      doc: |
        Cache Bitmap - Revision 2 (section 2.2.2.2.1.2.3) Secondary Drawing Order with
        a compressed bitmap.
    0x07:
      id: brush
      doc: |
        Cache Brush (section 2.2.2.2.1.2.7) Secondary Drawing Order.
    0x08:
      id: bitmap_compressed_rev3
      doc: |
        Cache Bitmap - Revision 3 (section 2.2.2.2.1.2.8) Secondary Drawing Order with
        a compressed bitmap.

types:
  two_byte_unsigned_encoding:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.1.2 Two-Byte Unsigned Encoding (TWO_BYTE_UNSIGNED_ENCODING)
      The TWO_BYTE_UNSIGNED_ENCODING structure is used to encode a value in the range
      0x0000 to 0x7FFF by using a variable number of bytes. For example, 0x1A1B is encoded
      as { 0x9A, 0x1B }. The most significant bit of the first byte encodes the number of
      bytes in the structure.
      mstscax!Decode2ByteField
    seq:
      - id: val1
        type: b7
        doc: |
          A 7-bit, unsigned integer field containing the most significant 7 bits of the
          value represented by this structure.
      - id: c
        type: b1
        doc: |
          A 1-bit, unsigned integer field that contains an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: two_byte_encoding
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      two_byte_encoding:
        value: 'c ? true : false'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      value:
        value: 'two_byte_encoding ? (((val1_u4 << 8) & 0x7F00) | (val2_u4 & 0x00ff)) : (val1_u4 & 0x007f)'
        doc: Value in the range 0x0000 to 0x7FFF.

  two_byte_signed_encoding:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.1.3 Two-Byte Signed Encoding (TWO_BYTE_SIGNED_ENCODING)
      The TWO_BYTE_SIGNED_ENCODING structure is used to encode a value in the range
      -0x3FFF to 0x3FFF by using a variable number of bytes. For example, -0x1A1B is encoded
      as { 0xDA, 0x1B }, and -0x0002 is encoded as { 0x42 }. The most significant bits of the
      first byte encode the number of bytes in the structure and the sign.
      mstscax!Decode2ByteSignedField
    seq:
      - id: val1
        type: b6
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: s
        type: b1
        doc: |
          A 1-bit, unsigned integer field containing an encoded representation of whether
          the value is positive or negative.
      - id: c
        type: b1
        doc: |
          A 1-bit, unsigned integer field that contains an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: two_byte_encoding
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      two_byte_encoding:
        value: 'c ? true : false'
      sign:
        value: 's ? -1 : 1'
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      value:
        value: 'sign * (two_byte_encoding ? (((val1_u4 << 8) & 0x3F00) | (val2_u4 & 0x00ff)) : (val1_u4 & 0x003f))'
        doc: Value in the range -0x3FFF to 0x3FFF.

  four_byte_unsigned_encoding:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.1.4 Four-Byte Unsigned Encoding (FOUR_BYTE_UNSIGNED_ENCODING)
      The FOUR_BYTE_UNSIGNED_ENCODING structure is used to encode a value in the range
      0x00000000 to 0x3FFFFFFF by using a variable number of bytes. For example, 0x001A1B1C
      is encoded as { 0x9A, 0x1B, 0x1C }. The two most significant bits of the first byte
      encode the number of bytes in the structure.
      mstscax!Decode4ByteField
    seq:
      - id: val1
        type: b6
        doc: |
          A 6-bit, unsigned integer field containing the most significant 6 bits of the
          value represented by this structure.
      - id: c
        type: b2
        doc: |
          A 2-bit, unsigned integer field containing an encoded representation of the
          number of bytes in this structure.
      - id: val2
        type: u1
        if: c > 0
        doc: |
          An 8-bit, unsigned integer containing the second most significant bits of the
          value represented by this structure.
      - id: val3
        type: u1
        if: c > 1
        doc: |
          An 8-bit, unsigned integer containing the third most significant bits of the
          value represented by this structure.
      - id: val4
        type: u1
        if: c > 2
        doc: |
          An 8-bit, unsigned integer containing the least significant bits of the value
          represented by this structure.
    instances:
      val1_u4:
        value: val1 << 0
      val2_u4:
        value: val2 << 0
      val3_u4:
        value: val3 << 0
      val4_u4:
        value: val4 << 0
      val1_shifted:
        value: (val1_u4 & 0x3f) << (8 * c)
      val2_shifted:
        value: 'c > 0 ? ((val2_u4 & 0xff) << (8 * (c - 1))) : 0'
      val3_shifted:
        value: 'c > 1 ? ((val3_u4 & 0xff) << (8 * (c - 2))) : 0'
      val4_shifted:
        value: 'c > 2 ? (val4_u4 & 0xff) : 0'
      value:
        value: val1_shifted | val2_shifted | val3_shifted | val4_shifted
        doc: Value in the range 0x00000000 to 0x3FFFFFFF.

  ts_cache_bitmap_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.2 Cache Bitmap - Revision 1 (CACHE_BITMAP_ORDER)
      The Cache Bitmap - Revision 1 Secondary Drawing Order is used by the server
      to instruct the client to store a bitmap in a particular Bitmap Cache entry.
      This order only supports memory-based bitmap caching. Support for the Revision
      1 bitmap caches (section 3.1.1.1.1) is specified in the Revision 1 Bitmap Cache
      Capability Set ([MS-RDPBCGR] section 2.2.7.1.4.1).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHProcessCacheBitmapOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: cache_id
        type: u1
        doc: |
          An 8-bit, unsigned integer. The ID of the bitmap cache in which the bitmap
          data MUST be stored. This value MUST be in the range 0 to 2 (inclusive).
      - id: pad1_octet
        type: u1
        doc: |
          An 8-bit, unsigned integer. Padding. Values in this field are arbitrary and
          MUST be ignored.
      - id: bitmap_width
        type: u1
        doc: |
          An 8-bit, unsigned integer. The width of the bitmap in pixels.
      - id: bitmap_height
        type: u1
        doc: |
          An 8-bit, unsigned integer. The height of the bitmap in pixels.
      - id: bitmap_bits_per_pixel
        type: u1
        doc: |
          An 8-bit, unsigned integer. The color depth of the bitmap data in bits per pixel.
      - id: bitmap_length
        type: u2
        doc: |
          A 16-bit, unsigned integer. The size in bytes of the data in the bitmapComprHdr
          and bitmapDataStream fields.
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the target entry in the destination bitmap
          cache (specified by the cacheId field) where the bitmap data MUST be stored. This
          value MUST be greater than or equal to 0 and less than the maximum number of entries
          allowed in the destination bitmap cache. The maximum number of entries allowed in each
          individual bitmap cache is specified in the Revision 1 Bitmap Cache Capability Set
          ([MS-RDPBCGR] section 2.2.7.1.4.1) by the Cache0Entries, Cache1Entries, and Cache2Entries
          fields.
      - id: bitmap_compr_hdr
        type: ts_cd_header
        if: (order_type == 0x02) and ((extra_flags & 0x0400) == 0)
        doc: |
          Optional Compressed Data Header structure (see [MS-RDPBCGR] section 2.2.9.1.1.3.1.2.3)
          describing the bitmap data in the bitmapDataStream. This field MUST be present if the
          TS_CACHE_BITMAP_COMPRESSED (0x02) flag is present in the header field, but the
          NO_BITMAP_COMPRESSION_HDR (0x0400) flag is not.
      - id: bitmap_data_stream
        size: bitmap_data_stream_size
        doc: |
          A variable-length byte array containing bitmap data (the format of this data is defined
          in [MS-RDPBCGR] section 2.2.9.1.1.3.1.2.2).
    instances:
      bitmap_data_stream_size:
        value: '((order_type == 0x02) and ((extra_flags & 0x0400) == 0)) ? bitmap_length - 8 : bitmap_length'

  ts_cache_bitmap_rev2_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.3 Cache Bitmap - Revision 2 (CACHE_BITMAP_REV2_ORDER)
      The Cache Bitmap - Revision 2 Secondary Drawing Order is used by the server to instruct
      the client to store a bitmap in a particular Bitmap Cache entry. This order supports
      persistent disk bitmap caching and uses a compact encoding format. Support for the Revision
      2 bitmap caches (section 3.1.1.1.1) is specified in the Revision 2 Bitmap Cache Capability
      Set ([MS-RDPBCGR] section 2.2.7.1.4.2).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHProcessCacheBitmapOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: key1
        type: u4
        if: (flags & 0x02) != 0
        doc: |
          A 32-bit, unsigned integer. The low 32 bits of the 64-bit persistent bitmap cache key.
      - id: key2
        type: u4
        if: (flags & 0x02) != 0
        doc: |
          A 32-bit, unsigned integer. The high 32 bits of the 64-bit persistent bitmap cache key.
      - id: bitmap_width
        type: two_byte_unsigned_encoding
        doc: |
          A Two-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.2) structure. The width of the
          bitmap in pixels.
      - id: bitmap_height
        type: two_byte_unsigned_encoding
        if: (flags & 0x01) == 0
        doc: |
          A Two-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.2) structure. The height of the
          bitmap in pixels.
      - id: bitmap_length
        type: four_byte_unsigned_encoding
        doc: |
          A Four-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.4) structure. The size in bytes
          of the data in the bitmapComprHdr and bitmapDataStream fields.
      - id: cache_index
        type: two_byte_unsigned_encoding
        doc: |
          A Two-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.2) structure. The index of the target
          entry in the destination bitmap cache (specified by the cacheId field) where the bitmap
          data MUST be stored. If the CBR2_DO_NOT_CACHE flag is not set in the header field, the
          bitmap cache index MUST be greater than or equal to 0 and less than the maximum number of
          entries allowed in the destination bitmap cache. The maximum number of entries allowed in
          each individual bitmap cache is specified in the Revision 2 Bitmap Cache Capability Set
          ([MS-RDPBCGR] section 2.2.7.1.4.2) by the BitmapCache0CellInfo, BitmapCache1CellInfo,
          BitmapCache2CellInfo, BitmapCache3CellInfo, and BitmapCache4CellInfo fields. If the
          CBR2_DO_NOT_CACHE flag is set, the cacheIndex MUST be set to BITMAPCACHE_WAITING_LIST_INDEX
          (32767).
      - id: bitmap_compr_hdr
        type: ts_cd_header
        if: (order_type == 0x05) and ((extra_flags & 0x08) == 0)
        doc: |
          Optional Compressed Data Header structure (see [MS-RDPBCGR] section 2.2.9.1.1.3.1.2.3)
          describing the bitmap data in the bitmapDataStream. This field MUST be present if the
          TS_CACHE_BITMAP_COMPRESSED_REV2 (0x05) flag is present in the header field, but the
          CBR2_NO_BITMAP_COMPRESSION_HDR (0x08) flag is not.
      - id: bitmap_data_stream
        size: bitmap_data_stream_size
        doc: |
          A variable-length byte array containing bitmap data (the format of this data is defined
          in [MS-RDPBCGR] section 2.2.9.1.1.3.1.2.2).
    instances:
      cache_id:
        value: extra_flags & 0x7
        doc: |
          A 3-bit, unsigned integer. The ID of the bitmap cache in which the bitmap data MUST
          be stored. This value MUST be greater than or equal to 0 and less than the number of
          bitmap caches being used for the connection. The number of bitmap caches being used is
          specified by the NumCellCaches field of the Revision 2 Bitmap Cache Capability Set
          ([MS-RDPBCGR] section 2.2.7.1.4.2).
      bits_per_pixel_id:
        value: (extra_flags >> 3) & 0xf
        doc: |
          A 4-bit, unsigned integer. The color depth of the bitmap data in bits per pixel.
      bitmap_bits_per_pixel:
        value: (bits_per_pixel_id - 2) * 8
        doc: |
          The color depth of the bitmap data in bits per pixel.
      flags:
        value: (extra_flags >> 7) & 0x1ff
        doc: |
          A 9-bit, unsigned integer. Operational flags.
      bitmap_data_stream_size:
        value: '((order_type == 0x05) and ((extra_flags & 0x08) == 0)) ? bitmap_length.value - 8 : bitmap_length.value'
    enums:
      cbr2:
        0x01:
          id: height_same_as_width
          doc: |
            Implies that the bitmap height is the same as the bitmap width.
            If this flag is set, the bitmapHeight field MUST NOT be present.
        0x02:
          id: persistent_key_present
          doc: |
            Implies that the bitmap is intended to be persisted, and the
            key1 and key2 fields MUST be present.
        0x08:
          id: no_bitmap_compression_hdr
          doc: |
            Indicates that the bitmapComprHdr field is not present
            (removed for bandwidth efficiency to save 8 bytes).
        0x10:
          id: do_not_cache
          doc: |
            Implies that the cacheIndex field MUST be ignored, and the bitmap
            MUST be placed in the last entry of the bitmap cache specified by cacheId
            field.

  ts_cache_color_table_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.4 Cache Color Table (CACHE_COLOR_TABLE_ORDER)
      The Cache Color Table Secondary Drawing Order is used by the server to instruct
      the client to store a color table in a particular Color Table Cache entry. Color
      tables are used in the MemBlt (section 2.2.2.2.1.1.2.9) and Mem3Blt
      (section 2.2.2.2.1.1.2.10) Primary Drawing Orders.
      Support for color table caching is not specified in the Color Table Cache
      Capability Set (section 2.2.1.1), but is instead implied by support for the MemBlt
      (section 2.2.2.2.1.1.2.9) and Mem3Blt (section 2.2.2.2.1.1.2.10) Primary Drawing
      Orders. If support for these orders is advertised in the Order Capability Set
      (see [MS-RDPBCGR] section 2.2.7.1.3), the existence of a color table cache with
      entries for six palettes is assumed when palettized color is being used, and the
      Cache Color Table is used to update these palettes.
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHProcessCacheColorTableOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: cache_index
        type: u1
        doc: |
          An 8-bit, unsigned integer. An entry in the Cache Color Table where the color
          table MUST be stored. This value MUST be in the range 0 to 5 (inclusive).
      - id: number_colors
        type: u2
        doc: |
          A 16-bit, unsigned integer. The number of Color Quad (section 2.2.2.2.1.2.4.1)
          structures in the colorTable field. This field MUST be set to 256 entries.
      - id: color_table
        type: ts_color_quad
        repeat: expr
        repeat-expr: number_colors
        doc: |
          A Color Table composed of an array of Color Quad (section 2.2.2.2.1.2.4.1)
          structures. The number of entries in the array is given by the numberColors
          field.

  ts_color_quad:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.4.1 Color Quad (TS_COLOR_QUAD)
      The TS_COLOR_QUAD structure is used to express the red, green, and blue components
      necessary to reproduce a color in the additive RGB space.
    seq:
      - id: blue
        type: u1
        doc: An 8-bit, unsigned integer. The blue RGB color component.
      - id: green
        type: u1
        doc: An 8-bit, unsigned integer. The green RGB color component.
      - id: red
        type: u1
        doc: An 8-bit, unsigned integer. The red RGB color component.
      - id: pad1_octet
        type: u1
        doc: An 8-bit, unsigned integer. Padding.

  ts_cache_glyph_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.5 Cache Glyph - (CACHE_GLYPH_ORDER)
      The Cache Glyph -Secondary Drawing Order is used by the server
      to instruct the client to store a glyph in a particular glyph cache entry
      (section 3.1.1.1.2). Support for glyph caching is specified in the Glyph Cache
      Capability Set ([MS-RDPBCGR] section 2.2.7.1.8).
      The revision is determined by the GLYPH_ORDER_REV2 flag.
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsGlyphHandler::ProcessCacheGlyphOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: glyph_order
        type:
          switch-on: revision
          -name: type
          cases:
            1: ts_cache_glyph_rev1_order(control_flags, extra_flags, order_type)
            2: ts_cache_glyph_rev2_order(control_flags, extra_flags, order_type)
    instances:
      flags:
        value: (extra_flags >> 4) & 0xf
      revision:
        value: '((flags & 0x2) != 0) ? 2 : 1'

  ts_cache_glyph_rev1_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.5 Cache Glyph - Revision 1 (CACHE_GLYPH_ORDER)
      The Cache Glyph - Revision 1 Secondary Drawing Order is used by the server
      to instruct the client to store a glyph in a particular glyph cache entry
      (section 3.1.1.1.2). Support for glyph caching is specified in the Glyph Cache
      Capability Set ([MS-RDPBCGR] section 2.2.7.1.8).
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsGlyphHandler::ProcessCacheGlyphOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: cache_id
        type: u1
        doc: |
          An 8-bit, unsigned integer. The ID of the glyph cache in which the glyph data
          MUST be stored. This value MUST be in the range 0 to 9 (inclusive).
      - id: glyphs
        type: u1
        doc: |
          An 8-bit, unsigned integer. The number of glyph entries in the glyphData field.
      - id: glyph_data
        type: ts_cache_glyph_data_rev1
        doc: |
          An array of Cache Glyph Data (section 2.2.2.2.1.2.5.1) structures that describes
          each of the glyphs contained in this order (the number of glyphs is specified by
          the cGlyphs field).
      - id: unicode_characters
        type: str
        encoding: UTF-16LE
        size: glyphs * 2
        if: (extra_flags & 0x10) != 0
        doc: |
          An array of Unicode characters. Contains the Unicode character representation
          of each glyph in the glyphData field. The number of bytes in the field is given
          by cGlyphs * 2. This field MUST NOT be null-terminated. This string is used for
          diagnostic purposes only and is not necessary for successfully decoding and
          caching the glyphs in the glyphData field.

  ts_cache_glyph_data_rev1:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.5.1 Cache Glyph Data (TS_CACHE_GLYPH_DATA)
      The TS_CACHE_GLYPH_DATA structure contains information describing a single glyph 
      that is to be stored in a glyph cache (section 3.1.1.1.2). The ID of this destination
      glyph cache is specified by the cacheId field of the container Cache Glyph (Revision 1)
      Order (section 2.2.2.2.1.2.5).
    seq:
      - id: cache_index
        type: u2
        doc: |
          A 16-bit, unsigned integer. The index of the target entry in the destination glyph
          cache where the glyph data MUST be stored. This value MUST be greater than or equal
          to 0, and less than the maximum number of entries allowed in the destination glyph
          cache. The maximum number of entries allowed in each of the ten glyph caches is
          specified in the GlyphCache field of the Glyph Cache Capability Set ([MS-RDPBCGR]
          section 2.2.7.1.8).
      - id: x
        type: u2
        doc: |
          A 16-bit, signed integer. The X component of the coordinate that defines the origin
          of the character within the glyph bitmap. The top-left corner of the bitmap is (0, 0).
      - id: y
        type: u2
        doc: |
          A 16-bit, signed integer. The Y component of the coordinate that defines the origin
          of the character within the glyph bitmap. The top-left corner of the bitmap is (0, 0).
      - id: cx
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width of the glyph bitmap in pixels.
      - id: cy
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height of the glyph bitmap in pixels.
      - id: aj
        size: aj_bitmap_size
        doc: |
          A variable-sized byte array containing a 1-bit-per-pixel bitmap of the glyph. The
          individual scan lines are encoded in top-down order, and each scan line MUST be
          byte-aligned. Once the array has been populated with bitmap data, it MUST be padded to
          a double-word boundary (the size of the structure in bytes MUST be a multiple of 4).
          For examples of 1-bit-per-pixel encoded glyph bitmaps, see sections 4.6.1 and 4.6.2.
    instances:
      aj_bitmap_size:
        value: (cy * ((cx + 7) / 8) + 3) & 0xFFFFFFFC

  ts_cache_glyph_rev2_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.6 Cache Glyph - Revision 2 (CACHE_GLYPH_REV2_ORDER)
      The Cache Glyph - Revision 2 Secondary Drawing Order is used by the server to instruct
      the client to store a glyph in a particular Glyph Cache entry. This order is similar to
      the Cache Glyph - Revision 1 Secondary Drawing Order (section 2.2.2.2.1.2.5) except that
      it represents glyphs using a more compact format and moves a number of fields into the
      extraFlags field of the secondary order header. Support for glyph caching is specified in
      the Glyph Cache Capability Set ([MS-RDPBCGR] section 2.2.7.1.8).
      mstscax!CUH::ProcessOrders
      mstscax!CTSGraphicsGlyphHandler::ProcessCacheGlyphOrderRev2
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: glyph_data
        type: ts_cache_glyph_data_rev2
        doc: |
          The specification for each of the glyphs in this order (the number of glyphs
          is given by the cGlyphs field embedded in the header field) defined using
          Cache Glyph Data - Revision 2 (section 2.2.2.2.1.2.6.1) structures.
      - id: unicode_characters
        type: str
        encoding: UTF-16LE
        size: glyphs * 2
        doc: |
          An array of Unicode characters. Contains the Unicode character representation
          of each glyph in the glyphData field. The number of bytes in the field is given
          by cGlyphs * 2 (where cGlyphs is embedded in the header field). This field MUST
          NOT be null-terminated. This string is used for diagnostic purposes only and is
          not necessary for successfully decoding and caching the glyphs in the glyphData
          field.
    instances:
      cache_id:
        value: extra_flags & 0xf
        doc: |
          A 4-bit unsigned integer. The ID of the glyph cache in which the glyph data MUST
          be stored. This value MUST be in the range 0 to 9 (inclusive).
      flags:
        value: (extra_flags >> 4) & 0xf
        doc: |
          A 4-bit, unsigned integer. Various operational flags.
      glyphs:
        value: (extra_flags >> 8) & 0xff

  ts_cache_glyph_data_rev2:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.6.1 Cache Glyph Data - Revision 2 (TS_CACHE_GLYPH_DATA_REV2)
      The TS_CACHE_GLYPH_DATA_REV2 structure contains information describing a single glyph
      that is to be stored in a glyph cache (section 3.1.1.1.2). The ID of this destination
      glyph cache is specified by the cacheId field of the container Cache Glyph (Revision 2)
      Order (section 2.2.2.2.1.2.6).
    seq:
      - id: cache_index
        type: u1
        doc: |
          An 8-bit, unsigned integer. The index of the target entry in the destination glyph
          cache where the glyph data MUST be stored. This value MUST be greater than or equal
          to 0, and less than the maximum number of entries allowed in the destination glyph
          cache. The maximum number of entries allowed in each of the ten glyph caches is
          specified in the GlyphCache field of the Glyph Cache Capability Set ([MS-RDPBCGR]
          section 2.2.7.1.8).
      - id: x
        type: two_byte_signed_encoding
        doc: |
          A Two-Byte Signed Encoding (section 2.2.2.2.1.2.1.3) structure. The X component of
          the coordinate that defines the origin of the character within the glyph bitmap. The
          top-left corner of the bitmap is (0, 0).
      - id: y
        type: two_byte_signed_encoding
        doc: |
          A Two-Byte Signed Encoding (section 2.2.2.2.1.2.1.3) structure. The Y component of
          the coordinate that defines the origin of the character within the glyph bitmap. The
          top-left corner of the bitmap is (0, 0).
      - id: cx
        type: two_byte_unsigned_encoding
        doc: |
          A Two-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.2) structure. The width of the
          glyph bitmap in pixels.
      - id: cy
        type: two_byte_unsigned_encoding
        doc: |
          A Two-Byte Unsigned Encoding (section 2.2.2.2.1.2.1.2) structure. The height of the
          glyph bitmap in pixels.
      - id: aj
        size: aj_bitmap_size
        doc: |
          A variable-sized byte array containing a 1-bit-per-pixel bitmap of the glyph. The
          individual scan lines are encoded in top-down order, and each scan line MUST be
          byte-aligned. Once the array has been populated with bitmap data, it MUST be padded to
          a double-word boundary (the size of the structure in bytes MUST be a multiple of 4).
          For examples of 1-bit-per-pixel encoded glyph bitmaps, see sections 4.6.1 and 4.6.2.
    instances:
      aj_bitmap_size:
        value: (cy.value * ((cx.value + 7) / 8) + 3) & 0xFFFFFFFC

  ts_cache_brush_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.7 Cache Brush (CACHE_BRUSH_ORDER)
      The Cache Brush Secondary Drawing Order is used by the server to instruct the
      client to store a brush in a particular Brush Cache entry. Support for brush
      caching is specified in the Brush Cache Capability Set
      (see [MS-RDPBCGR]section 2.2.7.1.7).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHProcessCacheBrushOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: cache_entry
        type: u1
        doc: |
          An 8-bit, unsigned integer. The entry in a specified Brush Cache where the
          brush data MUST be stored. This value MUST be in the range 0 to 63 (inclusive).
      - id: bitmap_format
        type: u1
        doc: |
          An 8-bit, unsigned integer. The color depth of the brush bitmap data.
      - id: cx
        type: u1
        doc: |
          An 8-bit, unsigned integer. The width of the brush bitmap.
      - id: cy
        type: u1
        doc: |
          An 8-bit, unsigned integer. The height of the brush bitmap.
      - id: style
        type: u1
        doc: |
          An 8-bit, unsigned integer. This field is not used, and SHOULD<4> be set to 0x00.
      - id: brush_data_size
        type: u1
        doc: |
          An 8-bit, unsigned integer. The size of the brushData field in bytes.
      - id: brush_data
        size: brush_data_size
        doc: |
          A variable-sized byte array containing binary brush data that 
          represents an 8-by-8-pixel bitmap image. There are 64 pixels 
          in a brush bitmap, and the space used to represent each pixel 
          depends on the color depth of the brush bitmap and the number 
          of colors used. The size of the brushData field in bytes is 
          given by the iBytes field.
          In general, most brushes only use two colors (mono format), and 
          the majority of the remaining ones use four colors or fewer.
          For mono format brushes (iBitmapFormat is BMF_1BPP), brushData 
          contains 8 bytes of 1-bit-per-pixel data, each byte corresponding
          to a row of pixels in the brush. The rows are encoded in reverse
          order; that is, the pixels in the bottom row of the brush are
          encoded in the first byte of the brushData field, and the pixels
          in the top row are encoded in the eighth byte.
          For color brushes, a compression algorithm is used. If the data is
          compressed, the iBytes field is 20 for 256-color (iBitmapFormat is
          BMF_8BPP), 24 for 16-bit color (iBitmapFormat is BMF_16BPP), 28 for
          24-bit color (iBitmapFormat is BMF_24BPP), and 32 for 32-bit color
          (iBitmapFormat is BMF_32BPP). The compression algorithm reduces brush
          data size by storing each brush pixel as a 2-bit index (four possible
          values) into a translation table containing four entries. This equates
          to 2 bytes per brush bitmap line (16 bytes in total) followed by the
          translation table contents. This layout for four-color brushes conforms
          to the Compressed Color Brush (section 2.2.2.2.1.2.7.1) structure.
          For brushes using more than four colors, the data is simply copied
          uncompressed into the brushData at the appropriate color depth.
    instances:
      bitmap_bits_per_pixel:
        value: 'bitmap_format < 3 ? bitmap_format : 8 * (bitmap_format - 2)'
      compressed_8bpp:
        value: ((brush_data_size == 20) and (bitmap_bits_per_pixel == 8))
      compressed_16bpp:
        value: ((brush_data_size == 24) and (bitmap_bits_per_pixel == 16))
      compressed_24bpp:
        value: ((brush_data_size == 28) and (bitmap_bits_per_pixel == 24))
      compressed_32bpp:
        value: ((brush_data_size == 32) and (bitmap_bits_per_pixel == 32))
      compressed:
        value: compressed_8bpp or compressed_16bpp or compressed_24bpp or compressed_32bpp

  ts_cache_bitmap_rev3_order:
    doc: |
      MS-RDPEGDI 2.2.2.2.1.2.8 Cache Bitmap - Revision 3 (CACHE_BITMAP_REV3_ORDER)
      The Cache Bitmap - Revision 3 Secondary Drawing Order is used by the server to
      instruct the client to store a bitmap in a particular Bitmap Cache entry. This
      order supports persistent disk bitmap caching and also enables the use of the
      bitmap codecs specified in the Bitmap Codec Capability Set (see [MS-RDPBCGR]
      section 2.2.7.2.10). Support for the Cache Bitmap - Revision 3 Secondary Drawing
      Order is specified in the Order Capability Set (see [MS-RDPBCGR] section 2.2.7.1.3).
      mstscax!CUH::ProcessOrders
      mstscax!CUH::UHProcessCacheBitmapOrder
    params:
      - id: control_flags
        type: u1
        doc: |
          An 8-bit, unsigned integer. A control byte that identifies the class of
          the drawing order and describes the fields that are included in the order
          and the type of coordinates being used.
      - id: extra_flags
        type: u2
        doc: |
          A 16-bit, unsigned integer. Flags specific to each secondary drawing order.
      - id: order_type
        type: u1
        doc: |
          An 8-bit, unsigned integer. Identifies the type of secondary drawing order.
    seq:
      - id: cache_index
        type: u2
        doc: |
          A 16-bit unsigned integer. The index of the target entry in the destination bitmap
          cache (specified by the cacheId field) where the bitmap data MUST be stored. If the
          CBR3_DO_NOT_CACHE flag is not set in the header field, the bitmap cache index MUST be
          greater than or equal to 0 and less than the maximum number of entries allowed in the
          destination bitmap cache. The maximum number of entries allowed in each individual
          bitmap cache is specified in the Revision 2 Bitmap Cache Capability Set ([MS-RDPBCGR]
          section 2.2.7.1.4.2) by the BitmapCache0CellInfo, BitmapCache1CellInfo,
          BitmapCache2CellInfo, BitmapCache3CellInfo, and BitmapCache4CellInfo fields. If the
          CBR3_DO_NOT_CACHE flag is set, the cacheIndex MUST be set to BITMAPCACHE_WAITING_LIST_INDEX
          (32767).
      - id: key1
        type: u4
        doc: |
          A 32-bit, unsigned integer. The low 32 bits of the 64-bit persistent bitmap cache key.
      - id: key2
        type: u4
        doc: |
          A 32-bit, unsigned integer. The high 32 bits of the 64-bit persistent bitmap cache key.
      - id: bitmap_data
        type: ts_bitmap_data_ex
        doc: |
          An Extended Bitmap Data (see [MS-RDPBCGR] section 2.2.9.2.1.1) structure that contains
          an encoded bitmap image.
    instances:
      cache_id:
        value: extra_flags & 0x7
        doc: |
          A 3-bit, unsigned integer. The ID of the bitmap cache in which bitmap data MUST be stored.
          This value MUST be greater than or equal to 0 and less than the number of bitmap caches
          being used for the connection. The number of bitmap caches being used is specified by the
          NumCellCaches field of the Revision 2 Bitmap Cache Capability Set ([MS-RDPBCGR] section
          2.2.7.1.4.2).
      bits_per_pixel_id:
        value: (extra_flags >> 3) & 0xf
        doc: |
          A 4-bit, unsigned integer. The color depth of the bitmap data in bits per pixel.
      bitmap_bits_per_pixel:
        value: (bits_per_pixel_id - 2) * 8
        doc: |
          The color depth of the bitmap data in bits per pixel.
      flags:
        value: (extra_flags >> 7) & 0x1ff
        doc: |
          A 9-bit, unsigned integer. Operational flags.
    enums:
      cbr3:
        0x08:
          id: ignorable_flag
          doc: |
            This flag has no meaning and its value is ignored by the client.
        0x10:
          id: do_not_cache
          doc: |
            Implies that the cacheIndex field MUST be ignored, and the bitmap MUST be placed in the
            last entry of the bitmap cache specified by the cacheId field.
