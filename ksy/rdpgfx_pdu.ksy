meta:
  id: rdpgfx_pdu
  endian: le
  imports:
    - ts_monitor_layout_pdu
    - nscodec_bitmap_stream
    - ts_rfx_bitmap_stream
    - clearcodec_bitmap_stream
    - rfx_progressive_bitmap_stream
    - rdp6_bitmap_stream
    - rfx_avc420_bitmap_stream
    - alphacodec_bitmap_stream
    - rfx_avc444_bitmap_stream
    - rfx_avc444v2_bitmap_stream

doc: |
  MS-RDPEGFX 2.2.1.5 RDPGFX_HEADER
  Dynamic virtual channel 'Microsoft::Windows::RDS::Graphics'
  The RDPGFX_HEADER structure is included in all graphics command PDUs and specifies the
  graphics command type, the transport flags, and the length of the PDU.
  mstscax!RdpGfxClientChannel::OnDataReceived
  mstscax!RdpGfxProtocolClientDecoder::Decode

seq:
  - id: cmd_id
    type: u2
    enum: rdpgfx_cmdid
    doc: |
      A 16-bit unsigned integer that identifies the type of the graphics command PDU.
  - id: flags
    type: u2
    doc: |
      A 16-bit unsigned integer that contains graphics command flags common to all PDUs.
      No common graphics command flags are specified; therefore, this field MUST be set
      to zero.
  - id: pdu_length
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the length of the graphics command PDU,
      in bytes. This value MUST include the length of the RDPGFX_HEADER (8 bytes).
  - id: data
    size: data_size
    type:
      switch-on: cmd_id
      -name: type
      cases:
        'rdpgfx_cmdid::wiretosurface_1': rdpgfx_wire_to_surface_pdu_1
        'rdpgfx_cmdid::wiretosurface_2': rdpgfx_wire_to_surface_pdu_2
        'rdpgfx_cmdid::deleteencodingcontext': rdpgfx_delete_encoding_context_pdu
        'rdpgfx_cmdid::solidfill': rdpgfx_solidfill_pdu
        'rdpgfx_cmdid::surfacetosurface': rdpgfx_surface_to_surface_pdu
        'rdpgfx_cmdid::surfacetocache': rdpgfx_surface_to_cache_pdu
        'rdpgfx_cmdid::cachetosurface': rdpgfx_cache_to_surface_pdu
        'rdpgfx_cmdid::evictcacheentry': rdpgfx_evict_cache_entry_pdu
        'rdpgfx_cmdid::createsurface': rdpgfx_create_surface_pdu
        'rdpgfx_cmdid::deletesurface': rdpgfx_delete_surface_pdu
        'rdpgfx_cmdid::startframe': rdpgfx_start_frame_pdu
        'rdpgfx_cmdid::endframe': rdpgfx_end_frame_pdu
        'rdpgfx_cmdid::frameacknowledge': rdpgfx_frame_acknowledge_pdu
        'rdpgfx_cmdid::resetgraphics': rdpgfx_reset_graphics_pdu
        'rdpgfx_cmdid::mapsurfacetooutput': rdpgfx_map_surface_to_output_pdu
        'rdpgfx_cmdid::cacheimportoffer': rdpgfx_cache_import_offer_pdu
        'rdpgfx_cmdid::cacheimportreply': rdpgfx_cache_import_reply_pdu
        'rdpgfx_cmdid::capsadvertise': rdpgfx_caps_advertise_pdu
        'rdpgfx_cmdid::capsconfirm': rdp_caps_confirm_pdu
        'rdpgfx_cmdid::diagnostic': rdpgfx_diagnostic_pdu
        'rdpgfx_cmdid::mapsurfacetowindow': rdpgfx_map_surface_to_window_pdu
        'rdpgfx_cmdid::qoeframeacknownledge': rdpgfx_qoe_frame_acknowledge_pdu
        'rdpgfx_cmdid::mapsurfacetoscaledoutput': rdpgfx_map_surface_to_scaled_output_pdu
        'rdpgfx_cmdid::mapsurfacetoscaledwindow': rdpgfx_map_surface_to_scaled_window_pdu

instances:
  data_size:
    value: 'pdu_length >= 8 ? pdu_length - 8 : 0'

enums:
  rdpgfx_cmdid:
    0x0001:
      id: wiretosurface_1
      doc: RDPGFX_WIRE_TO_SURFACE_PDU_1 (section 2.2.2.1)
    0x0002:
      id: wiretosurface_2
      doc: RDPGFX_WIRE_TO_SURFACE_PDU_2 (section 2.2.2.2)
    0x0003:
      id: deleteencodingcontext
      doc: RDPGFX_DELETE_ENCODING_CONTEXT_PDU (section 2.2.2.3)
    0x0004:
      id: solidfill
      doc: RDPGFX_SOLIDFILL_PDU (section 2.2.2.4)
    0x0005:
      id: surfacetosurface
      doc: RDPGFX_SURFACE_TO_SURFACE_PDU (section 2.2.2.5)
    0x0006:
      id: surfacetocache
      doc: RDPGFX_SURFACE_TO_CACHE_PDU (section 2.2.2.6)
    0x0007:
      id: cachetosurface
      doc: RDPGFX_CACHE_TO_SURFACE_PDU (section 2.2.2.7)
    0x0008:
      id: evictcacheentry
      doc: RDPGFX_EVICT_CACHE_ENTRY_PDU (section 2.2.2.8)
    0x0009:
      id: createsurface
      doc: RDPGFX_CREATE_SURFACE_PDU (section 2.2.2.9)
    0x000A:
      id: deletesurface
      doc: RDPGFX_DELETE_SURFACE_PDU (section 2.2.2.10)
    0x000B:
      id: startframe
      doc: RDPGFX_START_FRAME_PDU (section 2.2.2.11)
    0x000C:
      id: endframe
      doc: RDPGFX_END_FRAME_PDU (section 2.2.2.12)
    0x000D:
      id: frameacknowledge
      doc: RDPGFX_FRAME_ACKNOWLEDGE_PDU (section 2.2.2.13)
    0x000E:
      id: resetgraphics
      doc: RDPGFX_RESET_GRAPHICS_PDU (section 2.2.2.14)
    0x000F:
      id: mapsurfacetooutput
      doc: RDPGFX_MAP_SURFACE_TO_OUTPUT_PDU (section 2.2.2.15)
    0x0010:
      id: cacheimportoffer
      doc: RDPGFX_CACHE_IMPORT_OFFER_PDU (section 2.2.2.16)
    0x0011:
      id: cacheimportreply
      doc: RDPGFX_CACHE_IMPORT_REPLY_PDU (section 2.2.2.17)
    0x0012:
      id: capsadvertise
      doc: RDPGFX_CAPS_ADVERTISE_PDU (section 2.2.2.18)
    0x0013:
      id: capsconfirm
      doc: RDP_CAPS_CONFIRM_PDU (section 2.2.2.19)
    0x0014:
      id: diagnostic
      doc: RDPGFX_DIAGNOSTIC_PDU (undocumented)
    0x0015:
      id: mapsurfacetowindow
      doc: RDPGFX_MAP_SURFACE_TO_WINDOW_PDU (section 2.2.2.20)
    0x0016:
      id: qoeframeacknownledge
      doc: RDPGFX_QOE_FRAME_ACKNOWLEDGE_PDU (section 2.2.2.21)
    0x0017:
      id: mapsurfacetoscaledoutput
      doc: RDPGFX_MAP_SURFACE_TO_SCALED_OUTPUT_PDU (section 2.2.2.22)
    0x0018:
      id: mapsurfacetoscaledwindow
      doc: RDPGFX_MAP_SURFACE_TO_SCALED_WINDOW_PDU (section 2.2.2.23)

  rdpgfx_codecid:
    0x0000:
      id: uncompressed
      doc: |
        The bitmap data encapsulated in the bitmapData field is uncompressed. Pixels in the
        uncompressed data are ordered from left to right and then top to bottom.
    0x0001:
      id: nscodec
      doc: |
        MS-RDPNSC Remote Desktop Protocol: NSCodec Extension
    0x0003:
      id: cavideo
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the RemoteFX
        Codec ([MS-RDPRFX] sections 2.2.1 and 3.1.8). Note that the TS_RFX_RECT ([MS-RDPRFX]
        section 2.2.2.1.6) structures encapsulated in the bitmapData field MUST all be relative
        to the top-left corner of the rectangle defined by the destRect field.
    0x0005:
       id: cacodec
       doc: MS-RDPRFX RemoteFX Codec
    0x0008:
      id: clearcodec
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the ClearCodec
        Codec (sections 2.2.4.1 and 3.3.8.1).
    0x0009:
      id: caprogressive
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the RemoteFX
        Progressive Codec (sections 2.2.4.2, 3.1.8.1, 3.2.8.1, and 3.3.8.2).
    0x000A:
      id: planar
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the Planar Codec
        ([MS-RDPEGDI] sections 2.2.2.5.1 and 3.1.9).
    0x000B:
      id: avc420
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the MPEG-4
        AVC/H.264 Codec in YUV420p mode (section 2.2.4.4).
    0x000C:
      id: alpha
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the Alpha Codec
        (section 2.2.4.3).
    0x000E:
      id: avc444
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the MPEG-4
        AVC/H.264 Codec in YUV444 mode (section 2.2.4.5).
    0x000F:
      id: avc444v2
      doc: |
        The bitmap data encapsulated in the bitmapData field is compressed using the MPEG-4
        AVC/H.264 Codec in YUV444v2 mode (section 2.2.4.6).

types:
  rdpgfx_pixelformat:
    doc: |
      MS-RDPEGFX 2.2.1.4 RDPGFX_PIXELFORMAT
      The RDPGFX_PIXELFORMAT structure specifies the color component layout in a pixel.
    seq:
      - id: format
        type: u1
        enum: pixel_format
        doc: |
          An 8-bit unsigned integer that specifies the pixel format.
    enums:
      pixel_format:
        0x20:
          id: xrgb_8888
          doc: 32bpp with no valid alpha (XRGB).
        0x21:
          id: argb_8888
          doc: 32bpp with valid alpha (ARGB).

  rdpgfx_rect16:
    doc: |
      MS-RDPEGFX 2.2.1.2 RDPGFX_RECT16
      The RDPGFX_RECT16 structure specifies a rectangle relative to the origin of a target
      surface using exclusive coordinates (the right and bottom bounds are not included in
      the rectangle).
    seq:
      - id: left
        type: u2
        doc: A 16-bit unsigned integer that specifies the leftmost bound of the rectangle.
      - id: top
        type: u2
        doc: A 16-bit unsigned integer that specifies the upper bound of the rectangle.
      - id: right
        type: u2
        doc: A 16-bit unsigned integer that specifies the rightmost bound of the rectangle.
      - id: bottom
        type: u2
        doc: A 16-bit unsigned integer that specifies the lower bound of the rectangle.

  rdpgfx_point16:
    doc: |
      MS-RDPEGFX 2.2.1.1 RDPGFX_POINT16
      The RDPGFX_POINT16 structure specifies a point relative to the origin of a target surface.
    seq:
      - id: x
        type: s2
        doc: |
          A 16-bit signed integer that specifies the x-coordinate of the point.
      - id: y
        type: s2
        doc: |
          A 16-bit signed integer that specifies the y-coordinate of the point.

  rdpgfx_color32:
    doc: |
      MS-RDPEGFX 2.2.1.3 RDPGFX_COLOR32
      The RDPGFX_COLOR32 structure specifies a 32bpp ARGB or XRGB color value.
    seq:
      - id: b
        type: u1
        doc: An 8-bit unsigned integer that specifies the blue ARGB or XRGB color component.
      - id: g
        type: u1
        doc: An 8-bit unsigned integer that specifies the green ARGB or XRGB color component.
      - id: r
        type: u1
        doc: An 8-bit unsigned integer that specifies the red ARGB or XRGB color component.
      - id: xa
        type: u1
        doc: |
          An 8-bit unsigned integer that in the case of ARGB specifies the alpha color component
          or in the case of XRGB MUST be ignored.

  rdpgfx_capset:
    doc: |
      MS-RDPEGFX 2.2.1.6 RDPGFX_CAPSET
      The RDPGFX_CAPSET structure specifies the layout of a capability set sent in the
      RDPGFX_CAPS_ADVERTISE_PDU (section 2.2.2.18) message. All of the capability sets
      specified in section 2.2.3 conform to this basic structure.
    seq:
      - id: version
        type: u4
        enum: rdpgfx_cap
        doc: |
          A 32-bit unsigned integer that specifies the version of the capability set.
          The format of the data in the capsData field and the length specified in the
          capsDataLength field are both determined by the version of the capability set.
      - id: caps_data_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the size, in bytes, of the capability
          set data present in the capsData field.
      - id: caps_data
        size: caps_data_length
        doc: |
          A variable-length array of bytes that contains data specific to the capability
          set. The number of bytes in this array is specified by the capsDataLength field.
    enums:
      rdpgfx_cap:
        0x00080004:
          id: version_8
          doc: RDPGFX_CAPSET_VERSION8 (section 2.2.3.1)
        0x00080105:
          id: version_81
          doc: RDPGFX_CAPSET_VERSION81 (section 2.2.3.2)
        0x000A0002:
          id: version_10
          doc: RDPGFX_CAPSET_VERSION10 (section 2.2.3.3)
        0x000A0100:
          id: version_101
          doc: RDPGFX_CAPSET_VERSION101 (section 2.2.3.4)
        0x000A0200:
          id: version_102
          doc: RDPGFX_CAPSET_VERSION102 (section 2.2.3.5)
        0x000A0301:
          id: version_103
          doc: RDPGFX_CAPSET_VERSION103 (section 2.2.3.6)
        0x000A0400:
          id: version_104
          doc: RDPGFX_CAPSET_VERSION104 (section 2.2.3.7)
        0x000A0502:
          id: version_105
          doc: RDPGFX_CAPSET_VERSION105 (section 2.2.3.8)
        0x000A0600:
          id: version_106
          doc: RDPGFX_CAPSET_VERSION106 (section 2.2.3.9)

  rdpgfx_cache_entry_metadata:
    doc: |
      MS-RDPEGFX 2.2.2.16.1 RDPGFX_CACHE_ENTRY_METADATA
      The RDPGFX_CACHE_ENTRY_METADATA structure specifies attributes of a bitmap cache entry stored
      on the client.
    seq:
      - id: cache_key
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies a unique key associated with the bitmap cache
          entry.
      - id: bitmap_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the size of the bitmap cache entry, in bytes.

  rdpgfx_raw_bitmap_stream:
    doc: |
      The contents of RDPGFX_WIRE_TO_SURFACE_PDU_1 is an uncompressed bitmap (0x0000).
    seq:
      - id: data
        size-eos: true
        doc: |
          Raw bitmap data.

  rdpgfx_wire_to_surface_pdu_1:
    doc: |
      MS-RDPEGFX 2.2.2.1 RDPGFX_WIRE_TO_SURFACE_PDU_1
      The RDPGFX_WIRE_TO_SURFACE_PDU_1 message is used to transfer encoded bitmap data from the
      server to a client-side destination surface.
      mstscax!RdpGfxProtocolClientDecoder::DecodeWireToSurface1
      mstscax!OffscreenSurface::DecodeBytesToSurface
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the destination surface.
      - id: codec_id
        type: u2
        enum: rdpgfx_codecid
        doc: |
          A 16-bit unsigned integer that specifies the codec that was used to encode the bitmap data
          encapsulated in the bitmapData field.
      - id: pixel_format
        type: rdpgfx_pixelformat
        doc: |
          An RDPGFX_PIXELFORMAT (section 2.2.1.4) structure that specifies the pixel format of the
          decoded bitmap data encapsulated in the bitmapData field.
      - id: dest_rect
        type: rdpgfx_rect16
        doc: |
          An RDPGFX_RECT16 (section 2.2.1.2) structure that specifies the target point on the destination
          surface to which to copy the decoded bitmap and the dimensions (width and height) of the bitmap
          data encapsulated in the bitmapData field. This field specifies a bounding rectangle if the
          codecId field contains:
           - RDPGFX_CODECID_AVC420 (0x000B)
           - RDPGFX_CODECID_AVC444 (0x000E)
           - RDPGFX_CODECID_AVC444V2 (0x000F)
      - id: bitmap_data_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the length, in bytes, of the bitmapData field.
      - id: bitmap_data
        size: bitmap_data_length
        doc: |
          A variable-length array of bytes containing bitmap data encoded using the codec identified by
          the ID in the codecId field.
        type:
          switch-on: codec_id
          -name: enum
          cases:
            'rdpgfx_codecid::uncompressed': rdpgfx_raw_bitmap_stream
            'rdpgfx_codecid::nscodec': nscodec_bitmap_stream
            'rdpgfx_codecid::cavideo': ts_rfx_bitmap_stream
            'rdpgfx_codecid::cacodec': ts_rfx_bitmap_stream
            'rdpgfx_codecid::clearcodec': clearcodec_bitmap_stream
            'rdpgfx_codecid::planar': rdp6_bitmap_stream
            'rdpgfx_codecid::avc420': rfx_avc420_bitmap_stream
            'rdpgfx_codecid::alpha': alphacodec_bitmap_stream
            'rdpgfx_codecid::avc444': rfx_avc444_bitmap_stream
            'rdpgfx_codecid::avc444v2': rfx_avc444v2_bitmap_stream

  rdpgfx_wire_to_surface_pdu_2:
    doc: |
      MS-RDPEGFX 2.2.2.2 RDPGFX_WIRE_TO_SURFACE_PDU_2
      The RDPGFX_WIRE_TO_SURFACE_PDU_2 message is used to transfer encoded bitmap data progressively from the
      server to a client-side destination surface by leveraging a compression context that persists on the
      server and the client until the transfer of the bitmap data is complete.
      Only RDPGFX_CODECID_CAPROGRESSIVE (0x0009) is supported - see RemoteFX Progressive Codec
      (sections 2.2.4.2, 3.1.8.1, 3.2.8.1, and 3.3.8.2).
      mstscax!RdpGfxProtocolClientDecoder::DecodeWireToSurface2
      mstscax!OffscreenSurface::DecodeProgressiveBytesToSurface
      mstscax!RdpSurfaceDecoder::DecodeProgressiveBytesToSurface
      mstscax!RdpSurfaceDecoder::GetDecoderContext
      mstscax!RdpSurfaceDecoder::CreateDecoderContext
      mstscax!CaProgressiveDecompressor::CreateDecodingSurfaceContext
      mstscax!CaProgressiveDecompressor::initializeEngine
      mstscax!CacNx::CreateDecoderFactoryInstance
      mstscax!CacNx::DecoderFactory::DecoderFactory
      mstscax!CacNx::DecoderFactory::CreateDecodingEngineCpu
      mstscax!CacNx::DecodingEngineCpu::DecodingEngineCpu
      mstscax!CacNx::DecodingEngineCpu::Init
      mstscax!CaDecProgressiveSurfaceContext::CaDecProgressiveSurfaceContext
      mstscax!CaDecProgressiveSurfaceContext::Initialize
      mstscax!CacNx::DecodingEngineCpu::CreateSurfaceDecoder
      mstscax!CacNx::SurfaceDecoderCpu::SurfaceDecoderCpu
      mstscax!CacNx::SurfaceDecoderCpu::Init
      mstscax!CacNx::SurfaceDecoder::init
      mstscax!CacNx::SurfaceDecoder::AllocatePersistentResources
      mstscax!CacNx::DecodingEngine::RegisterDecoder
      mstscax!CacNx::TileMap::Init
      mstscax!CaProgressiveDecompressor::CreateDecodingContext
      mstscax!CacNx::SurfaceDecoderCpu::CreateDecodingContext
      mstscax!CacNx::SurfaceDecoder::CreateDecodingContext
      mstscax!CacNx::EmptyShellDecodeContext::EmptyShellDecodeContext
      mstscax!CaDecProgressiveRectContext::CaDecProgressiveRectContext
      mstscax!CaProgressiveDecompressor::Decode
      mstscax!CaDecProgressiveRectContext::Decode
      mstscax!CacNx::SurfaceDecoderCpu::Decode
      mstscax!CacNx::SurfaceDecoder::Decode
      mstscax!CacNx::DecodingEngine::Decode
      mstscax!CacNx::DecoderImpl::processStream
      mstscax!CaDecProgressiveRectContext::SetRects
      mstscax!CaProgressiveDecompressor::getDecodedBits
      mstscax!PixelMap::Attach
      mstscax!OffscreenSurface::UpdateSurfaceFromBits
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the destination surface.
      - id: codec_id
        type: u2
        enum: rdpgfx_codecid
        doc: |
          A 16-bit unsigned integer that specifies the codec that was used to encode the bitmap data
          encapsulated in the bitmapData field.
          Only RDPGFX_CODECID_CAPROGRESSIVE (0x0009) is supported - see RemoteFX Progressive Codec
          (sections 2.2.4.2, 3.1.8.1, 3.2.8.1, and 3.3.8.2).
      - id: codec_context_id
        type: u4
        doc: |
          A 32-bit unsigned integer that identifies the compression context associated with the bitmap data
          encapsulated in the bitmapData field.
      - id: pixel_format
        type: rdpgfx_pixelformat
        doc: |
          An RDPGFX_PIXELFORMAT (section 2.2.1.4) structure that specifies the pixel format of the decoded
          bitmap data encapsulated in the bitmapData field.
      - id: bitmap_data_length
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the length, in bytes, of the bitmapData field.
      - id: bitmap_data
        size: bitmap_data_length
        type: rfx_progressive_bitmap_stream
        doc: |
          A variable-length array of bytes containing bitmap data encoded using the codec identified by the
          ID in the codecId field.
          Only RDPGFX_CODECID_CAPROGRESSIVE (0x0009) is supported - see RemoteFX Progressive Codec
          (sections 2.2.4.2, 3.1.8.1, 3.2.8.1, and 3.3.8.2).

  rdpgfx_delete_encoding_context_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.3 RDPGFX_DELETE_ENCODING_CONTEXT_PDU
      The RDPGFX_DELETE_ENCODING_CONTEXT_PDU message is sent by the server to instruct the
      client to delete a compression context that was used by a collection of
      RDPGFX_WIRE_TO_SURFACE_PDU_2 (section 2.2.2.2) messages to progressively transfer
      bitmap data.
      mstscax!RdpGfxProtocolClientDecoder::DecodeDeleteEncodingContext
      mstscax!RdpGfxProtocolClientDecoder::DeleteDecoderContext
      mstscax!RdpGfxProtocolClientDecoder::GetOffscreenSurface
      mstscax!OffscreenSurface::GetSurfaceDecoder
      mstscax!RdpSurfaceDecoder::DeleteDecodingContext
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface associated with the
          compression context ID specified in the codecContextId field.
      - id: codec_context_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the ID of the compression context to delete.

  rdpgfx_solidfill_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.4 RDPGFX_SOLIDFILL_PDU
      The RDPGFX_SOLIDFILL_PDU message is used to instruct the client to fill a collection of
      rectangles on a destination surface with a solid color.
      mstscax!RdpGfxProtocolClientDecoder::DecodeSolidFill
      mstscax!OffscreenSurface::Fill
      mstscax!RdpXByteArrayTexture2D::Fill
      mstscax!PixelMap::AttachInternal
      mstscax!PixelMap::RectIsContained
      mstscax!PixelMap::RectIsValid
      mstscax!PixelMap::Fill
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the destination surface.
      - id: fill_pixel
        type: rdpgfx_color32
        doc: |
          An RDPGFX_COLOR32 (section 2.2.1.3) structure that specifies the color that MUST be used to
          fill the destination rectangles specified in the fillRects field.
      - id: fill_rect_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_RECT16 (section 2.2.1.2) structures
          in the fillRects field.
      - id: fill_rects
        type: rdpgfx_rect16
        repeat: expr
        repeat-expr: fill_rect_count
        doc: |
          A variable-length array of RDPGFX_RECT16 structures that specifies rectangles on the destination
          surface to be filled. The number of structures in this array is specified by the fillRectCount
          field.

  rdpgfx_surface_to_surface_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.5 RDPGFX_SURFACE_TO_SURFACE_PDU
      The RDPGFX_SURFACE_TO_SURFACE_PDU message is used to instruct the client to copy bitmap data from a
      source surface to a destination surface or to replicate bitmap data within the same surface.
      mstscax!RdpGfxProtocolClientDecoder::DecodeSurfaceToSurface
      mstscax!OffscreenSurface::UpdateSurface
      mstscax!RdpXByteArrayTexture2D::CopyRect
      mstscax!RdpXByteArrayTexture2D::SetSurfacePixels
      mstscax!PixelMap::Attach
      mstscax!PixelMap::AttachInternal
      mstscax!PixelMap::BitBltPreserveAlpha
    seq:
      - id: surface_id_src
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface containing the source bitmap.
      - id: surface_id_dst
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the destination surface.
      - id: rect_src
        type: rdpgfx_rect16
        doc: |
          An RDPGFX_RECT16 (section 2.2.1.2) structure that specifies the rectangle that bounds the source bitmap.
      - id: dest_pts_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_POINT16 (section 2.2.1.1) structures in the
          destPts field.
      - id: dest_pts
        type: rdpgfx_point16
        repeat: expr
        repeat-expr: dest_pts_count
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_POINT16 (section 2.2.1.1) structures in the
          destPts field.

  rdpgfx_surface_to_cache_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.6 RDPGFX_SURFACE_TO_CACHE_PDU
      The RDPGFX_SURFACE_TO_CACHE_PDU message is used to instruct the client to copy bitmap
      data from a source surface to the bitmap cache.
      mstscax!RdpGfxProtocolClientDecoder::DecodeSurfaceToCache
      mstscax!RdpCacheDatabase::SurfaceToCache
      mstscax!RdpGfxClientChannel::CreateTexture2D
          mstscax!RdpXDelegatingOutput::CreateCompatibleTexture
          mstscax!RdpWinDesktopRemoteAppUIManager::CreateByteArrayTexture
          mstscax!RdpXByteArrayTexture2D::CreateInstance
      mstscax!RdpCacheDatabase::BitmapDataGPUToCPU
      mstscax!RdpCacheDatabase::BitmapDataCPUToGPU
      mstscax!RdpCacheDatabase::EvictCacheEntry
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface containing the source
          bitmap.
      - id: cache_key
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies a key to associate with the bitmap cache
          entry that will store the bitmap.
      - id: cache_slot
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the index of the bitmap cache entry in which
          the source bitmap data MUST be stored. The value of this field is constrained as
          specified in section 3.3.1.4.
      - id: rect_src
        type: rdpgfx_rect16
        doc: |
          An RDPGFX_RECT16 (section 2.2.1.2) structure that specifies the rectangle that bounds
          the source bitmap.

  rdpgfx_cache_to_surface_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.7 RDPGFX_CACHE_TO_SURFACE_PDU
      The RDPGFX_CACHE_TO_SURFACE_PDU message is used to instruct the client to copy bitmap data
      from the bitmap cache to a destination surface.
      mstscax!RdpGfxProtocolClientDecoder::DecodeCacheToSurface
      mstscax!RdpCacheDatabase::CacheToSurface
    seq:
      - id: cache_slot
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the index of the bitmap cache entry that
          contains the source bitmap. The value of this field is constrained as specified in
          section 3.3.1.4.
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the destination surface.
      - id: dest_pts_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_POINT16 (section 2.2.1.1)
          structures in the destPts field.
      - id: dest_pts
        type: rdpgfx_point16
        repeat: expr
        repeat-expr: dest_pts_count
        doc: |
          A variable-length array of RDPGFX_POINT16 structures that specifies target points on the
          destination surface to which to copy the source bitmap. The number of structures in this
          array is specified by the destPtsCount field.

  rdpgfx_evict_cache_entry_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.8 RDPGFX_EVICT_CACHE_ENTRY_PDU
      The RDPGFX_EVICT_CACHE_ENTRY_PDU message is used to instruct the client to delete an entry from
      the bitmap cache.
      mstscax!RdpGfxProtocolClientDecoder::DecodeEvictCacheEntry
      mstscax!RdpCacheDatabase::EvictCacheEntry
    seq:
      - id: cache_slot
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the index of the bitmap cache entry to delete from
          the bitmap cache. The value of this field is constrained as specified in section 3.3.1.4.

  rdpgfx_create_surface_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.9 RDPGFX_CREATE_SURFACE_PDU
      The RDPGFX_CREATE_SURFACE_PDU message is used to instruct the client to create a
      surface of a given width, height, and pixel format.
      mstscax!RdpGfxProtocolClientDecoder::DecodeCreateSurface
      mstscax!OffscreenSurface::CreateInstance
      mstscax!RdpGfxClientChannel::CreateTexture2D
      mstscax!RdpXDelegatingOutput::CreateCompatibleTexture
      mstscax!RdpWinDesktopRemoteAppUIManager::CreateByteArrayTexture
      mstscax!RdpXByteArrayTexture2D::CreateInstance
      mstscax!RdpSurfaceDecoderFactory::CreateSurfaceDecoder
      mstscax!RdpSurfaceDecoder::CreateInstance
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID that MUST be assigned to the
          surface once it has been created.
      - id: width
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the width of the surface to create.
      - id: height
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the height of the surface to create.
      - id: pixel_format
        type: rdpgfx_pixelformat
        doc: |
          An RDPGFX_PIXELFORMAT (section 2.2.1.4) structure that specifies the pixel
          format of the surface to create.

  rdpgfx_delete_surface_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.10 RDPGFX_DELETE_SURFACE_PDU
      The RDPGFX_DELETE_SURFACE_PDU message is used to instruct the client to delete a surface.
      mstscax!RdpGfxProtocolClientDecoder::DecodeDeleteSurface
      mstscax!RdpGfxProtocolClientDecoder::DeleteSurface
      mstscax!RdpGfxProtocolClientDecoder::GetOutputCompositeSurface
      mstscax!CRdpCompositePrimarySurface::RemoveSurface
      mstscax!RdpGfxProtocolClientDecoder::RemoveOutputCompositeSurface
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface to delete.

  rdpgfx_start_frame_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.11 RDPGFX_START_FRAME_PDU
      The RDPGFX_START_FRAME_PDU message is sent by the server to specify the start of a
      logical frame, enabling related graphics commands to be grouped together.
      mstscax!RdpGfxProtocolClientDecoder::DecodeStartFrame
    seq:
      - id: timestamp
        type: u4
        doc: |
          A 32-bit unsigned integer that contains a UTC timestamp assigned to the frame. If no
          timestamp is available, this field MUST be set to zero.
      - id: frame_id
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies a unique ID assigned to the frame.
    instances:
      milliseconds:
        value: (timestamp >> 0) & 0x3ff
        doc: |
          A 10-bit, unsigned integer that contains the millisecond value of the timestamp. This
          field MUST be greater than or equal to 0, and less than or equal to 999.
      seconds:
        value: (timestamp >> 10) & 0x3f
        doc: |
          A 6-bit, unsigned integer that contains the second value of the timestamp. This field
          MUST be greater than or equal to 0, and less than or equal to 59.
      minutes:
        value: (timestamp >> 16) & 0x3f
        doc: |
          A 6-bit, unsigned integer that contains the minute value of the timestamp. This field
          MUST be greater than or equal to 0, and less than or equal to 59.
      hours:
        value: (timestamp >> 22) & 0x3ff
        doc: |
          A 10-bit, unsigned integer that contains the hour value of the timestamp. This field
          MUST be greater than or equal to 0, and less than or equal to 23.

  rdpgfx_end_frame_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.12 RDPGFX_END_FRAME_PDU
      The RDPGFX_END_FRAME_PDU message is sent by the server to specify the end of a logical frame.
      mstscax!RdpGfxProtocolClientDecoder::DecodeEndFrame
    seq:
      - id: frame_id
        type: u4
        doc: |
          A 32-bit unsigned integer that contains the ID assigned to the frame in the
          RDPGFX_START_FRAME_PDU (section 2.2.2.11) message.

  rdpgfx_frame_acknowledge_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.13 RDPGFX_FRAME_ACKNOWLEDGE_PDU
      The RDPGFX_FRAME_ACKNOWLEDGE_PDU message is sent by the client to indicate to the server that
      a logical frame of graphics commands has been successfully decoded. This message MUST be sent
      in response to an RDPGFX_END_FRAME_PDU (section 2.2.2.12) message, unless the client has opted
      out of this behavior.
      mstscax!RdpGfxProtocolClientEncoder::FrameAcknowledge
    seq:
      - id: queue_depth
        type: u4
        doc: |
          A 32-bit unsigned integer that either specifies the number of unprocessed bytes buffered at
          the client, or indicates to the server that the client will no longer be transmitting
          RDPGFX_FRAME_ACKNOWLEDGE_PDU messages:
            - QUEUE_DEPTH_UNAVAILABLE (0x00000000)
              Specifies that no information is available regarding the size, in bytes, of the graphics
              messages that have been buffered at the client and not yet processed.
            - 0x00000001 – 0xFFFFFFFE
              Specifies the size, in bytes, of the graphics messages that have been buffered at the
              client and not yet processed.
            - SUSPEND_FRAME_ACKNOWLEDGEMENT (0xFFFFFFFF)
              Indicates to the server that the client will no longer be transmitting
              RDPGFX_FRAME_ACKNOWLEDGE_PDU messages. The client can opt back into sending these messages
              by sending an RDPGFX_FRAME_ACKNOWLEDGE_PDU message with the queueDepth field set to a value
              in the range 0x00000000 to 0xFFFFFFFE (inclusive) in response to an RDPGFX_END_FRAME_PDU
              message.
      - id: frame_id
        type: u4
        doc: |
          A 32-bit unsigned integer that contains the ID of the frame being acknowledged. The ID of the
          frame is specified in the RDPGFX_START_FRAME_PDU (section 2.2.2.11) and RDPGFX_END_FRAME_PDU
          (section 2.2.2.12) messages.
      - id: total_frames_decoded
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the number of frames that have been decoded by the
          client since the connection was initiated.

  rdpgfx_reset_graphics_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.14 RDPGFX_RESET_GRAPHICS_PDU
      The RDPGFX_RESET_GRAPHICS_PDU message is sent by the server to instruct the client to change the
      width and height of the Graphics Output Buffer (section 3.3.1.7) ADM element, and to update the
      monitor layout. Note that this message MUST be 340 bytes in size.
      mstscax!RdpGfxProtocolClientDecoder::DecodeResetGraphics
      mstscax!RdpGfxClientChannel::OnResetGraphics
      mstscax!RdpGfxClientChannel::VizualiserOnResetGraphics
      mstscax!RdpVizualizer::OnResetGraphics
      mstscax!RdpGfxClientChannel::ResetResources
      mstscax!RdpXDelegatingUIManager::CreateOutput
      mstscax!RdpWinLegacyUIManager::CreateOutput
      mstscax!RdpWinLegacyOutput::CreateInstance
      mstscax!RdpWinLegacyOutput::RdpWinLegacyOutput
      mstscax!RdpWinLegacyOutput::Initialize
      mstscax!RdpWinLegacyOutput::CreateShadowTexture
      mstscax!RdpWinGdiTexture2D::CreateInstance
      mstscax!RdpWinGdiTexture2D::CreateGdiBitmap
      mstscax!RdpXByteArrayTexture2D::SetupBuffer
      mstscax!RdpXDelegatingOutput::CreateInstance
      mstscax!RdpXDelegatingOutput::RdpXDelegatingOutput
      mstscax!RdpXDelegatingOutput::Initialize
      mstscax!RdpGfxClientChannel::SetMonitorLayout
    seq:
      - id: width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the new width of the Graphics Output Buffer ADM element
          (the maximum allowed width is 32766 pixels).
      - id: height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the new height of the Graphics Output Buffer ADM element
          (the maximum allowed height is 32766 pixels).
      - id: monitor_def_array
        type: ts_monitor_layout_pdu
        doc: |
          A variable-length array containing a series of TS_MONITOR_LAYOUT_PDU ([MS-RDPBCGR] section 2.2.12.1)
          structures that specify the display monitor layout of the session on the remote server.
      - id: pad
        size: 'monitor_def_array.monitor_count <= 16 ? (16 - monitor_def_array.monitor_count) * 20 : 0'
        doc: |
          A variable-length byte array that is used for padding. The number of bytes in this array is calculated
          by subtracting the combined size of the header, width, height, monitorCount, and monitorDefArray fields
          from the total size of the PDU (which is specified by the pduLength field embedded in the header field).
          The contents of the pad field MUST be ignored.
          ALTERNATIVE:
            = (340 - sizeof(header) - sizeof(width) - sizeof(height) - sizeof(monitor_count)) - (monitor_count * sizeof(monitor_def))
            = (340 - 8 - 4 - 4 - 4) - (monitor_count * 20)
            = (332 - 12) - (monitor_count * 20)
            = 320 - (monitor_count * 20)
            = (16 - monitor_count) * 20

  rdpgfx_map_surface_to_output_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.15 RDPGFX_MAP_SURFACE_TO_OUTPUT_PDU
      The RDPGFX_MAP_SURFACE_TO_OUTPUT_PDU message is sent by the server to instruct the client to map a surface
      to a rectangular area of the Graphics Output Buffer (section 3.3.1.7) ADM element.
      mstscax!RdpGfxProtocolClientDecoder::DecodeMapSurfaceToOutput
      mstscax!RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface to be associated with the output-to-surface
          mapping.
      - id: reserved
        type: u2
        doc: |
          A 16-bit unsigned integer that is reserved for future use. This field MUST be set to zero.
          If this is set to 0xffff, RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput will be called
          to map surface to outputId -1.
      - id: output_origin_x
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the x-coordinate of the point, relative to the origin of the Graphics
          Output Buffer ADM element, at which to map the top-left corner of the surface.
      - id: output_origin_y
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the y-coordinate of the point, relative to the origin of the Graphics
          Output Buffer ADM element, at which to map the upper-left corner of the surface.

  rdpgfx_cache_import_offer_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.16 RDPGFX_CACHE_IMPORT_OFFER_PDU
      The RDPGFX_CACHE_IMPORT_OFFER_PDU message is sent by the client to inform the server of bitmap data
      that is present in an optional client-side persistent bitmap cache.
      mstscax!RdpGfxProtocolClientEncoder::CacheImportOffer
    seq:
      - id: cache_entries_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_CACHE_ENTRY_METADATA (section 2.2.2.16.1)
          structures in the cacheEntries field. This value MUST be less than 5462 (0x1556).
      - id: cache_entries
        type: rdpgfx_cache_entry_metadata
        repeat: expr
        repeat-expr: cache_entries_count
        doc: |
          A variable-length array of RDPGFX_CACHE_ENTRY_METADATA structures that identifies a collection
          of bitmap cache entries present on the client. The number of structures in this array is specified
          by the cacheEntriesCount field.

  rdpgfx_cache_import_reply_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.17 RDPGFX_CACHE_IMPORT_REPLY_PDU
      The RDPGFX_CACHE_IMPORT_REPLY_PDU message is sent by the server to indicate that
      persistent bitmap cache metadata advertised in the RDPGFX_CACHE_IMPORT_OFFER_PDU
      (section 2.2.2.16) message has been transferred to the bitmap cache.
      mstscax!RdpGfxProtocolClientDecoder::DecodeCacheImportReply
      mstscax!RdpCacheDatabase::CacheImportReply
    seq:
      - id: imported_entries_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of entries that were imported
          into the server-side Bitmap Cache Map (section 3.2.1.1) ADM element from the most
          recent RDPGFX_CACHE_IMPORT_OFFER_PDU (section 2.2.2.16) message. A value of N implies
          that the first N entries were imported into the bitmap cache from the most recent
          RDPGFX_CACHE_IMPORT_OFFER_PDU message.
      - id: cache_slots
        type: u2
        repeat: expr
        repeat-expr: imported_entries_count
        doc: |
          An array of 16-bit unsigned integers. The number of integers in this array is specified
          by the importedEntriesCount field. Each integer in the array identifies the cache slot
          that an imported entry has been assigned. For example, an importedEntriesCount field value
          of 0x0003 and a cacheSlots field that contains the elements [0x0006, 0x0009, 0x0002]
          together imply that the first imported entry was associated with cache slot 6, the second
          imported entry was associated with cache slot 9, and the third imported entry was associated
          with cache slot 2. Each of the cache slot values contained in this field is constrained as
          specified in section 3.3.1.4.

  rdpgfx_caps_advertise_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.18 RDPGFX_CAPS_ADVERTISE_PDU
      The RDPGFX_CAPS_ADVERTISE_PDU message is sent by the client to advertise supported capabilities.
      mstscax!RdpGfxProtocolClientEncoder::CapsAdvertise
      mstscax!CRdpGfxCaps::GetCapsBuffer
    seq:
      - id: caps_set_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RDPGFX_CAPSET (section 2.2.1.6)
          structures in the capsSets field.
      - id: caps_sets
        type: rdpgfx_capset
        repeat: expr
        repeat-expr: caps_set_count
        doc: |
          A variable-length array of RDPGFX_CAPSET structures. The number of elements in this array is
          specified by the capsSetCount field.

  rdp_caps_confirm_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.19 RDPGFX_CAPS_CONFIRM_PDU
      The RDPGFX_CAPS_CONFIRM_PDU message is sent by the server to confirm capabilities for
      the connection.
      mstscax!RdpGfxProtocolClientDecoder::DecodeCapsConfirm
      mstscax!CRdpGfxCaps::CreateInstance
      mstscax!CRdpGfxCaps::InitializeSelf
    seq:
      - id: caps_set
        type: rdpgfx_capset
        doc: |
          A variable-length RDPGFX_CAPSET (section 2.2.1.6) structure that contains the
          capability set selected by the server from the RDPGFX_CAPS_ADVERTISE_PDU
          (section 2.2.2.18) message sent by the client.

  rdpgfx_diagnostic_pdu:
    doc: |
      Undocumented command.
      EtwEventWrite is not reachable as tracing is disabled in a
      default setup.
      mstscax!RdpGfxProtocolClientDecoder::DecodeDiagnosticPDU
      mstscax!CRDPPerfCounterGeneric::Set
      mstscax!CRDPETWLogger::LogGeneric
      ntdll!EtwEventWrite
    seq:
      - id: reserved
        type: u2
        doc: |
          These 2 bytes are skipped over in parsing. Must be zero.
      - id: size
        type: u2
        doc: |
          Size of data that makes up the last entry in an array of
          RDP_GENERIC_COUNTER_ITEM structures.
      - id: data
        size: size
        doc: |
          Data that makes up the last entry in an array of
          RDP_GENERIC_COUNTER_ITEM structures.

  rdpgfx_map_surface_to_window_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.20 RDPGFX_MAP_SURFACE_TO_WINDOW_PDU
      The RDPGFX_MAP_SURFACE_TO_WINDOW_PDU message is sent by the server to instruct the client to map
      a surface to a RAIL window on the client.
      mstscax!RdpGfxProtocolClientDecoder::DecodeMapSurfaceToWindow
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface to be associated with the
          surface-to-window mapping.
      - id: window_id
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies the ID of the RAIL window to be associated with the
          surface-to-window mapping. RAIL windows are created via the New or Existing Window Order
          ([MS-RDPERP] section 2.2.1.3.1.2.1). The WindowId field of the Common Header ([MS-RDPERP]
          section 2.2.1.3.1.1), embedded within the order, contains the window ID.
          windowId is passed to RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput as outputId.
      - id: mapped_width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the width of the rectangular region on the surface
          to which the window is mapped.
      - id: mapped_height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the height of the rectangular region on the surface
          to which the window is mapped.

  rdpgfx_qoe_frame_acknowledge_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.21 RDPGFX_QOE_FRAME_ACKNOWLEDGE_PDU
      The optional RDPGFX_QOE_FRAME_ACKNOWLEDGE_PDU message is sent by the client to enable the
      calculation of Quality of Experience (QoE) metrics. This message is sent solely for informational
      and debugging purposes and MUST NOT be transmitted to the server if the RDPGFX_CAPSET_VERSION10,
      RDPGFX_CAPSET_VERSION102, RDPGFX_CAPSET_VERSION103, RDPGFX_CAPSET_VERSION104,
      RDPGFX_CAPSET_VERSION105, or RDPGFX_CAPSET_VERSION106 structure
      (sections 2.2.3.3, 2.2.3.5, 2.2.3.6, 2.2.3.7, 2.2.3.8, and 2.2.3.9 respectively)
      was not received by the client.
      mstscax!RdpGfxProtocolClientDecoder::DecodeEndFrame
      mstscax!RdpGfxProtocolClientEncoder::QoEFrameAcknowledgeEx
    seq:
      - id: frame_id
        type: u4
        doc: |
          A 32-bit unsigned integer that contains the ID of the frame being annotated. The ID of the frame
          is specified in the RDPGFX_START_FRAME_PDU (section 2.2.2.11) and RDPGFX_END_FRAME_PDU
          (section 2.2.2.12) messages.
      - id: timestamp
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the timestamp (in milliseconds) when the client started
          decoding the RDPGFX_START_FRAME_PDU message. The value of the first timestamp sent by the client
          implicitly defines the origin for all subsequent timestamps. The server is responsible for handling
          roll-over of the timestamp.
      - id: time_diff_se
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the time, in milliseconds, that elapsed between the decoding
          of the RDPGFX_START_FRAME_PDU and RDPGFX_END_FRAME_PDU messages. If the elapsed time is greater than
          65 seconds, then this field SHOULD be set to 0x0000.
      - id: time_diff_edr
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the time, in milliseconds, that elapsed between the decoding
          of the RDPGFX_END_FRAME_PDU message and the completion of the rendering operation for the commands
          contained in the logical graphics frame. If the elapsed time is greater than 65 seconds, then this
          field SHOULD be set to 0x0000.

  rdpgfx_map_surface_to_scaled_output_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.22 RDPGFX_MAP_SURFACE_TO_SCALED_OUTPUT_PDU
      The RDPGFX_MAP_SURFACE_TO_SCALED_OUTPUT_PDU message is sent by the server to instruct the client to map
      a surface to a rectangular area of the Graphics Output Buffer (section 3.3.1.7) ADM element, including
      a target width and height to which the surface MUST be scaled.
      mstscax!RdpGfxProtocolClientDecoder::DecodeMapSurfaceToScaledOutput
      mstscax!RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface to be associated with the
          output-to-surface mapping.
      - id: reserved
        type: u2
        doc: |
          A 16-bit unsigned integer that is reserved for future use. This field MUST be set to zero.
          If this is set to 0xffff, RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput will be called
          to map surface to outputId -1.
      - id: output_origin_x
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the x-coordinate of the point, relative to the origin of
          the Graphics Output Buffer ADM element, at which to map the top-left corner of the surface.
      - id: output_origin_y
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the y-coordinate of the point, relative to the origin of
          the Graphics Output Buffer ADM element, at which to map the upper-left corner of the surface.
      - id: target_width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the width of the target Graphics Output Buffer ADM element
          to which the surface will be mapped, as specified in section 3.3.1.7.
      - id: target_height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the height of the target Graphics Output Buffer ADM element
          to which the surface will be mapped.

  rdpgfx_map_surface_to_scaled_window_pdu:
    doc: |
      MS-RDPEGFX 2.2.2.23 RDPGFX_MAP_SURFACE_TO_SCALED_WINDOW_PDU
      The RDPGFX_MAP_SURFACE_TO_SCALED_WINDOW_PDU message is sent by the server to instruct the client to map
      a surface to a RAIL window on the client, including a target width and height to which the surface should
      be scaled.
      mstscax!RdpGfxProtocolClientDecoder::DecodeMapSurfaceToScaledWindow
      mstscax!RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput
      mstscax!RdpGfxClientChannel::GetWindowOutputMap
      mstscax!RdpXDelegatingUIManager::CreateOutputMap
      mstscax!RdpWinDesktopRemoteAppUIManager::CreateOutputMap
      mstscax!RdpWin32RemoteAppPresenter::CreateDecodingTexture
      mstscax!RdpWin32RemoteAppPresenter::CreateGDIResources
      mstscax!RdpWin32RemoteAppPresenter::DiscardGDIResources
      mstscax!RdpWinGdiTexture2D::CreateInstance
      mstscax!RdpWinGdiTexture2D::CreateGdiBitmap
      mstscax!RdpWinGdiTexture2D::CreateBITMAP
      gdi32!CreateDIBSection
      win32u!NtGdiCreateDIBSection
      mstscax!RdpXByteArrayTexture2D::SetupBuffer
      mstscax!PixelMap::SetupImage
    seq:
      - id: surface_id
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the ID of the surface to be associated with the
          surface-to-window mapping.
      - id: window_id
        type: u8
        doc: |
          A 64-bit unsigned integer that specifies the ID of the RAIL window to be associated with the
          surface-to-window mapping. RAIL windows are created via the New or Existing Window order ([MS-RDPERP]
          section 2.2.1.3.1.2.1). The WindowId field of the Common Header ([MS-RDPERP] section 2.2.1.3.1.1),
          embedded within the order, contains the window ID.
          windowId is passed to RdpGfxProtocolClientDecoder::MapOffscreenSurfaceToOutput as outputId.
      - id: mapped_width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the width of the rectangular region on the surface to which
          the window is mapped.
      - id: mapped_height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the height of the rectangular region on the surface to which
          the window is mapped.
      - id: target_width
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the width of the target graphics output to which the surface
          will be mapped.
      - id: target_height
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the height of the target graphics output to which the surface
          will be mapped.
