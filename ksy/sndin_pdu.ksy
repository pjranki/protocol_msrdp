meta:
  id: sndin_pdu
  endian: le
  imports:
    - audio_format

doc: |
  MS-RDPEAI 2.1 Transport
  Used to capture/record audio on the RDP client.
  NOTE: For WDAG, Camera and Microphone must be enabled, disabled by default.
  Dynamic virtual channel "AUDIO_INPUT"
  mstscax!CSndInputListenerCallback::OnNewChannelConnection
  mstscax!CSndInputChannelCallback::OnDataReceived

seq:
  - id: message_id
    -orig-id: MessageId
    type: u1
    enum: msg_sndin
  - id: message
    type:
      switch-on: message_id
      -name: type
      cases:
        'msg_sndin::version': sndin_version
        'msg_sndin::formats': sndin_formats
        'msg_sndin::open': sndin_open
        'msg_sndin::open_reply': sndin_open_reply
        'msg_sndin::data_incoming': sndin_data_incoming
        'msg_sndin::data': sndin_data
        'msg_sndin::formatchange': sndin_formatchange

enums:
  msg_sndin:
    0x01:
      id: version
      doc: Version PDU
    0x02:
      id: formats
      doc: Sound Formats PDU
    0x03:
      id: open
      doc: Open PDU
    0x04:
      id: open_reply
      doc: Open Reply PDU
    0x05:
      id: data_incoming
      doc: Incoming Data PDU
    0x06:
      id: data
      doc: Data PDU
    0x07:
      id: formatchange
      doc: Format Change PDU

types:
  sndin_version:
    doc: |
      MS-RDPEAI 2.2.2.1 Version PDU (MSG_SNDIN_VERSION)
      The Version PDU is sent by the server and the client to negotiate which
      version of the protocol MUST be used.
      mstscax!CSndInputChannelCallback::OnVersion
    seq:
      - id: version
        -orig-id: Version
        type: u4
        -default: 0x00000001
        doc: |
          A 32-bit unsigned integer. This field specifies the protocol version
          used by the client or server that sent this PDU. This field MUST be
          set to 0x00000001.

  sndin_formats:
    doc: |
      MS-RDPEAI 2.2.2.2 Sound Formats PDU (MSG_SNDIN_FORMATS)
      The Sound Formats PDU is sent by the server and the client to negotiate
      a common list of supported audio formats.
      mstscax!CSndInputChannelCallback::OnFormatsReceived
      mstscax!CAudioMaster::AddFormat
    seq:
      - id: num_formats
        -orig-id: NumFormats
        type: u4
        doc: |
          A 32-bit unsigned integer. This field specifies the number of formats in
          the SoundFormats array.
      - id: size_formats_packet
        -orig-id: cbSizeFormatsPacket
        type: u4
        doc: |
          A 32-bit unsigned integer. This field is reserved in the Sound Formats PDU
          that is sent from server to client. The value is arbitrary in the PDU sent
          from server to client, and MUST be ignored upon receipt on the client side.
          In the PDU sent from client to server, this field MUST be set to the size,
          in bytes, of the entire PDU minus the size, in bytes, of the ExtraData field.
      - id: sound_formats
        -orig-id: SoundFormats
        type: audio_format
        repeat: expr
        repeat-expr: num_formats
        doc: |
          A variable-sized array of audio formats supported by the client or server.
          The number of formats is NumFormats. Each element in the array conforms to
          the AUDIO_FORMAT, as specified in [MS-RDPEA] section 2.2.2.1.1. Any of the
          audio codecs defined in [RFC2361] are allowed by this protocol. Implementations
          MUST, at a minimum, support WAVE_FORMAT_PCM (0x0001). For more information
          about this codec, see [MS-RDPEA] section 2.2.2.1.1.<1>
      - id: extra_data
        -orig-id: ExtraData
        size-eos: true
        doc: |
          An optional field that contains additional data. This data MAY be appended
          to the end of a Sound Formats PDU. The data is arbitrary and MUST be ignored
          by the recipient. The recipient finds out where ExtraData starts by parsing
          the PDU. This will tell the total size of all the data fields in this PDU
          except ExtraData. The size of ExtraData can be calculated by subtracting the
          size of the useful data from the size of the PDU. The MSG_SNDIN_FORMATS
          structure is encapsulated in the Dynamic VC Data First PDU
          ([MS-RDPEDYC] section 2.2.3.1) and Dynamic VC Data PDU
          ([MS-RDPEDYC] section 2.2.3.2). The length of these PDUs is determined as
          described in [MS-RDPEDYC] sections 3.1.5.1 and 3.1.5.2.

  sndin_open:
    doc: |
      MS-RDPEAI 2.2.2.3 Open PDU (MSG_SNDIN_OPEN)
      The Open PDU is sent by the server and indicates that the server requests to start
      recording from the client device. The wFormatTag, nChannels, nSamplesPerSec,
      nAvgBytesPerSec, nBlockAlign, wBitsPerSample, cbSize, and ExtraFormatData fields
      specify the audio format that the client SHOULD use to record from the audio input
      device. The initialFormat field contains an index into the list of audio formats
      agreed to by the client and the server. The initialFormat field specifies the format
      in which audio data MUST be encoded.
      mstscax!CSndInputChannelCallback::OnDataReceived
      mstscax!CSndInputChannelCallback::UpdateWaveInDevice
      mstscax!GetWaveDevice
      mstscax!CSndInputChannelCallback::OnNewFormat
      mstscax!CSndInputChannelCallback::OnWaveOpen
      mstscax!CSndInputChannelCallback::OpenAudioInDevice
      mstscax!CSndInputChannelCallback::StartWaveCapture
    seq:
      - id: frames_per_packet
        -orig-id: FramesPerPacket
        type: u4
        doc: |
          This field specifies the number of audio frames that the client MUST send in each
          Data PDU. The product of nChannels, 2, and FramesPerPacket is the size of audio
          data that the client MUST put in a Data PDU. The audio data size is determined by
          the server application and is not determined by the Remote Desktop Protocol: Audio
          Input Redirection Virtual Channel Extension.
      - id: initial_format
        -orig-id: initialFormat
        type: u4
        doc: |
          This field is an index into the list of audio formats agreed on by the client and
          server. This field specifies the initial audio format.
      - id: format_tag
        -orig-id: wFormatTag
        type: u2
        doc: |
          A 16-bit unsigned integer identifying the compression codec of the audio format.
          For more information on compression formats, see [RFC2361]. If this field is set to
          WAVE_FORMAT_EXTENSIBLE (0xFFFE), the cbSize field MUST be equal to 22.
      - id: channels
        -orig-id: nChannels
        type: u2
        doc: |
          This field specifies the number of channels in the audio format identified by
          wFormatTag. It is part of the audio format and is not determined by the Remote Desktop
          Protocol: Audio Input Redirection Virtual Channel Extension.
      - id: samples_per_sec
        -orig-id: nSamplesPerSec
        type: u4
        doc: |
          This field specifies the sampling rate, in hertz, for recording audio.
      - id: avg_bytes_per_sec
        -orig-id: nAvgBytesPerSec
        type: u4
        doc: |
          This field specifies average data-transfer rate for the format, in bytes per second.
      - id: block_align
        -orig-id: nBlockAlign
        type: u2
        doc: |
          This field specifies the block alignment, in bytes. The block alignment is the minimum
          atomic unit of data for this audio format.
      - id: bits_per_sample
        -orig-id: wBitsPerSample
        type: u2
        doc: |
          This field specifies the size, in bits, for each audio sample.
      - id: extra_format_data_size
        -orig-id: cbSize
        type: u2
        doc: |
          A 16-bit unsigned integer specifying the size of the ExtraFormatData field.
      - id: extra_format_data
        -orig-id: ExtraFormatData
        size: extra_format_data_size
        doc: |
          Extra data specific to the audio format. The size of ExtraFormatData, in bytes, is
          cbSize. If cbSize is 0, this field MUST NOT exist. If wFormatTag is set to
          WAVE_FORMAT_EXTENSIBLE (0xFFFE), this field MUST be a WAVEFORMAT_EXTENSIBLE
          structure.

  sndin_open_reply:
    doc: |
      MS-RDPEAI 2.2.2.4 Open Reply PDU (MSG_SNDIN_OPEN_REPLY)
      The Open Reply PDU is sent by the client to convey the result of an attempt to open
      the audio capture device.
      mstscax!CSndInputChannelCallback::OnWaveOpen
      mstscax!CSndInputChannelCallback::OpenAudioInDevice
    seq:
      - id: result
        -orig-id: Result
        type: u4
        doc: |
          An HRESULT containing the status of the attempt to open the client-side
          Wave Capture Device.

  sndin_data_incoming:
    doc: |
      MS-RDPEAI 2.2.3.1 Incoming Data PDU (MSG_SNDIN_DATA_INCOMING)
      The Incoming Data PDU is sent by the client to indicate that the next packet sent MUST
      either be a Data PDU or a Sound Formats PDU. This PDU is used for diagnostic purposes.
      The time used to transmit a Data PDU/Sound Formats PDU can be found out by comparing
      the time between receiving a Data PDU/Sound Formats PDU and receiving an Incoming Data
      PDU. The server MAY use this time and the data size of a Data PDU/Sound Formats PDU to
      calculate how fast the network is transmitting data. The result of this calculation MAY
      then be used to determine the audio format to use.
      (no data)

  sndin_data:
    doc: |
      MS-RDPEAI 2.2.3.2 Data PDU (MSG_SNDIN_DATA)
      The Data PDU is sent by the client and contains a fragment of audio data.
    seq:
      - id: data
        -orig-id: Data
        size-eos: true
        doc: |
          A variable-length field that contains audio data. The recipient can determine the
          length of the Data field by subtracting the length of the Header field from the length
          of the Data PDU. The MSG_SNDIN_DATA structure is encapsulated in the Dynamic VC Data
          First PDU ([MS-RDPEDYC] section 2.2.3.1) and Dynamic VC Data PDU
          ([MS-RDPEDYC] section 2.2.3.2). The length of these PDUs is determined as described
          in [MS-RDPEDYC] sections 3.1.5.1 and 3.1.5.2.

  sndin_formatchange:
    doc: |
      MS-RDPEAI 2.2.4.1 Format Change PDU (MSG_SNDIN_FORMATCHANGE)
      The Format Change PDU is sent by the server to request that the client change audio formats
      and by the client to confirm a format change.
      mstscax!CSndInputChannelCallback::OnNewFormat
    seq:
      - id: new_format
        -orig-id: NewFormat
        type: u4
        doc: |
          A 32-bit unsigned integer that represents an index into the list of audio formats
          exchanged between the client and server during the initialization phase, as specified
          in section 3.1.1. For more information about changing audio formats,
          see sections 3.2.5.3 and 3.3.5.3.
