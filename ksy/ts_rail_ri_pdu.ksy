meta:
  id: ts_rail_ri_pdu
  endian: le
  imports:
    - ts_window_order

doc: |
    MS-RDPERP 2.2.1.3 Windowing Alternate Secondary Drawing Orders
    The "rail_ri" channel handles Windows orders.
    mstscax!RdpWindowPlugin::OnVcPacket_TrayInfo
    mstscax!RdpWindowPlugin::OnWindowRailPdu

seq:
  - id: window_order
    type: ts_window_order
    doc: |
      All messages in this channel are TS_WINDOW_ORDER.
