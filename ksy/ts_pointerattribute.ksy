meta:
  id: ts_pointerattribute
  endian: le
  imports:
    - ts_pointerattribute
    - ts_colorpointerattribute

doc: |
  MS-RDPBCGR 2.2.9.1.1.4.5 New Pointer Update (TS_POINTERATTRIBUTE)
  The TS_POINTERATTRIBUTE structure is used to send pointer data at an
  arbitrary color depth. Support for the New Pointer Update is advertised
  in the Pointer Capability Set (section 2.2.7.1.5).
  mstscax!CCM::CM_PointerPDU
  mstscax!CCM::CMCreateNewCursor

seq:
  - id: xor_bpp
    type: u2
    doc: |
      A 16-bit, unsigned integer. The color depth in bits-per-pixel of the
      XOR mask contained in the colorPtrAttr field.
  - id: color_ptr_attr
    type: ts_colorpointerattribute
    doc: |
      Encapsulated Color Pointer Update (section 2.2.9.1.1.4.4) structure
      which contains information about the pointer. The Color Pointer Update
      fields are all used, as specified in section 2.2.9.1.1.4.4; however
      color XOR data is presented in the color depth described in the xorBpp
      field (for 8 bpp, each byte contains one palette index; for 4 bpp,
      there are two palette indices per byte).
