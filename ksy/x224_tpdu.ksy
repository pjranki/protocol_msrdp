meta:
  id: x224_tpdu
  endian: be
  imports:
    - t125_mcs_pdu

doc: |
  X224 13 Structure and encoding of TPDUs
  mstscax!CTSX224Filter::SendBuffer
  mstscax!CTSX224Filter::OnDataAvailable

seq:
  - id: length
    type: u1
    doc: |
      13.2.1 Length indicator field
      The field is contained in the first octet of the TPDUs. The length is
      indicated by a binary number, with a maximum value of 254 (1111 1110).
      The length indicated shall be the header length in octets including parameters,
      but excluding the length indicator field and user data, if any. The value 255
      (1111 1111) is reserved for possible extensions.
  - id: code
    type: b4
    enum: tpdu_code
    doc: |
      This field contains the TPDU code and is contained in octet 2 of the header.
      It is used to define the structure of the remaining header.
  - id: flags
    type: b4
    doc: |
      These flags depend on the value of code.
  - id: data
    -set:
      - id: code
        value: _case
    type:
      switch-on: code
      -name: type
      cases:
        'tpdu_code::data': tpdu_data(length)
        'tpdu_code::disconnect_request': tpdu_disconnect_request(length)
        'tpdu_code::connection_request': tpdu_connection_request(length)
        'tpdu_code::connection_confirm': tpdu_connection_confirm(length)

enums:
  tpdu_code:
    0xe:
     id: connection_request
     doc: 1110 - CR Connection request - see section 13.3
    0xd:
      id: connection_confirm
      doc: 1101 - CC Connection confirm - see section 13.4
    0x8:
      id: disconnect_request
      doc: 1000 - DR Disconnect request - see section 13.5
    0xc:
      id: disconnect_confirm
      doc: 1100 - DC Disconnect confirm - see section 13.6 
    0xf:
      id: data
      doc: 1111 - DT Data - see section 13.7
    0x1:
      id: expedited_data
      doc: 0001 - ED Expedited data - see section 13.8
    0x6:
      id: data_ack
      doc: 0110 - AK Data acknowledgement - see section 13.9
    0x4:
      id: expedited_data_ack
      doc: 0010 - EA Expedited data acknowledgement - see section 13.10
    0x5:
      id: reject
      doc: 0101 - RJ Reject - see section 13.11
    0x7:
      id: tpdu_error
      doc: 0111 - ER TPDU error - see section 13.12

types:
  tpdu_variable_part:
    doc: |
      X224 13.2.3 Variable part
    seq:
      - id: code
        type: u1
      - id: length
        type: u1
      - id: value
        size: length

  tpdu_variables:
    seq:
      - id: params
        type: tpdu_variable_part
        repeat: eos

  tpdu_data:
    doc: |
      X224 13.7 Data (DT) TPDU
      mstscax!CTSX224Filter::OnDataAvailable
    params:
      - id: header_length
        type: u1
    seq:
      - id: nr_eot
        type: u1
      - id: data
        type: t125_mcs_pdu(0)

  tpdu_disconnect_request:
    doc: |
      X224 13.5 Disconnect Request (DR) TPDU
      mstscax!CTSX224Filter::XTHandleControlPkt
    params:
      - id: header_length
        type: u1
    seq:
      - id: dst_ref
        type: u2
      - id: src_ref
        type: u2
      - id: reason
        type: u1
      - id: params
        type: tpdu_variables
        size: header_length - 6
        if: header_length > 6
      - id: data
        size-eos: true

  tpdu_connection_request:
    doc: |
      X224 13.3 Connection Request (CR) TPDU
      mstscax!CTSX224Filter::XTHandleControlPkt
    params:
      - id: header_length
        type: u1
    seq:
      - id: dst_ref
        type: u2
      - id: src_ref
        type: u2
      - id: class_option
        type: u1
      - id: params
        type: tpdu_variables
        size: header_length - 6
        if: header_length > 6
      - id: data
        size-eos: true

  tpdu_connection_confirm:
    doc: |
      X224 13.4 Connection Confirm (CC) TPDU
      mstscax!CTSX224Filter::XTHandleControlPkt
      mstscax!CTSX224Filter::XTHandleX224ConnectConfirm
      mstscax!CTSX224Filter::XTFireSecurityNegotiationComplete
      mstscax!CMCS::OnConnected
    params:
      - id: header_length
        type: u1
    seq:
      - id: dst_ref
        type: u2
      - id: src_ref
        type: u2
      - id: class_option
        type: u1
      - id: neg_type
        type: u1
      - id: neg_flags
        type: u1
      - id: neg_length
        type: u2le
        doc: |
          This must be set to the total size of PDU - 11.
          It is little endian (even though the rest of this protocol is big endian).
      - id: neg_msg
        type:
          switch-on: neg_type
          -name: type
          cases:
            2: tpdu_neg_rsp
            3: tpdu_neg_failure
    enums:
      neg_flags:
        0x01: extended_gcc_user_data_supported
        0x02: dynvc_gfx_protocol_server_supported
        0x04: live_id_supported
        0x08: redirected_authentication
        0x10: unrestricted_logon

  tpdu_neg_rsp:
    doc: |
      mstscax!CTSX224Filter::XTHandleNegRsp
    seq:
      - id: server_selected_protocols
        type: u4le

  tpdu_neg_failure:
    doc: |
      mstscax!CTSX224Filter::XTHandleNegFailure
    seq:
      - id: error_code
        type: u4le
        enum: neg_error
    enums:
      neg_error:
        1: server_ssl_support_1
        2: server_ssl_support_0
        3: server_cert_error
        4: code_2569
        5: code_2825
        6: code_3081
        7: code_2313
