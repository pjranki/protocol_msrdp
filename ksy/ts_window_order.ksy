meta:
  id: ts_window_order
  endian: le
  imports:
    - ts_rectangle16

doc: |
  MS-RDPERP 2.2.1.3 Windowing Alternate Secondary Drawing Orders
  fastpath:
    mstscax!CUH::UH_OnUnknownAltSecPacket
    mstscax!CTSCoreEventSource::FireSyncNotification
    mstscax!CTSCoreEventSource::InternalFireSyncNotification
    mstscax!CTSThread::AddCallback
    mstscax!CTSThread::RunQueueEvent
    mstscax!RdpWindowPlugin::OnAltSecPDUReceived
    mstscax!RdpWindowPlugin::OnWindowOrder
  rail_wi:
    mstscax!RdpWindowPlugin::OnVcPacket_WindowingInfo
    mstscax!RdpWindowPlugin::OnWindowRailPdu
    mstscax!RdpWindowPlugin::OnWindowOrder
  rail_ri:
    mstscax!RdpWindowPlugin::OnVcPacket_TrayInfo
    mstscax!RdpWindowPlugin::OnWindowRailPdu
    mstscax!RdpWindowPlugin::OnWindowOrder

seq:
  - id: order_size
    type: u2
    doc: |
      An unsigned 16-bit integer. The size of the entire packet, in bytes.
  - id: fields_present_flags
    type: u4
    doc: |
      An unsigned 32-bit integer. The flags indicating which fields are present
      in the packet. See Orders.
  - id: order
    # size: data_size
    type:
      switch-on: order_type
      -name: type
      cases:
        'window_order_type::window': ts_window_info_order(fields_present_flags)
        'window_order_type::notify': ts_notifyicon_order(fields_present_flags)
        'window_order_type::desktop': ts_desktop_order(fields_present_flags)

instances:
  order_type:
    value: fields_present_flags & 0x07000000
    enum: window_order_type
  data_size:
    value: order_size - 7
    doc: |
      Size of data (order_size minus size of header)

enums:
  window_order_type:
    0x01000000:
      id: window
      doc: |
        Indicates a Windowing Alternate Secondary Drawing Order describing a window.
    0x02000000:
      id: notify
      doc: |
        Indicates a Windowing Alternate Secondary Drawing Order specifying a notification icon.
    0x04000000:
      id: desktop
      doc: |
        Indicates an order specifying a desktop.

types:
  unicode_string:
    doc: |
      MS-RDPERP 2.2.1.2.1 Unicode String (UNICODE_STRING)
      The UNICODE_STRING packet is used to pack a variable-length Unicode string.
      mstscax!CWndPluginDecode::DecodeLengthAndString
    seq:
      - id: size
        type: u2
        doc: |
          An unsigned 16-bit integer. The number of bytes in the String field. If
          CbString is zero (0), then the String field is absent. The maximum allowed
          value for CbString depends on the context in which the string is used.
      - id: string
        type: str
        encoding: UTF-16LE
        size: size
        doc: |
          Optional and of variable length. A non-null-terminated Unicode character string.
          The number of characters in the string is equal to the value of CbString divided by 2.

  ts_icon_info:
    doc: |
      MS-RDPERP 2.2.1.2.3 Icon Info (TS_ICON_INFO)
      The TS_ICON_INFO packet describes an icon.
    seq:
      - id: cache_entry
        type: u2
        doc: |
          An unsigned 16-bit integer. The index within an icon cache at which this icon MUST
          be stored at the client. The index is unique within a given CacheId (see following
          description). The maximum value of CacheEntry is negotiated between server and client
          through the NumIconCacheEntries field of the Window List Capability Set during the
          connection establishment phase.
          mstscax!RdpWindowPlugin::DecodeIconOrder
      - id: cache_id
        type: u1
        doc: |
          An unsigned 8-bit integer. The index of the icon cache at which this icon MUST be
          stored at the client. If the value is 0xFFFF, the icon SHOULD NOT be cached. The
          CacheId is unique within a remote session.
          The maximum value of CacheId is negotiated between server and client through the
          NumIconCaches field of the Window List Capability Set while establishing the
          connection.
      - id: bpp
        type: u1
        doc: |
          An unsigned 8-bit integer. The color depth of the icon.
          Valid values are as follows: 1, 4, 8, 16, 24, 32.
      - id: width
        type: u2
        doc: |
          An unsigned 16-bit integer. The width, in pixels, of the icon.
      - id: height
        type: u2
        doc: |
          An unsigned 16-bit integer. The height, in pixels, of the icon.
      - id: color_table_size
        type: u2
        if: (bpp == 1) or (bpp == 4) or (bpp == 8)
        doc: |
          An unsigned 16-bit integer. The size, in bytes, of the color table data. This
          field is ONLY present if the bits per pixel (Bpp) value is 1, 4, or 8.
      - id: bits_mask_size
        type: u2
        doc: |
          An unsigned 16-bit integer. The size, in bytes, of the icon's one-bit color-depth
          mask image.
      - id: bits_color_size
        type: u2
        doc: |
          An unsigned 16-bit integer. The size, in bytes, of the icon's color image.
      - id: bits_mask
        size: bits_mask_size
        doc: |
          The image data for the 1-bpp bitmap. The length, in bytes, of this field is equal
          to the value of CbBitsMask. This field is optional.
      - id: color_table
        size: color_table_size
        if: (bpp == 1) or (bpp == 4) or (bpp == 8)
        doc: |
          The image data for the color bitmap. The length, in bytes, of this field is equal
          to the value of CbColorTable.
          This field is only present if the Bpp value is 1, 4,or 8.
      - id: bits_color
        size: bits_color_size
        doc: |
          The image data for the icon's color image. The length, in bytes, of this field is
          equal to the value of CbBitsColor. This field is optional.

  ts_cached_icon_info:
    doc: |
      MS-RDPERP 2.2.1.2.4 Cached Icon Info (TS_CACHED_ICON_INFO)
      The TS_CACHED_ICON_INFO packet describes a cached icon.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
    seq:
      - id: cache_entry
        type: u2
        doc: |
          An unsigned 16-bit integer. The index within an icon cache at the client that refers
          to the cached icon. This value MUST have been previously specified by the server in the
          Icon Info structure (section 2.2.1.2.3) of a Window Information Order (section 2.2.1.3.1)
          or Icon structure of a New or Existing Notification Icon (section 2.2.1.3.2.2.1).
      - id: cache_id
        type: u1
        doc: |
          An unsigned 8-bit integer. The index of the icon cache containing the cached icon. This
          value MUST have been previously specified by the server in the Icon Info structure of a
          Window Information Order or Icon structure of a New or Existing Notification Icon.

  ts_window_info_order:
    doc: |
      MS-RDPERP 2.2.1.3.1 Window Information (TS_WINDOW_ORDER)
      Window Information Orders specify the state of windows on the server.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window being described in the drawing
          order. It is generated by the server and is unique for every window in the session.
          mstscax!RdpWindowPlugin::FindWindowInfo
          mstscax!RdpWindowPlugin::CreateWindowInfo
      - id: order
        type:
          switch-on: order_type
          -name: type
          cases:
            'window_order_type::window_information': ts_window_info_order_info(fields_present_flags)
            'window_order_type::window_icon': ts_window_info_order_icon(fields_present_flags)
            'window_order_type::cached_icon': ts_window_info_order_cachedicon(fields_present_flags)
            'window_order_type::delete_window': ts_window_info_order_delete(fields_present_flags)
    instances:
      order_type:
        value: fields_present_flags & 0xe0000000
        enum: window_order_type
    enums:
      window_order_type:
        0x00000000: window_information
        0x20000000: delete_window
        0x40000000: window_icon
        0x80000000: cached_icon

  ts_window_info_order_info:
    doc: |
      MS-RDPERP 2.2.1.3.1.2.1 New or Existing Window (TS_WINDOW_ORDER_INFO)
      A Window Information Order is generated by the server whenever a new window is created on
      the server or when a property on a new or existing window is updated. The window metrics
      sent in this order are illustrated in the "An illustration of the window metrics sent in
      the Window Information Order" figure in section 3.3.5.1.6.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: owner_window_id
        type: u4
        if: (fields_present_flags & 0x00000002) != 0
        doc: |
          An unsigned 32-bit integer. The ID of the window on the server that is the owner of
          the window specified in WindowId field of Hdr. For more information on owned windows,
          see [MSDN-WINFEATURE]. This field is present if and only if the
          WINDOW_ORDER_FIELD_OWNER flag is set in the FieldsPresentFlags field of
          TS_WINDOW_ORDER_HEADER.
      - id: style
        type: u4
        if: (fields_present_flags & 0x00000008) != 0
        doc: |
          An unsigned 32-bit integer. Describes the window's current style. Window styles
          determine the appearance and behavior of a window. For more information, see
          [MSDN-WINSTYLE]. This field is present if and only if the WINDOW_ORDER_FIELD_STYLE
          flag is set in the FieldsPresentFlags field of the TS_WINDOW_ORDER_HEADER.
      - id: extended_style
        type: u4
        if: (fields_present_flags & 0x00000008) != 0
        doc: |
          An unsigned 32-bit integer. Extended window style information. For more information
          about extended window styles, see [MSDN-CREATEWINEX].
          This field is present if and only if the WINDOW_ORDER_FIELD_STYLE flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: show_state
        type: u1
        enum: show_state
        if: (fields_present_flags & 0x00000010) != 0
        doc: |
          An unsigned 8-bit integer. Describes the show state of the window.
          This field is present if and only if the WINDOW_ORDER_FIELD_SHOW flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: title_info
        type: unicode_string
        if: (fields_present_flags & 0x00000004) != 0
        doc: |
          UNICODE_STRING. Variable length. Contains the window's title string. This string is
          not guaranteed to be null-terminated. The maximum value for the CbString field of
          UNICODE_STRING is 520 bytes. This structure is present only if the WINDOW_ORDER_FIELD_TITLE
          flag is set in the FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: client_offset_x
        type: u4
        if: (fields_present_flags & 0x00004000) != 0
        doc: |
          A 32-bit signed integer. The X (horizontal) offset from the top-left corner of the screen
          to the top-left corner of the window's client area, expressed in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_CLIENTAREAOFFSET flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: client_offset_y
        type: u4
        if: (fields_present_flags & 0x00004000) != 0
        doc: |
          A 32-bit signed integer. The Y (vertical) offset from the top-left corner of the screen to
          the top-left corner of the window's client area, expressed in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_CLIENTAREAOFFSET flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: client_area_width
        type: u4
        if: (fields_present_flags & 0x10000) != 0
        doc: |
          An unsigned 32-bit integer specifying the width of the client area rectangle of the target
          window.
          This field only appears if the WndSupportLevel field of the Window List Capability Set message
          is set to TS_WINDOW_LEVEL_SUPPORTED_EX (as specified in section 2.2.1.1.2) and the
          WINDOW_ORDER_FIELD_CLIENTAREASIZE flag is set in the FieldsPresentFlags field of the
          TS_WINDOW_ORDER_HEADER packet (section 2.2.1.3.1.1).
      - id: client_area_height
        type: u4
        if: (fields_present_flags & 0x10000) != 0
        doc: |
          An unsigned 32-bit integer specifying the height of the client area rectangle of the target
          window.
          This field only appears if the WndSupportLevel field of the Window List Capability Set message
          is set to TS_WINDOW_LEVEL_SUPPORTED_EX (as specified in section 2.2.1.1.2) and the Hdr field has
          the WINDOW_ORDER_FIELD_CLIENTAREASIZE flag is set in the FieldsPresentFlags field of the
          TS_WINDOW_ORDER_HEADER packet (section 2.2.1.3.1.1).
      - id: window_left_resize_margin
        type: u4
        if: (fields_present_flags & 0x80) != 0
        doc: |
          An unsigned 32-bit integer specifying the width of the transparent hit-testable margin along the
          left edge of the window. Any mouse, pen, or touch input within this margin SHOULD be sent to the
          server.
          This field is present only if the WINDOW_ORDER_FIELD_RESIZE_MARGIN_X flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
          Resize margins SHOULD be used to extend the window geometry (defined by the WindowOffsetX,
          WindowOffsetY, WindowWidth and WindowHeight fields) and are not included in the window
          boundaries.
      - id: window_right_resize_margin
        type: u4
        if: (fields_present_flags & 0x80) != 0
        doc: |
          An unsigned 32-bit integer specifying the width of the transparent hit-testable margin along the
          right edge of the window. Any mouse, pen or touch input within this margin SHOULD be sent to the
          server.
          This field is present only if the WINDOW_ORDER_FIELD_ RESIZE_MARGIN_X flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
          Resize margins SHOULD be used to extend the window geometry (defined by the WindowOffsetX,
          WindowOffsetY, WindowWidth and WindowHeight fields) and are not included in the window
          boundaries.
      - id: window_top_resize_margin
        type: u4
        if: (fields_present_flags & 0x08000000) != 0
        doc: |
          An unsigned 32-bit integer specifying the height of the transparent hit-testable margin along the
          top edge of the window. Any mouse, pen or touch input within this margin SHOULD be sent to the
          server.
          This field is present only if the WINDOW_ORDER_FIELD_ RESIZE_MARGIN_Y flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
          Resize margins SHOULD be used to extend the window geometry (defined by the WindowOffsetX,
          WindowOffsetY, WindowWidth and WindowHeight fields) and are not included in the window
          boundaries.
      - id: window_bottom_resize_margin
        type: u4
        if: (fields_present_flags & 0x08000000) != 0
        doc: |
          An unsigned 32-bit integer specifying the height of the transparent hit-testable margin along the
          bottom edge of the window. Any mouse, pen or touch input within this margin SHOULD be sent to the
          server.
          This field is present only if the WINDOW_ORDER_FIELD_ RESIZE_MARGIN_Y flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
          Resize margins SHOULD be used to extend the window geometry (defined by the WindowOffsetX,
          WindowOffsetY, WindowWidth and WindowHeight fields) and are not included in the window
          boundaries.
      - id: rp_content
        type: u1
        if: (fields_present_flags & 0x00020000) != 0
        doc: |
          This field only appears if the WndSupportLevel field of the Window List Capability Set message
          is set to TS_WINDOW_LEVEL_SUPPORTED_EX (as specified in section 2.2.1.1.2) and the Hdr field has
          the WINDOW_ORDER_FIELD_RPCONTENT flag is set in the FieldsPresentFlags field of the
          TS_WINDOW_ORDER_HEADER packet (section 2.2.1.3.1.1).
            - 0x00: The window is not used by a render plug-in to do client-side rendering.
            - 0x01: The window is used by a render plug-in to do client-side rendering.
      - id: root_parent_handle
        type: u4
        if: (fields_present_flags & 0x00040000) != 0
        doc: |
          An unsigned 32-bit integer specifying the server-side target window's top-level parent window
          handle. A Top-Level parent window is the window immediately below "desktop" in the window
          hierarchy. If the target window is a top-level window, the window handle of the target window
          is sent.
          This field only appears if the WndSupportLevel field of the Window List Capability Set message
          is set to TS_WINDOW_LEVEL_SUPPORTED_EX (as specified in section 2.2.1.1.2) and the Hdr field has
          the WINDOW_ORDER_FIELD_ROOTPARENT flag is set in the FieldsPresentFlags field of the
          TS_WINDOW_ORDER_HEADER packet (section 2.2.1.3.1.1).
      - id: window_offset_x
        type: u4
        if: (fields_present_flags & 0x00000800) != 0
        doc: |
          A 32-bit signed integer. The X (horizontal) offset from the top-left corner of the primary monitor
          to the top-left corner of the window, expressed in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_WNDOFFSET flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: window_offset_y
        type: u4
        if: (fields_present_flags & 0x00000800) != 0
        doc: |
          A 32-bit signed integer. The Y (vertical) offset from the top-left corner of the primary monitor
          to the top-left corner of the window, expressed in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_WNDOFFSET flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: window_client_delta_x
        type: u4
        if: (fields_present_flags & 0x00008000) != 0
        doc: |
          A 32-bit signed integer. The X (horizontal) delta between the top-left corner of the window and the
          window's client area.
          This field is present only if the WINDOW_ORDER_FIELD_CLIENTDELTA flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: window_client_delta_y
        type: u4
        if: (fields_present_flags & 0x00008000) != 0
        doc: |
          A 32-bit signed integer. The Y (vertical) delta between the top-left corner of the window and the
          window's client area.
          This field is present only if the WINDOW_ORDER_FIELD_CLIENTDELTA flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: window_width
        type: u4
        if: (fields_present_flags & 0x00000400) != 0
        doc: |
          An unsigned 32-bit integer. The window width, in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_WNDSIZE flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: window_height
        type: u4
        if: (fields_present_flags & 0x00000400) != 0
        doc: |
          An unsigned 32-bit integer. The window height, in screen coordinates.
          This field is present only if the WINDOW_ORDER_FIELD_WNDSIZE flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: num_window_rects
        type: u2
        if: (fields_present_flags & 0x00000100) != 0
        doc: |
          An unsigned 16-bit integer. A count of rectangles describing the window geometry.
          This field is present only if the WINDOW_ORDER_FIELD_WNDRECTS flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
          mstscax!CWndPluginDecode::DecodeCountAndRects
      - id: window_rects
        type: ts_rectangle16
        if: (fields_present_flags & 0x00000100) != 0
        repeat: expr
        repeat-expr: num_window_rects
        doc: |
          An array of TS_RECTANGLE_16 structures, NumWindowRects wide, describing the window geometry. All
          coordinates are window coordinates.
          This field is present only if the NumWindowRects field is greater than 0 and the
          WINDOW_ORDER_FIELD_WNDRECTS flag is set in the FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: visible_offset_x
        type: u4
        if: (fields_present_flags & 0x00001000) != 0
        doc: |
          A 32-bit signed integer. The X (horizontal) offset from the top-left corner of the screen to the
          top-left corner of the window visible region's bounding rectangle, expressed in screen
          coordinates.<6>
          This field is present only if the WINDOW_ORDER_FIELD_VISOFFSET flag is set in the FieldsPresentFlags 
          field of TS_WINDOW_ORDER_HEADER.
      - id: visible_offset_y
        type: u4
        if: (fields_present_flags & 0x00001000) != 0
        doc: |
          A 32-bit signed integer. The Y (vertical) offset from the top-left corner of the screen to the
          top-left corner of the window visible region's bounding rectangle, expressed in screen
          coordinates.<7>
          This field is present only if the WINDOW_ORDER_FIELD_VISOFFSET flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: numb_visible_rects
        type: u2
        if: (fields_present_flags & 0x00000200) != 0
        doc: |
          An unsigned 16-bit integer. A count of rectangles describing the window visible region.
          This field is present only if the WINDOW_ORDER_FIELD_VISIBILITY flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: visibility_rects
        type: ts_rectangle16
        if: (fields_present_flags & 0x00000200) != 0
        repeat: expr
        repeat-expr: numb_visible_rects
        doc: |
          An array of TS_RECTANGLE_16 structures, NumVisibilityRects wide, describing the window visible region.
          All coordinates are window coordinates.
          This field is present only if the value of the NumVisibilityRects field is greater than 0 and the
          WINDOW_ORDER_FIELD_VISIBILITY flag is set in the FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: overlay_description
        type: unicode_string
        if: (fields_present_flags & 0x00400000) != 0
        doc: |
          A variable length UNICODE_STRING (section 2.2.1.2.1) that contains the description text for the window's
          overlay icon (see sections 2.2.1.3.1.2.2 and 2.2.1.3.1.2.3).
          This field is present only if the WINDOW_ORDER_FIELD_OVERLAY_DESCRIPTION flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: taskbar_button
        type: u1
        if: (fields_present_flags & 0x00800000) != 0
        doc: |
          An 8-bit unsigned integer. If this field is set to 0x00, then the client SHOULD add a tab to the taskbar
          button group for the window, if supported by the operating system, instead of adding a new taskbar button
          for the window. If this field is set to 0x01, then the client SHOULD remove the tab from the taskbar button
          group for the window.
          Windows share a taskbar button group if they have matching Application IDs, as specified by the Server Get
          Application ID Response PDU (section 2.2.2.8.1).
          This field is present only if the WINDOW_ORDER_FIELD_TASKBAR_BUTTON flag is set in the FieldsPresentFlags
          field of TS_WINDOW_ORDER_HEADER.
      - id: enforce_server_z_order
        type: u1
        if: (fields_present_flags & 0x00080000) != 0
        doc: |
          An 8-bit unsigned integer. If this field is set to 0x01, then the client SHOULD order this window, and all
          other windows in the Z-order list (in the Actively Monitored Desktop packet, as specified in section
          2.2.1.3.3.2.1) that also have the field set consecutively per the Z-order hierarchy. The client SHOULD NOT
          attempt to reorder these windows with respect to each other, or to move any window between the windows in
          this group.
          If this field is set to 0x00, then no special Z-order handling is required.
          This field is present only if the WINDOW_ORDER_FIELD_ENFORCE_SERVER_ZORDER flag is set in the
          FieldsPresentFlags field of TS_WINDOW_ORDER_HEADER.
      - id: app_bar_state
        type: u1
        if: (fields_present_flags & 0x00000040) != 0
        doc: |
          An 8-bit unsigned integer. If this field is set to 0x01, then the window SHOULD be registered as an
          application desktop toolbar. If this field is set to 0x00, then the application desktop toolbar SHOULD be
          deregistered.
          This field is present only if the WINDOW_ORDER_FIELD_APPBAR_STATE flag is set in the FieldsPresentFlags field
          of TS_WINDOW_ORDER_HEADER.
      - id: app_bar_edge
        type: u1
        if: (fields_present_flags & 0x00000001) != 0
        doc: |
          An 8-bit unsigned integer. The value of this field indicates the edge to which the application desktop toolbar
          SHOULD be anchored. This field MUST be set to one of the following possible values:
           - 0x00 - Anchor to the left edge.
           - 0x01 - Anchor to the top edge.
           - 0x02 - Anchor to the right edge.
           - 0x03 - Anchor to the bottom edge.
          This field is present only if the WINDOW_ORDER_FIELD_APPBAR_EDGE flag is set in the FieldsPresentFlags field
          of TS_WINDOW_ORDER_HEADER.
    enums:
      show_state:
        0x00:
          id: hide
          doc: Do not show the window.
        0x02:
          id: minimized
          doc: Show the window minimized.
        0x03:
          id: maximized
          doc: Show the window maximized.
        0x05:
          id: show
          doc: Show the window in its current size and position.
      window_order_state:
        0x10000000:
          id: new
          doc: |
            Indicates that the Windowing Alternate Secondary Drawing Order contains information
            for a new window. If this flag is not set, the order contains information for an
            existing window.
      window_order_field:
        0x08000000:
          id: resize_margin_y
          doc: |
            Indicates that the WindowTopResizeMargin and WindowBottomResizeMargin fields
            are present.
        0x00800000:
          id: taskbar_button
          doc: |
            Indicates that the TaskbarButton field is present.
        0x00400000:
          id: overlay_description
          doc: |
            Indicates that the OverlayDescription field is present.
        0x00080000:
          id: enforce_server_zorder
          doc: |
            Indicates that the EnforceServerZOrder field is present.
        0x00040000:
          id: rootparent
          doc: |
            Indicates that the RootParentHandle field is present. <5>
        0x00200000:
          id: icon_overlay_null
          doc: |
            Indicates that a taskbar overlay icon previously set by the window has been removed.
        0x00020000:
          id: rpcontent
          doc: |
            Indicates that the RPContent field is present. <4>
        0x00010000:
          id: clientareasize
          doc: |
            Indicates that the ClientAreaWidth and ClientAreaHeight fields are present.<3>
        0x00008000:
          id: clientdelta
          doc: |
            Indicates that the WindowClientDeltaX and WindowClientDeltaY fields are present.
        0x00004000:
          id: clientareaoffset
          doc: |
            Indicates that the ClientOffsetX and ClientOffsetY fields are present.
        0x00001000:
          id: vioffset
          doc: |
            Indicates that the VisibleOffsetX and VisibleOffsetY fields are present.
        0x00000800:
          id: wndoffset
          doc: |
            Indicates that the WindowOffsetX and WindowOffsetY fields are present.
        0x00000400:
          id: wndsize
          doc: |
            Indicates that the WindowWidth and WindowHeight fields are present.
        0x00000200:
          id: visibility
          doc: |
            Indicates that the NumVisibilityRects and VisibilityRects fields are present.
        0x00000100:
          id: wndrects
          doc: |
            Indicates that the NumWindowRects and WindowRects fields are present.
        0x00000080:
          id: resize_margin_x
          doc: |
            Indicates that the WindowLeftResizeMargin and WindowRightResizeMargin fields
            are present.
        0x00000040:
          id: appbar_state
          doc: |
            Indicates that the AppBarState field is present.
        0x00000020:
          id: not_used_00000020
          doc: |
            Unused field flag 0x00000020
        0x00000010:
          id: show
          doc: |
            Indicates that the ShowState field is present.
        0x00000008:
          id: style
          doc: |
            Indicates that the Style and ExtendedStyle fields are present.
        0x00000004:
          id: title
          doc: |
            Indicates that the TitleInfo field is present.
        0x00000002:
          id: owner
          doc: |
            Indicates that the OwnerWindowId field is present.
        0x00000001:
          id: appbar_edge
          doc: |
            Indicates that the AppBarEdge field is present.

  ts_window_info_order_icon:
    doc: |
      MS-RDPERP 2.2.1.3.1.2.2 Window Icon (TS_WINDOW_ORDER_ICON)
      The Window Icon packet is a Window Information Order generated by the server when a new or existing
      window sets or updates its associated icon.
      Icons are created by combining two bitmaps of the same size. The mask bitmap is always 1 bpp, although
      the color depth of the color bitmap can vary. The color bitmap can have an associated color table.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
      mstscax!RdpWindowPlugin::DecodeIconOrder
      mstscax!RdpIconCache::OnCacheIcon
      mstscax!RdpWinGraphicsPlatform::CreateIcon
      mstscax!RdpWinIcon::CreateInstance
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: icon_info
        type: ts_icon_info
        doc: |
          Variable length. TS_ICON_INFO structure. Describes the window's icon.
    enums:
      window_order_field:
        0x00100000:
          id: icon_overlay
          doc: |
            Indicates that the overlay icon for the window is being sent. If this flag is not
            present, the icon is an application icon.
        0x00002000:
          id: icon_big
          doc: |
            Indicates that the large version of the icon is being sent. If this flag is not present,
            the icon is a small icon. <8>

  ts_window_info_order_cachedicon:
    doc: |
      MS-RDPERP 2.2.1.3.1.2.3 Cached Icon (TS_WINDOW_ORDER_CACHEDICON)
      The Cached Icon Window Information Order is generated by the server when a new or existing
      window sets or updates the icon in its title bar or in the Alt-Tab dialog box. If the icon
      information was transmitted by the server in a previous Window Information Order or Notification
      Icon Information Order in the same session, and the icon was cacheable (that is, the server specified
      a cacheEntry and cacheId for the icon), the server reports the icon cache entries to avoid sending
      duplicate information.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
      mstscax!RdpIconCache::OnCachedIcon
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: cached_icon
        type: ts_cached_icon_info
        doc: |
          Three bytes. TS_CACHED_ICON_INFO structure. Describes a cached icon on the client.
    enums:
      window_order_field:
        0x00100000:
          id: icon_overlay
          doc: |
            Indicates that the overlay icon for the window is being sent. If this flag is not
            present, the icon is an application icon.
        0x00002000:
          id: icon_big
          doc: |
            Indicates that the large version of the icon is being sent. If this flag is not present,
            the icon is a small icon. <8>

  ts_window_info_order_delete:
    doc: |
      MS-RDPERP 2.2.1.3.1.2.4 Deleted Window (TS_WINDOW_ORDER_DELETE)
      The Deleted Window Information Order is generated by the server whenever an existing window is
      destroyed on the server.
      mstscax!RdpWindowPlugin::DecodeWindowInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.

  ts_notifyicon_order:
    doc: |
      MS-RDPERP 2.2.1.3.2 Notification Icon Information (TS_NOTIFYICON_ORDER)
      Notification Icon Information orders specify the state of the notification
      icon on the server.
      mstscax!RdpWindowPlugin::DecodeShellNotifyInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: window_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the window owning the notification icon specified
          in the drawing order. The ID is generated by the server and is unique for every window
          in the session.
          mstscax!RdpWindowPlugin::FindShellNotifyInfo
          mstscax!RdpWindowPlugin::CreateShellNotifyInfo
      - id: notify_icon_id
        type: u4
        doc: |
          An unsigned 32-bit integer. The ID of the notification icon specified in the drawing
          order.The ID is generated by the application that owns the notification icon and SHOULD
          be unique for every notification icon owned by the application.
          mstscax!RdpWindowPlugin::FindShellNotifyInfo
          mstscax!RdpWindowPlugin::CreateShellNotifyInfo
      - id: order
        type:
          switch-on: order_type
          -name: type
          cases:
            0x00000000: ts_notifyicon_order_info(fields_present_flags)
            0x20000000: ts_notifyicon_order_delete(fields_present_flags)
    instances:
      order_type:
        value: fields_present_flags & 0x20000000
    enums:
      notifyicon_order_type:
        0x00000000: icon_information
        0x20000000: delete_icon

  ts_notifyicon_order_info:
    doc: |
      MS-RDPERP 2.2.1.3.2.2.1 New or Existing Notification Icons (TS_NOTIFYICON_ORDER_INFO)
      The Notification Icon Information Order packet is generated by the server whenever a new
      notification icon is created on the server or when an existing notification icon is
      updated.
      mstscax!RdpWindowPlugin::DecodeShellNotifyInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: version
        type: u4
        if: (fields_present_flags & 0x00000008) != 0
        doc: |
          An unsigned 32-bit integer. Specifies the behavior of the notification icons. This
          field is present only if the WINDOW_ORDER_FIELD_NOTIFY_VERSION flag is set in the
          FieldsPresentFlags field of TS_NOTIFYICON_ORDER_HEADER. This field MUST be set to
          one of the following values.
           - 0: Use this value for applications designed for Windows NT 4.0 operating system.
           - 3: Use the Windows 2000 operating system notification icons behavior. Use this
                value for applications designed for Windows 2000 and Windows XP operating
                system.
           - 4: Use the current behavior. Use this value for applications designed for Windows
                Vista operating system and Windows 7 operating system.
      - id: tool_tip
        type: unicode_string
        if: (fields_present_flags & 0x00000001) != 0
        doc: |
          Variable length. UNICODE_STRING. Specifies the text of the notification icon tooltip.
          This structure is present only if the WINDOW_ORDER_FIELD_NOTIFY_TIP flag is set in the
          FieldsPresentFlags field of TS_NOTIFYICON_ORDER_HEADER.
      - id: info_tip
        type: ts_notify_icon_infotip
        if: (fields_present_flags & 0x00000002) != 0
        doc: |
          Variable length. A TS_NOTIFY_ICON_INFOTIP structure. Specifies the notification icon’s
          balloon tooltip. This field SHOULD NOT be present for icons that follow Windows 95
          operating system behavior (Version = 0). This structure is present only if the
          WINDOW_ORDER_FIELD_NOTIFY_INFO_TIP flag is set in the FieldsPresentFlags field of
          TS_NOTIFYICON_ORDER_HEADER.
      - id: state
        type: u4
        if: (fields_present_flags & 0x00000004) != 0
        doc: |
          Unsigned 32-bit integer. Specifies the state of the notification icon. This field SHOULD
          NOT be present for icons that follow Windows 95 behavior (Version = 0).
          This field is present only if the WINDOW_ORDER_FIELD_NOTIFY_STATE flag is set in the
          FieldsPresentFlags field of TS_NOTIFYICON_ORDER_HEADER.
           - 1: The notification icon is hidden.
      - id: icon
        type: ts_icon_info
        if: (fields_present_flags & 0x40000000) != 0
        doc: |
          Variable length. A TS_ICON_INFO structure. Specifies the notification icon’s image.
          This structure is present only if the WINDOW_ORDER_ICON flag is set in the
          FieldsPresentFlags field of TS_NOTIFYICON_ORDER_HEADER.
          A Notification Icon Order MUST NOT contain both an Icon field and a CachedIcon field.
          If the WINDOW_ORDER_STATE_NEW flag is set, either the Icon field or the CachedIcon
          field MUST be present.
      - id: cached_icon
        type: ts_cached_icon_info
        if: (fields_present_flags & 0x80000000) != 0
        doc: |
          Three bytes. A TS_CACHED_ICON_INFO structure. Specifies the notification icon as a
          cached icon on the client.
          This structure is present only if the WINDOW_ORDER_CACHEDICON flag is set in the
          FieldsPresentFlags field of TS_NOTIFYICON_ORDER_HEADER. Only one of Icon and CachedIcon
          fields SHOULD be present in the Notification Icon Order. If the WINDOW_ORDER_STATE_NEW
          flag is set, only one of these fields MUST be present.
    enums:
      window_order_state:
        0x10000000:
          id: new
          doc: |
            Indicates that the Windowing Alternate Secondary Drawing Order contains information
            for a new notification icon. If this flag is set, one of the Icon and CachedIcon
            fields MUST be present. If this flag is not set, the Windowing Alternate Secondary
            Drawing Order contains information for an existing notification icon.
      window_order_field:
        0x00000008:
          id: notify_version
          doc: Indicates that the Version field is present.
        0x00000001:
          id: notify_tip
          doc: Indicates that the ToolTip field is present.
        0x00000002:
          id: notify_info_tip
          doc: Indicates that the InfoTip field is present.
        0x00000004:
          id: notify_state
          doc: Indicates that the State field is present.
        0x40000000:
          id: icon
          doc: |
            Indicates that the Icon field is present.
            Either the Icon or the CachedIcon field SHOULD be present, but not both.
        0x80000000:
          id: cached_icon
          doc: |
            Indicates that the CachedIcon field is present.
            Either the Icon or the CachedIcon field SHOULD be present, but not both. <10>

  ts_notifyicon_order_delete:
    doc: |
      MS-RDPERP 2.2.1.3.2.2.2 Deleted Notification Icons (TS_NOTIFYICON_ORDER_DELETE)
      The server generates a Notification Icon Information (section 2.2.1.3.2) order packet
      whenever an existing notification icon is deleted on the server.
      mstscax!RdpWindowPlugin::DecodeShellNotifyInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.

  ts_notify_icon_infotip:
    doc: |
      MS-RDPERP 2.2.1.3.2.2.3 Notification Icon Balloon Tooltip (TS_NOTIFY_ICON_INFOTIP)
      The TS_NOTIFY_ICON_INFOTIP structure specifies the balloon tooltip of a notification icon.
      mstscax!RdpWindowPlugin::DecodeShellNotifyInformation
    seq:
      - id: timeout
        type: u4
        doc: |
          An unsigned 32-bit integer. The timeout in milliseconds for the notification icon’s
          balloon tooltip. After the specified timeout, the tooltip SHOULD be destroyed. <11>
      - id: info_flags
        type: u4
        enum: niif
        doc: |
          An unsigned 32-bit integer. The flags that can be set to add an icon to a balloon tooltip.
          It is placed to the left of the title. If the InfoTipText field length is zero-length,
          the icon is not shown.
      - id: info_tip_text
        type: unicode_string
        doc: |
          Variable length. A UNICODE_STRING specifying the text of the balloon tooltip. The maximum
          length of the tooltip text string is 510 bytes.
      - id: title
        type: unicode_string
        doc: |
          Variable length. A UNICODE_STRING specifying the title of the balloon tooltip. The maximum
          length of the tooltip title string is 126 bytes.
    enums:
      niif:
        0x00000000:
          id: none
          doc: Do not show an icon.
        0x00000001:
          id: info
          doc: Show an informational icon next to the balloon tooltip text.
        0x00000002:
          id: warning
          doc: Show a warning icon next to the balloon tooltip text.
        0x00000003:
          id: error
          doc: Show an error icon next to the balloon tooltip text.
        0x00000010:
          id: nosound
          doc: Do not play an associated sound.
        0x00000020:
          id: large_icon
          doc: Show the large version of the icon.

  ts_desktop_order:
    doc: |
      MS-RDPERP 2.2.1.3.3 Desktop Information (TS_DESKTOP_ORDER)
      Desktop Information Orders specify the state of the desktop on the server.
      mstscax!RdpWindowPlugin::DecodeDesktopInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: order
        type:
          switch-on: order_type
          -name: type
          cases:
            'desktop_order_type::actively_monitored': ts_desktop_order_actively_monitored(fields_present_flags)
            'desktop_order_type::non_monitored': ts_desktop_order_non_monitored(fields_present_flags)
    instances:
      order_type:
        value: fields_present_flags & 0x00000001
        enum: desktop_order_type
    enums:
      desktop_order_type:
        0x00000000: actively_monitored
        0x00000001: non_monitored

  ts_desktop_order_actively_monitored:
    doc: |
      MS-RDPERP 2.2.1.3.3.2.1 Actively Monitored Desktop (TS_DESKTOP_ORDER_ACTIVELY_MONITORED)
      The Actively Monitored Desktop packet contains information about the actively monitored desktop.
      mstscax!RdpWindowPlugin::DecodeDesktopInformation
      mstscax!RdpWndZOrder::DecodeZOrderInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
    seq:
      - id: active_window_id
        type: u4
        if: (fields_present_flags & 0x00000020) != 0
        doc: |
          Optional. An unsigned 32-bit integer. The ID of the currently active window on the server.
          This field is present if and only if the WINDOW_ORDER_FIELD_DESKTOP_ACTIVEWND flag is set
          in the FieldsPresentFlags field of the TS_DESKTOP_ORDER_HEADER packet (section 2.2.1.3.3.1).
      - id: num_windows_ids
        type: u1
        if: (fields_present_flags & 0x00000010) != 0
        doc: |
          Optional. An unsigned 8-bit integer. The number of top-level windows on the server. This field
          is present if and only if the WINDOW_ORDER_FIELD_DESKTOP_ZORDER flag is set in the
          FieldsPresentFlags field of the TS_DESKTOP_ORDER_HEADER packet (section 2.2.1.3.3.1).
      - id: window_ids
        type: u4
        repeat: expr
        repeat-expr: num_windows_ids
        if: (fields_present_flags & 0x00000010) != 0
        doc: |
          Variable length. An array of 4-byte window IDs, corresponding to the IDs of the top-level
          windows on the server, ordered by their Z-order on the server. The number of window IDs in
          the array is equal to the value of the NumWindowIds field.
          This field is present if and only if the NumWindowIds field is greater than 0 and the
          WINDOW_ORDER_FIELD_DESKTOP_ZORDER flag is set in the FieldsPresentFlags field of the
          TS_DESKTOP_ORDER_HEADER packet (section 2.2.1.3.3.1).
    enums:
      window_order_field:
        0x00000002:
          id: desktop_hooked
          doc: |
            Indicates that the server will be sending information for the server's current input desktop.
        0x00000008:
          id: desktop_arc_began
          doc: |
            Indicates that the server is beginning to synchronize information with the client after the
            client has auto-reconnected or the server has just begun monitoring a new desktop. If this
            flag is set, the WINDOW_ORDER_FIELD_DESKTOP_HOOKED flag MUST also be set.
        0x00000004:
          id: desktop_arc_completed
          doc: |
            Indicates that the server has finished synchronizing data after the client has auto-reconnected
            or the server has just begun monitoring a new desktop. The client SHOULD assume that any window
            or shell notification icon not received during the synchronization is discarded. This flag MUST
            only be combined with the WINDOW_ORDER_TYPE_DESKTOP flag.
        0x00000020:
          id: desktop_activewnd
          doc: Indicates that the ActiveWindowId field is present.
        0x00000010:
          id: desktop_zorder
          doc: |
            Indicates that the NumWindowIds field is present. If the NumWindowIds field has a value greater
            than 0, the WindowIds field MUST also be present.

  ts_desktop_order_non_monitored:
    doc: |
      MS-RDPERP 2.2.1.3.3.2.2 Non-Monitored Desktop (TS_DESKTOP_ORDER_NON_MONITORED)
      The Non-Monitored Desktop packet is generated by the server when it is not actively monitoring
      the current desktop on the server.
      mstscax!RdpWindowPlugin::DecodeDesktopInformation
    params:
      - id: fields_present_flags
        type: u4
        doc: |
          An unsigned 32-bit integer. The flags indicating which fields are present
          in the packet. See Orders.
