meta:
  id: rfx_avc444v2_bitmap_stream
  endian: le
  imports:
    - rfx_avc420_bitmap_stream

doc: |
  MS-RDPEGFX 2.2.4.6 RFX_AVC444V2_BITMAP_STREAM
  The RFX_AVC444V2_BITMAP_STREAM structure encapsulates regions of a graphics frame compressed
  using MPEG-4 AVC/H.264 compression techniques [ITU-H.264-201201] in YUV444v2 mode. The data
  compressed using these techniques is transported in the bitmapData field of the
  RDPGFX_WIRE_TO_SURFACE_PDU_1 (section 2.2.2.1) message.
  To send all the chroma data for a YUV444 frame, two RFX_AVC420_BITMAP_STREAM structures
  (section 2.2.4.4) are used. The format of the RFX_AVC444V2_BITMAP_STREAM structure is a
  four-byte integer that specifies which subframes are encoded, and the size of the first YUV420p
  subframe encoded bitstream, followed by the first subframe, and then, optionally, the second
  subframe. These bitstreams MUST be encoded using the same MPEG-4 AVC/H.264 encoder and decoded
  by a single MPEG-4 AVC/H.264 decoder as one stream. The method to combine the two streams is
  detailed in section 3.3.8.3.3. Note that the YUV420 and Chroma420 views (as shown in the figure
  captioned "A representation of a YUV444 frame as two YUV240p frames" in section 3.3.8.3.3) both
  have identical MPEG-4 AVC/H.264 bitstream formats.
  Note that the RFX_AVC444V2_BITMAP_STREAM structure is identical to the RFX_AVC444_BITMAP_STREAM
  structure except for the combination method of the YUV420 and Chroma420 views.
  mstscax!OffscreenSurface::DecodeBytesToSurfaceRegion
  mstscax!RdpSurfaceDecoder::DecodeToSurfaceTexture2D
  mstscax!RdpX_CreateObject
  mstscax!XObjectId_RdpXAvc444v2Decoder_CreateObject
  mstscax!HWCompatibilityDecodeTests::CheckHWDecodeCompatibility
  mstscax!Avc420Decompressor::InitializeDecoder
  mstscax!Avc420Decompressor::CreateAlphaTexture
  mstscax!Avc444Decompressor::Decompress
  mstscax!AvcDecompressor::DecompressHelper
  mstscax!Avc444Decompressor::DecompressDXVA (if DXVA enabled)
  mstscax!Avc444Decompressor::DecompressCPUTexture (if no byte array interface found)
  mstscax!Avc444Decompressor::DecompressCPUByteArray (if byte array interface found)
  mstscax!Avc444Decompressor::DecompressCPUCommon
  mstscax!Avc444Decompressor::GetRectangles

seq:
  - id: avc420_encoded_bitstream_info
    type: u4
    doc: |
      A 32-bit unsigned integer that specifies the size of the data present in the
      avc420EncodedBitstream1 field and which subframes are encoded.
  - id: yuy420_avc420_encoded_bitstream
    size: avc420_encoded_bitstream_1_size
    type: rfx_avc420_bitmap_stream
    if: '(lc == lc::yuy420_chroma420) or (lc == lc::yuy420)'
    doc: |
      An RFX_AVC420_BITMAP_STREAM structure that contains the first YUV420p subframe of a single
      frame that was encoded using the MPEG-4 AVC/H.264 codec in YUV444v2 mode.
  - id: chroma420_avc420_encoded_bitstream
    size-eos: true
    type: rfx_avc420_bitmap_stream
    if: '(lc == lc::yuy420_chroma420) or (lc == lc::chroma420)'
    doc: |
      An RFX_AVC420_BITMAP_STREAM structure that contains the first YUV420p subframe of a single
      frame that was encoded using the MPEG-4 AVC/H.264 codec in YUV444v2 mode.

instances:
  lc:
    value: (avc420_encoded_bitstream_info >> 30) & 0x3
    enum: lc
    doc: |
      A 2-bit unsigned integer that specifies how data is encoded in the avc420EncodedBitstream1 and
      avc420EncodedBitstream2 fields.
  avc420_encoded_bitstream_1_size:
    value: avc420_encoded_bitstream_info & 0x3fffffff
    doc: |
      A 30-bit unsigned integer that specifies the size, in bytes, of the luma frame present in the
      avc420EncodedBitstream1 field. If no YUV420 frame is present, then this field MUST be set to
      zero.

enums:
  lc:
    0x0:
      id: yuy420_chroma420
      doc: |
        A YUV420 frame is contained in the avc420EncodedBitstream1 field, and a Chroma420 frame is
        contained in the avc420EncodedBitstream2 field.
    0x1:
      id: yuy420
      doc: |
        A YUV420 frame is contained in the avc420EncodedBitstream1 field, and no data is present in the
        avc420EncodedBitstream2 field. No Chroma420 frame is present. The Chroma420 frame corresponding
        to the updates in the YUV420 frame is sent in a RFX_AVC444_BITMAP_STREAM message in subsequent
        frames if required.
    0x2:
      id: chroma420
      doc: |
        A Chroma420 frame is contained in the avc420EncodedBitstream1 field, and no data is present in
        the avc420EncodedBitstream2 field. No YUV420 frame is present. The Chroma420 frame MUST be
        combined with the decoded AVC stream from previous frames.
    0x3:
      id: invalid
      doc: |
        An invalid state that MUST NOT occur.
