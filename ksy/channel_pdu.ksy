meta:
  id: channel_pdu
  endian: le
  imports:
    - rdp_raw_pdu
    - rdpdr_pdu
    - ts_rail_pdu
    - ts_rail_wi_pdu
    - ts_rail_ri_pdu
    - cliprdr_pdu
    - dynvc_pdu

doc: |
  MS-RDPBCGR 2.2.6.1.1 Channel PDU Header (CHANNEL_PDU_HEADER)
  The CHANNEL_PDU_HEADER MUST precede all opaque static virtual channel
  traffic chunks transmitted via RDP between a client and server.
  mstscax!CChan::IntChannelSendWorker
  mstscax!CSL::SLReceivedDataPacket
  mstscax!CChan::ChannelOnPacketReceived

params:
  - id: to_server
    type: bool
  - id: channel_type
    type: u1

seq:
  - id: length
    type: u4
    doc: |
      A 32-bit, unsigned integer. The total length in bytes of the
      uncompressed channel data, excluding this header. The data can
      span multiple Virtual Channel PDUs and the individual chunks
      will need to be reassembled in that case (section 3.1.5.2.2).
  - id: flags
    type: u4
    doc:
      A 32-bit, unsigned integer. The channel control flags.
  - id: channel_data
    size-eos: true
    process: msrdp_static_channel_data(_root)
    type: channel_data(to_server, channel_type)

instances:
  is_fragment:
    value: (flags & 0x3) != 0x3
    doc: |
      If neither the CHANNEL_FLAG_FIRST (0x00000001) nor the
      CHANNEL_FLAG_LAST (0x00000002) flag is present, the chunk
      is from the middle of a sequence.
  is_compressed:
    value: (flags & 0x00200000) != 0
    doc: |
      The virtual channel data is compressed. This flag is
      equivalent to MPPC bit C (for more information see
      [RFC2118] section 3.1).
  compression_type:
    value: ((flags & 0x000F0000) >> 16) & 0xf
    enum: packet_compr
    doc: |
      Indicates the compression package which was used to compress the data.
  compression_packed_at_front:
    value: (flags & 0x00400000) != 0
    doc: |
      The decompressed packet MUST be placed at the beginning of the history
      buffer. This flag is equivalent to MPPC bit B (for more information see
      [RFC2118] section 3.1).
  compression_packed_flushed:
    value: (flags & 0x00800000) != 0
    doc: |
      The decompressor MUST reinitialize the history buffer (by filling it
      with zeros) and reset the HistoryOffset to zero. After it has been reinitialized,
      the entire history buffer is immediately regarded as valid. This flag is
      equivalent to MPPC bit A (for more information see [RFC2118] section 3.1). If
      the CHANNEL_PACKET_COMPRESSED (0x00200000) flag is also present, then the
      CHANNEL_PACKET_FLUSHED flag MUST be processed first.
  uncompressed_size:
    value: length
    doc: |
      A 32-bit, unsigned integer. The total length in bytes of the
      uncompressed channel data, excluding this header. The data can
      span multiple Virtual Channel PDUs and the individual chunks
      will need to be reassembled in that case (section 3.1.5.2.2).
  first_fragment:
    value: (flags & 0x00000001) != 0
  last_fragment:
    value: (flags & 0x00000002) != 0

enums:
  channel_type:
    0: raw
    1: security_layer
    2: slowpath
    3: rdpdr
    4: rail
    5: rail_wi
    6: rail_ri
    7: cliprdr
    8: drdynvc
  channel_flag:
    0x00000001:
      id: first
      doc: |
        Indicates that the chunk is the first in a sequence.
    0x00000002:
      id: last
      doc: |
        Indicates that the chunk is the last in a sequence.
    0x00000010:
      id: show_protocol
      doc: |
        The Channel PDU Header MUST be visible to the application endpoint
        (section 2.2.1.3.4.1).
    0x00000020:
      id: suspend
      doc: |
        All virtual channel traffic MUST be suspended. This flag is only
        valid in server-to-client virtual channel traffic. It MUST be ignored
        in client-to-server data.
    0x00000040:
      id: resume
      doc: |
        All virtual channel traffic MUST be resumed. This flag is only valid
        in server-to-client virtual channel traffic. It MUST be ignored in
        client-to-server data.
    0x00000080:
      id: shadow_persistent
      doc: |
        This flag is unused and its value MUST be ignored by the client and server.
    0x00200000:
      id: packet_compressed
      doc: |
        The virtual channel data is compressed. This flag is equivalent to MPPC
        bit C (for more information see [RFC2118] section 3.1).
    0x00400000:
      id: packet_at_front
      doc: |
        The decompressed packet MUST be placed at the beginning of the history
        buffer. This flag is equivalent to MPPC bit B (for more information see
        [RFC2118] section 3.1).
    0x00800000:
      id: packet_flushed
      doc: |
        The decompressor MUST reinitialize the history buffer (by filling it with
        zeros) and reset the HistoryOffset to zero. After it has been reinitialized,
        the entire history buffer is immediately regarded as valid. This flag is
        equivalent to MPPC bit A (for more information see [RFC2118] section 3.1).
        If the CHANNEL_PACKET_COMPRESSED (0x00200000) flag is also present, then the
        CHANNEL_PACKET_FLUSHED flag MUST be processed first.
  packet_compr:
    0x0:
      id: type_8k
      doc: |
        RDP 4.0 bulk compression (section 3.1.8.4.1).
    0x1:
      id: type_64k
      doc: |
        RDP 5.0 bulk compression (section 3.1.8.4.2).
    0x2:
      id: type_rdp6
      doc: |
        RDP 6.0 bulk compression ([MS-RDPEGDI] section 3.1.8.1).
    0x3:
      id: type_rdp61
      doc: |
        RDP 6.1 bulk compression ([MS-RDPEGDI] section 3.1.8.2).

types:
  channel_data:
    params:
      - id: to_server
        type: bool
      - id: channel_type
        type: u1
        enum: channel_type
    seq:
      - id: data
        size-eos: true
        type:
          switch-on: channel_type
          -name: type
          cases:
            'channel_type::raw.to_i': rdp_raw_pdu
            'channel_type::rdpdr.to_i': rdpdr_pdu
            'channel_type::rail.to_i': ts_rail_pdu
            'channel_type::rail_wi.to_i': ts_rail_wi_pdu
            'channel_type::rail_ri.to_i': ts_rail_ri_pdu
            'channel_type::cliprdr.to_i': cliprdr_pdu
            'channel_type::drdynvc.to_i': dynvc_pdu(to_server, 0)
