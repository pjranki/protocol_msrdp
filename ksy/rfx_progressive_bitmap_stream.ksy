meta:
  id: rfx_progressive_bitmap_stream
  endian: le

doc: |
  MS-RDPEGFX 2.2.4.2 RFX_PROGRESSIVE_BITMAP_STREAM
  The RFX_PROGRESSIVE_BITMAP_STREAM structure encapsulates regions of a graphics frame compressed
  using discrete wavelet transforms (DWTs), sub-band diffing, and progressive compression techniques.
  The data compressed using these techniques is transported in the bitmapData field of the
  RDPGFX_WIRE_TO_SURFACE_PDU_2 (section 2.2.2.2) message.
  mstscax!CaProgressiveDecompressor::Decode
  mstscax!CaDecProgressiveRectContext::Decode
  mstscax!CacNx::SurfaceDecoderCpu::Decode
  mstscax!CacNx::SurfaceDecoder::Decode
  mstscax!CacNx::DecodingEngine::Decode
  mstscax!CacNx::DecoderImpl::processStream
  mstscax!CaProgressiveDecompressor::getDecodedBits

seq:
  - id: progressive_data_blocks
    type: rfx_progressive_datablock
    repeat: eos
    doc: |
      A variable-length array of RFX_PROGRESSIVE_DATABLOCK (section 2.2.4.2.1) structures.

types:
  rfx_rect:
    doc: |
        MS-RDPRFX 2.2.2.1.6 TS_RFX_RECT
        The TS_RFX_RECT structure is used to specify a rectangle.
    seq:
      - id: x
        type: u2
        doc: |
          A 16-bit, unsigned integer. The X-coordinate of the rectangle.
      - id: y
        type: u2
        doc: |
          A 16-bit, unsigned integer. The Y-coordinate of the rectangle.
      - id: width
        type: u2
        doc: |
          A 16-bit, unsigned integer. The width of the rectangle.
      - id: height
        type: u2
        doc: |
          A 16-bit, unsigned integer. The height of the rectangle.

  rfx_component_codec_quant:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5.2 RFX_COMPONENT_CODEC_QUANT
      The RFX_COMPONENT_CODEC_QUANT structure stores information regarding the scalar quantization
      values for the ten sub-bands in the three-level discrete wavelet transform (DWT)
      decomposition.
      When embedded within the quantVals field of the RFX_PROGRESSIVE_REGION (section 2.2.4.2.1.5)
      structure, the RFX_COMPONENT_CODEC_QUANT structure contains the scalar quantization values.
      Each field in this structure MUST have a value in the range of 0 to 15 (inclusive).
      When embedded within the yQuantValues, cbQuantValues, and crQuantValues fields of the
      RFX_PROGRESSIVE_CODEC_QUANT (section 2.2.4.2.1.5.1) structure, the RFX_COMPONENT_CODEC_QUANT
      structure contains values to be added to the quantization values specified in the quantVals
      field of the RFX_PROGRESSIVE_REGION structure. Each field in this structure MUST have a value
      in the range of 0 to 8 (inclusive).
      Note that the RFX_COMPONENT_CODEC_QUANT structure differs from the TS_RFX_CODEC_QUANT
      ([MS-RDPRFX] section 2.2.2.1.5) structure with respect to the order of the bands.
    seq:
      - id: hl3
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factors for the level-3 DWT sub-band.
      - id: ll3
        type: b4
        doc: A 4-bit, unsigned integer. The LL quantization factor for the level-3 DWT sub-band.
      - id: hh3
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-3 DWT sub-band.
      - id: lh3
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-3 DWT sub-band.
      - id: lh2
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-2 DWT sub-band.
      - id: hl2
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factor for the level-2 DWT sub-band.
      - id: hl1
        type: b4
        doc: A 4-bit, unsigned integer. The HL quantization factor for the level-1 DWT sub-band.
      - id: hh2
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-2 DWT sub-band.
      - id: hh1
        type: b4
        doc: A 4-bit, unsigned integer. The HH quantization factor for the level-1 DWT sub-band.
      - id: lh1
        type: b4
        doc: A 4-bit, unsigned integer. The LH quantization factor for the level-1 DWT sub-band.

  rfx_progressive_codec_quant:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5.1 RFX_PROGRESSIVE_CODEC_QUANT
      The RFX_PROGRESSIVE_CODEC_QUANT structure specifies a progressive quantization table for
      compressing a tile.
    seq:
      - id: quality
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the quality associated with the progressive
          stage as a value between 0 (0x00) and 100 (0x64), where 100 (0x64) indicates that the
          tile will reach its final target quality. This value SHOULD be ignored by the decoder.
      - id: y_quant_values
        type: rfx_component_codec_quant
        doc: |
          An RFX_COMPONENT_CODEC_QUANT (section 2.2.4.2.1.5.2) structure that contains the
          progressive quantization table for the Luma (Y) component.
      - id: cb_quant_value
        type: rfx_component_codec_quant
        doc: |
          An RFX_COMPONENT_CODEC_QUANT structure that contains the progressive quantization table
          for the Chroma Blue (Cb) component.
      - id: cr_quant_value
        type: rfx_component_codec_quant
        doc: |
          An RFX_COMPONENT_CODEC_QUANT structure that contains the progressive quantization table
          for the Chroma Red (Cr) component.

  rfx_progressive_datablock:
    doc: |
      MS-RDPEGFX 2.2.4.2.1 RFX_PROGRESSIVE_DATABLOCK
      The RFX_PROGRESSIVE_DATABLOCK structure is used to wrap data sent from the server to the client.
      All RemoteFX Progressive data blocks conform to this basic structure and are specified in
      sections 2.2.4.2.1.1 through 2.2.4.2.1.5.5.
      mstscax!CacNx::DecoderImpl::processStream
    seq:
      - id: block_type
        type: u2
        enum: wbt
        doc: |
          A 16-bit unsigned integer that specifies the block type. This field MUST be set to one of
          the following values. If this field is not set to one of the specified values, the decoder
          SHOULD ignore the contents of the blockLen and blockData fields.
      - id: block_len
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the combined size, in bytes, of the blockType,
          blockLen and blockData fields.
      - id: block_data
        size: block_data_size
        doc: mstscax!CacNx::WfParser::GetNextBlock
        type:
          switch-on: block_type
          -name: type
          cases:
            'wbt::sync': rfx_progressive_sync
            'wbt::frame_begin': rfx_progressive_frame_begin
            'wbt::frame_end': rfx_progressive_frame_end
            'wbt::context': rfx_progressive_context
            'wbt::region': rfx_progressive_region
            'wbt::tile_simple': rfx_progressive_tile_simple
            'wbt::tile_progressive_first': rfx_progressive_tile_first
            'wbt::tile_progressive_upgrade': rfx_progressive_tile_upgrade
    instances:
      block_data_size:
        value: 'block_len >= 6 ? block_len - 6 : 0'
    enums:
      wbt:
        0xCCC0:
          id: sync
          doc: RFX_PROGRESSIVE_SYNC (section 2.2.4.2.1.1)
        0xCCC1:
          id: frame_begin
          doc: RFX_PROGRESSIVE_FRAME_BEGIN (section 2.2.4.2.1.2)
        0xCCC2:
          id: frame_end
          doc: RFX_PROGRESSIVE_FRAME_END (section 2.2.4.2.1.3)
        0xCCC3:
          id: context
          doc: RFX_PROGRESSIVE_CONTEXT (section 2.2.4.2.1.4)
        0xCCC4:
          id: region
          doc: RFX_PROGRESSIVE_REGION (section 2.2.4.2.1.5)
        0xCCC5:
          id: tile_simple
          doc: RFX_PROGRESSIVE_TILE_SIMPLE (section 2.2.4.2.1.5.3)
        0xCCC6:
          id: tile_progressive_first
          doc: RFX_PROGRESSIVE_TILE_FIRST (section 2.2.4.2.1.5.4)
        0xCCC7:
          id: tile_progressive_upgrade
          doc: RFX_PROGRESSIVE_TILE_UPGRADE (section 2.2.4.2.1.5.5)

  rfx_progressive_sync:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.1 RFX_PROGRESSIVE_SYNC
      The RFX_PROGRESSIVE_SYNC structure is used to transport codec version information. It is optional
      and SHOULD appear only once as the first block in the progressiveDataBlocks field of the
      encapsulating RFX_PROGRESSIVE_BITMAP_STREAM (section 2.2.4.2) structure. If this block appears
      out of sequence, the decoder SHOULD ignore it.
      mstscax!CacNx::DecoderImpl::processStream
    seq:
      - id: magic
        type: u4
        doc: |
          A 32-bit unsigned integer that SHOULD be set to 0xCACCACCA. The decoder SHOULD ignore this
          value.
      - id: version
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the version of the codec. The upper 8 bits indicate
          the major version number, while the lower 8 bits indicate the minor version number. The
          current version of the wire format is 1.0 (encoded as 0x0100). The decoder SHOULD ignore this
          value.

  rfx_progressive_frame_begin:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.2 RFX_PROGRESSIVE_FRAME_BEGIN
      The RFX_PROGRESSIVE_FRAME_BEGIN structure marks the beginning of the frame in the codec payload.
      This block MUST appear only once, before any RFX_PROGRESSIVE_REGION (section 2.2.4.2.1.5) blocks
      but after the RFX_PROGRESSIVE_CONTEXT (section 2.2.4.2.1.4) block.
      mstscax!CacNx::DecoderImpl::processStream
      mstscax!CacNx::DecoderImpl::processFrameBegin
    seq:
      - id: frame_index
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the frame index. This value SHOULD be ignored by the
          decoder.
      - id: region_count
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of RFX_PROGRESSIVE_REGION blocks that follow
          this RFX_PROGRESSIVE_FRAME_BEGIN block.
      - id: regions
        type: rfx_progressive_region
        repeat: expr
        repeat-expr: region_count
        doc: |
          An array of RFX_PROGRESSIVE_REGION (section 2.2.4.2.1.5) blocks. The number of elements in this
          array is specified by the regionCount field. If the number of elements specified by the
          regionCount field is larger than the actual number of elements in the regions field, the decoder
          SHOULD ignore this inconsistency.

  rfx_progressive_frame_end:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.3 RFX_PROGRESSIVE_FRAME_END
      The RFX_PROGRESSIVE_FRAME_END structure marks the end of the frame in the codec payload. This block
      SHOULD appear only once, after the final RFX_PROGRESSIVE_REGION (section 2.2.4.2.1.5) block. If this
      block appears more than once, the decoder SHOULD ignore the other occurrences.
      mstscax!CacNx::DecoderImpl::processStream

  rfx_progressive_context:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.4 RFX_PROGRESSIVE_CONTEXT
      The RFX_PROGRESSIVE_CONTEXT structure provides information about the compressed data. It is optional
      and SHOULD appear before the RFX_PROGRESSIVE_FRAME_BEGIN (section 2.2.4.2.1.2) block. If the block
      appears after the RFX_PROGRESSIVE_FRAME_BEGIN block, the decoder SHOULD process it.
      mstscax!CacNx::DecoderImpl::processStream
    seq:
      - id: ctx_id
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the context ID. This field SHOULD be set to 0x00. The
          decoder SHOULD ignore this value.
      - id: tile_size
        type: u2
        doc: |
          A 16-bit unsigned integer that indicates the width and height of a square tile. This field MUST
          be set to 0x0040.
      - id: flags
        type: u1
        doc: |
          An 8-bit unsigned integer that contains context flags.
          RFX_SUBBAND_DIFFING (0x01) - Indicates that sub-band diffing is enabled.

  rfx_progressive_region:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5 RFX_PROGRESSIVE_REGION
      The RFX_PROGRESSIVE_REGION structure contains the compressed data for a set of tiles from the frame.
      All RFX_PROGRESSIVE_REGION blocks SHOULD be present between the RFX_PROGRESSIVE_FRAME_BEGIN
      (section 2.2.4.2.1.2) and RFX_PROGRESSIVE_FRAME_END (section 2.2.4.2.1.3) blocks. If a block is not
      present between the RFX_PROGRESSIVE_FRAME_BEGIN and RFX_PROGRESSIVE_FRAME_END blocks, the decoder
      MUST ignore it.
      Note that RFX_PROGRESSIVE_REGION entries that are part of the same frame can share the tiles defined
      in the tiles field of each entry. In this scenario, tiles are not repeated in successive
      RFX_PROGRESSIVE_REGION entries. Across all of the RFX_PROGRESSIVE_REGION entries of a frame, the
      rectangles (defined in the rects field of each entry) MUST be distinct, and the region defined by
      these rectangles MUST be completely covered by all of the tiles defined in the RFX_PROGRESSIVE_REGION
      entries of the frames. Note that in this context, the frame is bracketed between the
      RDPGFX_START_FRAME_PDU and the RDPGFX_END_FRAME_PDU, and can span multiple RFX_PROGRESSIVE_FRAME_BEGIN
      and RFX_PROGRESSIVE_FRAME_END blocks.
      mstscax!CacNx::DecoderImpl::processStream
      mstscax!CacNx::DecoderImpl::processRegion
      mstscax!CacNx::DecoderImpl::HandleRegion
      mstscax!CacInvXformNx::IDwtCpu::PreHandleRegion
      mstscax!CacInvXformNx::FullTileBitField::AddRects
      mstscax!CacInvXformNx::FullTileBitField::addFullTileFromRect
      mstscax!CacNx::DecodingThreadManager::ProcessFrame
      mstscax!CacNx::DecodingThreadContext::ProcessNextTile
      mstscax!CacDecodingNx::Decoding::UnRlgr2LnTiles3V10_threadsafe
      mstscax!CacInvXformNx::IDwtCpu::CopyTile
    seq:
      - id: tile_size
        type: u1
        doc: |
          An 8-bit unsigned integer that indicates the width and height of a square tile. This field MUST
          be set to 0x40.
      - id: num_rects
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of TS_RFX_RECT ([MS-RDPRFX] section 2.2.2.1.6)
          structures in the rects field. The value of this field MUST be greater than zero.
      - id: num_quant
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the number of RFX_COMPONENT_CODEC_QUANT
          (section 2.2.4.2.1.5.2) structures in the quantVals field. The value of this field MUST be in the
          range 0 to 7 (inclusive).
      - id: num_prog_quant
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies the number of RFX_PROGRESSIVE_CODEC_QUANT
          (section 2.2.4.2.1.5.1) structures in the quantProgVals field.
      - id: flags
        type: u1
        doc: |
          An 8-bit unsigned integer that contains region flags.
          RFX_DWT_REDUCE_EXTRAPOLATE (0x01) - Indicates that the discrete wavelet transform (DWT) uses the
                                              "Reduce-Extrapolate" method.
      - id: num_tiles
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the number of elements in the tiles field.
      - id: tile_data_size
        type: u4
        doc: |
          A 32-bit unsigned integer that specifies the size, in bytes, of the tiles field.
      - id: rects
        type: rfx_rect
        repeat: expr
        repeat-expr: num_rects
        doc: |
          A variable-length array of TS_RFX_RECT structures that specifies the encoded region (the number of
          rectangles in this field is specified by the numRects field). This region MUST be completely covered
          by the tiles enumerated in the tiles field of this RFX_PROGRESSIVE_REGION entry and by tiles that
          were specified in RFX_PROGRESSIVE_REGION entries that previously appeared within the current frame.
          Note that because regions are not necessarily tile-aligned, it is valid for tiles to carry compressed
          information for pixels outside of the region.
      - id: quant_vals
        type: rfx_component_codec_quant
        repeat: expr
        repeat-expr: num_quant
        doc: |
          A variable-length array of RFX_COMPONENT_CODEC_QUANT structures (the number of quantization tables in
          this field is specified by the numQuant field).
      - id: quant_prog_vals
        type: rfx_progressive_codec_quant
        repeat: expr
        repeat-expr: num_prog_quant
        doc: |
          A variable-length array of RFX_PROGRESSIVE_CODEC_QUANT structures (the number of quantization tables
          in this field is specified by the numProgQuant field).
      - id: tiles
        type: rfx_progressive_datablock
        repeat: expr
        repeat-expr: num_tiles
        doc: |
          A variable-length array of RFX_PROGRESSIVE_DATABLOCK (section 2.2.4.2.1) structures. The value of the
          blockType field of each block present in the array MUST be:
           - WBT_TILE_SIMPLE (0xCCC5),
           - WBT_TILE_PROGRESSIVE_FIRST (0xCCC6)
           - WBT_TILE_PROGRESSIVE_UPGRADE (0xCCC7).

  rfx_progressive_tile_simple:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5.3 RFX_PROGRESSIVE_TILE_SIMPLE
      The RFX_PROGRESSIVE_TILE_SIMPLE structure specifies a tile that has been compressed without progressive
      techniques.
      mstscax!CacNx::DecoderImpl::processRegion
      mstscax!CacDecodingNx::Decoding::UnRlgr2LnTiles3V10_threadsafe
    seq:
      - id: quant_idx_y
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT
          (section 2.2.4.2.1.5.2) array (the quantVals field) of the containing RFX_PROGRESSIVE_REGION
          (section 2.2.4.2.1.5) block. The specified quantization table MUST be used for de-quantization of the
          sub-bands for the Luma (Y) component.
      - id: quant_idx_cb
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Blue (Cb) component.
      - id: quant_idx_cr
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Red (Cr) component.
      - id: x_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the x-index of the encoded tile in the screen tile grid. The
          pixel x-coordinate is obtained by multiplying the x-index by the size of the tile.
      - id: y_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the y-index of the encoded tile in the screen tile grid. The
          pixel y-coordinate is obtained by multiplying the y-index by the size of the tile.
      - id: flags
        type: u1
        doc: |
          An 8-bit unsigned integer that contains tile flags.
          RFX_TILE_DIFFERENCE (0x01) - Indicates that the tile contains the compressed difference of the DWT
                                       coefficients for the same tile between the current frame and the previous
                                       frame.
      - id: y_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the yData field.
      - id: cb_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the cbData field.
      - id: cr_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the crData field.
      - id: tail_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the tailData field. This field SHOULD<1>
          be set to zero.
      - id: y_data
        size: y_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Luma (Y) component of the tile
          using, a discrete wavelet transform (DWT), sub-band diffing if enabled, and quantization and entropy
          encoded using the RLGR1 method. The size of this field, in bytes, is specified by the yLen field.
      - id: cb_data
        size: cb_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Chroma Blue (Cb) component of
          the tile using the same methods as the yData field. The size of this field, in bytes, is specified by the
          cbLen field.
      - id: cr_data
        size: cr_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Chroma Red (Cr) component of
          the tile using the same methods as the yData field. The size of this field, in bytes, is specified by the
          crLen field.
      - id: tail_data
        size: tail_len
        doc: |
          A variable-length array of bytes that contains data that SHOULD<2> be ignored. The size of this field, in
          bytes, is specified by the tailLen field.

  rfx_progressive_tile_first:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5.4 RFX_PROGRESSIVE_TILE_FIRST
      The RFX_PROGRESSIVE_TILE_FIRST structure specifies the first-pass compression of a tile with progressive
      techniques. Subsequent passes, which improve the quality of the tile, are specified using
      the RFX_PROGRESSIVE_TILE_UPGRADE (section 2.2.4.2.1.5.5) block.
      mstscax!CacNx::DecoderImpl::processRegion
      mstscax!CacDecodingNx::Decoding::UnRlgr2LnTiles3V10_threadsafe
    seq:
      - id: quant_idx_y
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT
          (section 2.2.4.2.1.5.2) array (the quantVals field) of the containing RFX_PROGRESSIVE_REGION
          (section 2.2.4.2.1.5) block. The specified quantization table MUST be used for de-quantization of the
          sub-bands for the Luma (Y) component.
      - id: quant_idx_cb
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Blue (Cb) component.
      - id: quant_idx_cr
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Red (Cr) component.
      - id: x_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the x-index of the encoded tile in the screen tile grid. The
          pixel x-coordinate is obtained by multiplying the x-index by the size of the tile.
      - id: y_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the y-index of the encoded tile in the screen tile grid. The
          pixel y-coordinate is obtained by multiplying the y-index by the size of the tile.
      - id: flags
        type: u1
        doc: |
          An 8-bit unsigned integer that contains tile flags.
          RFX_TILE_DIFFERENCE (0x01) - Indicates that the tile contains the compressed difference of the DWT
                                       coefficients for the same tile between the current frame and the previous
                                       frame.
      - id: progressive_quality
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_PROGRESSIVE_CODEC_QUANT
          (section 2.2.4.2.1.5.1) array (the quantProgVals field) of the containing RFX_PROGRESSIVE_REGION block.
          A value of 255 (0xFF) indicates a full progressive quality table (the quality is 100%, and all the
          coefficients are zero).
      - id: y_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the yData field.
      - id: cb_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the cbData field.
      - id: cr_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the crData field.
      - id: tail_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the tailData field. This field SHOULD<1>
          be set to zero.
      - id: y_data
        size: y_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Luma (Y) component of the tile
          using, a discrete wavelet transform (DWT), sub-band diffing if enabled, and quantization and entropy
          encoded using the RLGR1 method. The size of this field, in bytes, is specified by the yLen field.
      - id: cb_data
        size: cb_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Chroma Blue (Cb) component of
          the tile using the same methods as the yData field. The size of this field, in bytes, is specified by the
          cbLen field.
      - id: cr_data
        size: cr_len
        doc: |
          A variable-length array of bytes that contains the compressed data for the Chroma Red (Cr) component of
          the tile using the same methods as the yData field. The size of this field, in bytes, is specified by the
          crLen field.
      - id: tail_data
        size: tail_len
        doc: |
          A variable-length array of bytes that contains data that SHOULD<2> be ignored. The size of this field, in
          bytes, is specified by the tailLen field.

  rfx_progressive_tile_upgrade:
    doc: |
      MS-RDPEGFX 2.2.4.2.1.5.5 RFX_PROGRESSIVE_TILE_UPGRADE
      The RFX_PROGRESSIVE_TILE_UPGRADE structure contains data required for an upgrade pass of a tile using progressive
      techniques. The block contains information that MUST be added to the information currently stored by the decoder
      in order to increase the quality of the tile.
      mstscax!CacNx::DecoderImpl::processRegion
      mstscax!CacDecodingNx::Decoding::UnRlgr2LnTiles3V10_threadsafe
      - id: quant_idx_y
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT
          (section 2.2.4.2.1.5.2) array (the quantVals field) of the containing RFX_PROGRESSIVE_REGION
          (section 2.2.4.2.1.5) block. The specified quantization table MUST be used for de-quantization of the
          sub-bands for the Luma (Y) component.
      - id: quant_idx_cb
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Blue (Cb) component.
      - id: quant_idx_cr
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_COMPONENT_CODEC_QUANT array (the
          quantVals field) of the containing RFX_PROGRESSIVE_REGION block. The specified quantization table MUST
          be used for de-quantization of the sub-bands for the Chroma Red (Cr) component.
      - id: x_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the x-index of the encoded tile in the screen tile grid. The
          pixel x-coordinate is obtained by multiplying the x-index by the size of the tile.
      - id: y_idx
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the y-index of the encoded tile in the screen tile grid. The
          pixel y-coordinate is obtained by multiplying the y-index by the size of the tile.
      - id: flags
        type: u1
        doc: |
          An 8-bit unsigned integer that contains tile flags.
          RFX_TILE_DIFFERENCE (0x01) - Indicates that the tile contains the compressed difference of the DWT
                                       coefficients for the same tile between the current frame and the previous
                                       frame.
      - id: progressive_quality
        type: u1
        doc: |
          An 8-bit unsigned integer that specifies an index into the RFX_PROGRESSIVE_CODEC_QUANT
          (section 2.2.4.2.1.5.1) array (the quantProgVals field) of the containing RFX_PROGRESSIVE_REGION block.
          A value of 255 (0xFF) indicates a full progressive quality table (the quality is 100%, and all the
          coefficients are zero).
      - id: y_srl_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the ySrlData field.
      - id: y_raw_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the yRawData field.
      - id: cb_srl_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the cbSrlData field.
      - id: cb_raw_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the cbRawData field.
      - id: cr_srl_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the crSrlData field.
      - id: cr_raw_len
        type: u2
        doc: |
          A 16-bit unsigned integer that specifies the size, in bytes, of the crRawData field.
      - id: y_srl_data
        size: y_srl_len
        doc: |
          A variable-length array of bytes that contains bits for the Luma (Y) component compressed using the
          Simplified-RL method.
      - id: y_raw_data
        size: y_raw_len
        doc: |
          A variable-length array of bytes that contains raw bits for the Luma (Y) component.
      - id: cb_srl_data
        size: cb_srl_len
        doc: |
          A variable-length array of bytes that contains bits for the Chroma Blue (Cb) component compressed
          using the Simplified-RL method.
      - id: cb_raw_data
        size: cb_raw_len
        doc: |
          A variable-length array of bytes that contains raw bits for the Chroma Blue (Cb) component.
      - id: cr_srl_data
        size: cr_srl_len
        doc: |
          A variable-length array of bytes that contains bits for the Chroma Red (Cr) component compressed
          using the Simplified-RL method.
      - id: cr_raw_data
        size: cr_raw_len
        doc: |
          A variable-length array of bytes that contains raw bits for the Chroma Red (Cr) component.
