#ifndef _H_MSRDP_STATIC_CHANNEL_TYPE_CPP_H_
#define _H_MSRDP_STATIC_CHANNEL_TYPE_CPP_H_

#include <cstdint>
#include <string>
#include <functional>

#include <kpb_cpp_stl_io.hpp>

#include "t125_mcs_pdu.hpp"
#include "channel_pdu.hpp"

/**
 * Provide a function that can lookup the type (msrdp::ChannelPdu::ChannelType) from the channel ID.
 * The provided function must return a msrdp::ChannelPdu::ChannelType (enumerator) as a uint8_t
 */
void msrdp_set_static_channel_type_lookup(
    std::function<uint8_t(
        bool            to_server,
        uint16_t        channel_id
    )> func
);

/**
 * Convert an ASCII channel name to its type (msrdp::ChannelPdu::ChannelType)
 */
msrdp::ChannelPdu::ChannelType msrdp_static_channel_type(const char *channel_name);

/**
 * Called internally by generated KPB code
 */
class MsrdpStaticChannelType
{
    public:
        MsrdpStaticChannelType(const msrdp::T125McsPdu &_root);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        msrdp::T125McsPdu &t124_mcs_pdu_;
};

#endif
