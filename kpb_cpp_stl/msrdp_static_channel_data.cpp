#include "msrdp_static_channel_data.hpp"

#include <map>
#include <string>
#include <mutex>

static std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> g_decompress = nullptr;

void msrdp_set_static_channel_decompress_function(
    std::function<bool(
        const std::string & data,
        bool                to_server,
        uint8_t             compression_type,
        bool                packed_at_front,
        bool                packed_flushed,
        std::string *       out_data
    )> func
)
{
    g_decompress = func;
}

static std::mutex g_fragment_mutex;
static bool g_fragment_init = false;
static std::map<uint8_t,std::string> g_fragment_to_client;
static std::map<uint8_t,size_t> g_fragment_to_client_size;
static std::map<uint8_t,std::string> g_fragment_to_server;
static std::map<uint8_t,size_t> g_fragment_to_server_size;

static void fragment_init()
{
    if ( g_fragment_init )
    {
        return;
    }
    g_fragment_init = true;
    for ( size_t i = 0; i < 0xff; i++ )
    {
        g_fragment_to_client[(uint8_t)i] = std::string("", 0);
        g_fragment_to_client_size[(uint8_t)i] = 0;
        g_fragment_to_server[(uint8_t)i] = std::string("", 0);
        g_fragment_to_server_size[(uint8_t)i] = 0;
    }
}

MsrdpStaticChannelData::MsrdpStaticChannelData(const msrdp::ChannelPdu &_root) : channel_pdu_(const_cast<msrdp::ChannelPdu&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpStaticChannelData::encode_size(kpb::IO &_io)
{
    // TODO: fragmentation and compression not implemented
    return _io.size();
}

bool MsrdpStaticChannelData::encode(kpb::IO &_io)
{
    // TODO: fragmentation and compression not implemented
    _io;
    return true;
}

bool MsrdpStaticChannelData::decode(kpb::IO &_io)
{
    if ( this->channel_pdu_.is_fragment() )
    {
        std::lock_guard<std::mutex> guard(g_fragment_mutex);
        fragment_init();
        uint8_t channel_type = this->channel_pdu_.channel_type();
        if ( channel_type == msrdp::ChannelPdu::CHANNEL_TYPE_RAW )
        {
            return true;
        }
        static std::map<uint8_t,std::string> &fragment = this->channel_pdu_.to_server() ? g_fragment_to_server : g_fragment_to_client;
        static std::map<uint8_t,size_t> &fragment_size = this->channel_pdu_.to_server() ? g_fragment_to_server_size : g_fragment_to_client_size;
        if ( this->channel_pdu_.first_fragment() )
        {
            fragment[channel_type] = _io.get();
            fragment_size[channel_type] = this->channel_pdu_.length_();
            this->channel_pdu_.set_channel_type(msrdp::ChannelPdu::CHANNEL_TYPE_RAW);
            _io.set(std::string("", 0));
            return true;
        }
        if ( this->channel_pdu_.last_fragment() )
        {
            std::string out_data = fragment[channel_type];
            //size_t out_data_size = fragment_size[channel_type];
            fragment[channel_type] = std::string("", 0);
            fragment_size[channel_type] = 0;
            _io.set(out_data);
            return true;
        }
        if ( fragment_size[channel_type] > 0 )
        {
            fragment[channel_type] = fragment[channel_type] + _io.get();
            this->channel_pdu_.set_channel_type(msrdp::ChannelPdu::CHANNEL_TYPE_RAW);
            _io.set(std::string("", 0));
            return true;
        }
        this->channel_pdu_.set_channel_type(msrdp::ChannelPdu::CHANNEL_TYPE_RAW);
        return true;
    }
    if (this->channel_pdu_.is_compressed())
    {
        std::string data = _io.get();
        std::string out_data;
        if ( g_decompress && g_decompress(
                data,
                this->channel_pdu_.to_server(),
                this->channel_pdu_.compression_type(),
                this->channel_pdu_.compression_packed_at_front(),
                this->channel_pdu_.compression_packed_flushed(),
                &out_data)
            )
        {
            _io.set(out_data);
            return true;
        }
        this->channel_pdu_.set_channel_type(msrdp::ChannelPdu::CHANNEL_TYPE_RAW);
        return true;
    }
    return true;
}
