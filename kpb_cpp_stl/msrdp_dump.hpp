#ifndef _H_MSRDP_DUMP_CPP_H_
#define _H_MSRDP_DUMP_CPP_H_

#include <rdp.hpp>

void msrdp_dump(const void *data, size_t data_size, bool to_server);
void msrdp_dump(const msrdp::Rdp &rdp);

#endif // _H_MSRDP_DUMP_CPP_H_
