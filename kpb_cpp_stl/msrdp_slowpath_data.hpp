#ifndef _H_MSRDP_SLOWPATH_DATA_CPP_H_
#define _H_MSRDP_SLOWPATH_DATA_CPP_H_

#include <cstdint>
#include <string>
#include <functional>

#include <kpb_cpp_stl_io.hpp>

#include "slowpath_data_pdu.hpp"

/**
 * Provide a function that can decompress slowpath data.
 */
void msrdp_set_slowpath_decompress_function(
    std::function<bool(
        const std::string & data,
        bool                to_server,
        uint8_t             compression_type,
        bool                packed_at_front,
        bool                packed_flushed,
        std::string *       out_data
    )> func
);

/**
 * Called internally by generated KPB code
 */
class MsrdpSlowpathData
{
    public:
        MsrdpSlowpathData(const msrdp::SlowpathDataPdu &_root);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        msrdp::SlowpathDataPdu &slowpath_data_pdu_;
};

#endif
