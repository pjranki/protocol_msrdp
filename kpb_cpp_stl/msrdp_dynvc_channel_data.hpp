#ifndef _H_MSRDP_DYNVC_CHANNEL_DATA_CPP_H_
#define _H_MSRDP_DYNVC_CHANNEL_DATA_CPP_H_

#include <cstdint>
#include <string>
#include <functional>

#include <kpb_cpp_stl_io.hpp>

#include "dynvc_pdu.hpp"
#include "rdp_segmented_data.hpp"
#include "rdp8_bulk_encoded_data.hpp"

/**
 * Provide a function that can lookup the type (msrdp::DynvcPdu::ChannelType) from the channel ID.
 * The provided function must return a msrdp::DynvcPdu::ChannelType (enumerator) as a uint8_t
 */
void msrdp_set_dynvc_channel_type_lookup(
    std::function<uint8_t(
        bool            to_server,
        uint32_t        channel_id
    )> func
);

/**
 * Convert an ASCII channel name to its type (msrdp::DynvcPdu::ChannelType)
 */
msrdp::DynvcPdu::ChannelType msrdp_dynvc_channel_type(const char *channel_name);

/**
 * Called internally by generated KPB code
 */
class MsrdpDynvcChannelData
{
    public:
        MsrdpDynvcChannelData(const msrdp::DynvcPdu &_root);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        msrdp::DynvcPdu &dynvc_pdu_;
};

#endif
