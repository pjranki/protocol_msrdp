#include "msrdp_cliprdr_data.hpp"

#include <string>

static size_t calc_format_name_size(const char *data, size_t size, bool ascii_names)
{
    size_t char_size = (ascii_names ? 1 : 2);
    for ( size_t i = 0; i < size; i += char_size )
    {
        if ( ascii_names )
        {
            if ( data[i] == 0x00 )
            {
                return i + char_size;
            }
        }
        else
        {
            if ( *((uint16_t*)&data[i]) == 0x0000 )
            {
                return i + char_size;
            }
        }
    }
    return size;
}

static void set_format_list_type(msrdp::CliprdrPdu &cliprdr_pdu, const std::string &data)
{
    // easy win, the size is not a mulitple of 36 (which it must be for short names)
    if ( (data.size() % 36) != 0 )
    {
        cliprdr_pdu.set_msg_flags(msrdp::CliprdrPdu::CB_FLAG_LONG_NAMES | cliprdr_pdu.msg_flags());
        return;
    }

    // heuristic
    bool ascii_names = (cliprdr_pdu.msg_flags() & msrdp::CliprdrPdu::CB_FLAG_ASCII_NAMES) != 0;
    for ( size_t i = 0; i < (data.size() / 36); i++ )
    {
        const char *format_item = data.c_str() + (i * 36);
        const char *format_name = format_item + 4;
        uint32_t format_id = *((uint32_t*)format_item);
        if ( format_id == 0 )
        {
            cliprdr_pdu.set_msg_flags(msrdp::CliprdrPdu::CB_FLAG_LONG_NAMES | cliprdr_pdu.msg_flags());
            return;
        }
        size_t format_name_size = calc_format_name_size(format_name, 32, ascii_names);
        if ( format_name_size <= 32 )
        {
            format_id = *((uint32_t*)(format_name + format_name_size));
            if ( format_id == 0 )
            {
                cliprdr_pdu.set_msg_flags(msrdp::CliprdrPdu::CB_FLAG_SHORT_NAMES | cliprdr_pdu.msg_flags());
                return;
            }
        }
    }

    // Could not determine
}

MsrdpCliprdrData::MsrdpCliprdrData(const msrdp::CliprdrPdu &_root) : cliprdr_pdu_(const_cast<msrdp::CliprdrPdu&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpCliprdrData::encode_size(kpb::IO &_io)
{
    // TODO: do I need to encode the format list flags?? not sure
    return _io.size();
}

bool MsrdpCliprdrData::encode(kpb::IO &_io)
{
    // TODO: do I need to encode the format list flags?? not sure
    _io;
    return true;
}

bool MsrdpCliprdrData::decode(kpb::IO &_io)
{
    if ( this->cliprdr_pdu_.msg_type() == msrdp::CliprdrPdu::CB_FORMAT_LIST )
    {
        std::string data = _io.get();
        set_format_list_type(this->cliprdr_pdu_, data);
    }
    return true;
}
