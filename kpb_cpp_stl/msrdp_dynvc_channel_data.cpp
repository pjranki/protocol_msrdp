#include "msrdp_dynvc_channel_data.hpp"

#include <map>
#include <string>
#include <mutex>

msrdp::DynvcPdu::ChannelType msrdp_dynvc_channel_type(const char *channel_name)
{
    if ( 0 == strcmp(channel_name, "ECHO") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_ECHO;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Input") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_INPUT;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Graphics") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GRAPHICS;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::AuthRedirection") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_AUTH_REDIRECTION;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Telemetry") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_TELEMETRY;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Location") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_LOCATION;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Geometry::v08.01") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GEOMETRY;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::DisplayControl") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_DISPLAY_CONTROL;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::RemoteAppGraphicsRedirection") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_REMOTE_APP_GRAPHICS_REDIRECTION;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Video::Control::v08.01") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_CONTROL_V08_01;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Video::Data::v08.01") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_DATA_V08_01;
    }
    if ( 0 == strcmp(channel_name, "Microsoft::Windows::RDS::Frame_Buffer::Control::v08.01") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_FRAME_BUFFER_CONTROL_V08_01;
    }
    if ( 0 == strcmp(channel_name, "AUDIO_PLAYBACK_DVC") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_PLAYBACK_DVC;
    }
    if ( 0 == strcmp(channel_name, "AUDIO_PLAYBACK_LOSSY_DVC") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_PLAYBACK_LOSSY_DVC;
    }
    if ( 0 == strcmp(channel_name, "AUDIO_INPUT") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_INPUT;
    }
    if ( 0 == strcmp(channel_name, "XPSRD") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_XPSRD;
    }
    if ( 0 == strcmp(channel_name, "TSVCTKT") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_TSVCTKT;
    }
    if ( 0 == strcmp(channel_name, "TSMF") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_TSMF;
    }
    if ( 0 == strcmp(channel_name, "PNPDR") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_PNPDR;
    }
    if ( 0 == strcmp(channel_name, "FileRedirectorChannel") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_FILE_REDIRECTOR_CHANNEL;
    }
    if ( 0 == strcmp(channel_name, "WDAG_DVC") )
    {
        return msrdp::DynvcPdu::CHANNEL_TYPE_WDAG_DVC;
    }
    if ( channel_name == strstr(channel_name, "RDCamera_Device_") )
    {
        if ( 0 == strcmp(channel_name, "RDCamera_Device_Enumerator") )
        {
            return msrdp::DynvcPdu::CHANNEL_TYPE_RDCAMERA_DEVICE_ENUMERATOR;
        }
        return msrdp::DynvcPdu::CHANNEL_TYPE_RDCAMERA_DEVICE;
    }
    return msrdp::DynvcPdu::CHANNEL_TYPE_RAW;
}

static std::function<uint8_t(bool,uint32_t)> g_lookup_channel_type = nullptr;

void msrdp_set_dynvc_channel_type_lookup(std::function<uint8_t(bool,uint32_t)> func)
{
    g_lookup_channel_type = func;
}

static std::mutex g_fragment_mutex;
static bool g_fragment_init = false;
static std::map<uint32_t,std::string> g_fragment_to_client;
static std::map<uint32_t,size_t> g_fragment_to_client_size;
static std::map<uint32_t,std::string> g_fragment_to_server;
static std::map<uint32_t,size_t> g_fragment_to_server_size;

static void fragment_init()
{
    if ( g_fragment_init )
    {
        return;
    }
    g_fragment_init = true;
    for ( size_t i = 0; i < 0xff; i++ )
    {
        g_fragment_to_client[(uint32_t)i] = std::string("", 0);
        g_fragment_to_client_size[(uint32_t)i] = 0;
        g_fragment_to_server[(uint32_t)i] = std::string("", 0);
        g_fragment_to_server_size[(uint32_t)i] = 0;
    }
}

static size_t decode_first_fragment_length_size(uint8_t sp)
{
    return (size_t)(((size_t)1) << sp);
}

static size_t decode_first_fragment_total_size(uint8_t sp, const std::string &data)
{
    size_t length_size = decode_first_fragment_length_size(sp);
    if ( data.size() < length_size )
    {
        return 0;
    }
    uint32_t length_byte_1 = 0;
    uint32_t length_byte_2 = 0;
    uint32_t length_byte_3 = 0;
    uint32_t length_byte_4 = 0;
    if ( length_size > 0 )
    {
        length_byte_1 = ((uint32_t)(data[0])) & 0xff;
    }
    if ( length_size > 1 )
    {
        length_byte_2 = ((uint32_t)(data[1])) & 0xff;
    }
    if ( length_size > 2 )
    {
        length_byte_3 = ((uint32_t)(data[2])) & 0xff;
    }
    if ( length_size > 3 )
    {
        length_byte_4 = ((uint32_t)(data[3])) & 0xff;
    }
    uint32_t length = ((length_byte_4 << 24) & 0xff000000) | ((length_byte_3 << 16) & 0xff0000) | ((length_byte_2 << 8) & 0xff00) | ((length_byte_1 << 0) & 0xff);
    return (size_t)length;
}

static std::string decode_first_fragment_total_size_data(uint8_t sp, const std::string &data)
{
    size_t length_size = decode_first_fragment_length_size(sp);
    if ( data.size() < length_size )
    {
        return data;
    }
    return std::string(data.c_str(), length_size);
}

static std::string decode_first_fragment_data(uint8_t sp, const std::string &data)
{
    size_t length_size = decode_first_fragment_length_size(sp);
    if ( length_size >= data.size() )
    {
        return std::string("", 0);
    }
    return std::string(data.c_str() + length_size, data.size() - length_size);
}

static std::string decode_fragment(msrdp::DynvcPdu &dynvc_pdu, const std::string &data)
{
    std::lock_guard<std::mutex> guard(g_fragment_mutex);
    fragment_init();
    uint32_t channel_id = (uint32_t)dynvc_pdu.channel_id();
    static std::map<uint32_t,std::string> &fragment = dynvc_pdu.to_server() ? g_fragment_to_server : g_fragment_to_client;
    static std::map<uint32_t,size_t> &fragment_size = dynvc_pdu.to_server() ? g_fragment_to_server_size : g_fragment_to_client_size;
    bool is_fragment = false;
    std::string out_data;
    if ( dynvc_pdu.cmd_type() == msrdp::DynvcPdu::DYNVC_CMD_TYPE_DATA_FIRST )
    {
        // first fragment
        is_fragment = true;
        fragment[channel_id] = decode_first_fragment_data((uint8_t)(dynvc_pdu.sp()), data);
        fragment_size[channel_id] = decode_first_fragment_total_size((uint8_t)(dynvc_pdu.sp()), data);
        out_data = decode_first_fragment_total_size_data((uint8_t)(dynvc_pdu.sp()), data);
    }
    else if ( fragment_size[channel_id] > 0 )
    {
        // next fragment
        is_fragment = true;
        fragment[channel_id] += data;
        out_data = std::string("", 0);
    }
    if ( is_fragment )
    {
        // check if we have all the fragments
        if ( fragment[channel_id].size() >= fragment_size[channel_id] )
        {
            // completed
            out_data = std::string(fragment[channel_id].c_str(), fragment_size[channel_id]);
            fragment_size[channel_id] = 0;
            fragment[channel_id] = std::string("", 0);
        }
        else
        {
            // still receiving fragments
            // treat data as raw
            dynvc_pdu.set_channel_type(msrdp::DynvcPdu::CHANNEL_TYPE_RAW);
        }
        return out_data;
    }
    return data;
}

static std::string decode_rdpgfx_segment(msrdp::DynvcPdu &dynvc_pdu, const std::string &data)
{
    if ( data.size() >= 2 )
    {
        if ( ( (uint8_t)(data[0]) == msrdp::RdpSegmentedData::RDP_SEGMENTED_SINGLE ) &&
           ( ( (uint8_t)(data[1]) & msrdp::Rdp8BulkEncodedData::PACKET_COMPRESSED) == 0) )
        {
            // complete segment, no compression
            return std::string(data.c_str() + 2, data.size() - 2);
        }
    }
    // TODO: decode a segment
    dynvc_pdu.set_channel_type(msrdp::DynvcPdu::CHANNEL_TYPE_RAW);
    return data;
}

MsrdpDynvcChannelData::MsrdpDynvcChannelData(const msrdp::DynvcPdu &_root) : dynvc_pdu_(const_cast<msrdp::DynvcPdu&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpDynvcChannelData::encode_size(kpb::IO &_io)
{
    // TODO: fragmentation and compression not implemented
    return _io.size();
}

bool MsrdpDynvcChannelData::encode(kpb::IO &_io)
{
    // TODO: fragmentation and compression not implemented
    _io;
    return true;
}

bool MsrdpDynvcChannelData::decode(kpb::IO &_io)
{
    // channel type
    if ( g_lookup_channel_type && this->dynvc_pdu_.channel_id() )
    {
        this->dynvc_pdu_.set_channel_type(g_lookup_channel_type(this->dynvc_pdu_.to_server(), (uint32_t)this->dynvc_pdu_.channel_id()));
    }

    // decode fragment
    std::string out_data = _io.get();
    out_data = decode_fragment(this->dynvc_pdu_, out_data);
    if ( out_data.size() == 0 )
    {
        _io.set(out_data);
        return true;
    }

    // decode RDPGFX_PDU (server-to-client) wrapped in RDP segmented data
    if ( (this->dynvc_pdu_.channel_type() == msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GRAPHICS ) && !(this->dynvc_pdu_.to_server()) )
    {
        out_data = decode_rdpgfx_segment(this->dynvc_pdu_, out_data);
    }
    _io.set(out_data);
    return true;
}
