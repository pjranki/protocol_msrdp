#include "msrdp_slowpath_data.hpp"

#include <string>

static std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> g_decompress = nullptr;

void msrdp_set_slowpath_decompress_function(
    std::function<bool(
        const std::string & data,
        bool                to_server,
        uint8_t             compression_type,
        bool                packed_at_front,
        bool                packed_flushed,
        std::string *       out_data
    )> func
)
{
    g_decompress = func;
}

MsrdpSlowpathData::MsrdpSlowpathData(const msrdp::SlowpathDataPdu &_root) : slowpath_data_pdu_(const_cast<msrdp::SlowpathDataPdu&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpSlowpathData::encode_size(kpb::IO &_io)
{
    // TODO: compression not implemented
    return _io.size();
}

bool MsrdpSlowpathData::encode(kpb::IO &_io)
{
    // TODO: compression not implemented
    _io;
    return true;
}

bool MsrdpSlowpathData::decode(kpb::IO &_io)
{
    if (this->slowpath_data_pdu_.is_compressed())
    {
        std::string data = _io.get();
        std::string out_data;
        if ( g_decompress && g_decompress(
                data,
                false, // to client
                this->slowpath_data_pdu_.compression_type(),
                this->slowpath_data_pdu_.compression_packed_at_front(),
                this->slowpath_data_pdu_.compression_packed_flushed(),
                &out_data)
            )
        {
            _io.set(out_data);
            return true;
        }
        this->slowpath_data_pdu_.set_pdu_type_2(msrdp::SlowpathDataPdu::PDUTYPE2_RAW);
        return true;
    }
    return true;
}
