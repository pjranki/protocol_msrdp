#ifndef _H_MSRDP_CLIPRDR_DATA_CPP_H_
#define _H_MSRDP_CLIPRDR_DATA_CPP_H_

#include <cstdint>
#include <string>

#include <kpb_cpp_stl_io.hpp>

#include "cliprdr_pdu.hpp"

/**
 * Called internally by generated KPB code
 */
class MsrdpCliprdrData
{
    public:
        MsrdpCliprdrData(const msrdp::CliprdrPdu &_root);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        msrdp::CliprdrPdu &cliprdr_pdu_;
};

#endif
