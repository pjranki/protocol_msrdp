#include "msrdp_static_channel_type.hpp"

static uint16_t get_channel_id(const msrdp::T125McsPdu &t125_mcs_pdu, const std::string &data)
{
    uint16_t channel_id = 0;
    if ( t125_mcs_pdu.msg() == msrdp::T125McsPdu::T125_MCS_MSG_SEND_DATA_REQUEST )
    {
        if ( data.size() >= 4 )
        {
            // big endian
            channel_id = ((((uint16_t)(data[2])) << 8) & 0xff00) | ((((uint16_t)(data[3])) << 0) & 0x00ff);
        }
    }
    if ( t125_mcs_pdu.msg() == msrdp::T125McsPdu::T125_MCS_MSG_SEND_DATA_INDICATION )
    {
        if ( data.size() >= 4 )
        {
            // big endian
            channel_id = ((((uint16_t)(data[2])) << 8) & 0xff00) | ((((uint16_t)(data[3])) << 0) & 0x00ff);
        }
    }
    return channel_id;
}

static std::function<uint8_t(bool,uint16_t)> g_lookup_channel_type = nullptr;

void msrdp_set_static_channel_type_lookup(std::function<uint8_t(bool,uint16_t)> func)
{
    g_lookup_channel_type = func;
}

msrdp::ChannelPdu::ChannelType msrdp_static_channel_type(const char *channel_name)
{
    if ( 0 == strcmp(channel_name, "security_layer") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_SECURITY_LAYER;
    }
    if ( 0 == strcmp(channel_name, "slowpath") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_SLOWPATH;
    }
    if ( 0 == strcmp(channel_name, "rdpdr") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_RDPDR;
    }
    if ( 0 == strcmp(channel_name, "rail") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_RAIL;
    }
    if ( 0 == strcmp(channel_name, "rail_wi") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_RAIL_WI;
    }
    if ( 0 == strcmp(channel_name, "rail_ri") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_RAIL_RI;
    }
    if ( 0 == strcmp(channel_name, "cliprdr") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_CLIPRDR;
    }
    if ( 0 == strcmp(channel_name, "drdynvc") )
    {
        return msrdp::ChannelPdu::CHANNEL_TYPE_DRDYNVC;
    }
    return msrdp::ChannelPdu::CHANNEL_TYPE_RAW;
}

MsrdpStaticChannelType::MsrdpStaticChannelType(const msrdp::T125McsPdu &_root) : t124_mcs_pdu_(const_cast<msrdp::T125McsPdu&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpStaticChannelType::encode_size(kpb::IO &_io)
{
    // data size is the same regards of channel type/name.
    return _io.size();
}

bool MsrdpStaticChannelType::encode(kpb::IO &_io)
{
    // channel type does not need to be set when encoding - it should have already been determined.
    _io;
    return true;
}

bool MsrdpStaticChannelType::decode(kpb::IO &_io)
{
    uint16_t channel_id = get_channel_id(this->t124_mcs_pdu_, _io.get());
    if ( g_lookup_channel_type && channel_id )
    {
        if ( !this->t124_mcs_pdu_.set_channel_type(g_lookup_channel_type(this->t124_mcs_pdu_.to_server(), channel_id)) )
        {
            return false;
        }
    }
    return true;
}
