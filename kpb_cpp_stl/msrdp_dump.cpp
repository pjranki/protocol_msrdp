#include "msrdp_dump.hpp"

#include <iostream>
#include <string>
#include <cstdint>

#include <rdp.hpp>
#include <ts_fp_input_pdu.hpp>
#include <ts_fp_update_pdu.hpp>
#include <ts_fp_update.hpp>
#include <ts_rail_pdu.hpp>
#include <ts_rail_ri_pdu.hpp>
#include <ts_rail_wi_pdu.hpp>
#include <ts_drawing_order.hpp>
#include <ts_primary_drawing_order.hpp>
#include <ts_secondary_drawing_order.hpp>
#include <ts_altsec_drawing_order.hpp>
#include <ts_window_order.hpp>
#include <ts_compdesk_drawing_order.hpp>
#include <ts_update_bitmap_data.hpp>
#include <ts_update_palette_data.hpp>
#include <ts_update_sync.hpp>
#include <ts_bitmap_data.hpp>
#include <ts_bitmap_data_ex.hpp>
#include <ts_pointerposattribute.hpp>
#include <ts_colorpointerattribute.hpp>
#include <ts_cachedpointerattribute.hpp>
#include <ts_pointerattribute.hpp>
#include <ts_cd_header.hpp>
#include <ts_compressed_bitmap_header_ex.hpp>
#include <ts_palette_entry.hpp>
#include <ts_surfcmd.hpp>
#include <ts_point16.hpp>
#include <t123_tpkt.hpp>
#include <x224_tpdu.hpp>
#include <t125_mcs_pdu.hpp>
#include <rdp_server_redirection_packet.hpp>
#include <ts_security_pdu.hpp>
#include <slowpath_pdu.hpp>
#include <slowpath_control_pdu.hpp>
#include <slowpath_data_pdu.hpp>
#include <channel_pdu.hpp>
#include <rdp_raw_pdu.hpp>
#include <rdpdr_pdu.hpp>
#include <cliprdr_pdu.hpp>
#include <dynvc_pdu.hpp>
#include <echo_pdu.hpp>
#include <rdpsnd_pdu.hpp>
#include <rdpsnd_lossy_pdu.hpp>
#include <sndin_pdu.hpp>
#include <audio_format.hpp>
#include <rdpgfx_pdu.hpp>
#include <rdpgfx_redir_pdu.hpp>
#include <wdag_dvc_pdu.hpp>

static void hexdumpline(size_t offset, void *buf, size_t size)
{
    char line[128];
    char *data = (char*)buf;
    char *pos = &(line[0]);
    size_t i;

    memset(line, 0, sizeof(line));

    if (size > 16)
    {
        size = 16;
    }

    for (i = 0; i < size; i++)
    {
        snprintf(pos, 4, "%02x ", (unsigned char)(data[i]));
        pos += 3;
    }
    for (i = 0; i < (16 - size); i++)
    {
        memcpy(pos, "   ", 3);
        pos += 3;
    }
    memcpy(pos, "| ", 2);
    pos += 2;
    for (i = 0; i < size; i++)
    {
        if ((data[i] >= ' ') && (data[i] <= '~'))
        {
            *pos = data[i];
        }
        else
        {
            *pos = '.';
        }
        pos += 1;
    }
    printf("%08x %s\n", (int) offset, line);
}

static void hexdump(const void *buf, size_t size)
{
    size_t i;
    char *data;

    for (i = 0; i < size; i += 16)
    {
        data = ((char*)buf) + i;
        if ((i + 16) <= size)
        {
            hexdumpline(i, data, 16);
        }
        else
        {
            hexdumpline(i, data, size - i);
        }
    }
}

static const char *rdp_decode_direction_str(bool to_server)
{
    return to_server ? "client-to-server" : "server-to-client";
}

static const char *rdp_decode_fragment_compressed_str(bool fragment, bool compressed)
{
    if ( fragment && compressed )
    {
        return "FRAGMENT | COMPRESSED";
    }
    if ( fragment )
    {
        return "FRAGMENT";
    }
    if ( compressed )
    {
        return "COMPRESSED";
    }
    return "RAW";
}

static void dump_ts_fp_input_pdu(const msrdp::TsFpInputPdu &ts_fp_input_pdu)
{
    if ( 0 == ts_fp_input_pdu.input_event_array().input_event().size() )
    {
        printf("(TS_FP_INPUT_PDU with no events)\n");
    }
    for ( auto _event : ts_fp_input_pdu.input_event_array().input_event() )
    {
        auto &event = _event.get();
        switch(event.code())
        {
            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_SCANCODE:
                printf("TS_FP_KEYBOARD_EVENT: scancode:0x%02x\n", event.ts_fp_keyboard_event().key_code());
                break;

            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_MOUSE:
                printf("TS_FP_POINTER_EVENT: (%d, %d)\n",
                    event.ts_fp_pointer_event().x_pos(),
                    event.ts_fp_pointer_event().y_pos());
                break;

            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_MOUSEX:
                printf("TS_FP_POINTERX_EVENT: (%d, %d)\n",
                    event.ts_fp_pointerx_event().x_pos(),
                    event.ts_fp_pointerx_event().y_pos());
                break;

            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_SYNC:
                printf("TS_FP_SYNC_EVENT\n");
                break;

            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_UNICODE:
                printf("TS_FP_UNICODE_KEYBOARD_EVENT: unicode_code:0x%04x\n", event.ts_fp_unicode_keyboard_event().unicode_code());
                break;

            case msrdp::TsFpInputEvent::FASTPATH_INPUT_EVENT_QOE_TIMESTAMP:
                printf("TS_FP_QOE_TIMESTAMP_EVENT: %d (ms)\n", event.ts_fp_qoe_timestamp_event().timestamp());
                break;

            default:
                printf("(unknown event)\n");
                break;
        }
    }
}

static void dump_ts_window_info_order_info(const msrdp::TsWindowInfoOrderInfo &ts_window_info_order_info)
{
    const msrdp::TsWindowInfoOrder &ts_window_info_order = ts_window_info_order_info.parent();
    printf("[window_id=0x%08x] TS_WINDOW_ORDER_INFO\n", ts_window_info_order.window_id());
    printf(" fields_present_flags: 0x%08x\n", ts_window_info_order.fields_present_flags());
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000002) != 0 )
    {
        printf(" owner_window_id: 0x%08x\n", ts_window_info_order_info.owner_window_id());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000008) != 0 )
    {
        printf(" style: 0x%08x\n", ts_window_info_order_info.style());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000008) != 0 )
    {
        printf(" extended_style: 0x%08x\n", ts_window_info_order_info.extended_style());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000010) != 0 )
    {
        printf(" show_state: 0x%02x\n", ts_window_info_order_info.show_state());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000004) != 0 )
    {
        printf(" title_info: '%S'\n", ts_window_info_order_info.title_info().string().c_str());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00004000) != 0 )
    {
        printf(" client_offset_x: 0x%08x\n", ts_window_info_order_info.client_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00004000) != 0 )
    {
        printf(" client_offset_y: 0x%08x\n", ts_window_info_order_info.client_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000) != 0 )
    {
        printf(" client_area_width: 0x%08x\n", ts_window_info_order_info.client_area_width());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000) != 0 )
    {
        printf(" client_area_height: 0x%08x\n", ts_window_info_order_info.client_area_height());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x80) != 0 )
    {
        printf(" window_left_resize_margin: 0x%08x\n", ts_window_info_order_info.window_left_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x80) != 0 )
    {
        printf(" window_right_resize_margin: 0x%08x\n", ts_window_info_order_info.window_right_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x08000000) != 0 )
    {
        printf(" window_top_resize_margin: 0x%08x\n", ts_window_info_order_info.window_top_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x08000000) != 0 )
    {
        printf(" window_bottom_resize_margin: 0x%08x\n", ts_window_info_order_info.window_bottom_resize_margin());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00020000) != 0 )
    {
        printf(" rp_content: 0x%02x\n", ts_window_info_order_info.rp_content());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00040000) != 0 )
    {
        printf(" root_parent_handle: 0x%08x\n", ts_window_info_order_info.root_parent_handle());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000800) != 0 )
    {
        printf(" window_offset_x: 0x%08x\n", ts_window_info_order_info.window_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000800) != 0 )
    {
        printf(" window_offset_y: 0x%08x\n", ts_window_info_order_info.window_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00008000) != 0 )
    {
        printf(" window_client_delta_x: 0x%08x\n", ts_window_info_order_info.window_client_delta_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00008000) != 0 )
    {
        printf(" window_client_delta_y: 0x%08x\n", ts_window_info_order_info.window_client_delta_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000400) != 0 )
    {
        printf(" window_width: 0x%08x\n", ts_window_info_order_info.window_width());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000400) != 0 )
    {
        printf(" window_height: 0x%08x\n", ts_window_info_order_info.window_height());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000100) != 0 )
    {
        printf(" num_window_rects: 0x%d\n", ts_window_info_order_info.num_window_rects());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000100) != 0 )
    {
        // ts_rectangle_16
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00001000) != 0 )
    {
        printf(" visible_offset_x: 0x%08x\n", ts_window_info_order_info.visible_offset_x());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00001000) != 0 )
    {
        printf(" visible_offset_y: 0x%08x\n", ts_window_info_order_info.visible_offset_y());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000200) != 0 )
    {
        printf(" numb_visible_rects: 0x%d\n", ts_window_info_order_info.numb_visible_rects());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000200) != 0 )
    {
        // ts_rectangle_16
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00400000) != 0 )
    {
        printf(" overlay_description: '%S'\n", ts_window_info_order_info.overlay_description().string().c_str());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00800000) != 0 )
    {
        printf(" taskbar_button: 0x%02x\n", ts_window_info_order_info.taskbar_button());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00080000) != 0 )
    {
        printf(" enforce_server_z_order: 0x%02x\n", ts_window_info_order_info.enforce_server_z_order());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000040) != 0 )
    {
        printf(" app_bar_state: 0x%02x\n", ts_window_info_order_info.app_bar_state());
    }
    if ( ( ts_window_info_order.fields_present_flags() & 0x00000001) != 0 )
    {
        printf(" app_bar_edge: 0x%02x\n", ts_window_info_order_info.app_bar_edge());
    }
}

static void dump_ts_window_info_order(const msrdp::TsWindowInfoOrder &ts_window_info_order)
{
    if ( ( ts_window_info_order.fields_present_flags() & 0x10000000 ) != 0 )
    {
        printf("------------------------ NEW WINDOW ------------------------\n");
    }
    switch(ts_window_info_order.order_type())
    {
        case msrdp::TsWindowInfoOrder::WINDOW_ORDER_TYPE_WINDOW_INFORMATION:
            dump_ts_window_info_order_info(ts_window_info_order.ts_window_info_order_info());
            break;

        case msrdp::TsWindowInfoOrder::WINDOW_ORDER_TYPE_WINDOW_ICON:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_ICON\n", ts_window_info_order.window_id());
            break;

        case msrdp::TsWindowInfoOrder::WINDOW_ORDER_TYPE_CACHED_ICON:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_CACHED_ICON\n", ts_window_info_order.window_id());
            break;

        case msrdp::TsWindowInfoOrder::WINDOW_ORDER_TYPE_DELETE_WINDOW:
            printf("[window_id=0x%08x] TS_WINDOW_ORDER_DELETE_WINDOW\n", ts_window_info_order.window_id());
            break;

        default:
            printf("(unknown window order 0x%08x)\n", ts_window_info_order.order_type());
            break;
    }
}

static void dump_ts_notifyicon_order(const msrdp::TsNotifyiconOrder &ts_notifyicon_order)
{
    switch(ts_notifyicon_order.order_type())
    {
        case msrdp::TsNotifyiconOrder::NOTIFYICON_ORDER_TYPE_ICON_INFORMATION:
            printf("[window_id=0x%08x] TS_NOTIFYICON_ORDER_INFO\n", ts_notifyicon_order.window_id());
            break;

        case msrdp::TsNotifyiconOrder::NOTIFYICON_ORDER_TYPE_DELETE_ICON:
            printf("[window_id=0x%08x] TS_NOTIFYICON_ORDER_DELETE\n", ts_notifyicon_order.window_id());
            break;

        default:
            printf("(unknown notify icon order 0x%08x)\n", ts_notifyicon_order.order_type());
            break;
    }
}

static void dump_ts_desktop_order(const msrdp::TsDesktopOrder &ts_desktop_order)
{
    switch(ts_desktop_order.order_type())
    {
        case msrdp::TsDesktopOrder::DESKTOP_ORDER_TYPE_ACTIVELY_MONITORED :
            printf("TS_DESKTOP_ORDER_ACTIVELY_MONITORED\n");
            break;

        case msrdp::TsDesktopOrder::DESKTOP_ORDER_TYPE_NON_MONITORED :
            printf("TS_DESKTOP_ORDER_NON_MONITORED\n");
            break;

        default:
            printf("(unknown desktop order 0x%08x)\n", ts_desktop_order.order_type());
            break;
    }
}

static void dump_ts_window_order(const msrdp::TsWindowOrder &ts_window_order)
{
    switch(ts_window_order.order_type())
    {
        case msrdp::TsWindowOrder::WINDOW_ORDER_TYPE_WINDOW:
            dump_ts_window_info_order(ts_window_order.ts_window_info_order());
            break;

        case msrdp::TsWindowOrder::WINDOW_ORDER_TYPE_NOTIFY:
            dump_ts_notifyicon_order(ts_window_order.ts_notifyicon_order());
            break;

        case msrdp::TsWindowOrder::WINDOW_ORDER_TYPE_DESKTOP:
            dump_ts_desktop_order(ts_window_order.ts_desktop_order());
            break;

        default:
            printf("(unknown window drawing order 0x%08x)\n", ts_window_order.order_type());
            break;
    }
}

static void dump_ts_altsec_drawing_orders(const msrdp::TsAltsecDrawingOrder &ts_altsec_drawing_order)
{
    switch(ts_altsec_drawing_order.order_type())
    {
        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_SWITCH_SURFACE:
            printf("TS_ALTSEC_SWITCH_SURFACE\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_CREATE_OFFSRC_BITMAP:
            printf("TS_ALTSEC_CREATE_OFFSRC_BITMAP\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_STREAM_BITMAP_FIRST:
            printf("TS_ALTSEC_STREAM_BITMAP_FIRST\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_STREAM_BITMAP_NEXT:
            printf("TS_ALTSEC_STREAM_BITMAP_NEXT\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_CREATE_NINEGRID_BITMAP:
            printf("TS_ALTSEC_CREATE_NINEGRID_BITMAP\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_FIRST:
            printf("TS_ALTSEC_GDIP_FIRST\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_NEXT:
            printf("TS_ALTSEC_GDIP_NEXT\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_END:
            printf("TS_ALTSEC_GDIP_END\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_CACHE_FIRST:
            printf("TS_ALTSEC_GDIP_CACHE_FIRST\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_CACHE_NEXT:
            printf("TS_ALTSEC_GDIP_CACHE_NEXT\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_GDIP_CACHE_END:
            printf("TS_ALTSEC_GDIP_CACHE_END\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_WINDOW:
            dump_ts_window_order(ts_altsec_drawing_order.ts_window_order());
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_COMPDESK_FIRST:
            printf("TS_COMPDESK_DRAWING_ORDER\n");
            break;

        case msrdp::TsAltsecDrawingOrder::TS_ALTSEC_FRAME_MARKER:
            printf("TS_ALTSEC_FRAME_MARKER\n");
            break;

        default:
            printf("(unknown alternative order type 0x%02x)\n", ts_altsec_drawing_order.order_type());
            break;
    }
}

static void dump_ts_fp_update_orders(const msrdp::TsFpUpdateOrders &ts_fp_update_orders)
{
    for ( auto _drawing_order : ts_fp_update_orders.orders() )
    {
        auto &drawing_order = _drawing_order.get();
        switch(drawing_order.order_class())
        {
            case msrdp::TsDrawingOrder::ORDER_CLASS_PRIMARY:
                printf("ORDER_CLASS_PRIMARY\n");
                break;
            case msrdp::TsDrawingOrder::ORDER_CLASS_SECONDARY:
                printf("ORDER_CLASS_SECONDARY\n");
                break;
            case msrdp::TsDrawingOrder::ORDER_CLASS_ALTERNATIVE:
                dump_ts_altsec_drawing_orders(drawing_order.ts_altsec_drawing_order());
                break;
            default:
                printf("(unknown drawing order class)\n");
                break;
        }
    }
}

static void dump_ts_update_bitmap_data(const msrdp::TsUpdateBitmapData &ts_update_bitmap_data)
{
    ts_update_bitmap_data;
    printf("TS_UPDATE_BITMAP_DATA\n");
}

static void dump_ts_update_palette_data(const msrdp::TsUpdatePaletteData &ts_update_palette_data)
{
    ts_update_palette_data;
    printf("TS_UPDATE_PALETTE_DATA\n");
}

static void dump_ts_update_sync(const msrdp::TsUpdateSync &ts_update_sync)
{
    ts_update_sync;
    printf("TS_UPDATE_SYNC\n");
}

static void dump_ts_surfcmd(const msrdp::TsSurfcmd &ts_surfcmd)
{
    switch(ts_surfcmd.cmd_type())
    {
        case msrdp::TsSurfcmd::CMDTYPE__SET_SURFACE_BITS:
            printf("TS_SURFCMD_SET_SURF_BITS\n");
            break;
        case msrdp::TsSurfcmd::CMDTYPE_FRAME_MARKER:
            printf("TS_FRAME_MARKER\n");
            break;
        case msrdp::TsSurfcmd::CMDTYPE_STREAM_SURFACE_BITS:
            printf("TS_SURFCMD_STREAM_SURF_BITS\n");
            break;
        default:
            printf("(unknown TS_SURFCMD class)\n");
            break;
    }
}

static void dump_ts_pointerposattribute(const msrdp::TsPointerposattribute &ts_pointerposattribute)
{
    printf("TS_POINTERPOSATTRIBUTE (%d, %d)\n",
        ts_pointerposattribute.position().x_pos(),
        ts_pointerposattribute.position().y_pos());
}

static void dump_ts_colorpointerattribute(const msrdp::TsColorpointerattribute &ts_colorpointerattribute)
{
    ts_colorpointerattribute;
    printf("TS_COLORPOINTERATTRIBUTE\n");
}

static void dump_ts_cachedpointerattribute(const msrdp::TsCachedpointerattribute &ts_cachedpointerattribute)
{
    ts_cachedpointerattribute;
    printf("TS_CACHEDPOINTERATTRIBUTE\n");
}

static void dump_ts_pointerattribute(const msrdp::TsPointerattribute &ts_pointerattribute)
{
    ts_pointerattribute;
    printf("TS_POINTERATTRIBUTE\n");
}

static void dump_ts_fp_surfcmds(const msrdp::TsFpSurfcmds &ts_fp_surfcmds)
{
    for ( auto _ts_surfcmd : ts_fp_surfcmds.surface_commands() )
    {
        auto &ts_surfcmd = _ts_surfcmd.get();
        dump_ts_surfcmd(ts_surfcmd);
    }
}

static void dump_ts_fp_systempointerhiddenattribute(const msrdp::TsFpSystempointerhiddenattribute &ts_fp_systempointerhiddenattribute)
{
    ts_fp_systempointerhiddenattribute;
    printf("TS_FP_SYSTEMPOINTERHIDDENATTRIBUTE\n");
}

static void dump_ts_fp_systempointerdefaultattribute(const msrdp::TsFpSystempointerdefaultattribute &ts_fp_systempointerdefaultattribute)
{
    ts_fp_systempointerdefaultattribute;
    printf("TS_FP_SYSTEMPOINTERDEFAULTATTRIBUTE\n");
}

static void dump_ts_fp_largepointerattribute(const msrdp::TsFpLargepointerattribute &ts_fp_largepointerattribute)
{
    ts_fp_largepointerattribute;
    printf("TS_FP_LARGEPOINTERATTRIBUTE\n");
}

static void dump_ts_fp_meta_command_update(const msrdp::TsFpMetaCommandUpdate &ts_fp_meta_command_update)
{
    ts_fp_meta_command_update;
    printf("TS_FP_META_COMMAND_UPDATE (UNDOCUMENTED)\n");
}

static void dump_ts_fp_delta_encoder_update(const msrdp::TsFpDeltaEncoderUpdate &ts_fp_delta_encoder_update)
{
    ts_fp_delta_encoder_update;
    printf("TS_FP_DELTA_ENCODER_UPDATE (UNDOCUMENTED)\n");
}

static void dump_ts_fp_update(const msrdp::TsFpUpdate &ts_fp_update)
{
    auto &ts_fp_update_data = ts_fp_update.data();
    switch(ts_fp_update.code())
    {
        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_RAW:
            if ( ts_fp_update_data.ts_fp_update_raw().data().size() > 0 )
            {
                printf("FASTPATH UPDATE (%s) DATA (%s):\n",
                    rdp_decode_fragment_compressed_str(ts_fp_update.is_fragment(), ts_fp_update.is_compressed()),
                    rdp_decode_direction_str(false));
                hexdump(
                    ts_fp_update_data.ts_fp_update_raw().data().c_str(),
                    ts_fp_update_data.ts_fp_update_raw().data().size()
                );
            }
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_ORDERS:
            dump_ts_fp_update_orders(ts_fp_update_data.ts_fp_update_orders());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_BITMAP:
            dump_ts_update_bitmap_data(ts_fp_update_data.ts_fp_update_bitmap().bitmap_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_PALETTE:
            dump_ts_update_palette_data(ts_fp_update_data.ts_fp_update_palette().palette_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_SYNCHRONIZE:
            dump_ts_update_sync(ts_fp_update_data.ts_fp_update_synchronize().update_sync());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_SURFCMDS:
            dump_ts_fp_surfcmds(ts_fp_update_data.ts_fp_surfcmds());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_PTR_NULL:
            dump_ts_fp_systempointerhiddenattribute(ts_fp_update_data.ts_fp_systempointerhiddenattribute());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_PTR_DEFAULT:
            dump_ts_fp_systempointerdefaultattribute(ts_fp_update_data.ts_fp_systempointerdefaultattribute());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_PTR_POSITION:
            dump_ts_pointerposattribute(ts_fp_update_data.ts_fp_pointerposattribute().pointer_position_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_COLOR:
            dump_ts_colorpointerattribute(ts_fp_update_data.ts_fp_colorpointerattribute().color_pointer_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_CACHED:
            dump_ts_cachedpointerattribute(ts_fp_update_data.ts_fp_cachedpointerattribute().cached_pointer_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_POINTER:
            dump_ts_pointerattribute(ts_fp_update_data.ts_fp_pointerattribute().new_pointer_data());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_LARGE_POINTER:
            dump_ts_fp_largepointerattribute(ts_fp_update_data.ts_fp_largepointerattribute());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_META_COMMAND:
            dump_ts_fp_meta_command_update(ts_fp_update_data.ts_fp_meta_command_update());
            break;

        case msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_DELTA_ENCODER:
            dump_ts_fp_delta_encoder_update(ts_fp_update_data.ts_fp_delta_encoder_update());
            break;

        default:
            printf("(unknown TS_FP_UPDATE)\n");
            break;
    }
}

static void dump_ts_fp_update_pdu(const msrdp::TsFpUpdatePdu &ts_fp_update_pdu)
{
    if ( 0 == ts_fp_update_pdu.update_array().update_().size() )
    {
        printf("(TS_FP_UPDATE_PDU with no updates)\n");
    }
    for ( auto _update : ts_fp_update_pdu.update_array().update_() )
    {
        auto &update = _update.get();
        dump_ts_fp_update(update);
    }
}

static void dump_server_redir_pkt(const msrdp::RdpServerRedirectionPacket &server_redirection_packet)
{
    server_redirection_packet;
    printf("SERVER REDIRECTION PACKET\n");
    // TODO
}

static void dump_security_layer_pdu(const msrdp::TsSecurityPdu &ts_security_pdu)
{
    switch(ts_security_pdu.msg())
    {
        case msrdp::TsSecurityPdu::SEC_EXCHANGE_PKT:
            printf("SEC_EXCHANGE_PKT\n");
            break;

        case msrdp::TsSecurityPdu::SEC_TRANSPORT_REQ:
            printf("SEC_TRANSPORT_REQ\n");
            break;

        case msrdp::TsSecurityPdu::SEC_TRANSPORT_RSP:
            printf("SEC_TRANSPORT_RSP\n");
            break;

        case msrdp::TsSecurityPdu::SEC_INFO_PKT:
            printf("SEC_INFO_PKT\n");
            break;

        case msrdp::TsSecurityPdu::SEC_LICENSE_PKT:
            printf("SEC_LICENSE_PKT\n");
            break;

        case msrdp::TsSecurityPdu::SEC_REDIRECTION_PKT:
            dump_server_redir_pkt(ts_security_pdu.rdp_server_redirection_packet());
            break;

        case msrdp::TsSecurityPdu::SEC_AUTODETECT_REQ:
            printf("SEC_AUTODETECT_REQ\n");
            break;

        case msrdp::TsSecurityPdu::SEC_AUTODETECT_RSP:
            printf("SEC_AUTODETECT_RSP\n");
            break;

        case msrdp::TsSecurityPdu::SEC_HEARTBEAT:
            printf("SEC_HEARTBEAT: reserved: %d, period: %d, count1: %d, count2: %d\n",
                ts_security_pdu.server_heartbeat_pdu().reserved(),
                ts_security_pdu.server_heartbeat_pdu().period(),
                ts_security_pdu.server_heartbeat_pdu().count1(),
                ts_security_pdu.server_heartbeat_pdu().count2());
            break;

        default:
            printf("(unknown security layer packet, flags=0x%04x)\n", ts_security_pdu.flags());
            break;
    }
}

static void dump_ts_update_orders(const msrdp::TsUpdateOrders &ts_update_orders)
{
    for ( auto _drawing_order : ts_update_orders.orders() )
    {
        auto &drawing_order = _drawing_order.get();
        switch(drawing_order.order_class())
        {
            case msrdp::TsDrawingOrder::ORDER_CLASS_PRIMARY:
                printf("ORDER_CLASS_PRIMARY\n");
                break;
            case msrdp::TsDrawingOrder::ORDER_CLASS_SECONDARY:
                printf("ORDER_CLASS_SECONDARY\n");
                break;
            case msrdp::TsDrawingOrder::ORDER_CLASS_ALTERNATIVE:
                dump_ts_altsec_drawing_orders(drawing_order.ts_altsec_drawing_order());
                break;
            default:
                printf("(unknown drawing order class)\n");
                break;
        }
    }
}

static void dump_ts_surfcmds(const msrdp::TsSurfcmds &ts_surfcmds)
{
    for ( auto _ts_surfcmd : ts_surfcmds.surface_commands() )
    {
        auto &ts_surfcmd = _ts_surfcmd.get();
        dump_ts_surfcmd(ts_surfcmd);
    }
}

static void dump_ts_graphics_update(const msrdp::TsGraphicsUpdate &ts_graphic_update)
{
    switch(ts_graphic_update.update_type())
    {
        case msrdp::TsGraphicsUpdate::UPDATETYPE_ORDERS:
            dump_ts_update_orders(ts_graphic_update.ts_update_orders());
            break;

        case msrdp::TsGraphicsUpdate::UPDATETYPE_BITMAP:
            dump_ts_update_bitmap_data(ts_graphic_update.ts_update_bitmap().bitmap_data());
            break;

        case msrdp::TsGraphicsUpdate::UPDATETYPE_PALETTE:
            dump_ts_update_palette_data(ts_graphic_update.ts_update_palette().palette_data());
            break;

        case msrdp::TsGraphicsUpdate::UPDATETYPE_SYNCHRONIZE:
            dump_ts_update_sync(ts_graphic_update.ts_update_sync());
            break;

        case msrdp::TsGraphicsUpdate::UPDATETYPE_SURFCMDS:
            dump_ts_surfcmds(ts_graphic_update.ts_surfcmds());
            break;

        default:
            printf("(unknown ts_graphics_update type 0x%02x)\n", (uint8_t)(ts_graphic_update.update_type()));
            break;
    }
}

static void dump_ts_pointer_pdu(const msrdp::TsPointerPdu &ts_pointer_pdu)
{
    switch(ts_pointer_pdu.message_type())
    {
        case msrdp::TsPointerPdu::TS_PTRMSGTYPE_SYSTEM:
            printf("TS_SYSTEMPOINTERATTRIBUTE system_pointer_type 0x%08x\n",
                ts_pointer_pdu.ts_systempointerattribute().system_pointer_type());
            break;

        case msrdp::TsPointerPdu::TS_PTRMSGTYPE_POSITION:
            dump_ts_pointerposattribute(ts_pointer_pdu.ts_pointerposattribute());
            break;

        case msrdp::TsPointerPdu::TS_PTRMSGTYPE_COLOR:
            dump_ts_colorpointerattribute(ts_pointer_pdu.ts_colorpointerattribute());
            break;

        case msrdp::TsPointerPdu::TS_PTRMSGTYPE_CACHED:
            dump_ts_cachedpointerattribute(ts_pointer_pdu.ts_cachedpointerattribute());
            break;

        case msrdp::TsPointerPdu::TS_PTRMSGTYPE_POINTER:
            dump_ts_pointerattribute(ts_pointer_pdu.ts_pointerattribute());
            break;

        default:
            printf("(unknown ts_pointer_pdu message type 0x%02x)\n", (uint8_t)(ts_pointer_pdu.message_type()));
            break;
    }
}

static void dump_slowpath_data(const msrdp::SlowpathData &slowpath_data)
{
    switch(slowpath_data.pdu_type_2())
    {
        case msrdp::SlowpathDataPdu::PDUTYPE2_RAW:
            printf("SLOWPATH DATA RAW\n");
            hexdump(
                slowpath_data.slowpath_data_raw().data().c_str(),
                slowpath_data.slowpath_data_raw().data().size());
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_UPDATE_:
            dump_ts_graphics_update(slowpath_data.ts_graphics_update());
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_CONTROL:
            printf("SLOWPATH CONTROL\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_POINTER:
            dump_ts_pointer_pdu(slowpath_data.ts_pointer_pdu());
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_INPUT:
            printf("SLOWPATH INPUT\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_SYNCHRONIZE:
            printf("SLOWPATH SYNCHRONIZE\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_REFRESH_RECT:
            printf("SLOWPATH REFRESH RECT\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_PLAY_SOUND:
            printf("PLAY SOUND duration %d ms, %d Hz\n",
            slowpath_data.ts_play_sound_pdu_data().duration(),
            slowpath_data.ts_play_sound_pdu_data().frequency());
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_SUPPRESS_OUTPUT:
            printf("SLOWPATH SUPPRESS OUTPUT\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_SHUTDOWN_REQUEST:
            printf("SLOWPATH SHUTDOWN REQUEST\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_SHUTDOWN_DENIED:
            printf("SLOWPATH SHUTDOWN DENIED\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_SAVE_SESSION_INFO:
            printf("SLOWPATH SAVE SESSION INFO\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_FONTLIST:
            printf("SLOWPATH FONTLIST\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_FONTMAP:
            printf("SLOWPATH FONT MAP\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2__SET_KEYBOARD_INDICATORS:
            printf("SLOWPATH SET KEYBOARD INDICATORS\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_BITMAPCACHE_PERSISTENT_LIST:
            printf("SLOWPATH BITMAPCACHE PERSISTENT LIST\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_BITMAPCACHE_ERROR_PDU:
            printf("SLOWPATH BITMAPCACHE ERROR PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2__SET_KEYBOARD_IME_STATUS:
            printf("SLOWPATH SET KEYBOARD IME STATUS\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_OFFSCRECACHE_ERROR_PDU:
            printf("SLOWPATH OFFSCRECACHE ERROR PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2__SET_ERROR_INFO_PDU:
            printf("SLOWPATH SET ERROR INFO PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_DRAWNINEGRID_ERROR_PDU:
            printf("SLOWPATH DRAWNINEGRID ERROR PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_DRAWGDIPLUS_ERROR_PDU:
            printf("SLOWPATH DRAWGDIPLUS ERROR PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_ARC_STATUS_PDU:
            printf("SLOWPATH ARC STATUS PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_INPUT_MODE_CHANGE_PDU:
            printf("SLOWPATH INPUT MODE CHANGE PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_STATUS_INFO_PDU:
            printf("SLOWPATH STATUS INFO PDU\n");
            break;

        case msrdp::SlowpathDataPdu::PDUTYPE2_MONITOR_LAYOUT_PDU:
            printf("SLOWPATH MONITOR LAYOUT PDU\n");
            break;

        default:
            printf("(unknown slowpath data PDU, pdu_type_2=0x%02x)\n", (uint8_t)(slowpath_data.pdu_type_2()));
            break;
    }
}

static void dump_slowpath_data_pdu(const msrdp::SlowpathDataPdu &data_pdu)
{
    dump_slowpath_data(data_pdu.data());
}

static void dump_slowpath_control_pdu(const msrdp::SlowpathControlPdu &control_pdu)
{
    switch(control_pdu.pdu_type())
    {
        case msrdp::SlowpathControlPdu::PDUTYPE_DEMANDACTIVEPDU:
            printf("SLOWPATH DEMAND ACTIVE PDU\n");
            break;

        case msrdp::SlowpathControlPdu::PDUTYPE_CONFIRMACTIVEPDU:
            printf("SLOWPATH CONFIRM ACTIVE PDU\n");
            break;

        case msrdp::SlowpathControlPdu::PDUTYPE_DEACTIVATEALLPDU:
            printf("SLOWPATH DEACTIVATE ALL PDU\n");
            break;

        case msrdp::SlowpathControlPdu::PDUTYPE_SERVER_REDIR_PKT:
            dump_server_redir_pkt(control_pdu.rdp_server_redirection_packet());
            break;

        case msrdp::SlowpathControlPdu::PDUTYPE_DATAPDU:
            dump_slowpath_data_pdu(control_pdu.slowpath_data_pdu());
            break;

        default:
            printf("(unknown slowpath PDU, pdu_type=0x%02x)\n", (uint8_t)(control_pdu.pdu_type()));
            break;
    }
}

static void dump_slowpath_pdu(const msrdp::SlowpathPdu &slowpath_pdu)
{
    for ( auto _data : slowpath_pdu.data() )
    {
        auto &data = _data.get();
        if (data.flow_pdu())
        {
            printf("SLOWPATH FLOW PDU value=0x%04x\n", data.flow_value());
        }
        else
        {
            dump_slowpath_control_pdu(data.data());
        }
    }
}

static void dump_rdpgfx_capset(const msrdp::RdpgfxCapset &capset)
{
    printf("RDPGFX CAPSET version 0x%08x, data %d bytes\n", capset.version(), (uint32_t)(capset.caps_data().size()));
}

static void dump_rdpgfx_caps_confirm_pdu(const msrdp::RdpCapsConfirmPdu &caps_confirm_pdu)
{
    printf("RDPGFX CAPSCONFIRM\n");
    dump_rdpgfx_capset(caps_confirm_pdu.caps_set_());
}

static void dump_rdpgfx_caps_advertise_pdu(const msrdp::RdpgfxCapsAdvertisePdu &caps_advertise_pdu)
{
    printf("RDPGFX CAPSADVERTISE\n");
    for ( auto _capset : caps_advertise_pdu.caps_sets() )
    {
        auto &capset = _capset.get();
        dump_rdpgfx_capset(capset);
    }
}

static void dump_rdpgfx_pdu(const msrdp::RdpgfxPdu &rdpgfx_pdu)
{
    switch(rdpgfx_pdu.cmd_id())
    {
        case msrdp::RdpgfxPdu::RDPGFX_CMDID_WIRETOSURFACE_1:
            printf("RDPGFX WIRETOSURFACE_1\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_WIRETOSURFACE_2:
            printf("RDPGFX WIRETOSURFACE_2\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_DELETEENCODINGCONTEXT:
            printf("RDPGFX DELETEENCODINGCONTEXT\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_SOLIDFILL:
            printf("RDPGFX SOLIDFILL\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_SURFACETOSURFACE:
            printf("RDPGFX SURFACETOSURFACE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_SURFACETOCACHE:
            printf("RDPGFX SURFACETOCACHE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CACHETOSURFACE:
            printf("RDPGFX CACHETOSURFACE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_EVICTCACHEENTRY:
            printf("RDPGFX EVICTCACHEENTRY\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CREATESURFACE:
            printf("RDPGFX CREATESURFACE surface_id=0x%02x %dx%d pixel_format=0x%02x\n",
                rdpgfx_pdu.rdpgfx_create_surface_pdu().surface_id(),
                rdpgfx_pdu.rdpgfx_create_surface_pdu().width(),
                rdpgfx_pdu.rdpgfx_create_surface_pdu().height(),
                rdpgfx_pdu.rdpgfx_create_surface_pdu().pixel_format().format());
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_DELETESURFACE:
            printf("RDPGFX DELETESURFACE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_STARTFRAME:
            printf("RDPGFX STARTFRAME\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_ENDFRAME:
            printf("RDPGFX ENDFRAME\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_FRAMEACKNOWLEDGE:
            printf("RDPGFX FRAMEACKNOWLEDGE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_RESETGRAPHICS:
            printf("RDPGFX RESETGRAPHICS\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_MAPSURFACETOOUTPUT:
            printf("RDPGFX MAPSURFACETOOUTPUT\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CACHEIMPORTOFFER:
            printf("RDPGFX CACHEIMPORTOFFER\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CACHEIMPORTREPLY:
            printf("RDPGFX CACHEIMPORTREPLY\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CAPSADVERTISE:
            dump_rdpgfx_caps_advertise_pdu(rdpgfx_pdu.rdpgfx_caps_advertise_pdu());
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_CAPSCONFIRM:
            dump_rdpgfx_caps_confirm_pdu(rdpgfx_pdu.rdp_caps_confirm_pdu());
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_DIAGNOSTIC:
            printf("RDPGFX DIAGNOSTIC\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_MAPSURFACETOWINDOW:
            printf("RDPGFX MAPSURFACETOWINDOW\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_QOEFRAMEACKNOWNLEDGE:
            printf("RDPGFX QOEFRAMEACKNOWNLEDGE\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_MAPSURFACETOSCALEDOUTPUT:
            printf("RDPGFX MAPSURFACETOSCALEDOUTPUT\n");
            break;

        case msrdp::RdpgfxPdu::RDPGFX_CMDID_MAPSURFACETOSCALEDWINDOW:
            printf("RDPGFX MAPSURFACETOSCALEDWINDOW\n");
            break;

        default:
            printf("(unknown Microsoft::Windows::RDS::Graphics type %d)\n", rdpgfx_pdu.cmd_id());
            break;
    }
}

static void dump_rdpgfx_redir_pdu(const msrdp::RdpgfxRedirPdu &rdpgfx_redir_pdu)
{
    switch(rdpgfx_redir_pdu.cmd_id())
    {
        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_PDU:
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_PDU\n",
                (uint32_t)(rdpgfx_redir_pdu.graphics_update_pdu().window_id() >> 32),
                (uint32_t)(rdpgfx_redir_pdu.graphics_update_pdu().window_id() >> 0));
            break;

        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_ACK_PDU:
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_GRAPHICS_UPDATE_ACK_PDU\n",
                (uint32_t)(rdpgfx_redir_pdu.graphics_update_ack_pdu().window_id() >> 32),
                (uint32_t)(rdpgfx_redir_pdu.graphics_update_ack_pdu().window_id() >> 0));
            break;

        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_CREATE_SURFACE_PDU:
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_CREATE_SURFACE_PDU\n",
                (uint32_t)(rdpgfx_redir_pdu.create_surface_pdu().window_id() >> 32),
                (uint32_t)(rdpgfx_redir_pdu.create_surface_pdu().window_id() >> 0));
            printf(" \\VmSharedMemory\\Host\\%S\\%S\n", L"<HiDefRemoteAppContainerGUID>",
                rdpgfx_redir_pdu.create_surface_pdu().section_name().c_str());
            printf(" vm='%S'\n", L"<HiDefRemoteAppContainerGUID>");
            printf(" section='%S'\n", rdpgfx_redir_pdu.create_surface_pdu().section_name().c_str());
            printf(" width=%d, height=%d, bpp=4, region_size=%d, scan_line_size=%d\n",
                rdpgfx_redir_pdu.create_surface_pdu().width(),
                rdpgfx_redir_pdu.create_surface_pdu().height(),
                rdpgfx_redir_pdu.create_surface_pdu().region_size_(),
                rdpgfx_redir_pdu.create_surface_pdu().scan_line_size_());
            break;

        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_DELETE_SURFACE_PDU:
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_DELETE_SURFACE_PDU\n",
                (uint32_t)(rdpgfx_redir_pdu.delete_surface_pdu().window_id() >> 32),
                (uint32_t)(rdpgfx_redir_pdu.delete_surface_pdu().window_id() >> 0));
            break;

        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_ERROR_PDU:
            printf("RDPGFX_REDIR_CMDID_ERROR_PDU error=0x%08x\n", rdpgfx_redir_pdu.error_pdu().error());
            break;

        case msrdp::RdpgfxRedirPdu::RDPGFX_REDIR_CMDID_CREATE_COMPOSITION_SURFACE_PDU:
            printf("[window_id=0x%08x%08x] RDPGFX_REDIR_CMDID_CREATE_COMPOSITION_SURFACE_PDU\n",
                (uint32_t)(rdpgfx_redir_pdu.create_composition_surface_pdu().window_id() >> 32),
                (uint32_t)(rdpgfx_redir_pdu.create_composition_surface_pdu().window_id() >> 0));
            break;

        default:
            printf("(unknown Microsoft::Windows::RDS::RemoteAppGraphicsRedirection type %d)\n", rdpgfx_redir_pdu.cmd_id());
            break;
    }
}

static void dump_wdag_dvc_pdu(const msrdp::WdagDvcPdu &wdag_dvc_pdu)
{
    switch(wdag_dvc_pdu.command())
    {
        case msrdp::WdagDvcPdu::WDAG_COMMAND_OPEN_DOWNLOADS_FOLDER:
            printf("WDAG_REQ_OPEN_DOWNLOADS_FOLDER\n");
            break;

        case msrdp::WdagDvcPdu::WDAG_COMMAND_VALIDATE_URL:
            printf("WDAG_REQ_VALIDATE_URL: %s://%S\n",
                wdag_dvc_pdu.wdag_msg_validate_url().use_http() ? "http" : "https",
                wdag_dvc_pdu.wdag_msg_validate_url().url().c_str());
            break;

        case msrdp::WdagDvcPdu::WDAG_COMMAND_OPEN_BROWSER_ON_HOST:
            printf("WDAG_REQ_OPEN_BROWSER_ON_HOST\n");
            break;

        default:
            printf("WDAG_RSP result 0x%08x\n", wdag_dvc_pdu.result());
            break;
    }
}

void dump_audio_format(const msrdp::AudioFormat &audio_format)
{
    printf("  AUDIO FORMAT format_tag 0x%04x, channels %d, samples_per_sec %d, avg_bytes_per_sec %d, block_align %d, bits_per_sample %d, size %d\n",
        audio_format.format_tag(),
        audio_format.channels(),
        audio_format.samples_per_sec(),
        audio_format.avg_bytes_per_sec(),
        audio_format.block_align(),
        audio_format.bits_per_sample(),
        audio_format.size_());
    //hexdump(audio_format.data().c_str(), audio_format.data().size());
}

void dump_rdpsnd_version_and_formats(const msrdp::SndVersionAndFormats &snd_version_and_formats)
{
    printf("RDPSND FORMATS flags 0x%08x, volume 0x%08x, pitch 0x%08x, d_gram_port 0x%04x, last_block_confirmed %d, version 0x%02x, pad 0x%02x, number_of_formats %d\n",
        snd_version_and_formats.flags(),
        snd_version_and_formats.volume(),
        snd_version_and_formats.pitch(),
        snd_version_and_formats.d_gram_port(),
        snd_version_and_formats.last_block_confirmed(),
        snd_version_and_formats.version(),
        snd_version_and_formats.pad(),
        snd_version_and_formats.number_of_formats());
    for ( auto _audio_format : snd_version_and_formats.snd_formats() )
    {
        auto &audio_format = _audio_format.get();
        dump_audio_format(audio_format);
    }
}

void dump_rdpsnd_sndwave2(const msrdp::Sndwave2 &sndwave2)
{
    printf("RDPSND WAVE2 time_stamp 0x%04x, format_no 0x%04x, block_no %d, pad 0x%02x 0x%02x 0x%02x, audio_time_stamp 0x%08x, data %d bytes\n",
        sndwave2.time_stamp(),
        sndwave2.format_no(),
        sndwave2.block_no(),
        (uint8_t)(sndwave2.pad()[0]), (uint8_t)(sndwave2.pad()[1]), (uint8_t)(sndwave2.pad()[2]),
        sndwave2.audio_time_stamp(),
        (uint32_t)sndwave2.data().size());
}

void dump_rdpsnd_pdu(const msrdp::RdpsndPdu &rdpsnd_pdu)
{
    switch(rdpsnd_pdu.msg_type())
    {
        case msrdp::RdpsndPdu::SNDC_WAVE_DATA:
            printf("RDPSND WAVE_DATA\n");
            break;

        case msrdp::RdpsndPdu::SNDC_CLOSE:
            printf("RDPSND CLOSE\n");
            break;

        case msrdp::RdpsndPdu::SNDC_WAVE:
            printf("RDPSND WAVE\n");
            break;

        case msrdp::RdpsndPdu::SNDC_SETVOLUME:
            printf("RDPSND SETVOLUME level %d\n", rdpsnd_pdu.sndvol().volume());
            break;

        case msrdp::RdpsndPdu::SNDC_SETPITCH:
            printf("RDPSND SETPITCH pitch %d\n", rdpsnd_pdu.sndpitch().pitch());
            break;

        case msrdp::RdpsndPdu::SNDC_WAVECONFIRM:
            printf("RDPSND WAVECONFIRM timestamp 0x%04x, confirm_block_no=%d, pad=0x%02x\n",
                rdpsnd_pdu.sndwav_confirm().time_stamp(),
                rdpsnd_pdu.sndwav_confirm().confirm_block_no(),
                rdpsnd_pdu.sndwav_confirm().pad());
            break;

        case msrdp::RdpsndPdu::SNDC_TRAINING:
            printf("RDPSND TRAINING timestamp 0x%04x, packet_size=0x%04x, data %d bytes\n",
                rdpsnd_pdu.sndtraining().time_stamp(),
                rdpsnd_pdu.sndtraining().packet_size_(),
                (uint32_t)(rdpsnd_pdu.sndtraining().data().size()));
            //hexdump(
            //    rdpsnd_pdu.sndtraining().data().c_str(),
            //    rdpsnd_pdu.sndtraining().data().size());
            break;

        case msrdp::RdpsndPdu::SNDC_FORMATS:
            dump_rdpsnd_version_and_formats(rdpsnd_pdu.snd_version_and_formats());
            break;

        case msrdp::RdpsndPdu::SNDC_CRYPTKEY:
            printf("RDPSND CRYPTKEY\n");
            break;

        case msrdp::RdpsndPdu::SNDC_WAVEENCRYPT:
            printf("RDPSND WAVEENCRYPT\n");
            break;

        case msrdp::RdpsndPdu::SNDC_UDPWAVE:
            printf("RDPSND UDPWAVE\n");
            break;

        case msrdp::RdpsndPdu::SNDC_UDPWAVELAST:
            printf("RDPSND UDPWAVELAST\n");
            break;

        case msrdp::RdpsndPdu::SNDC_QUALITYMODE:
            printf("RDPSND QUALITYMODE quality_mode 0x%04x, reserved 0x%04x\n",
                rdpsnd_pdu.sndqualitymode().quality_mode(),
                rdpsnd_pdu.sndqualitymode().reserved());
            break;

        case msrdp::RdpsndPdu::SNDC_WAVE2:
            dump_rdpsnd_sndwave2(rdpsnd_pdu.sndwave2());
            break;

        default:
            printf("(unknown RDPSND message type %d)\n", rdpsnd_pdu.msg_type());
            break;
    }
}

void dump_sndin_formats(const msrdp::SndinFormats &sndin_formats)
{
    printf("SNDIN FORMATS num_formats %d, size_formats_packet 0x%08x\n",
        sndin_formats.num_formats(),
        sndin_formats.size_formats_packet());
    for ( auto _audio_format : sndin_formats.sound_formats() )
    {
        auto &audio_format = _audio_format.get();
        dump_audio_format(audio_format);
    }
    hexdump(
         sndin_formats.extra_data().c_str(),
         sndin_formats.extra_data().size());
}

void dump_sndin_open(const msrdp::SndinOpen &sndin_open)
{
    printf("SNDIN OPEN frames_per_packet %d, initial_format %d, format_tag 0x%04x, channels %d, samples_per_sec %d, avg_bytes_per_sec %d, block_align %d, bits_per_sample %d\n",
        sndin_open.frames_per_packet(),
        sndin_open.initial_format(),
        sndin_open.format_tag(),
        sndin_open.channels(),
        sndin_open.samples_per_sec(),
        sndin_open.avg_bytes_per_sec(),
        sndin_open.block_align(),
        sndin_open.bits_per_sample());
    hexdump(sndin_open.extra_format_data().c_str(), sndin_open.extra_format_data().size());
}

static void dump_sndin_pdu(const msrdp::SndinPdu &sndin_pdu)
{
    switch(sndin_pdu.message_id())
    {
        case msrdp::SndinPdu::MSG_SNDIN_VERSION:
            printf("SNDIN VERSION version %d\n", sndin_pdu.sndin_version().version());
            break;

        case msrdp::SndinPdu::MSG_SNDIN_FORMATS:
            dump_sndin_formats(sndin_pdu.sndin_formats());
            break;

        case msrdp::SndinPdu::MSG_SNDIN_OPEN:
            dump_sndin_open(sndin_pdu.sndin_open());
            break;

        case msrdp::SndinPdu::MSG_SNDIN_OPEN_REPLY:
            printf("SNDIN OPEN REPLY result 0x%08x\n", sndin_pdu.sndin_open_reply().result());
            break;

        case msrdp::SndinPdu::MSG_SNDIN_DATA_INCOMING:
            printf("SNDIN INCOMING\n");
            break;

        case msrdp::SndinPdu::MSG_SNDIN_DATA:
            printf("SNDIN DATA size %d bytes\n", (uint32_t)sndin_pdu.sndin_data().data().size());
            break;

        case msrdp::SndinPdu::MSG_SNDIN_FORMATCHANGE:
            printf("SNDIN FORMATCHANGE new_format %d\n", sndin_pdu.sndin_formatchange().new_format());
            break;

        default:
            printf("(unknown SNDIN message type %d)\n", sndin_pdu.message_id());
            break;
    }
}

static void dump_dynvc_data(const msrdp::DynvcData &dynvc_data, uint32_t channel_id)
{
    switch(dynvc_data.channel_type())
    {
        case msrdp::DynvcPdu::CHANNEL_TYPE_RAW:
            if ( dynvc_data.rdp_raw_pdu().data().size() > 0 )
            {
                printf("DYNVC CHANNEL %d (RAW) DATA (%s):\n", 
                    channel_id, rdp_decode_direction_str(dynvc_data.to_server()));
                hexdump(
                    dynvc_data.rdp_raw_pdu().data().c_str(),
                    dynvc_data.rdp_raw_pdu().data().size()
                );
            }
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_REMOTE_APP_GRAPHICS_REDIRECTION:
            dump_rdpgfx_redir_pdu(dynvc_data.rdpgfx_redir_pdu());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_WDAG_DVC:
            dump_wdag_dvc_pdu(dynvc_data.wdag_dvc_pdu());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_ECHO:
            printf("ECHO (%s):\n", rdp_decode_direction_str(dynvc_data.to_server()));
            hexdump(
                dynvc_data.echo_pdu().message().c_str(),
                dynvc_data.echo_pdu().message().size()
            );
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_INPUT:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Input");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GRAPHICS:
            dump_rdpgfx_pdu(dynvc_data.rdpgfx_pdu());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_AUTH_REDIRECTION:
            printf("CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_AUTH_REDIRECTION\n");
            //printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::AuthRedirection");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_TELEMETRY:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Telemetry");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_LOCATION:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Location");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_GEOMETRY:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Geometry::v08.01");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_DISPLAY_CONTROL:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::DisplayControl");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_CONTROL_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Video::Control::v08.01");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_VIDEO_DATA_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Video::Data::v08.01");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_MICROSOFT_WINDOWS_RDS_FRAME_BUFFER_CONTROL_V08_01:
            printf("TODO: dump channel data '%s'\n", "Microsoft::Windows::RDS::Frame_Buffer::Control::v08.01");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_PLAYBACK_DVC:
            dump_rdpsnd_pdu(dynvc_data.rdpsnd_pdu());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_PLAYBACK_LOSSY_DVC:
            dump_rdpsnd_pdu(dynvc_data.rdpsnd_lossy_pdu().data());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_AUDIO_INPUT:
            dump_sndin_pdu(dynvc_data.sndin_pdu());
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_XPSRD:
            printf("TODO: dump channel data '%s'\n", "XPSRD");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_TSVCTKT:
            printf("TODO: dump channel data '%s'\n", "TSVCTKT");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_TSMF:
            printf("TODO: dump channel data '%s'\n", "TSMF");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_PNPDR:
            printf("TODO: dump channel data '%s'\n", "PNPDR");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_FILE_REDIRECTOR_CHANNEL:
            printf("TODO: dump channel data '%s'\n", "FileRedirectorChannel");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_RDCAMERA_DEVICE_ENUMERATOR:
            printf("TODO: dump channel data '%s'\n", "RDCamera_Device_Enumerator");
            break;

        case msrdp::DynvcPdu::CHANNEL_TYPE_RDCAMERA_DEVICE:
            printf("TODO: dump channel data '%s'\n", "RDCamera_Device");
            break;

        default:
            printf("ERROR: unimplemented dynvc channel type %d on channel id %d)\n",
                dynvc_data.channel_type(), channel_id);
            break;
    }
}

static void dump_rdpdr_pdu(const msrdp::RdpdrPdu &rdpdr_pdu)
{
    switch(rdpdr_pdu.packet_id())
    {
        case msrdp::RdpdrPdu::PAKID_CORE_CLIENTID_CONFIRM:
            printf("RDPDR CORE_CLIENTID_CONFIRM version %d.%d, client_id 0x%08x\n",
                rdpdr_pdu.dr_core_clientid_confirm().version_major(),
                rdpdr_pdu.dr_core_clientid_confirm().version_minor(),
                rdpdr_pdu.dr_core_clientid_confirm().client_id());
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_CLIENT_NAME:
            if ( rdpdr_pdu.dr_core_client_name_req().unicode() )
            {
                printf("RDPDR CORE_CLIENT_NAME unicode_flags 0x%08x, code_page 0x%08x, computer_name_w '%S'\n",
                    rdpdr_pdu.dr_core_client_name_req().unicode_flag(),
                    rdpdr_pdu.dr_core_client_name_req().code_page(),
                    (const wchar_t*)(rdpdr_pdu.dr_core_client_name_req().computer_name_w().c_str()));
            }
            else
            {
                printf("RDPDR CORE_CLIENT_NAME unicode_flags 0x%08x, code_page 0x%08x, computer_name_a '%s'\n",
                    rdpdr_pdu.dr_core_client_name_req().unicode_flag(),
                    rdpdr_pdu.dr_core_client_name_req().code_page(),
                    rdpdr_pdu.dr_core_client_name_req().computer_name_a().c_str());
            }
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_CLIENT_CAPABILITY:
            printf("RDPDR CORE_CLIENT_CAPABILITY\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_DEVICELIST_ANNOUNCE:
            printf("RDPDR CORE_DEVICELIST_ANNOUNCE\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_DEVICELIST_REMOVE:
            printf("RDPDR CORE_DEVICELIST_REMOVE\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_DEVICE_IOCOMPLETION:
            printf("RDPDR CORE_DEVICE_IOCOMPLETION\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_DEVICE_IOREQUEST:
            printf("RDPDR CORE_DEVICE_IOREQUEST\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_SERVER_ANNOUNCE:
            printf("RDPDR CORE_SERVER_ANNOUNCE version %d.%d, client_id 0x%08x\n",
                rdpdr_pdu.dr_core_server_announce_req().version_major(),
                rdpdr_pdu.dr_core_server_announce_req().version_minor(),
                rdpdr_pdu.dr_core_server_announce_req().client_id());
            break;

        case msrdp::RdpdrPdu::PAKID_PRN_CACHE_DATA:
            printf("RDPDR PRN_CACHE_DATA\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_SERVER_CAPABILITY:
            printf("RDPDR CORE_SERVER_CAPABILITY\n");
            break;

        case msrdp::RdpdrPdu::PAKID_PRN_USING_XPS:
            printf("RDPDR PRN_USING_XPS\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_USER_LOGGEDON:
            printf("RDPDR CORE_USER_LOGGEDON:\n");
            break;

        case msrdp::RdpdrPdu::PAKID_CORE_DEVICE_REPLY:
            printf("RDPDR CORE_DEVICE_REPLY\n");
            break;

        default:
            printf("(rdpdr has unknown packet ID 0x%02x)\n", rdpdr_pdu.packet_id());
            break;
    }
}

static void dump_cliprdr_format_list(const msrdp::CliprdrFormatList &format_list)
{
    switch(format_list.format_list_type())
    {
        case msrdp::CliprdrFormatList::FORMAT_LIST_SHORT_NAMES:
            printf("CLIPRDR FORMAT_LIST (short names)\n");
            for ( auto _short_format_name : format_list.cliprdr_short_format_names().short_format_names() )
            {
                auto &short_format_name = _short_format_name.get();
                if ( short_format_name.unicode() )
                {
                    printf("  FORMAT %d '%S' (unicode)\n", short_format_name.format_id(), (const wchar_t*)(short_format_name.format_unicode_name().c_str()));
                }
                else
                {
                    printf("  FORMAT %d '%s' (ascii)\n", short_format_name.format_id(), short_format_name.format_ascii_name().c_str());
                }
            }
            break;

        case msrdp::CliprdrFormatList::FORMAT_LIST_LONG_NAMES:
            printf("CLIPRDR FORMAT_LIST (long names)\n");
            for ( auto _long_format_name : format_list.cliprdr_long_format_names().long_format_names() )
            {
                auto &long_format_name = _long_format_name.get();
                printf("  FORMAT %d '%S' (unicode)\n", long_format_name.format_id(), (const wchar_t*)(long_format_name.format_name().c_str()));
            }
            break;

        case msrdp::CliprdrFormatList::FORMAT_LIST_DATA:
            printf("CLIPRDR FORMAT_LIST (raw data)\n");
            hexdump(format_list.cliprdr_raw_format_names().data().c_str(), format_list.cliprdr_raw_format_names().data().size());
            break;

        default:
            printf("  NOT IMPLEMENTED FORMAT LIST TYPE %d\n", format_list.format_list_type());
            break;
    }
}

const char *cliprdr_result(uint16_t msg_flags)
{
    if ( (msg_flags & 0x0002) != 0)
    {
        return "CB_RESPONSE_FAIL";
    }
    if ( (msg_flags & 0x0001) != 0)
    {
        return "CB_RESPONSE_OK";
    }
    return "unknown result";
}

static void dump_cliprdr_pdu(const msrdp::CliprdrPdu &cliprdr_pdu)
{
    switch(cliprdr_pdu.msg_type())
    {
        case msrdp::CliprdrPdu::CB_MONITOR_READY:
            printf("CLIPRDR MONITOR_READY\n");
            break;

        case msrdp::CliprdrPdu::CB_FORMAT_LIST:
            dump_cliprdr_format_list(cliprdr_pdu.cliprdr_format_list());
            break;

        case msrdp::CliprdrPdu::CB_FORMAT_LIST_RESPONSE:
            printf("CLIPRDR FORMAT_LIST_RESPONSE (%s)\n", cliprdr_result(cliprdr_pdu.msg_flags()));
            break;

        case msrdp::CliprdrPdu::CB_FORMAT_DATA_REQUEST:
            printf("CLIPRDR FORMAT_DATA_REQUEST\n");
            break;

        case msrdp::CliprdrPdu::CB_FORMAT_DATA_RESPONSE:
            printf("CLIPRDR FORMAT_DATA_RESPONSE (%s)\n", cliprdr_result(cliprdr_pdu.msg_flags()));
            hexdump(
                cliprdr_pdu.cliprdr_format_data_response().requested_format_data().c_str(),
                cliprdr_pdu.cliprdr_format_data_response().requested_format_data().size());
            break;

        case msrdp::CliprdrPdu::CB_TEMP_DIRECTORY:
            printf("CLIPRDR TEMP_DIRECTORY\n");
            break;

        case msrdp::CliprdrPdu::CB_CLIP_CAPS:
            printf("CLIPRDR CLIP_CAPS\n");
            break;

        case msrdp::CliprdrPdu::CB_FILECONTENTS_REQUEST:
            printf("CLIPRDR FILECONTENTS_REQUEST\n");
            break;

        case msrdp::CliprdrPdu::CB_FILECONTENTS_RESPONSE:
            printf("CLIPRDR FILECONTENTS_RESPONSE (%s) stream_id %d\n",
                cliprdr_result(cliprdr_pdu.msg_flags()),
                cliprdr_pdu.cliprdr_filecontents_response().stream_id());
            hexdump(
                cliprdr_pdu.cliprdr_filecontents_response().requested_file_contents_data().c_str(),
                cliprdr_pdu.cliprdr_filecontents_response().requested_file_contents_data().size());
            break;

        case msrdp::CliprdrPdu::CB_LOCK_CLIPDATA:
            printf("CLIPRDR LOCK_CLIPDATA clip_data_id %d\n", 
                cliprdr_pdu.cliprdr_lock_clipdata().clip_data_id());
            break;

        case msrdp::CliprdrPdu::CB_UNLOCK_CLIPDATA:
            printf("CLIPRDR UNLOCK_CLIPDATA clip_data_id %d\n", 
                cliprdr_pdu.cliprdr_unlock_clipdata().clip_data_id());
            break;

        default:
            printf("(cliprdr has unknown cmd 0x%02x)\n", cliprdr_pdu.msg_type());
            break;
    }
}

static void dump_dynvc_pdu(const msrdp::DynvcPdu &dynvc_pdu)
{
    uint32_t channel_id = dynvc_pdu.channel_id();
    switch(dynvc_pdu.cmd_type())
    {
        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CREATE_REQ:
            printf("DYNVC_CMD_CREATE_REQ: channel %d '%s'\n",
                dynvc_pdu.channel_id(),
                dynvc_pdu.dynvc_create_req().channel_name().c_str());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CREATE_RSP:
            printf("DYNVC_CMD_CREATE_RSP: channel %d status=0x%08x\n",
                dynvc_pdu.channel_id(),
                dynvc_pdu.dynvc_create_rsp().channel_status());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_DATA_FIRST:
            dump_dynvc_data(dynvc_pdu.dynvc_data_first().data(), channel_id);
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_DATA:
            dump_dynvc_data(dynvc_pdu.dynvc_data(), channel_id);
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CLOSE_REQ:
            printf("DYNVC_CMD_CLOSE_REQ: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CLOSE_RSP:
            printf("DYNVC_CMD_CLOSE_RSP: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CAPABILITY_REQ:
            printf("DYNVC_CMD_CAPABILITY_REQ: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_CAPABILITY_RSP:
            printf("DYNVC_CMD_CAPABILITY_RSP: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_DATA_FIRST_COMPRESSED:
            printf("DYNVC_CMD_DATA_FIRST_COMPRESSED: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_DATA_COMPRESSED:
            printf("DYNVC_CMD_DATA_COMPRESSED: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_SOFT_SYNC_REQ:
            printf("DYNVC_CMD_SOFT_SYNC_REQ: channel %d\n", dynvc_pdu.channel_id());
            break;

        case msrdp::DynvcPdu::DYNVC_CMD_TYPE_SOFT_SYNC_RSP:
            printf("DYNVC_CMD_SOFT_SYNC_RSP: channel %d\n", dynvc_pdu.channel_id());
            break;

        default:
            printf("(dynvc request on channel %d has unknown cmd 0x%02x)\n",
                dynvc_pdu.channel_id(), dynvc_pdu.cmd());
            break;
    }
}

static void dump_ts_rail_pdu(const msrdp::TsRailPdu &ts_rail_pdu)
{
    switch(ts_rail_pdu.order_type())
    {
        case msrdp::TsRailPdu::TS_RAIL_ORDER_EXEC:
            printf("TS_RAIL_ORDER_EXEC\n");
            printf(" exe='%S'\n", (const wchar_t*)(ts_rail_pdu.ts_rail_order_exec().exe_or_file().c_str()));
            printf(" args='%S'\n", (const wchar_t*)(ts_rail_pdu.ts_rail_order_exec().arguments().c_str()));
            printf(" working_dir='%S'\n", (const wchar_t*)(ts_rail_pdu.ts_rail_order_exec().working_dir().c_str()));
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_ACTIVATE:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_ACTIVATE\n", ts_rail_pdu.ts_rail_order_activate().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_SYSPARAM:
            ts_rail_pdu.ts_rail_order_sysparam();
            printf("TS_RAIL_ORDER_SYSPARAM\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_SYSCOMMAND:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SYSCOMMAND\n", ts_rail_pdu.ts_rail_order_syscommand().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_HANDSHAKE:
            ts_rail_pdu.ts_rail_order_handshake();
            printf("TS_RAIL_ORDER_HANDSHAKE\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_NOTIFY_EVENT:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_NOTIFY_EVENT\n", ts_rail_pdu.ts_rail_order_notify_event().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_WINDOWMOVE:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_WINDOWMOVE\n", ts_rail_pdu.ts_rail_order_windowmove().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_LOCALMOVESIZE:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_LOCALMOVESIZE\n", ts_rail_pdu.ts_rail_order_localmovesize().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_MINMAXINFO:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_MINMAXINFO\n", ts_rail_pdu.ts_rail_order_minmaxinfo().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_CLIENTSTATUS:
            ts_rail_pdu.ts_rail_order_clientstatus();
            printf("TS_RAIL_ORDER_CLIENTSTATUS\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_SYSMENU:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SYSMENU\n", ts_rail_pdu.ts_rail_order_sysmenu().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_LANGBARINFO:
            ts_rail_pdu.ts_rail_order_langbarinfo();
            printf("TS_RAIL_ORDER_LANGBARINFO\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_GET_APPID_REQ:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_REQ\n", ts_rail_pdu.ts_rail_order_get_appid_req().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_GET_APPID_RESP:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_RESP\n", ts_rail_pdu.ts_rail_order_get_appid_resp().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_TASKBARINFO:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_TASKBARINFO\n", ts_rail_pdu.ts_rail_order_taskbarinfo().window_id_tab());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_LANGUAGEIMEINFO:
            ts_rail_pdu.ts_rail_order_languageimeinfo();
            printf("TS_RAIL_ORDER_LANGUAGEIMEINFO\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_COMPARTMENTINFO:
            ts_rail_pdu.ts_rail_order_compartmentinfo();
            printf("TS_RAIL_ORDER_COMPARTMENTINFO\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_HANDSHAKE_EX:
            ts_rail_pdu.ts_rail_order_handshake_ex();
            printf("TS_RAIL_ORDER_HANDSHAKE_EX\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_ZORDER_SYNC:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_ZORDER_SYNC\n", ts_rail_pdu.ts_rail_order_zorder_sync().window_id_marker());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_CLOAK:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_CLOAK\n", ts_rail_pdu.ts_rail_order_cloak().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_POWER_DISPLAY_REQUEST:
            ts_rail_pdu.ts_rail_order_power_display_request();
            printf("TS_RAIL_ORDER_POWER_DISPLAY_REQUEST\n");
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_SNAP_ARRANGE:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_SNAP_ARRANGE\n", ts_rail_pdu.ts_rail_order_snap_arrange().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_GET_APPID_RESP_EX:
            printf("[window_id=0x%08x] TS_RAIL_ORDER_GET_APPID_RESP_EX\n", ts_rail_pdu.ts_rail_order_get_appid_resp_ex().window_id());
            break;

        case msrdp::TsRailPdu::TS_RAIL_ORDER_EXEC_RESULT:
            printf("TS_RAIL_ORDER_EXEC_RESULT\n");
            printf(" exe='%S'\n", (const wchar_t*)(ts_rail_pdu.ts_rail_order_exec_result().exe_or_file().c_str()));
            printf(" result=0x%08x\n", (uint16_t)ts_rail_pdu.ts_rail_order_exec_result().exec_result());
            printf(" returncode=0x%08x\n", ts_rail_pdu.ts_rail_order_exec_result().raw_result());
            break;

        default:
            printf("(don't know how to handle rail order 0x%04x)\n", (uint16_t)ts_rail_pdu.order_type());
            break;
    }
}

static void dump_static_channel(const msrdp::ChannelPdu &channel_pdu)
{
    // data
    switch( channel_pdu.channel_type())
    {
        case msrdp::ChannelPdu::CHANNEL_TYPE_RAW:
            printf("STATIC CHANNEL (%s) DATA (%s):\n",
                rdp_decode_fragment_compressed_str(channel_pdu.is_fragment(), channel_pdu.is_compressed()),
                rdp_decode_direction_str(channel_pdu.to_server()));
            hexdump(
                channel_pdu.channel_data().rdp_raw_pdu().data().c_str(),
                channel_pdu.channel_data().rdp_raw_pdu().data().size()
            );
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_RDPDR:
            dump_rdpdr_pdu(channel_pdu.channel_data().rdpdr_pdu());
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_CLIPRDR:
            dump_cliprdr_pdu(channel_pdu.channel_data().cliprdr_pdu());
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_DRDYNVC:
            dump_dynvc_pdu(channel_pdu.channel_data().dynvc_pdu());
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_RAIL:
            dump_ts_rail_pdu(channel_pdu.channel_data().ts_rail_pdu());
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_RAIL_RI:
            dump_ts_window_order(channel_pdu.channel_data().ts_rail_ri_pdu().window_order());
            break;

        case msrdp::ChannelPdu::CHANNEL_TYPE_RAIL_WI:
            dump_ts_window_order(channel_pdu.channel_data().ts_rail_wi_pdu().window_order());
            break;

        default:
            printf("ERROR: unimplemented static channel type %d\n", channel_pdu.channel_type());
            break;
    }
}

static void dump_t125_mcs_send_data_request(const msrdp::T125McsSendDataRequest &t125_mcs_send_data_request)
{
    if ( t125_mcs_send_data_request.channel_type() == msrdp::ChannelPdu::CHANNEL_TYPE_SECURITY_LAYER )
    {
        dump_security_layer_pdu(t125_mcs_send_data_request.ts_security_pdu());
    }
    else if ( t125_mcs_send_data_request.channel_type() == msrdp::ChannelPdu::CHANNEL_TYPE_SLOWPATH )
    {
        dump_slowpath_pdu(t125_mcs_send_data_request.slowpath_pdu());
    }
    else
    {
        dump_static_channel(t125_mcs_send_data_request.channel_pdu());
    }
}

static void dump_t125_mcs_send_data_indication(const msrdp::T125McsSendDataIndication &t125_mcs_send_data_indication)
{
    if ( t125_mcs_send_data_indication.channel_type() == msrdp::ChannelPdu::CHANNEL_TYPE_SECURITY_LAYER )
    {
        dump_security_layer_pdu(t125_mcs_send_data_indication.ts_security_pdu());
    }
    else if ( t125_mcs_send_data_indication.channel_type() == msrdp::ChannelPdu::CHANNEL_TYPE_SLOWPATH )
    {
        dump_slowpath_pdu(t125_mcs_send_data_indication.slowpath_pdu());
    }
    else
    {
        dump_static_channel(t125_mcs_send_data_indication.channel_pdu());
    }
}

static void dump_t125_mcs_pdu(const msrdp::T125McsPdu &t125_mcs_pdu)
{
    switch(t125_mcs_pdu.msg())
    {
        case msrdp::T125McsPdu::T125_MCS_MSG_SEND_DATA_REQUEST:
            dump_t125_mcs_send_data_request(t125_mcs_pdu.t125_mcs_send_data_request());
            break;

        case msrdp::T125McsPdu::T125_MCS_MSG_SEND_DATA_INDICATION:
            dump_t125_mcs_send_data_indication(t125_mcs_pdu.t125_mcs_send_data_indication());
            break;

        default:
            printf("(unhandled T125 MCS PDU of type %d)\n", t125_mcs_pdu.msg());
            break;
    }
}

static void dump_x224_tpdu(const msrdp::X224Tpdu &x224_tpdu)
{
    switch(x224_tpdu.code())
    {
        case msrdp::X224Tpdu::TPDU_CODE_EXPEDITED_DATA:
            printf("X224_TPDU_CODE_EXPEDITED_DATA\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_EXPEDITED_DATA_ACK:
            printf("X224_TPDU_CODE_EXPEDITED_DATA_ACK\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_REJECT:
            printf("X224_TPDU_CODE_REJECT\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_DATA_ACK:
            printf("X224_TPDU_CODE_DATA_ACK\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_TPDU_ERROR:
            printf("X224_TPDU_CODE_TPDU_ERROR\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_DISCONNECT_REQUEST:
            printf("X224_TPDU_CODE_DISCONNECT_REQUEST\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_DISCONNECT_CONFIRM:
            printf("X224_TPDU_CODE_DISCONNECT_CONFIRM\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_CONNECTION_CONFIRM:
            printf("X224_TPDU_CODE_CONNECTION_CONFIRM\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_CONNECTION_REQUEST:
            printf("X224_TPDU_CODE_CONNECTION_REQUEST\n");
            break;

        case msrdp::X224Tpdu::TPDU_CODE_DATA:
            dump_t125_mcs_pdu(x224_tpdu.tpdu_data().data());
            break;

        default:
            printf("(unknown X224 TPDU code)\n");
            break;
    }
}

static void dump_t123_tpkt(const msrdp::T123Tpkt &t123_tpkt)
{
    dump_x224_tpdu(t123_tpkt.data());
}

static void dump_rdp(const msrdp::Rdp &rdp)
{
    switch (rdp.pdu_type())
    {
        case msrdp::Rdp::PDU_TYPE_FASTPATH_INPUT:
            dump_ts_fp_input_pdu(rdp.ts_fp_input_pdu());
            break;
        case msrdp::Rdp::PDU_TYPE_FASTPATH_UPDATE:
            dump_ts_fp_update_pdu(rdp.ts_fp_update_pdu());
            break;
        case msrdp::Rdp::PDU_TYPE_X224:
            dump_t123_tpkt(rdp.t123_tpkt());
            break;
        default:
            printf("(unknown RDP PDU type)\n");
            break;
    }
}

void msrdp_dump(const msrdp::Rdp &rdp)
{
    dump_rdp(rdp);
}

void msrdp_dump(const void *data, size_t data_size, bool to_server)
{
    msrdp::Rdp rdp;
    rdp.set_to_server(to_server);
    bool ok = false;
    bool exception = false;
    try
    {
        ok = rdp.ParseFromString(std::string((const char*)data, data_size));
    }
    catch(...)
    {
        ok = false;
        exception = true;
    }
    if ( !ok )
    {
        if ( exception )
        {
            printf("!!! EXCEPTION PARSING DATA (%s):\n", rdp_decode_direction_str(to_server));
        }
        else
        {
            printf("ERROR PARSING DATA (%s):\n", rdp_decode_direction_str(to_server));
        }
        hexdump(data, data_size);
        return;
    }
    msrdp_dump(rdp);
}
