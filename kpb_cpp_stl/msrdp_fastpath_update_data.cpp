#include "msrdp_fastpath_update_data.hpp"

#include <string>

static std::function<bool(const std::string &,bool,uint8_t,bool,bool,std::string *)> g_decompress = nullptr;

void msrdp_set_fastpath_update_decompress_function(
    std::function<bool(
        const std::string & data,
        bool                to_server,
        uint8_t             compression_type,
        bool                packed_at_front,
        bool                packed_flushed,
        std::string *       out_data
    )> func
)
{
    g_decompress = func;
}

MsrdpFastpathUpdateData::MsrdpFastpathUpdateData(const msrdp::TsFpUpdate &_root) : ts_fp_update_(const_cast<msrdp::TsFpUpdate&>(_root))
{
    // safe to remove const as the _root can always be found by 'this' pointer a.k.a always exists in decode operation
}

size_t MsrdpFastpathUpdateData::encode_size(kpb::IO &_io)
{
    // TODO: compression not implemented
    return _io.size();
}

bool MsrdpFastpathUpdateData::encode(kpb::IO &_io)
{
    // TODO: compression not implemented
    _io;
    return true;
}

bool MsrdpFastpathUpdateData::decode(kpb::IO &_io)
{
    if ( this->ts_fp_update_.is_fragment() )
    {
        // TODO: handle fragment
        this->ts_fp_update_.set_code(msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_RAW);
        return true;
    }
    if (this->ts_fp_update_.is_compressed())
    {
        std::string data = _io.get();
        std::string out_data;
        if ( g_decompress && g_decompress(
                data,
                false, // to client
                this->ts_fp_update_.compression_type(),
                this->ts_fp_update_.compression_packed_at_front(),
                this->ts_fp_update_.compression_packed_flushed(),
                &out_data)
            )
        {
            _io.set(out_data);
            return true;
        }
        this->ts_fp_update_.set_code(msrdp::TsFpUpdateData::FASTPATH_UPDATETYPE_RAW);
        return true;
    }
    return true;
}
