#ifndef _H_MSRDP_FASTPATH_UPDATE_DATA_CPP_H_
#define _H_MSRDP_FASTPATH_UPDATE_DATA_CPP_H_

#include <cstdint>
#include <string>
#include <functional>

#include <kpb_cpp_stl_io.hpp>

#include "ts_fp_update.hpp"

/**
 * Provide a function that can decompress fastpath update data.
 */
void msrdp_set_fastpath_update_decompress_function(
    std::function<bool(
        const std::string & data,
        bool                to_server,
        uint8_t             compression_type,
        bool                packed_at_front,
        bool                packed_flushed,
        std::string *       out_data
    )> func
);

/**
 * Called internally by generated KPB code
 */
class MsrdpFastpathUpdateData
{
    public:
        MsrdpFastpathUpdateData(const msrdp::TsFpUpdate &_root);
        size_t encode_size(kpb::IO &_io);
        bool encode(kpb::IO &_io);
        bool decode(kpb::IO &_io);
    private:
        msrdp::TsFpUpdate &ts_fp_update_;
};

#endif
